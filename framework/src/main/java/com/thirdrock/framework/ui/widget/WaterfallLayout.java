package com.thirdrock.framework.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.thirdrock.framework.R;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static android.view.View.MeasureSpec.AT_MOST;
import static android.view.View.MeasureSpec.EXACTLY;
import static android.view.View.MeasureSpec.UNSPECIFIED;

/**
 * A simple waterfall layout, allows multi-column dynamic height cells.
 * Created by ywu on 15/1/26.
 */
public class WaterfallLayout extends ViewGroup {

    private class Column {
        int index;
        int measuredHeight;
        int measuredWidth;
        List<View> views;

        public Column(int index) {
            this.index = index;
            views = new LinkedList<>();
        }

        public void addView(View v) {
            views.add(v);
        }

        public int getMeasuredHeight() {
            return measuredHeight;
        }

        public int getMeasuredWidth() {
            return measuredWidth;
        }

        public void measure(int widthMeasureSpec, int heightMeasureSpec) {
            for (View child : views) {
                child.measure(widthMeasureSpec, heightMeasureSpec);
                measuredWidth = Math.max(child.getMeasuredWidth(), measuredWidth);
                measuredHeight += child.getMeasuredHeight() + rowSpacing;
            }
        }

        public void layout(int x0, int y0) {
            int y = y0;

            for (View child : views) {
                int w = child.getMeasuredWidth();
                int h = child.getMeasuredHeight();
                child.layout(x0, y, x0 + w, y + h);
                y += h + rowSpacing;
            }
        }
    }

    private int columnCount = 1;
    private int rowSpacing, colSpacing;
    private Map<Integer, Column> columns;

    public WaterfallLayout(Context context) {
        super(context);
    }

    public WaterfallLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public WaterfallLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.WaterfallLayout, 0, 0);

        try {
            setColumnCount(a.getInteger(R.styleable.WaterfallLayout_wl_column_count, 1));
            setRowSpacingPx(a.getDimensionPixelSize(R.styleable.WaterfallLayout_wl_row_spacing, 0));
            setColSpacingPx(a.getDimensionPixelSize(R.styleable.WaterfallLayout_wl_col_spacing, 0));
        } finally {
            a.recycle();
        }
    }

    public void setColumnCount(int columnCount) {
        this.columnCount = Math.max(columnCount, 1);
    }

    public void setRowSpacing(int resId) {
        setRowSpacingPx(getResources().getDimensionPixelSize(resId));
    }

    public void setRowSpacingPx(int spacingPx) {
        this.rowSpacing = Math.max(spacingPx, 0);
    }

    public void setColSpacingPx(int colSpacing) {
        this.colSpacing = Math.max(colSpacing, 0);
    }

    public void setColSpacing(int resId) {
        setColSpacingPx(getResources().getDimensionPixelSize(resId));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int x = getPaddingLeft();
        int y = getPaddingTop();

        for (int i = 0; i < columnCount; i++) {
            Column col = columns.get(i);
            col.layout(x, y);
            x += col.getMeasuredWidth() + colSpacing;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();
        int height = MeasureSpec.getSize(heightMeasureSpec) - getPaddingTop() - getPaddingBottom();
        int count = getChildCount();
        int childWidth = (width - (columnCount - 1) * colSpacing) / columnCount;

        int childHeightMeasureSpec;
        if (MeasureSpec.getMode(heightMeasureSpec) == AT_MOST) {
            childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(height, AT_MOST);
        } else {
            childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(0, UNSPECIFIED);
        }

        initColumns();
        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() == GONE) {
                continue;
            }

            Column col = columns.get(i % columnCount);
            col.addView(child);
        }

        for (int i = 0; i < columnCount; i++) {
            Column c = columns.get(i);
            c.measure(MeasureSpec.makeMeasureSpec(childWidth, EXACTLY), childHeightMeasureSpec);
            int colHeight = c.getMeasuredHeight();

            height = Math.max(height, colHeight);
        }

        setMeasuredDimension(width, height);
    }

    private void initColumns() {
        columns = new HashMap<>();
        for (int i = 0; i < columnCount; i++) {
            Column c = new Column(i);
            columns.put(i, c);
        }
    }
}
