package com.thirdrock.framework.ui.viewmodel;

/**
 * Created by ywu on 14-10-7.
 */
public interface ModelObserver<E extends Enum> {

    void onPropertyChanged(E property, Object oldValue, Object newValue) throws Exception;

    void onError(E property, Throwable e);

    void onMinorJobError(E property, Throwable e);

    void onError(E property, int errResId);

    void onMajorJobStarted();

    void onMajorJobCompleted();

    void onJobStarted(E property, Object data);

    void onJobCompleted(E property, Object data);
}
