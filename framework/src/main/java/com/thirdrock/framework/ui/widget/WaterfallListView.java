package com.thirdrock.framework.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListAdapter;

import com.etsy.android.grid.StaggeredGridView;
import com.thirdrock.framework.R;
import com.thirdrock.framework.util.L;

/**
 * A multi-column waterfall ListView
 * Created by ywu on 15/1/24.
 */
public class WaterfallListView extends StaggeredGridView {

    /**
     * Waterfall ListView event callback, for populating & rendering the view
     */
    public interface Callback {

        /**
         * Load more item to append to the ListView, when reaching the bottom
         */
        void onLoadMore();

        /**
         * Pause cell rendering, when scrolling fast
         */
        void onPauseRendering();

        /**
         * Resume cell rendering, when scrolling slowed down or stopped
         */
        void onResumeRendering();

        /**
         * ListView is scrolling
         *
         * @param isAtTop reached the top of the list or not
         * @param scrollDirection scrolling direction
         */
        void onScroll(boolean isAtTop, OnScrollListener.ScrollDirection scrollDirection);
    }

    public static class OnScrollListener implements AbsListView.OnScrollListener {
        public enum ScrollDirection {
            SCROLLING_UP, SCROLLING_DOWN, STOP_SCROLLING
        }

        protected WaterfallListView waterfallListView;

        private boolean loadingMore;

        // scrolling state
        private int prevFirstVisibleItem;
        private long lastSnapTime;
        private double scrollingSpeed;
        private boolean scrollingFast, stopped;

        public WaterfallListView getWaterfallListView() {
            return waterfallListView;
        }

        public boolean isLoadingMore() {
            return loadingMore;
        }

        public void setLoadingMore(boolean loadingMore) {
            this.loadingMore = loadingMore;
        }

        public double getScrollingSpeed() {
            return scrollingSpeed;
        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            switch (scrollState) {
                case SCROLL_STATE_IDLE:
                    if (!stopped) {
                        L.d("WFLV resume rendering when scrolling stop");
                        stopped = true;
                        waterfallListView.emitResumeRendering();
                    }
                    break;
                default:
                    if (stopped) {
                        stopped = false;
                    }
                    break;
            }
        }

        @Override
        public void onScroll(final AbsListView view, final int firstVisibleItem, final int visibleItemCount, final int totalItemCount) {
            int topRowPosition = (view == null || view.getChildCount() == 0) ?
                    0 : view.getChildAt(0).getTop();

            ScrollDirection scrollDir;
            if (firstVisibleItem > prevFirstVisibleItem) {
                scrollDir = ScrollDirection.SCROLLING_DOWN;
            } else if (firstVisibleItem < prevFirstVisibleItem) {
                scrollDir = ScrollDirection.SCROLLING_UP;
            } else {
                scrollDir = ScrollDirection.STOP_SCROLLING;
            }
            waterfallListView.emitScrolling(firstVisibleItem == 0 && topRowPosition >= 0, scrollDir);

            if (shouldLoadMore(firstVisibleItem, visibleItemCount, totalItemCount)) {
                // 距离底部还有一定距离的时候就预先加载
                L.d("WFLV time to load more");
                loadingMore = true;
                waterfallListView.emitLoadingMore();
            }

            // detect scrolling speed
            long now = System.currentTimeMillis();
            if (prevFirstVisibleItem != firstVisibleItem) {
                long timePerItem = now - lastSnapTime;
                scrollingSpeed = 1000.0 / timePerItem;

                L.v("WFLV scrolling %d->%d, at speed %f", prevFirstVisibleItem, firstVisibleItem, scrollingSpeed);
                prevFirstVisibleItem = firstVisibleItem;
                lastSnapTime = now;
            } else if (stopped && scrollingSpeed > 0) {
                L.v("WFLV scrolling stopped");
                scrollingSpeed = 0;
                lastSnapTime = now;
            }

            if (isFast() && !scrollingFast) {
                L.d("WFLV pause loading when scrolling fast");
                scrollingFast = true;
                waterfallListView.emitPauseRendering();
            } else if (isSlow() && scrollingFast) {
                L.d("WFLV resume loading when slow down");
                scrollingFast = false;
                waterfallListView.emitResumeRendering();
            }
        }

        protected boolean shouldLoadMore(final int firstVisibleItem, final int visibleItemCount, final int totalItemCount) {
            if (loadingMore) {
                return false;
            }

            int contentItemCount = totalItemCount - waterfallListView.getHeaderViewsCount() -
                    waterfallListView.getFooterViewsCount();
            int lastInScreen = firstVisibleItem + visibleItemCount;
            int columns = Math.max(waterfallListView.getColumnCount(), 1);
            int rowsToBottom = (int) Math.ceil(((double) totalItemCount - lastInScreen) / columns);

            waterfallListView.onRowsToBottomChanged(rowsToBottom);

//            L.v("WFLV shouldLoadMore? %d %d %d %d %d %d", firstVisibleItem, visibleItemCount,
//                    totalItemCount, contentItemCount, lastInScreen, rowsToBottom);
            // 加入loadMoreFailed的因素，是防止load more footer的显示、隐藏导致触发onScroll事件，从而无休止的触发load more
            return waterfallListView.isPreLoadAllowed() && contentItemCount > 0 && lastInScreen > 0 &&
                    rowsToBottom <= waterfallListView.getPreLoadDistance();
        }

        protected boolean isSlow() {
            return scrollingSpeed < 5;
        }

        protected boolean isFast() {
            return scrollingSpeed > 25;
        }
    }

    public static final int DEFAULT_COLUMN_COUNT = 2;

    private int columnCount;
    private int preLoadDistance;  // 距离底部还有多远（行数）时开始预加载
    private int headerMarginPx, footerMarginPx;  // header/footer placeholder的高度，>0 可避免被浮动action bar遮挡

    private Callback callback;
    private OnScrollListener onScrollListener;
    private boolean preLoadEnabled = true;

    // placeholders
    private boolean placeholderAdded;

    // loading more footer
    private boolean loadingMore, loadMoreFailed, hasMore = true;
    private View loadMoreView, prgLoadingMore, btnLoadMore, lblNoMore;

    public WaterfallListView(Context context) {
        super(context);
        init(context, null, 0);
    }

    public WaterfallListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public WaterfallListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            TypedArray superAttrs = context.getTheme().obtainStyledAttributes(attrs,
                    R.styleable.StaggeredGridView, 0, 0);
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                    R.styleable.WaterfallListView, 0, 0);

            try {
                columnCount = superAttrs.getInteger(R.styleable.StaggeredGridView_column_count,
                        DEFAULT_COLUMN_COUNT);
                preLoadDistance = a.getInteger(R.styleable.WaterfallListView_wlv_pre_load_distance, 0);
                headerMarginPx = a.getDimensionPixelSize(R.styleable.WaterfallListView_wlv_header_margin, 0);
                footerMarginPx = a.getDimensionPixelSize(R.styleable.WaterfallListView_wlv_footer_margin, 0);
            } finally {
                superAttrs.recycle();
            }
        }

        setOnScrollListener(new OnScrollListener());

        // add placeholder header before set adapter to keep it at the top of all headers
        addHeaderPlaceholder();
    }

    @Override
    public final void setAdapter(ListAdapter adapter) {
        // add placeholder footer before set adapter to keep it at the bottom of all footers
        initPlaceholderFooters();
        super.setAdapter(adapter);
    }

    private void initPlaceholderFooters() {
        if (placeholderAdded) {
            return;
        }

        addLoadMoreFooter();
        addFooterPlaceholder();
        placeholderAdded = true;
    }

    private void addHeaderPlaceholder() {
        if (headerMarginPx <= 0) {
            return;
        }

        View v = new View(getContext());
        v.setMinimumHeight(headerMarginPx);
        addHeaderView(v);
    }

    private void addFooterPlaceholder() {
        if (footerMarginPx <= 0) {
            return;
        }

        View v = new View(getContext());
        v.setMinimumHeight(footerMarginPx);
        addFooterView(v);
    }

    private void addLoadMoreFooter() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.wlv_loadmore_indicator, null);
        loadMoreView = view.findViewById(R.id.wrapper);
        prgLoadingMore = view.findViewById(R.id.prg_loading);
        btnLoadMore = view.findViewById(R.id.btn_load_more);
        lblNoMore = view.findViewById(R.id.lbl_no_more);

        loadMoreView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetLoadMore();
            }
        });

        btnLoadMore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoadMoreClick();
            }
        });

        addFooterView(view);
    }

    // force loading more
    private void onLoadMoreClick() {
        if (!loadingMore) {
            emitLoadingMore();
        }
    }

    // force stop loading more
    private void resetLoadMore() {
        if (loadingMore && hasMore) {
            loadingMore = false;
            hideLoadingMoreProgress(false, true);
        }
    }

    @Override
    public final void setOnScrollListener(AbsListView.OnScrollListener listener) {
        if (!(listener instanceof OnScrollListener)) {
            throw new IllegalArgumentException("An instance of WaterfallListView.OnScrollListener is required");
        }

        this.onScrollListener = (OnScrollListener) listener;
        onScrollListener.waterfallListView = this;
        super.setOnScrollListener(listener);
    }

    @Override
    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
        super.setColumnCount(columnCount);
    }

    public int getColumnCount() {
        return columnCount;
    }

    public void setPreLoadDistance(int preLoadDistance) {
        this.preLoadDistance = preLoadDistance;
    }

    public int getPreLoadDistance() {
        return preLoadDistance;
    }

    /**
     * Must set before setAdapter
     */
    public void setHeaderMarginPx(int px) {
        headerMarginPx = px;
    }

    /**
     * Must set before setAdapter
     */
    public void setHeaderMargin(int resId) {
        setHeaderMarginPx(getResources().getDimensionPixelSize(resId));
    }

    /**
     * Must set before setAdapter
     */
    public void setFooterMarginPx(int px) {
        footerMarginPx = px;
    }

    /**
     * Must set before setAdapter
     */
    public void setFooterMargin(int resId) {
        setFooterMarginPx(getResources().getDimensionPixelSize(resId));
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void onLoadMoreComplete() {
        onLoadMoreComplete(true, true);
    }

    public void onLoadMoreComplete(boolean success, boolean hasMore) {
        L.v("loading more complete, success=%s hasMore=%s", success, hasMore);
        loadingMore = false;
        loadMoreFailed = !success;
        this.hasMore = hasMore;
        hideLoadingMoreProgress(success, hasMore);

        if (onScrollListener != null) {
            onScrollListener.setLoadingMore(false);
        }
    }

    protected final void emitPauseRendering() {
        if (callback != null) {
            callback.onPauseRendering();
        }
    }

    protected final void emitResumeRendering() {
        if (callback != null) {
            callback.onResumeRendering();
        }
    }

    protected final void emitScrolling(boolean isAtTop, OnScrollListener.ScrollDirection scrollDirection) {
        L.v("scrolling %s", scrollDirection);
        if (callback != null) {
            callback.onScroll(isAtTop, scrollDirection);
        }
    }

    protected final void emitLoadingMore() {
        loadingMore = true;
        showLoadingMoreProgress();

        if (callback != null) {
            callback.onLoadMore();
        }
    }

    private boolean isPreLoadAllowed() {
        return preLoadEnabled && !loadMoreFailed && hasMore;
    }

    private void onRowsToBottomChanged(int rowsToBottom) {
        if (loadMoreFailed && rowsToBottom > 0) {
            // 解除加载失败标志，以便重新使能预加载
            loadMoreFailed = false;
        }
    }

    private void showLoadingMoreProgress() {
        loadMoreView.setVisibility(VISIBLE);
        btnLoadMore.setVisibility(GONE);
        lblNoMore.setVisibility(GONE);
        prgLoadingMore.setVisibility(VISIBLE);
    }

    private void hideLoadingMoreProgress(boolean success, boolean hasMore) {
        loadMoreView.setVisibility(success && hasMore ? GONE : VISIBLE);
        btnLoadMore.setVisibility(success ? GONE : VISIBLE);
        lblNoMore.setVisibility(success && !hasMore ? VISIBLE : GONE);
        prgLoadingMore.setVisibility(GONE);
    }

    public void setLoadingMoreVisible(boolean visible) {
        loadMoreView.setVisibility(visible ? VISIBLE : GONE);
    }

    public void setPreLoadEnabled(boolean enabled) {
        L.v("preLoadEnabled %s", enabled);
        preLoadEnabled = enabled;
    }

    // 消除NPE，参考https://github.com/jenzz/AndroidStaggeredGrid/commit/3e8f49914fa285e85089a48214342cf2dc53c501
    @Override
    public boolean showContextMenuForChild(View originalView) {
        return false;
    }

}
