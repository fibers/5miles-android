package com.thirdrock.framework.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import com.thirdrock.framework.R;


/**
 * Copied part of com.android.deskclock.timer.CountingTimeView from Google apps deskclock
 * Created by jubin on 3/2/15.
 */
public class CountingTimerView extends View {
    private static final String ONE_DIGIT = "%01d";
    private static final String TWO_DIGITS = "%02d";

    // Ratio of the space trailing the Hours and Minutes
    private static final float HOURS_MINUTES_SPACING = 0.4f;

    private String mHours, mMinutes, mSeconds;

    private Paint mPaintBigThin = new Paint();
    private float mBigFontSize;

    private UnsignedTime mBigHours, mBigMinutes, mBigSeconds;
    private float mTextHeight = 0;
    private float mTotalTextWidth;

    private int mDefaultColor;
    private boolean mRemeasreText = true;

    public CountingTimerView(Context context) {
        this(context, null);
    }

    public CountingTimerView(Context context, AttributeSet attrs){
        super(context, attrs);

        // prevent exception in Android Studio / ADT interface builder
        if (this.isInEditMode()) {
            return;
        }

        mDefaultColor = getResources().getColor(R.color.clock_orange);
        mBigFontSize = getResources().getDimensionPixelSize(R.dimen.update_time_font_size);

        Typeface androidClockMonoThin = Typeface.
                createFromAsset(context.getAssets(), "fonts/AndroidClockMono-Bold.ttf");
        mPaintBigThin.setAntiAlias(true);
        mPaintBigThin.setStyle(Paint.Style.STROKE);
        mPaintBigThin.setTextAlign(Paint.Align.CENTER);
        mPaintBigThin.setTypeface(androidClockMonoThin);

        resetTextSize();
        setTextColor(mDefaultColor);

        // allDigits will contain ten digits: "0123456789" in the default locale
        final String allDigits = String.format("%010d", 123456789);
        mBigSeconds = new UnsignedTime(mPaintBigThin, 0.f, allDigits, false);       // allDigits设为null时会使用"0123456789"
        mBigHours = new UnsignedTime(mBigSeconds, HOURS_MINUTES_SPACING, true);
        mBigMinutes = new UnsignedTime(mBigSeconds, HOURS_MINUTES_SPACING, true);
    }

    protected void resetTextSize() {
        mTextHeight = mBigFontSize;
        mPaintBigThin.setTextSize(mBigFontSize);
    }

    public void setTextColor(int textColor) {
        mPaintBigThin.setColor(textColor);
    }

    /**
     * Update the time to display. Separates that time into the hours, minutes, seconds and
     * hundredths. If update is true, the view is invalidated so that it will draw again.
     *
     * @param time           new time to display - in milliseconds
     * @param update         to invalidate the view - otherwise the time is examined to see if it is within
     *                       100 milliseconds of zero seconds and when so, invalidate the view.
     */
    // TODO:showHundredths S/B attribute or setter - i.e. unchanging over object life
    public void setTimeInMills(long time, boolean update) {
        long seconds, minutes, hours;
        seconds = time / 1000;
        minutes = seconds / 60;
        seconds = seconds - minutes * 60;
        hours = minutes / 60;
        minutes = minutes - hours * 60;
        if (hours > 9) {
            hours = 0;
        }

        mHours = String.format(ONE_DIGIT, hours);
        mMinutes = String.format(TWO_DIGITS, minutes);
        mSeconds = String.format(TWO_DIGITS, seconds);

        if (update) {
            invalidate();
        }
    }

    public void setTimeInSeconds(long time, boolean update){
        long timeInMills = time * 1000;
        setTimeInMills(timeInMills, update);
    }

    public void showTime() {
        invalidate();
    }

    @Override
    public void onDraw(Canvas canvas) {
        int width = getWidth();

        if (mRemeasreText && width != 0) {
            calcTotalTextWidth();
            width = getWidth();
            mRemeasreText = false;
        }

        int xCenter = width / 2;
        int yCenter = getHeight() / 2;

        float xTextStart = xCenter - mTotalTextWidth / 2;
        float yTextStart = yCenter + mTextHeight / 2;

        // Text color differs based on pressed state.
        int textColor = mDefaultColor;
        mPaintBigThin.setColor(textColor);

        if (mHours != null) {
            xTextStart = mBigHours.draw(canvas, mHours, xTextStart, yTextStart);
        }
        if (mMinutes != null) {
            xTextStart = mBigMinutes.draw(canvas, mMinutes, xTextStart, yTextStart);
        }
        if (mSeconds != null) {
            mBigSeconds.draw(canvas, mSeconds, xTextStart, yTextStart);
        }
    }

    private void calcTotalTextWidth() {
        mTotalTextWidth = mBigHours.calcTotalWidth(mHours) + mBigMinutes.calcTotalWidth(mMinutes)
                + mBigSeconds.calcTotalWidth(mSeconds);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mRemeasreText = true;
        resetTextSize();
    }

    /**
     * Class to measure and draw the digit pairs of hours, minutes, seconds or hundredths. Digits
     * may have an optional label. for hours, minutes and seconds, this label trails the digits
     * and for seconds, precedes the digits.
     */
    static class UnsignedTime {
        protected Paint mPaint;
        protected float mEm;
        protected float mWidth = 0;
        private final String mWidest;
        protected final float mSpacingRatio;
        private float mLabelWidth = 0;
        private boolean mHasSep = false;

        public UnsignedTime(Paint paint, float spacingRatio, String allDigits, boolean hasSeperator) {
            mPaint = paint;
            mSpacingRatio = spacingRatio;
            mHasSep = hasSeperator;

            if (TextUtils.isEmpty(allDigits)) {
                allDigits = "0123456789";
            }

            float widths[] = new float[allDigits.length()];
            int ll = mPaint.getTextWidths(allDigits, widths);
            int largest = 0;
            for (int ii = 1; ii < ll; ii++) {
                if (widths[ii] > widths[largest]) {
                    largest = ii;
                }
            }

            mEm = widths[largest];
            mWidest = allDigits.substring(largest, largest + 1);
        }

        public UnsignedTime(UnsignedTime unsignedTime, float spacingRatio, boolean hasSeparator) {
            this.mPaint = unsignedTime.mPaint;
            this.mEm = unsignedTime.mEm;
            this.mWidth = unsignedTime.mWidth;
            this.mWidest = unsignedTime.mWidest;
            this.mSpacingRatio = spacingRatio;
            this.mHasSep = hasSeparator;
        }

        protected void updateWidth(final String time) {
            mEm = mPaint.measureText(mWidest);
            mLabelWidth = mSpacingRatio * mEm;
            mWidth = time.length() * mEm;
        }

        protected void resetWidth() {
            mWidth = mLabelWidth = 0;
        }

        public float calcTotalWidth(final String time) {
            if (time != null) {
                updateWidth(time);
                return mWidth + mLabelWidth;
            } else {
                resetWidth();
                return 0;
            }
        }

        public float getLabelWidth() {
            return mLabelWidth;
        }

        /**
         * Draws each character with a fixed spacing from time starting at ii.
         *
         * @param canvas the canvas on which the time segment will be drawn
         * @param time   time segment
         * @param ii     what character to start the draw
         * @param x      offset
         * @param y      offset
         * @return X location for the next segment
         */
        protected float drawTime(Canvas canvas, final String time, int ii, float x, float y) {
            float textEm = mEm / 2f;
            while (ii < time.length()) {
                x += textEm;
                canvas.drawText(time.substring(ii, ii + 1), x, y, mPaint);
                x += textEm;
                ii++;
            }

            return x;
        }

        /**
         * Draw this time segment and append the intra-segment spacing to the x
         *
         * @param canvas the canvas on which the time segment will be drawn
         * @param time   time segment
         * @param x      offset
         * @param y      offset
         * @return X location for the next segment
         */
        public float draw(Canvas canvas, final String time, float x, float y) {
            float xEnd = drawTime(canvas, time, 0, x, y);
            if (mHasSep){
                float midPos = getLabelWidth() / 2f;
                canvas.drawText(":", xEnd + midPos, y, mPaint);
            }
            return xEnd + getLabelWidth();
        }
    }
}
