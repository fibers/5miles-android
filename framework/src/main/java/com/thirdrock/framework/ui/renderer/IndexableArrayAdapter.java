package com.thirdrock.framework.ui.renderer;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.SectionIndexer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ywu on 15/1/13.
 */
public class IndexableArrayAdapter<T> extends ArrayAdapter<T> implements SectionIndexer {

    private ArrayList<String> sectionList;
    private Map<String, Integer> sectionMap;
    private boolean sectionsBuilt;

    public IndexableArrayAdapter(Context context, int resource) {
        super(context, resource);
    }

    public IndexableArrayAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public IndexableArrayAdapter(Context context, int resource, T[] objects) {
        super(context, resource, objects);
    }

    public IndexableArrayAdapter(Context context, int resource, int textViewResourceId, T[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public IndexableArrayAdapter(Context context, int resource, List<T> objects) {
        super(context, resource, objects);
    }

    public IndexableArrayAdapter(Context context, int resource, int textViewResourceId, List<T> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @Override
    public String[] getSections() {
        buildSections();
        return sectionList.toArray(new String[sectionList.size()]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return sectionMap.get(sectionList.get(sectionIndex));
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    private void buildSections() {
        if (sectionsBuilt) {
            return;
        }

        sectionList = new ArrayList<>();
        sectionMap = new HashMap<>();
        String currSection = null;

        for (int i = 0; i < getCount(); i++) {
            T item = getItem(i);
            String strItem = String.valueOf(item).trim();
            if (item != null && !strItem.isEmpty()) {
                String c = strItem.substring(0, 1).toUpperCase();
                if (!TextUtils.equals(c, currSection)) {
                    // new section
                    currSection = c;
                    sectionList.add(currSection);
                    sectionMap.put(String.valueOf(c), i);
                }
            }
        }

        sectionsBuilt = true;
    }
}
