package com.thirdrock.framework.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thirdrock.framework.R;

/**
 * Created by ywu on 14/11/25.
 * @see <a href="http://nishantvnair.wordpress.com/2010/09/28/flowlayout-in-android/">flowlayout-in-android</a>
 */
public class FlowLayout extends ViewGroup {

    private static final int DEFAULT_MAX_LINES = 2;

    private int maxLines = DEFAULT_MAX_LINES;
    private int visibleChildCount;
    private int lineHeight, rowSpacing, colSpacing;
    private int ellipsesLayoutId;

    public FlowLayout(Context context) {
        super(context);
    }

    public FlowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FlowLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.FlowLayout,
                0, 0);

        try {
            maxLines = a.getInt(R.styleable.FlowLayout_fl_max_lines, DEFAULT_MAX_LINES);
            rowSpacing = a.getDimensionPixelSize(R.styleable.FlowLayout_fl_row_spacing, 0);
            colSpacing = a.getDimensionPixelSize(R.styleable.FlowLayout_fl_col_spacing, 0);
            ellipsesLayoutId = a.getResourceId(R.styleable.FlowLayout_fl_ellipses_layout, 0);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int width = r - l;
        int count = visibleChildCount;
        int xpos = getPaddingLeft();
        int ypos = getPaddingTop();

        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                final int childw = child.getMeasuredWidth();
                final int childh = child.getMeasuredHeight();
                if (xpos + childw > width) {
                    xpos = getPaddingLeft();
                    ypos += lineHeight;
                }
                child.layout(xpos, ypos, xpos + childw, ypos + childh);
                xpos += childw + colSpacing;
            }
        }

        // layout ellipses view: 'more...'
        if (visibleChildCount < getChildCount()) {
            View v = loadEllipsesView();
            if (v != null) {
                addView(v);
                v.measure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST), lineHeight);
                v.layout(xpos, ypos, xpos + v.getMeasuredWidth(), ypos + v.getMeasuredHeight());
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = MeasureSpec.getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();
        int height = MeasureSpec.getSize(heightMeasureSpec) - getPaddingTop() - getPaddingBottom();
        final int count = getChildCount();
        int line_height = 0;

        int childHeightMeasureSpec;
        if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.AT_MOST) {
            childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.AT_MOST);
        } else {
            childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        }

        int lines = 1;
        int allowedChildCount = 0;
        int xpos = getPaddingLeft();
        int ypos = getPaddingTop();

        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                child.measure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST), childHeightMeasureSpec);
                final int childw = child.getMeasuredWidth();
                line_height = Math.max(line_height, child.getMeasuredHeight() + rowSpacing);

                if (xpos + childw > width) {
                    if (lines < maxLines) {
                        // place it onto a new line
                        xpos = getPaddingLeft();
                        ypos += line_height;
                        lines++;
                    } else {
                        break;
                    }
                }

                allowedChildCount++;
                xpos += childw + colSpacing;
            }
        }

        this.visibleChildCount = hasEllipsesView() && allowedChildCount < getChildCount() ?
                allowedChildCount - 1 : allowedChildCount;  // reserve one place for 'more...'
        this.lineHeight = line_height;

        if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.UNSPECIFIED) {
            height = ypos + line_height;

        } else if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.AT_MOST) {
            if (ypos + line_height < height) {
                height = ypos + line_height;
            }
        }
        setMeasuredDimension(width, height);
    }

    private boolean hasEllipsesView() {
        return ellipsesLayoutId > 0;
    }

    private View loadEllipsesView() {
        if (ellipsesLayoutId == 0) {
            return null;
        }

        return LayoutInflater.from(getContext()).inflate(ellipsesLayoutId, null);
    }
}
