package com.thirdrock.framework.ui.helper;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import com.thirdrock.framework.util.L;

/**
 * Created by ywu on 15/3/16.
 */
public class SlidingUpPanelHelper implements Animator.AnimatorListener, View.OnTouchListener {
    private View panel;
    private int drawerViewId;

    private boolean animating;
    private boolean isExpanded;

    private OnPanelStateListener onPanelStateListener;

    public static interface OnPanelStateListener {
        void onPanelExpanded();

        void onPanelCollapsed();
    }

    public SlidingUpPanelHelper(View panel) {
        this(panel, 0);
    }

    public SlidingUpPanelHelper(View panel, int drawerViewId) {
        this.panel = panel;
        this.drawerViewId = drawerViewId;

        panel.setOnTouchListener(this);

        if (drawerViewId > 0) {
            panel.findViewById(drawerViewId).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickDrawer();
                }
            });
        }
    }

    private Animator makeCollapseAnimator() {
        Animator anim = ObjectAnimator.ofFloat(panel, "translationY", 0, -panel.getHeight());
        anim.setDuration(250);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.addListener(this);
        return anim;
    }

    private Animator makeExpandAnimator() {
        Animator anim = ObjectAnimator.ofFloat(panel, "translationY", -panel.getHeight(), 0);
        anim.setDuration(250);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.addListener(this);
        return anim;
    }

    public void setOnPanelStateListener(OnPanelStateListener onPanelStateListener) {
        this.onPanelStateListener = onPanelStateListener;
    }

    public void expand() {
        if (animating) {
            return;
        }

        L.d("show filter pane h=%d", panel.getHeight());
        makeExpandAnimator().start();
    }

    public void collapse() {
        if (animating) {
            return;
        }

        L.d("close filter pane h=%d", panel.getHeight());
        makeCollapseAnimator().start();
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public boolean isCollapsed() {
        return !isExpanded;
    }

    @Override
    public void onAnimationStart(Animator animation) {
        animating = true;
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        animating = false;
        togglePanelState();
    }

    @Override
    public void onAnimationCancel(Animator animation) {
        animating = false;
    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    private void togglePanelState() {
        isExpanded = !isExpanded;

        if (onPanelStateListener != null) {
            if (isExpanded) {
                onPanelStateListener.onPanelExpanded();
            } else {
                onPanelStateListener.onPanelCollapsed();
            }
        }
    }

    private void onClickDrawer() {
        if (isExpanded()) {
            collapse();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
