package com.thirdrock.framework.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class CustomView extends RelativeLayout{
	
	final int disabledBackgroundColor = Color.parseColor("#E2E2E2");
	int beforeBackground;

	final static String SCHEMA_RES_AUTO_XML = "http://schemas.android.com/apk/res-auto";
	final static String SCHEMA_ANDROID_XML = "http://schemas.android.com/apk/res/android";

	public CustomView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		if(enabled) {
			setBackgroundColor(beforeBackground);
		} else {
			setBackgroundColor(disabledBackgroundColor);
		}

		invalidate();
	}
	
	boolean animation = false;
	
	@Override
	protected void onAnimationStart() {
		super.onAnimationStart();
		animation = true;
	}
	
	@Override
	protected void onAnimationEnd() {
		super.onAnimationEnd();
		animation = false;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(animation) {
			invalidate();
		}
	}
}