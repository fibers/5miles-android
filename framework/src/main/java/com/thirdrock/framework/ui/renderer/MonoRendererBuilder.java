package com.thirdrock.framework.ui.renderer;

import com.pedrogomez.renderers.Renderer;
import com.pedrogomez.renderers.RendererBuilder;

import java.util.LinkedList;


/**
 * Simple RendererBuilder for ListView which has only one type of renderer
 * Created by ywu on 14-10-7.
 */
public class MonoRendererBuilder<T> extends RendererBuilder<T> {

    private Renderer<T> prototype;  // the mono Renderer prototype

    public <R extends Renderer<T>> MonoRendererBuilder(R prototype) {
        this.prototype = prototype;
        LinkedList<Renderer<T>> prototypes = new LinkedList<Renderer<T>>();
        prototypes.add(prototype);
        setPrototypes(prototypes);
    }

    @Override
    protected Class getPrototypeClass(T content) {
        return prototype.getClass();
    }
}
