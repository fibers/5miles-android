package com.thirdrock.framework.ui.renderer;

import com.pedrogomez.renderers.AdapteeCollection;
import com.thirdrock.framework.util.L;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ywu on 14-10-16.
 */
public class FixedSizeAdapteeCollection<T> implements AdapteeCollection<T> {
    private final int maxSize;
    private List<T> data;
    private int totalSize;
    private int firstIndex;
    private T nullValue;

    public FixedSizeAdapteeCollection(int maxSize, T nullValue) {
        this(maxSize, null, nullValue);
    }

    public FixedSizeAdapteeCollection(int maxSize, List<T> data, T nullValue) {
        this.data = data != null ? data : new ArrayList<T>();
        this.maxSize = Math.max(maxSize, 1);
        this.nullValue = nullValue;
    }

    @Override
    public int size() {
        return totalSize;
    }

    @Override
    public T get(int index) {
        return doGet(index - firstIndex);
    }

    protected T doGet(int i) {
        return (i >= 0 && i < data.size()) ? data.get(i) : nullValue;
    }

    @Override
    public void add(T element) {
        ensureFixedSize(1);
        data.add(element);
    }

    @Override
    public void remove(T element) {
        data.remove(element);
        totalSize--;
    }

    @Override
    public void addAll(Collection<T> elements) {
        if (elements != null) {
            ensureFixedSize(elements.size());
            data.addAll(elements);
            L.v("new elements %d, total %d", elements.size(), size());
        }
    }

    @Override
    public void removeAll(Collection<T> elements) {
        data.removeAll(elements);
        totalSize -= elements.size();
    }

    @Override
    public void clear() {
        totalSize = 0;
        firstIndex = 0;
        data.clear();
    }

    /**
     * Make room for new elements.
     * @param aSize additional size expected
     */
    private void ensureFixedSize(int aSize) {
        if (aSize > maxSize) {
            throw new IllegalArgumentException("size acquiring is larger than the max size:" + aSize);
        }

        totalSize += aSize;
        int free = maxSize - data.size();
        int diff = free - aSize;
        if (diff < 0) {
            L.v("remove %d old elements", -diff);
            data = data.subList(-diff, data.size());
            firstIndex -= diff;
        }

        L.v("current totalSize %d, firstIndex %d", totalSize, firstIndex);
    }
}
