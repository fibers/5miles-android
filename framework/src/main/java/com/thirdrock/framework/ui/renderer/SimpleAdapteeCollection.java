package com.thirdrock.framework.ui.renderer;

import com.pedrogomez.renderers.AdapteeCollection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ywu on 14-10-7.
 */
public class SimpleAdapteeCollection<T> implements AdapteeCollection<T> {

    private final List<T> data;

    public SimpleAdapteeCollection() {
        this(null);
    }

    public SimpleAdapteeCollection(List<T> data) {
        this.data = data != null ? data : new ArrayList<T>();
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public T get(int index) {
        return data.get(index);
    }

    @Override
    public void add(T element) {
        data.add(element);
    }

    @Override
    public void remove(T element) {
        data.remove(element);
    }

    @Override
    public void addAll(Collection<T> elements) {
        data.addAll(elements);
    }

    @Override
    public void removeAll(Collection<T> elements) {
        data.removeAll(elements);
    }

    @Override
    public void clear() {
        data.clear();
    }
}
