package com.thirdrock.framework.ui.viewmodel;

import android.app.Activity;
import android.app.Fragment;

import com.thirdrock.framework.rest.RestObserver;
import com.thirdrock.framework.rest.SingleResponseRestObserver;
import com.thirdrock.framework.util.L;

import java.util.LinkedList;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.observables.AndroidObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Simple base class of ViewModel
 * Created by ywu on 14-10-7.
 *
 * @param <E> type of the ViewModel properties enumeration
 */
public abstract class AbsViewModel<E extends Enum> {

    protected class MajorJobObserver<T> implements Observer<T> {
        protected final E property;

        public MajorJobObserver(E property) {
            this.property = property;
        }

        @Override
        public void onCompleted() {
            emitMajorJobCompleted();
        }

        @Override
        public void onError(Throwable e) {
            emitMajorJobCompleted();
            emit(property, e);
        }

        @Override
        public void onNext(T t) {
            emit(property, null, t);
        }
    }


    protected class MajorApiJobObserver<T> extends SingleResponseRestObserver<T> {
        protected final E property;

        // requestId is used for rest api tracking
        public MajorApiJobObserver(String requestId, E property) {
            super(requestId);
            this.property = property;
        }

        @Override
        public void onCompleted() {
            emitMajorJobCompleted();
        }

        @Override
        public void onError(Throwable e) {
            emitMajorJobCompleted();
            emit(property, e);
        }

        @Override
        public void doOnNext(T t) {
            emit(property, null, t);
        }
    }


    protected class MinorJobObserver<T> implements Observer<T> {
        protected final E property;
        protected final Object arg;

        public MinorJobObserver(E property) {
            this(property, null);
        }

        public MinorJobObserver(E property, Object arg) {
            this.property = property;
            this.arg = arg;
        }

        @Override
        public void onCompleted() {
            emitJobCompleted(property, arg);
        }

        @Override
        public void onError(Throwable e) {
            emitJobCompleted(property, arg);
            emitMinorJobError(property, e);
        }

        @Override
        public void onNext(T t) {
            emit(property, null, t);
        }
    }


    protected class MinorApiJobObserver<T> extends SingleResponseRestObserver<T> {
        protected final E property;
        protected final Object arg;

        public MinorApiJobObserver(String request, E property) {
            this(request, property, null);
        }

        public MinorApiJobObserver(String request, E property, Object arg) {
            super(request);
            this.property = property;
            this.arg = arg;
        }

        @Override
        public void onCompleted() {
            emitJobCompleted(property, arg);
        }

        @Override
        public void onError(Throwable e) {
            emitJobCompleted(property, arg);
            emitMinorJobError(property, e);
        }

        @Override
        public void doOnNext(T t) {
            emit(property, null, t);
        }
    }

    private Object context;

    private List<ModelObserver<E>> observers = new LinkedList<ModelObserver<E>>();

    public AbsViewModel() {

    }

    protected AbsViewModel(Activity activity) {
        this.context = activity;
    }

    protected AbsViewModel(Fragment fragment) {
        this.context = fragment;
    }

    protected AbsViewModel(android.support.v4.app.Fragment fragment) {
        this.context = fragment;
    }

    public Object getContext() {
        return context;
    }

    public void setContext(Activity activity) {
        this.context = activity;
    }

    public void setContext(android.support.v4.app.Fragment fragment) {
        this.context = fragment;
    }

    public void setContext(Fragment fragment) {
        this.context = fragment;
    }

    /**
     * Subscribes events of this ViewModel
     *
     * @param observer the observer
     * @param <T>      observer type
     */
    public <T extends ModelObserver<E>> void subscribe(T observer) {
        if (observer != null && !observers.contains(observer)) {
            observers.add(observer);
        }
    }

    /**
     * Stop listening to events of this ViewModel
     *
     * @param observer the observer
     * @param <T>      observer type
     */
    public <T extends ModelObserver<E>> void unsubscribe(T observer) {
        if (observer != null) {
            observers.remove(observer);
        }
        onModelObserverUnsubscribed();
    }

    /**
     * Called back when the ModelObserver is un-subscribed from this ViewModel
     */
    protected void onModelObserverUnsubscribed() {

    }

    /**
     * Emit property change events
     */
    protected void emit(E property, Object oldValue, Object newValue) {
        for (ModelObserver<E> o : observers) {
            try {
                o.onPropertyChanged(property, oldValue, newValue);
            } catch (Exception e) {
                L.e(e);
            }
        }
    }

    /**
     * Emit errors
     */
    protected void emit(E property, Throwable t) {
        for (ModelObserver<E> o : observers) {
            try {
                o.onError(property, t);
            } catch (Exception e) {
                L.e(e);
            }
        }
    }

    /**
     * Emit error for minor jobs.
     */
    protected void emitMinorJobError(E property, Throwable t) {
        for (ModelObserver<E> o : observers) {
            try {
                o.onMinorJobError(property, t);
            } catch (Exception e) {
                L.e(e);
            }
        }
    }

    /**
     * Emit errors
     */
    protected void emitError(E property, int errResId) {
        for (ModelObserver<E> o : observers) {
            try {
                o.onError(property, errResId);
            } catch (Exception e) {
                L.e(e);
            }
        }
    }

    protected void emitMajorJobStarted() {
        for (ModelObserver<E> o : observers) {
            try {
                o.onMajorJobStarted();
            } catch (Exception e) {
                L.e(e);
            }
        }
    }

    protected void emitMajorJobCompleted() {
        for (ModelObserver<E> o : observers) {
            try {
                o.onMajorJobCompleted();
            } catch (Exception e) {
                L.e(e);
            }
        }
    }

    protected void emitJobStarted(E property, Object data) {
        for (ModelObserver<E> o : observers) {
            try {
                o.onJobStarted(property, data);
            } catch (Exception e) {
                L.e(e);
            }
        }
    }

    protected void emitJobCompleted(E property, Object data) {
        for (ModelObserver<E> o : observers) {
            try {
                o.onJobCompleted(property, data);
            } catch (Exception e) {
                L.e(e);
            }
        }
    }

    protected void unsubscribe(Subscription... subscriptions) {
        if (subscriptions != null) {
            for (Subscription s : subscriptions) {
                if (s != null && !s.isUnsubscribed()) {
                    s.unsubscribe();
                }
            }
        }
    }

    protected <T> Observer<T> newMajorJobObserver(E property) {
        return new MajorJobObserver<>(property);
    }

    // request is used as id for http request tracking
    protected <T> RestObserver<T> newMajorApiJobObserver(String request, E property) {
        return new MajorApiJobObserver<>(request, property);
    }

    protected <T> Observer<T> newMinorJobObserver(E property) {
        return newMinorJobObserver(property, null);
    }

    protected <T> Observer<T> newMinorJobObserver(E property, Object arg) {
        return new MinorJobObserver<>(property, arg);
    }

    protected <T> RestObserver<T> newMinorApiJobObserver(String request, E property) {
        return newMinorApiJobObserver(request, property, null);
    }

    protected <T> RestObserver<T> newMinorApiJobObserver(String request, E property, Object arg) {
        return new MinorApiJobObserver<>(request, property, arg);
    }

    /**
     * Scheduling a most common case of Rx job in ViewModel, running in background, observed on main thread
     */
    protected <T> Observable<T> schedule(Observable<T> job) {
        return bindContext(job.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    /**
     * Scheduling a background Rx job, which running and observed in background,
     * and not bound to activity/fragment lifecycle either
     */
    protected <T> Observable<T> scheduleBackground(Observable<T> job) {
        return job.subscribeOn(Schedulers.io());
    }

    /**
     * Bind the given Observable to activity/fragment lifecycle, if context is available
     */
    protected <T> Observable<T> bindContext(Observable<T> observable) {
        if (context instanceof Activity) {
            return AndroidObservable.bindActivity((Activity) context, observable);
        } else if (context instanceof Fragment ||
                context instanceof android.support.v4.app.Fragment) {
            return AndroidObservable.bindFragment(context, observable);
        } else {
            return observable;
        }
    }
}
