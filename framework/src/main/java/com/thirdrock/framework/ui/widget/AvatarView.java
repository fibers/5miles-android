package com.thirdrock.framework.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.thirdrock.framework.R;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * Rounded ImageView for user avatar, with verification indicator.
 * Created by ywu on 15/1/7.
 */
public class AvatarView extends FrameLayout {

    public static final float DEFAULT_VERIFICATION_INDICATOR_SIZE_SCALE = 0.33f;
    public static final float MAX_VERIFICATION_INDICATOR_SIZE_SCALE = 0.5f;

    private int avatarInset;
    private int borderWidth;
    private int borderColor;
    private int verificationIndicator;  // resource id for verification indicator
    private float verificationIndicatorSizeScale = DEFAULT_VERIFICATION_INDICATOR_SIZE_SCALE;
    private int defaultImage;  // image shown for blank view

    private RoundedImageView ivAvatar;
    private ImageView ivIndicator;

    private final LayoutParams avatarLayoutParams = new LayoutParams(MATCH_PARENT, MATCH_PARENT);
    private final LayoutParams indicatorLayoutParams = new LayoutParams(WRAP_CONTENT, WRAP_CONTENT);

    public AvatarView(Context context) {
        this(context, null);
    }

    public AvatarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AvatarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        ivIndicator = new ImageView(context);
        ivIndicator.setScaleType(ImageView.ScaleType.FIT_XY);
        ivIndicator.setLayoutParams(indicatorLayoutParams);

        ivAvatar = new RoundedImageView(context);
        ivAvatar.setOval(true);
        ivAvatar.setScaleType(ImageView.ScaleType.CENTER_CROP);
        ivAvatar.setLayoutParams(avatarLayoutParams);

        addView(ivAvatar);
        addView(ivIndicator);

        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs == null) {
            return;
        }

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.AvatarView,
                0, 0);

        try {
            setVerificationIndicator(a.getResourceId(R.styleable.AvatarView_verificationIndicator, 0));
            setBorderWidthPixel(a.getDimensionPixelSize(R.styleable.AvatarView_avatarBorderWidth, 0));
            setBorderColor(a.getColor(R.styleable.AvatarView_avatarBorderColor, 0));
            avatarInset = a.getDimensionPixelSize(R.styleable.AvatarView_avatarInset, 0);
            verificationIndicatorSizeScale = a.getFloat(R.styleable.AvatarView_verificationIndicatorSizeScale,
                    DEFAULT_VERIFICATION_INDICATOR_SIZE_SCALE);
            verificationIndicatorSizeScale = Math.min(verificationIndicatorSizeScale,
                    MAX_VERIFICATION_INDICATOR_SIZE_SCALE);
            setDefaultImage(a.getResourceId(R.styleable.AvatarView_defaultImage, 0));
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        avatarLayoutParams.setMargins(avatarInset, avatarInset, avatarInset, avatarInset);

        final int width = MeasureSpec.getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();
        int size = (int) Math.ceil(width * verificationIndicatorSizeScale);
        indicatorLayoutParams.width = size;
        indicatorLayoutParams.height = size;

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        indicatorLayoutParams.gravity = Gravity.BOTTOM | Gravity.END;
        super.onLayout(changed, left, top, right, bottom);
    }

    public int getVerificationIndicatorResId() {
        return verificationIndicator;
    }

    public void setVerificationIndicator(int resId) {
        this.verificationIndicator = resId;
        if (verificationIndicator > 0) {
            ivIndicator.setImageResource(verificationIndicator);
        }
    }

    public int getAvatarInsetPixel() {
        return avatarInset;
    }

    public void setAvatarInset(int resId) {
        setAvatarInsetPixel(getResources().getDimensionPixelSize(resId));
        requestLayout();
    }

    public void setAvatarInsetPixel(int avatarInset) {
        this.avatarInset = avatarInset;
    }

    public int getBorderWidthPixel() {
        return borderWidth;
    }

    public void setBorderWidth(int resId) {
        setBorderWidthPixel(getResources().getDimensionPixelSize(resId));
    }

    public void setBorderWidthPixel(int borderWidth) {
        this.borderWidth = borderWidth;
        this.ivAvatar.setBorderWidth((float) borderWidth);
    }

    public int getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(int borderColor) {
        this.borderColor = borderColor;
        this.ivAvatar.setBorderColor(borderColor);
    }

    public int getDefaultImage() {
        return defaultImage;
    }

    public void setDefaultImage(int defaultImage) {
        this.defaultImage = defaultImage;
        if (defaultImage > 0) {
            ivAvatar.setImageResource(defaultImage);
        }
    }

    public boolean setImageDrawable(Drawable drawable) {
        ivAvatar.setImageDrawable(drawable);
        return true;
    }

    public boolean setImageBitmap(Bitmap bitmap) {
        ivAvatar.setImageBitmap(bitmap);
        return true;
    }

    public ImageView getAvatarImageView() {
        return ivAvatar;
    }
}
