package com.thirdrock.framework.exception;

/**
 * Restful api error
 * Created by ywu on 14-9-27.
 */
public class RestException extends NetworkException {

    private int errorCode;

    public RestException() {
    }

    public RestException(int statusCode, String detailMessage) {
        super(statusCode, detailMessage);
    }

    public RestException(int statusCode, String detailMessage, Throwable throwable) {
        super(statusCode, detailMessage, throwable);
    }

    public RestException(int statusCode, Throwable throwable) {
        super(statusCode, throwable);
    }

    public RestException(int statusCode, int errorCode, String detailMessage, Throwable throwable) {
        super(statusCode, detailMessage, throwable);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
