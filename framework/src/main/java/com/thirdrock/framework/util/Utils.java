package com.thirdrock.framework.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;

import javax.inject.Inject;

import static android.view.ViewTreeObserver.OnGlobalLayoutListener;

/**
 * Created by ywu on 14-10-7.
 */
public final class Utils {

    @Inject
    static Context context;

    private static Boolean isDebug;
    private static Boolean isGray;
    private static Boolean isOfficial;


    private Utils() {

    }

    public static Context getContext() {
        return context;
    }

    /**
     * TODO a better way to get context within a library project?
     *
     * @param ctx Context
     */
    public static void setContext(Context ctx) {
        if (ctx != null) {
            context = ctx.getApplicationContext();
        }
    }

    /**
     * It should be a debugging package, when the version name ends with 'SNAPSHOT'.
     *
     * @return is this a debugging package
     */
    public static boolean isDebug() {
        if (isDebug != null) {
            return isDebug;
        }

        ApplicationInfo info = getApplicationInfo();
        isDebug = (info != null) && info.metaData.getString("APP_PUB_TYPE").contains("SNAPSHOT");


        return isDebug;
    }

    public static boolean isGray() {
        if (isGray != null) {
            return isGray;
        }

        ApplicationInfo info = getApplicationInfo();
        isGray = (info != null) && info.metaData.getString("APP_PUB_TYPE").contains("STAGE");


        return isGray;
    }

    /**
     * Opposite of #isDebug
     *
     * @return is this a released package
     */
    public static boolean isRelease() {

        if (isOfficial != null) {
            return isOfficial;
        }

        ApplicationInfo info = getApplicationInfo();
        isOfficial = (info != null) && info.metaData.getString("APP_PUB_TYPE").contains("OFFICIAL");

        return isOfficial;

    }

    public static PackageInfo getPackageInfo() {
        if (context == null) {
            return null;
        }

        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Utils", "get packageInfo failed", e);  // 此处只能使用Log，因L依赖getPackageInfo
        }

        return null;
    }
    public static ApplicationInfo getApplicationInfo() {
        if (context == null) {
            return null;
        }

        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Utils", "get applicationInfo failed", e);
        }

        return null;
    }

    public static void removeOnGlobalLayoutListener(View v, OnGlobalLayoutListener listener) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            removeGlobalOnLayoutListenerOldApi(v, listener);
        } else {
            removeGlobalOnLayoutListenerNewApi(v, listener);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private static void removeGlobalOnLayoutListenerNewApi(View v, OnGlobalLayoutListener listener) {
        v.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
    }

    @SuppressWarnings("deprecation")
    private static void removeGlobalOnLayoutListenerOldApi(View v, OnGlobalLayoutListener listener) {
        v.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
    }

    public static void doOnGlobalLayout(final View v, final Runnable action) {
        if (v.getMeasuredWidth() == 0) {
            ViewTreeObserver observer = v.getViewTreeObserver();
            if (observer.isAlive()) {
                observer.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        invokeAction(action);
                        removeOnGlobalLayoutListener(v, this);
                    }
                });
            }
        } else {
            invokeAction(action);
        }
    }

    public static void doOnLayoutChange(final View v, final Runnable action) {
        if (v.getMeasuredWidth() == 0) {
            v.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    invokeAction(action);
                    v.removeOnLayoutChangeListener(this);
                }
            });
        } else {
            invokeAction(action);
        }
    }

    private static void invokeAction(Runnable action) {
        try {
            action.run();
        } catch (Exception e) {
            L.e(e);
        }
    }

    public static void setBackground(View v, Bitmap bitmap) {
        setBackground(v, bitmap, 0xff);
    }

    public static void setBackground(View v, Bitmap bitmap, int alpha) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            setBackgroundNewApi(v, bitmap, alpha);
        } else {
            setBackgroundOldApi(v, bitmap, alpha);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private static void setBackgroundNewApi(View v, Bitmap bitmap, int alpha) {
        v.setBackground(newBitmapDrawableNewApi(v.getResources(), bitmap, alpha));
    }

    @SuppressWarnings("deprecation")
    private static void setBackgroundOldApi(View v, Bitmap bitmap, int alpha) {
        v.setBackgroundDrawable(newBitmapDrawableOldApi(bitmap, alpha));
    }

    public static void setBackground(Window v, Bitmap bitmap) {
        BitmapDrawable d;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            d = newBitmapDrawableNewApi(v.getContext().getResources(), bitmap, 0xff);
        } else {
            d = newBitmapDrawableOldApi(bitmap, 0xff);
        }

        v.setBackgroundDrawable(d);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private static BitmapDrawable newBitmapDrawableNewApi(Resources resources, Bitmap bitmap, int alpha) {
        BitmapDrawable d = new BitmapDrawable(resources, bitmap);
        if (alpha < 0xff && alpha >= 0) {
            d.setAlpha(alpha);
        }
        return d;
    }

    @SuppressWarnings("deprecation")
    private static BitmapDrawable newBitmapDrawableOldApi(Bitmap bitmap, int alpha) {
        BitmapDrawable d = new BitmapDrawable(bitmap);
        if (alpha < 0xff && alpha >= 0) {
            d.setAlpha(alpha);
        }
        return d;
    }

    public static void sendLocalBroadcast(Intent intent) {
        if (context == null || intent == null) {
            return;
        }

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    /**
     * Get a concise description text for the active network state, for logging.
     */
    public static String getNetworkStateDesc() {
        if (context == null) {
            return "";
        }

        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connMgr.getActiveNetworkInfo();

        StringBuilder sb = new StringBuilder();
        if (info == null) {
            sb.append("N/A");
        } else {
            sb.append(info.getTypeName())
                    .append("|").append(info.getSubtypeName())
                    .append("|").append(info.getDetailedState())
                    .append("|").append(info.getReason());
        }

        return sb.toString();
    }

    /**
     * From https://github.com/navasmdc/MaterialDesignLibrary/blob/master/MaterialDesign/src/com/gc/materialdesign/utils/Utils.java
     * Convert Dp to Pixel
     */
    public static int dpToPx(float dp, Resources resources){
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics());
        return (int) px;
    }

    public static void setViewEnabledRecursively(View v, boolean enabled) {
        if (v == null) {
            return;
        }

        v.setEnabled(enabled);
        if (v instanceof ViewGroup) {
            ViewGroup g = (ViewGroup) v;
            for (int i = 0; i < g.getChildCount(); i++) {
                setViewEnabledRecursively(g.getChildAt(i), enabled);
            }
        }
    }

}
