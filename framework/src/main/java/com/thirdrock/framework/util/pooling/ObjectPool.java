package com.thirdrock.framework.util.pooling;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

/**
 * A generic purpose object pool.
 * Created by ywu on 14-9-27.
 */
public final class ObjectPool {
    protected static final int DEFAULT_POOL_CAPACITY = 50;
    private static final Map<Class, Entry> ENTRIES = new HashMap<Class, Entry>();

    /*
     * Managed polled unit, contains a linked list of pooled objects.
     */
    private static class Entry<T> {
        final Object lock = new Object[0];
        final int capacity;
        int size;
        SoftReference<Pooled<T>> head;

        Entry(int capacity) {
            this.capacity = capacity > 0 ? capacity : DEFAULT_POOL_CAPACITY;
        }

        public Pooled<T> getHead() {
            return head == null ? null : head.get();
        }

        public void setHead(Pooled<T> head) {
            this.head = new SoftReference<>(head);
        }
    }

    private ObjectPool() {
    }

    /**
     * Obtain a instance of the specified type, from pool if available, or an new instance.
     * @param type the poolable type, the class MUST have a public default constructor,
     *             and can have a cleaner method, which should be annotated as @Recycle,
     *             to do the cleaning job, before the object can be use again.
     * @param <T> type parameter
     * @return a pooled wrapper of the desired object,
     *   you can get the underlying object using the Pooled#get method,
     *   and return it to the pool using the Pooled#recycle method, if you want to reuse it later.
     */
    public static <T> Pooled<T> obtain(Class<T> type) {
        Entry<T> e = getEntry(type, getPoolCapacity(type));
        synchronized (e.lock) {
            if (e.head != null) {
                // get object from head of the linked list
                Pooled<T> obj = e.getHead();
                e.setHead(obj.getNext());
                obj.setNext(null);
                e.size--;
                return obj;
            }
        }
        return Pooled.newInstance(type);
    }

    /**
     * Empty the object pool.
     */
    public static void clean() {
        ENTRIES.clear();
    }

    @SuppressWarnings("unchecked")
    private static <T> Entry<T> getEntry(Class<T> type, int poolCapacity) {
        Entry<T> e;

        synchronized (ObjectPool.class) {
            e = ENTRIES.get(type);
            if (e == null) {
                e = new Entry<T>(poolCapacity);
                ENTRIES.put(type, e);
            }
        }

        return e;
    }

    /*
     * return the pooled object to the pool
     */
    static <T> void recycle(Pooled<T> pooled) {
        Entry<T> e = getEntry(pooled.type, 0);
        synchronized (e.lock) {
            if (e.size < e.capacity) {
                // prepend to the linked list
                pooled.setNext(e.getHead());
                e.setHead(pooled);
                e.size++;
                pooled.reset();
            }
        }
    }

    private static int getPoolCapacity(Class type) {
        int capacity = 0;
        if (type.isAnnotationPresent(PoolSize.class)) {
            PoolSize poolSize = (PoolSize) type.getAnnotation(PoolSize.class);
            capacity = poolSize.value();
        }
        return capacity > 0 ? capacity : DEFAULT_POOL_CAPACITY;
    }
}
