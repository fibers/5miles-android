package com.thirdrock.framework.util.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.thirdrock.framework.R;

/**
 * Act as target of notification, just redirect to another activity (through intent), and cancel the notification
 * Created by ywu on 14-10-17.
 */
public class RedirectActivity extends Activity {
    public static final String DATA_KEY_INTENT = "RedirectActivity.key.intent";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transparent);
        onNewIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        finish();

        Intent targetIntent = intent.getParcelableExtra(DATA_KEY_INTENT);

        if (targetIntent != null) {
            startActivity(targetIntent);
        }
    }
}
