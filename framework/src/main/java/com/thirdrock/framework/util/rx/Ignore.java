package com.thirdrock.framework.util.rx;

/**
 * A dummy Observer which just ignore all emits/notifications.
 * Created by ywu on 14-10-13.
 */
public final class Ignore<T> extends SimpleObserver<T> {

    private static Ignore instance;

    @SuppressWarnings("unchecked")
    public static <T> Ignore<T> getInstance() {
        if (instance == null) {
            instance = new Ignore();
        }
        return (Ignore<T>) instance;
    }

    private Ignore() {

    }
}
