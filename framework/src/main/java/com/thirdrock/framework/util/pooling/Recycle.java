package com.thirdrock.framework.util.pooling;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Mark a method capable for performing cleaning job when the object is being recycled.
 * Created by ywu on 14-9-27.
 */

@Retention(RUNTIME) @Target(METHOD)
public @interface Recycle {

}
