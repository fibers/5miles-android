package com.thirdrock.framework.util.pooling;

import android.util.Log;

import java.lang.ref.SoftReference;
import java.lang.reflect.Method;

/**
 * A wrapper of the pooled object.
 * Created by ywu on 14-9-27.
 */
public class Pooled<T> {
    private static final String TAG = Pooled.class.getName();

    Class<T> type;
    private T value;
    private SoftReference<Pooled<T>> next;

    // a factory method
    static <K> Pooled<K> newInstance(Class<K> type) {
        Pooled<K> p = new Pooled<K>();
        try {
            p.type = type;
            p.value = type.newInstance();

            Log.v(TAG, "create instance for " + type);
        } catch (Exception e) {
            throw new IllegalArgumentException("failed to create instance for " + type, e);
        }
        return p;
    }

    private Pooled() {

    }

    /**
     * Unboxing the pooled object
     *
     * @return the pooled object
     */
    public T get() {
        return value;
    }

    /**
     * Return the pooled object to the pool, so that you can reuse it later.
     */
    public void recycle() {
        ObjectPool.recycle(this);
    }

    /**
     * Reset the pooled object, before it can be reused.
     */
    void reset() {
        // find method of the pooled object which performs cleaning job
        for (Method method : type.getMethods()) {
            if (method.isAnnotationPresent(Recycle.class)) {
                try {
                    method.invoke(value);
                } catch (Exception e) {
                    Log.e(TAG, "invoking recycle method failed", e);
                }
                break;
            }
        }
    }

    Pooled<T> getNext() {
        return next == null ? null : next.get();
    }

    void setNext(Pooled<T> obj) {
        next = new SoftReference<Pooled<T>>(obj);
    }

}
