package com.thirdrock.framework.util.rx;

import com.thirdrock.framework.util.L;

import rx.Observer;

/**
 * Empty implement of Observer, for the convenience of sub-classing
 * Created by ywu on 14/11/26.
 */
public class SimpleObserver<T> implements Observer<T> {

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        L.e(e);
    }

    @Override
    public void onNext(T t) {

    }
}
