package com.thirdrock.framework.util.pooling;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Specify the pool capacity for a poolable type.
 * Created by ywu on 14-9-27.
 */
@Retention(RUNTIME) @Target(TYPE)
public @interface PoolSize {
    /**
     * Specify the pool capacity, default is 0, meaning ObjectPool#DEFAULT_POOL_CAPACITY is used
     */
    int value() default 0;
}
