package com.thirdrock.framework.util.rx;

import rx.Observable;
import rx.Subscriber;

/**
 * Simple Observable#OnSubscribe which emits only one event.
 * Created by ywu on 14/11/12.
 */
public abstract class Emit1<T> implements Observable.OnSubscribe<T> {

    @Override
    public void call(Subscriber<? super T> subscriber) {
        if (subscriber.isUnsubscribed()) {
            return;
        }

        try {
            T event = call();

            if (!subscriber.isUnsubscribed()) {
                subscriber.onNext(event);
                subscriber.onCompleted();
            }
        } catch (Exception e) {
            if (!subscriber.isUnsubscribed()) {
                subscriber.onError(e);
            }
        }
    }

    /**
     * Do your job here
     * @return the result should be emitted to subscribers
     */
    abstract protected T call() throws Exception;
}
