package com.thirdrock.framework;

import com.thirdrock.framework.util.Utils;

import dagger.Module;

/**
 * Created by ywu on 14-10-11.
 */
@Module(complete = false, library = true,
    staticInjections = {Utils.class}
)
public class FrameworkModule {
}
