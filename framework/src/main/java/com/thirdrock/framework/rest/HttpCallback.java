package com.thirdrock.framework.rest;

import com.loopj.android.http.TextHttpResponseHandler;
import com.thirdrock.framework.util.L;

import org.apache.http.Header;

import rx.Subscriber;

/**
 * Created by ywu on 14-9-27.
 */
public class HttpCallback<T> extends TextHttpResponseHandler {
    private final HttpBodyParser<T> bodyParser;
    private final Subscriber<? super T> subscriber;

    public HttpCallback(HttpBodyParser<T> bodyParser, Subscriber<? super T> subscriber) {
        this.bodyParser = bodyParser;
        this.subscriber = subscriber;
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String body, Throwable e) {
        if (!subscriber.isUnsubscribed()) {
            subscriber.onError(bodyParser.parseError(statusCode, headers, body, e));
        }
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, String body) {
        if (subscriber.isUnsubscribed()) {
            return;
        }

        if (statusCode > 399) {
            subscriber.onError(bodyParser.parseError(statusCode, headers, body, null));
            return;
        }

        try {
            L.v("http response: [%d] %s", statusCode, body);
            subscriber.onNext(bodyParser.parse(body));
            subscriber.onCompleted();
        } catch (Exception e) {
            subscriber.onError(new RuntimeException("response parsing failed", e));
        }
    }
}