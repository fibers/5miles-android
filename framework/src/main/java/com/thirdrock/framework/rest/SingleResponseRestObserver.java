package com.thirdrock.framework.rest;

/**
 * Restful api observer with api delay stats, which calculates delay in onNext method.
 * Created by ywu on 15/3/6.
 */
public abstract class SingleResponseRestObserver<T> extends RestObserver<T> {

    public SingleResponseRestObserver() {
    }

    public SingleResponseRestObserver(String requestId) {
        super(requestId);
    }

    @Override
    public final void onNext(T t) {
        onResponseReceived();  // response received
        doOnNext(t);
    }

    protected abstract void doOnNext(T t);
}
