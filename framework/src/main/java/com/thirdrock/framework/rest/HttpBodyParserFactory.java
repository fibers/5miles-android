package com.thirdrock.framework.rest;

/**
 * Created by ywu on 14-10-12.
 */
public interface HttpBodyParserFactory {

    <T> HttpBodyParser<T> createParser(Class<T> type);

}
