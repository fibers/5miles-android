package com.thirdrock.framework.rest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ywu on 14-9-27.
 */
public class HeaderBuilder {

    private Map<String, String> headers = new HashMap<String, String>();

    protected void append(String key, Object value) {
        headers.put(key, String.valueOf(value));
    }

    protected void append(Map<String, String> headers) {
        this.headers.putAll(headers);
    }

    protected void prepareHeaders() {
    }

    public Map<String, String> build() {
        // 目前每次重新构建header，避免注销后无法清除session信息
        headers.clear();
        prepareHeaders();
        return headers;
    }
}
