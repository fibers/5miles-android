package com.thirdrock.framework.rest;

import android.content.Intent;

import com.thirdrock.framework.exception.NetworkException;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.Utils;

import rx.Observer;

/**
 * Restful api observer with api delay stats
 * Created by ywu on 15/3/6.
 */
public abstract class RestObserver<T> implements Observer<T> {
    public static final String ACT_REQ_FAILURE = "com.thirdrock.framework.rest.HttpObserver.RequestFailure";
    public static final String EXTRA_THROWABLE = "com.thirdrock.framework.rest.HttpObserver.Throwable";

    public static final String ACT_RESP_RECEIVED = "com.thirdrock.framework.rest.HttpObserver.ResponseReceived";
    public static final String EXTRA_REQ_ID = "com.thirdrock.framework.rest.HttpObserver.RequestId";
    public static final String EXTRA_RESP_DELAY = "com.thirdrock.framework.rest.HttpObserver.ResponseDelay";

    private String requestId;
    private long startTime, delay;

    protected RestObserver() {
        this("");
    }

    public RestObserver(String requestId) {
        this.requestId = requestId;
    }

    public RestObserver<T> reset() {
        startTime = System.currentTimeMillis();
        delay = 0;
        return this;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public long getDelay() {
        return delay;
    }

    protected void onResponseReceived() {
        if (startTime == 0) {
            throw new IllegalStateException("forget reset the observer before subscription?");
        }

        delay = System.currentTimeMillis() - startTime;
        emitRequestDelay(requestId, delay);
    }

    public static void emitRequestDelay(String req, long delay) {
        L.v("api %s responses in %dms", req, delay);
        Utils.sendLocalBroadcast(new Intent(ACT_RESP_RECEIVED)
                .putExtra(EXTRA_REQ_ID, req)
                .putExtra(EXTRA_RESP_DELAY, delay));
    }

    public static void emitRequestFailure(NetworkException e) {
        Utils.sendLocalBroadcast(new Intent(ACT_REQ_FAILURE).putExtra(EXTRA_THROWABLE, e));
    }

    public static void emitRequestFailure(int statusCode, Throwable cause) {
        emitRequestFailure(new NetworkException(statusCode, cause));
    }
}
