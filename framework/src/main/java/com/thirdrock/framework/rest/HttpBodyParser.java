package com.thirdrock.framework.rest;

import com.thirdrock.framework.exception.NetworkException;

import org.apache.http.Header;

/**
 * Created by ywu on 14-9-27.
 */
public interface HttpBodyParser<T> {

    T parse(String body) throws Exception;

    NetworkException parseError(int statusCode, Header[] headers, String body, Throwable e);
}