package com.thirdrock.framework.rest;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.thirdrock.framework.util.L;

import java.util.Map;

import rx.Observable;
import rx.Subscriber;

/**
 * Base repository for accessing backend data through network
 * Created by ywu on 14-9-27.
 */
public abstract class AbsRestClient {
    public static final int DEFAULT_TIMEOUT = 10000;
    public static final int DEFAULT_RETRIES = 2;
    public static final int DEFAULT_RETRY_INTV = 2000;

    private String baseUrl;
    private HeaderBuilder headerBuilder;
    private HttpBodyParserFactory bodyParserFactory;
    private AsyncHttpClient httpClient;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public AsyncHttpClient getHttpClient() {
        if (httpClient == null) {
            // 此处使用SyncHttpClient，把线程控制的任务交给RxJava
            httpClient = new SyncHttpClient();
            httpClient.setTimeout(DEFAULT_TIMEOUT);
            httpClient.setMaxRetriesAndTimeout(DEFAULT_RETRIES, DEFAULT_RETRY_INTV);
        }
        return httpClient;
    }

    public void setHttpClient(AsyncHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public HeaderBuilder getHeaderBuilder() {
        return headerBuilder;
    }

    public void setHeaderBuilder(HeaderBuilder headerBuilder) {
        this.headerBuilder = headerBuilder;
    }

    public HttpBodyParserFactory getBodyParserFactory() {
        return bodyParserFactory;
    }

    public void setBodyParserFactory(HttpBodyParserFactory bodyParserFactory) {
        this.bodyParserFactory = bodyParserFactory;
    }

    /**
     * Reset http client, to clean headers.
     */
    public void reset() {
        httpClient = null;
    }

    /**
     * 如果已经是完整URL,则直接返回.
     * update: 如果是绝对地址,则直接返回.
     * @param url
     * @return
     */
    protected String toAbsoluteUrl(String url) {
        if(url.indexOf("http")==0)return url;//TODO: 使用正则进行匹配.
        return baseUrl + url;
    }

    protected synchronized void initHeaders() {
        AsyncHttpClient httpClient = getHttpClient();
        if (headerBuilder == null || httpClient == null) {
            return;
        }

        // 目前每次重新构建header，避免注销后无法清除session信息
        Map<String, String> headers = headerBuilder.build();
        if (headers != null) {
            for (Map.Entry<String, String> e : headers.entrySet()) {
                httpClient.addHeader(e.getKey(), e.getValue());
            }
        }
    }

    protected <T> Observable<T> get(final String url,
                                    final RequestParams params,
                                    final HttpBodyParser<T> respParser) {
        initHeaders();

        return Observable.create(new Observable.OnSubscribe<T>() {
            @Override
            public void call(Subscriber<? super T> subscriber) {
                if (subscriber.isUnsubscribed()) {
                    return;
                }

                String theUrl = toAbsoluteUrl(url);

                // disable caching
                theUrl += (theUrl.contains("?") ? "&" : "?") + "t=" + System.currentTimeMillis();

                L.v("http get <%s> with params: %s", theUrl, params);
                getHttpClient().get(theUrl, params,
                        new HttpCallback<>(respParser, subscriber));
            }
        });
    }

    protected <T> Observable<T> get(final String url,
                                    final HttpBodyParser<T> respParser) {
        return get(url, null, respParser);
    }

    protected <T> Observable<T> get(final String url,
                                    final RequestParams params,
                                    final Class<T> type) {
        HttpBodyParser<T> parser = bodyParserFactory.createParser(type);
        return get(url, params, parser);
    }

    protected <T> Observable<T> get(final String url,
                                    final Class<T> type) {
        return get(url, null, type);
    }

    protected <T> Observable<T> post(final String url,
                                     final RequestParams params,
                                     final HttpBodyParser<T> respParser) {
        initHeaders();

        return Observable.create(new Observable.OnSubscribe<T>() {
            @Override
            public void call(Subscriber<? super T> subscriber) {
                if (subscriber.isUnsubscribed()) {
                    return;
                }

                String theUrl = toAbsoluteUrl(url);
                L.v("http post <%s> with params: %s", theUrl, params);
                getHttpClient().post(theUrl, params,
                        new HttpCallback<>(respParser, subscriber));
            }
        });
    }

    protected <T> Observable<T> post(final String url,
                                     final HttpBodyParser<T> respParser) {
        return post(url, null, respParser);
    }

    protected <T> Observable<T> post(final String url,
                                     final RequestParams params,
                                     final Class<T> type) {
        HttpBodyParser<T> parser = bodyParserFactory.createParser(type);
        return post(url, params, parser);
    }

    protected <T> Observable<T> post(final String url, final Class<T> type) {
        return post(url, null, type);
    }
}
