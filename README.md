# [5miles android](https://play.google.com/store/apps/details?id=com.thirdrock.fivemiles)

[ ![Build Status on Ship.io](http://ship.io/jobs/bXrgStZYhBULHBt0/build_status.png)](https://ship.io/dashboard#/jobs/2615/history)


## 开发环境搭建

* 下载安装[Android SDK](http://developer.android.com/sdk/installing/index.html?pkg=tools)
    * 安装SDK Tools (24.0.2), Platform-tools (21), Build-tools (21.1.2)
    * 安装target SDK 5.0.1 (level 21)
    * Android Support Library (21.0.3), Android Support Repository (11)
    * Google Play services (22), Google Repository (15)
* 下载安装[Android Studio](http://developer.android.com/sdk/installing/studio.html)
* 获取代码 `git clone git@github.com:3rdStone/5miles-android.git`
* 使用Android Studio打开工程（或import），指向工程所在目录的`settings.gradle`即可


## 版本分支管理

* 使用 [Git Flow](https://github.com/nvie/gitflow) 模式管理多版本的并行开发和维护
* 需求和bug在[GitHub](https://github.com/3rdStone/5miles-android)中管理

### 关于版本号

Beta版本（开发、调试版本）的`versionName`(`app/build.gradle`)统一使用`-SNAPSHOT`后缀，框架将会自动识别，使用debug日志级别，并连接测试后台


## 新架构

 参考[Architecting Android…The clean way?](http://fernandocejas.com/2014/09/03/architecting-android-the-clean-way/)

 将app分为四层，每一层分别对应一个module（android library project）：

 1. framework
 2. domain
 3. repository
 4. app (presentation)

 主要技术：

 * 使用[RxJava](https://github.com/ReactiveX/RxJava/wiki)编写异步逻辑
 * IoC框架: [Dager](http://square.github.io/dagger/)
 * MVVM: 参考 [EffectiveAndroidUI](https://github.com/pedrovgs/EffectiveAndroidUI)，暂不采用auto data binding

新老架构将会共存很长一段时间，采取逐步改写的方式平滑过渡

   * app的root package是`com.thirdrock.fivemiles`，新代码请放在这个package下
   * 其余如`com.insthub.fivemiles`等package是继承自第三方开发团队的代码


## FAQ
[FAQ](https://github.com/3rdStone/5miles-android/wiki/FAQ)
