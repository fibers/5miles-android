package com.huewu.pla.lib;

import android.animation.Animator;
import android.animation.ValueAnimator;

/**
 * Simple base Animator listener
 * Created by ywu on 14/11/10.
 */
public abstract class SimpleAnimatorListener implements Animator.AnimatorListener {

    @Override
    public void onAnimationStart(Animator animator) {

    }

    @Override
    public void onAnimationEnd(Animator animator) {

    }

    @Override
    public void onAnimationCancel(Animator animator) {

    }

    @Override
    public void onAnimationRepeat(Animator animator) {

    }

}
