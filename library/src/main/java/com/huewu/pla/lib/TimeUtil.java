package com.huewu.pla.lib;

import android.content.Context;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TimeUtil {

	public static String timeAgo(Context ctx, String timeStr) {
		Date date = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss zzz");
			date = format.parse(timeStr);
		} catch (ParseException e) {
			e.printStackTrace();
			return "";
		}

		long timeStamp = date.getTime();

		Date currentTime = new Date();
		long currentTimeStamp = currentTime.getTime();
		long seconds = (currentTimeStamp - timeStamp) / 1000;

		long minutes = Math.abs(seconds / 60);
		long hours = Math.abs(minutes / 60);
		long days = Math.abs(hours / 24);

		if (seconds < 60) {
			return ctx.getString(R.string.time_elapse_now);
		} else if (seconds < 120) {
			return ctx.getString(R.string.time_elapse_1m);
		} else if (minutes < 60) {
			return ctx.getString(R.string.time_elapse_m, minutes);
		} else if (minutes < 120) {
			return ctx.getString(R.string.time_elapse_1h);
		} else if (hours < 24) {
			return ctx.getString(R.string.time_elapse_h, hours);
		} else if (hours < 24 * 2) {
			return ctx.getString(R.string.time_elapse_1d);
		} else if (days < 30) {
			return ctx.getString(R.string.time_elapse_d, days);
		} else if (days < 365) {
			return ctx.getString(R.string.time_elapse_month,
                new BigDecimal(days / 30).setScale(0, BigDecimal.ROUND_HALF_UP));
		} else {
//			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
//			String dateString = formatter.format(date);
//			return dateString;
            return DateFormat.getDateInstance(DateFormat.MEDIUM).format(date);
		}

	}

}
