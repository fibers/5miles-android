package com.thirdrock.repository;

import com.thirdrock.domain.SellerInfo;

import java.util.List;

import rx.Observable;

/**
 * 和User相关的读写请求接口
 * Created by ywu on 15/4/21.
 */
public interface UserRepository {
    String BLOCK_USER_URL = "/block/";
    String UNBLOCK_USER_URL = "/unblock/";

    String SELLERS_NEARBY_URL = "/users_nearby/";
    String FOLLOW_URL = "/follow/";

    /**
     * Block/unblock the given user
     *
     * @param uid      user id
     * @param blocking blocking or unblocking the user
     */
    Observable<Void> blockUser(String uid, boolean blocking);

    Observable<List<SellerInfo>> getNearbySellersList(float lat, float lon);

    Observable<Void> batchfollow(Iterable<String> userIdList);
}
