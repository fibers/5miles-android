package com.thirdrock.repository;

import rx.Observable;

/**
 * Configurations from server
 * Created by ywu on 15/4/29.
 */
public interface ConfigRepository {

    String INIT_URL = "/init/";

    /**
     * Get global configurations from backend
     *
     * @return promised app config json string
     */
    Observable<String> getAppConfig();

}
