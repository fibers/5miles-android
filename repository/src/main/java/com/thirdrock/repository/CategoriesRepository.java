package com.thirdrock.repository;

import com.thirdrock.protocol.CategoriesResp;

import rx.Observable;

/**
 * Created by jubin on 9/3/15.
 */
public interface CategoriesRepository {
    String CATGORIES_URL = "/new_categories/";

    Observable<CategoriesResp> getCategoriesResp();
}
