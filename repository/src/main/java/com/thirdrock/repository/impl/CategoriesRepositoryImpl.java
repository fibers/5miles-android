package com.thirdrock.repository.impl;

import com.thirdrock.framework.rest.HeaderBuilder;
import com.thirdrock.framework.rest.HttpBodyParserFactory;
import com.thirdrock.protocol.CategoriesResp;
import com.thirdrock.repository.AbsRepository;
import com.thirdrock.repository.CategoriesRepository;

import rx.Observable;

/**
 * Created by jubin on 9/3/15.
 */
public class CategoriesRepositoryImpl extends AbsRepository implements CategoriesRepository {

    public CategoriesRepositoryImpl(String baseUrl,
                                    HeaderBuilder headerBuilder,
                                    HttpBodyParserFactory bodyParserFactory){
        super(baseUrl, headerBuilder, bodyParserFactory);
    }

    @Override
    public Observable<CategoriesResp> getCategoriesResp() {
        return get(CATGORIES_URL, CategoriesResp.class);
    }
}
