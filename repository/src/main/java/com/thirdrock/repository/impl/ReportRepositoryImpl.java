package com.thirdrock.repository.impl;

import android.text.TextUtils;

import com.loopj.android.http.RequestParams;
import com.thirdrock.protocol.ReportReasonsResp;
import com.thirdrock.protocol.ReportReasonsResp__JsonHelper;
import com.thirdrock.protocol.ReportReasonsWrapper;
import com.thirdrock.framework.rest.HeaderBuilder;
import com.thirdrock.framework.rest.HttpBodyParserFactory;
import com.thirdrock.repository.AbsHttpBodyParser;
import com.thirdrock.repository.AbsRepository;
import com.thirdrock.repository.ReportRepository;

import java.util.Collections;
import java.util.List;

import rx.Observable;

/**
 * Implementation of ReportRepository
 * Created by jubin on 7/5/15.
 */
public class ReportRepositoryImpl extends AbsRepository implements ReportRepository {
    public ReportRepositoryImpl(String baseUrl,
                                HeaderBuilder headerBuilder,
                                HttpBodyParserFactory bodyParserFactory) {
        super(baseUrl, headerBuilder, bodyParserFactory);
    }

    @Override
    public Observable<List<ReportReasonsWrapper>> getRemoteReportReasons() {
        return get(REPORT_REASONS_URL, new AbsHttpBodyParser<List<ReportReasonsWrapper>>() {
            @Override
            public List<ReportReasonsWrapper> parse(String body) throws Exception {
                if (TextUtils.isEmpty(body)) {
                    return Collections.emptyList();
                }

                body = "{ \"objects\":" + body + "}";  // ig-json-parser不能处理jsonArray应答
                ReportReasonsResp resp = ReportReasonsResp__JsonHelper.parseFromJson(body);
                if (resp == null) {
                    return null;
                }
                return resp.getReportReasonsWrapperList();
            }
        });
    }

    @Override
    public Observable<List<ReportReasonsWrapper>> getRemoteReportReasonsItem() {
        RequestParams params = new RequestParams("type", REPORT_TYPE_ITEM);

        return get(REPORT_REASONS_URL, params, new AbsHttpBodyParser<List<ReportReasonsWrapper>>() {
            @Override
            public List<ReportReasonsWrapper> parse(String body) throws Exception {
                if (TextUtils.isEmpty(body)) {
                    return Collections.emptyList();
                }

                body = "{ \"objects\":" + body + "}";  // ig-json-parser不能处理jsonArray应答
                ReportReasonsResp resp = ReportReasonsResp__JsonHelper.parseFromJson(body);
                if (resp == null) {
                    return null;
                }
                return resp.getReportReasonsWrapperList();
            }
        });
    }

    @Override
    public Observable<List<ReportReasonsWrapper>> getRemoteReportReasonsUser() {
        RequestParams params = new RequestParams("type", REPORT_TYPE_USER);
        return get(REPORT_REASONS_URL, params, new AbsHttpBodyParser<List<ReportReasonsWrapper>>() {
            @Override
            public List<ReportReasonsWrapper> parse(String body) throws Exception {
                if (TextUtils.isEmpty(body)) {
                    return Collections.emptyList();
                }

                body = "{ \"objects\":" + body + "}";  // ig-json-parser不能处理jsonArray应答
                ReportReasonsResp resp = ReportReasonsResp__JsonHelper.parseFromJson(body);
                if (resp == null) {
                    return null;
                }
                return resp.getReportReasonsWrapperList();
            }
        });
    }

    @Override
    public Observable<List<ReportReasonsWrapper>> getRemoteReportReasonsReview() {
        RequestParams params = new RequestParams("type", REPORT_TYPE_REVIEW);
        return get(REPORT_REASONS_URL, params, new AbsHttpBodyParser<List<ReportReasonsWrapper>>() {
            @Override
            public List<ReportReasonsWrapper> parse(String body) throws Exception {
                if (TextUtils.isEmpty(body)) {
                    return Collections.emptyList();
                }

                body = "{ \"objects\":" + body + "}";  // ig-json-parser不能处理jsonArray应答
                ReportReasonsResp resp = ReportReasonsResp__JsonHelper.parseFromJson(body);
                if (resp == null) {
                    return null;
                }
                return resp.getReportReasonsWrapperList();
            }
        });
    }
}