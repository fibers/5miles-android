package com.thirdrock.repository.impl;

import com.thirdrock.domain.MyItems;
import com.thirdrock.framework.rest.HeaderBuilder;
import com.thirdrock.framework.rest.HttpBodyParserFactory;
import com.thirdrock.protocol.Meta;
import com.thirdrock.protocol.MyItemsResp;
import com.thirdrock.repository.AbsRepository;
import com.thirdrock.repository.ProfileRepository;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by fibers on 15/7/6.
 */
public class ProfileRepositoryImpl extends AbsRepository implements ProfileRepository{

    private Meta myItemsMeta;
    private MyItems myItems;

    public ProfileRepositoryImpl(String baseUrl, HeaderBuilder headerBuilder, HttpBodyParserFactory bodyParserFactory) {
        super(baseUrl, headerBuilder, bodyParserFactory);
    }


    @Override
    public Observable<MyItems> getMyItems() {

        String url = MY_ITEMS_URL;
        if(hasMoreMyItems()){
            url += myItemsMeta.getNext();
        }

        return get(url, MyItemsResp.class).map(new Func1<MyItemsResp, MyItems>() {
            @Override
            public MyItems call(MyItemsResp myItemsResp) {

                myItemsMeta = myItemsResp.getMeta();
                if(myItems == null){
                    myItems = new MyItems();
                }

                myItems.addItems(myItemsResp.getListMyiItems());

                return myItems;
            }
        });

    }

    @Override
    public boolean hasMoreMyItems() {
        return myItemsMeta != null && myItemsMeta.hasNext();
    }
}
