package com.thirdrock.repository.impl;

import com.loopj.android.http.RequestParams;
import com.thirdrock.domain.Reputation;
import com.thirdrock.domain.Review;
import com.thirdrock.framework.rest.HeaderBuilder;
import com.thirdrock.framework.rest.HttpBodyParserFactory;
import com.thirdrock.protocol.Meta;
import com.thirdrock.protocol.ReviewListResp;
import com.thirdrock.repository.AbsRepository;
import com.thirdrock.repository.ReviewRepository;

import rx.Observable;
import rx.functions.Func1;

/**
 * Review repository.
 * Note: 此处维护特定用户的状态数据，因此请不要使用singleton
 * Created by ywu on 15/4/17.
 */
public class ReviewRepositoryImpl extends AbsRepository implements ReviewRepository {

    private Meta reviewListMeta;
    private Reputation reputation;

    public ReviewRepositoryImpl(String baseUrl, HeaderBuilder headerBuilder, HttpBodyParserFactory bodyParserFactory) {
        super(baseUrl, headerBuilder, bodyParserFactory);
    }

    @Override
    public Observable<Void> postReview(String targetUid, String itemId, int rating, String comment, boolean isSeller) {
        if (rating < 1 || rating > 5) {
            throw new IllegalArgumentException("rating score is invalid: " + rating);
        }

        RequestParams params = new RequestParams();
        params.put("target_user", targetUid);
        params.put("item_id", itemId);
        params.put("direction", isSeller ? 1 : 0);
        params.put("score", rating);
        params.put("comment", comment);
        return post(POST_REVIEW_URL, params, Void.class);
    }

    // 目前没有必要提供refresh式的查询，如有next，一律采用增量查询
    @Override
    public Observable<Reputation> getUserReputation(String uid) {
        RequestParams params = new RequestParams();
        params.put("user_id", uid);

        String url = REVIEWS_URL;
        if (hasMoreReviews()) {
            url += reviewListMeta.getNext();
        } else {
            params.put("limit", REVIEW_PAGE_SIZE);
        }

        return get(url, params, ReviewListResp.class).map(new Func1<ReviewListResp, Reputation>() {
            @Override
            public Reputation call(ReviewListResp resp) {
                reviewListMeta = resp.getMeta();

                if (reputation == null) {
                    reputation = new Reputation();
                }

                reputation.setReputationScore(resp.getReputationScore());
                reputation.setReviewCount(resp.getReviewCount());
                reputation.addReviews(resp.getReviews());
                return reputation;
            }
        });
    }

    @Override
    public boolean hasMoreReviews() {
        return reviewListMeta != null && reviewListMeta.hasNext();
    }


    @Override
    public Observable<Review> postReply(String reviewID, String content){
        RequestParams params = new RequestParams();
        params.put("review_id", reviewID);
        params.put("reply_content", content);

        return post(POST_REVIEW_REPLY_URL, params, Review.class);

    }
}
