package com.thirdrock.repository.impl;

import com.loopj.android.http.RequestParams;
import com.thirdrock.domain.GeoLocation;
import com.thirdrock.framework.rest.HeaderBuilder;
import com.thirdrock.framework.rest.HttpBodyParserFactory;
import com.thirdrock.protocol.AddressResponse;
import com.thirdrock.repository.AbsRepository;
import com.thirdrock.repository.GeoLocationReportRepository;

import rx.Observable;

/**
 * GcmReporitory的实现类
 * Created by jubin on 23/4/15.
 */
public class GeoLocationReportRepositoryImpl extends AbsRepository implements GeoLocationReportRepository {

    public GeoLocationReportRepositoryImpl(String baseUrl, HeaderBuilder headerBuilder, HttpBodyParserFactory bodyParserFactory) {
        super(baseUrl, headerBuilder, bodyParserFactory);
    }

    @Override
    public Observable<Void> updateLocation(GeoLocation location) {

        RequestParams params = new RequestParams();
        if(location != null){
            params.put("lat", location.getLatitude());
            params.put("lon", location.getLongitude());
            params.put("country", location.getCountry());
            params.put("region", location.getRegion());
            params.put("city", location.getCity());
        }

        return post(UPDATE_LOCATION_URL, params, Void.class);
    }

    @Override
    public Observable<AddressResponse> getAddressFromGoogle(final double lat,final double lon){
        String geoUrl = String.format(GEO_CODE_LATLNG,lat,lon);
        return get(geoUrl, AddressResponse.class);

    }
    @Override
    public Observable<AddressResponse> getAddressFromGoogle(String address){
        String geoUrl = String.format(GEO_CODE_ZIPCODE, address);
        return get(geoUrl, AddressResponse.class);
    }

    @Override
    public Observable<Void> sendPostalCodeInfo(GeoLocation location, String zipcode) {
        RequestParams params = new RequestParams();
        if(location != null) {
            params.put("lat", location.getLatitude());
            params.put("lon", location.getLongitude());
            params.put("country", location.getCountry());
            params.put("region", location.getRegion());
            params.put("city", location.getCity());
        }
        params.put("zipcode", zipcode);

        return post(POSTAL_CODE_URL, params, Void.class);
    }
}
