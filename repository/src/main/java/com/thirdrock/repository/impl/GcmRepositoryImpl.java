package com.thirdrock.repository.impl;

import com.loopj.android.http.RequestParams;
import com.thirdrock.framework.rest.HeaderBuilder;
import com.thirdrock.framework.rest.HttpBodyParserFactory;
import com.thirdrock.repository.AbsRepository;
import com.thirdrock.repository.GcmRepository;

import rx.Observable;

/**
 * GcmReporitory的实现类
 * Created by jubin on 23/4/15.
 */
public class GcmRepositoryImpl extends AbsRepository implements GcmRepository{

    public GcmRepositoryImpl(String baseUrl, HeaderBuilder headerBuilder, HttpBodyParserFactory bodyParserFactory) {
        super(baseUrl, headerBuilder, bodyParserFactory);
    }

    @Override
    public Observable<Void> registerGcm(String pGcmSenderId) {
        RequestParams params = new RequestParams();
        params.put("device_id", pGcmSenderId);

        return post(GCM_REGISTER_URL, params, Void.class);
    }

    @Override
    public Observable<Void> unRegisterGcm(String pGcmSenderId) {
        RequestParams params = new RequestParams();
        params.put("device_id", pGcmSenderId);

        return post(GCM_UNREGISTER_URL, params, Void.class);
    }
}
