package com.thirdrock.repository.impl;

import android.content.Context;
import android.text.TextUtils;

import com.cloudinary.Cloudinary;
import com.cloudinary.android.Utils;
import com.loopj.android.http.RequestParams;
import com.thirdrock.domain.Campaign;
import com.thirdrock.domain.EnumCampaignType;
import com.thirdrock.domain.FeaturedCollection;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.framework.exception.NetworkException;
import com.thirdrock.framework.rest.HeaderBuilder;
import com.thirdrock.framework.rest.HttpBodyParserFactory;
import com.thirdrock.protocol.CampaignResp;
import com.thirdrock.protocol.CloudSignature;
import com.thirdrock.protocol.GetLikersResp;
import com.thirdrock.protocol.HomeResp;
import com.thirdrock.protocol.HomeResp__JsonHelper;
import com.thirdrock.protocol.ListItemReq;
import com.thirdrock.protocol.ListItemResp;
import com.thirdrock.protocol.Meta;
import com.thirdrock.protocol.RenewItemResp;
import com.thirdrock.protocol.ShortLinkResp;
import com.thirdrock.repository.AbsHttpBodyParser;
import com.thirdrock.repository.AbsRepository;
import com.thirdrock.repository.ItemRepository;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * Implementation of ItemRepository
 * Created by ywu on 14-9-27.
 */
public class ItemRepositoryImpl extends AbsRepository implements ItemRepository {
    private Meta lastNearbyRespMeta;
    private Meta lastFollowingRespMeta;

    public ItemRepositoryImpl(String baseUrl,
                              HeaderBuilder headerBuilder,
                              HttpBodyParserFactory bodyParserFactory) {
        super(baseUrl, headerBuilder, bodyParserFactory);
    }

    @Override
    public Observable<List<ItemThumb>> getFollowingItemThumbs(float latitude, float longitude) {
        return doGetFollowingItemThumbs(latitude, longitude, false);
    }

    @Override
    public Observable<List<ItemThumb>> getMoreFollowingItemThumbs(float latitude, float longitude) {
        return doGetFollowingItemThumbs(latitude, longitude, true);
    }

    private Observable<List<ItemThumb>> doGetFollowingItemThumbs(float latitude, float longitude, boolean incr) {
        RequestParams params = new RequestParams();
        params.put("lat", latitude);
        params.put("lon", longitude);

        String url = HOME_FOLLOWING_URL;
        if (incr) {
            if (hasMoreFollowingItems()) {
                url += lastFollowingRespMeta.getNext();
            } else throw new IllegalStateException("no more item can retrieve from following sellers");
        } else {
            params.put("limit", ITEM_PAGE_SIZE);
        }

        return get(url, params, HomeResp.class).map(new Func1<HomeResp, List<ItemThumb>>() {
            @Override
            public List<ItemThumb> call(HomeResp resp) {
                lastFollowingRespMeta = resp.getMeta();
                return resp.getItems();
            }
        });
    }

    @Override
    public boolean hasMoreFollowingItems() {
        return lastFollowingRespMeta != null && lastFollowingRespMeta.hasNext();
    }

    @Override
    public Observable<List<ItemThumb>> getNearbyItemThumbs(float latitude, float longitude) {
        return getNearbyItemThumbs(latitude, longitude, false);
    }

    @Override
    public Observable<List<ItemThumb>> getMoreNearbyItemThumbs(float latitude, float longitude) {
        return getNearbyItemThumbs(latitude, longitude, true);
    }

    private Observable<List<ItemThumb>> getNearbyItemThumbs(float latitude, float longitude, boolean incr) {
        RequestParams params = new RequestParams();
        params.put("lat", latitude);
        params.put("lon", longitude);

        String url = HOME_NEARBY_URL;
        if (incr) {
            if (hasMoreNearbyItems()) {
                url += lastNearbyRespMeta.getNext();
            } else throw new IllegalStateException("no more item can retrieve from local sellers");
        } else {
            params.put("limit", ITEM_PAGE_SIZE);
        }

        return get(url, params, HomeResp.class).map(new Func1<HomeResp, List<ItemThumb>>() {
            @Override
            public List<ItemThumb> call(HomeResp resp) {
                lastNearbyRespMeta = resp.getMeta();
                return resp.getItems();
            }
        });
    }

    @Override
    public boolean hasMoreNearbyItems() {
        return lastNearbyRespMeta != null && lastNearbyRespMeta.hasNext();
    }

    @Override
    public Observable<String> listItem(ListItemReq req) {
        RequestParams params = new RequestParams();
        params.put("title", req.title);
        params.put("desc", req.description);
        params.put("currency", req.currency);
        params.put("price", req.price);
        params.put("original_price", req.originPrice);
        params.put("shipping_method", req.deliveryType.ordinal());
        params.put("category", req.categoryId);
        params.put("brand", req.brand);
        params.put("lat", req.latitude);
        params.put("lon", req.longitude);
        params.put("city", req.city);
        params.put("state", req.region);
        params.put("country", req.country);
        params.put("images", req.getImages());
        params.put("mediaLink", req.mediaLink);
        params.put("share", req.autoShare ? 1 : 0);

        return post(LIST_ITEM_URL, params, ListItemResp.class)
                .map(new Func1<ListItemResp, String>() {
                    @Override
                    public String call(ListItemResp resp) {
                        return resp.getId();
                    }
                });
    }

    @Override
    public Observable<Void> editItem(ListItemReq req) {
        RequestParams params = new RequestParams();
        params.put("item_id", req.itemId);
        params.put("title", req.title);
        params.put("desc", req.description);
        params.put("currency", req.currency);
        params.put("price", req.price);
        params.put("original_price", req.originPrice);
        params.put("shipping_method", req.deliveryType.ordinal());
        params.put("category", req.categoryId);
        params.put("brand", req.brand);
        params.put("images", req.getImages());
        params.put("mediaLink", req.mediaLink);

        return post(EDIT_ITEM_URL, params, Void.class);
    }

    @Override
    public Observable<Void> deleteItem(String id) {
        RequestParams params = new RequestParams();
        params.put("item_id", id);
        return post(DEL_ITEM_URL, params, Void.class);
    }

    @Override
    public Observable<CloudSignature> signCloudUpload() {
        return post(SIGN_UPLOAD, null, CloudSignature.class);
    }

    @Override
    public Observable<Map> uploadToCloud(final Context context,
                                                final String file,
                                                final CloudSignature signature) {
        return Observable.create(new Observable.OnSubscribe<Map>() {
            @Override
            public void call(Subscriber<? super Map> subscriber) {
                try {
                    if (subscriber.isUnsubscribed()) {
                        return;
                    }

                    Cloudinary cloudinary = new Cloudinary(Utils.cloudinaryUrlFromContext(context));
                    Map result = cloudinary.uploader().upload(file,
                            Cloudinary.asMap(
                                    "signature", signature.getSignature(),
                                    "api_key", signature.getApiKey(),
                                    "timestamp", signature.getTimestamp(),
                                    "resource_type", "auto"
                            ));
                    subscriber.onNext(result);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(new NetworkException(-1, e));
                }
            }
        }).retry(DEFAULT_RETRIES);
    }

    @Override
    public Observable<String> shortenItemLink(String itemId) {
        RequestParams params = new RequestParams();
        params.put("item_id", itemId);

        return get(SHORTEN_URL, params, ShortLinkResp.class)
                .map(new Func1<ShortLinkResp, String>() {
                    @Override
                    public String call(ShortLinkResp resp) {
                        return resp.getShortUrl();
                    }
                });
    }

    @Override
    public Observable<Item> getItemById(String itemId) {
        RequestParams params = new RequestParams();
        params.put("item_id", itemId);

        return get(ITEM_DETAIL_URL, params, Item.class);
    }

    @Override
    public Observable<Item> getItemById(String itemId,HashMap<String,String> inParam) {
        RequestParams params = new RequestParams();
        params.put("item_id", itemId);
        Iterator iter = inParam.entrySet().iterator();
        while (iter.hasNext()){
            Map.Entry<String,String> entry = (Map.Entry<String,String>) iter.next();
            params.put(entry.getKey(),entry.getValue());
        }

        return get(ITEM_DETAIL_URL, params, Item.class);
    }


    @Override
    public Observable<Void> like(String itemId, boolean isLike) {
        RequestParams params = new RequestParams();
        params.put("item_id", itemId);

        return post(isLike ? LIKE_URL : UNLIKE_URL, params, Void.class);
    }

    @Override
    public Observable<GetLikersResp> getLikers(String itemId){
        RequestParams params = new RequestParams();
        params.put("item_id", itemId);
        params.put("limit", LIKER_PAGE_SIZE);
        return get(LIKERS_URL, params, GetLikersResp.class);
    }

    @Override
    public Observable<List<ItemThumb>> suggestByItem(String itemId, float latitude, float longitude) {
        RequestParams params = new RequestParams();
        params.put("item_id", itemId);
        params.put("lat", latitude);
        params.put("lon", longitude);
        params.put("limit", MAX_SUGGEST_ITEMS);

        return get(SUGGEST_URL, params, new AbsHttpBodyParser<List<ItemThumb>>() {
            @Override
            public List<ItemThumb> parse(String body) throws Exception {
                if (TextUtils.isEmpty(body)) {
                    return Collections.emptyList();
                }

                body = "{ \"objects\":" + body + "}";  // ig-json-parser不能处理jsonArray应答
                HomeResp resp = HomeResp__JsonHelper.parseFromJson(body);
                return resp.getItems();
            }
        });
    }

    @Override
    public Observable<List<ItemThumb>> getUserItems(String userId) {
        RequestParams params = new RequestParams();
        params.put("seller_id", userId);
        params.put("limit", MAX_SELLER_ITEMS);
        return get(USER_ITEMS_URL, params, HomeResp.class).map(new Func1<HomeResp, List<ItemThumb>>() {
            @Override
            public List<ItemThumb> call(HomeResp resp) {
                return resp.getItems();
            }
        });
    }

    @Override
    public Observable<List<Campaign>> getCampaigns(EnumCampaignType type) {
        RequestParams params = new RequestParams();
        params.put("type", type.ordinal());

        return get(CAMPAIGNS_URL, params, CampaignResp.class).map(new Func1<CampaignResp, List<Campaign>>() {
            @Override
            public List<Campaign> call(CampaignResp resp) {
                return resp.getCampaigns();
            }
        });
    }

    @Override
    public Observable<FeaturedCollection> getFeaturedCollection(String id) {
        RequestParams params = new RequestParams();
        params.put("featured_items_id", id);

        return get(FEATURED_COLLECTION_URL, params, FeaturedCollection.class);
    }

    @Override
    public Observable<Void> sendPasscode(String countryNo, String phoneNo) {
        RequestParams params = new RequestParams();
        params.put("phone", countryNo + phoneNo);

        return post(SEND_PASSCODE_URL, params, Void.class);
    }

    @Override
    public Observable<Void> verifyPasscode(String countryNo, String phoneNo, String passcode) {
        RequestParams params = new RequestParams();
        params.put("phone", countryNo + phoneNo);
        params.put("pass_code", passcode);

        return post(VERIFY_PASSCODE_URL, params, Void.class);
    }

    @Override
    public Observable<RenewItemResp> renewItem(String fuzzId) {
        RequestParams params = new RequestParams();
        params.put("item_id", fuzzId);

        return post(RENEW_ITEM_URL, params, RenewItemResp.class);
    }

    @Override
    public Observable<Void> markSold(String fuzzId){
        RequestParams params = new RequestParams();
        params.put("item_id", fuzzId);

        return post(MARK_SOLD_URL, params, Void.class);
    }

    @Override
    public Observable<Void> relistItem(String fuzzId){
        RequestParams params = new RequestParams();
        params.put("item_id", fuzzId);

        return post(RELIST_ITEM_URL, params, Void.class);
    }

    @Override
    public Observable<Void> unlistItem(String fuzzId){
        RequestParams params = new RequestParams();
        params.put("item_id", fuzzId);

        return post(UNLIST_ITEM_URL, params, Void.class);
    }
}
