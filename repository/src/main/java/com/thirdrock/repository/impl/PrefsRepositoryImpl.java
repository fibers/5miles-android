package com.thirdrock.repository.impl;

import android.text.TextUtils;

import com.loopj.android.http.RequestParams;
import com.thirdrock.domain.Subscription;
import com.thirdrock.framework.rest.HeaderBuilder;
import com.thirdrock.framework.rest.HttpBodyParserFactory;
import com.thirdrock.protocol.GetSubscriptionsResp;
import com.thirdrock.protocol.GetSubscriptionsResp__JsonHelper;
import com.thirdrock.repository.AbsHttpBodyParser;
import com.thirdrock.repository.AbsRepository;
import com.thirdrock.repository.PrefsRepository;

import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by ywu on 14/11/3.
 */
@Singleton
public class PrefsRepositoryImpl extends AbsRepository implements PrefsRepository {

    public PrefsRepositoryImpl(String baseUrl,
                                  HeaderBuilder headerBuilder,
                                  HttpBodyParserFactory bodyParserFactory) {
        super(baseUrl, headerBuilder, bodyParserFactory);
    }

    @Override
    public Observable<Subscription> subscribe(final Subscription subscription) {
        RequestParams params = new RequestParams();
        params.put("type", subscription.getTopicId());
        params.put("method", subscription.getPushMethodId());
        params.put("action", subscription.isEnabled() ? 1 : 0);

        return post(SUBS_URL, params, Void.class).map(new Func1<Void, Subscription>() {
            @Override
            public Subscription call(Void aVoid) {
                return subscription;
            }
        });
    }

    @Override
    public Observable<List<Subscription>> getSubscriptions() {
        return get(GET_SUBS_URL, new AbsHttpBodyParser<List<Subscription>>() {
            @Override
            public List<Subscription> parse(String body) throws Exception {
                if (TextUtils.isEmpty(body)) {
                    return Collections.emptyList();
                }

                body = "{ \"data\":" + body + "}";  // ig-json-parser不能处理jsonArray应答
                GetSubscriptionsResp resp = GetSubscriptionsResp__JsonHelper.parseFromJson(body);
                return resp.getSubscriptions();
            }
        });
    }
}
