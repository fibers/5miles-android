package com.thirdrock.repository.impl;

import android.text.TextUtils;

import com.loopj.android.http.RequestParams;
import com.thirdrock.domain.SellerInfo;
import com.thirdrock.framework.rest.HeaderBuilder;
import com.thirdrock.framework.rest.HttpBodyParserFactory;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.SellersNearbyResp;
import com.thirdrock.protocol.SellersNearbyResp__JsonHelper;
import com.thirdrock.repository.AbsHttpBodyParser;
import com.thirdrock.repository.AbsRepository;
import com.thirdrock.repository.UserRepository;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;

import rx.Observable;

/**
 * 和User相关的读写请求接口的实现
 * Created by ywu on 15/4/21.
 */
public class UserRepositoryImpl extends AbsRepository implements UserRepository {

    public UserRepositoryImpl(String baseUrl, HeaderBuilder headerBuilder, HttpBodyParserFactory bodyParserFactory) {
        super(baseUrl, headerBuilder, bodyParserFactory);
    }

    @Override
    public Observable<Void> blockUser(String uid, boolean blocking) {
        RequestParams params = new RequestParams();
        params.put("target_user", uid);
        return post(blocking ? BLOCK_USER_URL : UNBLOCK_USER_URL, params, Void.class);
    }

    public Observable<List<SellerInfo>> getNearbySellersList(float latitude, float longitude) {
        RequestParams params = new RequestParams();
        params.put("lat", latitude);
        params.put("lon", longitude);

        return get(SELLERS_NEARBY_URL, params, new AbsHttpBodyParser<List<SellerInfo>>() {
            @Override
            public List<SellerInfo> parse(String body) throws Exception {
                if (TextUtils.isEmpty(body)) {
                    return Collections.emptyList();
                }

                body = "{ \"objects\":" + body + "}";  // ig-json-parser不能处理jsonArray应答
                SellersNearbyResp resp = SellersNearbyResp__JsonHelper.parseFromJson(body);
                return resp.getSellerInfoList();
            }
        });
    }

    @Override
    public Observable<Void> batchfollow(Iterable<String> userIdList) {
        if (userIdList == null) {
            L.w("user id list should not be null");
            return null;
        }

        String batchIdParam = StringUtils.join(userIdList, ',');
        RequestParams params = new RequestParams();
        params.put("user_ids", batchIdParam);

        return post(FOLLOW_URL, params, Void.class);
    }
}
