package com.thirdrock.repository.impl;

import com.thirdrock.framework.rest.HttpBodyParser;
import com.thirdrock.framework.rest.HttpBodyParserFactory;
import com.thirdrock.protocol.BodyParser;
import com.thirdrock.repository.AbsHttpBodyParser;

/**
 * Created by ywu on 14-10-12.
 */
public final class BodyParserFactoryImpl implements HttpBodyParserFactory {

    private static class BodyParserImpl<T> extends AbsHttpBodyParser<T> {

        Class<T> type;

        BodyParserImpl(Class<T> type) {
            this.type = type;
        }

        @Override
        public T parse(String body) throws Exception {
            return BodyParser.parse(body, type);
        }
    }

    private static BodyParserFactoryImpl instance;

    public static BodyParserFactoryImpl getInstance() {
        if (instance == null) {
            instance = new BodyParserFactoryImpl();
        }

        return instance;
    }

    private BodyParserFactoryImpl() {

    }

    @Override
    public <T> HttpBodyParser<T> createParser(Class<T> type) {
        return new BodyParserImpl<>(type);
    }
}
