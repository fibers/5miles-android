package com.thirdrock.repository.impl;

import com.loopj.android.http.RequestParams;
import com.thirdrock.framework.rest.HeaderBuilder;
import com.thirdrock.framework.rest.HttpBodyParserFactory;
import com.thirdrock.protocol.MakeOfferResp;
import com.thirdrock.protocol.offer.OfferLineDetailResp;
import com.thirdrock.repository.AbsRepository;
import com.thirdrock.repository.OfferRepository;

import rx.Observable;

import static android.text.TextUtils.isEmpty;
import static com.thirdrock.protocol.offer.ChatMessage.MSG_TYPE_IMAGE;
import static com.thirdrock.protocol.offer.ChatMessage.MSG_TYPE_LOCATION;
import static com.thirdrock.protocol.offer.ChatMessage.MSG_TYPE_TEXT;

/**
 * Offer & Chat related
 * Created by ywu on 15/2/3.
 */
public class OfferRepositoryImpl extends AbsRepository implements OfferRepository {
    public static final String PARA_MESSAGE_TYPE = "msg_type";
    public static final String PARA_OFFERLINE_ID = "offerline_id";
    public static final String PARA_TIMESTAMP = "from_timestamp";
    public static final String PARA_ITEM_ID = "item_id";
    public static final String PARA_PRICE = "price";
    public static final String PARA_TARGET_USER_ID = "to_user";
    public static final String PARA_TEXT = "text";
    public static final String PARA_IMG_URL = "picture_url";
    public static final String PARA_PLACE_NAME = "place_name";
    public static final String PARA_ADDRESS_NAME = "address_name";
    public static final String PARA_LATITUDE = "lat";
    public static final String PARA_LONGITUDE = "lon";
    public static final String PARA_MAP_THUMB_URL = "address_map_thumb";


    public OfferRepositoryImpl(String baseUrl, HeaderBuilder headerBuilder, HttpBodyParserFactory bodyParserFactory) {
        super(baseUrl, headerBuilder, bodyParserFactory);
    }

    @Override
    public Observable<MakeOfferResp> sendTextMessage(String toUid, String itemId, String price, String message) {
        RequestParams params = new RequestParams();
        params.put(PARA_MESSAGE_TYPE, MSG_TYPE_TEXT);
        params.put(PARA_TARGET_USER_ID, toUid);
        params.put(PARA_ITEM_ID, itemId);
        params.put(PARA_PRICE, price);
        if (!isEmpty(message)) {
            params.put(PARA_TEXT, message.trim());
        }

        return post(MAKE_OFFER_URL, params, MakeOfferResp.class);
    }

    @Override
    public Observable<MakeOfferResp> sendImageMessage(String pToUid, String pItemId, String pImageUrl) {
        RequestParams params = new RequestParams();
        params.put(PARA_MESSAGE_TYPE, MSG_TYPE_IMAGE);
        params.put(PARA_TARGET_USER_ID, pToUid);
        params.put(PARA_ITEM_ID, pItemId);
        params.put(PARA_IMG_URL, pImageUrl);

        return post(MAKE_OFFER_URL, params, MakeOfferResp.class);
    }

    @Override
    public Observable<MakeOfferResp> sendLocationMessage(String pToUid, String pItemId
            , CharSequence pPlaceName, CharSequence pAddress
            , double pLat, double pLon, String pMapThumbUrl) {
        RequestParams params = new RequestParams();
        params.put(PARA_MESSAGE_TYPE, MSG_TYPE_LOCATION);
        params.put(PARA_TARGET_USER_ID, pToUid);
        params.put(PARA_ITEM_ID, pItemId);
        params.put(PARA_PLACE_NAME, pPlaceName);
        params.put(PARA_ADDRESS_NAME, pAddress);
        params.put(PARA_LATITUDE, pLat);
        params.put(PARA_LONGITUDE, pLon);
        params.put(PARA_MAP_THUMB_URL, pMapThumbUrl);

        return post(MAKE_OFFER_URL, params, MakeOfferResp.class);
    }

    @Override
    public Observable<OfferLineDetailResp> getOfferlineDetails(int offerLineId) {
        RequestParams params = new RequestParams();
        params.put(PARA_OFFERLINE_ID, offerLineId);

        return get(OFFER_LINE_URL, params, OfferLineDetailResp.class);
    }

    @Override
    public Observable<OfferLineDetailResp> syncOfferlieUpdates(int offerLineId, long lastMsgTimestamp) {
        RequestParams params = new RequestParams();
        params.put(PARA_OFFERLINE_ID, offerLineId);
        params.put(PARA_TIMESTAMP, lastMsgTimestamp);

        return get(OFFER_LINE_UPDATE_URL, params, OfferLineDetailResp.class);
    }
}
