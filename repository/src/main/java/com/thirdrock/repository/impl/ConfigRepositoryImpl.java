package com.thirdrock.repository.impl;

import com.thirdrock.framework.rest.HeaderBuilder;
import com.thirdrock.framework.rest.HttpBodyParserFactory;
import com.thirdrock.repository.AbsRepository;
import com.thirdrock.repository.ConfigRepository;

import rx.Observable;

/**
 * Implementation of ConfigRepository
 * Created by ywu on 15/4/29.
 */
public class ConfigRepositoryImpl extends AbsRepository implements ConfigRepository {

    public ConfigRepositoryImpl(String baseUrl,
                                HeaderBuilder headerBuilder,
                                HttpBodyParserFactory bodyParserFactory) {
        super(baseUrl, headerBuilder, bodyParserFactory);
    }

    @Override
    public Observable<String> getAppConfig() {
        return get(INIT_URL, String.class);
    }

}
