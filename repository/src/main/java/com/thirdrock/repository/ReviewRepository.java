package com.thirdrock.repository;

import com.thirdrock.domain.Reputation;
import com.thirdrock.domain.Review;

import rx.Observable;

/**
 * Review repository.
 * Created by ywu on 15/4/17.
 */
public interface ReviewRepository {
    String POST_REVIEW_URL = "/post_review/";
    String REVIEWS_URL = "/reviews/";
    String POST_REVIEW_REPLY_URL = "/post_review_reply/";

    int REVIEW_PAGE_SIZE = 30;

    /**
     * Post a review
     * @param targetUid receiver's id
     * @param itemId item id
     * @param rating score, 1~5
     * @param comment review comment
     * @param isSeller whether the review is sent by the item seller
     */
    Observable<Void> postReview(String targetUid, String itemId, int rating, String comment, boolean isSeller);

    /**
     * Get reputation detail of the user
     *
     * @param uid user id
     * @return promised Reputation object
     */
    Observable<Reputation> getUserReputation(String uid);

    /**
     * Whether there's more reviews in the user's review list.
     */
    boolean hasMoreReviews();


    /**
     * Post reply for a review
     * @param reviewID The review id.
     * @param content The reply content.
     * @return
     */
    Observable<Review> postReply(String reviewID, String content);
}
