package com.thirdrock.repository;

import com.thirdrock.domain.Subscription;

import java.util.List;

import rx.Observable;

/**
 * User Preferences repository
 * Created by ywu on 14/11/3.
 */
public interface PrefsRepository {

    String GET_SUBS_URL = "/user_subscriptions/";
    String SUBS_URL = "/subscribe/";

//    /**
//     * Save user's notification preferences to server.
//     * @param subscriptions subscriptions to submit
//     * @return promise
//     */
//    Observable<Void> subscribe(List<Subscription> subscriptions);

    /**
     * Submit a subscription/un-subscription.
     * @param subscription subscription
     * @return promised result
     */
    Observable<Subscription> subscribe(Subscription subscription);

    /**
     * Get user's notifications preferences from server.
     * @return promised preferences
     */
    Observable<List<Subscription>> getSubscriptions();
}
