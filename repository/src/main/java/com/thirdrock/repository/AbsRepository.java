package com.thirdrock.repository;

import com.thirdrock.framework.rest.AbsRestClient;
import com.thirdrock.framework.rest.HeaderBuilder;
import com.thirdrock.framework.rest.HttpBodyParserFactory;

/**
 * Created by ywu on 14-10-7.
 */
public abstract class AbsRepository extends AbsRestClient {
    
    protected AbsRepository(String baseUrl,
                            HeaderBuilder headerBuilder,
                            HttpBodyParserFactory bodyParserFactory) {
        setBaseUrl(baseUrl);
        setHeaderBuilder(headerBuilder);
        setBodyParserFactory(bodyParserFactory);
    }
}
