package com.thirdrock.repository;

import com.thirdrock.domain.GeoLocation;
import com.thirdrock.protocol.AddressResponse;

import rx.Observable;

/**
 * Interface for report reasons related io request
 * Created by jubin on 7/5/15.
 */
public interface GeoLocationReportRepository {
    String UPDATE_LOCATION_URL = "/update_location/";
    String POSTAL_CODE_URL = "/fill_profile/";
    String GEO_CODE_LATLNG = "http://maps.googleapis.com/maps/api/geocode/json?latlng=%1$s,%2$s&sensor=false&language=en";
    // scale可以选1或2，目前我们都用2，不适配density为1的机器
    String GOOGLE_STATIC_MAP_URL = "https://maps.googleapis.com/maps/api/staticmap?center=%1$s,%2$s&zoom=%3$s&size=%4$sx%5$s&markers=%1$s,%2$s&scale=2";
    String GEO_CODE_ZIPCODE = "http://maps.googleapis.com/maps/api/geocode/json?address=%1$s&sensor=false&language=en";

    Observable<Void> updateLocation(GeoLocation location);

    Observable<AddressResponse> getAddressFromGoogle(double lat, double lon);
    
    Observable<AddressResponse> getAddressFromGoogle(String postalCode);

    Observable<Void> sendPostalCodeInfo(GeoLocation location, String zipcode);
}
