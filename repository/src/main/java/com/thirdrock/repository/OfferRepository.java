package com.thirdrock.repository;

import android.support.annotation.Nullable;

import com.thirdrock.protocol.MakeOfferResp;
import com.thirdrock.protocol.offer.OfferLineDetailResp;

import rx.Observable;

/**
 * Offers repository
 * Created by ywu on 15/2/3.
 */
public interface OfferRepository {

    String MAKE_OFFER_URL = "/make_offer/";
    String OFFER_LINE_URL = "/offerline/";
    String OFFER_LINE_UPDATE_URL = "/offerline_updates/";

    /**
     * Make an offer
     * @param toUid the user id who should receive this offer
     * @param itemId id of the target item
     * @param price offer price
     * @param text additional message
     * @return promised offerLine id
     */
    Observable<MakeOfferResp> sendTextMessage(String toUid, String itemId, @Nullable String price, String text);

    Observable<MakeOfferResp> sendImageMessage(String pToUid, String pItemId, String pImageUrl);

    Observable<MakeOfferResp> sendLocationMessage(String pToUid, String pItemId, CharSequence pPlaceName
            , CharSequence pAddress, double pLat, double pLon, String pMapThumbUrl);

    Observable<OfferLineDetailResp> getOfferlineDetails(int offerLineId);

    Observable<OfferLineDetailResp> syncOfferlieUpdates(int offerLineId, long lastMsgTimestamp);
}
