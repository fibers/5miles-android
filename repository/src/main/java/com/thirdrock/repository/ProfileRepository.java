package com.thirdrock.repository;

import com.thirdrock.domain.MyItems;

import rx.Observable;

/**
 * Created by fibers on 15/7/6.
 */
public interface ProfileRepository {

    String MY_ITEMS_URL = "/my_sellings/";

    /**
     * Get my selling items
     *
     * @return
     */
    Observable<MyItems> getMyItems();

    /**
     * If there are more items in my selling items page.
     *
     * @return True for yes, false otherwise.
     */
    boolean hasMoreMyItems();
}
