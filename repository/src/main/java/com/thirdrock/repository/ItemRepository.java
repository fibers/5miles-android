package com.thirdrock.repository;

import android.content.Context;

import com.thirdrock.domain.Campaign;
import com.thirdrock.domain.EnumCampaignType;
import com.thirdrock.domain.FeaturedCollection;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.protocol.CloudSignature;
import com.thirdrock.protocol.GetLikersResp;
import com.thirdrock.protocol.ListItemReq;
import com.thirdrock.protocol.RenewItemResp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;

/**
 * Created by ywu on 14-9-25.
 */
public interface ItemRepository {

    String HOME_NEARBY_URL = "/home/";
    String HOME_FOLLOWING_URL = "/followings_items/";
    String LIST_ITEM_URL = "/post_item/";
    String EDIT_ITEM_URL = "/edit_item/";
    String DEL_ITEM_URL = "/delete_item/";
    String ITEM_DETAIL_URL = "/item_detail/";
    String SIGN_UPLOAD = "/sign_image_upload/";
    String SHORTEN_URL = "/item_short_url/";
    String LIKE_URL = "/like/";
    String UNLIKE_URL = "/unlike/";
    String LIKERS_URL = "/item_likers/";
    String SUGGEST_URL = "/suggest/";
    String USER_ITEMS_URL = "/user_sellings/";  // TODO move to user repo
    String CAMPAIGNS_URL = "/campaigns/";  // TODO move to campaign repo
    String FEATURED_COLLECTION_URL = "/featured_items/";
    String SEND_PASSCODE_URL = "/send_passcode/";  // TODO move to user repo
    String VERIFY_PASSCODE_URL = "/verify_passcode/";  // TODO move to user repo
    String RENEW_ITEM_URL = "/renew_item/";
    String MARK_SOLD_URL = "/mark_sold/";
    String RELIST_ITEM_URL = "/relist_item/";
    String UNLIST_ITEM_URL = "/unlist_item/";
    int ITEM_PAGE_SIZE = 50;
    int LIKER_PAGE_SIZE = 30;
    int MAX_SUGGEST_ITEMS = 10;
    int MAX_SELLER_ITEMS = 11;  // 确保除去当前商品后仍有10个商品

    /**
     * Get a list of item thumbs from following sellers, geo location is used for ranking
     * @param latitude current latitude
     * @param longitude current longitude
     * @return A promised list of item thumbs
     */
    Observable<List<ItemThumb>> getFollowingItemThumbs(float latitude, float longitude);

    /**
     * Get next page of item thumbs from following sellers, geo location is used for ranking
     * @param latitude current latitude
     * @param longitude current longitude
     * @return A promised list of item thumbs
     */
    Observable<List<ItemThumb>> getMoreFollowingItemThumbs(float latitude, float longitude);

    /**
     * Check if there's more items can retrieve.
     * @return has more item or not
     */
    boolean hasMoreFollowingItems();

    /**
     * Get a list of item thumbs, based on current geo location.
     * @param latitude current latitude
     * @param longitude current longitude
     * @return A promised list of item thumbs
     */
    Observable<List<ItemThumb>> getNearbyItemThumbs(float latitude, float longitude);

    /**
     * Get next page of item thumbs, based on current geo location.
     * @param latitude current latitude
     * @param longitude current longitude
     * @return A promised list of item thumbs
     */
    Observable<List<ItemThumb>> getMoreNearbyItemThumbs(float latitude, float longitude);

    /**
     * Check if there's more items can retrieve.
     * @return has more item or not
     */
    boolean hasMoreNearbyItems();

    /**
     * Lists a new item.
     * @param req request data
     * @return promised new item id
     */
    Observable<String> listItem(ListItemReq req);

    /**
     * Edit (re-list) a existed item.
     * @param req request data
     */
    Observable<Void> editItem(ListItemReq req);

    /**
     * Delete an item.
     * @param id item id
     */
    Observable<Void> deleteItem(String id);

    /**
     * Get a up-to-date (cloudinary) signature (before upload resource to the cloud)
     * @return promised signature
     */
    Observable<CloudSignature> signCloudUpload();

    /**
     * Upload a file to cloud (cloudinary).
     * @param context Context
     * @param file file to be upload
     * @param signature signature
     * @return promised return value (in Map)
     */
    Observable<Map> uploadToCloud(Context context, String file, CloudSignature signature);

    /**
     * Shorten given item's link
     * @param itemId item id
     * @return promised short url
     */
    Observable<String> shortenItemLink(String itemId);

    /**
     * Get item details by id.
     * @param itemId
     * @return promised item
     */
    Observable<Item> getItemById(String itemId);

    /**
     * Get item details by id and other params.
     * @param itemId
     * @param inParam other params
     * @return promised item
     */
    Observable<Item> getItemById(String itemId, HashMap<String,String> inParam);

    /**
     * Add or remove an item to/from likes.
     * @param itemId item id
     * @param isLike like or not
     * @return promise
     */
    Observable<Void> like(String itemId, boolean isLike);

    /**
     * GetLikekrsResp includes some of users who liked the given item and number of all liked users
     * @param itemId item id
     * @return GetLikersResp obj
     */
    Observable<GetLikersResp> getLikers(String itemId);

    /**
     * Suggest similar items
     * @param itemId item id
     * @param latitude current latitude
     * @param longitude current longitude
     * @return promised suggestions
     */
    Observable<List<ItemThumb>> suggestByItem(String itemId, float latitude, float longitude);

    /**
     * Get items of the given seller.
     * @param userId user id
     * @return promised items
     */
    Observable<List<ItemThumb>> getUserItems(String userId);

    /**
     * Get active promotion campaign list
     * @param type campaign type
     * @return promised campaigns
     */
    Observable<List<Campaign>> getCampaigns(EnumCampaignType type);

    /**
     * Get featured item collection
     * @param id collection id
     * @return promised collection
     */
    Observable<FeaturedCollection> getFeaturedCollection(String id);

    /**
     * Send passcode to given phone
     * @param countryNo international country code
     * @param phoneNo phone number
     * @return promise
     */
    Observable<Void> sendPasscode(String countryNo, String phoneNo);

    /**
     * Verify the passcode input by user
     * @param countryNo international country code
     * @param phoneNo phone number
     * @param passcode passcode
     * @return promise
     */
    Observable<Void> verifyPasscode(String countryNo, String phoneNo, String passcode);

    /**
     * Renew item of the given id
     * @param fuzzId item fuzz id
     * @return promise
     */
    Observable<RenewItemResp> renewItem(String fuzzId);

    /**
     * Mark item of the given id as sold
     * @param fuzzId item fuzz id
     * @return promise
     */
    Observable<Void> markSold(String fuzzId);

    /**
     * relist item of the given id
     * @param fuzzId item fuzz id
     * @return promise
     */
    Observable<Void> relistItem(String fuzzId);

    /**
     * unlist item of the given id
     * @param fuzzId item fuzz id
     * @return promise
     */
    Observable<Void> unlistItem(String fuzzId);
}
