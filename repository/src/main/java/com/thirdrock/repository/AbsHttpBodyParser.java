package com.thirdrock.repository;

import com.thirdrock.framework.exception.NetworkException;
import com.thirdrock.framework.exception.RestException;
import com.thirdrock.framework.rest.HttpBodyParser;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.ErrorMessage;
import com.thirdrock.protocol.ErrorMessage__JsonHelper;

import org.apache.http.Header;
import org.apache.http.client.HttpResponseException;

import java.io.IOException;

import static android.text.TextUtils.isEmpty;

/**
 * Created by ywu on 15/1/21.
 */
public abstract class AbsHttpBodyParser<T> implements HttpBodyParser<T> {
    public static final String DEFAULT_ERR_MSG = "Sorry, 5miles is experiencing a technical issue. Please try again later.";

    @Override
    public NetworkException parseError(int statusCode, Header[] headers, String body, Throwable e) {
        int errorCode = 0;
        String errorMessage;

        if (body == null || isEmpty(body.trim())) {
            errorMessage = DEFAULT_ERR_MSG;
        } else {
            // 尝试从body解析错误消息
            body = body.trim();
            ErrorMessage err = body.startsWith("{") ? parseErrorMessageFromJson(body) : null;

            if (err != null) {
                errorCode = err.getErrorCode();
                errorMessage = err.getMessage();
            } else {
                errorMessage = body;
            }
        }

        if (errorCode == 0) {
            errorCode = statusCode;
        }

        return e instanceof HttpResponseException ? new RestException(statusCode, errorCode, errorMessage, e) :
                new NetworkException(statusCode, errorMessage, e);
    }

    private ErrorMessage parseErrorMessageFromJson(String body) {
        try {
            return ErrorMessage__JsonHelper.parseFromJson(body);
        } catch (IOException e) {
            L.e("parse error message failed: %s", body, e);
        }

        return null;
    }
}
