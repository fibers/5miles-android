package com.thirdrock.repository;

import rx.Observable;

/**
 * FIXME: 原本放在PrefsRepository中，但base url不同，就新建一个
 * Created by jubin on 23/4/15.
 */
public interface GcmRepository {
    String GCM_BASE_URL = "http://chat.5milesapp.com";
    String GCM_REGISTER_URL = "/register/";
    String GCM_UNREGISTER_URL = "unregister";

    /**
     * send gcm registration id to server
     * @return if gcm id successfully sent to server
     */
    Observable<Void> registerGcm(String pGcmSenderId);
    Observable<Void> unRegisterGcm(String pGcmSenderId);
}
