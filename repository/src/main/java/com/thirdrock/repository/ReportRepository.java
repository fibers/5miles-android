package com.thirdrock.repository;

import com.thirdrock.protocol.ReportReasonsWrapper;

import java.util.List;

import rx.Observable;

/**
 * Interface for report reasons related io request
 * Created by jubin on 7/5/15.
 */
public interface ReportRepository {
    String REPORT_REASONS_URL = "/report_reasons/";
    // 与/report_reasons/中的类型数值一致
    int REPORT_TYPE_ITEM = 0;
    int REPORT_TYPE_USER = 1;
    int REPORT_TYPE_REVIEW = 2;
    int REPORT_TYPE_ALL = -1;   // custom defined, not a param value

    Observable<List<ReportReasonsWrapper>> getRemoteReportReasons();
    Observable<List<ReportReasonsWrapper>> getRemoteReportReasonsItem();
    Observable<List<ReportReasonsWrapper>> getRemoteReportReasonsUser();
    Observable<List<ReportReasonsWrapper>> getRemoteReportReasonsReview();
}
