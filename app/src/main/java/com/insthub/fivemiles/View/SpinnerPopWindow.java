package com.insthub.fivemiles.View;

import java.util.List;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.thirdrock.fivemiles.R;
import com.insthub.fivemiles.Adapter.CurrencyAdapter;
import com.insthub.fivemiles.Adapter.CurrencyAdapter.IOnItemSelectListener;

public class SpinnerPopWindow extends PopupWindow implements OnItemClickListener {

	private Context mContext;
	private ListView mListView;
	private List list;
	private CurrencyAdapter mAdapter;
	private IOnItemSelectListener mItemSelectListener;

	public SpinnerPopWindow(Context context, List list) {
		super(context);
		mContext = context;
		this.list = list;
		init();
	}

	public void setItemListener(IOnItemSelectListener listener) {
		mItemSelectListener = listener;
	}

	private void init() {
		View view = LayoutInflater.from(mContext).inflate(R.layout.spinner_window_layout, null);
		setContentView(view);
		setWidth(LayoutParams.WRAP_CONTENT);
		setHeight(LayoutParams.WRAP_CONTENT);

		setFocusable(true);
		ColorDrawable dw = new ColorDrawable(0x00);
		setBackgroundDrawable(dw);

		mListView = (ListView) view.findViewById(R.id.spinner_list);

		mAdapter = new CurrencyAdapter(mContext, list);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);
	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int pos, long arg3) {
		dismiss();
		if (mItemSelectListener != null) {
			mItemSelectListener.onItemClick(pos);
		}
	}

}
