package com.insthub.fivemiles.View;

import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.external.eventbus.EventBus;
import com.external.maxwin.view.IXListViewListener;
import com.external.maxwin.view.XListView;
import com.insthub.fivemiles.Activity.TabMessageActivity;
import com.insthub.fivemiles.Adapter.MessageAdapter;
import com.insthub.fivemiles.Adapter.MessageSystemAdapter;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.AppMessageModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.MessageObject;
import com.insthub.fivemiles.Protocol.SystemMessage;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.SysMsgActionUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.SystemAction;

import org.json.JSONException;
import org.json.JSONObject;

import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_MESSAGE;

public class MessageView extends LinearLayout implements IXListViewListener, BusinessResponse {

	private Context mContext;
	private XListView listView;

	private MessageAdapter messageBuyingAdapter;
	private MessageAdapter messageSellingAdapter;
	private MessageSystemAdapter messageSystemAdapter;
	private AppMessageModel messageModel;
	
	private String flag;
	private int position = 0;
	private int thread_id = 0;

    private SysMsgActionUtils actionUtils;
	
	public MessageView(Context context) {
		super(context);
		mContext = context;
	}

	public MessageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}

	public MessageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
	}

    public ListView getListView() {
        return listView;
    }

    @Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		init();
        actionUtils = new SysMsgActionUtils(mContext);
	}
	
	public void registerEvent() {
		if (!EventBus.getDefault().isregister(this)) {
			EventBus.getDefault().register(this);
		}
	}
	
	public void unRegisterEvent() {
		if (EventBus.getDefault().isregister(this)) {
			EventBus.getDefault().unregister(this);
		}
	}
	
	public void init() {
		listView = (XListView) findViewById(R.id.message_buying_list);
    	listView.setXListViewListener(this, 0);
		listView.setPullLoadEnable(true);
		listView.setRefreshTime();
//        listView.loadMoreHide();
//        listView.loadMoreInvisible();
		messageModel = new AppMessageModel(mContext);
		messageModel.addResponseListener(this);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MessageObject msg;
                int eid;

                int index = position - listView.getHeaderViewsCount();
                if (isBuyTab() && messageBuyingAdapter != null &&
                        index >= 0 && index < messageBuyingAdapter.getCount()) {
                    msg = messageBuyingAdapter.getItem(index);  // 0 is header
                    eid = MessageConstant.MSG_BUY_CLK;
                }
                else if (messageSellingAdapter != null &&
                        index >= 0 && index < messageSellingAdapter.getCount()) {
                    msg = messageSellingAdapter.getItem(index);  // 0 is header
                    eid = MessageConstant.MSG_SELL_CLK;
                } else {
                    return;
                }

                Message event = Message.obtain();
                event.what = eid;
                event.obj = msg;
                EventBus.getDefault().post(event);
            }
        });
	}
	
	public void bindData(String flag) {
		this.flag = flag;

        // init list view adapter
        if (TabMessageActivity.FLAG_BUY.equals(flag)) {
            setBuyingDataAdapter();
        }
        else if (TabMessageActivity.FLAG_SELL.equals(flag)) {
            setSellingDataAdapter();
        }
        else {
            setSystemDataAdapter();
        }
        listView.refreshNow();
	}


	@Override
	public void onRefresh(int id) {
		if(flag.equals(TabMessageActivity.FLAG_BUY)) {
			messageModel.getMessageBuyingList();

            TrackingUtils.trackTouch(VIEW_MESSAGE, "message_buy_refresh");
		} else if(flag.equals(TabMessageActivity.FLAG_SELL)) {
			messageModel.getMessageSellingList();

            TrackingUtils.trackTouch(VIEW_MESSAGE, "message_sell_refresh");
		} else if(flag.equals(TabMessageActivity.FLAG_SYS)) {
			messageModel.getMessageSystemList();

            TrackingUtils.trackTouch(VIEW_MESSAGE, "message_system_refresh");
		}

        readAllSysMsg();
	}

    private void readAllSysMsg() {
        messageModel.readAllSystem();
        DisplayUtils.cancelAllNotifications();
    }

    @Override
	public void onLoadMore(int id) {
		if(flag.equals(TabMessageActivity.FLAG_BUY)) {
			messageModel.getMessageBuyingListMore();

            TrackingUtils.trackTouch(VIEW_MESSAGE, "message_buy_loadmore");
		} else if(flag.equals(TabMessageActivity.FLAG_SELL)) {
			messageModel.getMessageSellingListMore();

            TrackingUtils.trackTouch(VIEW_MESSAGE, "message_sell_loadmore");
		} else if(flag.equals(TabMessageActivity.FLAG_SYS)) {
			messageModel.getMessageSystemListMore();

            TrackingUtils.trackTouch(VIEW_MESSAGE, "message_system_loadmore");
		}

        readAllSysMsg();
	}

	@Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {

		if(url.startsWith(ApiInterface.MESSAGE_BUYING)) {
            listView.stopRefresh();
            listView.stopLoadMore();
			setBuyingDataAdapter();
            updateListViewStatus();

            messageModel.getMessageUnreadCount();
		} else if(url.startsWith(ApiInterface.MESSAGE_SELLING)) {
            listView.stopRefresh();
            listView.stopLoadMore();
			setSellingDataAdapter();
            updateListViewStatus();

            messageModel.getMessageUnreadCount();
		} else if(url.startsWith(ApiInterface.MESSAGE_SYSTEM)) {
            listView.stopRefresh();
            listView.stopLoadMore();
			setSystemDataAdapter();
            updateListViewStatus();

            messageModel.getMessageUnreadCount();
		}
        else if (url.startsWith(ApiInterface.MESSAGE_SYSTEM_READ)) {
            messageModel.getMessageUnreadCount();
		}
        else if (url.startsWith(ApiInterface.READ_MESSAGE)) {
            messageModel.getMessageUnreadCount();
        }
	}

    private void updateListViewStatus() {
        if (messageModel.meta == null) {
            listView.setPullLoadEnable(false);
        }
        else {
            listView.setPullLoadEnable(!TextUtils.isEmpty(messageModel.meta.next));
        }
    }

    private void setBuyingDataAdapter() {
        if(messageBuyingAdapter == null) {
            messageBuyingAdapter = new MessageAdapter(mContext, messageModel.messageBuyingList, TabMessageActivity.FLAG_BUY);
            listView.setAdapter(messageBuyingAdapter);
        } else {
            messageBuyingAdapter.list = messageModel.messageBuyingList;
            messageBuyingAdapter.notifyDataSetChanged();
        }
	}

	private void setSellingDataAdapter() {
        if(messageSellingAdapter == null) {
            messageSellingAdapter = new MessageAdapter(mContext, messageModel.messageSellingList, TabMessageActivity.FLAG_SELL);
            listView.setAdapter(messageSellingAdapter);
        } else {
            messageSellingAdapter.list = messageModel.messageSellingList;
            messageSellingAdapter.notifyDataSetChanged();
        }
	}

	private void setSystemDataAdapter() {
        if(messageSystemAdapter == null) {
            messageSystemAdapter = new MessageSystemAdapter(mContext, messageModel.messageSystemList);
            listView.setAdapter(messageSystemAdapter);
        } else {
            messageSystemAdapter.list = messageModel.messageSystemList;
            messageSystemAdapter.notifyDataSetChanged();
        }
	}
	
	public void onEvent(Object event) {
		final Message message = (Message) event;

        if (message != null && message.what == MessageConstant.DELETE_SYSTEM) {
            String msgId = (String) message.obj;
            messageModel.deleteSystem(msgId);

            TrackingUtils.trackTouch(FiveMilesAppConst.VIEW_MESSAGE, "system_delete");
        }
        else if (message.what == MessageConstant.SYS_MSG_ACTION) {
            SystemMessage msgData = (SystemMessage) message.obj;
            handleSysAction(msgData);
            TrackingUtils.trackTouch(VIEW_MESSAGE, "message_system");
        }
	}

    private void handleSysAction(SystemMessage msgData) {
        SystemAction action = msgData.getAction();
        String actionData = msgData.getActionData();
        actionUtils.handleSystemAction(action, actionData, msgData.getExtraActionData());
    }

    // current content is buying items
    private boolean isBuyTab() {
        return TabMessageActivity.FLAG_BUY.equalsIgnoreCase(flag);
    }

    // current content is selling items
    private boolean isSellTab() {
        return TabMessageActivity.FLAG_SELL.equalsIgnoreCase(flag);
    }

    public void deleteMessage(int position) {
        int index = position - 1;
        MessageObject msg = null;

        try {
            if (isBuyTab()) {
                msg = messageBuyingAdapter.list.remove(index);
                messageBuyingAdapter.notifyDataSetChanged();
                TrackingUtils.trackTouch(VIEW_MESSAGE, "buy_delete");
            }
            else if (isSellTab()) {
                msg = messageSellingAdapter.list.remove(index);
                messageSellingAdapter.notifyDataSetChanged();
                TrackingUtils.trackTouch(VIEW_MESSAGE, "sell_delete");
            }

            if (msg != null) {
                messageModel.deleteMessage(flag, msg.id);
            }
        } catch (Exception e) {
            L.e(e);
        }
    }
}
