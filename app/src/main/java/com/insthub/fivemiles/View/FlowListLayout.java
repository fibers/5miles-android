package com.insthub.fivemiles.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.external.maxwin.view.XListViewHeader;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;
/**
 * 流式布局
 * 使用类似ListView的Adapter接口
 * @author planet
 *
 */
public class FlowListLayout extends LinearLayout implements OnTouchListener{
	static final String TAG = "FlowListLayout";

	/**
	 * 超出屏幕,回收的View
	 */
	private LinkedList<View> unUseViews = new LinkedList<View>();
	/**
	 * 重置布局回收的ItemView
	 */
	private LinkedList<ItemView> unUseItemViews = new LinkedList<ItemView>();
	private FlowListLayoutAdapter mAdapter;
	private OnItemClickListener mOnItemClickListener;
	private List<ItemView> mItemViews = new ArrayList<FlowListLayout.ItemView>();
	/**
	 * 列是否初始化
	 * 如果没有数据不进行初始化
	 */
	private boolean mColumnInitialized = false;

	/**
	 * 当前显示的项
	 */
	private List<ItemView> mVisiableItems = new ArrayList<ItemView>();

	/**
	 * 当前已经添加到流中的ItemView数量
	 */
	private int mTotalItemCount = 0;
	private ScrollView mScrollView;
	private View mEmptyView;
	private int mItemWidth = 0;
	private int mColumns = 2;

	private boolean mEnableScrollViewElasticity = false;
	private OnSnapListener mOnSnapListener;
	private int mTopSnapPadding = 0;
	private int mBottomSnapPadding = 0;
	private boolean mInTouch = false;

	private List<OnTouchListener> mDispatchOnTouchListeners = new ArrayList<View.OnTouchListener>();
	private GestureDetector mGestureDetector;
	private SimpleOnGestureListener mSimpleOnGestureListener;

	private SparseIntArray mHeightOfCol = new SparseIntArray();
	private XListViewHeader mHeaderView;
	public FlowListLayout(Context context) {
		super(context);
		//		mTouchSlop = ViewConfiguration.get(context).getScaledDoubleTapSlop();
		//		Log.i(TAG, "mTouchSlop="+mTouchSlop);
	}
	public FlowListLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		mHeaderView = new XListViewHeader(context);
		addView(mHeaderView);
	}
	
	public View getEmptyView() {
		return mEmptyView;
	}
	public void setEmptyView(View mEmptyView) {
		this.mEmptyView = mEmptyView;
		addEmtpyView();
	}
	
	private void addEmtpyView(){
		if(mEmptyView != null){
			LayoutParams lp = (LayoutParams) mEmptyView.getLayoutParams();
			if(lp != null){
				addView(mEmptyView, mEmptyView.getLayoutParams());
			}else{
				addView(mEmptyView);
			}
		}
	}

	/**
	 * 为了获取Item单击事件,FlowListLayou截取了ScrollView的OnTouchListener,
	 * 如果外部想要获取ScrollView的OnTouchListener,请调用此方法
	 * @param listener
	 */
	public void addScrollViewOnTouchListener(OnTouchListener listener){
		if(mDispatchOnTouchListeners.contains(listener)) return;
		mDispatchOnTouchListeners.add(listener);
	}
	
	public int getTopSnapPadding() {
		return mTopSnapPadding;
	}
	
	public int getBottomSnapPadding(){
		return mBottomSnapPadding;
	}

	/**
	 * 添加OnItemClick事件
	 * @param onItemClickListener
	 */
	public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
		mOnItemClickListener = onItemClickListener;
	}

	/**
	 * 获取当前可见的项目
	 * @return
	 */
	public List<Integer> getVisiableItemPositions(){
		List<Integer> pos = new ArrayList<Integer>();
		for(ItemView iv : mVisiableItems){
			pos.add(iv.getId());
		}
		return pos;
	}

	/**
	 * 获取当前可见的View
	 * @return
	 */
	public List<View> getVisiableItemViews(){
		List<View> views = new ArrayList<View>();
		for(ItemView iv : mVisiableItems){
			if(iv.getChildCount() != 0)
				views.add(iv.getChildAt(0));
		}
		return views;
	}

	private void getScrollView(){
		if(mScrollView != null) return;
		if(getParent() != null && getParent() instanceof ScrollView){
			mScrollView = (ScrollView)getParent();
		}else if(getParent()!=null && getParent().getParent()!=null &&
				getParent().getParent() instanceof ScrollView){
			mScrollView = (ScrollView)(getParent().getParent());
		}
		
	}

	/**
	 * 修改列数,如果列数不变，不执行操作
	 * @param col
	 */
	public boolean changeColumonCount(int col){
		if(mColumns == col) return false;
		mColumns = col;
		reset();
		return true;
	}

	/**
	 * 重新加载全部视图
	 * @param col
	 */
	public void reset(){
		mColumnInitialized = false;
		if(mAdapter == null) return;
		mVisiableItems.clear();
		unUseItemViews.clear();
		unUseViews.clear();
		mTotalItemCount = 0;
		mColumnInitialized = false;
		mHeightOfCol.clear();
		mItemViews.clear();
		
		if(mAdapter != null && mAdapter.getCount()>0)
		for(int i=0; i<getChildCount(); i++){
			final LinearLayout colLayout = (LinearLayout) getChildAt(i);
			int colItemCount = colLayout.getChildCount();
			for(int p=0; p<colItemCount; p++){
				final ItemView itemView = (ItemView) colLayout.getChildAt(p);
				if(!itemView.isRecycled){
					recycleView(itemView);
				}
			}
			
			while(colLayout.getChildCount()>0){
				ItemView iv = (ItemView) colLayout.getChildAt(0);
				colLayout.removeViewInLayout(iv);
				colLayout.removeView(iv);
				mItemViews.remove(iv);
				iv.setId(-1);
				unUseItemViews.add(iv);
			}
		}
		initItemViews();
	}

	/**
	 * 刷新数据
	 */
	public void notifyDataSetChanged(){
		if(mAdapter == null) return;
		if(mAdapter.getCount() == 0){
			reset();
			return;
		}
		initItemViews();
		getViewTreeObserver().dispatchOnGlobalLayout();
	}

	public void setAdapter(FlowListLayoutAdapter adapter){
		if(adapter == null) return;
		mAdapter = adapter;
		initItemViews();
	}

	/**
	 * 流中每一项的宽度,根据屏幕宽度分割
	 * @return
	 */
	public int getItemWidth() {
		return mItemWidth;
	}
	/**
	 * 返回列数
	 * @return
	 */
	public int getColumns() {
		return mColumns;
	}

	/**
	 * 添加每一项的View
	 * @param parent
	 * @param width
	 * @param height
	 */
	private void addItemView(int position, LinearLayout parent, int width, double height){
		ItemView converItemView = null;
		if(unUseItemViews.size()>0){
			converItemView = unUseItemViews.remove();
		}
		if(converItemView == null){
			converItemView = new ItemView(getContext());
			converItemView.setOrientation(LinearLayout.VERTICAL);
		}
		//初始化是没有内容,回收状态为true
		converItemView.isRecycled = true;
		converItemView.setId(position);
		LayoutParams lp = new LayoutParams(width, (int)height);
		converItemView.setLayoutParams(lp);
		parent.addView(converItemView, converItemView.getLayoutParams());
		mItemViews.add(converItemView);
	}

	/**
	 * 初始化成N列
	 * @param data
	 * @param colCount
	 */
	private void initColumnViews(int colCount){
		removeAllViews();
		if(mAdapter==null || mAdapter.getCount() == 0){
			mColumnInitialized = false;
			addEmtpyView();
			return;
		}
		for(int i=0; i<colCount; i++){
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT);
			LinearLayout ll = new LinearLayout(getContext());
			lp.weight = 1;
			ll.setOrientation(LinearLayout.VERTICAL);
			addView(ll, lp);
		}
		mColumns = colCount;
		unUseItemViews.clear();
		for(int i=0; i<mColumns; i++){
			mHeightOfCol.put(i, 0);
		}
		mColumnInitialized = true;
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	private void disableOverScroll(){
		mScrollView.setOverScrollMode(OVER_SCROLL_NEVER);
	}

	private void initItemViews(){
		if(!mColumnInitialized){
			initColumnViews(mColumns);
		}
		getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				getViewTreeObserver().removeGlobalOnLayoutListener(this);
				getScrollView();
				int windowWidth = getMeasuredWidth()-getPaddingLeft()-getPaddingRight();
				mItemWidth = windowWidth/mColumns;
				for(int i=mTotalItemCount; i<mAdapter.getCount(); i+=mColumns){
					for(int c=0; c<mColumns; c++){
						if(i+c < mAdapter.getCount()){
							mTotalItemCount++;
							int[] sortHeights = new int[mColumns];
							int ch = 0;
							for(ch=0; ch<mColumns; ch++){
								sortHeights[ch] = mHeightOfCol.get(ch);
							}
							Arrays.sort(sortHeights);
							ch = mHeightOfCol.indexOfValue(sortHeights[0]);
							LinearLayout colView = (LinearLayout) getChildAt(ch);
							double itemHeight = mAdapter.getItemViewHeight(i+c, mItemWidth);
							addItemView(i+c, colView, mItemWidth, itemHeight);
							mHeightOfCol.put(ch, (int)(mHeightOfCol.get(ch)+itemHeight));
						}
					}
				}
				//填充完View以后,清除多余的ItemView
				unUseItemViews.clear();
				itemViewBottomArray = new int[mAdapter.getCount()];
				itemViewTopArray = new int[mAdapter.getCount()];
				refresh();
			}
		});
	}

	/**
	 * 回收用户View
	 * @param parent
	 * @return
	 */
	private View recycleView(ItemView parent){
		parent.isRecycled = true;
		View view = parent.getChildAt(0);
		parent.removeView(view);
		unUseViews.add(view);
		return view;
	}

	/**
	 * 刷新(新增数据或无新数据)
	 */
	private void refresh(){
		for(final ItemView iv : mItemViews){
			iv.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					iv.getViewTreeObserver().removeGlobalOnLayoutListener(this);
					checkItemView(iv);
					itemViewBottomArray[iv.getId()] = iv.getBottom();
					itemViewTopArray[iv.getId()] = iv.getTop();
				}
			});
		}
	}

	private int[] itemViewBottomArray;
	private int[] itemViewTopArray;
	/**
	 * checkSum作用:将scrollListener事件出发次数减半
	 */
	private boolean checkSum = true;

	/**
	 * 滚动的时候,循环检查ItemView是否在屏幕中
	 */
	private OnScrollChangedListener mOnScrollChangedListener = new OnScrollChangedListener() {
		@Override
		public void onScrollChanged() {
			if(checkSum){
				if(mItemViews.size() == 0)return;
				int overBottom = mScrollView.getScrollY()-getTop();
				int overTop = (mScrollView.getScrollY()-getTop()+mScrollView.getMeasuredHeight());
				for(int i=0; i<itemViewBottomArray.length; i++){
					int top = itemViewTopArray[i];
					int bottom = itemViewBottomArray[i];
					ItemView itemView = mItemViews.get(i);
					if(bottom<overBottom || top>overTop){
						if(itemView.isOnScreen){
							itemView.isOnScreen = false;
							mVisiableItems.remove(itemView);
						}
						if(!itemView.isRecycled){
							recycleView(itemView);
						}
					}else{
						if(!itemView.isOnScreen){
							mVisiableItems.add(itemView);
							itemView.isOnScreen = true;
							if(itemView.isRecycled && itemView.getId() < mAdapter.getCount()){
								getView(itemView, itemView.getId());
							}
						}
					}
				}
			}
			checkSum = !checkSum;
		}
	};

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		
		getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListener);

		getScrollView();
		mScrollView.setOnTouchListener(FlowListLayout.this);

		//============ 添加ItemClick监听 ===========
		mScrollView.setOnTouchListener(FlowListLayout.this);
		mSimpleOnGestureListener = new SimpleOnGestureListener(){
			@Override
			public boolean onSingleTapUp(MotionEvent e) {
				if(mOnItemClickListener != null){
					Rect rect = new Rect();
					for(ItemView iv : mVisiableItems){
						iv.getRect(rect);
						if(rect.contains((int)e.getRawX(), (int)e.getRawY())){
							mOnItemClickListener.onItemClick(iv, iv.getId());
						}
					}
				}
				return super.onSingleTapUp(e);
			}
		};
		mGestureDetector = new GestureDetector(getContext(), mSimpleOnGestureListener, null);
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		getViewTreeObserver().removeOnScrollChangedListener(mOnScrollChangedListener);
	}

	/**
	 * 检查是否需要回收、重新显示
	 * @param itemView
	 */
	private void checkItemView(ItemView itemView){
		if(itemViewOnScreen(itemView)){
			//如果ItemView在屏幕上,而且被回收了,就重新getView
			if(itemView.isRecycled && itemView.getId() < mAdapter.getCount()){
				getView(itemView, itemView.getId());
			}
		}else{
			//如果View不再屏幕上,回收
			if(!itemView.isRecycled){
				recycleView(itemView);
			}
		}
	}

	private void getView(ItemView itemView, int position){
		//添加ImageView
		View convertView = null;
		if(unUseViews.size() > 0){
			convertView = unUseViews.remove();
		}
		View view = mAdapter.getView(convertView, position);
		itemView.addView(view, view.getLayoutParams());
		//infalate完成以后,isRecycled设置为false
		itemView.isRecycled = false;
	}

	private boolean itemViewOnScreen(ItemView itemView){
		if(itemView.getBottom()<mScrollView.getScrollY()-getTop()){
			if(itemView.isOnScreen){
				itemView.isOnScreen = false;
				mVisiableItems.remove(itemView);
			}
			return false;
		}else if(itemView.getTop()>(mScrollView.getScrollY()-getTop()+mScrollView.getMeasuredHeight())){
			if(itemView.isOnScreen){
				itemView.isOnScreen = false;
				mVisiableItems.remove(itemView);
			}
			return false;
		}else{
			if(!itemView.isOnScreen){
				mVisiableItems.add(itemView);
				itemView.isOnScreen = true;
			}
			return true;
		}
	}

	class ItemView extends LinearLayout{
		boolean isRecycled = true;
		/**
		 * 是否添加了OnScrollChangedListener
		 */
		boolean isListened = false;
		boolean isOnScreen = false;
		public ItemView(Context context) {
			super(context);
		}

		public ItemView(Context context, AttributeSet attrs) {
			super(context, attrs);
		}

		public LinearLayout getParentView(){
			return (LinearLayout) getParent();
		}

		public boolean getRect(Rect rect){
			int[] co = new int[2];
			this.getLocationOnScreen(co);
			rect.left = co[0];
			rect.top = co[1];
			rect.right = rect.left+this.getMeasuredWidth();
			rect.bottom = rect.top + this.getMeasuredHeight();
			return false;
		}
		
		@Override
		protected void onDetachedFromWindow() {
			isOnScreen = false;
			super.onDetachedFromWindow();
		}
	}

	public interface OnItemClickListener{
		public void onItemClick(View view, int position);
	}

	public interface FlowListLayoutAdapter{		
		public View getView(View convertView, int position);

		/**
		 * 获取数据总数
		 * @return
		 */
		public int getCount();

		/**
		 * 获取当前项的高度,用于布局
		 * 注意：高度必须返回，用来初始化列表高度
		 * @param currentItemWidth
		 * @return
		 */
		public int getItemViewHeight(int position, int currentItemWidth);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		boolean ret = false;
		for(OnTouchListener l : mDispatchOnTouchListeners){
			if(l.onTouch(v, event)){
				ret = true;
			}
		}
		if(ret){
			return true;
		}
		mGestureDetector.onTouchEvent(event);
		return false;
	}

	/**
	 * 下拉松开,上拉松开监听器
	 */
	public interface OnSnapListener{
		/**
		 * 下拉到头,松开
		 */
		public void onSnapToTop();
		/**
		 * 上拉到头,松开
		 */
		public void onSnapToBottom();
	}

	/**
	 * 使能ScrollView弹性
	 * @param topPadding 顶部隐藏的高度
	 * @param bottomPadding 底部隐藏的高度
	 */
	public void enableScrollViewElasticity(int topPadding, int bottomPadding){
		
		if(mEnableScrollViewElasticity == true) return;
		mEnableScrollViewElasticity = true;
		mTopSnapPadding = topPadding;
		mBottomSnapPadding = bottomPadding;
		getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				getViewTreeObserver().removeGlobalOnLayoutListener(this);
				Log.i(TAG, "添加弹性效果");
				getScrollView();

				//需要为flowlist设置最小高度,否则内容不够,弹性失效
				post(new Runnable() {
					@Override
					public void run() {
						setMinimumHeight(mScrollView.getMeasuredHeight()-getTop()+mBottomSnapPadding);
					}
				});

				//第一次动画滚动到顶部
				mScrollView.postDelayed(new Runnable() {
					@Override
					public void run() {
						mScrollView.smoothScrollTo(0, mBottomSnapPadding);
					}
				}, 500);

				//首先监听 触摸弹起事件,默认动画滚动到指定的距离
				addScrollViewOnTouchListener(new OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if(event.getAction() == MotionEvent.ACTION_DOWN){
							mInTouch = true;
						}else if(event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction()==MotionEvent.ACTION_UP){
							mInTouch = false;
							int y = mScrollView.getScrollY();
							int bottom = mScrollView.getChildAt(0).getBottom()-y-mScrollView.getMeasuredHeight();
							if(y<mTopSnapPadding){
								mScrollView.post(new Runnable() {
									public void run() {
										mScrollView.smoothScrollTo(0, mTopSnapPadding);
									}
								});
								if(y == 0 && mOnSnapListener !=null){
									mOnSnapListener.onSnapToTop();
								}
							}else if(bottom<mBottomSnapPadding){
								mScrollView.post(new Runnable() {
									public void run() {
										mScrollView.smoothScrollTo(0, mScrollView.getChildAt(0).getBottom()-mScrollView.getMeasuredHeight()-mBottomSnapPadding);
									}
								});
								if(bottom == 0 && mOnSnapListener !=null){
									mOnSnapListener.onSnapToBottom();
								}
							}
						}
						return false;
					}
				});

				//再次监听ScrollView的滚动事件, 如果滚动到底部或者顶部,自动滚动到指定位置
				mScrollView.getViewTreeObserver().addOnScrollChangedListener(new OnScrollChangedListener() {
					@Override
					public void onScrollChanged() {
						int y = mScrollView.getScrollY();
						int bottom = mScrollView.getChildAt(0).getBottom()-y-mScrollView.getMeasuredHeight();
						if(y<=0 && !mInTouch){
							mScrollView.smoothScrollTo(0, mTopSnapPadding);
						}
						if(bottom <= 0 && !mInTouch){
							mOnSnapListener.onSnapToBottom();
							mScrollView.smoothScrollTo(0, mScrollView.getChildAt(0).getBottom()-mScrollView.getMeasuredHeight()-mBottomSnapPadding);
						}
					}
				});

				if(Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD){
					disableOverScroll();
				}
			}
		});
	}

	/**
	 * 设置上拉松开,下拉松开监听器
	 * 调用此方法前,请使能ScrollView弹性
	 * @param onSnapListener
	 */
	public void setOnSnapListener(OnSnapListener onSnapListener) {
		mOnSnapListener = onSnapListener;
	}
}