package com.insthub.fivemiles.View;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;

import com.external.maxwin.view.XListView;
import com.insthub.fivemiles.Protocol.MessageItem;

public class ListViewCompat extends XListView {

    private static final String TAG = "ListViewCompat";

    private SlideView mFocusedItemView;
    
    private GestureDetector mGestureDetector;
//    View.OnTouchListener mGestureListener;

    public ListViewCompat(Context context) {
        super(context);
    }

    public ListViewCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
        mGestureDetector =new GestureDetector(new YScrollDetector());
        setFadingEdgeLength(0);
    }

    public ListViewCompat(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void shrinkListItem(int position) {
        View item = getChildAt(position);

        if (item != null) {
            try {
                ((SlideView) item).shrink();
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
        }
    }
    
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        super.onInterceptTouchEvent(ev);
        return mGestureDetector.onTouchEvent(ev);
    }

    class YScrollDetector extends SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                float distanceX, float distanceY) {
            if(Math.abs(distanceY) >= Math.abs(distanceX)) {
                Log.e("jj","上下....");
                return true;
            }
            Log.e("jj","左右....");
            return false;
        }
        
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	
        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN: {
            int x = (int) event.getX();
            int y = (int) event.getY();
            int position = pointToPosition(x, y);
            Log.e(TAG, "postion=" + position);
            if (position != INVALID_POSITION) {
                MessageItem data = (MessageItem) getItemAtPosition(position);
                //if(data.slideView != null) {
                	mFocusedItemView = data.slideView;
                //}
                
            }
        }
        
        default:
            break;
        }
        
        

        if (mFocusedItemView != null) {
        	getParent().requestDisallowInterceptTouchEvent(true);
            mFocusedItemView.onRequireTouchEvent(event);
        }
        
        return super.onTouchEvent(event);
    }

}
