package com.insthub.fivemiles.Adapter;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.thirdrock.fivemiles.R;
import com.insthub.fivemiles.Protocol.MessageItem;
import com.insthub.fivemiles.View.SlideView;
import com.insthub.fivemiles.View.SlideView.OnSlideListener;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MessageBuyingAdapter extends BaseAdapter {
	
	private Context context;
	private List<MessageItem> list;
	private LayoutInflater inflater;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	
	private SlideView mLastSlideViewWithStatusOn1;
    private SlideView mLastSlideViewWithStatusOn2;
    private SlideView mLastSlideViewWithStatusOn3;
    
    private boolean view1IsMove = false;
    
    int x;
	
	public MessageBuyingAdapter(Context context, List<MessageItem> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
	}
	
	public void setdata() {
		if (mLastSlideViewWithStatusOn1 != null ) {
            mLastSlideViewWithStatusOn1.shrink();
        }
		if (mLastSlideViewWithStatusOn2 != null) {
            mLastSlideViewWithStatusOn2.shrink();
        }
		if (mLastSlideViewWithStatusOn3 != null) {
            mLastSlideViewWithStatusOn3.shrink();
        }
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}
	
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return super.getItemViewType(position);
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.message_buying_item, null);
			holder.slideView1 = (SlideView) convertView.findViewById(R.id.message_view1);
			holder.slideView2 = (SlideView) convertView.findViewById(R.id.message_view2);
			holder.slideView3 = (SlideView) convertView.findViewById(R.id.message_view3);
			
			holder.deleteHolder1 = (ViewGroup)holder.slideView1.findViewById(R.id.holder);
			holder.deleteHolder2 = (ViewGroup)holder.slideView2.findViewById(R.id.holder);
			holder.deleteHolder3 = (ViewGroup)holder.slideView3.findViewById(R.id.holder);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.slideView1.setOnSlideListener(new OnSlideListener() {
			
			@Override
			public void onSlide(View view, int status) {
				// TODO Auto-generated method stub
				if (mLastSlideViewWithStatusOn1 != null && mLastSlideViewWithStatusOn1 != view) {
		            mLastSlideViewWithStatusOn1.shrink();
		        }

		        if (status == SLIDE_STATUS_ON) {
		            mLastSlideViewWithStatusOn1 = (SlideView) view;
		        }
			}
		});
		
		
		holder.slideView1.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				MessageItem item = list.get(position);
				item.slideView = holder.slideView1;
				if (mLastSlideViewWithStatusOn2 != null) {
		            mLastSlideViewWithStatusOn2.shrink();
		        }
				if (mLastSlideViewWithStatusOn3 != null) {
		            mLastSlideViewWithStatusOn3.shrink();
		        }
				
				
//				final Timer timer = new Timer();
//		        TimerTask timerTask = new TimerTask() {
//		            @Override
//		            public void run() {
//		                // TODO Auto-generated method stub
//		            	timer.cancel();
//		            	MessageItem item = list.get(position);
//						item.slideView = holder.slideView1;
//						if (mLastSlideViewWithStatusOn2 != null) {
//				            mLastSlideViewWithStatusOn2.shrink();
//				        }
//						if (mLastSlideViewWithStatusOn3 != null) {
//				            mLastSlideViewWithStatusOn3.shrink();
//				        }
//		            }
//		        };
//		        timer.schedule(timerTask, 2000);
				
				
//				switch(event.getAction()) {
//				case MotionEvent.ACTION_DOWN:
//					view1IsMove = false;
//					final Timer timer = new Timer();
//			        TimerTask timerTask = new TimerTask() {
//			            @Override
//			            public void run() {
//			                // TODO Auto-generated method stub
//			            	timer.cancel();
//			            	System.out.println("111111111111111111111111111111");
//							view1IsMove = true;
//			            }
//			        };
//			        timer.schedule(timerTask, 5000);
//					break;
//				case MotionEvent.ACTION_MOVE:
//					System.out.println("44444444444444444444");
//					
//					if(!view1IsMove) {
//						
//						System.out.println("22222222222222222222222222222");
//						return true;
//					} else {
//						return false;
//					}
//				case MotionEvent.ACTION_UP:
//					
//					view1IsMove = false;
//					System.out.println("5555555555555555555");
////					if(timer != null) {
////	            		timer.cancel();
////		            	timer = null;
////	            	}
//					
//					break;
//				case MotionEvent.ACTION_CANCEL:
//					//view1IsMove = false;
//					System.out.println("6666666666666666666666");
////					if(timer != null) {
////	            		timer.cancel();
////		            	timer = null;
////	            	}
//					break;
//				default:
//		            return false;
//					
//				}
//				
//				System.out.println("event.getAction():"+event.getAction());
//				
//				if(!view1IsMove) {
//					
//					System.out.println("22222222222222222222222222222");
//					//return true;
//				} 
//				System.out.println("333333333333333333333333333333");
				return false;
				
				//return false;
			}
		});
		
		holder.slideView2.setOnSlideListener(new OnSlideListener() {
			
			@Override
			public void onSlide(View view, int status) {
				// TODO Auto-generated method stub
				if (mLastSlideViewWithStatusOn2 != null && mLastSlideViewWithStatusOn2 != view) {
		            mLastSlideViewWithStatusOn2.shrink();
		        }

		        if (status == SLIDE_STATUS_ON) {
		            mLastSlideViewWithStatusOn2 = (SlideView) view;
		        }
			}
		});
		
		holder.slideView2.setOnTouchListener(new OnTouchListener() {
				
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				MessageItem item = list.get(position);
				item.slideView = holder.slideView2;
				if (mLastSlideViewWithStatusOn1 != null) {
					mLastSlideViewWithStatusOn1.shrink();
				}
				if (mLastSlideViewWithStatusOn3 != null) {
					mLastSlideViewWithStatusOn3.shrink();
				}
				return false;
			}
		});
		
		holder.slideView3.setOnSlideListener(new OnSlideListener() {
			
			@Override
			public void onSlide(View view, int status) {
				// TODO Auto-generated method stub
				if (mLastSlideViewWithStatusOn3 != null && mLastSlideViewWithStatusOn3 != view) {
		            mLastSlideViewWithStatusOn3.shrink();
		        }

		        if (status == SLIDE_STATUS_ON) {
		            mLastSlideViewWithStatusOn3 = (SlideView) view;
		        }
			}
		});
		
		holder.slideView3.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				MessageItem item = list.get(position);
				item.slideView = holder.slideView3;
				if (mLastSlideViewWithStatusOn1 != null) {
					mLastSlideViewWithStatusOn1.shrink();
				}
				if (mLastSlideViewWithStatusOn2 != null) {
					mLastSlideViewWithStatusOn2.shrink();
				}
				return false;
			}
		});
		
		
		return convertView;
	}
	
	class ViewHolder {
		private SlideView slideView1;
		private SlideView slideView2;
		private SlideView slideView3;
		
		private ViewGroup deleteHolder1;
		private ViewGroup deleteHolder2;
		private ViewGroup deleteHolder3;
	}
}
