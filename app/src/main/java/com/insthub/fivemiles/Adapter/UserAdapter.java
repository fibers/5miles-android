package com.insthub.fivemiles.Adapter;

import android.content.Context;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.external.eventbus.EventBus;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Protocol.USER;
import com.insthub.fivemiles.SESSION;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.ui.widget.AvatarView;

import java.util.List;

public class UserAdapter extends BaseAdapter {

    private final DisplayImageOptions avatarImgOpts;
    private final int avatarSizePx;
    private Context context;
    private int itemResourceId;
    public List<USER> list;
    private LayoutInflater inflater;
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    private String userId;
    public UserAdapter(Context context, List<USER> list, Boolean needFollowing) {
        this.userId = SESSION.getInstance().getUserId();
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
        //if need following icon, must use user_item_with_following layout.
        itemResourceId = needFollowing?R.layout.user_item_with_following:R.layout.user_item;
        avatarSizePx = context.getResources().getDimensionPixelSize(R.dimen.list_avatar_size);
        avatarImgOpts = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.head_loading)
                .showImageForEmptyUri(R.drawable.no_avatar)
                .showImageOnFail(R.drawable.no_avatar)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();
    }
    @Override
    public int getCount() {
// TODO Auto-generated method stub
        return list.size();
    }
    @Override
    public Object getItem(int position) {
// TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
// TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
// TODO Auto-generated method stub
        ViewHolder holder;
        USER user = list.get(position);
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(itemResourceId, null);
            holder.avatar = (AvatarView) convertView.findViewById(R.id.user_item_avatar);
            holder.name = (TextView) convertView.findViewById(R.id.user_item_name);
            holder.following = (ImageView) convertView.findViewById(R.id.user_item_following);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        DisplayUtils.showAvatar(holder.avatar, user, avatarSizePx, avatarImgOpts);
        holder.name.setText(user.nick_name);

        displayFollowing(position, holder, user);

        return convertView;
    }

    private void displayFollowing(final int position, ViewHolder holder, USER user) {
        if (holder.following == null) {
            return;
        }

        if(ModelUtils.isMe(user.id)){
            holder.following.setVisibility(View.INVISIBLE);
            return;
        }

        //render following icon;
        ImageView following = holder.following;
        if (!user.following) {
            //follow
            following.setBackgroundResource(R.drawable.rounded_button_following);
            following.setImageResource(R.drawable.ic_follow);
        } else {
            //unfollow
            following.setBackgroundResource(R.drawable.rounded_button_unfollowing);
            following.setImageResource(R.drawable.ic_following);
        }
        following.setVisibility(View.VISIBLE);

        //if there is following list,then not set the image source.
        following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                USER updatedUser = list.get(position);
                if (!updatedUser.following) {
                    Message msg = new Message();
                    msg.what = MessageConstant.PROFILE_FOLLOW_CLK;
                    msg.obj = updatedUser;
                    EventBus.getDefault().post(msg);
                } else {
                    Message msg = new Message();
                    msg.what = MessageConstant.PROFILE_UNFOLLOW_CLK;
                    msg.obj = updatedUser;
                    EventBus.getDefault().post(msg);
                }

            }
        });
    }

    class ViewHolder {
        AvatarView avatar;
        TextView name;
        ImageView following;
    }
}
