package com.insthub.fivemiles.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.insthub.fivemiles.Utils.ImageAsyncTask;
import com.insthub.fivemiles.Utils.ImageUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thirdrock.domain.ImageInfo;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.Location;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.Currencies;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.ModelUtils;

import static com.thirdrock.fivemiles.util.LocationUtils.formatDistance;
import static com.thirdrock.fivemiles.util.LocationUtils.formatLocation;

public class ItemLocationInfoWindowAdapter implements InfoWindowAdapter {

    private Context context;
    private final View mWindow;
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    private ImageView image;
    private TextView title;
    private TextView location;
    private TextView price;
    private Item item = null;

    public ItemLocationInfoWindowAdapter(Context context, Item homeObject) {
        this.context = context;
        this.item = homeObject;
        mWindow = LayoutInflater.from(context).inflate(R.layout.map_window_view, null);
        image = (ImageView) mWindow.findViewById(R.id.map_window_image);
        title = (TextView) mWindow.findViewById(R.id.map_window_title);
        location = (TextView) mWindow.findViewById(R.id.map_window_location);
        price = (TextView) mWindow.findViewById(R.id.map_window_price);
    }

    private void populateView(Marker marker) {
        title.setText(item.getTitle());
        price.setText(Currencies.formatCurrency(item.getCurrencyCode(), item.getPrice()));

        Location loc = item.getLocation();
        location.setText(formatDistance(loc.getLatitude(), loc.getLongitude()) + " " +
                formatLocation(item.getCountry(), item.getProvince(), item.getCity()));

        ImageInfo img = item.getDefaultImage();
        if (img != null && !ModelUtils.isEmpty(img.getUrl())) {

            String imgUrl = CloudHelper.toCropUrl(img.getUrl(),
                    DisplayUtils.toPixels(context, 80),
                    DisplayUtils.toPixels(context, 80),
                    "pad", null);
            imgUrl = imgUrl == null ? img.getUrl() : imgUrl;

            String imagePath = ImageUtils.makeImagePathFromUrl(imgUrl);
            Bitmap bitmap = ImageUtils.loadImageFromCache(new ImageCallback(marker), imagePath, imgUrl);
            if (bitmap == null) {
                image.setImageResource(R.drawable.loading);
            } else {
                image.setImageBitmap(bitmap);
            }
        }
    }

    @Override
    public View getInfoWindow(Marker marker) {
        populateView(marker);
        return mWindow;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    private class ImageCallback implements ImageAsyncTask.ImageAsyncTaskCallback {
        Marker marker;

        public ImageCallback(Marker marker) {
            this.marker = marker;
        }

        @Override
        public void onPreImageLoad() {

        }

        @Override
        public void onPostImageLoad(Bitmap bitmap, String imagePath) {
            if (bitmap == null) {
                return;
            }
            image.setImageBitmap(bitmap);
            marker.showInfoWindow();
        }

    }
}
