package com.insthub.fivemiles.Adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thirdrock.fivemiles.R;

public class CurrencyAdapter extends BaseAdapter {
	
	public List<String> list;
	private LayoutInflater inflater;
	
	public CurrencyAdapter(Context context, List<String> list) {
		this.list = list;
		inflater = LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}
	
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.spinner_item, null);
			holder.text = (TextView) convertView.findViewById(R.id.spinner_item_text);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.text.setText(list.get(position).toString());
		return convertView;
	}
	
	class ViewHolder {
		TextView text;
	}
	
	public static interface IOnItemSelectListener{
		public void onItemClick(int position);
	};
}
