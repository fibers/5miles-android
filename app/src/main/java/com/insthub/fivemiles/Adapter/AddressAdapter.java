package com.insthub.fivemiles.Adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thirdrock.fivemiles.R;
import com.insthub.fivemiles.Protocol.COUNTRY;
import com.nostra13.universalimageloader.core.ImageLoader;

public class AddressAdapter extends BaseAdapter {
	
	private Context context;
	private List<COUNTRY> list;
	private LayoutInflater inflater;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	public AddressAdapter(Context context, List<COUNTRY> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}
	
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.select_address_item, null);
			holder.name = (TextView) convertView.findViewById(R.id.address_text);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		COUNTRY country = list.get(position);
		holder.name.setText(country.country_name);
		return convertView;
	}
	
	class ViewHolder {
		TextView name;
	}
}
