package com.insthub.fivemiles.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.insthub.fivemiles.Model.SearchModel;
import com.pedrogomez.renderers.Renderer;
import com.thirdrock.fivemiles.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ywu on 15/3/11.
 */
public class FilterOptionRenderer extends Renderer<SearchModel.Option> {

    @InjectView(R.id.txt_opt)
    TextView txtOpt;

    @Override
    protected void setUpView(View rootView) {
        ButterKnife.inject(this, rootView);
    }

    @Override
    protected void hookListeners(View rootView) {

    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.filter_option_item, parent, false);
    }

    @Override
    public void render() {
        SearchModel.Option opt = getContent();
        txtOpt.setText(opt.displayName);
    }
}
