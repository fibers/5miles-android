package com.insthub.fivemiles.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.external.activeandroid.util.Log;
import com.insthub.fivemiles.Protocol.HOME_OBJECTS;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.StateUtils;

import java.util.List;

public class WatchingAdapter extends BaseAdapter {

    final static String TAG = "UI";
    private final int thumbSizePx;
    private final DisplayImageOptions thumbImgOpts;

    public List<HOME_OBJECTS> list;
	private LayoutInflater inflater;
	protected ImageLoader imageLoader = ImageLoader.getInstance();

    private Context context;

	public WatchingAdapter(Context context, List<HOME_OBJECTS> list) {
		this.list = list;
        this.context = context;
		inflater = LayoutInflater.from(context);

        thumbSizePx = context.getResources().getDimensionPixelSize(R.dimen.list_thumb_size);

        thumbImgOpts = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading)
                .showImageForEmptyUri(R.drawable.loading)
                .showImageOnFail(R.drawable.loading)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .build();
	}

    private void showThumb(String url, ImageView iv) {
        url = CloudHelper.toCropUrl(url, thumbSizePx, thumbSizePx, "fill", null);
        ImageLoader.getInstance().displayImage(url, iv, thumbImgOpts);
    }

	@Override
	public int getCount() {
		return list.size();
	}
	
	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.buying_item, null);
			holder.image = (ImageView) convertView.findViewById(R.id.buying_item_image);
			holder.text = (TextView) convertView.findViewById(R.id.buying_item_text);
			holder.status_icon = (ImageView) convertView.findViewById(R.id.buying_item_status_icon);
			holder.status_text = (TextView) convertView.findViewById(R.id.buying_item_status_text);
			holder.price = (TextView) convertView.findViewById(R.id.buying_item_price);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		HOME_OBJECTS item = list.get(position);
        try {
            inflateView(holder, item);
        }
        catch (Exception e) {
            Log.e(TAG, "error while inflating the UI", e);
        }
		return convertView;
	}

    private void inflateView(ViewHolder holder, HOME_OBJECTS item) {
        if(item.images.size() > 0) {
            showThumb(item.images.get(0).url, holder.image);
        }
        holder.text.setText(item.title);
        holder.price.setText(item.localPrice);
//        holder.status_text.setText(item.state.getDescription(context));
        holder.status_text.setText(StateUtils.getItemDesc(context, item.state));
//        holder.status_icon.setImageResource(item.state.getStatusImageId());
        holder.status_icon.setImageResource(StateUtils.getStatusImageId(item.state));
    }

    class ViewHolder {
		ImageView image;
		TextView text;
		ImageView status_icon;
		TextView status_text;
		TextView price;
	}
}
