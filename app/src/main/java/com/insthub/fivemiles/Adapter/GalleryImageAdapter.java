package com.insthub.fivemiles.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.insthub.fivemiles.Activity.LargeImageActivity;
import com.insthub.fivemiles.Protocol.IMAGES;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.thirdrock.domain.EnumItemState;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.Utils;

import java.io.Serializable;
import java.util.List;

import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_ITEM;

public class GalleryImageAdapter extends PagerAdapter {

    private static final DisplayImageOptions OPTIONS = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.loading) // 设置图片下载期间显示的图片
            .showImageForEmptyUri(R.drawable.loading) // 设置图片Uri为空或是错误的时候显示的图片
            .showImageOnFail(R.drawable.loading) // 设置图片加载或解码过程中发生错误显示的图片
            .cacheInMemory(false) // 设置下载的图片是否缓存在内存中
            .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中
            .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
            .build();

    private static final DisplayImageOptions OPTS_WITH_THUMB = new DisplayImageOptions.Builder()
            .cacheInMemory(false) // 设置下载的图片是否缓存在内存中
            .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中
            .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
            .build();

    private LayoutInflater mInflater;
    private Context context;
    public List<IMAGES> list;
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    private EnumItemState itemState;
    private Bitmap thumbBitmap;  // 作为预览图片的bitmap

    public GalleryImageAdapter(Context context, List<IMAGES> list, Bitmap thumbBitmap) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
        this.list = list;
        this.thumbBitmap = thumbBitmap;
    }

    /**
     * 仅提供一个bitmap作为预览图片，此时pager的size为1
     */
    public GalleryImageAdapter(Context context, Bitmap thumbBitmap) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
        this.thumbBitmap = thumbBitmap;
    }

    public void setItemState(EnumItemState itemState) {
        this.itemState = itemState;
    }

    @Override
    public int getCount() {
        return list != null ? list.size() : (thumbBitmap != null ? 1 : 0);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public void startUpdate(ViewGroup container) {
        super.startUpdate(container);
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        super.finishUpdate(container);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(final ViewGroup view, final int position) {
        final ViewHolder holder;

        holder = new ViewHolder();
        View imageLayout = mInflater.inflate(R.layout.gallery_image_item, null);

        holder.image = (ImageView) imageLayout.findViewById(R.id.gallery_image_item_image);
        holder.mask = (ImageView) imageLayout.findViewById(R.id.mask);

        if (thumbBitmap != null) {
            // 显示一张模糊的图片(放大首页显示的缩略图)
            Utils.doOnGlobalLayout(holder.image, new Runnable() {
                @Override
                public void run() {
                    L.d("show thumb v %dx%d iv %dx%d image %dx%d",
                            view.getMeasuredWidth(), view.getMeasuredHeight(),
                            holder.image.getMeasuredWidth(), holder.image.getMeasuredHeight(),
                            thumbBitmap.getWidth(), thumbBitmap.getHeight());
                    holder.image.setImageBitmap(thumbBitmap);
                    holder.image.setAlpha(0.8f);
                }
            });
        }

        if (list != null && !list.isEmpty()) {
            if (thumbBitmap == null) {
                holder.image.setImageResource(DisplayUtils.getNextBackgroundResource());
            }

            final IMAGES image = list.get(position);
            loadImage(view, holder.image, image);

            holder.image.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, LargeImageActivity.class);
                    intent.putExtra("images", (Serializable) list);
                    intent.putExtra("position", position);
                    context.startActivity(intent);
                    TrackingUtils.trackTouch(VIEW_ITEM, "photo");
                }
            });
        }

        holder.mask.setVisibility((itemState == null || itemState == EnumItemState.LISTING) ?
                View.GONE : View.VISIBLE);

        view.addView(imageLayout, 0);
        return imageLayout;
    }

    private void loadImage(ViewGroup viewGroup, final ImageView iv, IMAGES img) {
        // crop picture to square according to parent width
        int size = viewGroup.getMeasuredWidth();

        String url = getImageUrl(img, size, size);
        imageLoader.displayImage(url, iv,
                thumbBitmap == null ? OPTIONS : OPTS_WITH_THUMB,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        iv.setAlpha(1f);
                    }
                });

    }

    private String getImageUrl(IMAGES image, int width, int height) {
        width = width <= 0 ? 800 : width;
        height = height <= 0 ? 800 : height;
        return CloudHelper.toCropUrl(image.url, width, height, "fill", null);
    }

    class ViewHolder {
        private ImageView image, mask;
    }
}
