package com.insthub.fivemiles.Adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.BeeFramework.Utils.TimeUtil;
import com.insthub.fivemiles.Protocol.MessageObject;
import com.insthub.fivemiles.Protocol.USER;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.framework.ui.widget.AvatarView;

import java.util.List;

public class MessageAdapter extends BaseAdapter {

    private Context context;
	public List<MessageObject> list;
	private LayoutInflater inflater;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	private String flag;
	
	public int postionFlag = -1;
	public int indexFlag = -1;

    private final int thumbSizePx, avatarSizePx;
    private final DisplayImageOptions avatarImgOpts, thumbImgOpts;

	public MessageAdapter(Context context, List<MessageObject> list, String flag) {
		this.context = context;
		this.list = list;
		this.flag = flag;
		inflater = LayoutInflater.from(context);

        thumbSizePx = context.getResources().getDimensionPixelSize(R.dimen.list_thumb_size);

        thumbImgOpts = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading)
                .showImageForEmptyUri(R.drawable.loading)
                .showImageOnFail(R.drawable.loading)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        avatarSizePx = context.getResources().getDimensionPixelSize(R.dimen.list_avatar_size);
        avatarImgOpts = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.head_loading)
                .showImageForEmptyUri(R.drawable.no_avatar)
                .showImageOnFail(R.drawable.no_avatar)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();
	}
	
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public MessageObject getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	@Override
	public int getViewTypeCount() {
		return Math.max(1, list.size());
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		ViewHolder holder;
		if(view == null) {
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.message_cell_list, null);
			holder.avatar = (AvatarView) view.findViewById(R.id.msg_avatar);
			holder.itemImg = (ImageView) view.findViewById(R.id.msg_item_img);
			holder.unread_count = (TextView) view.findViewById(R.id.msg_unread_num);
            holder.time = (TextView) view.findViewById(R.id.msg_time);
			holder.userName = (TextView) view.findViewById(R.id.msg_user_name);
			holder.lastChat = (TextView) view.findViewById(R.id.msg_last_chat);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		
		if(position == postionFlag) {
			postionFlag = -1;
			indexFlag = -1;
		}

        final MessageObject message = list.get(position);
        USER oppositeUser = message.getOppositeUser();

        showAvatar(oppositeUser, holder.avatar);
        showThumb(message.itemImage.url, holder.itemImg);

        holder.userName.setText(oppositeUser.nick_name);
        holder.lastChat.setText(message.message);
        holder.lastChat.setVisibility(TextUtils.isEmpty(message.message) ? View.GONE : View.VISIBLE);

        if (message.timestamp > 0) {
            holder.time.setText(TimeUtil.toConciseTime(message.timestamp));
        }
        else {
            holder.time.setText("");
        }

        if(message.unread_num > 0) {
            holder.unread_count.setVisibility(View.VISIBLE);
            holder.unread_count.setText(DisplayUtils.toApproximalCount(message.unread_num, 99));
        } else {
            holder.unread_count.setVisibility(View.GONE);
        }

        // FIXME no data from backend?
//        holder.feebackStars.setVisibility(message.isFeedback() ? View.VISIBLE : View.GONE);
//        if (message.isFeedback()) {
//            showFeedbackStars(holder, message);
//        }

		return view;
	}

    private void showThumb(String url, ImageView iv) {
        url = CloudHelper.toCropUrl(url, thumbSizePx, thumbSizePx, "fill", null);
        ImageLoader.getInstance().displayImage(url, iv, thumbImgOpts);
    }

    private void showAvatar(USER user, AvatarView iv) {
        if (user == null) {
            return;
        }

        DisplayUtils.showAvatar(iv, user, avatarSizePx, avatarImgOpts);
    }

    class ViewHolder {
		private AvatarView avatar;
        private ImageView itemImg;
		private TextView unread_count;
        private TextView time;
		private TextView userName;
		private TextView lastChat;
	}

}
