package com.insthub.fivemiles.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thirdrock.fivemiles.R;

public class GuidePagerAdapter extends PagerAdapter {

	private LayoutInflater mInflater;
	private Context context;

	public GuidePagerAdapter(Context context) {
		mInflater = LayoutInflater.from(context);
		this.context = context;
	}

	@Override
	public int getCount() {
		return 4;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view.equals(object);
	}

    @Override
	public Object instantiateItem(ViewGroup container, int position) {
		View view = mInflater.inflate(R.layout.guide_pager_item, null);
        TextView title = (TextView) view.findViewById(R.id.guide_title);
        TextView text = (TextView) view.findViewById(R.id.guide_text);

        if (position == 0) {
            view.findViewById(R.id.guide_page_1).setVisibility(View.VISIBLE);
            view.setBackgroundColor(context.getResources().getColor(R.color.fm_red));
            title.setText(R.string.guide_page_title_1);
            text.setText(R.string.guide_page_text_1);
		} else if (position == 1) {
            // #417 引导页2和4对调，观察效果
            view.findViewById(R.id.guide_page_4).setVisibility(View.VISIBLE);
            view.setBackgroundColor(context.getResources().getColor(R.color.fm_green));
            title.setText(R.string.guide_page_title_4);
            text.setText(R.string.guide_page_text_4);
		}
        else if (position == 2) {
            view.findViewById(R.id.guide_page_3).setVisibility(View.VISIBLE);
            view.setBackgroundColor(context.getResources().getColor(R.color.fm_light_blue));
            title.setText(R.string.guide_page_title_3);
            text.setText(R.string.guide_page_text_3);
        }
        else {
            view.findViewById(R.id.guide_page_2).setVisibility(View.VISIBLE);
            view.setBackgroundColor(context.getResources().getColor(R.color.fm_yellow));
            title.setText(R.string.guide_page_title_2);
            text.setText(R.string.guide_page_text_2);
		}

		container.addView(view, 0);
        return view;
	}

}
