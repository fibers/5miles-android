package com.insthub.fivemiles.Adapter;

import android.content.Context;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.BeeFramework.Utils.TimeUtil;
import com.BeeFramework.view.RoundedWebImageView;
import com.external.eventbus.EventBus;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Protocol.COMMENT;
import com.insthub.fivemiles.Protocol.HOME_OBJECTS;
import com.insthub.fivemiles.Protocol.USER;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.ModelUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CommentAdapter extends BaseAdapter {

    // 单品页最多展示多少条 comment thread
    public static final int MAX_THREADS = 5;

    private Context context;
    private HOME_OBJECTS homeObject;
//	private List<CommentThread> threads;
	private List<COMMENT> threads;
	private LayoutInflater inflater;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
    private int maxThreads;  // 0 for unlimited

    public CommentAdapter(Context context, HOME_OBJECTS homeObject, List<COMMENT> comments, int maxThreads) {
        this.context = context;
        this.homeObject = homeObject;

        if (maxThreads > 0) {
            this.maxThreads = maxThreads;
        }
        else {
            this.maxThreads = 0;
        }

        setCommentList(comments);
        inflater = LayoutInflater.from(context);
    }

    public CommentAdapter(Context context, HOME_OBJECTS homeObject, List<COMMENT> comments) {
        this(context, homeObject, comments, MAX_THREADS);
	}

    public void setCommentList(List<COMMENT> comments) {
        if (comments == null || comments.isEmpty()) {
            this.threads = Collections.emptyList();
            return;
        }

        // 暂时不采用session的方式组织，直接平铺
        threads = comments;

//        threads = new ArrayList<CommentThread>();
//        List<COMMENT> replies = new ArrayList<COMMENT>(comments.size());
//        SparseArray<COMMENT> commentDict = new SparseArray<COMMENT>();
//
//        for (COMMENT c : comments) {
//            if (c.replyTo > 0) {
//                replies.add(c);
//            }
//            else {
//                commentDict.put(c.id, c);
//            }
//        }
//
//        // comment/reply threads
//        for (COMMENT r : replies) {
//            COMMENT c = commentDict.get(r.replyTo);
//            commentDict.remove(r.replyTo);
//            if (c != null) {
//                threads.add(new CommentThread(c, r));
//            }
//        }
//
//        // comments without reply
//        for (int i = 0; i < commentDict.size(); i++) {
//            COMMENT c = commentDict.valueAt(i);
//            threads.add(new CommentThread(c, null));
//        }
//
//        // sort thread by time desc
//        Collections.sort(threads, new Comparator<CommentThread>() {
//            @Override
//            public int compare(CommentThread lhs, CommentThread rhs) {
//                return -(lhs.updated - rhs.updated);
//            }
//        });
    }

    public int getAllThreadsCount() {
        return threads.size();
    }

	@Override
	public int getCount() {
		return maxThreads > 0 ? Math.min(threads.size(), maxThreads) : threads.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.order_detail_comment_item, null);
			holder.avatar = (RoundedWebImageView) convertView.findViewById(R.id.order_detail_comment_avatar);
			holder.nickname = (TextView) convertView.findViewById(R.id.order_detail_comment_name);
			holder.time = (TextView) convertView.findViewById(R.id.order_detail_comment_time);
			holder.reply = (TextView) convertView.findViewById(R.id.order_detail_comment_reply);
			holder.content = (TextView) convertView.findViewById(R.id.order_detail_comment_content);
            holder.replyWrapper = convertView.findViewById(R.id.order_detail_reply_wrapper);
            holder.replyAvatar = (RoundedWebImageView) convertView.findViewById(R.id.order_detail_reply_avatar);
            holder.replyName = (TextView) convertView.findViewById(R.id.order_detail_reply_name);
            holder.replyTime = (TextView) convertView.findViewById(R.id.order_detail_reply_time);
            holder.replyContent = (TextView) convertView.findViewById(R.id.order_detail_reply_content);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

        final COMMENT comment = threads.get(position);
//		CommentThread thread = threads.get(position);
//        final COMMENT comment = thread.comment;
//        final COMMENT reply = thread.reply;

		if (comment.author.portrait != null) {
            imageLoader.displayImage(comment.author.portrait, holder.avatar, FiveMilesApp.imageOptionsAvatar);
        }
        else {
            holder.avatar.setImageResource(R.drawable.head_loading);
        }
		holder.nickname.setText(comment.author.nick_name);
		holder.time.setText(TimeUtil.timeAgo(comment.timestamp));
		holder.content.setText(comment.body);

        boolean canReply = ModelUtils.canReplyComent(homeObject.owner.id,
                homeObject.isSold(), comment);
        holder.reply.setVisibility(canReply ? View.VISIBLE : View.GONE);
		holder.reply.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Message msg = new Message();
	            msg.what = MessageConstant.POST_REPLY;
	            msg.arg1 = comment.id;
                msg.obj  = comment;
	            EventBus.getDefault().post(msg);
			}
		});

        holder.avatar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                viewProfile(comment.author);
            }
        });

//        if (reply != null) {
//            holder.replyWrapper.setVisibility(View.VISIBLE);
//
//            if (reply.author.portrait != null) {
//                imageLoader.displayImage(reply.author.portrait, holder.replyAvatar, FiveMilesApp.options_head);
//            }
//            else {
//                holder.replyAvatar.setImageResource(R.drawable.head_loading);
//            }
//
//            holder.replyName.setText(reply.author.nickname);
//            holder.replyTime.setText(TimeUtil.timeAgo(reply.timestamp));
//            holder.replyContent.setText(reply.body);
//        }
//        else {
            holder.replyWrapper.setVisibility(View.GONE);
//        }
		
		return convertView;
	}

    private void viewProfile(USER user) {
        Message msg= Message.obtain();
        msg.what = MessageConstant.VIEW_PROFILE;
        msg.obj  = user;
        EventBus.getDefault().post(msg);
    }

    class ViewHolder {
		RoundedWebImageView avatar, replyAvatar;
		TextView nickname, replyName;
		TextView time, replyTime;
		TextView reply;
		TextView content, replyContent;
        View replyWrapper;
	}

    private static class CommentThread implements Comparator<CommentThread> {
        COMMENT comment;
        COMMENT reply;
        int updated;

        CommentThread(COMMENT comment, COMMENT reply) {
            this.comment = comment;
            this.reply = reply;

            int commentTime = (int) (TimeUtil.parse(comment.timestamp).getTime() / 1000);
            int replyTime = 0;

            if (reply != null) {
                 replyTime = (int) (TimeUtil.parse(reply.timestamp).getTime() / 1000);
            }

            updated = Math.max(commentTime, replyTime);
        }

        @Override
        public int compare(CommentThread lhs, CommentThread rhs) {
            return -(lhs.updated - rhs.updated);
        }
    }
}
