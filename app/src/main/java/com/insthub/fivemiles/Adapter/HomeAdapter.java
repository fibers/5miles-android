package com.insthub.fivemiles.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.Protocol.HOME_OBJECTS;
import com.insthub.fivemiles.Protocol.IMAGES;
import com.insthub.fivemiles.View.ScaleImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.ImageWidthCalcHelper;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;

import java.util.List;

import static com.thirdrock.fivemiles.util.Currencies.formatCurrency;
import static com.thirdrock.fivemiles.util.DisplayUtils.getNextBackgroundResource;
import static com.thirdrock.fivemiles.util.LocationUtils.formatDistance;
import static com.thirdrock.fivemiles.util.LocationUtils.formatLocation;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;

public class HomeAdapter extends BaseAdapter {

	public List<HOME_OBJECTS> data;
	protected LayoutInflater mInflater;
	protected ImageLoader imageLoader = ImageLoader.getInstance();

	public HomeAdapter(Context context, List<HOME_OBJECTS> data) {
		this.data = data;
		mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		if (position >= 0 && position < data.size()) {
            return data.get(position);
        }
        return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null || !(convertView.getTag() instanceof ViewHolder)) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.tab_location_item, null);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.home_item_title);
            holder.image = (ScaleImageView) convertView.findViewById(R.id.home_location_item_image);
            holder.icSold = convertView.findViewById(R.id.ic_item_state_sold);
            holder.icAudio = convertView.findViewById(R.id.ic_item_audio);
            holder.price = (TextView) convertView.findViewById(R.id.home_location_item_price);
            holder.txtOriginPrice = (TextView) convertView.findViewById(R.id.home_location_item_origin_price);
            holder.distance = (TextView) convertView.findViewById(R.id.home_location_item_distance);
            holder.icLoc = convertView.findViewById(R.id.home_location_item_mark);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        HOME_OBJECTS home_object = data.get(position);
        IMAGES img = home_object.getImage();
        int imgWidth = 0, imgHeight = 0;

        if (img != null) {
            imgWidth = img.image_width;
            imgHeight = img.image_height;
        }

        imgWidth = imgWidth > 0 ? imgWidth : ImageWidthCalcHelper.IMG_W_L;
        imgHeight = imgHeight > 0 ? imgHeight : ImageWidthCalcHelper.IMG_W_L;

        holder.image.setImageWidth(imgWidth);
        holder.image.setImageHeight(imgHeight);
//        holder.image.requestLayout();
//        parent.requestLayout();

        // load clipped version of image
        int parentWidth = convertView.getMeasuredWidth();
        int fitWidth = parentWidth > 0 ? parentWidth : ImageWidthCalcHelper.IMG_W_L;

        holder.image.setImageResource(getNextBackgroundResource());
        if (img != null) {
            L.d("home adapter render image %dx%d vw:%d", imgWidth, imgHeight, fitWidth);
            String imgUrl = CloudHelper.toCropUrl(img.url, fitWidth, null, "fit", null);
            imageLoader.displayImage(imgUrl, holder.image, FiveMilesApp.imageOptionsHome);
        }

        if (ModelUtils.isNotEmpty(home_object.title)) {
            holder.txtTitle.setVisibility(View.VISIBLE);
            holder.txtTitle.setText(home_object.title);
        } else {
            holder.txtTitle.setVisibility(View.GONE);
        }

        renderLocation(home_object, holder);
        holder.icSold.setVisibility(home_object.state.isSold() ? View.VISIBLE : View.GONE);
        // 暂不显示语音图标
//        holder.icAudio.setVisibility(ModelUtils.isNotEmpty(home_object.mediaLink) ? View.VISIBLE : View.GONE);

        // prices
        holder.price.setText(formatCurrency(home_object.currency_code, home_object.price));

        if (ModelUtils.isValidPrice(home_object.originPrice)) {
            holder.txtOriginPrice.setText(formatCurrency(home_object.currency_code, home_object.originPrice));
            holder.txtOriginPrice.setVisibility(View.VISIBLE);
        } else {
            holder.txtOriginPrice.setVisibility(View.GONE);
        }

        return convertView;
    }

    private void renderLocation(HOME_OBJECTS home_object, ViewHolder holder) {
        String loc = formatLocation(home_object.country, home_object.province, home_object.city);
        if (isNotEmpty(loc)) {
            holder.icLoc.setVisibility(View.VISIBLE);
            holder.distance.setVisibility(View.VISIBLE);
            holder.distance.setText(loc);
        } else if (home_object.latitude > 0 || home_object.longitude > 0) {
            holder.icLoc.setVisibility(View.VISIBLE);
            holder.distance.setVisibility(View.VISIBLE);
            holder.distance.setText(formatDistance(home_object.distance));
        } else {
            holder.icLoc.setVisibility(View.GONE);
            holder.distance.setVisibility(View.GONE);
        }
    }

	static class ViewHolder {
		ScaleImageView image;
        View icSold, icAudio, icLoc;
		TextView txtTitle, price, txtOriginPrice, distance;
	}

}
