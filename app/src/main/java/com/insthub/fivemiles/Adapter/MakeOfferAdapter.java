package com.insthub.fivemiles.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.BeeFramework.Utils.ImageUtil;
import com.BeeFramework.Utils.TimeUtil;
import com.external.eventbus.EventBus;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.insthub.fivemiles.Activity.LargeImageActivity;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Protocol.IMAGES;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.thirdrock.domain.IUser;
import com.thirdrock.domain.ImageInfo;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.ui.widget.AvatarView;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.offer.ChatMessage;
import com.thirdrock.protocol.offer.ChatMessagePayload;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.insthub.fivemiles.FiveMilesAppConst.EMPTY_STRING;
import static com.insthub.fivemiles.FiveMilesAppConst.GOOGLE_MAP_ZOOM_LEVEL;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_MAKE_OFFER;
import static com.thirdrock.protocol.offer.ChatMessage.MSG_TYPE_IMAGE;
import static com.thirdrock.protocol.offer.ChatMessage.MSG_TYPE_LOCATION;
import static com.thirdrock.protocol.offer.ChatMessage.MSG_TYPE_TEXT;
import static com.thirdrock.repository.GeoLocationReportRepository.GOOGLE_STATIC_MAP_URL;

/**
 * Replacement for MakeOfferAdapter
 * Created by jubin on 3/6/15.
 */
public class MakeOfferAdapter extends BaseAdapter implements OnMapReadyCallback {

    public static final int MAX_MSG_INTV = 300000;  // 间隔小于5分钟的消息不显示时间
    private final DisplayImageOptions msgImageOptions;

    protected ImageLoader imageLoader = ImageLoader.getInstance();

    private Context mContext;
    private DisplayImageOptions avatarImgOpts;
    private int avatarSizePx;
    private List<ChatMessage> chatHistory;
    private LayoutInflater inflater;
    private IUser buyer;
    private IUser seller;
    private Map<String, Boolean> keyMessageList;  // key items whose timestamp should be shown

    public MakeOfferAdapter(Context context, List<ChatMessage> chatMsgList, IUser buyer, IUser seller) {
        this.mContext = context;
        keyMessageList = new HashMap<>();
        inflater = LayoutInflater.from(context);

        avatarSizePx = context.getResources().getDimensionPixelSize(R.dimen.list_avatar_size);
        avatarImgOpts = FiveMilesApp.imageOptionsAvatar;

        this.chatHistory = chatMsgList;
        this.buyer = buyer;
        this.seller = seller;

        msgImageOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .resetViewBeforeLoading(true)   // 发送地图的时候不会复用上一幅图片
                .considerExifParams(true)       // 可以处理三星的旋转问题
                .showImageOnLoading(R.drawable.chat_msg_img_place_holder)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .showImageOnFail(R.drawable.msg_image_fail)
                .displayer(new FadeInBitmapDisplayer(500))
                .build();

        // TODO rename it
        chooseKeyItems();
    }

    private void showAvatar(AvatarView pAvatarView, String pAvatarUrl, boolean isVerified) {
        DisplayUtils.showAvatar(pAvatarView, pAvatarUrl, isVerified, avatarSizePx, avatarImgOpts);
    }

    private boolean shouldShowTime(String id) {
        return keyMessageList.containsKey(id) && keyMessageList.get(id);
    }

    // choose key items whose timestamp should be shown
    private void chooseKeyItems() {
        if (chatHistory == null) {
            L.w("ChatHistory is null");
            return;
        }

        long prevMsgTime = 0;
        for (ChatMessage chatMsg : chatHistory) {
            String timestamp = chatMsg.getTimestamp();
            if (ModelUtils.isEmpty(timestamp)) {
                continue;
            }

            long currMsgTime = TimeUtil.parse(timestamp).getTime();
            boolean longEnough = Math.abs(currMsgTime - prevMsgTime) >= MAX_MSG_INTV;

            if (longEnough) {
                prevMsgTime = currMsgTime;
                keyMessageList.put(chatMsg.getId(), true);
            }
        }
    }

    @Override
    public int getCount() {
        return chatHistory.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ChatMessage chatMessage = chatHistory.get(position);
        if (chatMessage == null) {
            L.w("ChatMessage should not be null");
            return null;
        }

        ViewHolder holder;
        int layoutResId = chatMessage.isFromMe() ? R.layout.chat_item_my : R.layout.chat_item_other;

        if(convertView == null || convertView.getTag(layoutResId) == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(layoutResId, null);
            holder.avatar = (AvatarView) convertView.findViewById(R.id.msg_avatar);
            holder.txtTime = (TextView) convertView.findViewById(R.id.msg_time);
            holder.imageContainer = (ViewGroup) convertView.findViewById(R.id.msg_image_container);
            holder.text = (TextView) convertView.findViewById(R.id.msg_text);
            holder.image = (ImageView) convertView.findViewById(R.id.msg_image);
            holder.txtPlaceName = (TextView) convertView.findViewById(R.id.msg_image_txt_place);
            holder.txtAddressName = (TextView) convertView.findViewById(R.id.msg_image_txt_address);
            holder.imageMask = (ImageView) convertView.findViewById(R.id.msg_image_mask);
            // 先保留，等使用Layer时如果还用不上，删除
//            holder.progress = (DonutProgress) convertView.findViewById(R.id.msg_image_progress);
            holder.progress = (ProgressBar) convertView.findViewById(R.id.msg_image_progress);
            convertView.setTag(layoutResId, holder);
        } else {
            holder = (ViewHolder) convertView.getTag(layoutResId);
        }

        final boolean isFromBuyer = chatMessage.isFromBuyer();
        if (isFromBuyer) {
            if (buyer != null) {
                showAvatar(holder.avatar, buyer.getAvatarUrl(), buyer.isVerified());
            }
        } else {
            if (seller != null) {
                showAvatar(holder.avatar, seller.getAvatarUrl(), seller.isVerified());
            }
        }

        holder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IUser sender = isFromBuyer ? buyer : seller;
                if (sender != null) {
                    String uId = sender.getId();
                    viewProfile(uId);
                } else {
                    TrackingUtils.trackException(new IllegalStateException("sender is null in makeofferadapter"));
                }
            }
        });

        String chatMsgId = chatMessage.getId();
        if (shouldShowTime(chatMsgId)) {
            String timestamp = chatMessage.getTimestamp();
            String conciseTime = TimeUtil.toConciseTime(timestamp);
            holder.txtTime.setText(conciseTime);
            holder.txtTime.setVisibility(View.VISIBLE);
        } else {
            holder.txtTime.setVisibility(View.GONE);
        }

        final ChatMessagePayload msgBody = chatMessage.getPayload();
        switch (chatMessage.getMessageType()) {
            case MSG_TYPE_TEXT:
                holder.text.setVisibility(View.VISIBLE);
                holder.imageContainer.setVisibility(View.GONE);

                String msg = (msgBody != null)? msgBody.getText() : EMPTY_STRING;
                holder.text.setText(msg);
                break;
            case MSG_TYPE_IMAGE:
                holder.text.setVisibility(View.GONE);
                holder.imageContainer.setVisibility(View.VISIBLE);
                // 地址信息，发图片的时候隐藏
                holder.txtPlaceName.setVisibility(View.GONE);
                holder.txtAddressName.setVisibility(View.GONE);

                if (msgBody != null) {
                    final int maxWidth = FiveMilesApp.appCtx.getResources().getDimensionPixelSize(R.dimen.chat_image_view_max_width);
                    String imgUrl = msgBody.getImageUrl();
                    boolean isTemp = chatMessage.isTemp();  // 不是从服务器返回的信息
                    final String croppedUrl = isTemp? imgUrl : CloudHelper.toCropUrl(imgUrl, maxWidth, null, "fit", null);

                    ImageLoader.getInstance().displayImage(croppedUrl, holder.image, msgImageOptions);

                    if (isTemp) {
                        holder.imageMask.setVisibility(View.VISIBLE);
                        holder.progress.setVisibility(View.VISIBLE);
                    } else {
                        holder.imageMask.setVisibility(View.GONE);
                        holder.progress.setVisibility(View.GONE);
                    }

                    // TODO 转为RxJava的方式
                    holder.image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (chatMessage.isTemp()) {
                                return;     // 还没有发送成功的信息不显示大图
                            }

                            // TODO 将LargeImageActivity中的IMAGES替换为String（图片的url）
                            ImageInfo imageInfo = new ImageInfo(croppedUrl, -1, -1); // 这里的高和宽用不到，所以随便填了
                            IMAGES images = new IMAGES(imageInfo);
                            List<IMAGES> imagesList = new ArrayList<>();
                            imagesList.add(images);

                            Intent largeImageIntent = new Intent(mContext, LargeImageActivity.class)
                                    .putExtra("images", (Serializable) imagesList);
                            if (mContext != null) {
                                mContext.startActivity(largeImageIntent);
                            } else {
                                L.w("mContext is null");
                            }

                            TrackingUtils.trackTouch(VIEW_MAKE_OFFER, "click_photo");
                        }
                    });
                }
                break;
            case MSG_TYPE_LOCATION:
                holder.text.setVisibility(View.GONE);
                holder.imageContainer.setVisibility(View.VISIBLE);

                if (msgBody != null) {
                    final double lat = msgBody.getLatitude();
                    final double lng = msgBody.getLongitude();

                    // TODO 封装成一个工具方法，MakeOfferActivity中有类似语句
                    float widthInDimen = getDimensionInternal(R.dimen.chat_location_width);
                    float heightInDimen = getDimensionInternal(R.dimen.chat_location_height);
                    final int width = ImageUtil.getGoogleStaticMapWidth(widthInDimen);
                    final int height = ImageUtil.getGoogleStaticMapHeight(heightInDimen);
                    String mapThumbUrl = String.format(GOOGLE_STATIC_MAP_URL, lat, lng, GOOGLE_MAP_ZOOM_LEVEL, width, height);
                    ImageLoader.getInstance().displayImage(mapThumbUrl, holder.image, msgImageOptions);

                    final String placeName = msgBody.getPlaceName();   // 分两行显示
                    final String addressName = msgBody.getAddressName();

                    if (ModelUtils.isNotEmpty(placeName)) {
                        holder.txtPlaceName.setVisibility(View.VISIBLE);
                        holder.txtPlaceName.setText(placeName);
                    }

                    if (ModelUtils.isNotEmpty(addressName)) {
                        holder.txtAddressName.setVisibility(View.VISIBLE);
                        holder.txtAddressName.setText(addressName);
                    }

                    boolean isTemp = chatMessage.isTemp();
                    if (isTemp) {
                        holder.imageMask.setVisibility(View.VISIBLE);
                        holder.progress.setVisibility(View.VISIBLE);
                    } else {
                        holder.imageMask.setVisibility(View.GONE);
                        holder.progress.setVisibility(View.GONE);
                    }

                    // https://developer.android.com/guide/components/intents-common.html
                    // TODO 转为RxJava的方式
                    holder.image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String geoUriStr = String.format("geo:0,0?q=%f,%f(%s)?z=%d", lat, lng, placeName, GOOGLE_MAP_ZOOM_LEVEL);
                            Uri geoUri = Uri.parse(geoUriStr);
                            Intent openMapIntent = new Intent(Intent.ACTION_VIEW, geoUri);
                            mContext.startActivity(openMapIntent);

                            TrackingUtils.trackTouch(VIEW_MAKE_OFFER, "click_location");
                        }
                    });
                }
                break;
        }

        return convertView;
    }

    private float getDimensionInternal(int pResId) {
        return FiveMilesApp.appCtx.getResources().getDimension(pResId);
    }

    private void viewProfile(String uId) {
        if (ModelUtils.isEmpty(uId)) {
            return;
        }

        Message msg= Message.obtain();
        msg.what = MessageConstant.VIEW_PROFILE;
        msg.obj  = uId;
        EventBus.getDefault().post(msg);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    static class ViewHolder {
        AvatarView avatar;
        TextView txtTime;
        TextView text;
        ViewGroup imageContainer;
        ImageView image;
        TextView txtPlaceName;
        TextView txtAddressName;
        ImageView imageMask;
//        MapView mapView;
        // 先保留，等使用Layer时如果还用不上，删除
//        DonutProgress progress;
        ProgressBar progress;
    }
}
