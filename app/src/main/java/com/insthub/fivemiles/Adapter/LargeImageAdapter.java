package com.insthub.fivemiles.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.external.imagezoom.ImageViewTouch;
import com.external.imagezoom.ImageViewTouchBase;
import com.insthub.fivemiles.Protocol.IMAGES;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.framework.util.L;

import java.util.List;

import static com.insthub.fivemiles.FiveMilesAppConst.IMAGE_MAX_HEIGHT;
import static com.insthub.fivemiles.FiveMilesAppConst.IMAGE_MAX_WIDTH;

public class LargeImageAdapter extends PagerAdapter {

    private final DisplayImageOptions imageOptions;
    private LayoutInflater mInflater;
    public List<IMAGES> list;
    protected ImageLoader imageLoader = ImageLoader.getInstance();

    public LargeImageAdapter(Context context, List<IMAGES> list) {
        mInflater =  LayoutInflater.from(context);
        this.list = list;

        imageOptions = new DisplayImageOptions.Builder()
//                .showImageOnLoading(R.drawable.loading) // 设置图片下载期间显示的图片
//                .showImageForEmptyUri(R.drawable.loading) // 设置图片Uri为空或是错误的时候显示的图片
//                .showImageOnFail(R.drawable.loading) // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(false) // 设置下载的图片是否缓存在内存中
                .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();
    }
	
	@Override
	public int getCount() {
		return list.size();
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
    	final ViewHolder holder;
		
		holder = new ViewHolder();
		View imageLayout = mInflater.inflate(R.layout.large_image_item, null);
		
		holder.image = (ImageViewTouch) imageLayout.findViewById(R.id.large_item_image);
		holder.image.setDisplayType( ImageViewTouchBase.DisplayType.FIT_TO_SCREEN );
		
		IMAGES image = list.get(position);
        // crop picture to square according to parent width
        int width = view.getMeasuredWidth();
        int height = view.getMeasuredHeight();
        String url = getImageUrl(image, width, height);
        if (url != null) {
            L.d("loading image %s", url);
            imageLoader.displayImage(url, holder.image, imageOptions);
        }

    	view.addView(imageLayout, 0);
    	
        return imageLayout;
    }

    private String getImageUrl(IMAGES image, int width, int height) {
        width = (width <= 0) ? IMAGE_MAX_WIDTH : width;
        height = (height <= 0) ? IMAGE_MAX_HEIGHT : height;
        return CloudHelper.toCropUrl(image.url, width, height, "fit", null);
    }

    class ViewHolder {
		private ImageViewTouch image;
	}
}
