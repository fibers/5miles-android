package com.insthub.fivemiles.Adapter;

import java.io.File;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.BeeFramework.Utils.ImageUtil;
import com.external.eventbus.EventBus;
import com.insthub.fivemiles.MessageConstant;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.DisplayUtils;

public class AddPicAdapter extends BaseAdapter {

    private DisplayImageOptions imageLoaderOpts = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.loading) // 设置图片下载期间显示的图片
            .showImageForEmptyUri(R.drawable.loading) // 设置图片Uri为空或是错误的时候显示的图片
            .showImageOnFail(R.drawable.loading) // 设置图片加载或解码过程中发生错误显示的图片
            .cacheInMemory(false) // 设置下载的图片是否缓存在内存中
            .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中
            .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
            .build();

	private Context context;
	public List<String> list;
	private LayoutInflater inflater;
	public boolean flag = false;
	public AddPicAdapter(Context context, List<String> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(list.size() >= 6) {
			return list.size();
		} else {
			return list.size()+1;
		}
		
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		final ViewHolderImage imageHolder;
		if(position == list.size()) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.grid_item_header, null);
			holder.addPic = (FrameLayout) convertView.findViewById(R.id.add_photo);
			
			holder.addPic.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Message msg = new Message();
		            msg.what = MessageConstant.ADD_PHOTO;
		            EventBus.getDefault().post(msg);
				}
			});
			
			if(list.size() >= 6) {
				convertView.setVisibility(View.GONE);
			} else {
				convertView.setVisibility(View.VISIBLE);
			}
			
		} else {
			imageHolder = new ViewHolderImage();
			convertView = inflater.inflate(R.layout.grid_item, null);
			imageHolder.image = (ImageView) convertView.findViewById(R.id.grid_item_pic);
			imageHolder.close = (ImageView) convertView.findViewById(R.id.grid_item_pic_close);

            String path = list.get(position);
			File file = path != null ? new File(path) : null;

            if (path != null && path.startsWith("http://")) {
                // image is an url
                String imgUrl = CloudHelper.toCropUrlInPids(path, 120, null, "fit", null);
                ImageLoader.getInstance().displayImage(imgUrl, imageHolder.image, imageLoaderOpts);
            } else if (file != null && file.exists()) {
                Options options = new Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                int max = Math.max(options.outWidth, options.outHeight);
                int desiredSize = DisplayUtils.toPixels(context, 120);
                int sample = 1;
                if (max > desiredSize) {
                    sample = max / desiredSize;
                }

                options.inJustDecodeBounds = false;
                options.inSampleSize = sample;
				Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);

                int orient = ImageUtil.getImageOrientation(file);
				imageHolder.image.setImageBitmap(ImageUtil.rotateBitmap(bitmap, orient));
			} else {
				imageHolder.image.setImageResource(R.drawable.loading);
			}
			
			imageHolder.close.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Message msg = new Message();
		            msg.what = MessageConstant.REMOVE_PHOTO;
		            msg.arg1 = position;
		            EventBus.getDefault().post(msg);
				}
			});
			
		}
		return convertView;
	}
	
	class ViewHolderImage {
		ImageView image;
		ImageView close;
	}
	
	class ViewHolder {
		FrameLayout addPic;
	}

}
