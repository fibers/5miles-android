package com.insthub.fivemiles.Adapter;

import android.content.Context;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.BeeFramework.Utils.TimeUtil;
import com.BeeFramework.view.MyDelDialog;
import com.external.eventbus.EventBus;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Protocol.SystemMessage;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.ui.widget.AvatarView;
import com.thirdrock.protocol.SystemAction;

import java.util.List;

public class MessageSystemAdapter extends BaseAdapter {

    private Context context;
    public List<SystemMessage> list;
    private LayoutInflater inflater;

    private final int thumbSizePx, avatarSizePx;
    private final DisplayImageOptions thumbImgOpts, avatarImgOpts;

    public MessageSystemAdapter(Context context, List<SystemMessage> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
        thumbSizePx = context.getResources().getDimensionPixelSize(R.dimen.sys_msg_thumb_size);

        thumbImgOpts = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading)
                .showImageForEmptyUri(R.drawable.loading)
                .showImageOnFail(R.drawable.loading)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        avatarSizePx = context.getResources().getDimensionPixelSize(R.dimen.sys_msg_thumb_size);
        avatarImgOpts = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.head_loading)
                .showImageForEmptyUri(R.drawable.no_avatar)
                .showImageOnFail(R.drawable.no_avatar)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public SystemMessage getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.message_system_item, parent, false);
            holder.title = (TextView) convertView.findViewById(R.id.sys_msg_title);
            holder.desc = (TextView) convertView.findViewById(R.id.sys_msg_desc);
            holder.time = (TextView) convertView.findViewById(R.id.msg_time);
            holder.image = (ImageView) convertView.findViewById(R.id.sys_msg_image);
            holder.avatar = (AvatarView) convertView.findViewById(R.id.avatar);
            holder.btnAction = convertView.findViewById(R.id.btn_msg_act);
            holder.ivReviewScore = (ImageView) convertView.findViewById(R.id.ic_review_stars);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final SystemMessage message = list.get(position);

        holder.title.setVisibility(View.GONE);
        if (ModelUtils.isNotEmpty(message.title)) {
            holder.title.setVisibility(View.VISIBLE);
            holder.title.setText(message.title.trim());
        }

        holder.desc.setVisibility(View.GONE);
        if (ModelUtils.isNotEmpty(message.detail)) {
            holder.desc.setVisibility(View.VISIBLE);
            holder.desc.setText(message.detail.trim());
        }

        // show image or avatar
        holder.image.setVisibility(View.GONE);
        holder.avatar.setVisibility(View.GONE);

        if (ModelUtils.isNotEmpty(message.image)) {
            holder.image.setVisibility(View.VISIBLE);
            showThumb(message.image, holder.image);
        } else {
            holder.avatar.setVisibility(View.VISIBLE);
            holder.avatar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_sys_msg_default));
        }

        convertView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onMsgLongClicked(position, message);
                return true;
            }
        });

        convertView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onMsgClicked(message);
            }
        });
        
        // time & action
        holder.time.setText(TimeUtil.toConciseTime(message.timestamp, true));

        holder.btnAction.setVisibility(hasAction(message) ? View.VISIBLE : View.GONE);
        holder.btnAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onMsgClicked(message);
            }
        });

        // review
        boolean showReview = message.isReview() && message.reviewScore > 0;
        holder.ivReviewScore.setVisibility(showReview ? View.VISIBLE : View.GONE);
        if (showReview) {
            DisplayUtils.showStars(message.reviewScore, holder.ivReviewScore);
        }

        return convertView;
    }

    private boolean hasAction(SystemMessage msg) {
        SystemAction act = msg.getAction();
        return act != null && act != SystemAction.message;
    }

    private void onMsgClicked(SystemMessage msgData) {
        SystemAction action = msgData.getAction();
        if (action == null) {
            return;
        }

        Message msg = new Message();
        msg.what = MessageConstant.SYS_MSG_ACTION;
        msg.obj = msgData;
        EventBus.getDefault().post(msg);
    }

    private void onMsgLongClicked(final int position, final SystemMessage message) {
        final MyDelDialog del = new MyDelDialog(context);
        del.show();
        del.dialog_message.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                list.remove(position);
                del.dismiss();
                Message msg = new Message();
                msg.what = MessageConstant.DELETE_SYSTEM;
                msg.obj = message.id;
                EventBus.getDefault().post(msg);
                MessageSystemAdapter.this.notifyDataSetChanged();
            }
        });
    }

    private void showThumb(String url, ImageView iv) {
        url = CloudHelper.toCropUrl(url, thumbSizePx, thumbSizePx, "fill", null);
        ImageLoader.getInstance().displayImage(url, iv, thumbImgOpts);
    }

    private void showAvatar(String url, ImageView iv) {
        url = CloudHelper.toCropUrl(url, avatarSizePx, avatarSizePx, "thumb,g_face", null);
        ImageLoader.getInstance().displayImage(url, iv, avatarImgOpts);
    }

    class ViewHolder {
        TextView title;
        TextView desc;
        TextView time;
        ImageView image;
        AvatarView avatar;
        View btnAction;
        ImageView ivReviewScore;
    }
}
