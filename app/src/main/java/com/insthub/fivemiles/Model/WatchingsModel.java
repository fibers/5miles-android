package com.insthub.fivemiles.Model;

import java.util.ArrayList;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Toast;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.BeeFramework.model.HttpStatusConsumer;
import com.BeeFramework.model.JSONConsumer;
import com.BeeFramework.model.SimpleCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.Constants;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.CommodityHomeResponse;
import com.insthub.fivemiles.Protocol.HOME_OBJECTS;
import com.insthub.fivemiles.Protocol.META;
import com.nostra13.universalimageloader.utils.L;
import com.thirdrock.fivemiles.util.ModelUtils;

public class WatchingsModel extends BaseModel {

	public ArrayList<HOME_OBJECTS> watchingList = new ArrayList<HOME_OBJECTS>();
	
	public ArrayList<HOME_OBJECTS> sellList = new ArrayList<HOME_OBJECTS>();
    public ArrayList<HOME_OBJECTS> buyingList = new ArrayList<HOME_OBJECTS>();
	public META meta;
	
	public WatchingsModel(Context context) {
		super(context);
	}

    private JSONConsumer watchingJsonConsumer = new JSONConsumer() {
        @Override
        public void consumeJSON(JSONObject jo) throws JSONException {
            CommodityHomeResponse response = new CommodityHomeResponse();
            response.fromJson(jo);
            meta = response.meta;
            watchingList.addAll(response.objects);
        }
    };

	public void getWatchingsList() {
        watchingList.clear();
        BeeCallback<JSONObject> cb = new SimpleCallback(this).jsonConsumer(watchingJsonConsumer);
        cb.url(ApiInterface.USER_LIKES).type(JSONObject.class).header().method(Constants.METHOD_GET);
        ajax(cb);
	}

    public void getWatchingListMore() {
        BeeCallback<JSONObject> cb = new SimpleCallback(this).jsonConsumer(watchingJsonConsumer);
        String url = ApiInterface.USER_LIKES + meta.next;
        cb.url(url).type(JSONObject.class).header().method(Constants.METHOD_GET);
        ajax(cb);
    }

    private JSONConsumer buyingJsonConsumer = new JSONConsumer() {
        @Override
        public void consumeJSON(JSONObject jo) throws JSONException {
            CommodityHomeResponse response = new CommodityHomeResponse();
            response.fromJson(jo);
            meta = response.meta;
            buyingList.addAll(response.objects);
        }
    };

	public void getBuyingList() {
        buyingList.clear();
        BeeCallback<JSONObject> cb = new SimpleCallback(this).jsonConsumer(buyingJsonConsumer);
		cb.url(ApiInterface.MY_BUYINGS).type(JSONObject.class).header().method(Constants.METHOD_GET);
		ajax(cb);
	}

    public void getBuyingListMore() {
        BeeCallback<JSONObject> cb = new SimpleCallback(this).jsonConsumer(buyingJsonConsumer);
        String url = ApiInterface.MY_BUYINGS + meta.next;
        cb.url(url).type(JSONObject.class).header().method(Constants.METHOD_GET);
        ajax(cb);
    }

    private JSONConsumer sellJsonConsumer = new JSONConsumer() {
        @Override
        public void consumeJSON(JSONObject jo) throws JSONException {
            CommodityHomeResponse response = new CommodityHomeResponse();
            response.fromJson(jo);
            meta = response.meta;
            sellList.addAll(response.objects);
        }
    };

	public void getSellList() {
        sellList.clear();
        BeeCallback<JSONObject> cb = new SimpleCallback(this).jsonConsumer(sellJsonConsumer);
		cb.url(ApiInterface.MY_SELLINGS).type(JSONObject.class).header().method(Constants.METHOD_GET);
		ajax(cb);
	}
	
	public void getSellListMore() {
        BeeCallback<JSONObject> cb = new SimpleCallback(this).jsonConsumer(sellJsonConsumer);
        String url = ApiInterface.MY_SELLINGS + meta.next;
		cb.url(url).type(JSONObject.class).header().method(Constants.METHOD_GET);
		ajax(cb);
	}

    public void deleteSellItem(final int itemIndex) {
        BeeCallback<JSONObject> cb = new SimpleCallback(this).statusConsumer(
        new HttpStatusConsumer() {
            @Override
            public void consumeHttpStatus(String url, AjaxStatus status) {
                if (status.getCode() == HttpStatus.SC_OK) {
                    Toast.makeText(getContext(), "Item deleted.", Toast.LENGTH_LONG).show();
                    try {
                        sellList.remove(itemIndex);
                        WatchingsModel.this.OnMessageResponse(url, new JSONObject(), status);
                    }
                    catch (Exception e) {
                        L.e(e);
                    }
                }
            }
        });
        HOME_OBJECTS item = sellList.get(itemIndex);
        cb.url(ApiInterface.DELETE_ITEM).type(JSONObject.class).header().param("item_id", item.id).method(Constants.METHOD_POST);
        ajaxProgress(cb);
    }
}
