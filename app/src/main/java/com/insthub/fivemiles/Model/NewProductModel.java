package com.insthub.fivemiles.Model;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.Constants;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.IMAGES;
import com.insthub.fivemiles.Utils.LocationManagerUtil;
import com.thirdrock.fivemiles.framework.service.CloudUploadService;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.concurrent.service.MainThreadResultReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NewProductModel extends BaseModel {

//    public static final int UPLOAD_PREPARED = 0;
//    public static final int UPLOADING = 1;
//    public static final int UPLOAD_SUCESSED = 2;
//    public static final int UPLOAD_FAILED = 3;

    public ArrayList<String> codeList = new ArrayList<String>();

    public NewProductModel(Context context) {
        super(context);
    }

    public void getCurrencyCode() {
        codeList.clear();
        codeList.add("ARS");
        codeList.add("AUD");
        codeList.add("BDT");
        codeList.add("BRL");
        codeList.add("BYR");
        codeList.add("CAD");
        codeList.add("CLP");
        codeList.add("CNY");
        codeList.add("EGP");
        codeList.add("EUR");
        codeList.add("GBP");
//        codeList.add("EUR");
        codeList.add("IDR");
        codeList.add("INR");
        codeList.add("IRR");
//        codeList.add("EUR");
        codeList.add("MXN");
        codeList.add("MYR");
        codeList.add("NGN");
        codeList.add("NZD");
        codeList.add("PHP");
        codeList.add("PKR");
        codeList.add("RUB");
//        codeList.add("EUR");
        codeList.add("THB");
        codeList.add("TRY");
        codeList.add("UAH");
        codeList.add("USD");
        codeList.add("VND");
        codeList.add("ZAR");

    }

    public interface OnPreUploaderFinishedListener {
        public void onUploadSuccess(String file);

        public void onUploadFailure();
    }

    public ProductImagePreUploader getProductImagePreUploaderInstance() {
        return new ProductImagePreUploader();
    }

    public ProductVoiceUploader getProductVoiceUploaderInstance() {
        return new ProductVoiceUploader();
    }

    /**
     * 预上载商品图片
     */
    public class ProductImagePreUploader {
//        @Deprecated
//        public JSONArray imgs;  // imageList of uploaded image data
        private ImagePreUploadResultReceiver mImagePreUploadResultReceiver;

        public ProductImagePreUploader() {
            mImagePreUploadResultReceiver = new ImagePreUploadResultReceiver();
//            this.imgs = new JSONArray();
        }

        public Map<String, IMAGES> mUploadedImages = new HashMap<String, IMAGES>();
        public Map<String, String> mImages = new HashMap<String, String>();

        public OnPreUploaderFinishedListener mOnImagePreUploaderFinishedListener;

        public void setOnPreUploaderFinishedListener(OnPreUploaderFinishedListener mOnPreUploaderFinishedListener) {
            this.mOnImagePreUploaderFinishedListener = mOnPreUploaderFinishedListener;
        }

        public void addImage(String file) {
            mImages.put(file, file);
//            CloudUploadService.startUploadImage(FiveMilesApp.getInstance(), file, mImagePreUploadResultReceiver);
        }

        public void clear() {
            mImages.clear();
            mUploadedImages.clear();
        }


        public class ImagePreUploadResultReceiver extends MainThreadResultReceiver {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultCode == Activity.RESULT_OK) {
                    L.d("image upload success");
                    IMAGES img = (IMAGES) resultData.getSerializable("image");
                    String file = resultData.getString("file");
                    if (mImages.containsKey(file)) {
                        mUploadedImages.put(file, img);
                    }
                    if (mOnImagePreUploaderFinishedListener != null) {
                        mOnImagePreUploaderFinishedListener.onUploadSuccess(file);
                    }
                } else {
                    L.d("image upload failed");
                    Exception ex = (Exception) resultData.getSerializable(CloudUploadService.EXTRA_EXCEPTION);
                    if (ex != null) {
                        L.e(ex);
                    }
//                    mImages.clear();
//                    mUploadedImages.clear();
                    if (mOnImagePreUploaderFinishedListener != null) {
                        mOnImagePreUploaderFinishedListener.onUploadFailure();
                    }
//                    NewProductModel.this.errorCallback();
//                    Exception ex = (Exception)
//                            resultData.getSerializable(CloudUploadService.EXTRA_EXCEPTION);
//                    Toast.makeText(FiveMilesApp.getInstance(),
//                            ex.getMessage(),
//                            Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    /**
     * 上传新商品
     */
    public void newProductUpload(String title, String description, String fixed_price, String currency_code, int ship_model, int categories, JSONArray imgs, String voiceUrl) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (jo != null) {
                        NewProductModel.this.OnMessageResponse(url, jo, status);
                    } else {
                        NewProductModel.this.OnMessageResponse(url, null, status);
                        NewProductModel.this.errorCallback(url, status);
                    }
                } catch (JSONException e) {
                    L.e(e);
                    try {
                        NewProductModel.this.OnMessageResponse(url, null, null);
                    }
                    catch (JSONException ex) {
                        // ignore
                    }
                    NewProductModel.this.errorCallback();
                }
            }
        };
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("title", title);
        params.put("category", categories);
        params.put("desc", description);
        params.put("price", fixed_price);
        params.put("currency", currency_code);
        params.put("lat", LocationManagerUtil.getLatitudeInSession());
        params.put("lon", LocationManagerUtil.getLongitudeInSession());
        params.put("images", imgs.toString());
        params.put("mediaLink", voiceUrl);
        cb.url(ApiInterface.COMMODITY_UPLOAD).type(JSONObject.class)
                .header().params(params).method(Constants.METHOD_POST);
        ajax(cb);
    }

    public class ProductVoiceUploader {
        public OnPreUploaderFinishedListener mOnVoicePreUploaderFinishedListener;
        private VoicePreUploadResultReceiver mVoicePreUploadResultReceiver;
        public JSONArray voice;
        public Map<String, IMAGES> mUploadedVoices = new HashMap<String, IMAGES>();
        public Map<String, String> mVoices = new HashMap<String, String>();
        public String voiceUrl;

        public ProductVoiceUploader() {
            mVoicePreUploadResultReceiver = new VoicePreUploadResultReceiver();
            this.voice = new JSONArray();
        }

        public void addVoice(String file) {
            mVoices.put(file, file);
//            CloudUploadService.startUploadVoice(FiveMilesApp.getInstance(), file, mVoicePreUploadResultReceiver);
        }

        public void setOnPreUploaderFinishedListener(OnPreUploaderFinishedListener mOnVoicePreUploaderFinishedListener) {
            this.mOnVoicePreUploaderFinishedListener = mOnVoicePreUploaderFinishedListener;
        }

        public void clear() {
            mVoices.clear();
            mUploadedVoices.clear();
        }

        public class VoicePreUploadResultReceiver extends MainThreadResultReceiver {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultCode == Activity.RESULT_OK) {
                    L.d("audio upload success");
                    String url = resultData.getString("url");
                    String file = resultData.getString("file");
                    if (mVoices.containsKey(file)) {
                        if (url != null) {
                            voiceUrl = url;
                        }
                    }
                    if (mOnVoicePreUploaderFinishedListener != null) {
                        mOnVoicePreUploaderFinishedListener.onUploadSuccess(file);
                    }
                } else {
                    L.d("audio upload failed");
                    Exception ex = (Exception) resultData.getSerializable(CloudUploadService.EXTRA_EXCEPTION);
                    if (ex != null) {
                        L.e(ex);
                    }
                    if (mOnVoicePreUploaderFinishedListener != null) {
                        mOnVoicePreUploaderFinishedListener.onUploadFailure();
                    }
//                    NewProductModel.this.errorCallback();
//                    Exception ex = (Exception)
//                            resultData.getSerializable(CloudUploadService.EXTRA_EXCEPTION);
//                    Toast.makeText(FiveMilesApp.getInstance(),
//                            ex.getMessage(),
//                            Toast.LENGTH_SHORT).show();
                }
            }
        }

    }


//    private class ImageUploadResultReceiver extends MainThreadResultReceiver {
//        private String title;
//        private String desc;
//        private String price;
//        private String currency;
//        private int imgCount;
//        private int imgDone;  // number of uploaded images
//        private JSONArray imgs;  // imageList of uploaded image data
//        private BeeCallback<JSONObject> callback;
//
//        public ImageUploadResultReceiver(String title,
//                                         String desc,
//                                         String price,
//                                         String currency,
//                                         int imageCount,
//                                         BeeCallback<JSONObject> cb) {
//            this.title = title;
//            this.desc = desc;
//            this.price = price;
//            this.currency = currency;
//            this.imgCount = imageCount;
//            this.callback = cb;
//            this.imgs = new JSONArray();
//        }
//
//        @Override
//        protected void onReceiveResult(int resultCode, Bundle resultData) {
//            this.imgDone++;
//
//            if (resultCode == Activity.RESULT_OK) {
//                IMAGES img = (IMAGES) resultData.getSerializable("image");
//                if (img != null) {
//                    try {
//                        imgs.put(img.toJson());
//                    } catch (JSONException e) {
//                        L.e("encode image json failed", e);
//                    }
//
//                }
//            } else {
//                // TODO remove toast
////                Exception ex = (Exception)
////                        resultData.getSerializable(CloudUploadService.EXTRA_EXCEPTION);
////                Toast.makeText(FiveMilesApp.getInstance(),
////                        ex.getMessage(),
////                        Toast.LENGTH_SHORT).show();New
//                NewProductModel.this.errorCallback();
//            }
//
//            if (imgDone >= imgCount) {
//                onAllDone();
//            }
//        }
//
//        // when all images are uploaded
//        private void onAllDone() {
//            Map<String, Object> params = new HashMap<String, Object>();
//            params.put("title", title);
//            params.put("description", desc);
//            params.put("price", price);
//            params.put("currency", currency);
//            params.put("lat", LocationManagerUtil.getLatitude());
//            params.put("lon", LocationManagerUtil.getLongitude());
//            params.put("images", imgs.toString());
//
//            callback.url(ApiInterface.COMMODITY_UPLOAD).type(JSONObject.class).header().params(params);
//            ajaxProgress(callback);
//        }
//    }

}
