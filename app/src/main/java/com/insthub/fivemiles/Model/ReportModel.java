package com.insthub.fivemiles.Model;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.Constants;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.di.RepositoryModule;
import com.thirdrock.fivemiles.util.FileUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.SimpleObserver;
import com.thirdrock.protocol.ReportReason;
import com.thirdrock.protocol.ReportReasonsWrapper;
import com.thirdrock.protocol.ReportReasonsWrapper__JsonHelper;
import com.thirdrock.repository.ReportRepository;
import com.thirdrock.repository.impl.BodyParserFactoryImpl;
import com.thirdrock.repository.impl.ReportRepositoryImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observer;
import rx.schedulers.Schedulers;

import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_APP_DATA;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_REASONS_ITEM;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_REASONS_REVIEW;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_REASONS_UESR;
import static com.thirdrock.repository.ReportRepository.REPORT_TYPE_ALL;
import static com.thirdrock.repository.ReportRepository.REPORT_TYPE_ITEM;
import static com.thirdrock.repository.ReportRepository.REPORT_TYPE_REVIEW;
import static com.thirdrock.repository.ReportRepository.REPORT_TYPE_USER;

public class ReportModel extends BaseModel {

    public List<ReportReason> reasonsList = new ArrayList<>();
    private ReportRepository reportRepository;
    private SharedPreferences appDataPrefs;
    private Observer<List<ReportReasonsWrapper>> reasonsObserver
            = new SimpleObserver<List<ReportReasonsWrapper>>() {
        @Override
        public void onNext(List<ReportReasonsWrapper> reasonsWrapperList) {
            if (reasonsWrapperList == null || reasonsWrapperList.isEmpty()) {
                L.w("report reason list should not be null or empty");
                return;
            }

            int type;
            String reasonsWrapperStr;
            SharedPreferences.Editor prefsEditor = FiveMilesApp.getInstance()
                    .getSharedPreferences(PREFS_APP_DATA, Context.MODE_PRIVATE).edit();
            for (ReportReasonsWrapper reportReasonsWrapper : reasonsWrapperList) {
                if (reportReasonsWrapper != null) {

                    try {
                        reasonsWrapperStr =
                                ReportReasonsWrapper__JsonHelper.serializeToJson(reportReasonsWrapper);
                    } catch (IOException e) {
                        L.e("error parsing ReportReasonsWrapper object to String");
                        break;
                    }

                    type = reportReasonsWrapper.getType();
                    switch (type) {
                        case REPORT_TYPE_ITEM:
                            prefsEditor.putString(PREF_KEY_REASONS_ITEM, reasonsWrapperStr).apply();
                            break;
                        case REPORT_TYPE_USER:
                            prefsEditor.putString(PREF_KEY_REASONS_UESR, reasonsWrapperStr).apply();
                            break;
                        case REPORT_TYPE_REVIEW:
                            prefsEditor.putString(PREF_KEY_REASONS_REVIEW, reasonsWrapperStr).apply();
                            break;
                        default:
                            L.w("should not reach here, type is: " + type);
                            break;
                    }
                }
            }
        }
    };

    public ReportModel(Context context) {
        super(context);
        reportRepository = new ReportRepositoryImpl(
                RepositoryModule.getBaseUrl(),
                RepositoryModule.getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }

    /**
     * 获取举报订单理由
     */
    public void getItemReasons() {
        getReportReasons(REPORT_TYPE_ITEM);
    }

    /**
     * 获取举报用户理由
     */
    public void getUserReasons() {
        getReportReasons(REPORT_TYPE_USER);
    }

    /**
     * 获取举报review理由
     */
    public void getReviewReasons() {
        getReportReasons(REPORT_TYPE_REVIEW);
    }

    private void getReportReasons(int type) {
        try {
            boolean noCachedReportReasons = !hasCachedReportReasons(type);
            if (noCachedReportReasons) {
                cacheLocalReportReasons(type);
            }

            loadReportReasonsFromCache(type);
            OnMessageResponse(ApiInterface.REPORT_REASONS, null, null);
            getRemoteReasonsInBg(type); // TODO 和Category一起，放到一个后台服务中升级
        } catch (JSONException e) {
            L.e(e);
        }
    }

    private void loadReportReasonsFromCache(int type) {
        switch (type) {
            case REPORT_TYPE_ITEM:
                String itemReasonsWrapperStr = appDataPrefs.getString(PREF_KEY_REASONS_ITEM, "");
                parseReasonsFromWrapperStr(itemReasonsWrapperStr);
                break;
            case REPORT_TYPE_USER:
                String userReasonsWrapperStr = appDataPrefs.getString(PREF_KEY_REASONS_UESR, "");
                parseReasonsFromWrapperStr(userReasonsWrapperStr);
                break;
            case REPORT_TYPE_REVIEW:
                String reviewReasonsWrapperStr = appDataPrefs.getString(PREF_KEY_REASONS_REVIEW, "");
                parseReasonsFromWrapperStr(reviewReasonsWrapperStr);
                break;
            default:
                L.w("unrecognized report reason type: " + type);
                break;
        }
    }

    private void getRemoteReasonsInBg(int type) {
        switch (type) {
            case REPORT_TYPE_ITEM:
                reportRepository.getRemoteReportReasonsItem()
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.newThread())
                        .subscribe(reasonsObserver);
                break;
            case REPORT_TYPE_USER:
                reportRepository.getRemoteReportReasonsUser()
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.newThread())
                        .subscribe(reasonsObserver);
                break;
            case REPORT_TYPE_REVIEW:
                reportRepository.getRemoteReportReasonsReview()
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.newThread())
                        .subscribe(reasonsObserver);
                break;
            case REPORT_TYPE_ALL:
                reportRepository.getRemoteReportReasons()
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.newThread())
                        .subscribe(reasonsObserver);
                break;
            default:
                L.w("should not reach here, and resaon type is: " + type);
                break;
        }
    }

    private void cacheLocalReportReasons(int type) {
        Resources resources = FiveMilesApp.getInstance().getResources();
        String localReasonStr = null;
        try {
            switch (type) {
                case REPORT_TYPE_ITEM:
                    localReasonStr =
                            FileUtils.readFileContent(resources.openRawResource(R.raw.report_reasons_item));
                    saveReportReasonsInPrefs(PREF_KEY_REASONS_ITEM, localReasonStr);
                    break;
                case REPORT_TYPE_USER:
                    localReasonStr =
                            FileUtils.readFileContent(resources.openRawResource(R.raw.report_reasons_user));
                    saveReportReasonsInPrefs(PREF_KEY_REASONS_UESR, localReasonStr);
                    break;
                case REPORT_TYPE_REVIEW:
                    localReasonStr =
                            FileUtils.readFileContent(resources.openRawResource(R.raw.report_reasons_review));
                    saveReportReasonsInPrefs(PREF_KEY_REASONS_REVIEW, localReasonStr);
                    break;
                default:
                    L.w("should not reach here in cacheLocalReportReasons()");
                    break;
            }
        } catch (IOException e) {
            L.e("load raw categories failed(local)");
        }
    }

    private void saveReportReasonsInPrefs(String key, String reason) {
        if (key != null) {
            appDataPrefs.edit().putString(key, reason).apply();
        }

    }

    private void parseReasonsFromWrapperStr(String wrapperStr) {
        if (wrapperStr == null || ModelUtils.isEmpty(wrapperStr)) {
            L.w("report reasons wrapper string should not be null or empty");
            return;
        }

        try {
            ReportReasonsWrapper wrapper = ReportReasonsWrapper__JsonHelper.parseFromJson(wrapperStr);
            parseReasonsFromWrapper(wrapper);
        } catch (IOException e) {
            L.e("exception stack trace: " + e);
        }
    }

    private void parseReasonsFromWrapper(ReportReasonsWrapper wrapper) {
        reasonsList.clear();
        if (wrapper != null) {
            List<ReportReason> reportReasonsList = wrapper.getReportReasonList();
            for (ReportReason reason : reportReasonsList) {
                reasonsList.add(reason);
            }
        }
    }

    private boolean hasCachedReportReasons(int type) {
        appDataPrefs = FiveMilesApp.getInstance().getSharedPreferences(PREFS_APP_DATA, Context.MODE_PRIVATE);
        if (appDataPrefs == null) {
            return false;
        }

        switch (type) {
            case REPORT_TYPE_ITEM:
                return appDataPrefs.contains(PREF_KEY_REASONS_ITEM);
            case REPORT_TYPE_USER:
                return appDataPrefs.contains(PREF_KEY_REASONS_UESR);
            case REPORT_TYPE_REVIEW:
                return appDataPrefs.contains(PREF_KEY_REASONS_REVIEW);
            default:
                return false;
        }
    }

    /**
     * 举报商品
     */
    public void reportItem(String itemId, int reason, String comment) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (status.getCode() == 200) {
                        ReportModel.this.OnMessageResponse(url, jo, status);
                    } else {
                        ReportModel.this.OnMessageResponse(url, null, status);
                        ReportModel.this.errorCallback(url, status);
                    }
                } catch (JSONException e) {
                    L.e(e);
                }
            }
        };

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("item_id", itemId);
        params.put("reason", reason);
        params.put("reason_content", comment);

        cb.url(ApiInterface.REPORT_ITEM).type(JSONObject.class).header().params(params).method(Constants.METHOD_POST);
        ajaxProgress(cb);
    }

    /**
     * 举报用户
     */
    public void reportUser(String userId, int reason, String comment) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (status.getCode() == 200) {
                        ReportModel.this.OnMessageResponse(url, jo, status);
                    } else {
                        ReportModel.this.OnMessageResponse(url, null, status);
                        ReportModel.this.errorCallback(url, status);
                    }
                } catch (JSONException e) {
                    L.e(e);
                }
            }
        };

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user_id", userId);
        params.put("reason", reason);
        params.put("reason_content", comment);

        cb.url(ApiInterface.REPORT_USER).type(JSONObject.class).header().params(params).method(Constants.METHOD_POST);
        ajaxProgress(cb);
    }

    public void reportReview(String reviewId, int reasonId, String msg) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (status.getCode() == 200) {
                        ReportModel.this.OnMessageResponse(url, jo, status);
                    } else {
                        ReportModel.this.OnMessageResponse(url, null, status);
                        ReportModel.this.errorCallback(url, status);
                    }
                } catch (JSONException e) {
                    L.e(e);
                }
            }
        };

        Map<String, Object> params = new HashMap<>();
        params.put("review_id", reviewId);
        params.put("reason", reasonId);
        params.put("reason_content", msg);

        cb.url(ApiInterface.REPORT_REVIEW).type(JSONObject.class).header().params(params).method(Constants.METHOD_POST);
        ajaxProgress(cb);
    }
}
