package com.insthub.fivemiles.Model;

import android.app.Activity;
import android.content.Context;
import android.os.Build;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.BeeFramework.view.MyProgressDialog;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.Constants;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.EditProfileResponse;
import com.insthub.fivemiles.Protocol.SignupResponse;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.domain.GeoLocation;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.di.RepositoryModule;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.CloudSignature;
import com.thirdrock.repository.ItemRepository;
import com.thirdrock.repository.impl.BodyParserFactoryImpl;
import com.thirdrock.repository.impl.ItemRepositoryImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.GETUI_PROVIDER;
import static com.insthub.fivemiles.FiveMilesAppConst.PARSE_PROVIDER;
import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLatitudeInSession;
import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLongitudeInSession;

public class LoginModel extends BaseModel {

    private static final String FIELD_AF_APP_USER_ID = "appsflyer_user_id";

	public SignupResponse signupResp;

    private ItemRepository itemRepository;

	public LoginModel(Context context) {
		super(context);
        itemRepository = new ItemRepositoryImpl(
                RepositoryModule.getBaseUrl(),
                RepositoryModule.getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
	}

    /**
     * Sign up a new user account by email
     * @param name nickname
     * @param email email address
     * @param password password
     */
    public void signupEmail(String name, String email, String password, GeoLocation geolocation) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (jo == null && status.getCode() != 200) {
                        LoginModel.this.OnMessageResponse(url, null, status);
                        L.e("signupEmail failed status: %s %s %s", status.getCode(),
                                status.getMessage(), status.getError());
                        LoginModel.this.errorCallback(url, status);
                        return;
                    }

                    signupResp = new SignupResponse();
                    signupResp.fromJson(jo);
                    LoginModel.this.OnMessageResponse(url, jo, status);
                } catch (JSONException e) {
                    L.e("signup email failed", e);
                }
            }
        };

        Map<String, String> params = new HashMap<String, String>();
        params.put("nickname", name);
        params.put("email", email);
        params.put("password", password);
        if(geolocation != null){
            geolocation.setParams(params);
        }else{
            // 上传当前有可用位置（如果有的话），此处不发起位置更新，避免某些用户反感
            params.put("lat", "" + getLatitudeInSession());
            params.put("lon", "" + getLongitudeInSession());
        }
        params.put(FIELD_AF_APP_USER_ID, FiveMilesApp.getAndroidId());
        TimeZone t =  TimeZone.getDefault();

        String timezoneString = t.getID();
        params.put("timezone",timezoneString);


        // 上传当前有可用位置（如果有的话），此处不发起位置更新，避免某些用户反感
        params.put("lat", "" + getLatitudeInSession());
        params.put("lon", "" + getLongitudeInSession());
        if (Build.SERIAL != null) {
            params.put("device_id", Build.SERIAL);      // FIXME 将device_id提取为一个常量
        }

//        if (SESSION.getInstance().anonyUserId != null) {
//            params.put("anonymous_user_id", SESSION.getInstance().anonyUserId);
//        }

        cb.url(ApiInterface.SIGNUP_EMAIL).type(JSONObject.class).
                noTokenHeader().params(params).method(Constants.METHOD_POST);
        ajaxProgress(cb, mContext.getString(R.string.join));
    }

    /**
     * Log in with email/password
     * @param email email address
     * @param password password
     */
    public void loginEmail(String email, String password) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (jo == null && status.getCode() != 200) {
                        LoginModel.this.OnMessageResponse(url, null, status);
                        L.e("loginEmail failed status: %s %s %s", status.getCode(),
                                status.getMessage(), status.getError());
                        LoginModel.this.errorCallback(url, status);
                        LoginModel.this.OnMessageResponse(url, null, status);
                        return;
                    }

                    signupResp = new SignupResponse();
                    signupResp.fromJson(jo);
                    LoginModel.this.OnMessageResponse(url, jo, status);
                } catch (JSONException e) {
                    L.e("login email failed", e);
                }
            }
        };

        Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        params.put("password", password);
        params.put(FIELD_AF_APP_USER_ID, FiveMilesApp.getAndroidId());

        // 上传当前有可用位置（如果有的话），此处不发起位置更新，避免某些用户反感
        params.put("lat", "" + getLatitudeInSession());
        params.put("lon", "" + getLongitudeInSession());

        cb.url(ApiInterface.LOGIN_EMAIL).type(JSONObject.class).
                noTokenHeader().params(params).method(Constants.METHOD_POST);
        ajaxProgress(cb, mContext.getString(R.string.action_sign_in_short));
    }

    /**
     * Login with Facebook
     * @param fbUid Facebook user id
     * @param name Facebook username
     * @param token Facebook api access token
     */
    public void loginFacebook(String fbUid, String name, String token, long expires,GeoLocation geolocation) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (jo == null && status.getCode() != 200) {
                        LoginModel.this.OnMessageResponse(url, null, status);
                        L.e("loginFacebook failed status: %s %s %s", status.getCode(),
                                status.getMessage(), status.getError());
                        LoginModel.this.errorCallback(url, status);
                        LoginModel.this.OnMessageResponse(url, null, status);
                        return;
                    }

                    signupResp = new SignupResponse();
                    signupResp.fromJson(jo);
                    LoginModel.this.OnMessageResponse(url, jo, status);
                } catch (JSONException e) {
                    L.e("login facebook failed", e);
                }
            }
        };

        Map<String, String> params = new HashMap<>();
        params.put("fb_userid", fbUid);
        params.put("fb_username", name);
        params.put("fb_token", token);
        params.put("fb_token_expires", String.valueOf(expires / 1000));
        params.put("fb_headimage", CloudHelper.IMG_CLOUD_PREFIX + "facebook/" + fbUid + ".jpg");

        if(geolocation != null){
            geolocation.setParams(params);
        }else{
            // 上传当前有可用位置（如果有的话），此处不发起位置更新，避免某些用户反感
            params.put("lat", "" + getLatitudeInSession());
            params.put("lon", "" + getLongitudeInSession());
        }
        params.put(FIELD_AF_APP_USER_ID, FiveMilesApp.getAndroidId());
        TimeZone t =  TimeZone.getDefault();
        String timezoneString = t.getID();
        params.put("timezone",timezoneString);



        if (Build.SERIAL != null) {
            params.put("device_id", Build.SERIAL);
        }

        cb.url(ApiInterface.LOGIN_FACEBOOK).type(JSONObject.class).
                noTokenHeader().params(params).method(Constants.METHOD_POST);
        ajax(cb);
    }

    public void linkFacebook(String fbUid, String token, Date expires) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (jo == null && status.getCode() != 201) {
                        LoginModel.this.OnMessageResponse(url, null, status);
                        L.e("linkFacebook failed status: %s %s %s", status.getCode(),
                                status.getMessage(), status.getError());
                        LoginModel.this.errorCallback(url, status);
                        LoginModel.this.OnMessageResponse(url, null, status);
                        return;
                    }

                    LoginModel.this.OnMessageResponse(url, jo, status);         // 成功
                } catch (JSONException e) {
                    L.e("link facebook failed", e);
                }
            }
        };

        Map<String, String> params = new HashMap<>();
        params.put(ACT_PARA_USER_ID, SESSION.getInstance().userId);
        params.put("fb_userid", fbUid);
        params.put("fb_token", token);
        params.put("fb_expire", String.valueOf(expires.getTime() / 1000));

        cb.url(ApiInterface.LINK_FACEBOOK).type(JSONObject.class).
                header().params(params).method(Constants.METHOD_POST);
        ajax(cb);
    }

    public void unlinkFacebook() {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (jo == null && status.getCode() != 200) {
                        LoginModel.this.OnMessageResponse(url, null, status);
                        L.e("unlinkFacebook failed status: %s %s %s", status.getCode(),
                                status.getMessage(), status.getError());
                        LoginModel.this.errorCallback(url, status);
                        LoginModel.this.OnMessageResponse(url, null, status);
                        return;
                    }

                    LoginModel.this.OnMessageResponse(url, jo, status);
                } catch (JSONException e) {
                    L.e("unlink facebook failed", e);
                }
            }
        };

        cb.url(ApiInterface.UNLINK_FACEBOOK).type(JSONObject.class).
                header().method(Constants.METHOD_POST);
        ajax(cb);
    }

    public void fillProfile(String avatarUrl, String nickname) {
        if (ModelUtils.isNotEmpty(avatarUrl)) {
            avatarUrl = avatarUrl.trim();
        }

//        L.d("fill profile with url: " + avatarUrl + " nickname: " + nickname);
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                L.d("status: " + status.getCode() + ", message: " + status.getMessage());
                try {
                    if(jo != null) {
                        EditProfileResponse editProfileResponse = new EditProfileResponse();
                        editProfileResponse.fromJson(jo);
                        LoginModel.this.OnMessageResponse(url,jo,status);

                        if (!ModelUtils.isEmpty(editProfileResponse.error)) {
                            LoginModel.this.errorCallback(url, editProfileResponse.error, editProfileResponse.error_description);
                        }
                    }
                    else {
                        LoginModel.this.OnMessageResponse(url, null, status);
                        LoginModel.this.callback(this, url, null, status);
                    }
                } catch (JSONException e) {
                    L.e(e);
                }
            }
        };
        Map<String, Object> params = new HashMap<String, Object>();
        if (ModelUtils.isNotEmpty(avatarUrl)) {
            params.put("portrait", avatarUrl);
        }

        if (ModelUtils.isNotNull(nickname)) {
            params.put("nickname", nickname);
        }
        cb.url(ApiInterface.USER_EDIT_PROFILE).type(JSONObject.class).header().params(params).method(Constants.METHOD_POST);
        ajaxProgress(cb);
    }

    // upload avatar before fill profile
    public void fillProfileAvatar(Activity activity, final File avatar) {
        if (avatar == null || !avatar.exists()) {
            fillProfile(null, "");
            return;
        }

        final MyProgressDialog prog = new MyProgressDialog(activity, activity.getString(R.string.msg_loading));
        prog.show();

        itemRepository.signCloudUpload().flatMap(new Func1<CloudSignature, Observable<Map>>() {
            @Override
            public Observable<Map> call(CloudSignature signature) {
                return itemRepository.uploadToCloud(getContext(),
                        avatar.getAbsolutePath(),
                        signature);
            }
        }).map(new Func1<Map, String>() {
            @Override
            public String call(Map resultMap) {
                String imageUrl = "";
                if (resultMap != null && resultMap.containsKey("url")) {
                    imageUrl = (String) resultMap.get("url");
                }
                return imageUrl;
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        L.e(e);

                        prog.dismiss();
                        LoginModel.this.errorCallback();

                        try {
                            OnMessageResponse(ApiInterface.USER_EDIT_PROFILE, null, null);
                        } catch (JSONException je) {
                            L.e(je);
                        }
                    }

                    @Override
                    public void onNext(String url) {
                        prog.dismiss();

                        if (ModelUtils.isNotEmpty(url)) {
                            fillProfile(url, "");
                            return;
                        }

                        try {
                            OnMessageResponse(ApiInterface.USER_EDIT_PROFILE, null, new AjaxStatus(200, "OK"));
                        } catch (JSONException e) {
                            L.e(e);
                        }
                    }
                });
    }

    /**
     * 若没有token，则不会给服务器发送请求。例如在用户退出登陆之后，匿名登录之前，这段时间没有token。
     * 个推的client id也被用作installationId
     * @param provider 个推或parse
     * @param installationId 个推的client id或parse的installation id
     */
    public void registerDevice(final String provider, String installationId) {
        boolean tokenNotExist = ModelUtils.isEmpty(SESSION.getInstance().token);
        if (tokenNotExist) {
            L.d("token not exist, abandon register device");
            return;     // 没有token，请求register_device会报401错误
        }

        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (status.getCode() == 200) {
                    if (GETUI_PROVIDER.equals(provider)) {
                        SESSION.getInstance().isGeTuiRegistered = true;
                        L.d(GETUI_PROVIDER + " is registered");
                    }
                    if (PARSE_PROVIDER.equals(provider)) {
                        SESSION.getInstance().isParseRegistered = true;
                        L.d(PARSE_PROVIDER + " is registered");
                    }

                    try {
                        LoginModel.this.OnMessageResponse(url, jo, status);
                    } catch (JSONException e) {
                        L.e(e);
                    }
                } else {
                    LoginModel.this.callback(this, url, null, status);
                }
            }
        };


        Map<String, Object> params = new HashMap<String, Object>();
        params.put("provider", provider);
        params.put("installation_id", installationId);
        params.put("device_id", Build.SERIAL);
        params.put("push_token", "");       // GCM Registration ID
        cb.url(ApiInterface.REGISTER_DEVICE).type(JSONObject.class)
                .header().params(params).background().method(Constants.METHOD_POST);
        ajax(cb);
    }

    public void resetPassword(String email) {
        BeeCallback<String> cb = new BeeCallback<String>() {

            @Override
            public void callback(String url, String body, AjaxStatus status) {
                try {
                    if (body == null && status.getCode() != 200) {
                        LoginModel.this.OnMessageResponse(url, null, status);
                        L.e("resetPassword failed: %s %s %s", status.getCode(),
                                status.getMessage(), status.getError());
                        LoginModel.this.errorCallback(url, status);
                        return;
                    }

                    LoginModel.this.OnMessageResponse(url, new JSONObject(), status);
                } catch (JSONException e) {
                    L.e("resetPassword failed", e);
                }
            }
        };

        Map<String, String> params = new HashMap();
        params.put("email", email);
        cb.url(ApiInterface.RESET_PASSWORD).type(String.class).
                params(params).header().method(Constants.METHOD_GET);
        ajaxProgress(cb);
    }
}
