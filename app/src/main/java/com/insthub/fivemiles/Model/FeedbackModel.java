package com.insthub.fivemiles.Model;

import android.content.Context;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.Constants;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.FEEDBACK;
import com.insthub.fivemiles.Protocol.FeedbackResponse;
import com.insthub.fivemiles.Protocol.META;
import com.thirdrock.fivemiles.util.ModelUtils;

import com.thirdrock.framework.util.L;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FeedbackModel extends BaseModel {

	public ArrayList<FEEDBACK> feedbackList = new ArrayList<FEEDBACK>();
    public META meta;

	public FeedbackModel(Context context) {
		super(context);
	}
	
	public void feedback(String item_id, int rate_num, String comment) {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (jo == null)
                    jo = new JSONObject();
				try {
					if (status.getCode() == 201) {
						FeedbackModel.this.OnMessageResponse(url, jo, status);
					} else {
						FeedbackModel.this.errorCallback(url, status);
					}
				} catch (JSONException e) {
                    L.e(e);
                    FeedbackModel.this.errorCallback();
				}
			}
		};

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("item_id", item_id);
		params.put("score", rate_num);
		params.put("text", ModelUtils.trim(comment));
		
		cb.url(ApiInterface.FEEDBACK).type(JSONObject.class).header().
                params(params).method(Constants.METHOD_POST);
		ajaxProgress(cb);
	}
	
	public void getFeedbackList(String userId) {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {
			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (jo == null)
                    jo = new JSONObject();
				try {
					feedbackList.clear();
                    FeedbackResponse response = new FeedbackResponse();
                    response.fromJson(jo);
                    meta = response.meta;
                    feedbackList.addAll(response.objects);
					FeedbackModel.this.OnMessageResponse(url, null, status);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		};
		
		cb.url(ApiInterface.USER_FEEDBACKS).type(JSONObject.class).header().param("seller_id", userId).method(Constants.METHOD_GET);
		ajax(cb);
	}

    public void getFeedbackListMore(String userId) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (jo == null)
                    jo = new JSONObject();
                try {
                    FeedbackResponse response = new FeedbackResponse();
                    response.fromJson(jo);
                    meta = response.meta;
                    feedbackList.addAll(response.objects);
                    FeedbackModel.this.OnMessageResponse(url, null, status);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        String url = ApiInterface.MY_BUYINGS + meta.next;
        cb.url(url).type(JSONObject.class).header().param("seller_id", userId).method(Constants.METHOD_GET);
        ajax(cb);
    }

}
