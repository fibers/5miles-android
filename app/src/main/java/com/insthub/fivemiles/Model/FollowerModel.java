package com.insthub.fivemiles.Model;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.Constants;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.META;
import com.insthub.fivemiles.Protocol.USER;
import com.insthub.fivemiles.Protocol.UserResponse;

public class FollowerModel extends BaseModel {

	public ArrayList<USER> userList = new ArrayList<USER>();
	public META meta;
	public FollowerModel(Context context) {
		super(context);
	}
	
	public void getFollowerList(String userId) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try {
                        FollowerModel.this.callback(this, url, jo, status);
                        if (jo != null) {
                            UserResponse response = new UserResponse();
                            response.fromJson(jo);
                            meta = response.meta;
                            userList.clear();
                            userList.addAll(response.objects);
                            FollowerModel.this.OnMessageResponse(url, jo, status);
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        FollowerModel.this.errorCallback();
                    }
                } else {
                    FollowerModel.this.errorCallback(url, status);
                }
            }
        };

        cb.url(ApiInterface.USER_FOLLOWERS).type(JSONObject.class).header().param("user_id", userId).method(Constants.METHOD_GET);
        ajax(cb);
    }
	
	public void getFollowerListMore(String userId) {

        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try {
                        FollowerModel.this.callback(this, url, jo, status);
                        if (jo != null) {
                            UserResponse response = new UserResponse();
                            response.fromJson(jo);
                            meta = response.meta;
                            userList.addAll(response.objects);
                            FollowerModel.this.OnMessageResponse(url, jo, status);
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        FollowerModel.this.errorCallback();
                    }
                } else {
                    FollowerModel.this.errorCallback(url, status);
                }
            }
        };

        cb.url(ApiInterface.USER_FOLLOWERS + meta.next).type(JSONObject.class).header().param("user_id", userId).method(Constants.METHOD_GET);
        ajax(cb);
	}
	
	
}
