package com.insthub.fivemiles.Model;

import android.app.Activity;
import android.content.Context;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.external.androidquery.callback.AbstractAjaxCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.AQUtility;
import com.external.androidquery.util.Constants;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.CommodityHomeResponse;
import com.insthub.fivemiles.Protocol.HOME_OBJECTS;
import com.insthub.fivemiles.Protocol.META;
import com.insthub.fivemiles.Protocol.USER_DETAIL;
import com.insthub.fivemiles.Protocol.USER_INFO;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.fivemiles.di.RepositoryModule;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.SimpleObserver;
import com.thirdrock.repository.UserRepository;
import com.thirdrock.repository.impl.BodyParserFactoryImpl;
import com.thirdrock.repository.impl.UserRepositoryImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import rx.Subscription;
import rx.android.observables.AndroidObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class ProfileModel extends BaseModel {

    // public USER_PROFILE user_profile;
    public USER_INFO user_info;         // 个人页上看到的“我自己”的统计信息
    public USER_DETAIL user_detail;     // 他人页上看到的他人的统计信息

    public ArrayList<HOME_OBJECTS> dataList = new ArrayList<HOME_OBJECTS>();
    public META meta;

    private boolean isSendingMail;
    public boolean userBlocked;
    private UserRepository userRepo;

    public ProfileModel(Context context) {
        super(context);
        userRepo = new UserRepositoryImpl(
                RepositoryModule.getBaseUrl(),
                RepositoryModule.getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }

    public String getMetaNext() {
        if (meta != null)
            return meta.next;
        return "null";
    }

    /**
     * @param initialized 如果为true，不显示滚动条，出错时也不报错
     */
    public void getMeInfo(final boolean initialized) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try {
                        ProfileModel.this.callback(this, url, jo, status);
                        user_info = new USER_INFO();
                        user_info.fromJson(jo);

                        // 更新session中的认证信息
                        SESSION.getInstance()
                                .fbVerified(user_info.isFacebookVerified)
                                .emlVerified(user_info.isEmailVerified)
                                .phoneVerified(user_info.isPhoneVerified)
                                .save();

                        ProfileModel.this.OnMessageResponse(url, jo, status);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        ProfileModel.this.errorCallback();
                    }

                } else {
                    if (!initialized)
                        ProfileModel.this.errorCallback(url, status);
                }
            }
        };

        AQUtility.debug("headers: " + AbstractAjaxCallback.getDefaultHeaders());
        cb.url(ApiInterface.ME_INFO).type(JSONObject.class).header().method(Constants.METHOD_GET);
        if (initialized)
            ajax(cb);
        else
            ajaxProgress(cb);
    }

    public void getUserDetail(final String userId) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try {
                        ProfileModel.this.callback(this, url, jo, status);
                        user_detail = new USER_DETAIL();
                        user_detail.fromJson(jo);
                        user_detail.userInfo.id = userId;
                        userBlocked = user_detail.userBlocked;
                        ProfileModel.this.OnMessageResponse(url, jo, status);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        ProfileModel.this.errorCallback();
                    }
                } else {
                    try {
                        if (status.getRestErrorCode() != FiveMilesAppConst.ERR_BLOCKED) {
                            ProfileModel.this.errorCallback(url, status);
                        }

                        ProfileModel.this.OnMessageResponse(url, jo, status);
                    } catch (JSONException e) {
                        L.e(e);
                    }
                }
            }
        };

        cb.url(ApiInterface.USER_DETAIL).type(JSONObject.class).header().param("user_id", userId).method(Constants.METHOD_GET);
        ajax(cb);
    }


//	public void getUserStats(String userId) {
//
//		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {
//
//			@Override
//			public void callback(String url, JSONObject jo, AjaxStatus status) {
//				if (status.getCode() == 200) {
//					try {
//						ProfileModel.this.callback(this, url, jo, status);
//						user_stats = new USER_INFO();
//						user_stats.fromJson(jo);
//						ProfileModel.this.OnMessageResponse(url, jo, status);
//					} catch (JSONException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				} else {
//					ProfileModel.this.errorCallback();
//				}
//			}
//		};
//
//		cb.url(ApiInterface.USER_INFO + userId + "/stats/").type(JSONObject.class).header().method(Constants.METHOD_GET);
//		// ajaxProgress(cb);
//
//        MockServer.ajax(cb);
//
//	}

    public void getUserItems(String userId) {

        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try {
                        if (jo != null) {
                            CommodityHomeResponse response = new CommodityHomeResponse();
                            response.fromJson(jo);
                            if (ModelUtils.isEmpty(response.error)) {
                                meta = response.meta;
                                dataList.clear();
                                dataList.addAll(response.objects);
                                ProfileModel.this.OnMessageResponse(url, jo, status);
                            } else {
                                ProfileModel.this.errorCallback(url, response.error, response.error_description);
                            }
                        } else {
                            ProfileModel.this.callback(this, url, null, status);
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else {
                    ProfileModel.this.callback(this, url, null, status);
                }
            }
        };

        cb.url(ApiInterface.USER_SELLINGS).type(JSONObject.class).header().param("seller_id", userId).method(Constants.METHOD_GET);
        ajax(cb);
    }

    public void getUserItemsMore(String userId) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try {
                        if (jo != null) {
                            CommodityHomeResponse response = new CommodityHomeResponse();
                            response.fromJson(jo);
                            if (ModelUtils.isEmpty(response.error)) {
                                meta = response.meta;
                                dataList.addAll(response.objects);
                                ProfileModel.this.OnMessageResponse(url, jo, status);
                            } else {
                                ProfileModel.this.errorCallback(url, response.error, response.error_description);
                            }
                        } else {
                            ProfileModel.this.callback(this, url, null, status);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    ProfileModel.this.callback(this, url, null, status);
                }
            }
        };

        cb.url(ApiInterface.USER_SELLINGS + meta.next).type(JSONObject.class).header().param("seller_id", userId).method(Constants.METHOD_GET);
        ajax(cb);
    }

    public boolean hasMoreSellings() {
        return meta != null && ModelUtils.isNotEmpty(meta.next);
    }

    public void follow(final String userId) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (status.getCode() == 201) {
                        jo.put("follow_user_id",userId);
                        ProfileModel.this.OnMessageResponse(url, jo, status);
                    } else {
                        ProfileModel.this.errorCallback(url, status);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        cb.url(ApiInterface.FOLLOW).type(JSONObject.class).header().param("user_id", userId).method(Constants.METHOD_POST);
        ajaxProgress(cb);
    }

    public void unfollow(final String userId) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (status.getCode() == 200) {
                        jo.put("unfollow_user_id",userId);
                        ProfileModel.this.OnMessageResponse(url, jo, status);
                    } else {
                        ProfileModel.this.errorCallback(url, status);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        cb.url(ApiInterface.UNFOLLOW).type(JSONObject.class).header().param("user_id", userId).method(Constants.METHOD_POST);
        ajaxProgress(cb);
    }

    /**
     * 发送验证邮件，邮件地址后台可以根据header中的user id查到，所以不必添加参数。只有请求发送成功（http code 200）时才会通知监听者
     */
    public void sendVerifyEmail() {

        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (status != null && status.getCode() != 200){
                        ProfileModel.this.callback(this, url, null, status); // 公共的错误处理
                    }

                    ProfileModel.this.OnMessageResponse(url, jo, status);       // TabProfileActivity需要维护isSendingMail的状态，所以无论成功，失败都要返回
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        cb.url(ApiInterface.SEND_VERIFY_EMAIL).type(JSONObject.class).header().method(Constants.METHOD_POST);
        ajax(cb);
    }

    /**
     * 从服务器获取分享店铺的信息
     *
     * @param fuzzUserId 用户的fuzzId
     */
    public void shareStore(String fuzzUserId) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (status != null && status.getCode() == 200) {
                        ProfileModel.this.OnMessageResponse(url, jo, status);
                    } else {
                        ProfileModel.this.callback(this, url, null, status);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        cb.url(ApiInterface.SHARE_STORE).type(JSONObject.class).header().method(Constants.METHOD_GET).param("user_id", fuzzUserId);
        ajaxProgress(cb);
    }

    public Subscription blockUser(Activity activity, String uid, final Action1<Void> callback) {
        return AndroidObservable.bindActivity(activity, userRepo.blockUser(uid, !userBlocked))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<Void>() {
                    @Override
                    public void onCompleted() {
                        callback.call(null);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        errorCallback("block/unblock", "", e.getMessage());
                    }
                });
    }
}
