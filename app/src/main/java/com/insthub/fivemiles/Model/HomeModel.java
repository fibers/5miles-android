package com.insthub.fivemiles.Model;

import android.content.Context;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.Constants;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.CommodityHomeResponse;
import com.insthub.fivemiles.Protocol.HOME_OBJECTS;
import com.insthub.fivemiles.Protocol.META;
import com.insthub.fivemiles.Utils.LocationManagerUtil;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HomeModel extends BaseModel {

	public ArrayList<HOME_OBJECTS> dataList = new ArrayList<HOME_OBJECTS>();
	public META meta;
	public HomeModel(Context context) {
		super(context);
	}
	
	public void commodityHome() {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				try {
					HomeModel.this.callback(this, url, jo, status);
					if(jo != null) {
						CommodityHomeResponse response = new CommodityHomeResponse();
						response.fromJson(jo);
						if(ModelUtils.isEmpty(response.error)) {
							meta = response.meta;
							dataList.clear();
                            dataList.addAll(response.objects);
							HomeModel.this.OnMessageResponse(url,jo,status);
						} else {
							HomeModel.this.errorCallback(url, response.error, response.error_description);
                            HomeModel.this.OnMessageResponse(url, null, null);
						}
					}
                    else {
                        HomeModel.this.OnMessageResponse(url, null, null);
                    }
				} catch (JSONException e) {
                    L.e("api home failed", e);
				}
			}
		};
		
		cb.url(ApiInterface.COMMODITY_HOME
				+ "?lat=" + LocationManagerUtil.getLatitudeInSession()
				+ "&lon=" + LocationManagerUtil.getLongitudeInSession()
                + "&limit=20").type(JSONObject.class).header().method(Constants.METHOD_GET);
		ajax(cb);
	}
	
	public void commodityHomeMore() {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				try {
					HomeModel.this.callback(this, url, jo, status);
					if(jo != null) {
						CommodityHomeResponse response = new CommodityHomeResponse();
						response.fromJson(jo);
						if(ModelUtils.isEmpty(response.error)) {
							meta = response.meta;
                            dataList.addAll(response.objects);
							HomeModel.this.OnMessageResponse(url,jo,status);
						} else {
							HomeModel.this.errorCallback(url, response.error, response.error_description);
                            HomeModel.this.OnMessageResponse(url, null, null);
						}
					} else {
                        HomeModel.this.OnMessageResponse(url, null, null);
                    }
				} catch (JSONException e) {
                    L.e("api home/more failed", e);
				}
			}
		};

        cb.url(ApiInterface.COMMODITY_HOME + meta.next +
                "&lat=" + LocationManagerUtil.getLatitudeInSession() +
                "&lon=" + LocationManagerUtil.getLongitudeInSession()
        ).type(JSONObject.class).header().method(Constants.METHOD_GET);
        ajax(cb);
    }

    public boolean hasMore() {
        return meta != null && ModelUtils.isNotEmpty(meta.next);
    }
}
