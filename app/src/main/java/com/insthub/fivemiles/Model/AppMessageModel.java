package com.insthub.fivemiles.Model;

import android.content.Context;
import android.os.Message;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.Constants;
import com.external.eventbus.EventBus;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.META;
import com.insthub.fivemiles.Protocol.MessageObject;
import com.insthub.fivemiles.Protocol.MessageResponse;
import com.insthub.fivemiles.Protocol.SystemMessage;
import com.insthub.fivemiles.Protocol.SystemMessageResponse;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;

public class AppMessageModel extends BaseModel {
	
	public int unread_num;
	
	public ArrayList<MessageObject> messageBuyingList = new ArrayList<>();
	public ArrayList<MessageObject> messageSellingList = new ArrayList<>();
	public ArrayList<SystemMessage> messageSystemList = new ArrayList<>();
	public META meta;
	
	public boolean buyingUnread = false;
	public boolean sellingUnread = false;
	public boolean systemUnread = false;

	public AppMessageModel(Context context) {
		super(context);
	}
	
	public void getMessageUnreadCount() {
        boolean isNotAuthed = !SESSION.getInstance().isAuth();
        if (isNotAuthed) {      // 若不是注册用户，则返回。
            return;
        }
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				if(status.getCode() == 200 && jo != null) {
					try {
						int selling = jo.optInt("selling");
                        int buying = jo.optInt("buying");
                        int system = jo.optInt("system");
                        unread_num = selling + buying + system;
                        buyingUnread = buying > 0;
                        sellingUnread = selling > 0;
                        systemUnread = system > 0;
	                    AppMessageModel.this.OnMessageResponse(url,jo,status);
                        postNewMsgFlag();
					} catch (JSONException e) {
                        L.e(e);
					}
				} else {
					 AppMessageModel.this.callback(this, url, null, status);
				}
			}
		};
		
		cb.url(ApiInterface.MESSAGE_UNREAD_NUM).type(JSONObject.class).header().method(Constants.METHOD_GET);
		ajax(cb);
	}

    private void postNewMsgFlag() {
        postNewMsgFlag(MessageConstant.UNREAD_BUYING, buyingUnread);
        postNewMsgFlag(MessageConstant.UNREAD_SELLING, sellingUnread);
        postNewMsgFlag(MessageConstant.UNREAD_SYSTEM, systemUnread);
    }

    private void postNewMsgFlag(int type, boolean unread) {
        Message msg = Message.obtain();
        msg.what    = type;
        msg.obj     = unread;
        EventBus.getDefault().post(msg);
    }
	
	public void getMessageBuyingList() {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				if(status.getCode() == 200) {
					try {
						MessageResponse resp = new MessageResponse();
						resp.fromJson(jo);
						meta = resp.meta;
						messageBuyingList.clear();
						messageBuyingList.addAll(resp.objects);

//                        checkBuyMsgStatus();
						
	                    AppMessageModel.this.OnMessageResponse(url,jo,status);
					} catch (JSONException e) {
                        L.e(e);
					}
				} else {
					 AppMessageModel.this.errorCallback(url, status);
				}
			}
		};
		
		cb.url(ApiInterface.MESSAGE_BUYING + "&limit=20").
                type(JSONObject.class).header().method(Constants.METHOD_GET);
		ajax(cb);
	}

//    public void checkBuyMsgStatus() {
//        buyingUnread = false;
//        for (MessageObject msg : messageBuyingList) {
//            if (msg.unread) {
//                buyingUnread = true;
//                break;
//            }
//        }
//    }

    public void getMessageBuyingListMore() {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				if(status.getCode() == 200) {
					try {
						MessageResponse resp = new MessageResponse();
						resp.fromJson(jo);
						meta = resp.meta;
						messageBuyingList.addAll(resp.objects);

//                        checkBuyMsgStatus();

	                    AppMessageModel.this.OnMessageResponse(url,jo,status);
					} catch (JSONException e) {
                        L.e(e);
					}
				} else {
					 AppMessageModel.this.callback(this, url, null, status);
				}
			}
		};

        String url = ApiInterface.MESSAGE_BUYING;
        if (meta == null || isEmpty(meta.next)) {
            try {
                AppMessageModel.this.OnMessageResponse(url, null, new AjaxStatus(200, "OK"));
            } catch (JSONException e) {
                L.e(e);
            }
            return;
        }

        url += "&" + meta.next.replaceFirst("\\?", "");
        cb.url(url).type(JSONObject.class).header().method(Constants.METHOD_GET);
		ajax(cb);
	}
	
	public void getMessageSellingList() {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				if(status.getCode() == 200) {
					try {
						MessageResponse resp = new MessageResponse();
						resp.fromJson(jo);
						meta = resp.meta;
						messageSellingList.clear();
						messageSellingList.addAll(resp.objects);

//                        checkSellMsgStatus();

	                    AppMessageModel.this.OnMessageResponse(url,jo,status);
					} catch (JSONException e) {
                        L.e(e);
					}
				} else {
					 AppMessageModel.this.errorCallback(url, status);
				}
			}
		};
		
		cb.url(ApiInterface.MESSAGE_SELLING + "&limit=20").
                type(JSONObject.class).header().method(Constants.METHOD_GET);
		ajax(cb);
	}

//    public void checkSellMsgStatus() {
//        sellingUnread = false;
//        for (MessageObject msg : messageSellingList) {
//            if (msg.unread) {
//                sellingUnread = true;
//                break;
//            }
//        }
//    }

    public void getMessageSellingListMore() {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				if(status.getCode() == 200) {
					try {
						MessageResponse resp = new MessageResponse();
						resp.fromJson(jo);
						meta = resp.meta;
						messageSellingList.addAll(resp.objects);

//                        checkSellMsgStatus();

                        AppMessageModel.this.OnMessageResponse(url,jo,status);
					} catch (JSONException e) {
                        L.e(e);
					}
				} else {
					 AppMessageModel.this.callback(this, url, null, status);
				}
			}
		};

        String url = ApiInterface.MESSAGE_SELLING;
        if (meta == null || isEmpty(meta.next)) {
            try {
                AppMessageModel.this.OnMessageResponse(url, null, new AjaxStatus(200, "OK"));
            } catch (JSONException e) {
                L.e(e);
            }
            return;
        }

        url += "&" + meta.next.replaceFirst("\\?", "");
		cb.url(url).type(JSONObject.class).header().method(Constants.METHOD_GET);
		ajax(cb);
	}
	
	public void getMessageSystemList() {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				if(status.getCode() == 200) {
					try {
						SystemMessageResponse resp = new SystemMessageResponse();
						resp.fromJson(jo);
						meta = resp.meta;
						messageSystemList.clear();
						messageSystemList.addAll(resp.objects);
	                    AppMessageModel.this.OnMessageResponse(url,jo,status);
					} catch (JSONException e) {
                        L.e(e);
					}
				} else {
					 AppMessageModel.this.errorCallback(url, status);
				}
			}
		};
		
		cb.url(ApiInterface.MESSAGE_SYSTEM + "&limit=20")
                .type(JSONObject.class).header().method(Constants.METHOD_GET);
		ajax(cb);
	}
	
	public void getMessageSystemListMore() {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				if(status.getCode() == 200) {
					try {
						SystemMessageResponse resp = new SystemMessageResponse();
						resp.fromJson(jo);
						meta = resp.meta;
						messageSystemList.addAll(resp.objects);
	                    AppMessageModel.this.OnMessageResponse(url,jo,status);
					} catch (JSONException e) {
                        L.e(e);
					}
				} else {
					 AppMessageModel.this.callback(this, url, null, status);
				}
			}
		};

        String url = ApiInterface.MESSAGE_SYSTEM;
        if (meta == null || isEmpty(meta.next)) {
            // no more
            try {
                AppMessageModel.this.OnMessageResponse(url, null, new AjaxStatus(200, "OK"));
            } catch (JSONException e) {
                L.e(e);
            }
            return;
        }

        url += "&" + meta.next.replaceFirst("\\?", "");
        cb.url(url).type(JSONObject.class).header().method(Constants.METHOD_GET);
		ajax(cb);
	}

//    public void checkSysMsgStatus() {
//        systemUnread = false;
//        for (SystemMessage msg : messageSystemList) {
//            if (msg.isUnread()) {
//                systemUnread = true;
//                break;
//            }
//        }
//    }

    public void setOfferLineRead(String messageId) {
        if (ModelUtils.isEmpty(messageId)) {
            return;
        }

		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				if(status.getCode() == 200) {
					try {
	                    AppMessageModel.this.OnMessageResponse(url,jo,status);
					} catch (JSONException e) {
                        L.e(e);
					}
				} else {
					 AppMessageModel.this.callback(this, url, null, status);
				}
			}
		};
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("message_id", messageId);
		cb.url(ApiInterface.READ_MESSAGE).type(JSONObject.class).
                header().params(params).
                method(Constants.METHOD_POST);
		ajax(cb);
	}

    public void deleteMessage(String type, String msgId) {
        if (ModelUtils.isEmpty(msgId)) {
            return;
        }

        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                if(status.getCode() == 200) {
                    try {
                        AppMessageModel.this.OnMessageResponse(url,jo,status);
                    } catch (JSONException e) {
                        L.e(e);
                    }
                } else {
                    AppMessageModel.this.errorCallback(url, status);
                }
            }
        };

        Map<String, Object> params = new HashMap<>();
        params.put("message_id", msgId);
        params.put("type", type);

        cb.url(ApiInterface.DELETE_MESSAGE).type(JSONObject.class).
                header().params(params).method(Constants.METHOD_POST);
        ajax(cb);
    }

	public void deleteSystem(String id) {
		deleteMessage("system", id);
	}

	public void readAllSystem() {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				if(status.getCode() == 200) {
					try {
	                    AppMessageModel.this.OnMessageResponse(url,jo,status);
                        systemUnread = false;
					} catch (JSONException e) {
                        L.e(e);
					}
				} else {
					 AppMessageModel.this.callback(this, url, null, status);
				}
			}
		};
		Map<String, Object> params = new HashMap<>();
		cb.url(ApiInterface.MESSAGE_SYSTEM_READ).type(JSONObject.class).header()
                .params(params).method(Constants.METHOD_POST);
		ajax(cb);
	}
}
