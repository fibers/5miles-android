package com.insthub.fivemiles.Model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.Constants;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.COUNTRY;
import com.insthub.fivemiles.Protocol.OFFER;

public class CountryModel extends BaseModel {

	public ArrayList<COUNTRY> countryList = new ArrayList<COUNTRY>();
	
	public CountryModel(Context context) {
		super(context);
	}
	
	public void getCountryDialcodesList() {
		BeeCallback<JSONArray> cb = new BeeCallback<JSONArray>() {

			@Override
			public void callback(String url, JSONArray jo, AjaxStatus status) {
				try {
					countryList.clear();
					for (int i = 0; i < jo.length(); i++) {
						COUNTRY country = new COUNTRY();
						JSONObject item = new JSONObject();
						item = (JSONObject) jo.get(i);
						country.fromJson(item);
						countryList.add(country);
					}
					CountryModel.this.OnMessageResponse(url, null, status);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		};
		
		cb.url(ApiInterface.COUNTRY_DIALCODES).type(JSONArray.class).header().method(Constants.METHOD_GET);
		ajaxProgress(cb);
	}
	
}
