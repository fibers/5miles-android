package com.insthub.fivemiles.Model;

import android.content.Context;
import android.text.TextUtils;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.Constants;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.COMMENT;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Deprecated
public class OrderModel extends BaseModel {

	public ArrayList<COMMENT> commentList = new ArrayList<COMMENT>();
    public String commentsNextUrl;
	public boolean checked = false;
    public String shortUrl;
	
	public OrderModel(Context context) {
		super(context);
	}

	/**
	 * 评论列表
	 */
	public void refreshItemComments(String itemId, int limit) {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				try {
					commentList.clear();
                    if (jo == null) {
                        return;
                    }

                    JSONObject metaJo = jo.getJSONObject("meta");
                    commentsNextUrl = metaJo.optString("next");

                    JSONArray commentsJa = jo.getJSONArray("objects");

                    for (int i = 0; i < commentsJa.length(); i++) {
						COMMENT comment = new COMMENT();
						JSONObject item = (JSONObject) commentsJa.get(i);
						comment.fromJson(item);
						commentList.add(comment);
					}
					OrderModel.this.OnMessageResponse(url, jo, status);
				} catch (Exception e) {
					L.e(e);
				}
			}
		};

        String url = ApiInterface.ITEM_COMMENTS + "?item_id=" + itemId;

        if (limit > 0) {
            url += "&limit=" + limit;
        }

		cb.url(url).type(JSONObject.class).header().method(Constants.METHOD_GET);
		ajax(cb);
	}

    public void loadMoreItemComments(String itemId) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                try {
                    if (jo == null) {
                        return;
                    }

                    JSONObject metaJo = jo.getJSONObject("meta");
                    commentsNextUrl = metaJo.optString("next");

                    JSONArray commentsJa = jo.getJSONArray("objects");

                    for (int i = 0; i < commentsJa.length(); i++) {
                        COMMENT comment = new COMMENT();
                        JSONObject item = (JSONObject) commentsJa.get(i);
                        comment.fromJson(item);
                        commentList.add(comment);
                    }
                    OrderModel.this.OnMessageResponse(url, jo, status);
                } catch (Exception e) {
                    L.e(e);
                }
            }
        };

        String url = ApiInterface.ITEM_COMMENTS +
                (TextUtils.isEmpty(commentsNextUrl) ? "" : commentsNextUrl) +
                "&item_id=" + itemId;
        cb.url(url).type(JSONObject.class).header().method(Constants.METHOD_GET);
        ajax(cb);
    }

	/**
	 * 发布评论
	 * @param id item id
	 * @param body comment body
	 */
	public void postQuestion(String id, String body) {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				try {
					if (jo != null) {
						OrderModel.this.OnMessageResponse(url, jo, status);
					}
				} catch (JSONException e) {
                    L.e("add comment failed", e);
				}
			}
		};

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("item_id", id);
		params.put("text", ModelUtils.trim(body));
		cb.url(ApiInterface.POST_QUESTION).type(JSONObject.class).header().params(params).method(Constants.METHOD_POST);
		ajaxProgress(cb);
	}
	
	/**
	 * 回复评论
	 * @param id item id
	 * @param body comment id
	 */
	public void postReply(String id, String body, int ref_post_id) {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				try {
					if (jo != null) {
						OrderModel.this.OnMessageResponse(url, jo, status);
					}
				} catch (JSONException e) {
                    L.e("replay failed", e);
				}
			}
		};

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("item_id", id);
		params.put("text", ModelUtils.trim(body));
		params.put("reply_to", ref_post_id);
		cb.url(ApiInterface.POST_REPLY).type(JSONObject.class).header().params(params).method(Constants.METHOD_POST);
		ajaxProgress(cb);
	}
}
