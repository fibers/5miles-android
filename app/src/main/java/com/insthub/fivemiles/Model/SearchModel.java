package com.insthub.fivemiles.Model;

import android.content.Context;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.AQUtility;
import com.external.androidquery.util.Constants;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.Utils.LocationManagerUtil;
import com.thirdrock.domain.Campaign;
import com.thirdrock.domain.Campaign__JsonHelper;
import com.thirdrock.domain.CategoryInfo;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.SearchFilter;
import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.Meta;
import com.thirdrock.protocol.SearchResp;
import com.thirdrock.protocol.SearchResp__JsonHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.insthub.fivemiles.Protocol.ApiInterface.AD_BANNER;
import static com.insthub.fivemiles.Protocol.ApiInterface.SEARCH_BRAND;
import static com.insthub.fivemiles.Protocol.ApiInterface.SEARCH_CAT;
import static com.insthub.fivemiles.Protocol.ApiInterface.SEARCH_Q;
import static com.thirdrock.fivemiles.util.ModelUtils.encodeUrl;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;

public class SearchModel extends BaseModel {

    public static final int SEARCH_PAGE_SIZE = 20;
    public Meta meta = new Meta();
    public List<Item> items = Collections.emptyList();
    public List<CategoryInfo> categoryStats = Collections.emptyList();
    public SearchFilter searchFilter;
    public Campaign bannerCampaign;

	public SearchModel(Context context) {
		super(context);
	}
	
	public void searchByKeyword(String str, String rfTag) {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try {
                        SearchModel.this.callback(this, url, jo, status);
                        if (jo != null) {
                            SearchResp resp = SearchResp__JsonHelper.parseFromJson(jo.toString());
                            if (resp.getMeta() != null) {
                                meta = resp.getMeta();
                                items = resp.getItems();
                                if (!resp.getCategoryStats().isEmpty()) {
                                    categoryStats = resp.getCategoryStats();
                                }
                                searchFilter = resp.getSearchFilter();
                                SearchModel.this.OnMessageResponse(url, jo, status);
                            } else {
                                SearchModel.this.errorCallback(url, status);
                                SearchModel.this.OnMessageResponse(url, null, status);
                            }
                        }
                    } catch (Exception e) {
                        L.e(e);
                        SearchModel.this.errorCallback();
                    }
                } else {
                    SearchModel.this.errorCallback(url, status);
                    try {
                        SearchModel.this.OnMessageResponse(url, null, status);
                    } catch (JSONException e) {
                        L.e(e);
                    }
                }
			}
		};

        cb.url(SEARCH_Q + "?limit="+ SEARCH_PAGE_SIZE + "&q=" + encodeUrl(str)                       // 用户输入的字符要进行encode
                        + "&lat=" + LocationManagerUtil.getLatitudeInSession()
                        + "&lon=" + LocationManagerUtil.getLongitudeInSession()
                        + (searchFilter != null ? "&" + searchFilter.toQueryString() : "&refind=0")  // 首次查询refind=0
                        + ("&rf_tag=" + rfTag)
        ).type(JSONObject.class).header().method(Constants.METHOD_GET);
        ajax(cb);
    }

	public void searchByKeywordMore(String str, String rfTag) {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try {
                        SearchModel.this.callback(this, url, jo, status);
                        if (jo != null) {
                            SearchResp resp = SearchResp__JsonHelper.parseFromJson(jo.toString());
                            if (resp.getMeta() != null) {
                                meta = resp.getMeta();
                                items.addAll(resp.getItems());
                                SearchModel.this.OnMessageResponse(url, jo, status);
                            } else {
                                SearchModel.this.errorCallback(url, status);
                                SearchModel.this.OnMessageResponse(url, null, status);
                            }
                        }
                    } catch (Exception e) {
                        L.e(e);
                        SearchModel.this.errorCallback();
                    }
                }
                else {
                    SearchModel.this.errorCallback(url, status);
                    try {
                        SearchModel.this.OnMessageResponse(url, null, status);
                    } catch (JSONException e) {
                        L.e(e);
                    }
                }
			}
		};

        String url = hasMore() ? SEARCH_Q + meta.getNext() + "&" : SEARCH_Q + "?";
        url += "q=" + encodeUrl(str)
                + "&lat=" + LocationManagerUtil.getLatitudeInSession()
                + "&lon=" + LocationManagerUtil.getLongitudeInSession()
                + (searchFilter != null ? "&" + searchFilter.toQueryString() : "&refind=0")
                + ("&rf_tag=" + rfTag);  // 首次查询refind=0

		cb.url(url).type(JSONObject.class).header().method(Constants.METHOD_GET);
		ajax(cb);
	}
	
	public void searchCategory(final String categoryId, final int topCategoryId, String rfTag) {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
                AQUtility.debug("callback: ajax status code is " + status.getCode());
                if (status.getCode() == 200) {
                    try {
                        SearchModel.this.callback(this, url, jo, status);
                        if (jo != null) {
                            SearchResp resp = SearchResp__JsonHelper.parseFromJson(jo.toString());
                            if (resp.getMeta() != null) {
                                meta = resp.getMeta();
                                items = resp.getItems();

                                if (!resp.getCategoryStats().isEmpty()) {
                                    categoryStats = resp.getCategoryStats();
                                }

                                if (resp.getSearchFilter() != null) {
                                    searchFilter = resp.getSearchFilter();
                                    // 容错，category查询结果不应出现all
                                    if (searchFilter.getCategory() < 1) {
                                        searchFilter.setCategory(Integer.valueOf(categoryId));
                                    }
                                } else if (searchFilter == null) {
                                    searchFilter = new SearchFilter();
                                    searchFilter.setCategory(Integer.valueOf(categoryId));
                                }

                                SearchModel.this.OnMessageResponse(url, jo, status);
                            } else {
                                SearchModel.this.errorCallback(url, status);
                                SearchModel.this.OnMessageResponse(url, null, status);
                            }
                        }
                    } catch (Exception e) {
                        L.e(e);
                    }
                }
                else {
                    SearchModel.this.errorCallback(url, status);
                    try {
                        SearchModel.this.OnMessageResponse(url, null, status);
                    } catch (JSONException e) {
                        L.e(e);
                    }
                }
			}
		};

        // cat_id以filter中的值为准，除非初次查询无filter
        cb.url(SEARCH_CAT + "?limit=" + SEARCH_PAGE_SIZE + "&lat=" + LocationManagerUtil.getLatitudeInSession()
                        + "&lon=" + LocationManagerUtil.getLongitudeInSession()
                        + (searchFilter != null ? "&" + searchFilter.toQueryString() : "&refind=0&cat_id=" + categoryId + "&root_cat_id=" + topCategoryId)  // 首次查询refind=0
                        + ("&rf_tag=" + rfTag)
        ).type(JSONObject.class).header().method(Constants.METHOD_GET);
        ajax(cb);
    }

    public void searchCategoryMore(String categoryId, final int topCategoryId, String rfTag) {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try {
                        SearchModel.this.callback(this, url, jo, status);
                        AQUtility.debug("callback: ajax status code is " + status.getCode());
                        if (jo != null) {
                            SearchResp resp = SearchResp__JsonHelper.parseFromJson(jo.toString());
                            if (resp.getMeta() != null) {
                                meta = resp.getMeta();
                                items.addAll(resp.getItems());
                                SearchModel.this.OnMessageResponse(url, jo, status);
                            } else {
                                SearchModel.this.errorCallback(url, status);
                                SearchModel.this.OnMessageResponse(url, null, status);
                            }
                        }
                    } catch (Exception e) {
                        L.e(e);
                        SearchModel.this.errorCallback();
                    }
                } else {
                    SearchModel.this.errorCallback(url, status);
                    try {
                        SearchModel.this.OnMessageResponse(url, null, status);
                    } catch (JSONException e) {
                        L.e(e);
                    }
                }
			}
		};

        String url = hasMore() ? SEARCH_CAT + meta.getNext() + "&" : SEARCH_CAT + "?";

        // cat_id以filter中的值为准，除非初次查询无filter
        url += "lat=" + LocationManagerUtil.getLatitudeInSession()
                + "&lon=" + LocationManagerUtil.getLongitudeInSession()
                + (searchFilter != null ? "&" + searchFilter.toQueryString() : "&refind=0&cat_id=" + categoryId + "&root_cat_id=" + topCategoryId)  // 首次查询refind=0
                + ("&rf_tag=" + rfTag);
        cb.url(url).type(JSONObject.class).header().method(Constants.METHOD_GET);
        ajax(cb);
    }

    public void searchBrand(int brandId, String rfTag) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try {
                        SearchModel.this.callback(this, url, jo, status);
                        if (jo != null) {
                            SearchResp resp = SearchResp__JsonHelper.parseFromJson(jo.toString());
                            if (resp.getMeta() != null) {
                                meta = resp.getMeta();
                                items = resp.getItems();
                                if (!resp.getCategoryStats().isEmpty()) {
                                    categoryStats = resp.getCategoryStats();
                                }
                                searchFilter = resp.getSearchFilter();
                                SearchModel.this.OnMessageResponse(url, jo, status);
                            } else {
                                SearchModel.this.errorCallback(url, status);
                                SearchModel.this.OnMessageResponse(url, null, status);
                            }
                        }
                    } catch (Exception e) {
                        L.e(e);
                        SearchModel.this.errorCallback();
                    }
                }
                else {
                    SearchModel.this.errorCallback(url, status);
                    try {
                        SearchModel.this.OnMessageResponse(url, null, status);
                    } catch (JSONException e) {
                        L.e(e);
                    }
                }
            }
        };

        cb.url(SEARCH_BRAND + "?limit=" + SEARCH_PAGE_SIZE + "&brand_id=" + brandId
                        + "&lat=" + LocationManagerUtil.getLatitudeInSession()
                        + "&lon=" + LocationManagerUtil.getLongitudeInSession()
                        + (searchFilter != null ? "&" + searchFilter.toQueryString() : "&refind=0")  // 首次查询refind=0
                        + ("&rf_tag=" + rfTag)
        ).type(JSONObject.class).header().method(Constants.METHOD_GET);
        ajax(cb);
    }

    public void searchBrandMore(int brandId, String rfTag) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try {
                        SearchModel.this.callback(this, url, jo, status);
                        if (jo != null) {
                            SearchResp resp = SearchResp__JsonHelper.parseFromJson(jo.toString());
                            if (resp.getMeta() != null) {
                                meta = resp.getMeta();
                                items.addAll(resp.getItems());
                                SearchModel.this.OnMessageResponse(url, jo, status);
                            } else {
                                SearchModel.this.errorCallback(url, status);
                                SearchModel.this.OnMessageResponse(url, null, status);
                            }
                        }
                    } catch (Exception e) {
                        L.e(e);
                        SearchModel.this.errorCallback();
                    }
                }
                else {
                    SearchModel.this.errorCallback(url, status);
                    try {
                        SearchModel.this.OnMessageResponse(url, null, status);
                    } catch (JSONException e) {
                        L.e(e);
                    }
                }
            }
        };

        String url = hasMore() ? SEARCH_BRAND + meta.getNext() + "&" : SEARCH_BRAND + "?";
        url += "brand_id=" + brandId
                + "&lat=" + LocationManagerUtil.getLatitudeInSession()
                + "&lon=" + LocationManagerUtil.getLongitudeInSession()
                + (searchFilter != null ? "&" + searchFilter.toQueryString() : "&refind=0")  // 首次查询refind=0
                + ("&rf_tag=" + rfTag);
        cb.url(url).type(JSONObject.class).header().method(Constants.METHOD_GET);
        ajax(cb);
    }

    public boolean hasMore() {
        return meta != null && isNotEmpty(meta.getNext());
    }

    public void updateFilter(Option opt) {
        if (searchFilter == null) {
            searchFilter = new SearchFilter();
        }

        EnumFilter filter = opt.type;
        switch (filter) {
            case DISTANCE:
                updateDistanceFilter(opt);
                break;
            case CATEGORY:
                updateCategoryFilter(opt);
                break;
            case SORTING:
                updateSorting(opt);
                break;
        }
    }

    public void setRefindFlag(int f) {
        if (searchFilter == null) {
            searchFilter = new SearchFilter();
        }

        searchFilter.setRefind(f);
    }

    private void updateDistanceFilter(Option opt) {
        searchFilter.setDistanceRange(Integer.valueOf(opt.value));
    }

    private void updateCategoryFilter(Option opt) {
        searchFilter.setCategory(Integer.valueOf(opt.value));
    }

    private void updateSorting(Option opt) {
        searchFilter.setOrderBy(Integer.valueOf(opt.value));
    }

    public void updateMiscFilter(boolean verifiedOnly, String strPrice1, String strPrice2) {
        if (searchFilter == null) {
            searchFilter = new SearchFilter();
        }
        searchFilter.setVerifiedUserOnly(verifiedOnly);

        Integer price1, price2, minPrice, maxPrice;
        strPrice1 = strPrice1.trim();
        strPrice2 = strPrice2.trim();
        price1 = parsePrice(strPrice1);
        price2 = parsePrice(strPrice2);

        if (isValidPrice(price1) && isValidPrice(price2)) {
            Integer[] prices = {price1, price2};
            Arrays.sort(prices);
            minPrice = prices[0];
            maxPrice = prices[1];
        } else {
            minPrice = isValidPrice(price1) ? price1 : null;
            maxPrice = isValidPrice(price2) ? price2 : null;
        }

        searchFilter.setMinPrice(minPrice);
        searchFilter.setMaxPrice(maxPrice);
    }

    public Integer parsePrice(String strPrice) {
        if (isNotEmpty(strPrice)) {
            try {
                return Double.valueOf(strPrice).intValue();
            } catch (Exception e) {
                // ignore
            }
        }
        return null;
    }

    public boolean isValidPrice(Integer p) {
        return p != null && p > 0;
    }

    /**
     * 获取搜索页广告banner
     */
    public void getBanner(String campaignId) {
        BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject jo, AjaxStatus status) {
                if (status.getCode() == 200) {
                    try {
                        SearchModel.this.callback(this, url, jo, status);
                        if (jo != null) {
                            bannerCampaign = Campaign__JsonHelper.parseFromJson(jo.toString());
                            SearchModel.this.OnMessageResponse(url, jo, status);
                        }
                    } catch (Exception e) {
                        L.e(e);
                        SearchModel.this.errorCallback();
                    }
                }
                else {
                    SearchModel.this.errorCallback(url, status);
                    try {
                        SearchModel.this.OnMessageResponse(url, null, status);
                    } catch (JSONException e) {
                        L.e(e);
                    }
                }
            }
        };

        cb.url(AD_BANNER + "?cid=" + campaignId).type(JSONObject.class).header().method(Constants.METHOD_GET);
        ajax(cb);
    }

    /**
     * Filter types
     */
    public static enum EnumFilter {
        DISTANCE, CATEGORY, SORTING, MISC
    }

    /**
     * A name-value pair for displaying filter options
     */
    public static class Option {
        public EnumFilter type;
        public String name;  // the origin name
        public String displayName;  // the displayed name used in list adapter
        public String value;
        private boolean isDefault;

        public Option(EnumFilter type, String name, String value) {
            this(type, name, value, false);
        }

        public Option(EnumFilter type, String name, String value, boolean isDefault) {
            this.type = type;
            this.name = name;
            this.value = value;
            this.isDefault = isDefault;

            this.displayName = isDefault ? getDefaultOptionName() : name;
        }

        public String getDefaultOptionName() {
            return type != EnumFilter.DISTANCE ? name + FiveMilesApp.getInstance().getString(R.string.default_option_suffix) : name;
        }
    }
}
