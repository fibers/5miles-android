package com.insthub.fivemiles.Model;

import android.content.Context;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.insthub.fivemiles.Protocol.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.external.androidquery.util.Constants.METHOD_POST;


public class ComplainModel extends BaseModel{

	public ComplainModel(Context context) {
		super(context);
	}
	
	public void Complain(String body) {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {
			
			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				try {
					if(status.getCode() == 200 || status.getCode() == 201){
						ComplainModel.this.OnMessageResponse(url, jo, status);
					}else{
						ComplainModel.this.errorCallback(url, status);
					}
				} catch (JSONException e) {
					e.printStackTrace();
                    ComplainModel.this.errorCallback();
				}
				
			}
		};
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("text", body);
		cb.url(ApiInterface.REMARK_APP).type(JSONObject.class)
                .header().params(params).method(METHOD_POST);
		ajaxProgress(cb);
	}
}
