package com.insthub.fivemiles.Model;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.BeeFramework.model.BaseModel;
import com.BeeFramework.model.BeeCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.Constants;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.META;
import com.insthub.fivemiles.Protocol.USER;
import com.insthub.fivemiles.Protocol.UserResponse;

public class FollowingModel extends BaseModel {

	public ArrayList<USER> userList = new ArrayList<USER>();
	public META meta;
	public FollowingModel(Context context) {
		super(context);
	}
	
	public void getFollowingList(String userId) {
		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				if (status.getCode() == 200) {
					try {
						FollowingModel.this.callback(this, url, jo, status);
						if (jo != null) {
							UserResponse response = new UserResponse();
							response.fromJson(jo);
							meta = response.meta;
							userList.clear();
							userList.addAll(response.objects);
							FollowingModel.this.OnMessageResponse(url, jo, status);
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					    FollowingModel.this.errorCallback();
					}
				} else {
					FollowingModel.this.errorCallback(url, status);
				}
			}
		};
		
		cb.url(ApiInterface.USER_FOLLOWLING).type(JSONObject.class).header().param("user_id", userId).method(Constants.METHOD_GET);
		ajax(cb);
	}
	
	public void getFollowingListMore(String userId) {

		BeeCallback<JSONObject> cb = new BeeCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject jo, AjaxStatus status) {
				if (status.getCode() == 200) {
					try {
						FollowingModel.this.callback(this, url, jo, status);
						if (jo != null) {
							UserResponse response = new UserResponse();
							response.fromJson(jo);
							meta = response.meta;
							userList.addAll(response.objects);
							FollowingModel.this.OnMessageResponse(url, jo, status);
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					    FollowingModel.this.errorCallback();
					}
				} else {
					FollowingModel.this.errorCallback(url, status);
				}
			}
		};
        meta.next=meta.next+"&user_id="+userId;
        cb.url(ApiInterface.USER_FOLLOWLING + meta.next).type(JSONObject.class).header().param("user_id", userId).method(Constants.METHOD_GET);
		ajax(cb);
	}
	
	
}
