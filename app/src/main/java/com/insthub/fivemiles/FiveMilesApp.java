package com.insthub.fivemiles;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.http.HttpResponseCache;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

import com.BeeFramework.BeeFrameworkApp;
import com.appsflyer.AppsFlyerLib;
import com.external.androidquery.util.AQUtility;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.insthub.fivemiles.Activity.GuidePagerActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.parse.Parse;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.thirdrock.domain.AppConfig;
import com.thirdrock.domain.AppConfig__JsonHelper;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.di.RepositoryModule;
import com.thirdrock.fivemiles.di.RootModule;
import com.thirdrock.fivemiles.util.ApiStatsReceiver;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.rest.RestObserver;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.Utils;
import com.thirdrock.framework.util.rx.SimpleObserver;
import com.thirdrock.repository.ConfigRepository;
import com.thirdrock.repository.impl.BodyParserFactoryImpl;
import com.thirdrock.repository.impl.ConfigRepositoryImpl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import dagger.ObjectGraph;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

import static android.provider.Settings.Secure.ANDROID_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_APP_STATE;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_APP_CONFIG;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_SHORTCUT_V;

/*
 *	 ______    ______    ______
 *	/\  __ \  /\  ___\  /\  ___\
 *	\ \  __<  \ \  __\_ \ \  __\_
 *	 \ \_____\ \ \_____\ \ \_____\
 *	  \/_____/  \/_____/  \/_____/
 *
 *
 *	Copyright (c) 2013-2014, {Bee} open source community
 *	http://www.bee-framework.com
 *
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a
 *	copy of this software and associated documentation files (the "Software"),
 *	to deal in the Software without restriction, including without limitation
 *	the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *	and/or sell copies of the Software, and to permit persons to whom the
 *	Software is furnished to do so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in
 *	all copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *	IN THE SOFTWARE.
 */
public class FiveMilesApp extends BeeFrameworkApp {

    public static DisplayImageOptions displayImageOptions; // DisplayImageOptions是用于设置图片显示的类
	public static DisplayImageOptions imageOptionsAvatar; // DisplayImageOptions是用于设置图片显示的类
	public static DisplayImageOptions imageOptionsHome; // DisplayImageOptions for home page

    public static Context appCtx;

    private static AdvertisingIdClient.Info advertisingIdInfo;
    private static String fbAttributionId;
    private static AppConfig appConfig;

    private ObjectGraph objectGraph;
    private RefWatcher refWatcher;
    private static String androidId;

    public static FiveMilesApp getInstance() {
        return (FiveMilesApp) BeeFrameworkApp.getInstance();
    }

    @Override
	public void onCreate() {
		super.onCreate();

        appCtx = getApplicationContext();
        // 使用Android Id：http://stackoverflow.com/questions/2785485/is-there-a-unique-android-device-id/2853253#2853253
        androidId = Settings.Secure.getString(getContentResolver(), ANDROID_ID);

        initializeDependencyInjector();
        AQUtility.setDebug(com.thirdrock.framework.util.Utils.isDebug());

        L.d("session loaded, uid: %s, fbUid: %s",
                SESSION.getInstance().userId,
                SESSION.getInstance().getFbUid());

        loadSavedAppConfig();

        String device_id = FiveMilesApp.getDeviceId(getApplicationContext());
        SharedPreferences shared;
        SharedPreferences.Editor editor;

        shared = this.getSharedPreferences(FiveMilesAppConst.USERINFO, 0);
        editor = shared.edit();
		editor.putString(ConstantS.DEVICE_UUID, device_id);
		editor.apply();

        // Parse push config
        Parse.initialize(this, getString(R.string.parse_app_id), getString(R.string.parse_client_key));

		displayImageOptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.loading) // 设置图片下载期间显示的图片
				.showImageForEmptyUri(R.drawable.loading) // 设置图片Uri为空或是错误的时候显示的图片
				.showImageOnFail(R.drawable.loading) // 设置图片加载或解码过程中发生错误显示的图片
				.cacheInMemory(false) // 设置下载的图片是否缓存在内存中
				.cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        getResources();

        imageOptionsAvatar = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.head_loading)            // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.drawable.no_avatar)    // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.drawable.no_avatar)        // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true)                                    // 设置下载的图片是否缓存在内存中
                .cacheOnDisk(true)                                        // 设置下载的图片是否缓存在SD卡中
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        imageOptionsHome = new DisplayImageOptions.Builder()
                .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        if (com.thirdrock.framework.util.Utils.isDebug() || com.thirdrock.framework.util.Utils.isGray()) {
            com.nostra13.universalimageloader.utils.L.writeLogs(true);
        }

        DisplayMetrics disp = getResources().getDisplayMetrics();
        Configuration cfg = getResources().getConfiguration();
        L.d("detected screen metrics: %sx%s, widthDp: %s, dpi: %s",
                disp.widthPixels, disp.heightPixels, cfg.screenWidthDp, disp.densityDpi);

        enableHttpResponseCache(appCtx);

        // AppsFlyer SDK
        AppsFlyerLib.setAppsFlyerKey(getString(R.string.af_dev_key));
        AppsFlyerLib.setAppUserId(androidId);
        AppsFlyerLib.sendTracking(appCtx);  // track app session

        // Google Analytics
        TrackingUtils.initGaTracker();

        // check & install shortcut icon
        if (canInstallShortcut()) {
            removeShortcut();
            installShortcut();
        }

        // enabling api access stats
        enableApiStats();

        loadAdvertisingInfo();

        // Memory leak check tool
        refWatcher = LeakCanary.install(this);
	}

    public static RefWatcher getRefWatcher() {
        return getInstance().refWatcher;
    }

    /**
     * load global config from backend
     */
    public void loadAppConfig() {
        ConfigRepository cfgRepo = new ConfigRepositoryImpl(
                RepositoryModule.getApiBase().replaceAll("/api/v\\d+", "/api"),
                RepositoryModule.getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());

        cfgRepo.getAppConfig()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<String>() {
                    @Override
                    public void onNext(String cfgJson) {
                        try {
                            setAppConfig(AppConfig__JsonHelper.parseFromJson(cfgJson));
                        } catch (IOException e) {
                            L.e("parse app config failed", e);
                        }
                    }
                });
    }

    private void loadSavedAppConfig() {
        SharedPreferences shared = getSharedPreferences(PREFS_APP_STATE, MODE_PRIVATE);
        String cfgJson = shared.getString(PREF_KEY_APP_CONFIG, null);
        if (cfgJson != null) {
            try {
                appConfig = AppConfig__JsonHelper.parseFromJson(cfgJson);
            } catch (IOException e) {
                L.e("parse saved app config failed", e);
            }
        }

        if (appConfig == null) {
            appConfig = new AppConfig();
        }
    }

    private void saveAppConfig(AppConfig appConfig) {
        try {
            getSharedPreferences(PREFS_APP_STATE, MODE_PRIVATE).edit()
                    .putString(PREF_KEY_APP_CONFIG, AppConfig__JsonHelper.serializeToJson(appConfig))
                    .apply();
        } catch (IOException e) {
            L.e("save app config failed", e);
        }
    }

    public static void loadAdvertisingInfo() {
        Observable.zip(TrackingUtils.getAdvertisingId(), TrackingUtils.getFbAttributionId(),
                new Func2<AdvertisingIdClient.Info, String, Void>() {
                    @Override
                    public Void call(AdvertisingIdClient.Info adInfo, String fbAttrId) {
                        FiveMilesApp.advertisingIdInfo = adInfo;
                        FiveMilesApp.fbAttributionId = fbAttrId;

                        if (adInfo != null) {
                            L.d("google advertising id: %s, isLimitAdTrackingEnabled: %s",
                                    adInfo.getId(), adInfo.isLimitAdTrackingEnabled());
                        }

                        if (fbAttrId != null) {
                            L.d("facebook attribution id: %s", fbAttrId);
                        }
                        return null;
                    }
                }).subscribe(new SimpleObserver<>());
    }

    public static AdvertisingIdClient.Info getAdvertisingIdInfo() {
        return advertisingIdInfo;
    }

    public static String getFbAttributionId() {
        return fbAttributionId;
    }

    private void enableApiStats() {
        boolean trackApiDelay = getResources().getBoolean(R.bool.track_api_delay);
        boolean trackApiException = getResources().getBoolean(R.bool.track_api_exception);

        IntentFilter filter = new IntentFilter();
        if (trackApiDelay) {
            filter.addAction(RestObserver.ACT_RESP_RECEIVED);
        }

        if (trackApiException) {
            filter.addAction(RestObserver.ACT_REQ_FAILURE);
        }

        if (trackApiDelay || trackApiException) {
            LocalBroadcastManager.getInstance(this).registerReceiver(new ApiStatsReceiver(), filter);
        }
    }

	public static String getDeviceId(Context context) {
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(TELEPHONY_SERVICE);
        return tm.getDeviceId();
	}

    /**
     * 根据 http://developer.android.com/reference/android/net/http/HttpResponseCache.html
     * 当系统支持HttpResponseCache时打开缓存
     */
    private static void enableHttpResponseCache(Context context) {
        try {
            File cacheDir = new File(context.getCacheDir(), "http");
            long cacheSize = 10 * 1024 * 1024; // 10 MiB
            HttpResponseCache.install(cacheDir, cacheSize);
        }
        catch (Exception e) {
            L.e(e);
        }
    }

    private void installShortcut() {
        Intent shortcutIntent = new Intent(appCtx, GuidePagerActivity.class);
        shortcutIntent.setAction(Intent.ACTION_MAIN);
        shortcutIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Intent addIntent = new Intent();
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
        addIntent.putExtra("duplicate", false);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(appCtx, R.drawable.app_icon));
        appCtx.sendBroadcast(addIntent);

        SharedPreferences shared = getSharedPreferences(PREFS_APP_STATE, MODE_PRIVATE);
        int currVer = Utils.getPackageInfo().versionCode;
        shared.edit().putInt(PREF_KEY_SHORTCUT_V, currVer).apply();
    }

    private void removeShortcut() {
        Intent shortcutIntent = new Intent(appCtx, GuidePagerActivity.class);
        shortcutIntent.setAction(Intent.ACTION_MAIN);
        shortcutIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Intent removeIntent = new Intent();
        removeIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        removeIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        removeIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
        removeIntent.putExtra("duplicate", false);
        appCtx.sendBroadcast(removeIntent);
    }

    private boolean canInstallShortcut() {
        SharedPreferences shared = getSharedPreferences(PREFS_APP_STATE, MODE_PRIVATE);
        int shortcutVer = shared.getInt(PREF_KEY_SHORTCUT_V, 0);
        int currVer = -1;
        if (Utils.getPackageInfo() != null) {
            currVer = Utils.getPackageInfo().versionCode;
        }

        return currVer > shortcutVer;
    }

    /**
     * Extend the dependency container graph will new dependencies provided by the modules passed as
     * arguments.
     *
     * @param modules used to populate the dependency container.
     */
    public ObjectGraph plus(List<Object> modules) {
        if (modules == null) {
            throw new IllegalArgumentException(
                    "You can't plus a null module, review your getModules() implementation");
        }
        return objectGraph.plus(modules.toArray());
    }

    private void initializeDependencyInjector() {
        objectGraph = ObjectGraph.create(new RootModule(this));
        objectGraph.injectStatics();
        objectGraph.inject(this);
    }

    public static AppConfig getAppConfig() {
        if (appConfig == null) {
            appConfig = new AppConfig();
        }
        return appConfig;
    }

    public static void setAppConfig(AppConfig appConfig) {
        if (appConfig == null) {
            appConfig = new AppConfig();
        }
        FiveMilesApp.appConfig = appConfig;
        getInstance().saveAppConfig(appConfig);
    }

    @SuppressWarnings("unused")
    public static String getAndroidId() {
        return androidId;
    }
}
