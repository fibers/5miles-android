package com.insthub.fivemiles;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;

import com.facebook.Session;
import com.thirdrock.domain.GeoLocation;
import com.thirdrock.domain.IUser;
import com.thirdrock.domain.User;
import com.thirdrock.framework.util.session.Action;
import com.thirdrock.framework.util.session.PendingActionStore;

import java.util.Queue;

import static com.insthub.fivemiles.FiveMilesAppConst.EMPTY_STRING;
import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_GCM_INFO;
import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_USER_PRIVATE_DATA;
import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_USER_SUBSCRIPTION;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_DISABLED_FB_TOKEN_REMIND;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_FB_TOKEN_REMIND_TIME;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_LAUNCH_COUNT;
import static com.insthub.fivemiles.FiveMilesAppConst.SESSION_KEY_PENDING_ACTIONS;
import static com.insthub.fivemiles.FiveMilesAppConst.SP_IS_ANONY_LOGIN;
import static com.insthub.fivemiles.FiveMilesAppConst.SP_IS_GETUI_REGISTERED;
import static com.insthub.fivemiles.FiveMilesAppConst.SP_IS_PARSE_REGISTERED;
import static com.insthub.fivemiles.FiveMilesAppConst.SP_IS_SELLERS_NEARBY_SHOWN;
import static com.insthub.fivemiles.FiveMilesAppConst.SP_IS_WEL_SHOWN;
import static com.insthub.fivemiles.FiveMilesAppConst.USERINFO;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;

public class SESSION {

    private static final long FB_REMINDER_AGED_TIME = 86400000;  // 1 day to expire the reminder suspension

    public String userId;
//    public String anonyUserId;      // 匿名用户登陆后，anonyUserId和user_id一致。
    public boolean isAnonymous = false;     // 匿名登陆成功后会改为false，注册或登陆为正式用户（email, fb）后会将此值设为false。默认是false。
    public String token;
    public String nickname;
    public String avatarUrl;
    public String clientId;
    public String deviceToken;      // deviceToken from Parse, for server bind user_id with parse deviceToken
    public String installationId;   // parse中的installation id，每个安装的唯一标识
    public boolean isGeTuiRegistered = false;   // 是否注册个推到服务器
    public boolean isParseRegistered = false;   // 是否注册Parse到服务器

    public String homeBranchUrl;

    // sing up with email
    public String email;
    public long lastLogin;
    public long createdAt;

    public int launchCount;  // how many times the app is launched (from desktop)

    public static SESSION instance;

    public String fbUid;  // facebook user id
    private boolean fbVerified;  // 有fbUid就一定是fbVerified
    private boolean emlVerified;
    private boolean phoneVerified;
    private long fbTokenExpires;  // facebook token到期时间, ms
    private boolean fbTokenRemindDisabled;  // 用户是否选择了暂不relink facebook
    private long fbTokenRemindTime;  // 上次提示facebook token到期的时间 ms
    private String uuid;  // 客户端生成的用户唯一标示，主要用于绑定AppsFlyer appUserId

    private boolean isSellersNearbyHasShown;

    private static boolean isWelcomeShown; // Welcome页是否显示

    private IUser baseUser;
    private String country;// Zip 请求返回地理位置
    private String region;
    private String city;
    private float latitude = 0;
    private float longitude = 0;
    private String gcmSenderId;

    private PendingActionStore pendingActionStore = new PendingActionStore();

    public static SESSION getInstance() {
        if (instance == null) {
            instance = new SESSION();
            instance.load();
        }
        return instance;
    }

    public static void clear() {
        instance = null;

        // user_info暂不使用clear，以便保留身份信息以外的数据
        // is_welcome_shown暂不clear，用户logout之后直接进入首页
        FiveMilesApp.getInstance()
                .getSharedPreferences(USERINFO, Context.MODE_PRIVATE)
                .edit()
                .remove("installationId")
                .remove("deviceToken")
                .remove("uid")
                .remove("uuid")
                .remove("fbUid")
                .remove("nickname")
                .remove("avatar_url")
                .remove("token")
                .remove("created_at")
                .remove("last_login")
                .remove("email")
                .remove(ConstantS.CLIENT_ID)
                .remove("fbVerified")
                .remove("emlVerified")
                .remove("phoneVerified")
                .remove("fbTokenExpires")
                .remove("country")
                .remove("region")
                .remove("city")
                .remove("latitude")
                .remove("longitude")
                .remove(PREF_KEY_DISABLED_FB_TOKEN_REMIND)
                .remove(PREF_KEY_FB_TOKEN_REMIND_TIME)
                .remove(PREF_KEY_LAUNCH_COUNT)
                .remove(SESSION_KEY_PENDING_ACTIONS)
                .remove(SP_IS_ANONY_LOGIN)
                .remove(SP_IS_PARSE_REGISTERED)
                .remove(SP_IS_GETUI_REGISTERED)
                .apply();

        FiveMilesApp.getInstance()
                .getSharedPreferences(PREFS_USER_SUBSCRIPTION, Context.MODE_PRIVATE)
                .edit().clear().apply();

        FiveMilesApp.getInstance()
                .getSharedPreferences(PREFS_USER_PRIVATE_DATA, Context.MODE_PRIVATE)
                .edit().clear().apply();

        FiveMilesApp.getInstance()
                .getSharedPreferences(PREFS_GCM_INFO, Context.MODE_PRIVATE)
                .edit().clear().apply();
    }

    public void save() {
        doSave().apply();
    }

    public void saveSync() {
        doSave().commit();
    }

    @SuppressLint("CommitPrefEdits")
    private SharedPreferences.Editor doSave() {
        return FiveMilesApp.getInstance().getSharedPreferences(USERINFO, 0)
                .edit()
                .putString("installationId", installationId)
                .putString(ConstantS.CLIENT_ID, clientId)
                .putString("deviceToken", deviceToken)
                .putString("uid", userId)
                .putString("uuid", uuid)
                .putString("fbUid", fbUid)
                .putString("nickname", nickname)
                .putString("avatar_url", avatarUrl)
                .putString("token", token)
                .putLong("created_at", createdAt)
                .putLong("last_login", lastLogin)
                .putString("email", email)
                .putBoolean("fbVerified", fbVerified)
                .putBoolean("emlVerified", emlVerified)
                .putBoolean("phoneVerified", phoneVerified)
                .putLong("fbTokenExpires", fbTokenExpires)
                .putString("country", country)
                .putString("region", region)
                .putString("city", city)
                .putFloat("latitude", latitude)
                .putFloat("longitude", longitude)
                .putBoolean(PREF_KEY_DISABLED_FB_TOKEN_REMIND, fbTokenRemindDisabled)
                .putLong(PREF_KEY_FB_TOKEN_REMIND_TIME, fbTokenRemindTime)
                .putInt(PREF_KEY_LAUNCH_COUNT, launchCount)
                .putString(SESSION_KEY_PENDING_ACTIONS, pendingActionStore.toJsonString())
                .putBoolean(SP_IS_WEL_SHOWN, isWelcomeShown)
                .putBoolean(SP_IS_ANONY_LOGIN, isAnonymous)
                .putBoolean(SP_IS_GETUI_REGISTERED, isGeTuiRegistered)
                .putBoolean(SP_IS_PARSE_REGISTERED, isParseRegistered)
                .putBoolean(SP_IS_SELLERS_NEARBY_SHOWN, isSellersNearbyHasShown);   // 匿名登陆后会调用doSave()，会导致这个值变为false
    }

    private void load() {
        SharedPreferences shared;
        shared = FiveMilesApp.getInstance().getSharedPreferences(USERINFO, 0);
        clientId = shared.getString(ConstantS.CLIENT_ID, "");
        installationId = shared.getString("installationId", "");
        deviceToken = shared.getString("deviceToken", "");
        userId = shared.getString("uid", null);
        uuid = shared.getString("uuid", null);
        fbUid = shared.getString("fbUid", null);
        nickname = shared.getString("nickname", nickname);
        avatarUrl = shared.getString("avatar_url", EMPTY_STRING);
        token = shared.getString("token", "");
        email = shared.getString("email", "");
        lastLogin = shared.getLong("lastLogin", 0);
        createdAt = shared.getLong("createdAT", 0);
        fbVerified = isNotEmpty(fbUid) || shared.getBoolean("fbVerified", false);
        emlVerified = shared.getBoolean("emlVerified", false);
        phoneVerified = shared.getBoolean("phoneVerified", false);
        fbTokenExpires = shared.getLong("fbTokenExpires", 0);
        fbTokenRemindDisabled = shared.getBoolean(PREF_KEY_DISABLED_FB_TOKEN_REMIND, false);
        fbTokenRemindTime = shared.getLong(PREF_KEY_FB_TOKEN_REMIND_TIME, 0);
        loadPendingActions(shared);
        loadLaunchCount();
        isWelcomeShown = shared.getBoolean(SP_IS_WEL_SHOWN, false);
        isAnonymous = shared.getBoolean(SP_IS_ANONY_LOGIN, false);
        isGeTuiRegistered = shared.getBoolean(SP_IS_GETUI_REGISTERED, false);
        isParseRegistered = shared.getBoolean(SP_IS_PARSE_REGISTERED, false);
        isSellersNearbyHasShown = shared.getBoolean(SP_IS_SELLERS_NEARBY_SHOWN, false);
        country = shared.getString("country", "");
        region = shared.getString("region", "");
        city = shared.getString("city", "");
        latitude = shared.getFloat("latitude", 0);
        longitude = shared.getFloat("longitude",0);
    }

    private void loadPendingActions(SharedPreferences shared) {
        pendingActionStore = PendingActionStore.fromJsonString(shared.getString(SESSION_KEY_PENDING_ACTIONS, "{}"));
    }

    /**
     * Whether this session is authenticated.
     */
    public boolean isAuth() {
        return (!isAnonymous) && isNotEmpty(userId);
    }

    public boolean isFbLogin() {
        return (!isAnonymous) && isNotEmpty(fbUid);
    }

    public String getFbUid() {
        return fbUid;
    }

    public void setFbUid(String fbUid) {
        this.fbUid = fbUid;
        this.fbVerified = isNotEmpty(fbUid);
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isFbVerified() {
        return fbVerified;
    }

    public long getFbTokenExpires() {
        return fbTokenExpires;
    }

    public void setFbTokenExpires(long fbTokenExpires) {
        this.fbTokenExpires = fbTokenExpires;
    }

    public SESSION fbTokenExpires(long ts) {
        setFbTokenExpires(ts);
        return this;
    }

    public SESSION fbVerified(boolean verified) {
        setFbVerified(verified);
        return this;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public SESSION emlVerified(boolean verified) {
        setEmlVerified(verified);
        return this;
    }

    public SESSION phoneVerified(boolean verified) {
        setPhoneVerified(verified);
        return this;
    }

    public void setFbVerified(boolean fbVerified) {
        this.fbVerified = fbVerified;
    }

    public boolean isEmlVerified() {
        return emlVerified;
    }

    public void setEmlVerified(boolean emlVerified) {
        this.emlVerified = emlVerified;
    }

    public boolean isPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public boolean isVerified() {
        return isPhoneVerified();
    }

    /**
     * 使用Build.SERIAL取代UUID，Push使用的device_id也是此值，并保持此ID不变
     * @return Build.SERIAL
     */
    public String getUuid() {
        return Build.SERIAL;
    }

    public int loadLaunchCount() {
        if (isAuth()) {
            launchCount = FiveMilesApp.getInstance().getSharedPreferences(USERINFO, Context.MODE_PRIVATE)
                    .getInt(PREF_KEY_LAUNCH_COUNT, 0);
        }
        return launchCount;
    }

    public void saveLaunchCount() {
        FiveMilesApp.getInstance().getSharedPreferences(USERINFO, Context.MODE_PRIVATE)
                .edit().putInt(PREF_KEY_LAUNCH_COUNT, launchCount).apply();
    }

    public Queue<Action> getPendingActions(String key) {
        return pendingActionStore.getPendingActions(key);
    }

    public SESSION queuePendingAction(String key, Action action) {
        pendingActionStore.queuePendingAction(key, action);
        return this;
    }

    public SESSION removePendingActions(String key) {
        pendingActionStore.removePendingActions(key);
        return this;
    }

    public SESSION fbTokenRemindDisabled(boolean disabled) {
        setFbTokenRemindDisabled(disabled);
        return this;
    }

    public boolean isFbTokenRemindDisabled() {
        return fbTokenRemindDisabled;
    }

    public void setFbTokenRemindDisabled(boolean disabled) {
        this.fbTokenRemindDisabled = disabled;
        this.fbTokenRemindTime = disabled ? System.currentTimeMillis() : 0;
    }

    public boolean isTimeToRemindFbToken() {
        return !fbTokenRemindDisabled || (System.currentTimeMillis() - fbTokenRemindTime) >= FB_REMINDER_AGED_TIME;
    }

    public boolean isFbTokenExpired() {
        Session fbSession = Session.getActiveSession();
        if (isFbLogin() && (fbSession == null || !fbSession.isOpened())) {
            return true;
        }

        long fbExpires = getFbTokenExpires();
        return isFbVerified() && fbExpires <= System.currentTimeMillis();
    }

    public void setWelcomeShown(boolean isWelcomeShown) {
        this.isWelcomeShown = isWelcomeShown;
    }

    public boolean isSellersNearbyHasShown() {
        return isSellersNearbyHasShown;
    }

    public void setSellersNearbyHasShown(boolean isSellersNearbyShouldShow) {
        this.isSellersNearbyHasShown = isSellersNearbyShouldShow;
        this.save();    // TODO 也许放到Activity的生命周期方法更合适
    }

    public IUser getSelf() {
        if (baseUser == null) {
            synchronized (SESSION.class) {
                if (baseUser == null) {
                    baseUser = new User(getUserId(), getNickname(), isVerified(), getAvatarUrl());
                }
            }
        }
        return baseUser;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        SESSION.this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        SESSION.this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        SESSION.this.country = country;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public void setGeoLocation(GeoLocation geolocation){
        city = geolocation.getCity();
        region = geolocation.getRegion();
        country = geolocation.getCountry();
        latitude = geolocation.getLatitude();
        longitude = geolocation.getLongitude();
    }

    public GeoLocation getGeoLocation(){
        return new GeoLocation(latitude,longitude,country,region,city);
    }

    public boolean isGeoLocationEmpty(){
        return (TextUtils.isEmpty(city)&&TextUtils.isEmpty(region)&&TextUtils.isEmpty(country));
    }

    public String getGcmSenderId() {
        return gcmSenderId;
    }
}
