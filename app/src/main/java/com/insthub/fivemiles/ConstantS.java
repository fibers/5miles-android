package com.insthub.fivemiles;

public interface ConstantS {

	// 应用的key 请到官方申请正式的appkey替换APP_KEY
	public static final String SINA_APP_KEY = "386263849";

	// 替换为开发者REDIRECT_URL
	public static final String SINA_REDIRECT_URL = "https://api.weibo.com/oauth2/default.html";

	// 新支持scope：支持传入多个scope权限，用逗号分隔
	public static final String SINA_SCOPE = "email,direct_messages_read,direct_messages_write,"
			+ "friendships_groups_read,friendships_groups_write,statuses_to_me_read,"
			+ "follow_app_official_microblog," + "invitation_write";
	public static final int SHARE_TYPE_SINA = 2;
	public static final int SHARE_TYPE_TENCENT = 1;
	
	// 腾讯微博
	public static String TENCENT_WEIBO_REDIRECT_URI = "http://127.0.0.1"; // 回调地址
	public static String TENCENT_WEIBO_CLIENTID = "801448557"; // APP KEY
	public static String TENCENT_WEIBO_CLIENT_SECRET = "4c8e9e8bb8333f3a9decd76844e96aab";// SECRET

	public static final String CLIENT_ID = "client_id";
	public static final String RESPONSE_TYPE = "response_type";
	public static final String USER_REDIRECT_URL = "redirect_uri";
	public static final String DISPLAY = "display";
	public static final String USER_SCOPE = "scope";
	public static final String PACKAGE_NAME = "packagename";
	public static final String KEY_HASH = "key_hash";

	public static final String BAIDU_USERID = "baidu_userid";
	public static final String DEVICE_UUID = "device_uuid";

	// 在百度开发者中心查询应用的API Key
	public static final String API_KEY = "urBOsgrFhQePMQ3z4Cw43Fpm";
    public static final String API_KEY_TEST = "urBOsgrFhQePMQ3z4Cw43Fpm";

	public static final String WEIXIN_APP_ID = "wx0c38217b16c08df7";
	public static final String WEIXIN_APP_KEY = "3357aeeff79264bdf10477bb6189dade";

    //QQ Key
    public static final String QQZone_API_ID="1101675771";
    public static final String QQZone_API_KEY="n36E9W0QzbJxZpmR";

    // Parse params
    public static final String DEVICE_TOKEN = "device_token";  // Parse中的deviceToken
    public static final String INSTALLATION_ID = "installation_id"; // Parse生成的installation id

    // home url of 5miles
    public static final String HOME_URL = "https://bnc.lt/5miles-v2-e9d31c";    // 不含用户信息的Branch url

    // 5miles icon url
    public static final String DEFAULT_LOGO_URL = "" + "/images/5miles_facebook_xx.png";

    public static final String DESKTOP_HOME_URL = "http://www.5milesapp.com";

    // branch param中的key
    public static final String USER_ID = "user_id";
    public static final String SHARE_TYPE = "share_type";
    public static final String EVENT_ID = "event_id";
    public static final String BRANCH_DESKTOP_URL = "$desktop_url";
    public static final String BRANCH_OG_URL = "$og_url";
}
