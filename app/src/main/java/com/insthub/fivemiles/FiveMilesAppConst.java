package com.insthub.fivemiles;

import android.content.Context;
import android.os.Environment;

import com.thirdrock.domain.CategoryInfo;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FiveMilesAppConst {
	public static final String FILEPATH = Environment.getExternalStorageDirectory() + "/.5Miles/.cache/";
	public static final String IMAGECACHE = Environment.getExternalStorageDirectory()+ "/.5Miles/.cache/.img/";
	public static final String CAMERACACHE = Environment.getExternalStorageDirectory() + "/5miles/5miles_album/";
    public static final String VOICECACHE = Environment.getExternalStorageDirectory()+ "/.5Miles/.cache/.voice/";
    public static final String CATG_ICON_DIR_NAME = "category_icons";  // category icons dir

    public static final int ITEM_MAX_IMAGES = 6;
    // 上传图片的最长边的最大值和宽度的最大值，以Nexus6为准。超过这个宽度的照片在大部分手机上是没有意义的
    public static final int IMAGE_MAX_HEIGHT = 2560;
    public static final int IMAGE_MAX_WIDTH = 1440;

    public static final int MAX_NICKNAME_LEN = 15;

    // https://developers.google.com/maps/documentation/staticmaps/#scale_values
    public static final int GOOGLE_STATIC_MAP_SCALE = 2;
	
    public static final String EMPTY_STRING = "";

	public static final String USERINFO = "user_info";
    public static final String SESSION_KEY_PENDING_ACTIONS = "session_pending_actions";
    public static final String USER_ID_ME = "com.insthub.fivemiles.USER_ID_ME";  // a special user id, representing the current user

    public static final String PREFS_APP_STATE = "app_state";
    public static final String PREF_KEY_SHORTCUT_V = "app_shortcut_version";
    public static final String PREF_KEY_LAUNCH_COUNT = "app_launch_count";
    public static final String PREF_KEY_DISABLED_FB_TOKEN_REMIND = "disabled_fb_token_remind";
    public static final String PREF_KEY_FB_TOKEN_REMIND_TIME = "fb_token_remind_time";
    public static final String PREF_KEY_CLOSED_AD = "closed_ad_campaign";
    public static final String PREF_KEY_CLOSED_SEARCH_AD = "closed_search_ad_campaign";
    public static final String PREF_KEY_CURR_LAUNCH_AD = "current_launch_ad_campaign";
    public static final String PREF_KEY_LAST_UPDATE_CHECK_V = "update_check_app_version";
    public static final String PREF_KEY_LAST_UPDATE_CHECK_TIME = "update_check_time";
    public static final String PREF_KEY_HAS_UPDATE = "update_check_has_update";
    public static final String PREF_KEY_AUTO_SHARE = "auto_share_new_item";
    public static final String PREF_KEY_DONT_REMIND_VERIFICATION = "never_remind_verification";
    public static final String PREF_KEY_APP_CONFIG = "app_config";
    public static final String PREF_KEY_APP_RATED = "app_already_rated";
    public static final String PREF_KEY_IS_FIRST_MARK_SOLD = "fist_mark_as_sold";
    public static final String PREF_KEY_IS_FIRST_FIVE_STAR = "first_five_star_review";

    // app data keys in SP, such as categories
    public static final String PREFS_APP_DATA = "app_data";
    public static final String PREF_KEY_CATGRY_RESP = "categories_resp";
    public static final String PREF_KEY_NEW_CATEGORIES = "new_categories";
    public static final String PREF_KEY_REASONS_ITEM = "report_reasons_item";
    public static final String PREF_KEY_REASONS_UESR = "report_reasons_user";
    public static final String PREF_KEY_REASONS_REVIEW = "report_reasons_review";
    public static final String PREF_KEY_REASONS_ALL = "report_reasons";

    // 存放较为敏感的key-value数据，注销时需清理
    public static final String PREFS_USER_PRIVATE_DATA = "user_private_data";

    public static final String PREFS_USER_SUBSCRIPTION = "user_subscription_state";
    public static final String PREF_KEY_USER_SUBSCRIPTION_V = "user_subscription_state_version";
    public static final int USER_SUBSCRIPTION_V = 1;

    public static final String PREFS_GCM_INFO = "gcm_info";
    public static final String PREF_KEY_GCM_SENDER_ID = "gcm_defaultSenderId";
    public static final String PREF_KEY_APP_VERSION = "app_version_code";

    public static final String ACT_GOTO_HOME = "goto_home_view";
    public static final String ACT_REFRESH_HOME_TAB = "refresh_home_tab";
    public static final String ACT_EDIT = "edit";
    public static final String ACT_LINK_FB = "link_facebook";
    public static final String ACT_REMIND_VERIFICATION = "remind_verification";
    public static final String ACT_REVIEWS = "show_reviews";

    public static final String ACT_PUSH_OFFER_PREFIX = "push_offer_";
    public static final String ACT_PUSH_ITEM_PREFIX = "push_item_";
    public static final String ACT_PUSH_COMMENT_PREFIX = "push_comment_";
    public static final String ACT_PUSH_MY_PROFILE = "push_profile_me";
    public static final String ACT_PUSH_USER_PREFIX = "push_user_";
    public static final String ACT_PUSH_FEATURED_COLLECTION_PREFIX = "featured_collection_";
    public static final String ACT_PUSH_LINK_PREFIX = "push_link_";

    public static final String ACT_PARA_REF = "referral_view";
    public static final String ACT_PARA_TAB_TAG_SPEC = "tab_tag_spec";
    public static final String ACT_PARA_MSG_LIST = "message_list";
    public static final String ACT_PARA_S_KEYWORD = "s_keyword";
    public static final String ACT_PARA_S_CATEGORY = "s_category";
    public static final String ACT_PARA_S_BRAND_NAME = "s_brand_name";
    public static final String ACT_PARA_S_BRAND_ID = "s_brand_id";
    public static final String ACT_PARA_S_CAMPAIGN_ID = "s_campaign_id";
    public static final String ACT_PARA_USER = "user";
    public static final String ACT_PARA_USER_ID = "user_id";
    public static final String ACT_PARA_ITEM = "item";
    public static final String ACT_PARA_ITEM_THUMB = "item_thumb";
    public static final String ACT_PARA_ITEM_ID = "itemId";
    public static final String ACT_PARA_RF_TAG = "rfTag";
    public static final String ACT_PARA_OFFER_LINE_ID = "offerLineId";
    public static final String ACT_PARA_SHOULD_POP_KEYBOARD = "should_auto_pop_keyboard";
    public static final String ACT_PARA_OPPOSITE_USER = "oppersiteUser";
    public static final String ACT_PARA_MSG_ID = "messageId";
    public static final String ACT_PARA_BUYING_COUNT = "buying_count";
    public static final String ACT_PARA_SELLING_COUNT = "selling_count";
    public static final String ACT_PARA_FOLLOWING_COUNT = "following_count";
    public static final String ACT_PARA_FOLLOWER_COUNT = "follower_count";
    public static final String ACT_PARA_LIKES_COUNT = "likes_count";
    public static final String ACT_PARA_SELLER_RATE_COUNT = "seller_rate_count";
    public static final String ACT_PARA_SELLER_RATE_AVG = "seller_rate_avg";
    public static final String ACT_PARA_SELECTION_DATA_LIST = "selection_data_list";
    public static final String ACT_PARA_SELECTION_TITLE = "selection_title";
    public static final String ACT_PARA_SELECTION_SCREEN_NAME = "selection_screen_name";
    public static final String ACT_PARA_SELECTION_USE_FAST_SCROLL = "selection_use_fast_scroll";
    public static final String ACT_PARA_CAN_GO_BACK = "canNavigateBack";
    public static final String ACT_PARA_REDIRECT_INTENT = "redirect_intent";  // redirection target (after login)
    public static final String ACT_PARA_SHOW_FB_LIKES = "show_facebook_likes";  // redirection target (after login)
//    public static final String ACT_PARA_SHOW_RATE_APP = "show_app_market_rate";
    public static final String ACT_PARA_PAGE_TITLE = "page_title";
    public static final String ACT_PARA_PAGE_URL = "page_url";
    public static final String ACT_PARA_COLLECTION_ID = "collection_id";
    public static final String ACT_PARA_FB_VERIFIED = "facebook_verified";
    public static final String ACT_PARA_USER_VERIFIED = "user_verified";
    public static final String ACT_PARA_VERIFIED_PHONE = "verified_phone_number";
    public static final String ACT_PARA_VERIFICATION_REMIND_TIME = "verification_remind_time";
    public static final String ACT_PARA_REPORT_TYPE = "report_type";
    public static final String ACT_PARA_REPORTED_OID = "reported_object_id";
    public static final String ACT_PARA_HOME_TAB = "home_tab_name";
    public static final String ACT_PARA_NICK_NAME = "nickname";
    public static final String ACT_PARA_EMAIL = "email";
    public static final String ACT_PARA_BACK_ENABLED = "sellers_nearby_back_enabled";
    public static final String ACT_PARA_IMAGE_PATHS = "all_path";
    public static final String ACT_PARA_SHOW_SHARE = "show_share";
    public static final String ACT_PARA_TOP_SHARE_TEXT = "top_view_share_text_content";

    public static final String ACT_RESULT_SELECTION_INDEX = "selection_index";
    public static final String ACT_RESULT_SELECTION_ITEM = "selection_item";
    public static final String ACT_RESULT_CURRENCY = "currency_name";
    public static final String ACT_RESULT_VERIFIED_PHONE = "verified_phone_number";

    @Deprecated
    public static final String ACT_PARA_HOME_OBJ = "home_object";  // use new domain model

    public static final String FB_PERMISSION_PUBLIC = "public_profile";
    public static final String FB_PERMISSION_PUBLISH = "publish_actions";
    public static final String FB_PERMISSION_EMAIL = "email";
    public static final String FB_PERMISSION_FRIENDS = "user_friends";

    // ----------------------------
    // Home Tab WebView Url
    //
    public static final String HOME_SERVICE_URL = "http://m.5milesapp.com/app/service";
    public static final String HOME_HOUSING_URL = "http://m.5milesapp.com/app/house";
    public static final String HOME_JOB_URL = "http://m.5milesapp.com/app/job";

    public static final String HOME_REVIEW_PAGE_URL = "http://m.5milesapp.com/app/reviewTopic";
    public static final String HOME_REVIEW_SHARE_URL = "https://5milesapp.com/review";

    // ------------------------
    // Screen names
    //
    public static final String VIEW_HOME = "home_view";
    public static final String VIEW_LIST_ITEM = "sell_view";
    public static final String VIEW_ITEM = "product_view";
    public static final String VIEW_NOTIFICATIONS = "notifications_view";
    public static final String VIEW_REPORT_ITEM = "reportitem_view";
    public static final String VIEW_REPORT_SELLER = "reportseller_view";
    public static final String VIEW_REPORT_REVIEW = "reportreview_view";
    public static final String VIEW_PHOTO_ALBUM = "photoalbum_view";
    public static final String VIEW_BRAND = "brandselect_view";
    public static final String VIEW_DELIVERY = "deliveryselect_view";
    public static final String VIEW_COUNTRY = "phoneselectcountry_view";
    public static final String VIEW_SETTING = "setting_view";
    public static final String VIEW_CURRENCY = "currency_view";
    public static final String VIEW_MY_ITEMS = "myitems_view";
    public static final String VIEW_FOLLOWING = "following_view";
    public static final String VIEW_FOLLOWER = "followers_view";
    public static final String VIEW_FEATURED_COLLECTION = "producttopic_view";
    public static final String VIEW_VERIFY_PHONE = "verifyphone_view";
    public static final String VIEW_SEARCH_RESULT = "searchresult_view";
    public static final String VIEW_BUY = "buy_view";
    public static final String VIEW_GLIDE_2 = "guide2_view";
    public static final String VIEW_GLIDE_3 = "guide3_view";
    public static final String VIEW_GLIDE_4 = "guide4_view";
    public static final String VIEW_LOG_IN  = "login_view";
    public static final String VIEW_SELLERS_NEARBY = "sellersnearby";
    public static final String VIEW_TX_REVIEW  = "review_view";
    public static final String VIEW_REVIEW_LIST  = "myreviews_view";
    public final static String VIEW_RATE_POPUP = "rate_5miles_popup";
    public static final String VIEW_MAKE_OFFER = "chat_view";
    public final static String VIEW_MESSAGE = "message_view";
    public final static String VIEW_SEARCH = "discovery_view";

    // ------------------------
    // User education screen version numbers
    //
    public static final String PREF_EDU_MAIN_TAB = "PREF_EDU_MAIN_TAB";
    public static final int EDU_MAIN_TAB_V = 1;
    public static final String PREF_EDU_VERIFICATION = "PREF_EDU_VERIFICATION";
    public static final int EDU_VERIFICATION_V = 1;
    public static final String CHAT_EDU_REVIEW = "CHAT_EDU_REVIEW";
    public static final int EDU_CHAT_REVIEW_V = 1;
    public static final String ITEM_EDU_VERIFY_RENEW_KEY = "ITEM_EDU_VERIFY_RENEW";
    public static final int ITEM_EDU_VERIFY_RENEW_V = 1;
    public static final String PROFILE_EDU_REVIEW_KEY = "PROFILE_EDU_REVIEW";
    public static final int PROFILE_EDU_REVIEW_V = 1;

    // ------------------------
    // Time Unit
    public static final int SECOND_IN_MILLS     = 1000;
    public static final int MINUTE_IN_SECOND    = 60;
    public static final int HOUR_IN_MINUTE      = 60;

    public static final long ONE_MINUTE_MILLS = MINUTE_IN_SECOND * SECOND_IN_MILLS;

    public static final long HOUR_IN_SECOND     = HOUR_IN_MINUTE * MINUTE_IN_SECOND;
    public static final long DAY_IN_SECOND      = 24 * HOUR_IN_SECOND;
    public static final long MONTH_IN_SECOND    = 31 * DAY_IN_SECOND;

    // ------------------------
    // Time
    public static final long DEFAULT_UPDATE_INTERVAL = 4 * HOUR_IN_SECOND;

    // ------------------------
    // Push Provider
    public static final String PARSE_PROVIDER = "parse";
    public static final String GETUI_PROVIDER = "getui";

    // ------------------------------------------------
    // Category
    public static final String CATEGORY_ID = "category_id";
    public static final String CATEGORY_TITLE = "category_title";

    // ------------------------------------------------
    public static final double USERS_NEARBY_BOUND_IN_MILES = 60;

    // ------------------------------------------------
    // Version code
    public static final int VERSION_CODE_300 = 300;

    // ------------------------------------------------
    // double comparsion
    public static final double EPSILON = 0.01;  // 价格的最高精度，用于double做比较

    // Shared Preferences key
    public static final String SP_IS_WEL_SHOWN = "is_welcome_shown";
    public static final String SP_IS_ANONY_LOGIN = "is_anony_login";
    public static final String SP_IS_GETUI_REGISTERED = "is_getui_registered";
    public static final String SP_IS_PARSE_REGISTERED = "is_parse_registered";
    public static final String SP_IS_SELLERS_NEARBY_SHOWN = "is_sellers_nearby_page_should_show";

    public static final int DEFAULT_CATEGORY_ID = 1;       // 获取类别id出错时使用此默认值

    public static final String FM_DB_BRAND = "fm_brand";
    public static final int GOOGLE_MAP_ZOOM_LEVEL = 16;

    public static String imageName() {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
		Date date = new Date();
		String time = format.format(date);
		String imageName = "IMG_" + time + ".jpg";
		return imageName;
	}

	public static String videoName() {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
		Date date = new Date();
		String time = format.format(date);
		String imageName = "Video_" + time + ".mp4";
		return imageName;
	}

    public static File getCategoryIconPath(CategoryInfo catg) {
        Context ctx = FiveMilesApp.getInstance();
        File dir = ctx.getDir(CATG_ICON_DIR_NAME, Context.MODE_PRIVATE);
        return new File(dir, String.valueOf(catg.getId()));
    }

    // error codes
    public static final int ERR_BLOCKED = 9001;
}
