
package com.insthub.fivemiles.Protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class FEEDBACK implements Serializable
{
	 public int id;
     public int rate_num;
     public String comment;
     public long created_at;
     public BUYER rater;

     public void  fromJson(JSONObject jsonObject)  throws JSONException
     {
          if(null == jsonObject){
            return ;
           }

          this.id = jsonObject.optInt("id");
          this.rate_num = jsonObject.optInt("score");
          this.comment = jsonObject.optString("text");
          this.created_at = jsonObject.optLong("created_at") * 1000;    // API返回的是Unix纪元的秒数，而不是毫秒数

          BUYER rater = new BUYER();
          rater.fromJson(jsonObject.optJSONObject("rater"));
          this.rater = rater;
          return ;
     }

     public JSONObject  toJson() throws JSONException 
     {
          JSONObject localItemObject = new JSONObject();
          JSONArray itemJSONArray = new JSONArray();
         
          localItemObject.put("id", id);
          localItemObject.put("score", rate_num);
          localItemObject.put("text", comment);
          localItemObject.put("created_at", created_at/1000);
          if(null != rater)
          {
            localItemObject.put("rater", rater.toJson());
          }
          return localItemObject;
     }

}
