package com.insthub.fivemiles.Protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class CommodityHomeResponse implements Serializable {

	public String error;

	public String error_description;

	public ArrayList<HOME_OBJECTS> objects = new ArrayList<HOME_OBJECTS>();

	public META meta;

	public void fromJson(JSONObject jsonObject) throws JSONException {
		if (null == jsonObject) {
			return;
		}

		this.error = jsonObject.optString("error");
		this.error_description = jsonObject.optString("error_description");

		JSONArray subItemArray;
		subItemArray = jsonObject.optJSONArray("objects");
		if (null != subItemArray) {
			for (int i = 0; i < subItemArray.length(); i++) {
				JSONObject subItemObject = subItemArray.getJSONObject(i);
				HOME_OBJECTS subItem = new HOME_OBJECTS();
				subItem.fromJson(subItemObject);
				this.objects.add(subItem);
			}
		}

		META meta = new META();
		meta.fromJson(jsonObject.optJSONObject("meta"));
		this.meta = meta;
	}
    public void fromJson(JSONArray jsonArray) throws JSONException {


        if (null != jsonArray) {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject subItemObject = jsonArray.getJSONObject(i);
                HOME_OBJECTS subItem = new HOME_OBJECTS();
                subItem.fromJson(subItemObject);
                this.objects.add(subItem);
            }
        }
    }

}
