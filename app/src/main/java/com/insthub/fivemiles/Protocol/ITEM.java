
package com.insthub.fivemiles.Protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class ITEM implements Serializable
{
     public int id;

     public String   title;

     public String   thumbnail;

     public String price;

     public String   currency;
     
     public int status;

     public void  fromJson(JSONObject jsonObject)  throws JSONException
     {
          if(null == jsonObject){
            return ;
           }

          JSONArray subItemArray;

          this.id = jsonObject.optInt("id");

          this.title = jsonObject.optString("title");

          this.thumbnail = jsonObject.optString("thumbnail");

          this.price = jsonObject.optString("price");

          this.currency = jsonObject.optString("currency");
          this.status = jsonObject.optInt("status");
          return ;
     }

     public JSONObject  toJson() throws JSONException 
     {
          JSONObject localItemObject = new JSONObject();
          JSONArray itemJSONArray = new JSONArray();
          localItemObject.put("id", id);
          localItemObject.put("title", title);
          localItemObject.put("thumbnail", thumbnail);
          localItemObject.put("price", price);
          localItemObject.put("currency", currency);
          localItemObject.put("status", status);
          return localItemObject;
     }

}
