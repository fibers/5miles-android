package com.insthub.fivemiles.Protocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by ywu on 14-8-18.
 */
@Deprecated
public class OfferLine implements Serializable {

    public int id;
    public String description;
    public USER buyer;

    public void fromJson(JSONObject jo) throws JSONException {
        id = jo.optInt("id");
        description = jo.optString("description");
        buyer = new USER();
        buyer.fromJson(jo.optJSONObject("buyer"));
    }

}
