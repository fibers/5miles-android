package com.insthub.fivemiles.Protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class ITEMS_OBJECTS implements Serializable {

    public static final String TAG = "MODEL";

	public int id;
	public String title;
	public String norm_id;
	public String fixed_price;
	public String update_time;
	public String added_time;
	public String description;
	public OWNER owner;
    public String distance;
    public double longitude;
	public double latitude;
	public int status;

    public ArrayList<IMAGES> images = new ArrayList<IMAGES>();

	public void fromJson(JSONObject jsonObject) throws JSONException {
		if (null == jsonObject) {
			return;
		}

		this.id = jsonObject.optInt("id");
		this.distance = jsonObject.optString("distance");
		this.title = jsonObject.optString("title");
		this.norm_id = jsonObject.optString("norm_id");
		this.fixed_price = jsonObject.optString("local_price");
		this.update_time = jsonObject.optString("update_time");
		this.added_time = jsonObject.optString("created_at");
		this.description = jsonObject.optString("description");
        this.status = jsonObject.optInt("status");

		OWNER owner = new OWNER();
		owner.fromJson(jsonObject.optJSONObject("owner"));
		this.owner = owner;

        JSONObject coordins = jsonObject.optJSONObject("location");
        if (coordins != null) {
            this.longitude = coordins.optDouble("lon");
            this.latitude = coordins.optDouble("lat");
        }

		JSONArray subItemArray = jsonObject.optJSONArray("images");
		if (null != subItemArray) {
			for (int i = 0; i < subItemArray.length(); i++) {
				JSONObject subItemObject = subItemArray.getJSONObject(i);
				IMAGES subItem = new IMAGES();
				subItem.fromJson(subItemObject);
				this.images.add(subItem);
			}
		}
		return;
	}
	
	public JSONObject  toJson() throws JSONException 
    {
         JSONObject localItemObject = new JSONObject();
         JSONArray itemJSONArray = new JSONArray();
         localItemObject.put("id", id);
         localItemObject.put("distance", distance);
         localItemObject.put("title", title);
         localItemObject.put("norm_id", norm_id);
         localItemObject.put("localPrice", fixed_price);
         localItemObject.put("update_time", update_time);
         localItemObject.put("created_at", added_time);
         localItemObject.put("description", description);
         localItemObject.put("longitude", longitude);
         localItemObject.put("latitude", latitude);

         return localItemObject;
    }

}
