package com.insthub.fivemiles.Protocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class META implements Serializable {

	public int limit;

	public String previous;

	public String next;

	public int offset;

	public int total_count;

	public void fromJson(JSONObject jsonObject) throws JSONException {
		if (null == jsonObject) {
			return;
		}

		this.limit = jsonObject.optInt("limit");

		this.previous = jsonObject.optString("previous");
        previous = previous != null && previous.equalsIgnoreCase("null") ? null : previous;

		this.next = jsonObject.optString("next");
        next = next != null && next.equalsIgnoreCase("null") ? null : next;

		this.offset = jsonObject.optInt("offset");

		this.total_count = jsonObject.optInt("total_count");
		return;
	}

}
