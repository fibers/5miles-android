
package com.insthub.fivemiles.Protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class OFFER implements Serializable
{

     public int id;

     public String   desc;

     public BUYER   buyer;

     public void  fromJson(JSONObject jsonObject)  throws JSONException
     {
          if(null == jsonObject){
            return ;
           }

          JSONArray subItemArray;

          this.id = jsonObject.optInt("id");

          this.desc = jsonObject.optString("desc");
          BUYER  buyer = new BUYER();
          buyer.fromJson(jsonObject.optJSONObject("buyer"));
          this.buyer = buyer;
          return ;
     }

     public JSONObject  toJson() throws JSONException 
     {
          JSONObject localItemObject = new JSONObject();
          JSONArray itemJSONArray = new JSONArray();
          localItemObject.put("id", id);
          localItemObject.put("desc", desc);
          if(null != buyer)
          {
            localItemObject.put("buyer", buyer.toJson());
          }
          return localItemObject;
     }

}
