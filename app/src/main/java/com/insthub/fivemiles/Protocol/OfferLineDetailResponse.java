
package com.insthub.fivemiles.Protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class OfferLineDetailResponse implements Serializable {
    public volatile USER buyer, owner;
    public volatile HOME_OBJECTS item;
    public volatile boolean soldToOthers;
    public volatile boolean userBlocked;
    public volatile int latestTs;  // timestamp of latest message
    public volatile ArrayList<TIMELINE> timeline = new ArrayList<TIMELINE>();

    // object pool/cache
    private static final Object POOL_LOCK = new Object[0];
    private static final int MAX_POOL_SIZE = 5;
    private static OfferLineDetailResponse head;
    private static int poolSize;
    private volatile OfferLineDetailResponse next;

    /**
     * Use obtain() instead, and returns it using recycle() whenever it's available again.
     */
    private OfferLineDetailResponse() {
       // not allowed
    }

    /**
     * Don't forget returning it with recycle() whenever it's available again.
     */
    public static OfferLineDetailResponse obtain() {
        synchronized (POOL_LOCK) {
            if (head != null) {
                // get object from head of the linked list
                OfferLineDetailResponse obj = head;
                head = obj.next;
                obj.next = null;
                poolSize--;
                return obj;
            }
        }

        return new OfferLineDetailResponse();
    }

    public void recycle() {
        synchronized (POOL_LOCK) {
            if (poolSize < MAX_POOL_SIZE) {
                // prepend to the linked list
                next = head;
                head = this;
                poolSize++;

                reset();
            }
        }
    }

    private void reset() {
        buyer = owner = null;
        item = null;
        timeline.clear();
    }

    // TODO 使用json helper替代
    public void fromJson(JSONObject jo) throws JSONException {
        if (null == jo) {
            return;
        }

        JSONObject metaJo = jo.getJSONObject("meta");
        buyer = new USER();
        buyer.fromJson(metaJo.optJSONObject("buyer"));

        item = new HOME_OBJECTS();
        item.fromJson(metaJo.optJSONObject("item"));
        owner = item.owner;

        if (metaJo.has("item_state")) {
            item.state = HOME_OBJECTS.State.valueOf(metaJo.getInt("item_state"));
        }

        soldToOthers = metaJo.optBoolean("sold_to_others");
        userBlocked = metaJo.optBoolean("user_blocked");
        latestTs = metaJo.getInt("latest_ts");

        JSONArray timelinesJa = jo.optJSONArray("objects");
        if (null != timelinesJa) {
            for (int i = 0; i < timelinesJa.length(); i++) {
                JSONObject timelineJo = timelinesJa.getJSONObject(i);
                TIMELINE timeline = TIMELINE.obtain();
                timeline.fromJson(timelineJo);
                this.timeline.add(timeline);
            }
        }
    }

//    public JSONObject toJson() throws JSONException {
//        JSONObject localItemObject = new JSONObject();
//        JSONArray itemJSONArray = new JSONArray();
//        if (null != item) {
//            localItemObject.put("item", item.toJson());
//        }
//
//        for (int i = 0; i < timeline.size(); i++) {
//            TIMELINE itemData = timeline.get(i);
//            JSONObject itemJSONObject = itemData.toJson();
//            itemJSONArray.put(itemJSONObject);
//        }
//        localItemObject.put("timeline", itemJSONArray);
//        if (null != buyer) {
//            localItemObject.put("buyer", buyer.toJson());
//        }
//        if (null != seller) {
//            localItemObject.put("seller", seller.toJson());
//        }
//        return localItemObject;
//    }

}
