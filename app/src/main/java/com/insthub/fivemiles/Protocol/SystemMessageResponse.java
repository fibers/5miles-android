
package com.insthub.fivemiles.Protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class SystemMessageResponse implements Serializable
{
	public String error;

	public String error_description;

	public ArrayList<SystemMessage> objects = new ArrayList<SystemMessage>();

	public META meta;

	public void fromJson(JSONObject jsonObject) throws JSONException {
		if (null == jsonObject) {
			return;
		}

		JSONArray subItemArray;

		this.error = jsonObject.optString("error");

		this.error_description = jsonObject.optString("error_description");

		subItemArray = jsonObject.optJSONArray("objects");
		if (null != subItemArray) {
			for (int i = 0; i < subItemArray.length(); i++) {
				JSONObject subItemObject = subItemArray.getJSONObject(i);
				SystemMessage subItem = new SystemMessage();
				subItem.fromJson(subItemObject);
				this.objects.add(subItem);
			}
		}

		META meta = new META();
		meta.fromJson(jsonObject.optJSONObject("meta"));
		this.meta = meta;
		return;
	}

}
