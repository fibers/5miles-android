
package com.insthub.fivemiles.Protocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class BUYER implements Serializable
{
     public String   nick_name;

     public String id;

     public String   portrait;

     public void  fromJson(JSONObject jsonObject)  throws JSONException
     {
          if(null == jsonObject){
            return ;
           }
          this.nick_name = jsonObject.optString("nickname");
          this.id = jsonObject.optString("id");
          this.portrait = jsonObject.optString("portrait");
          return ;
     }

     public JSONObject  toJson() throws JSONException 
     {
          JSONObject localItemObject = new JSONObject();
          localItemObject.put("nickname", nick_name);
          localItemObject.put("id", id);
          localItemObject.put("portrait", portrait);
          return localItemObject;
     }

}
