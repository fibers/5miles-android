package com.insthub.fivemiles.Protocol;

import com.thirdrock.domain.IUser;
import com.thirdrock.domain.User;
import com.thirdrock.protocol.offer.Buyer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

@Deprecated
public class USER implements Serializable {

    public String nick_name;
    public String id;
    public String portrait;
    public boolean followed;
    public boolean following;

    // 实名认证信息
    public boolean isVerified;
    public boolean isEmailVerified;
    public boolean isPhoneVerified;
    public boolean isFacebookVerified;

    //review信息
    public int reviewCount;
    public double reputationScore;

    // FIXME available only for offerLine apis
    public double latitude, longitude;
    public String city;  // @JsonField(fieldName = "place")

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }
        this.nick_name = jsonObject.optString("nickname");
        this.id = jsonObject.optString("id");
        this.followed = jsonObject.optBoolean("followed");
        this.following = jsonObject.optBoolean("following");
        this.portrait = jsonObject.optString("portrait");

        if ("null".equalsIgnoreCase(portrait)) {
            portrait = null;
        }

        this.isVerified = jsonObject.optBoolean(USER_INFO.IS_VERIFIED);
        this.isEmailVerified = jsonObject.optBoolean(USER_INFO.IS_EMAIL_VERIFIED);
        this.isPhoneVerified = jsonObject.optBoolean(USER_INFO.IS_PHONE_VERIFIED);
        this.isFacebookVerified = jsonObject.optBoolean(USER_INFO.IS_FACEBOOK_VERIFIED);

        city = jsonObject.optString("place");
        JSONObject locJo = jsonObject.optJSONObject("location");
        if (locJo != null) {
            latitude = locJo.optDouble("lat");
            longitude = locJo.optDouble("lon");
        }

        this.reviewCount = jsonObject.optInt("review_num");
        this.reputationScore = jsonObject.optDouble("review_score");
    }

    public JSONObject toJson() throws JSONException {
		JSONObject localItemObject = new JSONObject();
		localItemObject.put("nickname", nick_name);
		localItemObject.put("id", id);
		localItemObject.put("portrait", portrait);
        localItemObject.put("followed", followed);
        localItemObject.put("following", following);

        localItemObject.put(USER_INFO.IS_VERIFIED, isVerified);
        localItemObject.put(USER_INFO.IS_EMAIL_VERIFIED, isEmailVerified);
        localItemObject.put(USER_INFO.IS_PHONE_VERIFIED, isPhoneVerified);
        localItemObject.put(USER_INFO.IS_FACEBOOK_VERIFIED, isFacebookVerified);

        localItemObject.put("review_num", reviewCount);
        localItemObject.put("review_score", reputationScore);

        return localItemObject;
    }

    public static USER fromDomain(User domain) {
        if (domain == null) {
            return null;
        }

        USER user = new USER();
        user.id = domain.getId();
        user.nick_name = domain.getNickname();
        user.portrait = domain.getAvatarUrl();

        user.isVerified = domain.isVerified();
        user.isEmailVerified = domain.isEmailVerified();
        user.isPhoneVerified = domain.isPhoneVerified();
        user.isFacebookVerified = domain.isFacebookVerified();

        user.reviewCount = domain.getReviewCount();
        user.reputationScore = domain.getReputationScore();

        return user;
    }

    public static USER fromDomain(Buyer buyer) {
        if (buyer == null) {
            return null;
        }
        USER user = new USER();
        user.id = buyer.getId();
        user.nick_name = buyer.getNickname();
        user.portrait = buyer.getAvatarUrl();
        user.isVerified = buyer.isVerified();

        return user;
    }

    public static USER fromDomain(IUser pUser) {
        if (pUser == null) {
            return null;
        }

        USER user = new USER();
        user.id = pUser.getId();
        user.nick_name = pUser.getNickname();
        user.portrait = pUser.getAvatarUrl();
        user.isVerified = pUser.isVerified();

        return user;
    }

    public User toDomain() {
        User user = new User();
        user.setId(id);
        user.setAvatar(portrait);
        user.setNickname(nick_name);

        user.setVerified(isVerified);
        user.setEmailVerified(isEmailVerified);
        user.setPhoneVerified(isPhoneVerified);
        user.setFacebookVerified(isFacebookVerified);

        user.setReviewCount(reviewCount);
        user.setReputationScore(reputationScore);

        return user;
    }
}
