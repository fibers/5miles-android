package com.insthub.fivemiles.Protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class USER_PROFILE implements Serializable {

	public String about;
	public int id;
	public String nick_name;
	public String portrait;
    public String email;
    // public String facebook;

	public void fromJson(JSONObject jsonObject) throws JSONException {
		if (null == jsonObject) {
			return;
		}
		this.nick_name = jsonObject.optString("nickname");
		this.id = jsonObject.optInt("id");
		this.portrait = jsonObject.optString("portrait");
		this.about = jsonObject.optString("about");
        this.email = jsonObject.optString("email");

		return;
	}

	public JSONObject toJson() throws JSONException {
		JSONObject localItemObject = new JSONObject();
		JSONArray itemJSONArray = new JSONArray();
		localItemObject.put("nickname", nick_name);
		localItemObject.put("id", id);
		localItemObject.put("portrait", portrait);
		localItemObject.put("about", about);
        localItemObject.put("email", email);

		return localItemObject;
	}

}
