
package com.insthub.fivemiles.Protocol;

import com.external.activeandroid.annotation.Column;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class COUNTRY implements Serializable
{

     @Column(name = "country_name")
     public String   country_name;

     @Column(name = "dial_code")
     public int dial_code;

     public void  fromJson(JSONObject jsonObject)  throws JSONException
     {
          if(null == jsonObject){
            return ;
           }

          JSONArray subItemArray;

          this.country_name = jsonObject.optString("country_name");

          this.dial_code = jsonObject.optInt("dial_code");

          return ;
     }

     public JSONObject  toJson() throws JSONException 
     {
          JSONObject localItemObject = new JSONObject();
          JSONArray itemJSONArray = new JSONArray();
          localItemObject.put("country_name", country_name);
          localItemObject.put("dial_code", dial_code);
          return localItemObject;
     }

}
