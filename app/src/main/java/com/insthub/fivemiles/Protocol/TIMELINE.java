
package com.insthub.fivemiles.Protocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

// Replaced by ChatMessage
@Deprecated
public class TIMELINE implements Serializable, Comparable<TIMELINE> {

    public enum Type {
        OFFER(0), OFFER_ACCEPTED(1), CHAT(2), RECEIVED(3), SELLER_RATED(4);

        private int v;

        Type(int v) {
            this.v = v;
        }

        public static Type valueOf(int v) {
            switch (v) {
                case 0:
                    return OFFER;
                case 1:
                    return OFFER_ACCEPTED;
                case 3:
                    return RECEIVED;
                case 4:
                    return SELLER_RATED;
                default:
                    return CHAT;
            }
        }

        public int value() {
            return v;
        }
    }

    public volatile int id;
    public volatile Type type;  // 0: offer, 1: system, 2: chat

    public volatile String text, localPrice, timestamp;

    public volatile Double price;

    public volatile boolean fromBuyer, fromMe;

    // object pool/cache
    private static final Object POOL_LOCK = new Object[0];
    private static final int MAX_POOL_SIZE = 200;
    private static TIMELINE head;
    private static int poolSize;
    private volatile TIMELINE next;

    /**
     * Use obtain() instead, and returns it using recycle() whenever it's available again.
     */
    private TIMELINE() {
        // not allowed
    }

    /**
     * Don't forget returning it with recycle() whenever it's available again.
     */
    public static TIMELINE obtain() {
        synchronized (POOL_LOCK) {
            if (head != null) {
                // get object from head of the linked list
                TIMELINE obj = head;
                head = obj.next;
                obj.next = null;
                poolSize--;
                return obj;
            }
        }

        return new TIMELINE();
    }

    public void recycle() {
        synchronized (POOL_LOCK) {
            if (poolSize < MAX_POOL_SIZE) {
                // prepend to the linked list
                next = head;
                head = this;
                poolSize++;

                reset();
            }
        }
    }

    private void reset() {
        id = 0;
        type = null;
        text = localPrice = timestamp = null;
        price = null;
        fromBuyer = fromMe = false;
    }

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.id = jsonObject.getInt("id");
        this.type = Type.valueOf(jsonObject.getInt("type"));
        this.timestamp = jsonObject.getString("timestamp");
        this.price = jsonObject.optDouble("price", Double.NaN);
        this.localPrice = jsonObject.optString("local_price");
        this.fromBuyer = jsonObject.optBoolean("from_buyer");
        this.fromMe = jsonObject.optBoolean("from_me");
        this.text = jsonObject.optString("text", "");
    }

//    public JSONObject toJson() throws JSONException {
//        JSONObject localItemObject = new JSONObject();
//        JSONArray itemJSONArray = new JSONArray();
//        localItemObject.put("id", id);
//        localItemObject.put("timestamp", timestamp);
//        localItemObject.put("price", price);
//        localItemObject.put("fromBuyer", fromBuyer);
//        localItemObject.put("comment", comment);
//        return localItemObject;
//    }


    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof TIMELINE)) {
            return false;
        }

        TIMELINE that = (TIMELINE) obj;
        return that.id == this.id;
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    @Override
    public int compareTo(TIMELINE another) {
        return this.id - another.id;
    }

}
