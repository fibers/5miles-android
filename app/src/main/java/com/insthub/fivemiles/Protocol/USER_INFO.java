package com.insthub.fivemiles.Protocol;

import com.thirdrock.fivemiles.util.ModelUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class USER_INFO implements Serializable {

    public String nickname;
    public String portrait;
	public int followings_count;
	public int followers_count;
	public int likes_count;
	public int items_count;
	public int purchases_count;
    public String city;
    public String region;
    public String country;

    @Deprecated
    public int seller_feedback_count;
    @Deprecated
    public int seller_feedback_score;
    @Deprecated
    public int buyer_feedback_count;
    @Deprecated
    public int buyer_feedback_score;

    public int reviewCount;
    public double reputationScore;

    // 实名认证信息
    public boolean isVerified;
    public boolean isEmailVerified;
    public boolean isPhoneVerified;
    public boolean isFacebookVerified;
    public String phoneNo;

    public static final String IS_VERIFIED = "verified";    // 头像是否加标记
    public static final String IS_EMAIL_VERIFIED = "email_verified";
    public static final String IS_PHONE_VERIFIED = "mobile_phone_verified";
    public static final String IS_FACEBOOK_VERIFIED = "facebook_verified";
    public static final String PHONE_NO = "mobile_phone";

	public void fromJson(JSONObject jsonObject) throws JSONException {
		if (null == jsonObject) {
			return;
		}
        this.portrait = jsonObject.optString("portrait");
        this.nickname = jsonObject.optString("nickname");
		this.followings_count = jsonObject.optInt("following_count");
		this.followers_count = jsonObject.optInt("followers_count");
		this.likes_count = jsonObject.optInt("likes_count");
		this.items_count = jsonObject.optInt("items_count");
		this.purchases_count = jsonObject.optInt("purchases_count");
        this.buyer_feedback_count = jsonObject.optInt("buyer_feedback_count");
		this.buyer_feedback_score = ModelUtils.toScore(jsonObject.optDouble("buyer_feedback_score"));
        this.seller_feedback_count = jsonObject.optInt("seller_feedback_count");
        this.seller_feedback_score = ModelUtils.toScore(jsonObject.optDouble("seller_feedback_score"));
        this.reviewCount = jsonObject.optInt("review_num");
        this.reputationScore = jsonObject.optDouble("review_score");
        this.isVerified = jsonObject.optBoolean(IS_VERIFIED);
        this.isEmailVerified = jsonObject.optBoolean(IS_EMAIL_VERIFIED);
        this.isPhoneVerified = jsonObject.optBoolean(IS_PHONE_VERIFIED);
        this.isFacebookVerified = jsonObject.optBoolean(IS_FACEBOOK_VERIFIED);
        this.phoneNo = jsonObject.optString(PHONE_NO);
        this.city = jsonObject.optString("city");
        this.region = jsonObject.optString("region");
        this.region = jsonObject.optString("country");
    }

	public JSONObject toJson() throws JSONException {
		JSONObject localItemObject = new JSONObject();
        localItemObject.put("portrait", portrait);
        localItemObject.put("nickname", nickname);
		localItemObject.put("following_count", followings_count);
		localItemObject.put("followers_count", followers_count);
		localItemObject.put("likes_count", likes_count);
		localItemObject.put("items_count", items_count);
		localItemObject.put("purchases_count", purchases_count);
        localItemObject.put("buyer_feedback_count", buyer_feedback_count);
		localItemObject.put("buyer_feedback_score", buyer_feedback_score);
        localItemObject.put("seller_feedback_count", seller_feedback_count);
		localItemObject.put("seller_feedback_score", seller_feedback_score);
        localItemObject.put("review_num", reviewCount);
        localItemObject.put("review_score", reputationScore);
        localItemObject.put(IS_VERIFIED, isVerified);
        localItemObject.put(IS_EMAIL_VERIFIED, isEmailVerified);
        localItemObject.put(IS_PHONE_VERIFIED, isPhoneVerified);
        localItemObject.put(IS_FACEBOOK_VERIFIED, isFacebookVerified);
        localItemObject.put(PHONE_NO, phoneNo);
        localItemObject.put("city", city);
        localItemObject.put("region", region);
        localItemObject.put("country", country);

		return localItemObject;
	}

}
