package com.insthub.fivemiles.Protocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class PROFILE implements Serializable {

	public int id;

	public String nick_name;

	public int seller_total_rating;

	public int buyer_positive_rating;

	public int buyer_total_rating;

	public int seller_positive_rating;

	public String about;

	public int seller_rating_score;

	public String language;

	public String portrait;

	public String country;

	public int buyer_rating_score;

	public void fromJson(JSONObject jsonObject) throws JSONException {
		if (null == jsonObject) {
			return;
		}

		this.id = jsonObject.optInt("id");

		this.nick_name = jsonObject.optString("nickname");

		this.seller_total_rating = jsonObject.optInt("seller_total_rating");

		this.buyer_positive_rating = jsonObject.optInt("buyer_positive_rating");

		this.buyer_total_rating = jsonObject.optInt("buyer_total_rating");

		this.seller_positive_rating = jsonObject.optInt("seller_positive_rating");

		this.about = jsonObject.optString("about");

		this.seller_rating_score = jsonObject.optInt("seller_rating_score");

		this.language = jsonObject.optString("language");

		this.portrait = jsonObject.optString("portrait");

		this.country = jsonObject.optString("country");

		this.buyer_rating_score = jsonObject.optInt("buyer_rating_score");
		return;
	}

}
