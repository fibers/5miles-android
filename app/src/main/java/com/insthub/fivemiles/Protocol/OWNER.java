package com.insthub.fivemiles.Protocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class OWNER implements Serializable {

	public String id;

	public String first_name;

	public String username;

	public String last_name;

	public PROFILE profile;

	public void fromJson(JSONObject jsonObject) throws JSONException {
		if (null == jsonObject) {
			return;
		}

		this.id = jsonObject.optString("id");

		this.first_name = jsonObject.optString("first_name");

		this.username = jsonObject.optString("username");

		this.last_name = jsonObject.optString("last_name");
		PROFILE profile = new PROFILE();
		profile.fromJson(jsonObject.optJSONObject("profile"));
		this.profile = profile;
		return;
	}

	

}
