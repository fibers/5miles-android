package com.insthub.fivemiles.Protocol;

import com.thirdrock.framework.util.Utils;

public class ApiInterface {

    public static final String API_HOST_PROD = "https://api.5milesapp.com/api/v2";
    public static final String API_HOST_TEST = "https://api-test.5milesapp.com/api/v2";
    public static final String API_HOST_GRAY = "http://gray.5milesapp.com/api/v2";
    // isA? A : (isB? B : C)
    public static final String API_HOST = Utils.isDebug() ? API_HOST_TEST : (Utils.isGray()? API_HOST_GRAY : API_HOST_PROD);

    // sign up/in
    public static final String SIGNUP_EMAIL = API_HOST + "/signup_email/";
    public static final String LOGIN_EMAIL = API_HOST + "/login_email/";
    public static final String LOGIN_FACEBOOK = API_HOST + "/login_facebook/";
    public static final String LINK_FACEBOOK = API_HOST + "/link_facebook/";
    public static final String UNLINK_FACEBOOK = API_HOST + "/unlink_facebook/";
    public static final String RESET_PASSWORD = API_HOST + "/forget_password/";

    public static final String USER_EDIT_PROFILE = API_HOST + "/fill_profile/";

    public static final String COMMODITY_HOME = API_HOST + "/home/";
    public static final String ITEM_COMMENTS = API_HOST + "/item_comments/";

    public static final String COUNTRY_DIALCODES = API_HOST + "/country_dialcodes/";

    public static final String POST_QUESTION = API_HOST + "/post_comment/";
    public static final String POST_REPLY = API_HOST + "/post_comment/";

    public static final String REPORT_REASONS = "/report_reasons/";

    public static final String REPORT_ITEM = API_HOST + "/report_bad_item/";
    public static final String REPORT_USER = API_HOST + "/report_bad_user/";
    public static final String REPORT_REVIEW = API_HOST + "/report_review/";

    public static final String OFFER_LINE_DETAILS = API_HOST + "/offerline/";

    public static final String COMMODITY_UPLOAD = API_HOST + "/post_item/";

    public static final String SEARCH_Q = API_HOST + "/search_new/";
    public static final String SEARCH_CAT = API_HOST + "/search/category_new/";
    public static final String SEARCH_BRAND = API_HOST + "/brand_items/";
    public static final String AD_BANNER = API_HOST + "/ad_banner/";

    public static final String ME_INFO = API_HOST + "/me/";
    public static final String MY_BUYINGS = API_HOST + "/my_purchases/";
    public static final String MY_SELLINGS = API_HOST + "/my_sellings/";
    public static final String DELETE_ITEM = API_HOST + "/delete_item/";

    public static final String USER_DETAIL = API_HOST + "/user_detail/";
    public static final String USER_SELLINGS = API_HOST + "/user_sellings/";
    public static final String USER_FOLLOWLING = API_HOST + "/user_following/";
    public static final String USER_FOLLOWERS = API_HOST + "/user_followers/";
    public static final String USER_LIKES = API_HOST + "/user_likes/";
    public static final String USER_FEEDBACKS = API_HOST + "/seller_ratings/";

    public static final String FOLLOW = API_HOST + "/follow/";
    public static final String UNFOLLOW = API_HOST + "/unfollow/";

    public static final String FEEDBACK = API_HOST + "/rate_seller/";

    public static final String REMARK_APP = API_HOST + "/feedback/";

    // message
    public static final String MESSAGE_UNREAD_NUM = API_HOST + "/new_message_count/";
    public static final String MESSAGE_BUYING = API_HOST + "/my_messages/?type=buying";
    public static final String MESSAGE_SELLING = API_HOST + "/my_messages/?type=selling";
    public static final String MESSAGE_SYSTEM = API_HOST + "/my_messages/?type=system";
    public static final String MESSAGE_SYSTEM_READ = API_HOST + "/read_system_message/";

    public static final String DELETE_MESSAGE = API_HOST + "/delete_message/";
    public static final String READ_MESSAGE = API_HOST + "/read_message/";

    public static final String REGISTER_DEVICE = API_HOST + "/register_device/";

    // verfication
    public static final String SEND_VERIFY_EMAIL = API_HOST + "/send_verify_email/";

    public static final String SHARE_STORE = API_HOST + "/share_store/";
}