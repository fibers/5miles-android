package com.insthub.fivemiles.Protocol;

import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.SystemAction;
import com.thirdrock.protocol.SystemActionData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Map;

public class SystemMessage implements Serializable {
    public static final int MSG_TYPE_NORMAL = 0;
    public static final int MSG_TYPE_REVIEW = 1;

    public String id;
    public int timestamp;
    public String title;  // title or short content
    public String detail;  // optional detailed descriptive text
    private boolean unread;

    public String image;
    private String actionSection;  // raw action data

    public int type;
    public double reviewScore = -1;

    private SystemActionData actionWrapper = new SystemActionData();

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }
        this.id = jsonObject.optString("id");
        this.timestamp = jsonObject.optInt("timestamp");
        this.title = jsonObject.optString("text");
        this.detail = jsonObject.optString("desc");
        this.unread = jsonObject.optBoolean("unread");
        this.type = jsonObject.optInt("sm_type");
        this.reviewScore = jsonObject.optInt("review_score");

        image = jsonObject.optString("image");
        actionSection = jsonObject.optString("action");

        parse();
    }

    public boolean isUnread() {
        return unread;
    }

    public boolean isRead() {
        return !unread;
    }

    public boolean isReview() {
        return type == MSG_TYPE_REVIEW;
    }

    private void parse() {
        try {
            // parse action & extract data
            actionWrapper = SystemActionData.valueOf(actionSection);
        } catch (Exception e) {
            L.e("parse system message failed", e);
        }
    }

    public String getActionData() {
        return actionWrapper.getActionData();
    }

    public SystemAction getAction() {
        return actionWrapper.getAction();
    }

    public Map<String, String> getExtraActionData() {
        return actionWrapper.getExtraActionData();
    }
}
