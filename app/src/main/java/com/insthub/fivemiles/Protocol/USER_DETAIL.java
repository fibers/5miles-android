package com.insthub.fivemiles.Protocol;

import com.thirdrock.fivemiles.util.ModelUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class USER_DETAIL implements Serializable {

    public USER userInfo;
    public int followings_count;
    public int followers_count;
    public int items_count;
    public int seller_feedback_count;
    public int seller_feedback_score;
    public String place;

    //review信息
    public int reviewNum;
    public double reviewScore;
    public boolean userBlocked;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        userInfo = new USER();
        userInfo.fromJson(jsonObject);

        this.followings_count = jsonObject.optInt("count_following");
        this.followers_count = jsonObject.optInt("count_followers");
        this.items_count = jsonObject.optInt("count_sellings");
        this.seller_feedback_count = jsonObject.optInt("count_seller_ratings");
        this.seller_feedback_score = ModelUtils.toScore(jsonObject.optDouble("seller_rating_score"));
        this.place = jsonObject.optString("place");
        this.reviewNum = jsonObject.optInt("review_num");
        this.userBlocked = jsonObject.optBoolean("user_blocked");
    }
}
