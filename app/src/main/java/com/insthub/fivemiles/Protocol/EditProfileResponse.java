package com.insthub.fivemiles.Protocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class EditProfileResponse implements Serializable {

	public String error;
	public String error_description;
	public int user_id;

	public void fromJson(JSONObject jsonObject) throws JSONException {
		if (null == jsonObject) {
			return;
		}
		this.error = jsonObject.optString("error");
		this.error_description = jsonObject.optString("error_description");
		this.user_id = jsonObject.optInt("user_id"); 
	}

}
