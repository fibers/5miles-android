
package com.insthub.fivemiles.Protocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class COMMENT implements Serializable {

    public String timestamp;

    public int id;

    public int replyTo;

    public String body;

    public USER author;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

        this.timestamp = jsonObject.optString("timestamp");

        this.id = jsonObject.optInt("id");
        this.replyTo = jsonObject.optInt("reply_to");
        this.body = jsonObject.optString("text");
        USER author = new USER();
        author.fromJson(jsonObject.optJSONObject("author"));
        this.author = author;
    }

}
