package com.insthub.fivemiles.Protocol;

import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class MessageObject implements Serializable {

	public String id;
	public int timestamp;
//	@Deprecated public int sell_side;
//	@Deprecated public int status;  // message status: read/unread
	public int offerLineId;
//	public String price;
//    public String item_image;
	public String message;  // 最新的message，包含offer的comment或纯chat
	public String type;
//    public TIMELINE.Type offerType;
//	@Deprecated public String content;
//    public boolean accepted;
    public int unread_num;
    public boolean unread;
//    @Deprecated public int rate_num;

    public String itemId;
    public IMAGES itemImage;

    // opposite user
//    public String thatName, thatHead;

    // last message sender, maybe self or opposite
    public USER sender;
//    public String fromName, fromUid;

    // last message receiver, maybe self or opposite
    public USER receiver;

	public void fromJson(JSONObject jo) throws JSONException {
		if (null == jo) {
			return;
		}
		this.id = jo.optString("id");
		this.timestamp = jo.optInt("timestamp");
//		this.sell_side = jo.optInt("sell_side");
//		this.status = jo.optInt("status", 0);
//		this.rate_num = jo.optInt("rate_num");
		this.offerLineId = jo.optInt("thread_id");
//		this.price = jo.optString("price");
//        this.item_image = jo.optString("item_image");
		this.message = jo.optString("text");
		this.type = jo.optString("type");
        this.unread_num = jo.optInt("count");
        unread = jo.optBoolean("unread");
//        this.accepted = jo.optBoolean("accepted");

        if (unread && unread_num < 1) {
            L.w("data inconsistent unread_num=%d, unread=true", unread_num);
            unread_num = 1;
        }
        else if (!unread && unread_num > 0) {
            L.w("data inconsistent unread_num=%d, unread=false", unread_num);
            unread_num = 0;
        }

        sender = new USER();
        sender.fromJson(jo.optJSONObject("from"));

        receiver = new USER();
        receiver.fromJson(jo.optJSONObject("to"));

        itemId = jo.optString("item_id");
        itemImage = new IMAGES();
        itemImage.fromJson(jo.optJSONObject("item_image"));

        // opposite user
//        this.thatName = jo.optString("nick");
//		this.thatHead = jo.optString("head");

//        this.fromUid = jo.optString("from_user_id");
//        this.fromName = jo.optString("from_user_nick");

//        if (jo.has("offer_type")) {
//            this.offerType = TIMELINE.Type.valueOf(jo.getInt("offer_type"));
//        }
	}

//    public boolean isFeedback() {
//        return "F".equalsIgnoreCase(type);
//    }
//
//    public boolean isChat() {
//        return offerType != null && TIMELINE.Type.CHAT == offerType;
//    }
//
//    public boolean isOffer() {
//        return offerType != null && TIMELINE.Type.OFFER == offerType;
//    }
//
//    public boolean isMine() {
//        return ModelUtils.isOwner(fromUid);
//    }

    public USER getOppositeUser() {
        return ModelUtils.isMe(sender) ? receiver : sender;
    }
}
