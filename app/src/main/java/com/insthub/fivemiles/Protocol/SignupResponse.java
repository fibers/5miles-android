package com.insthub.fivemiles.Protocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by ywu on 14-8-15.
 */
public class SignupResponse implements Serializable {
    public long createdAt;
    public long lastLogin;
    public String uid;
    public String nickname;
    public String token;
    public String email;
    public String portrait;
    public String fbUid;  // Facebook user id
    public long fbTokenExpires;  // facebook token expires time, in ms
    public boolean isFacebookVerified;
    public String country;
    public String city;
    public String region;
    public float latitude;
    public float longitude;

    public void fromJson(JSONObject jo) throws JSONException {
        if (jo == null) {
            return;
        }

        createdAt = Integer.parseInt(jo.getString("created_at")) * 1000;
        lastLogin = Integer.parseInt(jo.getString("last_login_at")) * 1000;
        uid = jo.getString("id");
        token = jo.getString("token");
        nickname = jo.optString("nickname");
        email = jo.optString("email");
        fbUid = jo.optString("fb_user_id", null);
        portrait = jo.optString("portrait");
        fbTokenExpires = jo.optLong("fb_token_expires") * 1000;
        isFacebookVerified = jo.optBoolean(USER_INFO.IS_FACEBOOK_VERIFIED);

        country = jo.optString("country");
        city = jo.optString("city");
        region = jo.optString("region");
        latitude = (float)jo.optDouble("lat");
        longitude = (float)jo.optDouble("lon");
    }
}
