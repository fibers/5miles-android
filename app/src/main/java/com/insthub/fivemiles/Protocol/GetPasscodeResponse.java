package com.insthub.fivemiles.Protocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class GetPasscodeResponse implements Serializable {

	public String error;
	public String error_description;
	public boolean passcode_enabled;
	public String passcode;
	public boolean is_new_user;

	public void fromJson(JSONObject jsonObject) throws JSONException {
		if (null == jsonObject) {
			return;
		}
		this.error = jsonObject.optString("error");
		this.error_description = jsonObject.optString("error_description");
		this.passcode_enabled = jsonObject.optBoolean("passcode_enabled");
		this.passcode = jsonObject.optString("passcode");
		this.is_new_user = jsonObject.optBoolean("is_new_user");
		return;
	}

}
