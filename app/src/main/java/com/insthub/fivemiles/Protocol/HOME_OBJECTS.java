package com.insthub.fivemiles.Protocol;

import android.text.TextUtils;

import com.thirdrock.domain.ImageInfo;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.Location;
import com.thirdrock.fivemiles.util.LocationUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.thirdrock.domain.ItemThumb.IS_NEW;
import static com.thirdrock.domain.ItemThumb.NOT_NEW;

@Deprecated
public class HOME_OBJECTS implements Serializable {

    public enum State {
        LISTING, SOLD, RECEIVED, SELLER_RATED, UNAPPROVED, UNAVAILABLE, UNLISTED;

        public static State valueOf(int v) {
            State[] vs = State.values();
            return (v >= 0 && v < vs.length) ? vs[v] : LISTING;
        }

        public boolean isSold() {
            return this != LISTING;
        }

    }

	public String id;
	public String title;
	public String description;
    public String mediaLink; // link to audio clip
	public double distance;
	public double latitude;
	public double longitude;
    public String currency_code;
    public double price;
    public Double originPrice;
    public String localPrice;  // price in localized string
    public long created_at;     // seconds
    public int lastModified;     // edit time, seconds
    public int lastUpdated;     // renew time, seconds
    public int categoryId;
    public String brand;
    public int deliveryTypeId;
    public String city, province, country;
    public String image;
	@Deprecated
	public USER owner;
    public List<IMAGES> images;
    public State state;

    public boolean isNew;

    public void fromJson(JSONObject jo) throws JSONException {
		if (null == jo) {
			return;
		}

		this.id = jo.getString("id");
		this.title = jo.optString("title");
		this.description = jo.optString("desc");
        this.mediaLink = jo.optString("mediaLink");

        this.currency_code = jo.optString("currency");
        this.price = jo.optDouble("price");
        this.localPrice = jo.optString("local_price");
        this.originPrice = jo.optDouble("original_price");
        this.deliveryTypeId = jo.optInt("shipping_method");

        this.created_at = jo.optLong("created_at");
        int state = jo.optInt("state", 0);
        this.state = State.valueOf(state);

        this.categoryId = jo.optInt("cat_id");
        this.brand = jo.optString("brand_name");

        JSONObject coordins = jo.optJSONObject("location");
        if (coordins == null && jo.has("location")) {
            coordins = new JSONObject(jo.optString("location", "{}"));
        }
        if (coordins != null) {
            this.longitude = coordins.optDouble("lon");
            this.latitude = coordins.optDouble("lat");
            this.distance = LocationUtils.computeDistance(latitude, longitude);
        }

        city = jo.optString("city");
        province = jo.optString("region");
        country = jo.optString("country");

        JSONArray imgsJa = jo.optJSONArray("images");
        if (imgsJa == null && jo.has("images")) {
            imgsJa = new JSONArray(jo.optString("images", "[]"));
        }
        if (imgsJa != null) {
            this.images = new ArrayList<>(imgsJa.length());
            for (int i = 0; i < imgsJa.length(); i++) {
                IMAGES img = new IMAGES();
                img.fromJson(imgsJa.getJSONObject(i));
                this.images.add(img);
            }
        }

		USER owner = new USER();
		owner.fromJson(jo.optJSONObject("owner"));
		this.owner = owner;
        int newIntVaule = jo.optInt("is_new");
        this.isNew = newIntVaule == IS_NEW;

        lastModified = jo.optInt("modified_at");
        lastUpdated = jo.optInt("updated_at");
	}
	
	public JSONObject  toJson() throws JSONException 
    {
        JSONObject jo = new JSONObject();
        jo.put("id", id);
        jo.put("title", title);
        jo.put("desc", description);
        jo.put("distance", distance);
        jo.put("longitude", longitude);
        jo.put("latitude", latitude);
        jo.put("local_price", localPrice);

        JSONObject coordins = new JSONObject();
        coordins.put("lat", latitude);
        coordins.put("lon", longitude);
        jo.put("location", coordins);
        jo.put("created_at", created_at);

        JSONArray imgJa = new JSONArray();
        if (images != null) {
            for (IMAGES img : images) {
                imgJa.put(img.toJson());
            }
        }
        jo.put("images", imgJa);
        jo.put("is_new", isNew? IS_NEW : NOT_NEW);
        jo.put("modified_at", lastModified);
        jo.put("updated_at", lastUpdated);
        return jo;
    }

    /**
     * get the unique representative image of this item
     * @return an IMAGES object, or null if no images for this item
     */
    public IMAGES getImage() {
        return images == null ? null : images.get(0);
    }

    public boolean isSold() {
        return state != null && state.isSold();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof HOME_OBJECTS)) {
            return false;
        }

        HOME_OBJECTS that = (HOME_OBJECTS) obj;
        return TextUtils.equals(this.id, that.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public static HOME_OBJECTS fromDomain(Item item) {
        if (item == null) {
            return null;
        }

        HOME_OBJECTS obj = new HOME_OBJECTS();
        obj.id = item.getId();
        obj.title = item.getTitle();
        obj.localPrice = item.getLocalPrice();
        obj.currency_code = item.getCurrencyCode();
        obj.price = item.getPrice();
        obj.originPrice = item.getOriginPrice();

        obj.owner = USER.fromDomain(item.getOwner());
        obj.images = new ArrayList<>();
        for (ImageInfo img : item.getImages()) {
            obj.images.add(new IMAGES(img));
        }

        obj.state = State.valueOf(item.getState().ordinal());
        obj.isNew = item.isNew();
        obj.lastUpdated = item.getLastUpdated();
        obj.lastModified = item.getLastModified();
        return obj;
    }

    public Item toDomain() {
        Item item = new Item();

        item.setId(id);
        item.setTitle(title);
        item.setLocalPrice(localPrice);
        item.setPrice(price);
        item.setOriginPrice(originPrice);
        item.setCurrency(currency_code);
        item.setOwner(owner.toDomain());
        item.setLocation(new Location((float) latitude, (float) longitude));
        item.setCity(city);
        item.setCountry(country);
        item.setProvince(province);
        item.setStateId(state != null ? state.ordinal() : 0);

        List<ImageInfo> imgs = new ArrayList<ImageInfo>(images.size());
        for (IMAGES img : images) {
            imgs.add(img.toDomain());
        }

        item.setImages(imgs);
        item.setIsNew(isNew? IS_NEW : NOT_NEW);
        item.setLastModified(lastModified);
        item.setLastUpdated(lastUpdated);
        return item;
    }
}
