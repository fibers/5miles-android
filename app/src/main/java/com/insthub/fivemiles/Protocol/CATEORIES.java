
package com.insthub.fivemiles.Protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class CATEORIES implements Serializable
{
     public int id;

     public String   title;

     public String   parent;

     public String   titles;

     public int _order;

     public String   slug;

     public void  fromJson(JSONObject jsonObject)  throws JSONException
     {
          if(null == jsonObject){
            return ;
           }

          JSONArray subItemArray;

          this.id = jsonObject.optInt("id");

          this.title = jsonObject.optString("title");

          this.parent = jsonObject.optString("parent");

          this.titles = jsonObject.optString("titles");

          this._order = jsonObject.optInt("_order");

          this.slug = jsonObject.optString("slug");
          return ;
     }

     public JSONObject  toJson() throws JSONException 
     {
          JSONObject localItemObject = new JSONObject();
          JSONArray itemJSONArray = new JSONArray();
          localItemObject.put("id", id);
          localItemObject.put("title", title);
          localItemObject.put("parent", parent);
          localItemObject.put("titles", titles);
          localItemObject.put("_order", _order);
          localItemObject.put("slug", slug);
          return localItemObject;
     }

}
