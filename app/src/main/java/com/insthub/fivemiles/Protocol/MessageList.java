package com.insthub.fivemiles.Protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class MessageList implements Serializable {

	public String item_image;
	public String nick;
	public String item_title;
	public String last_message;
	public int count;
	public int item_id;
	public int question_id;

	public ArrayList<MessageObject> objects = new ArrayList<MessageObject>();

	public void fromJson(JSONObject jsonObject) throws JSONException {
		if (null == jsonObject) {
			return;
		}

		JSONArray subItemArray;

		this.item_image = jsonObject.optString("item_image");
		this.nick = jsonObject.optString("nick");
		this.item_title = jsonObject.optString("item_title");
		this.last_message = jsonObject.optString("last_message");
		this.count = jsonObject.optInt("count");
		this.item_id = jsonObject.optInt("item_id");
		this.question_id = jsonObject.optInt("question_id");
		
		subItemArray = jsonObject.optJSONArray("objects");
		if (null != subItemArray) {
			for (int i = 0; i < subItemArray.length(); i++) {
				JSONObject subItemObject = subItemArray.getJSONObject(i);
				MessageObject subItem = new MessageObject();
				subItem.fromJson(subItemObject);
				this.objects.add(subItem);
			}
		}
		return;
	}

}
