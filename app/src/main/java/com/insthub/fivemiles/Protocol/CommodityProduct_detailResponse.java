
package com.insthub.fivemiles.Protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommodityProduct_detailResponse implements Serializable {

    public String error;

    public int status;

    public String title;

    public String description;

    public String localPrice;

    public double price;

    public String createdAt;

    public double longitude;

    public double latitude;

    public String error_description;

    public String currency;

    public List<OfferLine> offerLines;

    public List<COMMENT> comments;

    public int commentCount;

    public boolean hasMoreComments;

    public boolean liked;

    public boolean isMine;

    public HOME_OBJECTS item;

    public void fromJson(JSONObject jo) throws JSONException {
        if (null == jo) {
            return;
        }

        JSONArray subItemArray;

        this.error = jo.optString("error");
        this.error_description = jo.optString("error_description");

        this.item = new HOME_OBJECTS();
        this.item.fromJson(jo);

        this.latitude = item.latitude;
        this.longitude = item.longitude;

        this.status = jo.optInt("state");
        this.isMine = jo.optBoolean("isMine");
        this.liked = jo.optBoolean("liked");

        this.title = jo.optString("title");
        this.description = jo.optString("desc");
        this.createdAt = jo.optString("created_at");

        this.currency = jo.optString("currency");
        this.price = jo.optDouble("price");
        this.localPrice = jo.optString("local_price");

        subItemArray = jo.optJSONArray("offerLines");
        if (subItemArray != null) {
            offerLines = new ArrayList<OfferLine>(subItemArray.length());
            for (int i = 0; i < subItemArray.length(); i++) {
                JSONObject lineJo = subItemArray.getJSONObject(i);
                OfferLine line = new OfferLine();
                line.fromJson(lineJo);
                offerLines.add(line);
            }
        }
        else {
            offerLines = Collections.emptyList();
        }

        subItemArray = jo.optJSONArray("comments");
        if (subItemArray != null) {
            comments = new ArrayList<COMMENT>(subItemArray.length());
            for (int i = 0; i < subItemArray.length(); i++) {
                JSONObject commentJo = subItemArray.getJSONObject(i);
                COMMENT comment = new COMMENT();
                comment.fromJson(commentJo);
                comments.add(comment);
            }
        }
        else {
            comments = Collections.emptyList();
        }

        hasMoreComments = jo.optBoolean("has_more_comments");
        commentCount = jo.optInt("comment_count");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();
        localItemObject.put("title", title);
        localItemObject.put("description", description);
        localItemObject.put("localPrice", localPrice);
        localItemObject.put("createdAt", createdAt);
        localItemObject.put("longitude", longitude);
        localItemObject.put("latitude", latitude);
        return localItemObject;
    }


}
