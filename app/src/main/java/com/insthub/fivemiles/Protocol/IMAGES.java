
package com.insthub.fivemiles.Protocol;

import com.external.activeandroid.annotation.Column;
import com.thirdrock.domain.ImageInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

@Deprecated
public class IMAGES implements Serializable {

    @Column(name = "IMAGES_id")
    @Deprecated
    public int id;

    @Column(name = "image_name")
    @Deprecated
    public String image_name;

    @Column(name = "image_width")
    public int image_width;

    @Column(name = "image_height")
    public int image_height;

    @Column(name = "url")
    public String url;

    public IMAGES() {
    }

    public IMAGES(ImageInfo imgInfo) {
        this.url = imgInfo.getUrl();
        this.image_width = imgInfo.getWidth();
        this.image_height = imgInfo.getHeight();
    }

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }

//          this.id = jsonObject.optInt("id");
//          this.image_name = jsonObject.optString("image_name");
        this.image_width = jsonObject.optInt("width");
        this.image_height = jsonObject.optInt("height");
        if (jsonObject.has("imageLink")) {
            this.url = jsonObject.optString("imageLink");
        }
        else {
            this.url = jsonObject.getString("url");
        }
    }

    public JSONObject toJson() throws JSONException {
        JSONObject localItemObject = new JSONObject();
//          localItemObject.put("id", id);
//          localItemObject.put("image_name", image_name);
        localItemObject.put("width", image_width);
        localItemObject.put("height", image_height);
        localItemObject.put("url", url);
        localItemObject.put("imageLink", url);
        return localItemObject;
    }

    public ImageInfo toDomain() {
        return new ImageInfo(url, image_width, image_height);
    }
}
