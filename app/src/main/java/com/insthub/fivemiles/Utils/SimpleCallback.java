package com.insthub.fivemiles.Utils;

import android.net.Uri;

/**
 * Created by lishangqiang on 2014/9/14.
 */
public interface SimpleCallback {

    public void onResult(Object... params);

}
