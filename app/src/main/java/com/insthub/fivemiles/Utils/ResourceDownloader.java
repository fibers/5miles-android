/**
 * Copyright (c) 2012 Eleven Inc. All Rights Reserved.
 */
package com.insthub.fivemiles.Utils;

import android.content.Context;
import android.os.AsyncTask;

import com.insthub.fivemiles.FiveMilesApp;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import rx.Observable;
import rx.Subscriber;

/**
 * 调用HttpURLConnection获取服务器资源，并保存为本地文件。根据
 * http://developer.android.com/reference/android/net/http/HttpResponseCache.html
 * 当打开缓存的时候，HttpResponseCache会在背后缓存HttpURLConnection打开的资源，从而减少对
 * 服务器的压力
 */

public class ResourceDownloader {

    private static final int    RETRY_TIMES = 5;

    private static AsyncTask<String, Void, File> task;

    static File downloadWithRetries(Context context, String url, int retryTimes) {
        try {
            File file = File.createTempFile("resource", ".xyz", context.getCacheDir());
            return downloadWithRetries(context, url, file, retryTimes);
        }
        catch (Exception e) {
            L.e(e);
            return null;
        }
    }

    static File downloadWithRetries(Context context, String url, File dest, int retryTimes) {
        L.d("download resource " + url + "...");
        try {
            for (int i = 0; i < retryTimes; i++) {
                try {
                    downloadResourceToFile(url, dest);
                    L.d("save to " + dest + ": " + dest.exists());
                    break;
                } catch (IOException e) {
                    L.d(e);
                }
            }
            return dest;
        }
        catch (Exception e) {
            L.e(e);
            return null;
        }
    }

    /**
     * Get resource from server by HttpURLConnection and save to file
     *
     * @return
     */
    static void downloadResourceToFile(String url, File file) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        BufferedInputStream is = new BufferedInputStream(conn.getInputStream());
        BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(file));
        try {
            byte[] buf = new byte[1024];
            for (int len; (len = is.read(buf)) != -1; ) {
                os.write(buf, 0, len);
                os.flush();
            }
        }
        finally {
            ModelUtils.close(is);
            ModelUtils.close(os);
        }
    }

    public static Observable<File> downloadResource(final String url) {
        return Observable.create(new Observable.OnSubscribe<File>() {
            @Override
            public void call(Subscriber<? super File> subscriber) {
                if (subscriber.isUnsubscribed()) {
                    return;
                }

                File f = downloadWithRetries(FiveMilesApp.getInstance(), url, RETRY_TIMES);
                if (subscriber.isUnsubscribed()) {
                    return;
                }

                subscriber.onNext(f);
                subscriber.onCompleted();
            }
        });
    }

    public static Observable<File> downloadResource(final String url, final File dest) {
        return Observable.create(new Observable.OnSubscribe<File>() {
            @Override
            public void call(Subscriber<? super File> subscriber) {
                if (subscriber.isUnsubscribed()) {
                    return;
                }

                downloadWithRetries(FiveMilesApp.getInstance(), url, dest, RETRY_TIMES);
                if (subscriber.isUnsubscribed()) {
                    return;
                }

                subscriber.onNext(dest);
                subscriber.onCompleted();
            }
        });
    }
}
