/**
 * Copyright (c) 2012 Eleven Inc. All Rights Reserved.
 */
package com.insthub.fivemiles.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import com.insthub.fivemiles.Utils.ImageAsyncTask.ImageAsyncTaskCallback;

/**
 * @File ImageUtils.java
 * 
 * @Author wangming1988
 * 
 * @Date Sep 9, 2012, 9:47:32 PM
 * 
 * @Version v0.1
 * 
 * @Description
 * 
 */

public class ImageUtils {
	/******************************Image cache on SDCard******************************************************/
	/**You should make a directory for image cache.*/
	private static final String APP_NAME = "5miles";
	private static final String SDCARD_CACHE_IMAGE_PATH = Environment.getExternalStorageDirectory().getPath()
			+"/"+ APP_NAME + "/images/";
	
	public static String getCacheImagePath() {
		return SDCARD_CACHE_IMAGE_PATH;
	}
	
	/**
	 * Save the image to SD card.
	 * @param imagePath
	 * @param buf
	 * @throws IOException
	 */
	public static void saveImage2SDCard(String imagePath, byte[] buf) throws IOException {
		LogUtils.i("imagePath=============", imagePath);
		File file = new File(imagePath);
		if(file.exists()) {
			return;
		}else {
			File parentFile = file.getParentFile();
			if(!parentFile.exists()) {
				parentFile.mkdirs();
			}
			//you should make the directory first, and then make the file
			//not make the expanded name
			file.createNewFile(); 
			FileOutputStream fos = new FileOutputStream(imagePath);
			fos.write(buf);
			fos.flush();
			fos.close();
		}
	}
	
	/**
	 * Get the bitmap from SD card cache.
	 * @param imagePath
	 * @return
	 */
	public static Bitmap getImageFromSDCard(String imagePath) {
		File file = new File(imagePath);
		if(file.exists()) {
			Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
			file.setLastModified(System.currentTimeMillis());
			return bitmap;
		}
		return null;
	}
	
	/**
	 * Compress the bitmap to JPEG, and write to ByteArrayOutputStream.
	 * @param bitmap
	 * @return
	 */
	public static byte[] bitmap2Bytes(Bitmap bitmap) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		return baos.toByteArray();
	}
	
	/***************************Fetch image from server(HttpURLConnection, HttpClient)*********************************************/
	
	/**
	 * Get image from server by HttpURLConnection
	 * 
	 * @param imageUrl
	 * @return
	 */
	public static Bitmap getImageByHttpURLConnection(String imageUrl) {
		Bitmap bitmap = null;
		try {
			URL url = new URL(imageUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			InputStream is = conn.getInputStream();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			for(int len = 0; (len = is.read(buf)) != -1; ) {
				baos.write(buf, 0, len);
			}
			if(is != null) {
				is.close();
			}
			baos.flush();
			byte[] data = baos.toByteArray();
			bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bitmap;
	}
	
	/**
	 * Get image from server by HttpClient
	 * 
	 * @param imageUrl
	 * @return
	 */
	public static Bitmap getImageByHttpClient(String imageUrl) {
		Bitmap bitmap = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(imageUrl);
			HttpResponse response = client.execute(getRequest);
			//Http response status code 200 OK
			if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				//response entity
				HttpEntity responseEntity = response.getEntity();
				InputStream is = responseEntity.getContent();
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				byte[] buf = new byte[1024];
				for(int len = 0; (len = is.read(buf)) != -1; ) {
					baos.write(buf, 0, len);
				}
				if(is != null) {
					is.close();
				}
				baos.flush();
				byte[] data = baos.toByteArray();
				bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bitmap;
	}
	
	/***************Load image from local******************************/
	/**
	 * 
	 * @param callback
	 * @param imagePath local path
	 * @param imageUrl  server url
	 * @return
	 */
	public static Bitmap loadImageFromCache(ImageAsyncTaskCallback callback, String imagePath, String imageUrl) {
		
		Bitmap localBitmap = getImageFromSDCard(imagePath);
		if(localBitmap != null) {
			return localBitmap;
		}else {
			new ImageAsyncTask(callback).execute(imagePath, imageUrl);
		}
		return null;
	}
	
	/**
	 * Make the image path(local path) from image url,to generate cache for image
	 * 
	 * @param imageUrl
	 * @return
	 */
	public static String makeImagePathFromUrl(String imageUrl) {
		return getCacheImagePath().concat(MD5Encoder.encode(imageUrl));
	}
}
