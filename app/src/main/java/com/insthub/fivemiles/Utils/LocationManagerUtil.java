package com.insthub.fivemiles.Utils;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;

import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.SESSION;

import java.math.BigDecimal;

public class LocationManagerUtil implements LocationListener {

    private static final int MAX_LOCATION_AGE = 4 * 3600000;  // 4hr
    private static final int MIN_LOC_ACCURACY = 500;

    private static LocationManagerUtil instance;

	private OnLocationListener onLocaiton;
	private Context context;
	
//	public SharedPreferences shared;

    private LocationManager locationMgr;
    private static Location currLocation;

    public static LocationManagerUtil getInstance() {
        if (instance == null) {
            instance = new LocationManagerUtil();
        }

        return instance;
    }

	private LocationManagerUtil() {
		this.context = FiveMilesApp.getInstance();
//    	shared = context.getSharedPreferences(FiveMilesAppConst.USERINFO, 0);
        locationMgr = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	}
	
	public boolean isEnabled() {
		return locationMgr != null && (locationMgr.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationMgr.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
	}

    public boolean isZipcodeRegisted(){
        SESSION session = SESSION.getInstance();
        return !(TextUtils.isEmpty(session.getCountry())&&TextUtils.isEmpty(session.getCity())&&TextUtils.isEmpty(session.getRegion()));
    }

	public void getLocation() {
        if (locationMgr == null) {
            return;
        }

        Location lastNtwLoc = locationMgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (isBetterLocation(lastNtwLoc)) {
            saveLocation(lastNtwLoc);
        }

        Location lastPassiveLoc = locationMgr.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        if (isBetterLocation(lastPassiveLoc)) {
            saveLocation(lastPassiveLoc);
        }

        Location lastGpsLoc = locationMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (isBetterLocation(lastGpsLoc)) {
            saveLocation(lastGpsLoc);
        }

        requestLocationUpdates();
	}

    public void requestLocationUpdates() {
        if (locationMgr == null) {
            return;
        }

        if (locationMgr.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MAX_LOCATION_AGE, MIN_LOC_ACCURACY, this);
        }

        if(locationMgr.isProviderEnabled(LocationManager.PASSIVE_PROVIDER)) {
            locationMgr.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, MAX_LOCATION_AGE, MIN_LOC_ACCURACY, this);
        }

        if (locationMgr.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, MAX_LOCATION_AGE, MIN_LOC_ACCURACY, this);
        }
    }

    public void stopLocationUpdates() {
        if (locationMgr == null) {
            return;
        }

        locationMgr.removeUpdates(this);
    }

    // Provider的状态在可用、暂时不可用和无服务三个状态直接切换时触发此函数
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    // Provider被enable时触发此函数，比如GPS被打开
    @Override
    public void onProviderEnabled(String provider) {

    }

    // Provider被disable时触发此函数，比如GPS被关闭
    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location == null) {
            return;
        }

        if (isBetterLocation(location)) {
            saveLocation(location);
            if (onLocaiton != null) {
                onLocaiton.onLocationUpdated();
            }
        }
    }

    private void saveLocation(Location newLoc) {
        currLocation = newLoc;

//        if (currLocation != null) {
//            SharedPreferences.Editor editor = shared.edit();
//            editor.putFloat("latitude", (float) currLocation.getLatitude());
//            editor.putFloat("longitude", (float) currLocation.getLongitude());
//            editor.apply();
//        }
    }

    private boolean isBetterLocation(Location newLoc) {
        if (newLoc == null) {
            return false;
        }

        if (currLocation == null) {
            return true;
        }

        long timeDelta = newLoc.getTime() - currLocation.getTime();
        boolean isNewer = timeDelta > 0;
        boolean isSignificantNewer = timeDelta > MAX_LOCATION_AGE;
        boolean isSignificantOlder = timeDelta < -MAX_LOCATION_AGE;

        if (isSignificantNewer) {
            return true;
        }
        else if (isSignificantOlder) {
            return false;
        }

        int accuracyDelta = (int) (newLoc.getAccuracy() - currLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantLessAccurate = accuracyDelta > MIN_LOC_ACCURACY;

        boolean isSameProvider = TextUtils.equals(newLoc.getProvider(), currLocation.getProvider());

        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantLessAccurate && isSameProvider) {
            return true;
        }
        return false;
    }

    private static float getLatitude() {
//        return 39.998445f;  // for debug
//		shared = context.getSharedPreferences(FiveMilesAppConst.USERINFO, 0);
//		editor = shared.edit();
//		float latitude = shared.getFloat("latitude", 0.0f);
//		BigDecimal bd1  = new BigDecimal(latitude);
//        bd1 = bd1.setScale(6,4);
//        float lat = bd1.floatValue();
//		return lat;
        if (currLocation == null) {
            return 0f;
        }

        BigDecimal bigLat = new BigDecimal(currLocation.getLatitude());
        bigLat = bigLat.setScale(6, 4);
		return bigLat.floatValue();
	}

    public static float getLatitudeInSession() {
        SESSION session = SESSION.getInstance();
        if(session.getLatitude() == 0 && session.getLongitude() == 0) {
            //not set latitude in session
            return getLatitude();
        } else {
            return session.getLatitude();
        }
    }

    private static float getLongitude() {
//        return 116.480598f;  // for debug
//		shared = context.getSharedPreferences(FiveMilesAppConst.USERINFO, 0);
//		editor = shared.edit();
//		float longitude = shared.getFloat("longitude", 0.0f);
//		BigDecimal bd2  = new BigDecimal(longitude);
//        bd2 = bd2.setScale(6,4);
//        float log = bd2.floatValue();
//		return log;
        if (currLocation == null) {
            return 0f;
        }

        BigDecimal bigLon = new BigDecimal(currLocation.getLongitude());
        bigLon = bigLon.setScale(6, 4);
        return bigLon.floatValue();
	}


    public static float getLongitudeInSession() {
        SESSION session = SESSION.getInstance();
        if(session.getLongitude() == 0) {
            //not set latitude in session
            return getLongitude();
        } else {
            return session.getLongitude();
        }
    }

    public static final double EARTH_RADIUS = 6378137.0;
    public static double gps2m(double lat_a, double lng_a, double lat_b, double lng_b) {
        double radLat1 = (lat_a * Math.PI / 180.0);
        double radLat2 = (lat_b * Math.PI / 180.0);
        double a = radLat1 - radLat2;
        double b = (lng_a - lng_b) * Math.PI / 180.0;
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
                + Math.cos(radLat1) * Math.cos(radLat2)
                * Math.pow(Math.sin(b / 2), 2)));
        s = s * EARTH_RADIUS;
        s = Math.round(s * 10000) / 10000;
        return s;
    }
    
    public void setOnLocationListener(OnLocationListener onLocationListener) {
    	this.onLocaiton = onLocationListener;
    }
	
	public interface OnLocationListener {
		public void onLocationUpdated();
	}

}
