package com.insthub.fivemiles.Utils;

/**
 * Created by lishangqiang on 2014/9/14.
 */
public interface GenericCallback<T> {

    public void onResult(T result);

}
