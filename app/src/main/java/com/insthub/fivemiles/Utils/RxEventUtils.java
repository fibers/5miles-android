package com.insthub.fivemiles.Utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import rx.Observable;
import rx.subjects.BehaviorSubject;

/**
 * RxAndroid Event Helper
 * Created by jubin on 29/5/15.
 */
public class RxEventUtils {
    private RxEventUtils() {
        // no instance for helper class
    }

    /**
     *  Creates a subject that emits events for the current text and each text change event
     *  Ref: https://github.com/andrewhr/rxjava-android-example/blob/master/app/src/main/java/rx/android/Events.java
     * @param view TextView that will be observed on text change event
     * @return Observable on text change event
     */
    public static Observable<String> text(TextView view) {
        String currentText = String.valueOf(view.getText());
        final BehaviorSubject<String> subject = BehaviorSubject.create(currentText);
        view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable != null) {
                    subject.onNext(editable.toString());
                }
            }
        });
        return subject;
    }

}
