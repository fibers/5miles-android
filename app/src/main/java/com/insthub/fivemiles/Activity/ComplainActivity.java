package com.insthub.fivemiles.Activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.BeeFramework.view.ToastView;
import com.external.androidquery.callback.AjaxStatus;
import com.insthub.fivemiles.Model.ComplainModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.thirdrock.fivemiles.R;

import org.json.JSONException;
import org.json.JSONObject;

public class ComplainActivity extends BaseActivity implements BusinessResponse {

	private TextView title,send;
	private ImageView back;
	private EditText complainbody,email;
	private ComplainModel complainModel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.complain);

		title = (TextView) findViewById(R.id.complain_title);
		title.setText(R.string.complain);
		back = (ImageView) findViewById(R.id.complain_back);
		send = (TextView)findViewById(R.id.complain_send);
		complainbody = (EditText)findViewById(R.id.complain_et1);
		email = (EditText)findViewById(R.id.complain_et2);

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		complainModel = new ComplainModel(this);
		complainModel.addResponseListener(this);
		
		send.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(TextUtils.isEmpty(complainbody.getText())) {
					ToastView toast = new ToastView(ComplainActivity.this, getString(R.string.msg_complain_say_something));
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
//				else if (email.getText().toString().equals("")) {
//					ToastView toast = new ToastView(ComplainActivity.this, getString(R.string.msg_complain_no_email));
//	                toast.setGravity(Gravity.CENTER, 0, 0);
//	                toast.show();
//				}
//				else if (!Utils.isEmail(email.getText().toString())) {
//					ToastView toast = new ToastView(ComplainActivity.this, getString(R.string.msg_complain_wrong_email));
//					toast.setGravity(Gravity.CENTER, 0, 0);
//	                toast.show();
//				}
				else{
					complainModel.Complain(complainbody.getText().toString());
				}
			}
		});
	}

	@Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {
		if(url.startsWith(ApiInterface.REMARK_APP)){
			finish();
			ToastView toast = new ToastView(ComplainActivity.this, getString(R.string.msg_action_success));
			toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
		}
		
	}
}
