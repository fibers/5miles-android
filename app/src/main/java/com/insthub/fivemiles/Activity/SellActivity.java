package com.insthub.fivemiles.Activity;

//public class SellActivity extends BaseActivity implements IXListViewListener, BusinessResponse {
//
//    static final String TAG = "UI";
//
//    private ImageView back;
//    private TextView title;
//    private XListView listView;
//    private WatchingsModel watchingsModel;
//    private WatchingAdapter sellAdapter;
//
//    private boolean isEmpty;  // in empty page mode
//
//    public static final String VIEW_NAME = "myitems_view";
//    private int selectItemIndex = -1;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        setContentView(R.layout.common_list_view);
//
//        back = (ImageView) findViewById(R.id.top_view_back);
//        title = (TextView) findViewById(R.id.top_view_title);
//        listView = (XListView) findViewById(R.id.list);
//
//        back.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//
//        title.setText(getString(R.string.profile_items_for_sale));
//
//        listView.setXListViewListener(this, 1);
//        listView.setPullLoadEnable(false);
//        listView.setRefreshTime();
//
//        listView.setOnItemClickListener(new OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//                try {
//                    Intent intent = new Intent(SellActivity.this, ItemActivity.class);
//                    HOME_OBJECTS item = watchingsModel.sellList.get(position - 1);
//                    intent.putExtra(FiveMilesAppConst.ACT_PARA_ITEM_ID, item.id);
//                    startActivity(intent);
//                    TrackingUtils.trackTouch(VIEW_NAME, "myitems_product");
//                } catch (Exception e) {
//                    Log.e(TAG, "Error while starting order detail activity", e);
//                }
//            }
//        });
//
//        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
//                selectItemIndex = position;
////                new AlertDialog.Builder(SellActivity.this)
////                        .setTitle(R.string.profile_delete_item)
////                        .setMessage(R.string.profile_delete_item_message)
////                        .setPositiveButton(R.string.button_confirm, new DialogInterface.OnClickListener() {
////                            @Override
////                            public void onClick(DialogInterface dialog, int which) {
////
////                            }
////                        })
////                        .setNegativeButton(R.string.cancel, null)
////                        .show();
//                return false;
//            }
//        });
//
//        watchingsModel = new WatchingsModel(this);
//        watchingsModel.addResponseListener(this);
//
//        sellAdapter = new WatchingAdapter(this, watchingsModel.sellList);
//        listView.setAdapter(sellAdapter);
//
//        isEmpty = 0 == getIntent().getIntExtra(FiveMilesAppConst.ACT_PARA_SELLING_COUNT, 0);
//        showContent();
//
//        TrackingUtils.trackView(VIEW_NAME);
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        if (!isEmpty) {
//            listView.refreshNow();
//        }
//    }
//
//
//    private void showContent() {
//        ViewGroup emptyView = (ViewGroup) findViewById(R.id.empty_view_placeholder);
//
//        if (!isEmpty) {
//            listView.setVisibility(View.VISIBLE);
//            emptyView.setVisibility(View.GONE);
//            return;
//        }
//
//        listView.setVisibility(View.GONE);
//        emptyView.setVisibility(View.VISIBLE);
//        showMyEmptyView(emptyView);
//    }
//
//    private void showMyEmptyView(ViewGroup parent) {
//        if (parent.getChildCount() > 0) {
//            parent.removeAllViews();
//        }
//
//        View v = getLayoutInflater().inflate(R.layout.my_empty_view, parent);
//        TextView txtDesc = (TextView) v.findViewById(R.id.empty_view_desc);
//        TextView txtAction = (TextView) v.findViewById(R.id.empty_view_button);
//
//        txtDesc.setText(R.string.hint_empty_selling_list_desc);
//        txtAction.setText(R.string.empty_selling_list_action);
//
//        txtAction.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(SellActivity.this, ListItemActivity.class);
//                startActivity(intent);
//                finish();
//
//                TrackingUtils.trackTouch(VIEW_NAME, "item_empty");
//            }
//        });
//    }
//
//    @Override
//    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
//            throws JSONException {
//        listView.stopRefresh();
//        listView.stopLoadMore();
//        if (url.startsWith(ApiInterface.MY_SELLINGS)) {
//            setDataAdapter();
//        } else if (url.startsWith(ApiInterface.DELETE_ITEM)) {
//            setDataAdapter();
//        }
//    }
//
//    private void setDataAdapter() {
//        if (sellAdapter == null) {
//            sellAdapter = new WatchingAdapter(this, watchingsModel.sellList);
//            listView.setAdapter(sellAdapter);
//        } else {
//            sellAdapter.list = watchingsModel.sellList;
//            sellAdapter.notifyDataSetChanged();
//        }
//
//        if (ModelUtils.isNull(watchingsModel.meta.next)) {
//            listView.setPullLoadEnable(false);
//        } else {
//            listView.setPullLoadEnable(true);
//        }
//        registerForContextMenu(listView);
//
//        isEmpty = sellAdapter.list.isEmpty();
//        showContent();
//    }
//
//    @Override
//    public void onRefresh(int id) {
//        watchingsModel.getSellList();
//    }
//
//    @Override
//    public void onLoadMore(int id) {
//        watchingsModel.getSellListMore();
//    }
//}

