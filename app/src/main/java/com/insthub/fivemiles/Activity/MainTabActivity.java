package com.insthub.fivemiles.Activity;

import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.external.eventbus.EventBus;
import com.facebook.UiLifecycleHelper;
import com.facebook.internal.NativeProtocol;
import com.facebook.widget.LikeView;
import com.igexin.sdk.PushManager;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.AppMessageModel;
import com.insthub.fivemiles.Model.LoginModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.SESSION;
import com.insthub.fivemiles.Utils.LocationManagerUtil;
import com.insthub.fivemiles.Utils.LocationManagerUtil.OnLocationListener;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.SaveCallback;
import com.thirdrock.domain.GeoLocation;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.view.GenericDialog;
import com.thirdrock.fivemiles.main.home.GcmInfoHelper;
import com.thirdrock.fivemiles.main.home.HomeActivity;
import com.thirdrock.fivemiles.main.listing.ListItemActivity;
import com.thirdrock.fivemiles.reco.SellersNearbyActivity;
import com.thirdrock.fivemiles.search.SearchActivity;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.DeepLinkHelper;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.EduScreen;
import com.thirdrock.fivemiles.util.EduUtils;
import com.thirdrock.fivemiles.util.EventUtils;
import com.thirdrock.fivemiles.util.FacebookLinker;
import com.thirdrock.fivemiles.util.LocationUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_GOTO_HOME;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_HOME_TAB;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_MSG_LIST;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REDIRECT_INTENT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SHOW_FB_LIKES;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_TAB_TAG_SPEC;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_REFRESH_HOME_TAB;
import static com.insthub.fivemiles.FiveMilesAppConst.EDU_MAIN_TAB_V;
import static com.insthub.fivemiles.FiveMilesAppConst.GETUI_PROVIDER;
import static com.insthub.fivemiles.FiveMilesAppConst.PARSE_PROVIDER;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_EDU_MAIN_TAB;
import static com.insthub.fivemiles.FiveMilesAppConst.VERSION_CODE_300;
import static com.insthub.fivemiles.MessageConstant.APP_HAS_UPDATE;
import static com.insthub.fivemiles.MessageConstant.GEO_LOCATION_UPDATED;
import static com.insthub.fivemiles.MessageConstant.HOME_TOOLBAR_SEARCH_CLK;
import static com.insthub.fivemiles.MessageConstant.PUSH_ACT_NEW_OFFER;
import static com.insthub.fivemiles.MessageConstant.PUSH_ACT_NEW_SYS_MSG;
import static com.insthub.fivemiles.MessageConstant.PUSH_ACT_REFRESH_HOME;
import static com.insthub.fivemiles.MessageConstant.PUSH_ACT_SHOW_HOME;
import static com.insthub.fivemiles.MessageConstant.REFRESH_HOME;
import static com.insthub.fivemiles.MessageConstant.SIGNOUT;
import static com.insthub.fivemiles.MessageConstant.UNREAD_BUYING;
import static com.insthub.fivemiles.MessageConstant.UNREAD_SELLING;
import static com.insthub.fivemiles.MessageConstant.UNREAD_SYSTEM;
import static com.thirdrock.fivemiles.main.home.HomeActivity.IDX_NEARBY_PAGE;
import static com.thirdrock.framework.util.Utils.getPackageInfo;

public class MainTabActivity extends TabActivity implements BusinessResponse, OnLocationListener {

    public static final String TAB_TAG_SPEC_HOME        = "spec_location";
    public static final String TAB_TAG_SPEC_SEARCH      = "spec_search";
    public static final String TAB_TAG_SPEC_MESSAGE     = "spec_message";
    public static final String TAB_TAG_SPEC_PROFILE     = "spec_profile";
    public static final String GA_TAG_MESSAGE       = "message";
    public static final String GA_TAG_PROFILE       = "profile";
    public static final String GA_TAG_HOME          = "home";
    public static final String GA_TAG_SEARCH        = "discover";

    public static boolean isLive = false;
    private static boolean onTop = false;

    private LocationManagerUtil locationManager;

    private TabHost tabHost;

    private RadioButton rdbLocation, rdbSearch, rdbMessage, rdbProfile;
    private RadioButton currentRdb;

    private LoginModel loginModel;

    private EduScreen eduScreen;

    private ImageView message_unread_num, ivAppNewMsg;
    private AppMessageModel messageModel;

    private boolean buyingUnread, sellingUnread, systemUnread;

    final static String VIEW_NAME = FiveMilesAppConst.VIEW_HOME;

    private boolean fbLikesShown;
    private GenericDialog fbLikesDlg;
    private UiLifecycleHelper fbUiHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventUtils.register(this);

        setContentView(R.layout.main_tab);
        isLive = true;

        // 未登陆，跳转到登陆页
        boolean userNotAuthed = !SESSION.getInstance().isAuth();
        if (userNotAuthed) {
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
            return;
        }

        Button tabAddBtn = (Button) findViewById(R.id.main_tab_add);
        tabHost = getTabHost();

        PushManager.getInstance().initialize(this.getApplicationContext());

        TabSpec spec_location = tabHost.newTabSpec(TAB_TAG_SPEC_HOME).setIndicator(TAB_TAG_SPEC_HOME)
                .setContent(new Intent(MainTabActivity.this, HomeActivity.class));
        tabHost.addTab(spec_location);

        TabSpec spec_search = tabHost.newTabSpec(TAB_TAG_SPEC_SEARCH).setIndicator(TAB_TAG_SPEC_SEARCH)
                .setContent(new Intent(MainTabActivity.this, SearchActivity.class));
        tabHost.addTab(spec_search);

        TabSpec spec_message = tabHost.newTabSpec(TAB_TAG_SPEC_MESSAGE).setIndicator(TAB_TAG_SPEC_MESSAGE)
                .setContent(new Intent(MainTabActivity.this, TabMessageActivity.class));
        tabHost.addTab(spec_message);

        TabSpec spec_profile = tabHost.newTabSpec(TAB_TAG_SPEC_PROFILE).setIndicator(TAB_TAG_SPEC_PROFILE)
                .setContent(new Intent(MainTabActivity.this, TabProfileActivity.class));
        tabHost.addTab(spec_profile);

        message_unread_num = (ImageView) findViewById(R.id.message_unread_num);
        ivAppNewMsg = (ImageView) findViewById(R.id.app_new_message);
        ivAppNewMsg.setVisibility(View.GONE);       // 2.6中底部暂时去掉小红点，未来可能恢复

        rdbMessage = (RadioButton) findViewById(R.id.main_tab_message);

        tabAddBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainTabActivity.this, ListItemActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_buttom_in, R.anim.push_buttom_out);
                TrackingUtils.trackTouch(VIEW_NAME, "sell");
            }
        });

        RadioGroup group = (RadioGroup) this.findViewById(R.id.main_tab_group);
        rdbLocation = (RadioButton) findViewById(R.id.main_tab_location);
        rdbSearch = (RadioButton) findViewById(R.id.main_tab_search);
        rdbProfile = (RadioButton) findViewById(R.id.main_tab_profile);
        rdbLocation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                selectOrRefreshHomeView();
            }
        });

        currentRdb = rdbLocation;
        group.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.main_tab_search:
                        selectSearchView();
                        break;
                    case R.id.main_tab_message:
                        selectMessageView();

                        // post msg to show system message tab in message view
                        String tab = getIntent().getStringExtra(ACT_PARA_TAB_TAG_SPEC);
                        if (TAB_TAG_SPEC_MESSAGE.equalsIgnoreCase(tab)) {
                            Message msg = Message.obtain();
                            msg.what = MessageConstant.OPEN_SYS_MSG;
                            msg.obj = getIntent().getStringExtra(ACT_PARA_MSG_LIST);
                            EventBus.getDefault().post(msg);
                        }

                        // 进入登陆页面，也要记录一次TAB点击
                        TrackingUtils.trackTouch(VIEW_NAME, GA_TAG_MESSAGE);
                        break;
                    case R.id.main_tab_profile:
                        selectProfileView();

                        TrackingUtils.trackTouch(VIEW_NAME, GA_TAG_PROFILE);
                        break;
                }
            }
        });

        locationManager = LocationManagerUtil.getInstance();
        locationManager.setOnLocationListener(this);

        if (locationManager.isEnabled()) {
            locationManager.getLocation();
        }

        loginModel = new LoginModel(this);
        loginModel.addResponseListener(this);

        messageModel = new AppMessageModel(this);
        messageModel.addResponseListener(this);

        // Save the current Installation to Parse.
        final ParseInstallation currentParseInstallation = ParseInstallation.getCurrentInstallation();
        currentParseInstallation.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                if (e == null) {        // 成功
                    String deviceToken = currentParseInstallation.getString("deviceToken");
                    String installId = currentParseInstallation.getInstallationId();
                    L.d("Parse installation id in SESSION: " + SESSION.getInstance().installationId);
                    L.d("parse push deviceToken: %s, installationId: %s", deviceToken, installId);

                    SESSION session = SESSION.getInstance();
                    session.deviceToken = deviceToken;
                    session.installationId = installId;
                    session.save();

                    boolean parseNotRegistered = !SESSION.getInstance().isParseRegistered;
                    if (parseNotRegistered) {
                        loginModel.registerDevice(PARSE_PROVIDER, installId);
                        L.d("registerDevice is called to register parse");
                    }
                } else {
                    String installId = currentParseInstallation.getInstallationId();

                    String errorMsg = "Error saving Parse installation, with installId: "
                            + installId + ", and stack trace: " + e;
                    TrackingUtils.trackException(errorMsg, false);
                }
            }
        });

        // facebook likes
        fbUiHelper = new UiLifecycleHelper(this, null);

        DisplayUtils.setTypeface(this, tabAddBtn, "Museo_300.otf");

        handleRefreshIntent(getIntent());

        handleHomePageIntent(getIntent());

        showSellersNearbyIfNeeded();

        LocationUtils.getGeolocationByGPS();
    }



    private void showSellersNearbyIfNeeded() {
        if (getPackageInfo() == null) {
            return;
        }
        int versionCode = getPackageInfo().versionCode;
        if (VERSION_CODE_300 == versionCode) {  // sellers nearby是3.0的需求，之前的用户需要显示此页面
            boolean sellersNearbyNotShow = !SESSION.getInstance().isSellersNearbyHasShown();
            if (sellersNearbyNotShow) {
                Intent sellersNearbyIntent = new Intent(MainTabActivity.this, SellersNearbyActivity.class);
                startActivity(sellersNearbyIntent);
            }
        }
    }

    private void selectProfileView() {
        tabHost.setCurrentTabByTag(TAB_TAG_SPEC_PROFILE);
        currentRdb = rdbProfile;
        currentRdb.setChecked(true);
    }

    private void selectMessageView() {
        tabHost.setCurrentTabByTag(TAB_TAG_SPEC_MESSAGE);
        currentRdb = rdbMessage;
        currentRdb.setChecked(true);
    }

    private void selectSearchView() {
        tabHost.setCurrentTabByTag(TAB_TAG_SPEC_SEARCH);
        currentRdb = rdbSearch;
        currentRdb.setChecked(true);                    // not necessary, for consistence

        TrackingUtils.trackTouch(VIEW_NAME, GA_TAG_SEARCH);
    }

    /**
     * getIntent返回的是打开Activity的intent，
     */
    private boolean redirectIfNeed() {
        Intent startIntent = getIntent();
        return redirectIfNeed(startIntent);
    }

    /**
     * 若intent中的ACT_PARA_REDIRECT_INTENT，则跳入相应的tab或打开相应的activity。
     * @param pIntent 传入的intent
     * @return true：需要跳转；false：不需要跳转
     */
    private boolean redirectIfNeed(Intent pIntent) {
        if (pIntent.hasExtra(ACT_PARA_REDIRECT_INTENT)) {
            Intent rdIntent = pIntent.getParcelableExtra(ACT_PARA_REDIRECT_INTENT);
            if (rdIntent != null) {
                if (rdIntent.getComponent() != null) {
                    String targetClassName = rdIntent.getComponent().getClassName();

                    if (TabMessageActivity.class.getName().equals(targetClassName)) {
                        selectMessageView();
                        return true;
                    }

                    if (TabProfileActivity.class.getName().equals(targetClassName)) {
                        selectProfileView();
                        return true;
                    }
                }

                startActivity(rdIntent);
                return true;
            }
        }
        return false;
    }

    // user education screen
    private void showEducation() {
        if (!shouldShowEducation() ||
                (eduScreen != null && eduScreen.isShowing())) {
            return;
        }

        eduScreen = EduUtils.showEduScreen(this, R.layout.edu_main_screen,
                PREF_EDU_MAIN_TAB, EDU_MAIN_TAB_V);
    }

    private boolean shouldShowEducation() {
//        Intent intent = getIntent();
//        if (intent == null) {
            return false;
//        }
        // 若有多线程，这里可能还可能报错
//        String from = intent.getStringExtra("from");
//        boolean newUser = RegisterUserActivity.VIEW_NAME.equals(from) ||
//                WelcomeActivity.VIEW_NAME.equals(from) ||
//                QuickTourActivity.VIEW_NAME.equals(from) ||
//                ZipcodeVerifyActivity.VIEW_ID.equals(from);
//
//        return newUser && EduUtils.shouldShow(PREF_EDU_MAIN_TAB, EDU_MAIN_TAB_V);
    }

    private void hideEducation() {
        if (eduScreen != null && eduScreen.isShowing()) {
            eduScreen.dismiss();
        }
    }

    private void handleRefreshIntent(Intent intent) {
        if (redirectIfNeed()) {
            return;
        }

        if (ACT_REFRESH_HOME_TAB.equals(intent.getAction())) {

            String tab = intent.getStringExtra(ACT_PARA_TAB_TAG_SPEC);

            if (TAB_TAG_SPEC_MESSAGE.equalsIgnoreCase(tab)) {
                rdbMessage.setChecked(true);

                Message msg = Message.obtain();
                msg.what = MessageConstant.PUSH_ACT_REFRESH_MSG;
                msg.obj = intent.getStringExtra(ACT_PARA_MSG_LIST);
                EventBus.getDefault().post(msg);
            } else if (TAB_TAG_SPEC_PROFILE.equalsIgnoreCase(tab)) {
                rdbProfile.setChecked(true);
            } else if (TAB_TAG_SPEC_SEARCH.equalsIgnoreCase(tab)) {
                rdbSearch.setChecked(true);

                Message msg = Message.obtain();
                msg.what = MessageConstant.PUSH_ACT_SEARCH;

                Bundle data = msg.getData();
                data.putAll(intent.getExtras());

                EventBus.getDefault().post(msg);
            } else {
                L.w("unknown tab: %s", tab);
            }
        }
    }

    /**
     * 若已在Home页，则刷新；否则，切换到Home页，但不刷新；
     */
    private void selectOrRefreshHomeView() {
        String currTabTag = tabHost.getCurrentTabTag();

        // 已在Home页，刷新；否则，切换tab页。
        if (TAB_TAG_SPEC_HOME.equals(currTabTag)) {
            Message msg = new Message();
            msg.what = REFRESH_HOME;
            EventBus.getDefault().post(msg);
            TrackingUtils.trackTouch(VIEW_NAME, "hometotop");
        } else {
            tabHost.setCurrentTabByTag(TAB_TAG_SPEC_HOME);
            currentRdb = rdbLocation;
            currentRdb.setChecked(true);
        }

        TrackingUtils.trackTouch(VIEW_NAME, GA_TAG_HOME);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        redirectIfNeed(intent);

        if (ACT_GOTO_HOME.equals(intent.getAction())) {
            selectOrRefreshHomeView();
        } else if (ACT_REFRESH_HOME_TAB.equals(intent.getAction())) {
            handleRefreshIntent(intent);
        }

        handleHomePageIntent(intent);
    }

    private void handleHomePageIntent(Intent intent) {
        if(intent == null) {
            L.w("intent should not be null");
            return;
        }

        if(intent.hasExtra(ACT_PARA_HOME_TAB)) {
            int homePageIdx = intent.getIntExtra(ACT_PARA_HOME_TAB, IDX_NEARBY_PAGE);

            EventUtils.postSticky(homePageIdx);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // 匿名用户
        JSONObject referringParams = FiveMilesApp.getInstance().retrieveReferringParams();
        DeepLinkHelper.handleDeepLink(referringParams);
        FiveMilesApp.getInstance().clearReferringParams();
        L.i("branch link: params cleared");

        showEducation();

        EventUtils.register(MainTabActivity.this);

        // show facebook likes?
        if (!fbLikesShown && getIntent().getBooleanExtra(ACT_PARA_SHOW_FB_LIKES, false)) {
            showFbLikes();
        }

        // 若个推未注册，注册之
        boolean getuiNotRegistered = !SESSION.getInstance().isGeTuiRegistered;
        boolean clientIdReceived = ModelUtils.isNotEmpty(SESSION.getInstance().clientId);
        if (clientIdReceived && getuiNotRegistered) {
            loginModel.registerDevice(GETUI_PROVIDER, SESSION.getInstance().clientId);
        }

        // 若Parse未注册，注册之
        boolean parseNotRegistered = !SESSION.getInstance().isParseRegistered;
        boolean installIdGenerated = ModelUtils.isNotEmpty(SESSION.getInstance().installationId);
        if (installIdGenerated && parseNotRegistered) {
            loginModel.registerDevice(PARSE_PROVIDER, SESSION.getInstance().installationId);
        }

        // 测试环境没有api，到生产环境再调用
        if (Utils.isRelease()) {
            // 若需要注册gcm，注册gcm
            GcmInfoHelper.getInstance().registerGcmInBackgroundIfNecessary();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        messageModel.getMessageUnreadCount();
        locationManager.requestLocationUpdates();
        onTop = true;

        CloudHelper.checkUpdate(this);
        FacebookLinker.validateToken(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        locationManager.stopLocationUpdates();
        onTop = false;
    }

    @Override
    protected void onStop() {
        FiveMilesApp.getInstance().clearReferringParams();
        hideEducation();
        closeFbLikes();
        super.onStop();
    }



    @Override
    public void onDestroy() {
        isLive = false;
        EventUtils.unregister(MainTabActivity.this);
        super.onDestroy();
    }

    public void onEvent(Object event) {
        Message message = (Message) event;

        switch (message.what) {
            case UNREAD_BUYING:
                buyingUnread = (Boolean) message.obj;
                break;
            case UNREAD_SELLING:
                sellingUnread = (Boolean) message.obj;
                break;
            case UNREAD_SYSTEM:
                systemUnread = (Boolean) message.obj;
                break;
            case PUSH_ACT_NEW_OFFER:
                messageModel.getMessageUnreadCount();
                break;
            case SIGNOUT:
                // close main activity when logout
                finish();
                break;
            case PUSH_ACT_NEW_SYS_MSG:
                messageModel.getMessageUnreadCount();
                break;
            case PUSH_ACT_REFRESH_HOME:
                DisplayUtils.cancelNotification(message.arg1);
                selectOrRefreshHomeView();
                rdbLocation.setChecked(true);
                break;
            case PUSH_ACT_SHOW_HOME:
                selectOrRefreshHomeView();
                break;
            case APP_HAS_UPDATE:
                ivAppNewMsg.setVisibility(View.GONE);       // 2.6中底部暂时去掉小红点，未来可能恢复
                break;
            case GEO_LOCATION_UPDATED:
                GeoLocation location = (GeoLocation)message.obj;
                LocationUtils.registUserLocation(location);
                break;
            case HOME_TOOLBAR_SEARCH_CLK:
                selectSearchView();
                break;
        }

        boolean unread = buyingUnread || sellingUnread || systemUnread;
        message_unread_num.setVisibility(unread ? View.VISIBLE : View.GONE);
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
            throws JSONException {
        if (url.startsWith(ApiInterface.MESSAGE_UNREAD_NUM)) {
            buyingUnread = messageModel.buyingUnread;
            sellingUnread = messageModel.sellingUnread;
            systemUnread = messageModel.systemUnread;

            if (messageModel.unread_num > 0) {
                message_unread_num.setVisibility(View.VISIBLE);
            } else {
                message_unread_num.setVisibility(View.GONE);
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fbUiHelper.onActivityResult(requestCode, resultCode, data);
        // facebook dialog completed
        if (NativeProtocol.DIALOG_REQUEST_CODE == requestCode) {
            closeFbLikes();
        }

    }

    @Override
    public void onLocationUpdated() {
        // nothing to do
    }

    public static boolean isSysMsgViewOnTop() {
        return onTop && TabMessageActivity.isSysViewOnTop();
    }

    private void showFbLikes() {
        if (fbLikesShown || (fbLikesDlg != null && fbLikesDlg.isShowing())) {
            return;
        }

        if (fbLikesDlg == null) {
            fbLikesDlg = new GenericDialog(this, R.layout.dialog_fb_likes, new GenericDialog.ViewController() {
                @Override
                public void onInit(final GenericDialog dialog, View v) {
                    LikeView likeView = (LikeView) v.findViewById(R.id.fb_like_view);
                    likeView.setObjectId("https://www.facebook.com/5milesapp");

                    v.findViewById(R.id.btn_cancel).setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            TrackingUtils.trackTouch("likefacebook_popup", "likefacebookpageno");
                        }
                    });

                    v.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // 拦截click事件，避免点击背景关闭对话框
                        }
                    });
                }
            });

            fbLikesDlg.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    TrackingUtils.trackTouch("likefacebook_popup", "likefacebookpageno");
                }
            });
        }

        fbLikesDlg.setCanceledOnTouchOutside(false).show();
        fbLikesShown = true;  // ensure facebook likes dialog shown only once
        TrackingUtils.trackTouch("likefacebook_popup", "likefacebookpage");
    }

    private void closeFbLikes() {
        if (fbLikesDlg != null) {
            fbLikesDlg.dismiss();
            fbLikesDlg = null;
        }
    }
}
