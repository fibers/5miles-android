package com.insthub.fivemiles.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Message;

import com.external.eventbus.EventBus;
import com.facebook.Session;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.LoginModel;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.fivemiles.main.home.GcmInfoHelper;
import com.thirdrock.framework.util.Utils;

/**
 * Actions taken on sign out
 * Created by lishangqiang on 2014/9/10.
 */
public class SignOutAction {

    public Context context;
    public LoginModel loginModel;

    public SignOutAction(LoginModel loginModel) {
        this.context = loginModel.getContext();
        this.loginModel = loginModel;
    }

    public SignOutAction(Context context) {
        this.context = context;
        this.loginModel = new LoginModel(context);
    }

    public void signOut() {
        signOut(WelcomeActivity.class);
    }

    public void signOut(Class targetActivity) {
        boolean isNotAuthed = !SESSION.getInstance().isAuth();
        if (isNotAuthed) {      // 未登录，无需logout
            return;
        }

        // close facebook session, if logged in with facebook
        Session fbSession = Session.getActiveSession();
        if (fbSession != null && fbSession.isOpened()) {
            fbSession.closeAndClearTokenInformation();
        }

        redirectToLogin(context, targetActivity, null);

        // clean session
        SESSION.clear();

        // 测试环境没有api，到生产环境再调用
        if (Utils.isRelease()) {
            String gcmSenderId = SESSION.getInstance().getGcmSenderId();
            GcmInfoHelper.getInstance().unRegisterGcmInBackground(gcmSenderId);
        }
    }

    public static void redirectToLogin(Context context, Intent redirectIntent) {
        redirectToLogin(context, WelcomeActivity.class, redirectIntent);
    }

    public static void redirectToLogin(Context context, Class targetActivity, Intent redirectIntent) {
        // 关闭主activity并跳转到登录页
        Intent intent = new Intent(context, targetActivity).addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        if (redirectIntent != null) {
            intent.putExtra(FiveMilesAppConst.ACT_PARA_REDIRECT_INTENT, redirectIntent);
        }

        context.startActivity(intent);

        Message msg = Message.obtain();
        msg.what = MessageConstant.SIGNOUT;
        EventBus.getDefault().post(msg);
    }
}
