package com.insthub.fivemiles.Activity;

import android.net.Uri;
import android.util.Pair;

import com.BeeFramework.Utils.ImageUtil;
import com.insthub.fivemiles.FiveMilesApp;
import com.thirdrock.domain.ImageInfo;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.rest.RestObserver;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.CloudSignature;
import com.thirdrock.protocol.MakeOfferResp;
import com.thirdrock.protocol.offer.ChatMessage;
import com.thirdrock.repository.ItemRepository;
import com.thirdrock.repository.OfferRepository;
import com.thirdrock.repository.UserRepository;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

import static com.insthub.fivemiles.Activity.MakeOfferViewModel.Property.block_user;
import static com.insthub.fivemiles.Activity.MakeOfferViewModel.Property.compress_sign;
import static com.insthub.fivemiles.Activity.MakeOfferViewModel.Property.send_location_start;
import static com.insthub.fivemiles.Activity.MakeOfferViewModel.Property.upload_image_start;
import static com.insthub.fivemiles.FiveMilesAppConst.EMPTY_STRING;
import static com.insthub.fivemiles.FiveMilesAppConst.IMAGE_MAX_HEIGHT;
import static com.insthub.fivemiles.FiveMilesAppConst.IMAGE_MAX_WIDTH;
import static com.thirdrock.repository.OfferRepository.MAKE_OFFER_URL;
import static com.thirdrock.repository.OfferRepository.OFFER_LINE_UPDATE_URL;
import static com.thirdrock.repository.OfferRepository.OFFER_LINE_URL;

/**
 * ViewModel for MakeOfferActivity
 * Created by jubin on 27/5/15.
 */
public class MakeOfferViewModel extends AbsViewModel<MakeOfferViewModel.Property>{

    public enum Property{
        block_user, txt_msg_sent, img_msg_sent, loc_msg_sent, offer_line, offer_line_update
        , upload_image_start, upload_image_end, compress_sign, send_location_start
        , upload_image_start_kitkat
    }

    private Map<String, String> compressedImages;
    private Subscription subsMakeOffer, subsOfferline, subsBlockUser, subsUpdate;
    private RestObserver<Object> offerLineObserver, updateObserver;
    private RestObserver<MakeOfferResp> txtMsgSentObserver, imgMsgSentObserver, locMsgSentObserver;
    private Observer<Object> blockUserObserver, imageUploadObserver;

    @Inject UserRepository userRepo;
    @Inject OfferRepository offerRepo;
    @Inject ItemRepository itemRepo;

    public MakeOfferViewModel() {
        compressedImages = new HashMap<>();

        txtMsgSentObserver = newMinorApiJobObserver(MAKE_OFFER_URL, Property.txt_msg_sent);
        imgMsgSentObserver = newMinorApiJobObserver(MAKE_OFFER_URL, Property.img_msg_sent);
        locMsgSentObserver = newMinorApiJobObserver(MAKE_OFFER_URL, Property.loc_msg_sent);
        offerLineObserver = newMajorApiJobObserver(OFFER_LINE_URL, Property.offer_line);
        updateObserver = newMinorApiJobObserver(OFFER_LINE_UPDATE_URL, Property.offer_line_update);
        blockUserObserver = newMinorJobObserver(block_user);
        imageUploadObserver = newMinorJobObserver(Property.upload_image_end);
    }

    public void getOfferLineDetails(int offerlineId) {
        emitMajorJobStarted();
        subsOfferline = offerRepo.getOfferlineDetails(offerlineId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(offerLineObserver.reset());
    }

    public void blockUser(String uid, boolean userBlockStatus) {
        boolean isBlocking = !userBlockStatus;  // TODO isBLocking的命名需要修改
        subsBlockUser = userRepo.blockUser(uid, isBlocking)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(blockUserObserver);
    }

    public void sendTextMessage(String toUid, String itemId, String price, String text) {
        subsMakeOffer = offerRepo.sendTextMessage(toUid, itemId, price, text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(txtMsgSentObserver.reset());
    }

    public void sendImageMessage(String pToUid, String pItemId, String pImgUrl) {
        subsMakeOffer = offerRepo.sendImageMessage(pToUid, pItemId, pImgUrl)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(imgMsgSentObserver.reset());
    }

    public void sendLocationMessage(String pToUid, String pItemId, CharSequence pAddress
            , CharSequence pPlaceName, double pLat, double pLon, String pMapThumbUrl, boolean pIsSeller) {
        String placeNameStr = (pPlaceName == null)? EMPTY_STRING : pPlaceName.toString();
        String addressStr = (pAddress == null)? EMPTY_STRING : pAddress.toString();
        ChatMessage message = ChatMessage.getTempLocationMessage(pLat, pLon, placeNameStr, addressStr, pMapThumbUrl, pIsSeller);
        emit(send_location_start, null, message);
        subsMakeOffer = offerRepo.sendLocationMessage(pToUid, pItemId, placeNameStr, addressStr, pLat, pLon, pMapThumbUrl)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(locMsgSentObserver.reset());
    }

    public void syncOfferlineUpdates(int offerLineId, long lastMsgTimestamp) {
        subsUpdate = offerRepo.syncOfferlieUpdates(offerLineId, lastMsgTimestamp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(updateObserver.reset());
    }

    // TODO 不知RxAndroid会不会自动unsubscribe
    public synchronized void unSubscribeAll() {
        unsubscribe(subsOfferline, subsBlockUser, subsMakeOffer, subsUpdate);
    }

    // TODO 通过将文件路径转为Uri的方式，合并两个uploadImage
    public void uploadImage(final Uri pImageUri) {
        if (pImageUri == null) {
            L.i("uploadImage(), pImageUri should not be null");
            return;
        }
        // TODO 若能返回Uri，则效果会更好。可以先用File类型的Uri，后面再考虑封装为Provider
        final String resultBitmapPath = ImageUtil.adjustImage(pImageUri, IMAGE_MAX_HEIGHT, IMAGE_MAX_WIDTH);

        if (ModelUtils.isEmpty(resultBitmapPath)) {
            L.w("uploadImage(), resultBitmapPath is empty");
            return;
        }

        compressedImages.put(pImageUri.toString(), resultBitmapPath);

        uploadImage(resultBitmapPath);
    }

    public void uploadImage(final String pImagePath) {
        if (ModelUtils.isEmpty(pImagePath)) {
            L.i("uploadImage(), pImageFilePath should not be " + pImagePath);
        }

        emit(upload_image_start, null, pImagePath);

        final Observable<String> processObservable =
                Observable.create(new Observable.OnSubscribe<String>() {
                    @Override
                    public void call(Subscriber<? super String> subscriber) {
                        String compressedImgPath = compressedImages.get(pImagePath);
                        if (compressedImgPath == null) {
                            compressedImgPath = ImageUtil.adjustImage(pImagePath, IMAGE_MAX_HEIGHT, IMAGE_MAX_WIDTH);
                            compressedImages.put(pImagePath, compressedImgPath);
                        }

                        subscriber.onNext(compressedImgPath);
                        subscriber.onCompleted();
                    }
                });
        Observable<CloudSignature> sign = itemRepo.signCloudUpload();

        Observable.zip(processObservable, sign, new Func2<String, CloudSignature, Pair<String, CloudSignature>>() {
            @Override
            public Pair<String, CloudSignature> call(String zImgPath, CloudSignature sign) {
                emit(compress_sign, null, zImgPath);
                return new Pair<>(zImgPath, sign);
            }
        }).flatMap(new Func1<Pair<String, CloudSignature>, Observable<Map>>() {
            @Override
            public Observable<Map> call(Pair<String, CloudSignature> data) {
                // upload image
                return itemRepo.uploadToCloud(FiveMilesApp.appCtx, data.first, data.second);
            }
        }).map(new Func1<Map, ImageInfo>() {
            @Override
            public ImageInfo call(Map map) {
                String url = "";
                int width = 0, height = 0;
                if (map != null) {
                    if (map.containsKey("url")) {
                        url = (String) map.get("url");
                    }
                    if (map.containsKey("width")) {
                        width = (int) map.get("width");
                    }
                    if (map.containsKey("height")) {
                        height = (int) map.get("height");
                    }
                }
                ImageInfo imageInfo = new ImageInfo(url, width, height);
                L.d("image upload result: %s -> %s", pImagePath, imageInfo.getUrl());
                return imageInfo;
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(imageUploadObserver);
    }
}
