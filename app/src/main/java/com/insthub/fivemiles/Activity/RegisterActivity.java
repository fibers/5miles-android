package com.insthub.fivemiles.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.BeeFramework.BeeFrameworkApp;
import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BeeQuery;
import com.BeeFramework.model.BusinessResponse;
import com.BeeFramework.view.ToastView;
import com.external.androidquery.callback.AjaxStatus;
import com.external.eventbus.EventBus;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.LoginModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.SESSION;
import com.insthub.fivemiles.Utils.LocationManagerUtil;
import com.thirdrock.domain.GeoLocation;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.login.ZipcodeVerifyActivity;
import com.thirdrock.fivemiles.util.EventUtils;
import com.thirdrock.fivemiles.util.LocationUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REDIRECT_INTENT;
import static com.insthub.fivemiles.FiveMilesAppConst.GETUI_PROVIDER;
import static com.insthub.fivemiles.MessageConstant.GEO_LOCATION_UPDATED;
import static com.insthub.fivemiles.MessageConstant.SIGN_UP_SUCCESS;


public class RegisterActivity extends BaseActivity implements BusinessResponse {

	public static final Pattern EML_PATTERN = Pattern.compile(
            "^[-.a-z_+\\d]+@[-a-z_\\d]+(\\.[-a-z_\\d]+)+$",
            Pattern.CASE_INSENSITIVE);

	private ImageView 		back;
    private TextView        txtTerms;
	private EditText 		edtName, edtEmail, edtPassword;

	private LoginModel 		loginModel;

    public static final String VIEW_NAME = "signup_view";

    private GeoLocation geolocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);

        EventUtils.register(this);

        if(SESSION.getInstance().isAuth()) {
            Intent intent = new Intent(this, MainTabActivity.class);
			startActivity(intent);
			finish();
            return;
		}

		if(BeeQuery.environment() == BeeQuery.ENVIROMENT_DEVELOPMENT || BeeQuery.environment() == BeeQuery.ENVIROMENT_MOCKSERVER)
        {
            BeeFrameworkApp.getInstance().showBug(this);
        }

		back 			= (ImageView) 		findViewById(R.id.register_back);
        edtName         = (EditText)        findViewById(R.id.login_nickname);
        edtEmail        = (EditText)        findViewById(R.id.login_email);
        edtPassword     = (EditText)        findViewById(R.id.login_password);
        txtTerms        = (TextView)        findViewById(R.id.txt_terms);

		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
                TrackingUtils.trackTouch(VIEW_NAME, "signup_back");
				finish();
			}
		});

        edtName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                TrackingUtils.trackTouch(VIEW_NAME, "signup_nickname");
                return false;
            }
        });

        edtEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                TrackingUtils.trackTouch(VIEW_NAME, "signup_email");
                return false;
            }
        });

        edtPassword.setTypeface(Typeface.DEFAULT);  // http://bit.ly/1bNGZeF
        edtPassword.setImeOptions(EditorInfo.IME_ACTION_GO);
        edtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                TrackingUtils.trackTouch(VIEW_NAME, "signup_password");
                if (actionId != EditorInfo.IME_ACTION_GO) {
                    return false;
                }

                doRegister(null);
                return true;
            }
        });
		
		loginModel = new LoginModel(this);
		loginModel.addResponseListener(this);
		
		if (!EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().register(this);
        }

        txtTerms.setMovementMethod(LinkMovementMethod.getInstance());

        LocationUtils.getGeolocationByGPS();

        TrackingUtils.trackView(VIEW_NAME);
    }


	@Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {
        if (url.startsWith(ApiInterface.SIGNUP_EMAIL)) {
            if (jo == null) {
                return;
            }

            SESSION session = SESSION.getInstance();
            session.isAnonymous = false;
            session.userId = loginModel.signupResp.uid;
            session.token = loginModel.signupResp.token;
            session.nickname = loginModel.signupResp.nickname;
            session.avatarUrl = loginModel.signupResp.portrait;
            session.email = loginModel.signupResp.email;
            session.createdAt = loginModel.signupResp.createdAt;
            session.lastLogin = loginModel.signupResp.lastLogin;
            session.save();

            boolean sessionTokenNotEmpty = !TextUtils.isEmpty(session.token);
            if (sessionTokenNotEmpty) {
                TrackingUtils.trackAFEvent("registration", null);

                Message msg = new Message();
                msg.what = MessageConstant.SIGN_UP_SUCCESS;
                EventBus.getDefault().post(msg);

                LocationManagerUtil locationMgr = LocationManagerUtil.getInstance();
                if(locationMgr.isEnabled()) {
                    startActivity(new Intent(RegisterActivity.this, RegisterUserActivity.class));
                }else{
                    startActivity(new Intent(RegisterActivity.this, ZipcodeVerifyActivity.class)
                            .putExtra("from", VIEW_NAME));
                }
            }


            boolean isClientIdExist = ModelUtils.isNotEmpty(SESSION.getInstance().clientId);
            if (isClientIdExist) {
                loginModel.registerDevice(GETUI_PROVIDER, SESSION.getInstance().clientId);
            }
        }
	}
	
    public void onEvent(Object event) {
    	Message message = (Message)event;

        switch (message.what) {
            case SIGN_UP_SUCCESS:
                finish();
                break;
            case GEO_LOCATION_UPDATED:
                geolocation = (GeoLocation) message.obj;
                break;
        }
    }

    @Override
    protected void onDestroy() {
    	if (EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().unregister(this);
        }
    	super.onDestroy();
    }

    private int verifySignup() {
        int error = 0;

        CharSequence name = edtName.getText();
        if (TextUtils.isEmpty(name)) {
            return R.string.error_signup_name_required;
        }

        CharSequence email = edtEmail.getText();
        if (TextUtils.isEmpty(email)) {
            return R.string.error_invalid_email;
        }
        else if (!EML_PATTERN.matcher(email).matches()) {
            return R.string.error_invalid_email;
        }

        CharSequence psswd = edtPassword.getText();
        if (TextUtils.isEmpty(psswd)) {
            return R.string.error_password_required;
        }
        else if (psswd.length() < 6 || psswd.length() > 16) {
            return R.string.error_invalid_password;
        }

        return error;
    }

    public void doRegister(View v) {
        int error = verifySignup();
        if (error > 0) {
            ToastView toast = new ToastView(RegisterActivity.this, error);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            return;
        }

        loginModel.signupEmail(edtName.getText().toString(),
                edtEmail.getText().toString(),
                edtPassword.getText().toString(), geolocation);

        TrackingUtils.trackTouch(VIEW_NAME, "signup_confirm");
    }

    // add redirection target before start next activity
    @Override
    public void startActivity(Intent intent) {
        intent.putExtra(ACT_PARA_REDIRECT_INTENT, getIntent().getParcelableExtra(ACT_PARA_REDIRECT_INTENT));
        super.startActivity(intent);
    }

    @Override
    protected boolean isLoginProtected() {
        return false;
    }
}
