package com.insthub.fivemiles.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.BeeFramework.view.ToastView;
import com.external.androidquery.callback.AjaxStatus;
import com.insthub.fivemiles.Model.LoginModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.TrackingUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangeNicknameActivity extends BaseActivity implements BusinessResponse {
	public static final String VIEW_NAME = "changename_view";

	private EditText nickname;
	private LoginModel loginModel;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_nickname);

		String name = getIntent().getStringExtra("nickname");

		ImageView back = (ImageView) findViewById(R.id.nickname_back);
		TextView done = (TextView) findViewById(R.id.nickname_done);
		nickname = (EditText) findViewById(R.id.nickname_edit);
		
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
                TrackingUtils.trackTouch(VIEW_NAME, "changename_back");
			}
		});
		
		nickname.setText(name);
		
		loginModel = new LoginModel(this);
		loginModel.addResponseListener(this);
		
		done.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                submitNickname();
            }
        });
		
		nickname.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                submitNickname();
                return false;
            }
        });

        TrackingUtils.trackView(VIEW_NAME);
	}

    private void submitNickname() {
        if(nickname.getText().toString().equals("")) {
            ToastView toast = new ToastView(ChangeNicknameActivity.this, getString(R.string.msg_input_nickname));
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            // 设为null，服务器会使用以前的值
            loginModel.fillProfile(null, nickname.getText().toString());

            TrackingUtils.trackTouch(VIEW_NAME, "changename_save");
        }
    }

	@Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {
		if(url.startsWith(ApiInterface.USER_EDIT_PROFILE)) {
			Intent intent = new Intent();
			intent.putExtra("nickname", nickname.getText().toString());
			setResult(Activity.RESULT_OK, intent); 
			ToastView toast = new ToastView(ChangeNicknameActivity.this, getString(R.string.msg_action_success));
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
			finish();
		}
	}
	
}
