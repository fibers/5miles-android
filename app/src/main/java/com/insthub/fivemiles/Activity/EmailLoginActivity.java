package com.insthub.fivemiles.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.external.eventbus.EventBus;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.LoginModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.SESSION;
import com.insthub.fivemiles.Utils.LocationManagerUtil;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.login.ResetPasswordActivity;
import com.thirdrock.fivemiles.login.ZipcodeVerifyActivity;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REDIRECT_INTENT;
import static com.insthub.fivemiles.FiveMilesAppConst.GETUI_PROVIDER;

public class EmailLoginActivity extends BaseActivity implements BusinessResponse {

	private ImageView 	back;
	private EditText 	edtEmail, edtPasswd;

	private LoginModel 	loginModel;

    public static final String VIEW_NAME = "signin_view";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        if(SESSION.getInstance().isAuth()) {
            startActivity(new Intent(this, MainTabActivity.class));
            finish();
            return;
        }

		setContentView(R.layout.login);
		back 			= (ImageView) 	findViewById(R.id.top_view_back);
        edtEmail        = (EditText)    findViewById(R.id.login_email);
        edtPasswd       = (EditText)    findViewById(R.id.login_password);

        edtEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                TrackingUtils.trackTouch(VIEW_NAME, "signin_email");
                return false;
            }
        });

        edtPasswd.setTypeface(Typeface.DEFAULT);  // http://bit.ly/1bNGZeF
        edtPasswd.setImeOptions(EditorInfo.IME_ACTION_GO);
        edtPasswd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                TrackingUtils.trackTouch(VIEW_NAME, "signin_password");

                if (actionId != EditorInfo.IME_ACTION_GO) {
                    return false;
                }

                doLogin(null);
                return true;
            }
        });

        back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
                TrackingUtils.trackTouch(VIEW_NAME, "signin_back");
				finish();
			}
		});

		loginModel = new LoginModel(this);
		loginModel.addResponseListener(this);
		
		if (!EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().register(this);
        }

        TrackingUtils.trackView(VIEW_NAME);
    }

    @Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {
        if (url.startsWith(ApiInterface.LOGIN_EMAIL)) {
            if (jo == null) {
                return;
            }

            SESSION session = SESSION.getInstance();
            session.isAnonymous = false;
            session.userId = loginModel.signupResp.uid;
            session.nickname = loginModel.signupResp.nickname;
            session.avatarUrl = loginModel.signupResp.portrait;
            session.token = loginModel.signupResp.token;
            session.email = loginModel.signupResp.email;
            session.createdAt = loginModel.signupResp.createdAt;
            session.lastLogin = loginModel.signupResp.lastLogin;
            session.setLatitude(loginModel.signupResp.latitude);
            session.setLongitude(loginModel.signupResp.longitude);
            session.setCity(loginModel.signupResp.city);
            session.setRegion(loginModel.signupResp.region);
            session.setCountry(loginModel.signupResp.country);
            session.setFbVerified(loginModel.signupResp.isFacebookVerified);
            session.setFbTokenExpires(loginModel.signupResp.fbTokenExpires);
            session.setRegion(loginModel.signupResp.region);
            session.setCity(loginModel.signupResp.city);
            session.setCountry(loginModel.signupResp.country);
            session.save();

            boolean sessionTokenNotEmpty = !TextUtils.isEmpty(session.token);
            if (sessionTokenNotEmpty) {
                Message msg = new Message();
                msg.what = MessageConstant.SIGN_UP_SUCCESS;
                EventBus.getDefault().post(msg);

                LocationManagerUtil locationMgr = LocationManagerUtil.getInstance();
                if(locationMgr.isEnabled() || locationMgr.isZipcodeRegisted()){
                    Intent intent = new Intent(this, MainTabActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }else{
                    startActivity(new Intent(EmailLoginActivity.this, ZipcodeVerifyActivity.class)
                            .putExtra("from", VIEW_NAME));
                }

                finish();
            }

            // 成功登陆后，若个推已返回client id，注册推送
            boolean isClientIdExist = ModelUtils.isNotEmpty(SESSION.getInstance().clientId);
            if (isClientIdExist) {
                loginModel.registerDevice(GETUI_PROVIDER, SESSION.getInstance().clientId);
            }
        }
	}

    public void onEvent(Object event) {
    	Message message = (Message)event;
    	if(message.what == MessageConstant.SIGN_UP_SUCCESS) {
    		finish();
        }
    }
    
    @Override
    protected void onDestroy() {
    	if (EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().unregister(this);
        }
    	super.onDestroy();
    }

    private int verifyLogin() {
        int error = 0;

        CharSequence email = edtEmail.getText();
        if (TextUtils.isEmpty(email)) {
            return R.string.error_invalid_email;
        }

        CharSequence passwd = edtPasswd.getText();
        if (TextUtils.isEmpty(passwd)) {
            return R.string.error_password_required;
        }
        else if (passwd.length() < 6 || passwd.length() > 16) {
            return R.string.error_invalid_password;
        }

        return error;
    }

    @SuppressWarnings("unused")
    public void doLogin(View v) {
        int errorResId = verifyLogin();
        if (errorResId > 0) {
            DisplayUtils.toast(errorResId);
        } else {
            loginModel.loginEmail(edtEmail.getText().toString(), edtPasswd.getText().toString());
        }

        TrackingUtils.trackTouch(VIEW_NAME, "signin_confirm");
    }

    @SuppressWarnings("unused")
    public void redirectToRegister(View v) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);

        TrackingUtils.trackTouch(VIEW_NAME, "createaccount");
    }

    @SuppressWarnings("ununsed")
    public void redirectToReset(View view) {
        startActivity(new Intent(this, ResetPasswordActivity.class));
        TrackingUtils.trackTouch(VIEW_NAME, "forgotpassword");
    }

    // add redirection target before start next activity
    @Override
    public void startActivity(Intent intent) {
        intent.putExtra(ACT_PARA_REDIRECT_INTENT, getIntent().getParcelableExtra(ACT_PARA_REDIRECT_INTENT));
        super.startActivity(intent);
    }

    @Override
    protected boolean isLoginProtected() {
        return false;
    }
}
