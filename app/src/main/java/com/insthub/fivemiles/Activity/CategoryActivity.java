package com.insthub.fivemiles.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.facebook.AppEventsLogger;
import com.pedrogomez.renderers.AdapteeCollection;
import com.pedrogomez.renderers.RendererAdapter;
import com.thirdrock.domain.CategoryInfo;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.common.category.CategoryListItemRenderer;
import com.thirdrock.fivemiles.main.home.CategoryHelper;
import com.thirdrock.framework.ui.renderer.MonoRendererBuilder;
import com.thirdrock.framework.ui.renderer.SimpleAdapteeCollection;

public class CategoryActivity extends BaseActivity {
	
	private ImageView back;
	private TextView title;

    private ListView catListView;
    private AdapteeCollection<CategoryInfo> catAdptColl;
    private RendererAdapter<CategoryInfo> catListAdapter;

    private CategoryHelper catgryHelper;

    private AppEventsLogger fbEventsLogger;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fbEventsLogger = AppEventsLogger.newLogger(this);

        setContentView(R.layout.category);

        back = (ImageView) findViewById(R.id.top_view_back);
        title = (TextView) findViewById(R.id.top_view_title);
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title.setText(R.string.category);

        catgryHelper = new CategoryHelper();
        catListView = (ListView) findViewById(R.id.category_list);

        catAdptColl = new SimpleAdapteeCollection<>();
        catgryHelper.populateCategoryCollection(catAdptColl);
        catListAdapter = new RendererAdapter<>(getLayoutInflater()
                , new MonoRendererBuilder(new CategoryListItemRenderer(getApplicationContext()))
                , catAdptColl);
        
        catListView.setAdapter(catListAdapter);



        catListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent();
                CategoryInfo category = catListAdapter.getItem(position);

                if(category != null){
                    fbEventsLogger.logEvent("category_" + category.getId());
                }

                if (category != null) {
                    intent.putExtra("categoryId", category.getId());
                    intent.putExtra("categoryTitle", category.getTitle());
                } else {
                    // TODO 错误处理，可以用GA追踪并给用户适当提示
                }
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }
}
