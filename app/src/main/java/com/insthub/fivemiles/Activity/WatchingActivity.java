package com.insthub.fivemiles.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.external.maxwin.view.IXListViewListener;
import com.external.maxwin.view.XListView;
import com.insthub.fivemiles.Adapter.WatchingAdapter;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.Model.WatchingsModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.HOME_OBJECTS;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class WatchingActivity extends BaseActivity implements IXListViewListener, BusinessResponse {

    private TextView 		title;
	private XListView 		listView;
	private WatchingsModel 	watchingsModel;
	private WatchingAdapter watchingAdapter;

    private boolean isEmpty;  // in empty page mode

    public static final String VIEW_NAME = "mylikes_view";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.common_list_view);

        ImageView back = (ImageView) findViewById(R.id.top_view_back);
		title 		= (TextView) 	findViewById(R.id.top_view_title);
		listView 	= (XListView) 	findViewById(R.id.list);
		
		back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                TrackingUtils.trackTouch(VIEW_NAME, "mylikes_back");
            }
        });
		
		title.setText(R.string.profile_likes);
		
		listView.setXListViewListener(this, 1);
		listView.setPullLoadEnable(false);
		listView.setRefreshTime();

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
                Intent intent = new Intent(WatchingActivity.this, ItemActivity.class);
                HOME_OBJECTS item = watchingsModel.watchingList.get(position - 1);
                intent.putExtra(FiveMilesAppConst.ACT_PARA_ITEM_ID, item.id);
                startActivity(intent);
                TrackingUtils.trackTouch(VIEW_NAME, "mylikes_product");
			}
		});

		watchingsModel = new WatchingsModel(this);
		watchingsModel.addResponseListener(this);

        watchingAdapter = new WatchingAdapter(this, watchingsModel.watchingList);
        listView.setAdapter(watchingAdapter);

        isEmpty = 0 == getIntent().getIntExtra(FiveMilesAppConst.ACT_PARA_LIKES_COUNT, 0);
        showContent();

        TrackingUtils.trackView(VIEW_NAME);
	}

    @Override
    protected void onStart() {
        super.onStart();
        if (!isEmpty) {
            listView.refreshNow();
        }
    }

    private void showContent() {
        ViewGroup emptyView = (ViewGroup) findViewById(R.id.empty_view_placeholder);

        if (!isEmpty) {
            listView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            return;
        }

        listView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        showMyEmptyView(emptyView);
    }

    private void showMyEmptyView(ViewGroup parent) {
        if (parent.getChildCount() > 0) {
            parent.removeAllViews();
        }

        View v = getLayoutInflater().inflate(R.layout.my_empty_view, parent);
        TextView txtDesc = (TextView) v.findViewById(R.id.empty_view_desc);
        TextView txtAction = (TextView) v.findViewById(R.id.empty_view_button);

        txtDesc.setText(R.string.hint_empty_likes_list_desc);
        txtAction.setText(R.string.empty_view_action_go_explore);

        txtAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WatchingActivity.this, MainTabActivity.class);
                intent.setAction(FiveMilesAppConst.ACT_GOTO_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

                TrackingUtils.trackTouch(VIEW_NAME, "mylikes_empty");
            }
        });
    }

    @Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {
		listView.stopRefresh();
        listView.stopLoadMore();
		if(url.startsWith(ApiInterface.USER_LIKES)) {
			setDataAdapter();
		}
	}
	
	private void setDataAdapter() {
		if(watchingAdapter == null) {
			watchingAdapter = new WatchingAdapter(this, watchingsModel.watchingList);
			listView.setAdapter(watchingAdapter);
		} else {
			watchingAdapter.list = watchingsModel.watchingList;
			watchingAdapter.notifyDataSetChanged();
		}
        if(ModelUtils.isNull(watchingsModel.meta.next)) {
            listView.setPullLoadEnable(false);
        } else {
            listView.setPullLoadEnable(true);
        }

        isEmpty = watchingAdapter.list.isEmpty();
        showContent();
	}

	@Override
	public void onRefresh(int id) {
		watchingsModel.getWatchingsList();
	}

	@Override
	public void onLoadMore(int id) {
        watchingsModel.getWatchingListMore();
        TrackingUtils.trackTouch(VIEW_NAME, "mylikes_loadmore");
	}

}
