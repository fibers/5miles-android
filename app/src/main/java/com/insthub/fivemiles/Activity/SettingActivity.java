package com.insthub.fivemiles.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.BeeFramework.Utils.AnimationUtil;
import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.LoginModel;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.common.currency.SelectCurrencyActivity;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.Currencies;
import com.thirdrock.fivemiles.util.EventUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_PAGE_URL;
import static com.thirdrock.fivemiles.util.TrackingUtils.trackTouch;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_NICK_NAME;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_EMAIL;


public class SettingActivity extends BaseActivity implements BusinessResponse {

    private static final int REQ_CURRENCY = 1;

	private TextView logon;
	private ImageView back;
	private TextView title;
	
	private View blackView;
	private LinearLayout logonView;
	private TextView confirm;
	private TextView cancel;
	private TextView appVersion, txtUpdate;
    private TextView txtCurrency;
    private ImageView ivAppNewMsg, icUpdate;

    private Currencies currencies;
	private LoginModel loginModel;

    private String email, nickname;

    public static final String VIEW_NAME = FiveMilesAppConst.VIEW_SETTING;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab_profile_setting);
		
		title = (TextView)findViewById(R.id.top_view_title);
		title.setText(R.string.settings);
		logon = (TextView)findViewById(R.id.logon);
		back = (ImageView)findViewById(R.id.top_view_back);

		blackView = findViewById(R.id.unfollow_black_view);
		logonView = (LinearLayout) findViewById(R.id.signout_dialog_view);
		confirm = (TextView) findViewById(R.id.signout_confirm);
		cancel = (TextView) findViewById(R.id.signout_cancel);
        txtCurrency = (TextView) findViewById(R.id.currency);
        appVersion = (TextView) findViewById(R.id.setting_app_version);
        txtUpdate = (TextView) findViewById(R.id.txt_update_remind);
        icUpdate = (ImageView) findViewById(R.id.ic_update_app);
        ivAppNewMsg = (ImageView) findViewById(R.id.app_new_message);
        showAppUpdate();

		loginModel = new LoginModel(getApplicationContext());
		loginModel.addResponseListener(this);

		back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
		
		logon.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				blackView.setVisibility(View.VISIBLE);
				logonView.setVisibility(View.VISIBLE);
				AnimationUtil.showAnimation(logonView);
			}
		});
		
		blackView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                blackView.setVisibility(View.GONE);
                logonView.setVisibility(View.GONE);
                AnimationUtil.backAnimation(logonView);
            }
        });
		
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				blackView.setVisibility(View.GONE);
				logonView.setVisibility(View.GONE);
				AnimationUtil.backAnimation(logonView);
			}
		});
		
		confirm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				blackView.setVisibility(View.GONE);
				logonView.setVisibility(View.GONE);
				AnimationUtil.backAnimation(logonView);

                new SignOutAction(loginModel).signOut();

                finish();

                trackTouch(VIEW_NAME, "logout");
			}
		});

        PackageInfo pkg = Utils.getPackageInfo();
        if (pkg != null) {
            appVersion.setVisibility(View.VISIBLE);
            appVersion.setText(getString(R.string.app_version, getString(R.string.app_name),
                    pkg.versionName));
        }
        else {
            appVersion.setVisibility(View.GONE);
        }

        // default currency
        currencies = new Currencies(this);
        txtCurrency.setText(currencies.getDefaultCurrency());

        email = getIntent().getStringExtra(ACT_PARA_EMAIL);
        nickname = getIntent().getStringExtra(ACT_PARA_NICK_NAME);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventUtils.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventUtils.unregister(this);
    }

    @SuppressWarnings("unused")
    public void onEvent(Object event) {
        Message message = (Message) event;
        if (message.what == MessageConstant.APP_HAS_UPDATE) {
            showAppUpdate();
        }
    }

    @Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {
		// TODO Auto-generated method stub
		
	}

    private void showWebPage(String url){
        Intent intent = new Intent(SettingActivity.this, GenericWebActivity.class);
        intent.putExtra(ACT_PARA_PAGE_URL, url);
        startActivity(intent);
    }

    @SuppressWarnings("unused")
    public void onAbout(View v) {
        showWebPage(getString(R.string.url_about));
        trackTouch(VIEW_NAME, "aboutus");
    }

    @SuppressWarnings("unused")
    public void onSupport(View v) {
        showWebPage(getString(R.string.url_support, email, nickname));
        trackTouch(VIEW_NAME, "support");
    }

    @SuppressWarnings("unused")
    public void onTerms(View v) {
        showWebPage(getString(R.string.url_terms));
        trackTouch(VIEW_NAME, "termsofuse");
    }

    @SuppressWarnings("unused")
    public void onPrivacy(View v) {
        showWebPage(getString(R.string.url_privacy));
        trackTouch(VIEW_NAME, "policy");
    }

    @SuppressWarnings("unused")
    public void onComplain(View v) {
        Intent intent = new Intent(SettingActivity.this, ComplainActivity.class);
        startActivity(intent);
        trackTouch(VIEW_NAME, "complain");
    }

    @SuppressWarnings("unused")
    public void onSettingNotifications(View view) {
        Intent helpIntent = new Intent(SettingActivity.this, GenericWebActivity.class)
                .putExtra(ACT_PARA_PAGE_URL, getString(R.string.url_notification, SESSION.getInstance().token));
        startActivity(helpIntent);

        trackTouch(VIEW_NAME, "set_notifications");
    }

    @SuppressWarnings("unused")
    public void onSettingCurrency(View view) {
        startActivityForResult(new Intent(this, SelectCurrencyActivity.class), REQ_CURRENCY);
        trackTouch(VIEW_NAME, "set_currency");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_CURRENCY && resultCode == Activity.RESULT_OK) {
            onCurrencySelected(data);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void onCurrencySelected(Intent data) {
        String currency = data.getStringExtra(FiveMilesAppConst.ACT_RESULT_CURRENCY);
        if (ModelUtils.isNotEmpty(currency)) {
            currencies.setDefaultCurrency(currency);
            txtCurrency.setText(currency);
        }
    }

    public void onVersion(View view) {
        if (!CloudHelper.isAppHasUpdate()) {
            return;
        }

        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
        trackTouch(VIEW_NAME, "updateapp");
    }

    private void showAppUpdate() {
        boolean hasUpdate = CloudHelper.isAppHasUpdate();
        ivAppNewMsg.setVisibility(hasUpdate ? View.VISIBLE : View.GONE);
        icUpdate.setVisibility(hasUpdate ? View.VISIBLE : View.GONE);
        txtUpdate.setText(hasUpdate ? R.string.lbl_update_app : R.string.lbl_latest_app);
    }

    @Override
    public void onBackPressed(){
        if (logonView != null && View.VISIBLE == logonView.getVisibility()){
            logonView.setVisibility(View.GONE);
            blackView.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }
}
