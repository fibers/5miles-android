package com.insthub.fivemiles.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.external.androidquery.callback.AbstractAjaxCallback;
import com.external.androidquery.util.AQUtility;
import com.external.eventbus.EventBus;
import com.facebook.AppEventsLogger;
import com.insthub.fivemiles.Adapter.Bee_PageAdapter;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Protocol.MessageObject;
import com.insthub.fivemiles.SESSION;
import com.insthub.fivemiles.View.MessageView;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.EventUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;

import java.util.ArrayList;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_MSG_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_OFFER_LINE_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SHOULD_POP_KEYBOARD;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_MESSAGE;
import static com.insthub.fivemiles.MessageConstant.PUSH_ACT_SHOW_HOME;

public class TabMessageActivity extends Activity implements OnClickListener {
    public static final String TAB_BUY = "buy";
    public static final String TAB_SELL = "sell";
    public static final String TAB_SYS = "system";

    public static final String FLAG_BUY = "buying";
    public static final String FLAG_SELL = "selling";
    public static final String FLAG_SYS = "system";

    private static boolean onTop;
    private static String currView;
    private CompoundButton buying, selling, system;
    private TextView txtTitle;
	private TextView buying_text;
	private TextView selling_text;
	private TextView system_text;
	private ImageView buying_unread;
	private ImageView selling_unread;
	private ImageView system_unread;

	private ViewPager viewPager;
	
	private ArrayList<View> viewList = new ArrayList<View>();
	private ArrayList<MessageView> childView = new ArrayList<MessageView>();


    private AppEventsLogger fbEventsLogger;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        fbEventsLogger = AppEventsLogger.newLogger(this);

		setContentView(R.layout.tab_message);

        txtTitle = (TextView) findViewById(R.id.top_view_title);
		buying = (CompoundButton) findViewById(R.id.rdb_msg_buy);
		selling = (CompoundButton) findViewById(R.id.rdb_msg_sell);
		system = (CompoundButton) findViewById(R.id.rdb_msg_sys);
		buying_text = (TextView) findViewById(R.id.txt_msg_buy);
		selling_text = (TextView) findViewById(R.id.txt_msg_sell);
		system_text = (TextView) findViewById(R.id.txt_msg_sys);
		buying_unread = (ImageView) findViewById(R.id.message_buying_unread);
		selling_unread = (ImageView) findViewById(R.id.message_selling_unread);
		system_unread = (ImageView) findViewById(R.id.message_system_unread);
//		line = findViewById(R.id.message_line);
		viewPager = (ViewPager) findViewById(R.id.message_viewPager);
		
//		android.view.ViewGroup.LayoutParams cursorParams = (android.view.ViewGroup.LayoutParams) line.getLayoutParams();
//		cursorParams.width = tabWidth;
//		line.setLayoutParams(cursorParams);

        // 设置顶部占位栏标题为Message
        findViewById(R.id.top_view_back).setVisibility(View.GONE);
        findViewById(R.id.top_view_logo).setVisibility(View.GONE);
        txtTitle.setVisibility(View.VISIBLE);
        txtTitle.setText(R.string.title_tab_msg);
		
		buying.setOnClickListener(this);
		selling.setOnClickListener(this);
		system.setOnClickListener(this);
		
		viewList.clear();
		childView.clear();
		for(int i=0;i<3;i++) {
			MessageView child = (MessageView) getLayoutInflater().inflate(R.layout.message_view,null);
			viewList.add(child);
			childView.add(child);
		}
		
		viewPager.setAdapter(new Bee_PageAdapter(viewList));
		viewPager.setCurrentItem(0);
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				if(arg0 == 1) {
					selectSelling(true);
				} else if(arg0 == 2) {
					selectSystem(true);
				} else {
					selectBuying(true);
				}
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            EventUtils.post(PUSH_ACT_SHOW_HOME, 0);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void bindData() {
        int index = viewPager.getCurrentItem();
        MessageView currView = childView.get(index);
        switch (index) {
            case 1:
                currView.bindData(FLAG_SELL);
                break;
            case 2:
                currView.bindData(FLAG_SYS);
                break;
            default:
                currView.bindData(FLAG_BUY);
                break;
        }
        currView.registerEvent();
        registerForContextMenu(currView.getListView());

        TrackingUtils.trackTouch(VIEW_MESSAGE, "tab_buy");
    }

    @Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.rdb_msg_buy:
            selectBuying(false);
            TrackingUtils.trackTouch(VIEW_MESSAGE, "tab_buy");
			break;
		case R.id.rdb_msg_sell:
			selectSelling(false);
            TrackingUtils.trackTouch(VIEW_MESSAGE, "tab_sell");
            break;
		case R.id.rdb_msg_sys:
			selectSystem(false);
            TrackingUtils.trackTouch(VIEW_MESSAGE, "tab_system");
			break;
		}
	}
	
	private void selectBuying(boolean postEvent) {
        currView = FLAG_BUY;
        if (0 == viewPager.getCurrentItem() && !postEvent) {
            return;
        }

        buying.setChecked(true);
		buying_text.setTextColor(Color.WHITE);
		selling_text.setTextColor(getResources().getColor(R.color.fm_text_dark_gray));
		system_text.setTextColor(getResources().getColor(R.color.fm_text_dark_gray));
		viewPager.setCurrentItem(0);
		childView.get(0).bindData(FLAG_BUY);
		childView.get(0).registerEvent();
        registerForContextMenu(childView.get(0).getListView());
		childView.get(1).unRegisterEvent();
		childView.get(2).unRegisterEvent();
	}
	
	private void selectSelling(boolean postEvent) {
        currView = FLAG_SELL;
        if (1 == viewPager.getCurrentItem() && !postEvent) {
            return;
        }

		selling.setChecked(true);
		buying_text.setTextColor(getResources().getColor(R.color.fm_text_dark_gray));
		selling_text.setTextColor(Color.WHITE);
		system_text.setTextColor(getResources().getColor(R.color.fm_text_dark_gray));
		viewPager.setCurrentItem(1);
		childView.get(1).bindData(FLAG_SELL);
		childView.get(1).registerEvent();
        registerForContextMenu(childView.get(1).getListView());
		childView.get(0).unRegisterEvent();
		childView.get(2).unRegisterEvent();
	}
	
	private void selectSystem(boolean postEvent) {
        currView = FLAG_SYS;
        if (2 == viewPager.getCurrentItem() && !postEvent) {
            return;
        }

		system.setChecked(true);
		buying_text.setTextColor(getResources().getColor(R.color.fm_text_dark_gray));
		selling_text.setTextColor(getResources().getColor(R.color.fm_text_dark_gray));
		system_text.setTextColor(Color.WHITE);
		viewPager.setCurrentItem(2);
		childView.get(2).bindData(FLAG_SYS);
		childView.get(2).registerEvent();
        registerForContextMenu(childView.get(2).getListView());
		childView.get(1).unRegisterEvent();
		childView.get(0).unRegisterEvent();
	}

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.msg_ctx_menu, menu);
    }

    private String getCurrentTabName() {
        int currentTabIndex = viewPager.getCurrentItem();
        AQUtility.debug("current selected index: " + currentTabIndex);
        if (currentTabIndex == 0)
            return TAB_BUY;
        else if (currentTabIndex == 1)
            return TAB_SELL;
        else if (currentTabIndex == 2)
            return TAB_SYS;
        else
            return TAB_BUY;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        boolean handled;
        int itemId = item.getItemId();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)
                item.getMenuInfo();

        String touchAction;

        String currentTabName = getCurrentTabName();
        if (itemId == R.id.msg_ctx_menu_del) {
            int page = viewPager.getCurrentItem();
            childView.get(page).deleteMessage(info.position);
            handled = true;
            touchAction = currentTabName + "_delete";
        }
        else {
            handled = super.onContextItemSelected(item);
            touchAction = "message_" + currentTabName;
        }
        TrackingUtils.trackTouch(VIEW_MESSAGE, touchAction);
        return handled;
    }

    @Override
    protected void onResume() {
        super.onResume();

        fbEventsLogger.logEvent(VIEW_MESSAGE);

        // TabProfileActivity继承自Activity，无法使用isLoginProtected
        boolean isNotAuthed = !SESSION.getInstance().isAuth();
        if (isNotAuthed) {
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
            return;
        }

        bindData();  // refresh data
        onTop = true;

        // 作为首页的一个tab，只有一次create、start，pv需在resume里记录
        TrackingUtils.trackView(VIEW_MESSAGE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        for (MessageView mv : childView) {
            mv.unRegisterEvent();
        }
        onTop = false;
        AbstractAjaxCallback.cancel();  // cancel all running & pending (BeeFramework) ajax jobs
    }

    @Override
	protected void onStart() {
		super.onStart();
        registerEventListener();
    }

    private void registerEventListener() {
        if (!EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
	protected void onStop() {
		super.onStop();
		if (EventBus.getDefault().isregister(this)) {
			EventBus.getDefault().unregister(this);
		}
	}
	
	public void onEvent(Object event) {
		Message message = (Message) event;

        switch (message.what) {
            case MessageConstant.UNREAD_BUYING:
                boolean isUnread = (Boolean) message.obj;
                buying_unread.setVisibility(isUnread ? View.VISIBLE : View.GONE);
                break;
            case MessageConstant.UNREAD_SELLING:
                isUnread = (Boolean) message.obj;
                selling_unread.setVisibility(isUnread ? View.VISIBLE : View.GONE);
                break;
            case MessageConstant.UNREAD_SYSTEM:
                isUnread = (Boolean) message.obj;
                system_unread.setVisibility(isUnread ? View.VISIBLE : View.GONE);
                break;
            case MessageConstant.MSG_BUY_CLK:
                openOfferLineDetail((MessageObject) message.obj);
                TrackingUtils.trackTouch(VIEW_MESSAGE, "message_buy");
                break;
            case MessageConstant.MSG_SELL_CLK:
                openOfferLineDetail((MessageObject) message.obj);
                TrackingUtils.trackTouch(VIEW_MESSAGE, "message_sell");
                break;
            case MessageConstant.PUSH_ACT_NEW_OFFER:
                // has new message, update current page
                int tab = viewPager.getCurrentItem();
                if(tab == 1) {
                    selectSelling(true);
                }
                else if (tab == 0) {
                    selectBuying(true);
                }
                break;
            case MessageConstant.PUSH_ACT_NEW_SYS_MSG:
                boolean alreadyInSys = viewPager.getCurrentItem() == 2;
                if (onTop && alreadyInSys) {
                    selectSystem(true);
                }
                break;
            case MessageConstant.PUSH_ACT_REFRESH_MSG:
                alreadyInSys = viewPager.getCurrentItem() == 2;
                selectSystem(alreadyInSys);
                break;
            case MessageConstant.OPEN_SYS_MSG:
                // 目前仅跳转到system
                selectSystem(false);
                break;
        }
	}

    private void openOfferLineDetail(MessageObject msg) {
        Intent intent = new Intent(this, MakeOfferActivity.class);
        intent.putExtra(ACT_PARA_MSG_ID, msg.id);
        intent.putExtra(ACT_PARA_OFFER_LINE_ID, msg.offerLineId);
        intent.putExtra(ACT_PARA_SHOULD_POP_KEYBOARD, false);
        startActivity(intent);
    }

    public static boolean isSysViewOnTop() {
        return onTop && FLAG_SYS.equals(currView);
    }

    public static boolean isBuyingViewOnTop() {
        return onTop && FLAG_BUY.equals(currView);
    }

    public static boolean isSellingViewOnTop() {
        return onTop && FLAG_SELL.equals(currView);
    }
}
