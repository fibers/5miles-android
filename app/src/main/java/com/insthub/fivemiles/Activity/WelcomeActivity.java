package com.insthub.fivemiles.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

import com.BeeFramework.activity.BaseActivity;
import com.external.eventbus.EventBus;
import com.insthub.fivemiles.Adapter.GuidePagerAdapter;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.SESSION;
import com.insthub.fivemiles.Utils.LocationManagerUtil;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.login.FacebookLoginFragment;
import com.thirdrock.fivemiles.login.QuickTourActivity;
import com.thirdrock.fivemiles.login.ZipcodeVerifyActivity;
import com.thirdrock.fivemiles.reco.SellersNearbyActivity;
import com.thirdrock.fivemiles.util.TrackingUtils;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REDIRECT_INTENT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SHOW_FB_LIKES;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_GLIDE_2;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_GLIDE_3;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_GLIDE_4;
import static com.thirdrock.fivemiles.util.TrackingUtils.trackTouch;
import static com.thirdrock.fivemiles.util.TrackingUtils.trackView;

public class WelcomeActivity extends BaseActivity implements OnTouchListener, OnGestureListener {
    private ViewPager viewPager;
    private GuidePagerAdapter guidePagerAdapter;
    private int pageNumber;
    private ImageView pointview01, pointview02, pointview03, pointview04;
    private GestureDetector mygesture = new GestureDetector(this);
    private View lnkTour, pnlAuth;
    private Intent redirectIntent;  // redirection target (after login)sign

    public static final String VIEW_NAME = "welcome_view";

    private static boolean alive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);

        boolean isAuth = SESSION.getInstance().isAuth();
        if (isAuth) {
            startActivity(new Intent(this, MainTabActivity.class));
            finish();
            return;
        }
        SESSION.getInstance().setWelcomeShown(true);

        redirectIntent = getIntent().getParcelableExtra(ACT_PARA_REDIRECT_INTENT);

        pointview01 = (ImageView) findViewById(R.id.point_1);
        pointview02 = (ImageView) findViewById(R.id.point_2);
        pointview03 = (ImageView) findViewById(R.id.point_3);
        pointview04 = (ImageView) findViewById(R.id.point_4);
        lnkTour = findViewById(R.id.guide_tour_link);
        pnlAuth = findViewById(R.id.signin_ctrl_wrapper);

        viewPager = (ViewPager) findViewById(R.id.image_pager);
        guidePagerAdapter = new GuidePagerAdapter(this);
        viewPager.setAdapter(guidePagerAdapter);
        viewPager.setOnPageChangeListener(new OnPageChangeListener() {

            public void onPageSelected(int position) {
                pageNumber = position;
                lnkTour.setVisibility(View.GONE);

                if (pageNumber == 0) {
                    pointview01.setBackgroundResource(R.drawable.ic_pager_indicator_active);
                    pointview02.setBackgroundResource(R.drawable.ic_pager_indicator);
                    pointview03.setBackgroundResource(R.drawable.ic_pager_indicator);
                    pointview04.setBackgroundResource(R.drawable.ic_pager_indicator);
                } else if (pageNumber == 1) {
                    pointview01.setBackgroundResource(R.drawable.ic_pager_indicator);
                    pointview02.setBackgroundResource(R.drawable.ic_pager_indicator_active);
                    pointview03.setBackgroundResource(R.drawable.ic_pager_indicator);
                    pointview04.setBackgroundResource(R.drawable.ic_pager_indicator);
                    TrackingUtils.trackView(VIEW_GLIDE_2);
                    TrackingUtils.trackTouch(VIEW_NAME, "welcome_view2");
                } else if (pageNumber == 2) {
                    pointview01.setBackgroundResource(R.drawable.ic_pager_indicator);
                    pointview02.setBackgroundResource(R.drawable.ic_pager_indicator);
                    pointview03.setBackgroundResource(R.drawable.ic_pager_indicator_active);
                    pointview04.setBackgroundResource(R.drawable.ic_pager_indicator);
                    TrackingUtils.trackView(VIEW_GLIDE_3);
                } else {
                    pointview01.setBackgroundResource(R.drawable.ic_pager_indicator);
                    pointview02.setBackgroundResource(R.drawable.ic_pager_indicator);
                    pointview03.setBackgroundResource(R.drawable.ic_pager_indicator);
                    pointview04.setBackgroundResource(R.drawable.ic_pager_indicator_active);
//                    lnkTour.setVisibility(View.VISIBLE);
                    TrackingUtils.trackView(VIEW_GLIDE_4);
                    trackTouch(VIEW_GLIDE_4, "slidetohome");
                }
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });
        viewPager.setOnTouchListener(this);

        // Facebook login
        FacebookLoginFragment fbFrm = new FacebookLoginFragment();
        fbFrm.setLoginListener(new FacebookLoginFragment.LoginListener() {
            @Override
            public void onLoginSuccess() {
                LocationManagerUtil locationMgr = LocationManagerUtil.getInstance();
                if(locationMgr.isEnabled()) {
                    startActivity(new Intent(WelcomeActivity.this, SellersNearbyActivity.class)
                            .putExtra("from", VIEW_NAME)
                            .putExtra(ACT_PARA_REDIRECT_INTENT, redirectIntent));
                } else {
                    //无法获取当前地理位置信息.则跳转到ZipcodeVersify 页面里.
                    startActivity(new Intent(WelcomeActivity.this, ZipcodeVerifyActivity.class)
                            .putExtra("from", VIEW_NAME)
                            .putExtra(ACT_PARA_REDIRECT_INTENT, redirectIntent));
                }
                finish();
                TrackingUtils.trackTouch(VIEW_NAME, "facebook");
                Message msg = new Message();
                msg.what = MessageConstant.SIGN_UP_SUCCESS;
                EventBus.getDefault().post(msg);
            }
        });
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fb_login_placeholder, fbFrm);
        ft.commit();

        trackView(VIEW_NAME);
    }

    public static boolean isAlive() {
        return alive;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().register(this);
        }
        alive = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().unregister(this);
        }
        alive = false;
    }

    @SuppressWarnings("unused")
    public void onEvent(Object event) {
        Message msg = (Message) event;
        if (msg.what == MessageConstant.SIGN_UP_SUCCESS) {
            finish();
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        return mygesture.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                           float velocityY) {
//		if ((e1.getX() - e2.getX() > 100) && (pageNumber == 3) ){
//			quickTour(null);
//		}
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                            float distanceY) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public void redirectToLogin(View v) {
        startActivity(new Intent(this, EmailLoginActivity.class)
                .putExtra(ACT_PARA_REDIRECT_INTENT, redirectIntent));
        TrackingUtils.trackTouch(VIEW_NAME, "signin");
    }

    public void redirectToRegister(View v) {
        startActivity(new Intent(this, RegisterActivity.class)
                .putExtra(ACT_PARA_REDIRECT_INTENT, redirectIntent));
        TrackingUtils.trackTouch(VIEW_NAME, "signup");
    }

    @SuppressWarnings("unused")
    public void quickTour(View view) {
        startActivity(new Intent(this, QuickTourActivity.class)
                .putExtra(ACT_PARA_REDIRECT_INTENT, redirectIntent));
        TrackingUtils.trackTouch(VIEW_NAME, "checkitout");
    }

    // add redirection target before start next activity
    @Override
    public void startActivity(Intent intent) {
        Intent prevIntent = getIntent();
        intent.putExtra(ACT_PARA_REDIRECT_INTENT, prevIntent.getParcelableExtra(ACT_PARA_REDIRECT_INTENT));
        intent.putExtra(ACT_PARA_SHOW_FB_LIKES, prevIntent.getBooleanExtra(ACT_PARA_SHOW_FB_LIKES, false));
        super.startActivity(intent);
    }
//    // 未被调用，可能是因为WelcomeActiviy被回收？这段逻辑和Context无关，放到Model里面更容易管理
//    @Override
//    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
//            throws JSONException {
//        if (url.startsWith(ApiInterface.SIGNUP_ANONYMOUS) && jo != null) {
//            try {
//                AnonySignUpResp anonyResp = AnonySignUpResp__JsonHelper.parseFromJson(jo.toString());
//                if(anonyResp != null) {
//                    SESSION session = SESSION.getInstance();
//                    session.user_id = anonyResp.getUserId();
//                    session.anonyUserId = anonyResp.getUserId();
//                    session.token   = anonyResp.getToken();
//                    session.email   = anonyResp.getEmail();
//                    session.createdAt = anonyResp.getCreatedAt();
//                    session.lastLogin = anonyResp.getLastLoginAt();
//                    session.setFbVerified(false);
//                    session.save();
//                }
//            } catch (IOException e) {
//                L.e("error parse anonymous user info");
//            }
//        }
//    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (viewPager.getCurrentItem() > 0) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected boolean isLoginProtected() {
        return false;
    }
}