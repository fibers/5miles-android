package com.insthub.fivemiles.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.external.maxwin.view.IXListViewListener;
import com.external.maxwin.view.XListView;
import com.insthub.fivemiles.Adapter.WatchingAdapter;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.Model.WatchingsModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.HOME_OBJECTS;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class BuyingActivity extends BaseActivity implements IXListViewListener, BusinessResponse {

    static final String TAG = "UI";

    private TextView 		title;
	private XListView 		listView;
	private WatchingsModel 	watchingsModel;
	private WatchingAdapter     buyingAdapter;

    private boolean isEmpty;  // in empty page mode

    public static final String VIEW_NAME = "mybuying_view";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.common_list_view);

        ImageView back = (ImageView) findViewById(R.id.top_view_back);
		title 		= (TextView) 	findViewById(R.id.top_view_title);
		listView 	= (XListView) 	findViewById(R.id.list);
		
		back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                TrackingUtils.trackTouch(VIEW_NAME, "mybuying_back");
            }
        });
		
		title.setText(R.string.profile_buying);
		
		listView.setXListViewListener(this, 1);
		listView.setPullLoadEnable(false);
		listView.setRefreshTime();

        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                try {
                    Intent intent = new Intent(BuyingActivity.this, ItemActivity.class);
                    HOME_OBJECTS item = watchingsModel.buyingList.get(position - 1);
                    intent.putExtra(FiveMilesAppConst.ACT_PARA_ITEM_ID, item.id);
                    startActivity(intent);
                    TrackingUtils.trackTouch(VIEW_NAME, "mybuying_product");
                }
                catch (Exception e) {
                    Log.e(TAG, "Error while starting order detail activity", e);
                }
            }
        });
		
		watchingsModel = new WatchingsModel(this);
		watchingsModel.addResponseListener(this);

        buyingAdapter = new WatchingAdapter(this, watchingsModel.buyingList);
        listView.setAdapter(buyingAdapter);

        initContent();
        TrackingUtils.trackView(VIEW_NAME);
	}

    private void initContent() {
        ViewGroup emptyView = (ViewGroup) findViewById(R.id.empty_view_placeholder);
        int count = getIntent().getIntExtra(FiveMilesAppConst.ACT_PARA_BUYING_COUNT, 0);
        isEmpty = count == 0;

        if (!isEmpty) {
            listView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            listView.refreshNow();
            return;
        }

        listView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        showMyEmptyView(emptyView);
    }

    private void showMyEmptyView(ViewGroup parent) {
        View v = getLayoutInflater().inflate(R.layout.my_empty_view, parent);
        TextView txtDesc = (TextView) v.findViewById(R.id.empty_view_desc);
        TextView txtAction = (TextView) v.findViewById(R.id.empty_view_button);

        txtDesc.setText(R.string.hint_empty_buying_list_desc);
        txtAction.setText(R.string.empty_buying_list_action);

        txtAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuyingActivity.this, MainTabActivity.class);
                intent.setAction(FiveMilesAppConst.ACT_GOTO_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

                TrackingUtils.trackTouch(VIEW_NAME, "buying_empty");
            }
        });
    }

	@Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {
		listView.stopRefresh();
		listView.stopLoadMore();
		if(url.startsWith(ApiInterface.MY_BUYINGS)) {
			setDataAdapter();
		}
	}
	
	private void setDataAdapter() {
		if(buyingAdapter == null) {
			buyingAdapter = new WatchingAdapter(this, watchingsModel.buyingList);
			listView.setAdapter(buyingAdapter);
		} else {
			buyingAdapter.list = watchingsModel.buyingList;
			buyingAdapter.notifyDataSetChanged();
		}
        if(ModelUtils.isNull(watchingsModel.meta.next)) {
            listView.setPullLoadEnable(false);
        } else {
            listView.setPullLoadEnable(true);
        }
	}

	@Override
	public void onRefresh(int id) {
        watchingsModel.getBuyingList();
	}

	@Override
	public void onLoadMore(int id) {
        watchingsModel.getBuyingListMore();
        TrackingUtils.trackTouch(VIEW_NAME, "mybuying_loadmore");
	}
}
