package com.insthub.fivemiles.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.external.eventbus.EventBus;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.LoginModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.reco.SellersNearbyActivity;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.ui.widget.AvatarView;

import org.json.JSONException;
import org.json.JSONObject;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REDIRECT_INTENT;

public class RegisterUserActivity extends BaseActivity implements OnClickListener, BusinessResponse {

	private ImageView 		back;
	private TextView 		title, skip;
	private AvatarView avatar;

	private LoginModel 		loginModel;
    private TakePhotoAction takePhotoAction;

    public static final String VIEW_NAME = "headphoto_view";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_user);
		back 		= (ImageView) 		findViewById(R.id.top_view_back);
		title 		= (TextView) 		findViewById(R.id.top_view_title);
		skip 		= (TextView) 		findViewById(R.id.top_view_action_txt);
        avatar 		= (AvatarView) 		findViewById(R.id.register_avatar);

        back.setVisibility(View.GONE);
        back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

        title.setText(getString(R.string.add_your_photo));
        title.setVisibility(View.VISIBLE);

        skip.setText(getString(R.string.txt_skip_fill_profile));
        skip.setVisibility(View.VISIBLE);
        skip.setOnClickListener(this);

		loginModel = new LoginModel(this);
		loginModel.addResponseListener(this);
        takePhotoAction = new TakePhotoAction(this, loginModel, avatar);

        TrackingUtils.trackView(VIEW_NAME);
	}

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // nothing to do
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
	public void onClick(View v) {
		switch(v.getId()) {
        case R.id.top_view_action_txt:      // skip
            TrackingUtils.trackTouch(VIEW_NAME, "photo_skip");
            redirectToRecoPage();
            break;
		}
	}

    @Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {
		if(url.startsWith(ApiInterface.USER_EDIT_PROFILE)) {
            if (jo == null) {
                return;
            }

			Message msg = new Message();
            msg.what = MessageConstant.SIGN_UP_SUCCESS;
            EventBus.getDefault().post(msg);

            redirectToRecoPage();
		}
	}

    private void redirectToRecoPage() {
        startActivity(new Intent(this, SellersNearbyActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .putExtra("from", VIEW_NAME));
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            takePhotoAction.processActivityResult(requestCode, data);
        }
    }

    public void choosePhoto(View view) {
        takePhotoAction.choosePhoto();
        TrackingUtils.trackTouch(VIEW_NAME, "headphoto_choose");
    }

    public void takePhoto(View view) {
        takePhotoAction.takePhoto();
        TrackingUtils.trackTouch(VIEW_NAME, "headphoto_take");
    }

    // add redirection target before start next activity
    @Override
    public void startActivity(Intent intent) {
        intent.putExtra(ACT_PARA_REDIRECT_INTENT, getIntent().getParcelableExtra(ACT_PARA_REDIRECT_INTENT));
        super.startActivity(intent);
    }

    @Override
    protected boolean isLoginProtected() {
        return false;
    }
}
