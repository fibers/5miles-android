package com.insthub.fivemiles.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import com.BeeFramework.Utils.ImageUtil;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.Model.LoginModel;
import com.soundcloud.android.crop.Crop;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.framework.ui.widget.AvatarView;
import com.thirdrock.framework.util.L;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 拍照
 * Created by lishangqiang on 2014/9/10.
 */
public class TakePhotoAction {

    public static final int REQUEST_CAMERA=1;
    public static final int REQUEST_PHOTO=2;
    public static final int MAX_AVATAR_WIDTH = 800;

    private Activity        activity;
    private LoginModel      loginModel;
    private AvatarView avatar;

    private File            fileDir;
    private File        fileDir2;
    private String      imagePath;
    private File        avatarFile;

    public TakePhotoAction(Activity activity, LoginModel loginModel, AvatarView avatar) {
        this.activity = activity;
        this.loginModel = loginModel;
        this.avatar = avatar;
    }

    private String getTempPhotoFilePath(String fileName) {
        File dir = new File(FiveMilesAppConst.FILEPATH + "img");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return FiveMilesAppConst.FILEPATH + "img/" + fileName;
    }

    public String startPhotoZoom(Uri uri) {
        if (fileDir2 == null) {
            fileDir2 = new File(FiveMilesAppConst.FILEPATH + "img/");
            if (!fileDir2.exists()) {
                fileDir2.mkdirs();
            }
        }

        String fileName;
        fileName = "/temp.jpg";

        String filePath = fileDir2 + fileName;
        File loadingFile = new File(filePath);

        new Crop(uri).
                output(Uri.fromFile(loadingFile)).
                asSquare().
                withMaxSize(MAX_AVATAR_WIDTH, MAX_AVATAR_WIDTH).
                withAspect(1, 1).
                start(activity);

        imagePath = filePath;
        avatarFile = new File(imagePath);

        return filePath;
    }

    public void submitProfile() {
        loginModel.fillProfileAvatar(activity, avatarFile);
    }

    public void choosePhoto() {
        Intent picture = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        activity.startActivityForResult(picture, REQUEST_PHOTO);
    }

    public void takePhoto() {
        if (fileDir == null) {
            fileDir = new File(FiveMilesAppConst.FILEPATH + "img/");
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }
        }
        imagePath = FiveMilesAppConst.FILEPATH + "img/" + "temp.jpg";
        avatarFile = new File(imagePath);
        Uri imageuri = Uri.fromFile(avatarFile);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, imageuri);
        intent.putExtra("return-data", false);
        activity.startActivityForResult(intent, REQUEST_CAMERA);
    }

    public void fillAvatar() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        Bitmap bitmap = BitmapFactory.decodeFile(avatarFile.getAbsolutePath(), options);
        avatar.setImageBitmap(bitmap);
    }

    /**
     * 处理公共的事件
     * @return true：在此处理了事件；false:在此没有处理事件
     */
    public boolean processActivityResult(int requestCode, Intent data) {
        if (requestCode == REQUEST_CAMERA) {
            if (avatarFile != null && avatarFile.exists()) {
                File tmpAvatar = postProcessPhoto(Uri.fromFile(avatarFile));
                if (tmpAvatar != null) {
                    startPhotoZoom(Uri.fromFile(tmpAvatar));
                }
            }
            else {
                DisplayUtils.toast(R.string.msg_photo_not_exists);
            }
            return true;
        } else if (requestCode == REQUEST_PHOTO) {
            Uri selectedImage = data.getData();
            File tmpAvatar = postProcessPhoto(selectedImage);
            if (tmpAvatar != null) {
                startPhotoZoom(Uri.fromFile(tmpAvatar));
            }
            return true;
        } else if (requestCode == Crop.REQUEST_CROP) {
            if (avatarFile != null && avatarFile.exists()) {
                fillAvatar();
                submitProfile();
            } else {
                DisplayUtils.toast(R.string.msg_photo_not_exists);
            }
            return true;
        }
        return false;
    }

    private File postProcessPhoto(Uri pic) {
        InputStream picIs = null;
        OutputStream picOs = null;
        File tmpAvatar = null;

        try {
            picIs = FiveMilesApp.getInstance().getContentResolver().openInputStream(pic);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeStream(picIs, null, options);

            // detect photo orientation & fix it. http://bit.ly/1uXf1Bz
            int angle = ImageUtil.getImageOrientation(pic);
            options.inJustDecodeBounds = false;
            if (options.outWidth > MAX_AVATAR_WIDTH) {
                options.inSampleSize = options.outWidth / MAX_AVATAR_WIDTH;
            }

            // show the avatar
            picIs = FiveMilesApp.getInstance().getContentResolver().openInputStream(pic);
            Bitmap bmp = BitmapFactory.decodeStream(picIs, null, options);
            Bitmap bitmap = ImageUtil.rotateBitmap(bmp, angle);

            tmpAvatar = new File(getTempPhotoFilePath("tmp_avatar.png"));
            picOs = new FileOutputStream(tmpAvatar);
            bitmap.compress(Bitmap.CompressFormat.PNG, 85, picOs);
        }
        catch (Exception e) {
            L.e(e);
        }
        finally {
            try {
                if (picIs != null) {
                    picIs.close();
                }

                if (picOs != null) {
                    picOs.close();
                }
            } catch (IOException e) {
                L.e(e);
            }
        }

        return tmpAvatar;
    }

}
