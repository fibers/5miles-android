package com.insthub.fivemiles.Activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.BeeFramework.activity.BaseActivity;
import com.external.viewpagerindicator.CirclePageIndicator;
import com.insthub.fivemiles.Adapter.LargeImageAdapter;
import com.insthub.fivemiles.Protocol.IMAGES;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.TrackingUtils;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LargeImageActivity extends BaseActivity {
	public static final String VIEM_NAME = "productlookphoto_view";

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.large_image);
        ButterKnife.inject(this);

        findViewById(R.id.top_view_logo).setVisibility(View.VISIBLE);

		List<IMAGES> list = (List<IMAGES>) getIntent().getSerializableExtra("images");
		int position = getIntent().getIntExtra("position", 0);
		ViewPager viewPager = (ViewPager) findViewById(R.id.image_pager);
		CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.image_pager_indicator);

		LargeImageAdapter largeImageAdapter = new LargeImageAdapter(this, list);
		viewPager.setAdapter(largeImageAdapter);
		viewPager.setCurrentItem(position);
		indicator.setViewPager(viewPager, position);
		indicator.requestLayout();
	}

    @OnClick(R.id.top_view_back)
    void onBack() {
        this.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        TrackingUtils.trackTouch(VIEM_NAME, "productphoto_close");
    }
}
