package com.insthub.fivemiles.Activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.external.androidquery.callback.AbstractAjaxCallback;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.Protocol.CommodityProduct_detailResponse;
import com.thirdrock.domain.Item;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.util.L;

import static com.google.maps.android.SphericalUtil.computeDistanceBetween;
import static com.insthub.fivemiles.FiveMilesAppConst.EPSILON;
import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLatitudeInSession;
import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLongitudeInSession;
import static com.thirdrock.fivemiles.util.LocationUtils.calcDistanceOnScreen;
import static com.thirdrock.fivemiles.util.LocationUtils.calcLatLngBounds;
import static com.thirdrock.fivemiles.util.LocationUtils.findCoordinate;

public class ItemLocationMapActivity extends FragmentActivity implements OnInfoWindowClickListener, OnMapReadyCallback {

    public static final int DEFAULT_RANGE_RADIUS = 500;  // in meters
    public static final int MAP_PADDING_RADIUS = 800;  // in meters
    public static final String VIEW_NAME = "map_view";
    private ImageView back;
    private TextView title;
    private View location;
    private GoogleMap mMap;
    private Item homeObject;
    private CommodityProduct_detailResponse orderDetail;

    private ViewGroup mapContainer;

    private double latitude, longitude;
    private LatLng itemLocation;
    private String titleString;
    private boolean autoFixing;

    private Marker itemMarker, userMarker;
    private Circle itemCircle;
    private int minItemMapRadiusPx;  // min radius of the item location range, in px

    private float previousZoomLevel = -1;

    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        setContentView(R.layout.order_location);
        homeObject = (Item) getIntent().getSerializableExtra(FiveMilesAppConst.ACT_PARA_ITEM);
        latitude = homeObject.getLocation().getLatitude();
        longitude = homeObject.getLocation().getLongitude();
        itemLocation = new LatLng(latitude, longitude);
        titleString = homeObject.getTitle();

        minItemMapRadiusPx = getResources().getDimensionPixelSize(R.dimen.item_big_map_circle_radius);

        back = (ImageView) findViewById(R.id.top_view_back);
        title = (TextView) findViewById(R.id.top_view_title);
        location = findViewById(R.id.map_location);

        mapContainer = (ViewGroup) findViewById(R.id.map_container);
        mapContainer.requestTransparentRegion(mapContainer);
        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                ItemLocationMapActivity.this.onBackPressed();
            }
        });

        title.setText(R.string.map);

        initMapView();
        location.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                fixLocation();
                TrackingUtils.trackTouch(VIEW_NAME, "showmylocation");
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TrackingUtils.trackTouch(VIEW_NAME, "productmapback");
    }

    private void initMapView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {
                    SupportMapFragment mapFragment = SupportMapFragment.newInstance(
                            new GoogleMapOptions()
                                    .mapType(GoogleMap.MAP_TYPE_NORMAL)
                                    .mapToolbarEnabled(false)
                                    .compassEnabled(false)
                                    .rotateGesturesEnabled(false)
                                    .scrollGesturesEnabled(true)
                                    .tiltGesturesEnabled(false)
                                    .zoomControlsEnabled(false)
                                    .zoomGesturesEnabled(true)
                    );
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.map, mapFragment)
                            .commitAllowingStateLoss();         // 异步里面commit() transaction会导致IllegalStateException

                    mapFragment.getMapAsync(ItemLocationMapActivity.this);
                }
            }
        }, 100);
    }

    protected void fixLocation() {
        if (mMap == null) {
            return;
        }

        autoFixing = true;

        // current user location
        if (userMarker != null) {
            userMarker.remove();
        }

        LatLng userLatLng = new LatLng(getLatitudeInSession(), getLongitudeInSession());
        userMarker = mMap.addMarker(new MarkerOptions()
                .position(userLatLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_location))
                .draggable(false));

        LatLngBounds bounds = calcLatLngBounds(itemLocation, userLatLng, MAP_PADDING_RADIUS);
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,
                getResources().getDimensionPixelSize(R.dimen.item_big_map_padding)));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        AbstractAjaxCallback.cancel();  // cancel all running & pending (BeeFramework) ajax jobs
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {
                gaTrackZoomEvent(position);

                trackMapScale();

                if (!autoFixing) {
                    return;
                }

                // make sure that the item is visible
                LatLng itemLatLng = new LatLng(latitude, longitude);
                VisibleRegion region = mMap.getProjection().getVisibleRegion();
                if (isMinZoom(position.zoom) && !region.latLngBounds.contains(itemLatLng)) {
                    L.d("item is invisible at minimum zoom level, move camera to item");
                    mMap.stopAnimation();
                    autoFixItem(itemLatLng);
                } else {
                    L.d("visible or not-at-min-zoom");
                }
            }
        });

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                L.d("map loaded, show item markers");
                showItem();
            }
        });
    }

    private void gaTrackZoomEvent(CameraPosition position) {
        if (position == null) {
            return;
        }

        if (previousZoomLevel > 0) {
            if (position.zoom - previousZoomLevel > EPSILON) {
                TrackingUtils.trackTouch(VIEW_NAME, "productmapzoomout");
            } else if (position.zoom - previousZoomLevel < EPSILON) {
                TrackingUtils.trackTouch(VIEW_NAME, "productmapzoomin");
            }

        }

        previousZoomLevel = position.zoom;
    }

    protected void autoFixItem(LatLng itemLatLng) {
        mMap.animateCamera(
                CameraUpdateFactory.newLatLngBounds(
                        calcLatLngBounds(itemLatLng, MAP_PADDING_RADIUS),
                        getResources().getDimensionPixelSize(R.dimen.item_big_map_padding)),
                new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {
                        autoFixing = false;
                    }

                    @Override
                    public void onCancel() {
                        autoFixing = true;
                    }
                });
    }

    protected void showItem() {
        if (mMap == null) {
            return;
        }

        showItemCircle(DEFAULT_RANGE_RADIUS);

        // placing a marker
//        mMap.setInfoWindowAdapter(new ItemLocationInfoWindowAdapter(
//                this,
//                homeObject));
//        mMap.setOnInfoWindowClickListener(this);

//        if (itemMarker != null) {
//            itemMarker.remove();
//        }

//        itemMarker = mMap.addMarker(new MarkerOptions()
//                .position(itemLocation)
//                .title(titleString)
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.transparent_bg))
//                .draggable(false)); //标记指定的 纬-经 度

        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
                calcLatLngBounds(itemLocation, MAP_PADDING_RADIUS),
                getResources().getDimensionPixelSize(R.dimen.item_big_map_padding)));

        // keep infoWindow displaying
//        itemMarker.showInfoWindow();
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
//                marker.showInfoWindow();
                autoFixing = false;  // interrupt the auto-fixing listener
                return true;
            }
        });
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
//                itemMarker.showInfoWindow();
                autoFixing = false;  // interrupt the auto-fixing listener
            }
        });
    }

    private void showItemCircle(double radiusInMeters) {
        // range overlay
        if (itemCircle != null) {
            itemCircle.remove();
        }

        CircleOptions circle = new CircleOptions()
                .center(itemLocation)
                .radius(radiusInMeters)
                .fillColor(getResources().getColor(R.color.fm_blue_map_overlay))
                .strokeColor(getResources().getColor(android.R.color.transparent))
                .strokeWidth(1);
        itemCircle = mMap.addCircle(circle);
    }

    private boolean isMinZoom(float level) {
        return mMap != null && Math.abs(mMap.getMinZoomLevel() - level) <= 1e-6;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        try {
            String q = marker.getPosition().latitude + "," +
                    marker.getPosition().longitude + "(" + homeObject.getTitle() + ")";
            q = Uri.encode(q);
            Uri uri = Uri.parse("geo:0,0?q=" + q);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
        catch (ActivityNotFoundException e) {
            L.e(e);
        }
    }

    // 跟踪地图缩放比例，保证商品位置circle不会被缩小到不可见
    private void trackMapScale() {
        Projection prj = mMap.getProjection();
        Point itemPnt = prj.toScreenLocation(itemLocation);
        LatLng edgeLocation = findCoordinate(itemLocation, DEFAULT_RANGE_RADIUS, 90);
        Point edgePnt = prj.toScreenLocation(edgeLocation);

        double radiusOnScreen = calcDistanceOnScreen(itemPnt, edgePnt);
        L.v("item radiusOnScreen: %f", radiusOnScreen);

        if (radiusOnScreen < minItemMapRadiusPx) {
            // 确保商品半径在屏幕至少达到一定的宽度（px）
            edgePnt = new Point(itemPnt);
            edgePnt.x += minItemMapRadiusPx;
            edgeLocation = prj.fromScreenLocation(edgePnt);
            double radius = computeDistanceBetween(itemLocation, edgeLocation);
            showItemCircle(radius);
        } else {
            showItemCircle(DEFAULT_RANGE_RADIUS);
        }
    }
}
