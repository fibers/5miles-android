package com.insthub.fivemiles.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.BeeFramework.Utils.ImageUtil;
import com.external.eventbus.EventBus;
import com.facebook.AppEventsLogger;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.insthub.fivemiles.Adapter.MakeOfferAdapter;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.AppMessageModel;
import com.insthub.fivemiles.Protocol.USER;
import com.insthub.fivemiles.SESSION;
import com.insthub.fivemiles.Utils.RxEventUtils;
import com.manuelpeinado.glassactionbar.BlurTask;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.thirdrock.domain.IUser;
import com.thirdrock.domain.ImageInfo;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.domain.Location;
import com.thirdrock.domain.User;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.activity.AbsActivity;
import com.thirdrock.fivemiles.framework.view.PopupMenu;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.review.ReviewActivity;
import com.thirdrock.fivemiles.review.ReviewViewModel;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.Currencies;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.EduTip;
import com.thirdrock.fivemiles.util.EduUtils;
import com.thirdrock.fivemiles.util.FileUtils;
import com.thirdrock.fivemiles.util.ImageWidthCalcHelper;
import com.thirdrock.fivemiles.util.LocationUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.RateHelper;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.MakeOfferResp;
import com.thirdrock.protocol.offer.Buyer;
import com.thirdrock.protocol.offer.ChatMessage;
import com.thirdrock.protocol.offer.OfferLineDetailResp;
import com.thirdrock.protocol.offer.OfferlineMeta;
import com.thirdrock.repository.ReportRepository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import rx.functions.Action1;
import rx.functions.Func1;

import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;
import static android.provider.MediaStore.EXTRA_OUTPUT;
import static com.insthub.fivemiles.Activity.ItemLocationMapActivity.MAP_PADDING_RADIUS;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_CAN_GO_BACK;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_MSG_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_OFFER_LINE_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_OPPOSITE_USER;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REPORTED_OID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REPORT_TYPE;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_RF_TAG;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SHOULD_POP_KEYBOARD;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PUSH_OFFER_PREFIX;
import static com.insthub.fivemiles.FiveMilesAppConst.CHAT_EDU_REVIEW;
import static com.insthub.fivemiles.FiveMilesAppConst.EDU_CHAT_REVIEW_V;
import static com.insthub.fivemiles.FiveMilesAppConst.EMPTY_STRING;
import static com.insthub.fivemiles.FiveMilesAppConst.GOOGLE_MAP_ZOOM_LEVEL;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_MAKE_OFFER;
import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLatitudeInSession;
import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLongitudeInSession;
import static com.thirdrock.fivemiles.review.ReviewActivity.RESULT_FIVE_STAR;
import static com.thirdrock.fivemiles.util.DisplayUtils.toast;
import static com.thirdrock.fivemiles.util.LocationUtils.formatLocation;
import static com.thirdrock.fivemiles.util.LocationUtils.isValidLocation;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;
import static com.thirdrock.fivemiles.util.RateHelper.RateEvent.fiveStarReview;
import static com.thirdrock.framework.util.Utils.doOnLayoutChange;
import static com.thirdrock.framework.util.Utils.setBackground;
import static com.thirdrock.repository.GeoLocationReportRepository.GOOGLE_STATIC_MAP_URL;

/**
 * 取代BeeFramework版的MakeOfferActivity。初始化状态在两个地方：新聊天在doOnNewIntent中初始化，
 * 非新聊天在onPropertyChange中初始化。
 * TODO 打开新聊天和打开带历史记录的聊天要有一个区分，涉及到很多属性的初始化
 * Created by jubin on 27/5/15.
 */
public class MakeOfferActivity extends AbsActivity<MakeOfferViewModel, MakeOfferViewModel.Property>{

    private static final int MENU_REVIEW = 0;
    private static final int MENU_REPORT = 1;
    private static final int MENU_BLOCK = 2;

    private static final int MENU_ALBUM = 3;
    private static final int MENU_CAMERA = 4;
    private static final int MENU_LOCATION = 5;

    private static final int REQUEST_REVIEW = 101;
    private static final int REQUEST_ALBUM = 102;
    private static final int REQUEST_CAMERA = 103;

    private static final int REQUEST_PLACE_PICKER = 104;
    // KitKat引入ACTION_OPEN_DOCUMENT，返回的URI发生变化，所以要区分
//    private static final int REQUEST_ALBUM_KITKAT = 105;

    @InjectView(R.id.make_offer_image) ImageView itemImage;

    @InjectView(R.id.top_view_title) TextView title;

    @InjectView(R.id.make_offer_price) TextView price;

    @InjectView(R.id.lst_make_offer_history) ListView listView;

    @InjectView(R.id.make_offer_btn_send) TextView btnSend;

    @InjectView(R.id.make_offer_input_box) EditText txtInputBox;

    @InjectView(R.id.root_view) ViewGroup rootView;

    @InjectView(R.id.banner_wrapper) View headerWrapper;

    @InjectView(R.id.top_view_logo) View logoView;

    @InjectView(R.id.top_view_action_wrapper) ViewGroup topActionLayout;

    private TextView txtOfferHint;
    private View listViewMarginTop;

    @Inject MakeOfferViewModel viewModel;

    private int thumbSizePx;
    private DisplayImageOptions bgImgOpts;
    private boolean hasUnread;
    // messageId用于记录当前最新消息的id
    private String messageId = EMPTY_STRING;
    private int offerLineId = 0;            // 改为String类型

    private AppMessageModel msgModel;
    private RateHelper mRateHelper;
    private Item item;
    private EduTip reviewTip;
    private MakeOfferAdapter mAdapter;

    private boolean reviewEnabled;
    private String reviewAction, blockAction;
    private TextView btnToggleActionMenu;

    private static boolean isOnTop;

    private boolean isOppositeUserBlocked;
    private IUser oppositeUser;
    private long lastMsgTimestamp;

    private List<ChatMessage> chatMsgList;
    private File tmpCameraImageFile;

    // 聊天列表中的本地图片，服务器返回聊天列表时不再重复加载
    // TODO  这部分逻辑放到ViewModel中
    private Set<String> imagePlaceHolderIdSet;

    private LatLng oppositeUserLatLng;

    private AppEventsLogger fbEventsLogger;

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {

        fbEventsLogger = AppEventsLogger.newLogger(this);

        thumbSizePx = getResources().getDimensionPixelSize(R.dimen.list_thumb_size);

        BitmapFactory.Options bgImgDecodeOpts = new BitmapFactory.Options();
        bgImgDecodeOpts.inMutable = true;
        bgImgOpts = new DisplayImageOptions.Builder()
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .decodingOptions(bgImgDecodeOpts)
                .build();

        View headerView = View.inflate(MakeOfferActivity.this, R.layout.make_offer_header, null);
        txtOfferHint = (TextView) headerView.findViewById(R.id.txt_offer_hint);

        View bannerPlaceholder = View.inflate(MakeOfferActivity.this, R.layout.home_banner_placeholder, null);
        listViewMarginTop = bannerPlaceholder.findViewById(R.id.placeholder);

        listView.addHeaderView(bannerPlaceholder);
        listView.addHeaderView(headerView);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // empty
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (hasUnread && isLatestMsgVisible()) {
                    hasUnread = false;
                    msgModel.setOfferLineRead(messageId);
                }
            }
        });

        // send button is only enabled when we have some message body content
        RxEventUtils.text(txtInputBox)
                .map(new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String text) {
                        return ModelUtils.isEmpty(text);
                    }
                }).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean isEmpty) {
                if (isEmpty) {
                    btnSend.setEnabled(false);
                } else {
                    btnSend.setEnabled(true);
                }
            }
        });

        msgModel = new AppMessageModel(FiveMilesApp.appCtx);

        getWindow().setBackgroundDrawableResource(R.drawable.loading);

        doOnNewIntent(getIntent());

        initTopViewAction();

        mRateHelper = new RateHelper(MakeOfferActivity.this);

        initializeInputBox();

        imagePlaceHolderIdSet = new HashSet<>();
    }

    private void initializeInputBox() {
        txtInputBox.setImeOptions(EditorInfo.IME_ACTION_SEND);

        txtInputBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (EditorInfo.IME_ACTION_SEND != actionId) {
                    return false;
                }

                sendTextMessage();
                return true;
            }
        });
    }

    @OnClick(R.id.make_offer_btn_send)
    void sendTextMessage() {
        String text = txtInputBox.getText().toString().trim();

        if (TextUtils.isEmpty(text)) {
            DisplayUtils.toast(R.string.error_comment_required);
            return;
        }

        String toUid = null;
        String itemId = null;
        if (item != null) {
            itemId = item.getId();
            toUid = oppositeUser.getId();
        }

        viewModel.sendTextMessage(toUid, itemId, EMPTY_STRING, text);

        txtInputBox.setText("");
    }

    @OnClick(R.id.make_offer_btn_more)
    void showAttachMenu() {
        new PopupMenu(MakeOfferActivity.this)
                .addOptions(
                        new PopupMenu.Option(MENU_CAMERA, getString(R.string.chat_attach_from_camera)),
                        new PopupMenu.Option(MENU_ALBUM, getString(R.string.chat_attach_from_album)),
                        new PopupMenu.Option(MENU_LOCATION, getString(R.string.chat_attach_location))
                )
                .setOnSelectionListener(new PopupMenu.OnSelectionListener() {

                    @Override
                    public void onOptionSelected(PopupMenu.Option option) {
                        switch (option.getId()) {
                            case MENU_ALBUM:
                                pickImageFromAlbum();
                                trackTouch("chat_choosephoto");
                                break;
                            case MENU_CAMERA:
                                pickImageFromCamera();
                                trackTouch("chat_takephoto");
                                break;
                            case MENU_LOCATION:
                                pickLocation();
                                trackTouch("chat_selectlocation");
                                break;
                        }
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        trackTouch("chatplus_cancel");
                    }
                })
                .show();

        trackTouch("chatplus");
    }

    private void pickLocation() {
        LatLng myLatLng = new LatLng(getLatitudeInSession(), getLongitudeInSession());
        LatLngBounds bounds = LocationUtils.calcLatLngBounds(myLatLng, oppositeUserLatLng, MAP_PADDING_RADIUS);
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder()
                .setLatLngBounds(bounds);

        try {
            Intent pickPlaceIntent = builder.build(FiveMilesApp.appCtx);
            startActivityForResult(pickPlaceIntent, REQUEST_PLACE_PICKER);
        } catch (GooglePlayServicesRepairableException e) {
            L.w(e);
            DisplayUtils.toast("Please update your Google Play service");
        } catch (GooglePlayServicesNotAvailableException e) {
            L.w(e);
            DisplayUtils.toast("Sorry, Google Play service not available, can't pick location");
        }
    }

    private void pickImageFromCamera() {
        try {
            tmpCameraImageFile = FileUtils.getTempCameraImage();

            Intent pickFromCameraIntent = new Intent(ACTION_IMAGE_CAPTURE)
                    .putExtra(EXTRA_OUTPUT, Uri.fromFile(tmpCameraImageFile))
                    .putExtra("return-data", false);
            startActivityForResult(pickFromCameraIntent, REQUEST_CAMERA);
        } catch (IOException e) {
            L.e(e);
        }
    }

    private void pickImageFromAlbum() {

        // TODO 后面尽量用Uri的方式，目前先注释掉这块代码
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
//            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT)
//                    .setType("image/*")
//                    .addCategory(Intent.CATEGORY_OPENABLE);
//            // Only the system receives the ACTION_OPEN_DOCUMENT, so no need to test.
//            startActivityForResult(intent, REQUEST_ALBUM_KITKAT);
//        } else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT)
                    .setType("image/*");

            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, REQUEST_ALBUM);
            }
//        }

    }

    @Override
    protected void doOnNewIntent(Intent intent) {
        super.doOnNewIntent(intent);

        boolean isNewChat = !intent.hasExtra(ACT_PARA_OFFER_LINE_ID);
        // 新聊天，没有offerline，此时"我"是buyer
        if (isNewChat && intent.hasExtra(ACT_PARA_ITEM)) {
            chatMsgList = new ArrayList<>();
            item = (Item) intent.getSerializableExtra(ACT_PARA_ITEM);

            if (item != null) {
                oppositeUser = item.getOwner();
                oppositeUserLatLng = LocationUtils.locationToLatLng(item.getLocation());
                IUser buyer = SESSION.getInstance().getSelf();

                mAdapter = new MakeOfferAdapter(MakeOfferActivity.this, chatMsgList, buyer, oppositeUser);
                listView.setAdapter(mAdapter);

                showItemThumb(item, itemImage);
                price.setText(Currencies.formatCurrency(item.getCurrencyCode(), item.getPrice()));
            } else {
                L.w("item should not be null");
            }
            showItemImageAsBg(item);
            showOppositeLocation(item);
        } else {
            offerLineId = intent.getIntExtra(ACT_PARA_OFFER_LINE_ID, 0);
            messageId = intent.getStringExtra(ACT_PARA_MSG_ID);

            if (offerLineId > 0) {
                viewModel.getOfferLineDetails(offerLineId);
            }

            L.d("new intent offer push %s %s %s", intent.getAction(), offerLineId, messageId);
            cleanNotification();
        }

        if (intent.hasExtra(ACT_PARA_SHOULD_POP_KEYBOARD)) {
            boolean shouldPopKeyboard = intent.getBooleanExtra(ACT_PARA_SHOULD_POP_KEYBOARD, true);
            if (!shouldPopKeyboard) {       // 启动的时候隐藏键盘
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
        }

    }

    private void showOppositeLocation(Item pItem) {
        Location location = null;
        String nickname = "", country = "", province = "", city = "";
        if (pItem != null) {
            location = pItem.getLocation();
            country = pItem.getCountry();
            province = pItem.getProvince();
            city = pItem.getCity();

            User seller = pItem.getOwner();
            if (seller != null) {
                nickname = seller.getNickname();
            }
        }
        CharSequence hint = formatOppositeLocation(nickname, location, country, province, city);
        txtOfferHint.setText(hint);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().register(this);
        }

        // 教学
        if (shouldShowReviewTip()) {
            btnToggleActionMenu.post(new Runnable() {
                @Override
                public void run() {
                    reviewTip = EduUtils.showEduTip(CHAT_EDU_REVIEW, EDU_CHAT_REVIEW_V, new EduTip()
                            .text(R.string.tip_leave_review)
                            .anchor(btnToggleActionMenu)
                            .gravity(Gravity.BOTTOM | Gravity.END));
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        fbEventsLogger.logEvent(VIEW_MAKE_OFFER);

        isOnTop = true;
        updateHeaderHeight();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isOnTop = false;
        hideReviewTip();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().unregister(this);
        }

        closeKeyBoard();
    }

    // FIXME 放到更合适的位置，subscribe和unsubscribe应该一一对应
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (viewModel != null) {
            viewModel.unSubscribeAll();
        }
    }

    @Override
    protected void doOnActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_REVIEW:
                if (resultCode == RESULT_FIVE_STAR) {
                    if (mRateHelper.shouldShowRateDialog(fiveStarReview)) {
                        mRateHelper.showRateApp();
                        mRateHelper.markNotFirstFiveStar(); // TODO 改善逻辑，应该不必每次都写
                    }
                }
                break;
            case REQUEST_ALBUM:
                if (data == null) {
                    L.i("doOnActivityResult() pick from album: no data... :( ");
                    return;
                }

                Uri selectedImageUri = data.getData();
                String selectedImagePath = getGalleryImagePath(selectedImageUri);
                String resultFileName = EMPTY_STRING;
                if (selectedImagePath != null) {
                    resultFileName = selectedImagePath;
                } else if (selectedImageUri != null && selectedImageUri.getPath() != null) {
                    resultFileName = selectedImageUri.getPath();
                }

                if (ModelUtils.isNotEmpty(resultFileName)) {
                    viewModel.uploadImage(resultFileName);
                } else {
                    DisplayUtils.toast("Failed to select file");
                }
                break;
//            case REQUEST_ALBUM_KITKAT:
//                if (data == null) {
//                    L.i("doOnActivityResult() pick image from album kitkat: no data... :( ");
//                    return;
//                }
//                Uri selectedImageUriKitKat = data.getData();
//                if (selectedImageUriKitKat != null) {
//                    viewModel.uploadImage(selectedImageUriKitKat);
//                }
//                break;
            case REQUEST_CAMERA:
                if (resultCode != RESULT_OK) {
                    L.i("doOnActivityResult() taking photo: result code is not RESULT_OK");
                    return;
                }

                if (tmpCameraImageFile != null && tmpCameraImageFile.exists()) {
                    String filePath = tmpCameraImageFile.getAbsolutePath();
                    viewModel.uploadImage(filePath);
                }
                break;
            case REQUEST_PLACE_PICKER:
                if (resultCode != RESULT_OK) {
                    L.i("doOnActivityResult() taking photo: result code is not OK");
                    return;
                }

                Place place = PlacePicker.getPlace(data, MakeOfferActivity.this);
                if (place != null) {
                    String toUid = (oppositeUser == null)? EMPTY_STRING : oppositeUser.getId();
                    String itemId = (item == null)? EMPTY_STRING : item.getId();
                    CharSequence placeName = place.getName();
                    CharSequence address = place.getAddress();
                    LatLng latLng = place.getLatLng();
                    double lat = latLng.latitude;
                    double lng = latLng.longitude;
                    float widthInDimen = getResources().getDimension(R.dimen.chat_location_width);
                    float heightInDimen = getResources().getDimension(R.dimen.chat_location_height);
                    final int width = ImageUtil.getGoogleStaticMapWidth(widthInDimen);
                    final int height = ImageUtil.getGoogleStaticMapHeight(heightInDimen);
                    String mapThumbUrl = String.format(GOOGLE_STATIC_MAP_URL, lat, lng, GOOGLE_MAP_ZOOM_LEVEL, width, height);
                    boolean isSeller = isSeller();

                    viewModel.sendLocationMessage(toUid, itemId, address, placeName, lat, lng, mapThumbUrl, isSeller);
                    L.e("Map Thumb Url: " + mapThumbUrl);
                }
        }
    }
    /**
     * pick file name from content provider with Gallery-flavor format
     */
    public String getGalleryImagePath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor == null) {
            return null;        // uri could be not suitable for ContentProviders, i.e. points to file
        }
        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(columnIndex);
    }

    private void updateHeaderHeight() {
        doOnLayoutChange(headerWrapper, new Runnable() {
            @Override
            public void run() {
                ViewGroup.LayoutParams params = listViewMarginTop.getLayoutParams();
                params.height = headerWrapper.getMeasuredHeight();
                listViewMarginTop.setLayoutParams(params);
                listViewMarginTop.requestLayout();
            }
        });
    }

    private boolean shouldShowReviewTip() {
        return reviewEnabled && (reviewTip == null || !reviewTip.isShowing()) &&
                EduUtils.shouldShow(CHAT_EDU_REVIEW, EDU_CHAT_REVIEW_V);
    }

    private void hideReviewTip() {
        if (reviewTip != null) {
            reviewTip.dismiss();
        }
    }

    // 聊天是基于Push的，Push过来时会生成一个notification，在聊天页需要清除那个notification
    private void cleanNotification() {
        if (offerLineId > 0) {
            DisplayUtils.cancelNotification(ACT_PUSH_OFFER_PREFIX, offerLineId);
        }
    }

    private void showItemImageAsBg(final ItemThumb pItemThumb) {
        if (pItemThumb == null) {
            return;
        }

        doOnLayoutChange(rootView, new Runnable() {
            @Override
            public void run() {
                loadItemImage(pItemThumb);
            }
        });
    }

    // TODO move to view model
    private CharSequence formatOppositeLocation(String nickname, Location location, String country, String province, String city) {
        String distance = "", result;
        if (isValidLocation(location)) {
            distance = LocationUtils.formatDistance(location.getLatitude(), location.getLongitude());
        }

        String cityStr = formatLocation(country, province, city);
        boolean isSeller = isSeller();
        int res;

        if (isNotEmpty(distance, cityStr)) {
            res = isSeller ? R.string.hint_chat_offer_for_seller
                    : R.string.hint_chat_offer_for_buyer;
            result = getString(res, nickname, distance, cityStr);
        } else if (isNotEmpty(distance)) {
            res = isSeller ? R.string.hint_chat_offer_for_seller_no_location
                    : R.string.hint_chat_offer_for_buyer_no_location;
            result = getString(res, nickname, distance);
        } else if (isNotEmpty(cityStr)) {
            res = isSeller ? R.string.hint_chat_offer_for_seller_no_distance
                    : R.string.hint_chat_offer_for_buyer_no_distance;
            result = getString(res, nickname, cityStr);
        } else {
            res = isSeller ? R.string.hint_chat_offer_for_seller_no_distance_location
                    : R.string.hint_chat_offer_for_buyer_no_distance_location;
            result = getString(res);
        }

        return Html.fromHtml(result);
    }

    // TODO move to view model
    private boolean isSeller() {
        return item != null && item.getOwner() != null && ModelUtils.isOwner(item.getOwner().getId());
    }


    private void loadItemImage(ItemThumb item) {
        ImageInfo img = item.getDefaultImage();
        if (img == null) {
            return;
        }

        final ImageSize imgSize = getReasonableImageSize(
                getWindow().getDecorView().getMeasuredWidth(),
                getWindow().getDecorView().getMeasuredHeight());
        String url = CloudHelper.toCropUrl(img.getUrl(),
                imgSize.getWidth(), imgSize.getHeight(), "fill", true);

        ImageLoader.getInstance().loadImage(url, imgSize, bgImgOpts, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, final Bitmap loadedImage) {
                BlurTask.Listener onBlur = new BlurTask.Listener() {
                    @Override
                    public void onBlurOperationFinished() {
                        setBackground(getWindow(), loadedImage);
                    }
                };
                new BlurTask(MakeOfferActivity.this, onBlur, loadedImage, 25);
            }
        });
    }

    // 背景图要进行模糊化，因此取一半大小即可
    private ImageSize getReasonableImageSize(int w, int h) {
        double ratio = w > 0 ? ((double) h) / w : 1;
        int fitWidth = ImageWidthCalcHelper.getOnlineImageWidth((int) (w * 0.5));
        int fitHeight = (int) (fitWidth * ratio);

        return new ImageSize(fitWidth, fitHeight);
    }

    private void initTopViewAction() {
        logoView.setVisibility(View.VISIBLE);

        reviewEnabled = FiveMilesApp.getAppConfig().isReviewEnabled();
        if (reviewEnabled) {
            topActionLayout.setVisibility(View.VISIBLE);
            getLayoutInflater().inflate(R.layout.top_action_button_orange_line, topActionLayout);
            btnToggleActionMenu = (TextView) topActionLayout.findViewById(R.id.top_action_button);
            btnToggleActionMenu.setText(R.string.btn_leave_review);
            btnToggleActionMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showReviewActionMenu();
                    trackTouch("reviewbutton");
                }
            });
        }
    }

    private void showReviewActionMenu() {
        int blockRes = isOppositeUserBlocked? R.string.menu_unblock_person : R.string.menu_block_person;
        blockAction = isOppositeUserBlocked? "chat_unblock" : "chat_block";
        int reviewRes = R.string.menu_leave_review;
        reviewAction = "leavereview";

        String oppositeUid = (oppositeUser != null) ? oppositeUser.getId() : null;
        String itemId = item != null ? item.getId() : null;

        if (ReviewViewModel.hasReview(oppositeUid, itemId)) {
            reviewRes = R.string.menu_edit_review;
            reviewAction = "changereview";
        }

        new PopupMenu(this)
                .addOptions(new PopupMenu.Option(MENU_REVIEW, getString(reviewRes)),
                        new PopupMenu.Option(MENU_REPORT, getString(R.string.menu_report_person)),
                        new PopupMenu.Option(MENU_BLOCK, getString(blockRes)))
                .setOnSelectionListener(new PopupMenu.OnSelectionListener() {
                    @Override
                    public void onOptionSelected(PopupMenu.Option option) {
                        switch (option.getId()) {
                            case MENU_REVIEW:
                                leaveReview();
                                break;
                            case MENU_REPORT:
                                reportUser();
                                break;
                            case MENU_BLOCK:
                                blockUser();
                                break;
                        }
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        trackTouch("reviewbuttoncancel");
                    }
                })
                .show();

        trackTouch("reviewbutton");
    }

    private void leaveReview() {
        String itemId = item != null ? item.getId() : null;

        if (oppositeUser == null || isEmpty(itemId)) {
            return;
        }

        USER transformedOppositeUser = USER.fromDomain(oppositeUser);
        Intent reviewIntent = new Intent(this, ReviewActivity.class)
                .putExtra(ACT_PARA_OFFER_LINE_ID, offerLineId)
                .putExtra(ACT_PARA_OPPOSITE_USER, transformedOppositeUser.toDomain())
                .putExtra(ACT_PARA_ITEM, item);
        startActivityForResult(reviewIntent, REQUEST_REVIEW);
        trackTouch(reviewAction);
    }

    private void reportUser() {
        if (oppositeUser == null) {
            return;
        }

        startActivity(new Intent(this, ReportActivity.class)
                .putExtra(ACT_PARA_REPORT_TYPE, ReportRepository.REPORT_TYPE_USER)
                .putExtra(ACT_PARA_REPORTED_OID, oppositeUser.getId()));
        trackTouch("chat_reportperson");
    }

    private void blockUser() {
        if (oppositeUser == null) {
            return;
        }

        int blockRes = isOppositeUserBlocked? R.string.msg_alert_unblock_person : R.string.msg_alert_block_person;

        // TODO use material-design dialog
        new AlertDialog.Builder(this)
                .setMessage(blockRes)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        viewModel.blockUser(oppositeUser.getId(), isOppositeUserBlocked);
                        trackTouch(blockAction + "yes");
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        trackTouch(blockAction + "no");
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        trackTouch(blockAction + "no");
                    }
                })
                .show();

        trackTouch(blockAction);
    }

    @OnClick(R.id.top_view_back)
    public void back() {
        closeKeyBoard();

        finish();
        trackTouch("chat_back");
    }

    @OnClick(R.id.item_bar)
    void viewItemDetail() {
        if (item == null) {
            return;
        }

        Intent intent = new Intent(this, ItemActivity.class);
        intent.putExtra(ACT_PARA_ITEM_ID, item.getId());
        intent.putExtra(ACT_PARA_RF_TAG, "chat");
        startActivity(intent);

        trackTouch("chat_item");
    }

    private boolean isLatestMsgVisible() {
        int lastVisPos = listView.getLastVisiblePosition();
        return mAdapter != null && lastVisPos >= (mAdapter.getCount() - 1);
    }

    // 关闭键盘
    public void closeKeyBoard() {
        txtInputBox.clearFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txtInputBox.getWindowToken(), 0);
    }

    @Override
    protected String getScreenName() {
        return VIEW_MAKE_OFFER;
    }

    @Override
    protected int getContentView() {
        return R.layout.make_offer;
    }

    @Override
    protected MakeOfferViewModel getViewModel() {
        return viewModel;
    }

    @Override
    public void onPropertyChanged(MakeOfferViewModel.Property property, Object oldValue, Object newValue) throws Exception {
        switch (property) {
            case offer_line:
                OfferLineDetailResp detail = (OfferLineDetailResp) newValue;
                if (detail == null) {
                    L.w("OfferLineDetailResp is null");     // FIXME better way to handle it
                    return;
                }

                OfferlineMeta meta = detail.getMeta();
                if (meta == null) {
                    L.w("OfferlineMeta is null");
                    return;
                }
                // -------------------- 从这里开始，meta不会为null -------------------------------
                lastMsgTimestamp = meta.getLatestTs();
                isOppositeUserBlocked = meta.isUserBlocekd();
                item = meta.getItem();

                chatMsgList = detail.getChatMsgList();
                Buyer buyer = meta.getBuyer();
                User seller = meta.getItem().getOwner();

                mAdapter = new MakeOfferAdapter(MakeOfferActivity.this, chatMsgList, buyer, seller);
                listView.setAdapter(mAdapter);

                // -------------------- 获取聊天对象的名字，并更新顶部文字 --------------------------
                boolean isSeller = isSeller();
                String oppositeUserName = null;
                if (isSeller) {
                    if (meta.getBuyer() != null) {
                        oppositeUser = meta.getBuyer();
                        oppositeUserName = oppositeUser.getNickname();
                    }
                } else {
                    if (meta.getItem() != null && meta.getItem().getOwner() != null) {
                        oppositeUser = meta.getItem().getOwner();
                        oppositeUserName = oppositeUser.getNickname();
                    }
                }
                if (isNotEmpty(oppositeUserName)) {
                    title.setText(oppositeUserName);
                    logoView.setVisibility(View.GONE);
                }

                if (item != null) {
                    // -------------------- 从这里开始，item不会为null -------------------------------
                    // -------------------- 显示聊天界面item的缩略图和价格 --------------------------
                    showItemThumb(item, itemImage);
                    price.setText(Currencies.formatCurrency(item.getCurrencyCode(), item.getPrice()));

                    // --------------------- 使用商品图像做聊天背景 -------------------------------
                    doOnLayoutChange(rootView, new Runnable() {
                        @Override
                        public void run() {
                            loadItemImage(item);
                        }
                    });

                    // --------------------- 显示对方的位置信息 -------------------------------
                    Location location = null;
                    String nickname = "", country = "", province = "", city = "";
                    if (isSeller) {             // 当前用户是seller，需要显示对方的信息
                        if (buyer != null) {
                            nickname = buyer.getNickname();
                            location = buyer.getLocation();
                            city = buyer.getPlaceStr();
                        }
                    } else {
                        if (item.getOwner() != null) {
                            nickname = item.getOwner().getNickname();
                        }
                        location = item.getLocation();
                        country = item.getCountry();
                        province = item.getProvince();
                        city = item.getCity();
                    }
                    CharSequence hint = formatOppositeLocation(nickname, location, country, province, city);
                    txtOfferHint.setText(hint);

                    oppositeUserLatLng = LocationUtils.locationToLatLng(location);
                }
                // ---------------------- 滚动到聊天页最下方 -------------------------------
                scrollToLatestMsg();

                // ---------------------- 清除push通知 -------------------------------
                cleanNotification();
                break;
            case block_user:
                boolean newBlockStatus = !isOppositeUserBlocked;
                isOppositeUserBlocked = newBlockStatus;
                toast(newBlockStatus? R.string.msg_person_blocked : R.string.msg_person_unblocked);
                break;
            // TODO 将去重的逻辑放到ViewModel中，在返回给Activity之前就过滤
            case txt_msg_sent:
                MakeOfferResp txtMsgResp = (MakeOfferResp) newValue;
                if (txtMsgResp != null) {
                    messageId = txtMsgResp.getMessageId();
                    offerLineId = txtMsgResp.getOfferLineId();
                    viewModel.syncOfferlineUpdates(offerLineId, lastMsgTimestamp + 1);
                }
                break;
            case img_msg_sent:
                onImageOrLocationSent((MakeOfferResp) newValue);
                trackTouch("chat_sendphoto");
                break;
            case loc_msg_sent:
                onImageOrLocationSent((MakeOfferResp) newValue);
                trackTouch("");
                break;
            case offer_line_update:
                OfferLineDetailResp updateResp = (OfferLineDetailResp) newValue;
                if (chatMsgList != null && updateResp != null) {

                    synchronized (MakeOfferActivity.class) {
                        List<ChatMessage> chatUpdateList = updateResp.getChatMsgList();
                        if (chatUpdateList == null || chatUpdateList.isEmpty()) {
                            L.d("Chat meesage update list is null or empty");
                            return;
                        }

                        if (imagePlaceHolderIdSet == null || imagePlaceHolderIdSet.isEmpty()) {
                            chatMsgList.addAll(chatUpdateList);
                        } else {    // 查找被占据位置的图片信息，这些信息将不被加入聊天列表，从而避免重复数据
                            for (ChatMessage chatMsg : chatUpdateList) {
                                if (chatMsg == null) continue;

                                String msgId = chatMsg.getId();
                                if(imagePlaceHolderIdSet.contains(msgId)) {
                                    // FIXME 这里将所有的发送过程都置为已发送的状态，是不对的。需要找一个方法跟踪每一条消息
                                    // 例如建立localPath -> cloudUrl -> msgId的映射，但这么做需要增加ViewModel的参数
                                    for (ChatMessage msg : chatMsgList) {
                                        if (msg != null && msg.isTemp()) {
                                            // TODO 这种方式不好，增加一个字段，shouldShowProgress更好
                                            msg.setIsTemp(false);        // FIXME 用Map来存储可能会更好一些
                                        }
                                    }
                                } else {
                                    chatMsgList.add(chatMsg);
                                }
                            }
                        }

                        if (updateResp.getMeta() != null) {
                            lastMsgTimestamp = updateResp.getMeta().getLatestTs();
                        }

                        mAdapter.notifyDataSetChanged();
                    }
                }

                scrollToLatestMsg();
                break;
            case upload_image_start:
                // 在聊天列表中放入临时会话
                String localImagePath = (String) newValue;
                ChatMessage tmpImageMsg = ChatMessage.getTempImageMessage(localImagePath, isSeller(), false);
                synchronized (MakeOfferActivity.class) {
                    chatMsgList.add(tmpImageMsg);
                    mAdapter.notifyDataSetChanged();
                }
                scrollToLatestMsg();
                break;
//            case upload_image_start_kitkat:
//                Uri imageUri = (Uri) newValue;
//                if (imageUri != null) {
//                    // FIXME imageUri转为String的方式是否合适
//                    ChatMessage tmpImageMsgKitKat = ChatMessage.getTempImageMessage(imageUri.toString(), isSeller(), true);
//                    chatMsgList.add(tmpImageMsgKitKat);
//                    mAdapter.notifyDataSetChanged();
//                }
//                break;
            case upload_image_end:
                ImageInfo imageInfo = (ImageInfo) newValue;
                if (imageInfo != null) {
                    viewModel.sendImageMessage(oppositeUser.getId(), item.getId(), imageInfo.getUrl());
                }
                break;
            case send_location_start:
                ChatMessage tmpLocationMsg = (ChatMessage) newValue;
                synchronized (MakeOfferActivity.class) {
                    chatMsgList.add(tmpLocationMsg);
                    mAdapter.notifyDataSetChanged();
                }
                scrollToLatestMsg();
                break;
        }
    }

    private void onImageOrLocationSent(MakeOfferResp newValue) {
        MakeOfferResp imgMsgResp = newValue;
        if (imgMsgResp != null) {
            messageId = imgMsgResp.getMessageId();
            if (imagePlaceHolderIdSet == null) {
                imagePlaceHolderIdSet = new HashSet<>(1);
            }
            imagePlaceHolderIdSet.add(messageId);

            offerLineId = imgMsgResp.getOfferLineId();
            viewModel.syncOfferlineUpdates(offerLineId, lastMsgTimestamp + 1);
        }
    }

    private void showItemThumb(Item pItem, ImageView pItemImage) {
        if (pItem == null) {
            return;
        }

        List<ImageInfo> imageList = pItem.getImages();
        if (imageList != null && imageList.get(0) != null) {
            String imageUrl = imageList.get(0).getUrl();
            String cloudImageUrl = CloudHelper.toCropUrl(imageUrl, thumbSizePx, thumbSizePx, "fill", null);
            ImageLoader.getInstance().displayImage(cloudImageUrl, pItemImage, FiveMilesApp.displayImageOptions);
        }
    }

    // FIXME 图片较多的时候滚不到底部
    void scrollToLatestMsg() {
        if (mAdapter != null) {
            listView.post(new Runnable() {
                @Override
                public void run() {
                    listView.smoothScrollToPosition(mAdapter.getCount() + listView.getHeaderViewsCount());
                }
            });
        }
    }

    public void onEvent(Object event) {
        Message message = (Message) event;

        if (message.what == MessageConstant.VIEW_PROFILE) {
            String uId = (String) message.obj;
            viewProfile(uId);
            trackTouch("chatperson");
        } else if (message.what == MessageConstant.PUSH_ACT_NEW_OFFER &&
                offerLineId == message.arg1) {
            // 当前offerLine有新push，则获取新消息
            messageId = (String) message.obj;
            viewModel.syncOfferlineUpdates(offerLineId, lastMsgTimestamp);
        }
    }

    private void viewProfile(String userId) {
        if (ModelUtils.isEmpty(userId)) {
            L.w("user should not be null.");
            return;
        }

        Intent intent;
        if (ModelUtils.isOwner(userId)) {
            intent = new Intent(this, TabProfileActivity.class);
            intent.putExtra(ACT_PARA_CAN_GO_BACK, true);
            startActivity(intent);
        } else {
            intent = new Intent(this, OtherProfileActivity.class);
            intent.putExtra(ACT_PARA_USER_ID, userId);
            startActivity(intent);
        }
        trackTouch("product_seller");
    }

    // 用于判断当前Active的Activity是不是MakeOfferActivity
    public static boolean isOfferLineOnTop(int offerLineId) {
        return isOnTop && offerLineId > 0;
    }
}
