package com.insthub.fivemiles.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AbstractAjaxCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.external.androidquery.util.AQUtility;
import com.external.eventbus.EventBus;
import com.facebook.Session;
import com.facebook.Settings;
import com.facebook.internal.NativeProtocol;
import com.facebook.widget.LikeView;
import com.facebook.widget.LoginButton;
import com.insthub.fivemiles.ConstantS;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.ProfileModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.USER_INFO;
import com.insthub.fivemiles.SESSION;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.common.phone.VerifyPhoneActivity;
import com.thirdrock.fivemiles.framework.service.CloudUploadService;
import com.thirdrock.fivemiles.main.listing.ListItemActivity;
import com.thirdrock.fivemiles.profile.MyItemsActivity;
import com.thirdrock.fivemiles.review.ReviewListActivity;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.EduScreen;
import com.thirdrock.fivemiles.util.EduUtils;
import com.thirdrock.fivemiles.util.EventUtils;
import com.thirdrock.fivemiles.util.FacebookLinker;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.ui.widget.AvatarView;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.StoreInfo;
import com.thirdrock.protocol.StoreInfo__JsonHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_BUYING_COUNT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_CAN_GO_BACK;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_EMAIL;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_FOLLOWER_COUNT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_FOLLOWING_COUNT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_LIKES_COUNT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_NICK_NAME;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELECTION_SCREEN_NAME;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELLER_RATE_AVG;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELLER_RATE_COUNT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELLING_COUNT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_VERIFIED;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_VERIFIED_PHONE;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_PAGE_URL;
import static com.insthub.fivemiles.FiveMilesAppConst.EDU_VERIFICATION_V;
import static com.insthub.fivemiles.FiveMilesAppConst.FB_PERMISSION_PUBLISH;
import static com.insthub.fivemiles.FiveMilesAppConst.ONE_MINUTE_MILLS;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_EDU_VERIFICATION;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_FOLLOWER;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_FOLLOWING;
import static com.insthub.fivemiles.MessageConstant.PUSH_ACT_SHOW_HOME;
import static com.thirdrock.framework.util.Utils.doOnGlobalLayout;

public class TabProfileActivity extends Activity implements OnClickListener, BusinessResponse {

    private static final int REQ_VERIFY_PHONE = 5;
    private AvatarView avatar;
    private TextView title;
    private LinearLayout following;
    private TextView following_count;
    private LinearLayout items;
    private TextView items_count;
    private LinearLayout followers;
    private TextView followers_count;
    private LinearLayout watch;
    private TextView watch_count;
    private LinearLayout buying;
    private TextView buying_count;

    @InjectView(R.id.profile_reputation)
    ViewGroup vgReputation;

    @InjectView(R.id.profile_reputation_divider)
    View reputationDivider;

    @InjectView(R.id.profile_reviews_star)
    ImageView ivReviewStars;

    @InjectView(R.id.profile_reviews_num)
    TextView txtReviewCount;

    @InjectView(R.id.edit_profile)
    TextView editProfile;

    private ImageView back;
    private ProfileModel profileModel;
    private boolean canNaviBack;
    protected ImageLoader imageLoader = ImageLoader.getInstance();

    private int buyingCount, sellingCount, followingCount, followerCount, reviewCount, likesCount;
    private double reputationScore;
    private boolean isEmailVerified, isFacebookVerified, isPhoneVerified;
    private String email, nickname;

    public final static String VIEW_NAME = "myprofile_view";
    private String avatarUrl;

    private ImageView ivAppNewMsg;

    // seller verification icons
    private ImageView emailVerifyView;
    private ImageView facebookVerifyView;
    private ImageView phoneVerifyView;
    private ImageView verifyInfoView;

    private FacebookLinker fbLinker;

    private View summaryView;
    private EduScreen eduScreen;
    private PopupWindow verifyTip;

    private boolean initialized = false;        // 对于第一次进入My Profile，显示进度条和网络错误；对于之后再进入，则没有必要
    private boolean isSharingShop = false;  // 记录当前状态，看是否在分享自己的店铺到facebook
    private long lastEmailSendTimeMills = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AQUtility.debug("----------------- onCreate --------------------");
        setContentView(R.layout.tab_profile);//以list为布局
        ButterKnife.inject(this);

        // TabProfileActivity继承自Activity，无法使用isLoginProtected
        boolean isNotAuthed = !SESSION.getInstance().isAuth();
        if (isNotAuthed) {
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
            return;
        }

        // set the header views
        back = (ImageView) findViewById(R.id.top_view_back);
        title = (TextView) findViewById(R.id.top_view_title);
        title.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
        title.setEllipsize(TextUtils.TruncateAt.END);
        findViewById(R.id.top_view_logo).setVisibility(View.VISIBLE);

        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        editProfile.setVisibility(View.VISIBLE);
        editProfile.setOnClickListener(this);

        summaryView = findViewById(R.id.summary_wrapper);
        avatar = (AvatarView) findViewById(R.id.profile_avatar);
        following = (LinearLayout) findViewById(R.id.profile_following);
        following_count = (TextView) findViewById(R.id.profile_following_count);
        items = (LinearLayout) findViewById(R.id.profile_items);
        items_count = (TextView) findViewById(R.id.profile_items_count);
        followers = (LinearLayout) findViewById(R.id.profile_followers);
        followers_count = (TextView) findViewById(R.id.profile_followers_count);
        watch = (LinearLayout) findViewById(R.id.profile_watch);
        watch_count = (TextView) findViewById(R.id.profile_watch_count);
        buying = (LinearLayout) findViewById(R.id.profile_buying);
        buying_count = (TextView) findViewById(R.id.profile_buying_count);

        LinearLayout shareMyShopLayout = (LinearLayout) findViewById(R.id.profile_share_my_shop);

        LinearLayout invite = (LinearLayout) findViewById(R.id.profile_invite);
        LinearLayout help = (LinearLayout) findViewById(R.id.profile_help);
        LinearLayout setting = (LinearLayout) findViewById(R.id.profile_setting);

        emailVerifyView = (ImageView) findViewById(R.id.iv_my_profile_verify_email);
        facebookVerifyView = (ImageView) findViewById(R.id.iv_my_profile_verify_facebook);
        phoneVerifyView = (ImageView) findViewById(R.id.iv_my_profile_verify_phone);
        verifyInfoView = (ImageView) findViewById(R.id.iv_my_profile_verify_info);

        avatar.setOnClickListener(this);
        items.setOnClickListener(this);
        following.setOnClickListener(this);
        followers.setOnClickListener(this);
        watch.setOnClickListener(this);
        buying.setOnClickListener(this);
        vgReputation.setOnClickListener(this);
        shareMyShopLayout.setOnClickListener(this);
        invite.setOnClickListener(this);
        help.setOnClickListener(this);
        setting.setOnClickListener(this);

        profileModel = new ProfileModel(this);
        profileModel.addResponseListener(this);

        ivAppNewMsg = (ImageView) findViewById(R.id.app_new_message);
        emailVerifyView.setOnClickListener(this);
        facebookVerifyView.setOnClickListener(this);
        phoneVerifyView.setOnClickListener(this);

        // Facebook likes
        Settings.sdkInitialize(this);
        LikeView fbLikeView = (LikeView) findViewById(R.id.fb_like_view);
        fbLikeView.setLikeViewStyle(LikeView.Style.BOX_COUNT);
        fbLikeView.setAuxiliaryViewPosition(LikeView.AuxiliaryViewPosition.INLINE);
        fbLikeView.setObjectId("https://www.facebook.com/5milesapp");

        onNewIntent(getIntent());

        verifyInfoView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupWindow window = getVerifyTip();
                int popupEndPartWidth = getResources().getDimensionPixelSize(R.dimen.profile_info_popup_end_part_width);
                int infoIconViewWidth = verifyInfoView.getWidth();
                int xOffset = window.getWidth() - popupEndPartWidth - infoIconViewWidth / 2;

                verifyTip.showAsDropDown(v, -xOffset, 0);

                TrackingUtils.trackTouch(VIEW_NAME, "myverifyinfoicon");
            }
        });

        LoginButton btnFbLogin = (LoginButton) findViewById(R.id.tab_profile_fb_login_btn);
        fbLinker = new FacebookLinker(this,
                btnFbLogin,
                null,
                Arrays.asList(FB_PERMISSION_PUBLISH),
                new FacebookLinker.Callback() {
                    @Override
                    public boolean shouldSubmitLinkRequest() {
                        return (isFacebookVerified == false);    // facebook未验证才可以提交连接Facebook请求
                    }

                    @Override
                    public void onCanceledOrFailed(int requestCode) {
                        DisplayUtils.showFacebookVerificationIcon(facebookVerifyView, isFacebookVerified);

                        isSharingShop = false;
                        DisplayUtils.toast(R.string.toast_fail_link_fb);
                    }

                    @Override
                    public void onComplete(int requestCode) {
                        isFacebookVerified = true;
                        SESSION.getInstance().setFbVerified(isFacebookVerified);
                        DisplayUtils.showFacebookVerificationIcon(facebookVerifyView, isFacebookVerified);

                        if (isSharingShop){
                            isSharingShop = false;              // 对于FB而言，分享店铺中和自己相关的逻辑已经结束

                            tryShareMyShopToFB();
                        }
                    }
                });
        fbLinker.onCreate(savedInstanceState);

        boolean reviewEnabled = FiveMilesApp.getAppConfig().isReviewEnabled();
        reputationDivider.setVisibility(reviewEnabled ? View.VISIBLE : View.GONE);
        vgReputation.setVisibility(reviewEnabled ? View.VISIBLE : View.GONE);
    }

    /**
     * 如果在售item数目大于0，分享店铺；否则，提示发布新商品
     */
    private void tryShareMyShopToFB() {
        if (sellingCount > 0){
            String fuzzUserId = SESSION.getInstance().userId;
            profileModel.shareStore(fuzzUserId);
        }else {
            new AlertDialog.Builder(TabProfileActivity.this)
                    .setMessage(R.string.dlg_txt_list_item_first)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent listItemIntent = new Intent(TabProfileActivity.this, ListItemActivity.class);
                            startActivity(listItemIntent);
                            finish();

                            TrackingUtils.trackTouch(VIEW_NAME, "shareshoplistyes");
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TrackingUtils.trackTouch(VIEW_NAME, "shareshoplistno");
                        }
                    })
                    .show();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initialized = false;
        canNaviBack = getIntent().getBooleanExtra(ACT_PARA_CAN_GO_BACK, false);
        if (canNaviBack) {
            back.setVisibility(View.VISIBLE);
        } else {
            back.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus bus = EventBus.getDefault();
        if (!bus.isregister(this)) {
            bus.register(this);
        }
        showEducation();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        fbLinker.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        fbLinker.onStop();

        hideEducation();
        if (verifyTip != null && verifyTip.isShowing()) {
            verifyTip.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fbLinker.onDestroy();
        EventBus bus = EventBus.getDefault();
        if (bus.isregister(this)) {
            bus.unregister(this);
        }
    }

    @SuppressWarnings("unused")
    public void onEvent(Object event) {
        Message message = (Message) event;
        if (message.what == MessageConstant.PUSH_ACT_REFRESH_PROFILE) {
            DisplayUtils.cancelNotification(message.arg1);
            profileModel.getMeInfo(initialized);
        } else if (message.what == MessageConstant.APP_HAS_UPDATE) {
            ivAppNewMsg.setVisibility(CloudHelper.isAppHasUpdate() ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.edit_profile :
                TrackingUtils.trackTouch(VIEW_NAME, "editprofile");
                // leave through, no break
            case R.id.profile_avatar :

                intent = new Intent(TabProfileActivity.this, MyProfileActivity.class);
                if (profileModel.user_info != null) {
                    intent.putExtra("avatar", avatarUrl);
                    intent.putExtra("nickname", profileModel.user_info.nickname);
                    intent.putExtra("city",profileModel.user_info.city);
                    intent.putExtra("region",profileModel.user_info.region);
                    intent.putExtra("country",profileModel.user_info.country);
                    intent.putExtra(ACT_PARA_USER_VERIFIED, profileModel.user_info.isVerified);
                    intent.putExtra(ACT_PARA_VERIFIED_PHONE, profileModel.user_info.phoneNo);
                }
                startActivityForResult(intent, 1);
                TrackingUtils.trackTouch(VIEW_NAME, "headphoto");
                break;
            case R.id.profile_items:

                intent = new Intent(TabProfileActivity.this, MyItemsActivity.class);
                intent.putExtra("user_id", SESSION.getInstance().userId);
                intent.putExtra(ACT_PARA_SELLING_COUNT, sellingCount);
                startActivity(intent);
                TrackingUtils.trackTouch(VIEW_NAME, "myitem");
                break;
            case R.id.profile_following:

                intent = new Intent(TabProfileActivity.this, FollowingActivity.class);
                intent.putExtra("user_id", SESSION.getInstance().userId);
                intent.putExtra(ACT_PARA_FOLLOWING_COUNT, followingCount);
                intent.putExtra(ACT_PARA_SELECTION_SCREEN_NAME, VIEW_FOLLOWING);
                startActivity(intent);
                TrackingUtils.trackTouch(VIEW_NAME, "myfollowing");
                break;
            case R.id.profile_followers:

                intent = new Intent(TabProfileActivity.this, FollowerActivity.class);
                intent.putExtra("user_id", SESSION.getInstance().userId);
                intent.putExtra(ACT_PARA_FOLLOWER_COUNT, followerCount);
                intent.putExtra(ACT_PARA_SELECTION_SCREEN_NAME, VIEW_FOLLOWER);
                startActivity(intent);
                TrackingUtils.trackTouch(VIEW_NAME, "myfollowers");
                break;
            case R.id.profile_watch:

                intent = new Intent(TabProfileActivity.this, WatchingActivity.class);
                intent.putExtra(ACT_PARA_LIKES_COUNT, likesCount);
                startActivity(intent);
                TrackingUtils.trackTouch(VIEW_NAME, "mylikes");
                break;
            case R.id.profile_buying:

                intent = new Intent(TabProfileActivity.this, BuyingActivity.class);
                intent.putExtra(ACT_PARA_BUYING_COUNT, buyingCount);
                startActivity(intent);
                TrackingUtils.trackTouch(VIEW_NAME, "mybuying");
                break;
            case R.id.profile_reputation:

                intent = new Intent(TabProfileActivity.this, ReviewListActivity.class);
                intent.putExtra(ACT_PARA_USER_ID, SESSION.getInstance().userId);
                intent.putExtra(ACT_PARA_SELLER_RATE_COUNT, reviewCount);
                intent.putExtra(ACT_PARA_SELLER_RATE_AVG, reputationScore);
                startActivity(intent);
                TrackingUtils.trackTouch(VIEW_NAME, "myreview");
                break;
            case R.id.profile_share_my_shop:

                TrackingUtils.trackTouch(VIEW_NAME, "sharemyshop");
                if (!isFacebookVerified) {
                    new AlertDialog.Builder(TabProfileActivity.this)
                            .setMessage(R.string.dlg_txt_link_fb_to_share)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    isSharingShop = true;
                                    fbLinker.link();
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();
                    // 在facebook回调中处理后面的逻辑
                }else{
                    tryShareMyShopToFB();
                }

                break;
            case R.id.profile_setting:

                intent = new Intent(TabProfileActivity.this, SettingActivity.class);

                intent.putExtra(ACT_PARA_EMAIL, email);
                intent.putExtra(ACT_PARA_NICK_NAME, nickname);
                startActivity(intent);
                TrackingUtils.trackTouch(VIEW_NAME, "setting");
                break;
            case R.id.profile_help:

                Intent helpIntent = new Intent(TabProfileActivity.this, GenericWebActivity.class);

                helpIntent.putExtra(ACT_PARA_PAGE_URL, getString(R.string.url_help, email, nickname));
                startActivity(helpIntent);
                break;
            case R.id.profile_invite:

                intent = new Intent(Intent.ACTION_SEND); // 启动分享发送的属性
                intent.setType("text/plain"); // 分享发送的数据类型
                String urlInSession = SESSION.getInstance().homeBranchUrl;
                String url = ModelUtils.isNull(urlInSession) ? ConstantS.HOME_URL : urlInSession;
                String inviteMsg = getString(R.string.share_invite_friends, url);
                intent.putExtra(Intent.EXTRA_TEXT, inviteMsg); // 分享的内容
                startActivity(Intent.createChooser(intent, "Share To"));// 目标应用选择对话框的标题
                overridePendingTransition(R.anim.push_buttom_in, R.anim.push_buttom_out);
                TrackingUtils.trackTouch(VIEW_NAME, "invite");
                break;
            case R.id.top_view_share:

                shareApp();
                overridePendingTransition(R.anim.push_buttom_in, R.anim.push_buttom_out);
                TrackingUtils.trackTouch(VIEW_NAME, "shareapp");
                break;
            case R.id.iv_my_profile_verify_email:

                if (!isEmailVerified) {

                    long currentTimeMills = System.currentTimeMillis();
                    if (currentTimeMills - lastEmailSendTimeMills < ONE_MINUTE_MILLS){
                        new AlertDialog.Builder(TabProfileActivity.this).setMessage(R.string.dlg_txt_wait_a_minute).setPositiveButton(android.R.string.yes, null).show();
                        break;
                    }else {
                        profileModel.sendVerifyEmail();
                        lastEmailSendTimeMills = currentTimeMills;
                    }


                    TrackingUtils.trackTouch(VIEW_NAME, "verifyemailicon");
                }
                break;
            case R.id.iv_my_profile_verify_facebook:

                if (!isFacebookVerified) {
                    fbLinker.link();
                    TrackingUtils.trackTouch(VIEW_NAME, "verifyfacebookicon");
                }
                break;
            case R.id.iv_my_profile_verify_phone:

                if (!isPhoneVerified) {
                    startActivityForResult(new Intent(this, VerifyPhoneActivity.class), REQ_VERIFY_PHONE);
                    TrackingUtils.trackTouch(VIEW_NAME, "verifyphoneicon");
                }
                break;

        }
    }

    private void shareApp() {
        Intent intent = new Intent(Intent.ACTION_SEND); // 启动分享发送的属性
        intent.setType("text/plain"); // 分享发送的数据类型
        String msg = getString(R.string.share_app);
        intent.putExtra(Intent.EXTRA_TEXT, msg); // 分享的内容
        startActivity(Intent.createChooser(intent, "Share To"));// 目标应用选择对话框的标题
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
            throws JSONException {
        if (url.startsWith(ApiInterface.ME_INFO)) {
            USER_INFO user = profileModel.user_info;

            if (user.portrait != null) {
                avatarUrl = CloudHelper.toCropUrlInPids(user.portrait,
                        getResources().getDimension(R.dimen.profile_avatar_size),
                        getResources().getDimension(R.dimen.profile_avatar_size),
                        "thumb,g_face", null);
            }
            DisplayUtils.showAvatar(avatar,
                    user,
                    getResources().getDimensionPixelSize(R.dimen.profile_avatar_size),
                    FiveMilesApp.imageOptionsAvatar);

            title.setText(user.nickname);
            findViewById(R.id.top_view_logo).setVisibility(View.GONE);

            sellingCount = user.items_count;
            buyingCount = user.purchases_count;
            followerCount = user.followers_count;
            followingCount = user.followings_count;
            likesCount = user.likes_count;
            reviewCount = user.reviewCount;
            reputationScore = user.reputationScore;

            email = SESSION.getInstance().email;
            nickname = user.nickname;

            DisplayUtils.showCount(items, items_count, sellingCount);
            DisplayUtils.showCount(following, following_count, followingCount);
            DisplayUtils.showCount(followers, followers_count, followerCount);
            DisplayUtils.showCount(watch, watch_count, likesCount);
            DisplayUtils.showCount(buying, buying_count, buyingCount);
            DisplayUtils.showCount(vgReputation, txtReviewCount, reviewCount);
            DisplayUtils.showStars(user, ivReviewStars);

            isEmailVerified = user.isEmailVerified;
            isFacebookVerified = user.isFacebookVerified;
            isPhoneVerified = user.isPhoneVerified;

            DisplayUtils.showEmailVerificationIcon(emailVerifyView, user.isEmailVerified);
            DisplayUtils.showFacebookVerificationIcon(facebookVerifyView, user.isFacebookVerified);
            DisplayUtils.showPhoneVerificationIcon(phoneVerifyView, user.isPhoneVerified);

            SESSION.getInstance()
                    .emlVerified(user.isEmailVerified)
                    .fbVerified(user.isFacebookVerified)
                    .phoneVerified(user.isPhoneVerified)
                    .save();
        }

        if (url.startsWith(ApiInterface.SEND_VERIFY_EMAIL)) {
            if (status != null){
                if (status.getCode() == 200){
                    // 弹出对话框提示用户去邮箱打开邮件
                    new AlertDialog.Builder(TabProfileActivity.this).setMessage(R.string.verify_email_alert_success_content).setPositiveButton("OK", null).show();
                } else {
                    DisplayUtils.toast(status.getMessage());
                }
            } else {
                L.d("Beeframe work callback's status is: null");        // 应该不会发生
            }
        }

        if (url.startsWith(ApiInterface.SHARE_STORE) && status != null && status.getCode() == 200) {
            if (jo != null) {

                try{
                    StoreInfo myStore = StoreInfo__JsonHelper.parseFromJson(jo.toString());

                    if (myStore != null && isFacebookVerified) {
                        Session fbSession = Session.getActiveSession();

                        if (fbSession != null){
                            if (!fbSession.isOpened()) {
                                Session.openActiveSessionFromCache(TabProfileActivity.this);
                                // 此时Session.getActiveSession()是open的，fbSession不一定，也许Session.getActiveSession()并不是单例
                            }

                            CloudUploadService.postMyShopToFacebook(TabProfileActivity.this, myStore.getFuzzId(), myStore.getNickname());
                        }
                    }
                }catch (Exception e){
                    L.e("error in parse share store returned json");
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        fbLinker.onResume();

        // 对于第一次进入My Profile，显示进度条和网络错误；对于之后再进入，则没有必要
        profileModel.getMeInfo(initialized);
        initialized = true;

        ivAppNewMsg.setVisibility(CloudHelper.isAppHasUpdate() ? View.VISIBLE : View.GONE);

        // 作为首页的一个tab，只有一次create、start，pv需在resume里记录
        TrackingUtils.trackView(VIEW_NAME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        fbLinker.onPause();
        AbstractAjaxCallback.cancel();  // cancel all running & pending (BeeFramework) ajax jobs
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!canNaviBack && KeyEvent.KEYCODE_BACK == keyCode) {
            EventUtils.post(PUSH_ACT_SHOW_HOME, 0);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fbLinker.onActivityResult(requestCode, resultCode, data);
        LikeView.handleOnActivityResult(this, requestCode, resultCode, data);
        // facebook dialog completed
        if (NativeProtocol.DIALOG_REQUEST_CODE == requestCode) {
            // FIXME 由于无法跟踪likeView的click，暂在此统计likeView
            L.d("FB like clicked");
            TrackingUtils.trackTouch("likefacebook_popup", "likefacebookpage");
        }

        if (resultCode == Activity.RESULT_OK) {
            if (REQ_VERIFY_PHONE == requestCode) {
                isPhoneVerified = true;
                DisplayUtils.showPhoneVerificationIcon(phoneVerifyView, isPhoneVerified);
            }
        }
    }

    // TODO extracts to super class
    // user education screen
    private void showEducation() {
        if (!shouldShowEducation() ||
                (eduScreen != null && eduScreen.isShowing())) {
            return;
        }

        eduScreen = EduUtils.showEduScreen(this, R.style.dialog_edu_transparent,
                R.layout.edu_verification, PREF_EDU_VERIFICATION, EDU_VERIFICATION_V,
                new EduScreen.ViewController() {
                    @Override
                    public void onInit(EduScreen screen, View contentView) {
                        final View summaryPlaceholder = contentView.findViewById(R.id.summary_placeholder);
                        final ViewGroup.LayoutParams params = summaryPlaceholder.getLayoutParams();

                        doOnGlobalLayout(summaryView, new Runnable() {
                            @Override
                            public void run() {
                                params.height = summaryView.getMeasuredHeight();
                                summaryPlaceholder.requestLayout();
                            }
                        });
                    }
                });
    }

    private boolean shouldShowEducation() {
        return EduUtils.shouldShow(PREF_EDU_VERIFICATION, EDU_VERIFICATION_V);
    }

    private void hideEducation() {
        if (eduScreen != null && eduScreen.isShowing()) {
            eduScreen.dismiss();
        }
    }

    private PopupWindow getVerifyTip() {
        if (verifyTip == null) {
            View popupView = getLayoutInflater().inflate(R.layout.popup_profile_verify_info, null);
            final int popupWidth = getResources().getDimensionPixelSize(R.dimen.profile_info_popup_width);
            verifyTip = new PopupWindow(popupView, popupWidth, ViewGroup.LayoutParams.WRAP_CONTENT, true);
            verifyTip.setBackgroundDrawable(getResources().getDrawable(android.R.color.transparent));

            popupView.findViewById(R.id.tip_bg).getBackground().setAlpha(OtherProfileActivity.TIP_BG_ALPHA);

            /** my Profile的图标介绍文案与other profile的文案不一样 */
            TextView avatarVerifyDesc = (TextView) popupView.findViewById(R.id.profile_verify_popup_avatar_txt);
            avatarVerifyDesc.setText(R.string.tab_profile_verify_popup_avatar_desc);
            TextView emailVerifyDesc = (TextView) popupView.findViewById(R.id.profile_verify_popup_email_txt);
            emailVerifyDesc.setText(R.string.tab_profile_verify_popup_email_desc);
            TextView facebookVerifyDesc = (TextView) popupView.findViewById(R.id.profile_verify_popup_fb_txt);
            facebookVerifyDesc.setText(R.string.tab_profile_verify_popup_fb_desc);
            TextView phoneVerifyDesc = (TextView) popupView.findViewById(R.id.profile_verify_popup_phone_txt);
            phoneVerifyDesc.setText(R.string.tab_profile_verify_popup_phone_desc);

            popupView.findViewById(R.id.btn_get_verified).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent myProfileIntent = new Intent(TabProfileActivity.this, MyProfileActivity.class);
                    if (profileModel.user_info != null) {
                        myProfileIntent.putExtra("avatar", avatarUrl);
                        myProfileIntent.putExtra("nickname", profileModel.user_info.nickname);
                        myProfileIntent.putExtra(ACT_PARA_USER_VERIFIED, profileModel.user_info.isVerified);
                        myProfileIntent.putExtra(ACT_PARA_VERIFIED_PHONE, profileModel.user_info.phoneNo);
                    }
                    startActivity(myProfileIntent);
                    if (verifyTip != null) {
                        verifyTip.dismiss();
                    }
                    TrackingUtils.trackTouch(VIEW_NAME, "mygetverified");
                }
            });
        }
        return verifyTip;
    }
}
