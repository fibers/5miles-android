package com.insthub.fivemiles.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.BeeFramework.Utils.AnimationUtil;
import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.facebook.widget.LoginButton;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.Model.LoginModel;
import com.insthub.fivemiles.Model.ProfileModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.USER_INFO;
import com.insthub.fivemiles.SESSION;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.common.phone.VerifyPhoneActivity;
import com.thirdrock.fivemiles.login.ZipcodeVerifyActivity;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.FacebookLinker;
import com.thirdrock.fivemiles.util.LocationUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.ui.widget.AvatarView;
import com.thirdrock.framework.util.L;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_LINK_FB;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_VERIFIED;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_VERIFIED_PHONE;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_RESULT_VERIFIED_PHONE;
import static com.insthub.fivemiles.FiveMilesAppConst.FB_PERMISSION_PUBLISH;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;

/**
 * 创建时，若intent中没有数据，则从服务器读取数据。目前这个逻辑已经去除
 */
public class MyProfileActivity extends BaseActivity implements OnClickListener, BusinessResponse {

    private final int REQUEST_PHOTOZOOM=3;
    private final int CHANGE_AVATAR=4;
    private final int REQ_VERIFY_PHONE = 5;
    private final int SUBMIT_ZIPCODE = 6;

	private ImageView back;
	private TextView title;
	private LinearLayout changeAvatar;
	private AvatarView avatar;
	private LinearLayout changeNickname;
	private TextView nickname;
	private TextView emailView;

	private View blackView, lblEmail;
	private LinearLayout dialogView;
	private TextView camera;
	private TextView photo;
	private TextView cancel;


	private String avatarUrl;
	private String name;
    // private String email;
	protected ImageLoader imageLoader = ImageLoader.getInstance();

//    private File fileDir;
//    private File file;
//    private String fileName = "";
//    private File fileDir2;
//    private String imagePath;
//    private File avatarFile;

    private LoginModel 		loginModel;
    private ProfileModel profileModel;
    private boolean isInitialized;
    private TakePhotoAction takePhotoAction;

    @InjectView(R.id.my_profile_phone)
    TextView txtPhone;

    @InjectView(R.id.cbx_link_facebook)
    CheckBox cbxLinkFb;

    @InjectView(R.id.fb_login_btn)
    LoginButton btnFbLogin;

    @InjectView(R.id.my_profile_phone_wrapper)
    LinearLayout lytPhone;

    @InjectView(R.id.my_profile_link_fb)
    LinearLayout lytLinkFb;

    @InjectView(R.id.my_profile_location)
    LinearLayout lytLocation;

    @InjectView(R.id.location_detail)
    TextView txtLocationDetail;



    private boolean userVerified;
    private boolean fbVerified;
    private boolean phoneVerified;
    private FacebookLinker fbLinker;
    private boolean forceRelink;  // 是否正在重新link facebook，如token过期后

    private boolean isRefresh = false;

    public static final String VIEW_NAME = "editprofile_view";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_profile);
        ButterKnife.inject(this);

		avatarUrl = getIntent().getStringExtra("avatar");
		name = getIntent().getStringExtra("nickname");
        fbVerified = SESSION.getInstance().isFbVerified();
        phoneVerified = SESSION.getInstance().isPhoneVerified();
        userVerified = getIntent().getBooleanExtra(ACT_PARA_USER_VERIFIED, false);
        String phone = getIntent().getStringExtra(ACT_PARA_VERIFIED_PHONE);
        if (isNotEmpty(phone)) {
            updatePhoneLayout(phone);
        }

		back = (ImageView) findViewById(R.id.top_view_back);
		title = (TextView) findViewById(R.id.top_view_title);
		changeAvatar = (LinearLayout) findViewById(R.id.my_profile_change_avatar);
		avatar = (AvatarView) findViewById(R.id.my_profile_avatar);
		changeNickname = (LinearLayout) findViewById(R.id.my_profile_change_nickname);
		nickname = (TextView) findViewById(R.id.my_profile_nickname);
		emailView = (TextView) findViewById(R.id.my_profile_email);
        lblEmail = findViewById(R.id.my_profile_email_lbl);

		blackView = findViewById(R.id.my_profile_black_view);
		dialogView = (LinearLayout) findViewById(R.id.my_profile_dialog_view);
		camera = (TextView) findViewById(R.id.my_profile_camera);
		photo = (TextView) findViewById(R.id.my_profile_photo);
		cancel = (TextView) findViewById(R.id.my_profile_cancel);


        if (SESSION.getInstance().isFbLogin()){
            lytLinkFb.setVisibility(View.GONE);
        }

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(isRefresh) {
					Intent intent = new Intent();
					setResult(Activity.RESULT_OK, intent);
					finish();
				} else {
					finish();
				}

			}
		});
		title.setText(R.string.my_profile);

		changeAvatar.setOnClickListener(this);
		changeNickname.setOnClickListener(this);
		blackView.setOnClickListener(this);
		camera.setOnClickListener(this);
		photo.setOnClickListener(this);
		cancel.setOnClickListener(this);
        lytLocation.setOnClickListener(this);

		loginModel = new LoginModel(this);
		loginModel.addResponseListener(this);
        profileModel = new ProfileModel(this);
        profileModel.addResponseListener(this);
        takePhotoAction = new TakePhotoAction(this, loginModel, avatar);

        isInitialized = false;

        showFbLinkState();

        fbLinker = new FacebookLinker(this,
                btnFbLogin,
                null,
                Arrays.asList(FB_PERMISSION_PUBLISH),
                new FacebookLinker.Callback() {
                    @Override
                    public boolean shouldSubmitLinkRequest() {
                        return forceRelink || cbxLinkFb.isChecked() != fbVerified;
                    }

                    @Override
                    public void onCanceledOrFailed(int requestCode) {
                        forceRelink = false;
                        if (ACT_LINK_FB.equals(getIntent().getAction())) {
                            // 强制relink的情况，失败时记verified为false，以便能够手动重试
                            fbVerified = false;
                        }
                        L.d("linkfb restore link state on cancel, linked %s", fbVerified);
                        showFbLinkState();  // restore link state
                    }

                    @Override
                    public void onComplete(int requestCode) {
                        if (forceRelink) {
                            forceRelink = false;
                            // 重新绑定(更新token)不需要重置verified状态
                        } else {
                            fbVerified = !fbVerified;  // toggle link state
                        }
                        L.d("linkfb set linked state to %s on complete", fbVerified);
                        showFbLinkState();
                    }
                });
        fbLinker.onCreate(savedInstanceState);

        if (ACT_LINK_FB.equals(getIntent().getAction())) {
            cbxLinkFb.setEnabled(false);
            forceRelink = true;
            fbLinker.link();
        }
	}

    @Override
    protected void onPause() {
        super.onPause();
        fbLinker.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fbLinker.onResume();

        if (ModelUtils.isEmpty(avatarUrl) || ModelUtils.isEmpty(name)) {
            profileModel.getMeInfo(isInitialized);  // 若返回结果，后面则无需显示进度条
        } else {
            DisplayUtils.showAvatar(avatar,
                    avatarUrl,
                    userVerified,
                    getResources().getDimensionPixelSize(R.dimen.profile_row_avatar_size),
                    FiveMilesApp.imageOptionsAvatar);
            nickname.setText(name);
        }
        SESSION session = SESSION.getInstance();
        String email = session.email;
        if (!ModelUtils.isEmpty(email)) {
            lblEmail.setVisibility(View.VISIBLE);
            emailView.setVisibility(View.VISIBLE);
            emailView.setText(email);
        }
        else {
            lblEmail.setVisibility(View.INVISIBLE);
            emailView.setVisibility(View.INVISIBLE);
        }

        String formatedLocation = LocationUtils.getDisplayLocation(
                session.getCountry(),
                session.getRegion(),
                session.getCity());
        if(formatedLocation.equals("")) {
            formatedLocation = LocationUtils.getDisplayLocation(
                    getIntent().getStringExtra("country"),
                    getIntent().getStringExtra("region"),
                    getIntent().getStringExtra("city"));
        }
        txtLocationDetail.setText(formatedLocation);


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        fbLinker.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        fbLinker.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        fbLinker.onSaveInstanceState(outState);
    }

    @Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()) {
            case R.id.my_profile_change_avatar:
                blackView.setVisibility(View.VISIBLE);
                dialogView.setVisibility(View.VISIBLE);
                AnimationUtil.showAnimation(dialogView);
                TrackingUtils.trackTouch(VIEW_NAME, "changephoto");
                break;
            case R.id.my_profile_change_nickname:
                Intent intent1 = new Intent(this, ChangeNicknameActivity.class);
                intent1.putExtra("avatar", avatarUrl);
                intent1.putExtra("nickname", name);
                startActivityForResult(intent1, CHANGE_AVATAR);
                TrackingUtils.trackTouch(VIEW_NAME, "changename");
                break;
            case R.id.my_profile_black_view:
                blackView.setVisibility(View.GONE);
                dialogView.setVisibility(View.GONE);
                AnimationUtil.backAnimation(dialogView);
                break;
            case R.id.my_profile_camera:
                blackView.setVisibility(View.GONE);
                dialogView.setVisibility(View.GONE);
                AnimationUtil.backAnimation(dialogView);
                takePhotoAction.takePhoto();
                break;
            case R.id.my_profile_photo:
                blackView.setVisibility(View.GONE);
                dialogView.setVisibility(View.GONE);
                AnimationUtil.backAnimation(dialogView);
                takePhotoAction.choosePhoto();
                break;
            case R.id.my_profile_cancel:
                blackView.setVisibility(View.GONE);
                dialogView.setVisibility(View.GONE);
                AnimationUtil.backAnimation(dialogView);
                break;
            case R.id.my_profile_location:
                Intent zipCodeIntent = new Intent(MyProfileActivity.this, ZipcodeVerifyActivity.class)
                        .putExtra("from", VIEW_NAME);
                startActivityForResult(zipCodeIntent, SUBMIT_ZIPCODE);

                TrackingUtils.trackTouch(VIEW_NAME, "changezipcode");
                break;
        }
    }

	@Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {
		if (url.startsWith(ApiInterface.USER_EDIT_PROFILE)) {
			isRefresh = true;
            takePhotoAction.fillAvatar();
            DisplayUtils.toast(R.string.msg_operation_succeed);
		}

        if (url.startsWith(ApiInterface.ME_INFO)){
            USER_INFO user = profileModel.user_info;

            this.userVerified = user.isVerified;
            if (user.portrait != null) {
                avatarUrl = CloudHelper.toCropUrlInPids(user.portrait,
                        getResources().getDimension(R.dimen.profile_avatar_size),
                        getResources().getDimension(R.dimen.profile_avatar_size),
                        "thumb,g_face", null);
                DisplayUtils.showAvatar(avatar,
                        avatarUrl,
                        userVerified,
                        getResources().getDimensionPixelSize(R.dimen.profile_row_avatar_size),
                        FiveMilesApp.imageOptionsAvatar);
            }
            if (ModelUtils.isNotEmpty(user.nickname)){
                name = user.nickname;
                nickname.setText(name);
            }

            if (!ACT_LINK_FB.equals(getIntent().getAction())) {
                fbVerified = user.isFacebookVerified;
                L.d("linkfb set linked state to %s on /me/ loaded", fbVerified);
                showFbLinkState();
            }

            phoneVerified = user.isPhoneVerified;
            if (phoneVerified){
                updatePhoneLayout(user.phoneNo);
            }
        }
	}

    private void updatePhoneLayout(String phoneNum) {
        txtPhone.setText(phoneNum);
        lytPhone.setEnabled(false);             // 手机号验证以后，不可以更换
        txtPhone.setEnabled(false);             // 手机号验证以后，不可以更换
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fbLinker.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            boolean processed = takePhotoAction.processActivityResult(requestCode, data);
            if (!processed) {
                if (requestCode == CHANGE_AVATAR) {
                    isRefresh = true;
                    if (data != null) {
                        nickname.setText(data.getStringExtra("nickname"));
                        name = nickname.getText().toString();
                    }
                }
            }

            if (requestCode == REQ_VERIFY_PHONE) {
                String phoneNum = data.getStringExtra(ACT_RESULT_VERIFIED_PHONE);
                SESSION.getInstance().setPhoneVerified(true);
                updatePhoneLayout(phoneNum);
            } else if(requestCode == SUBMIT_ZIPCODE && data != null) {

                txtLocationDetail.setText(
                        LocationUtils.getDisplayLocation(
                                data.getStringExtra("country")
                                , data.getStringExtra("region")
                                , data.getStringExtra("city"))
                );
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (dialogView != null && View.VISIBLE == dialogView.getVisibility()){
                dialogView.setVisibility(View.GONE);
                blackView.setVisibility(View.GONE);
                return true;
            } else {
                if(isRefresh) {
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                } else {
                    finish();
                }
            }

			return false;
		}
		return true;
    }

    @OnClick(R.id.cbx_link_facebook)
    @SuppressWarnings("unused")
    void onLinkFb() {
        cbxLinkFb.setEnabled(false);

        if (cbxLinkFb.isChecked()) {  // to be unchecked
            fbLinker.unlink();

            TrackingUtils.trackTouch(VIEW_NAME, "closelinkfacebook");
        } else {  // to be checked
            fbLinker.link();

            TrackingUtils.trackTouch(VIEW_NAME, "verifyfacebook");
        }
    }

    private void showFbLinkState() {
        if (SESSION.getInstance().isFbLogin()) {
            cbxLinkFb.setEnabled(false);
            cbxLinkFb.setChecked(true);
        } else {
            cbxLinkFb.setEnabled(true);
            cbxLinkFb.setChecked(fbVerified);
        }
    }

    @OnClick({R.id.my_profile_phone_wrapper, R.id.my_profile_phone})
    @SuppressWarnings("unused")
    void onVerifyPhone() {
        startActivityForResult(new Intent(this, VerifyPhoneActivity.class), REQ_VERIFY_PHONE);

        TrackingUtils.trackTouch(VIEW_NAME, "verifyphone");
    }

    @Override
    public void onBackPressed(){
        if (dialogView != null && View.VISIBLE == dialogView.getVisibility()){
            dialogView.setVisibility(View.GONE);
            blackView.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }
}
