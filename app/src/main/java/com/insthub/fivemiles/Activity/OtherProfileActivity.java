package com.insthub.fivemiles.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.BeeFramework.Utils.AnimationUtil;
import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.Model.ProfileModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.HOME_OBJECTS;
import com.insthub.fivemiles.Protocol.IMAGES;
import com.insthub.fivemiles.Protocol.USER;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.pedrogomez.renderers.AdapteeCollection;
import com.pedrogomez.renderers.RendererAdapter;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.common.item.ItemThumbRenderer;
import com.thirdrock.fivemiles.framework.view.PopupMenu;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.review.ReviewListActivity;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.EduTip;
import com.thirdrock.fivemiles.util.EduUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.ui.renderer.MonoRendererBuilder;
import com.thirdrock.framework.ui.renderer.SimpleAdapteeCollection;
import com.thirdrock.framework.ui.widget.AvatarView;
import com.thirdrock.framework.ui.widget.WaterfallListView;
import com.thirdrock.repository.ReportRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import rx.Subscription;
import rx.functions.Action1;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_FOLLOWER_COUNT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_FOLLOWING_COUNT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM_THUMB;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REPORTED_OID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REPORT_TYPE;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELLER_RATE_AVG;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELLER_RATE_COUNT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ERR_BLOCKED;
import static com.insthub.fivemiles.FiveMilesAppConst.PROFILE_EDU_REVIEW_KEY;
import static com.insthub.fivemiles.FiveMilesAppConst.PROFILE_EDU_REVIEW_V;
import static com.thirdrock.fivemiles.util.DisplayUtils.toast;
import static com.thirdrock.framework.ui.widget.WaterfallListView.OnScrollListener.ScrollDirection;
import static com.thirdrock.framework.util.Utils.doOnGlobalLayout;

public class OtherProfileActivity extends BaseActivity implements BusinessResponse,
        WaterfallListView.Callback, SwipeRefreshLayout.OnRefreshListener {
    public static final int TIP_BG_ALPHA = 0xf3;  // alpha of tip window background, 95%
    private static final int MENU_BLOCK = 0;
    private static final int MENU_REPORT = 1;

    private static boolean onTop;
    private static String currUserId;

    private View 				headerView;
    private ImageView 			back;
    private TextView 			title;
    private AvatarView avatar;

    private View                follow;
    private TextView 			followTitle;
    private ImageView 			followIcon;

    //    private LinearLayout 		location;
//    private TextView 			location_text;
    private View profileWrapper;
    private LinearLayout        items;
    private TextView 			items_count;
    private LinearLayout 		following;
    private TextView 			following_count;
    private LinearLayout 		followers;
    private TextView 			followers_count;
    private LinearLayout 		seller_feedback;
    private TextView 			seller_feedback_count;
    private ImageView 			seller_feedback_more;
    private View                sellerPlaceWrapper;
    private TextView            tvSellerPlace;

    private ImageView[]        seller_stars;

    private LinearLayout reviews;
    private TextView reviewsNum;
    private ImageView otherProfileReviewsStar;
    private boolean reviewEnabled;

    @InjectView(R.id.swipe_refresh_view)
    SwipeRefreshLayout swipeRefreshView;

    @InjectView(R.id.other_profile_listview)
    WaterfallListView listView;

    @InjectView(R.id.top_view_share)
    ImageView btnTopMenu;

    View blankView;

    // seller verification icons
    private ImageView mVerifyEmailView;
    private ImageView mVerifyFacebookView;
    private ImageView mVerifyPhoneView;
    private ImageView mVerifyInfoView;

    // unfollow confirm view
    private View mUnfollowDlg;
    private TextView mUnfollowConfirm;
    private TextView mUnfollowCancel;
    private View mBlackBgView;

    private USER user;
    private String userId;

    private ProfileModel profileModel;
    private RendererAdapter<ItemThumb> itemListAdapter;
    private AdapteeCollection<ItemThumb> userItems;

    protected ImageLoader imageLoader = ImageLoader.getInstance();

    public static final String VIEW_NAME = "sellerprofile_view";

    private int itemCount, followingCount, followerCount, sellerRateCount;

    private PopupWindow verifyTip;
    private EduTip reviewTip;
    private Subscription subsUserBlocking;

    private final View.OnLayoutChangeListener updateTipsAction = new View.OnLayoutChangeListener() {
        private int lastY;

        // header加载完毕后高度变化导致tips偏移，此时需要刷新tips
        @Override
        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
            if (isFinishing() || bottom == lastY) {
                return;
            }

            updateReivewTip();
            lastY = bottom;
        }
    };

    public static boolean isUserOnTop(String userId) {
        return onTop && TextUtils.equals(userId, currUserId);
    }

    private boolean isFollowed() {
        return profileModel.user_detail != null && profileModel.user_detail.userInfo.followed;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.other_profile_list);
        ButterKnife.inject(this);

        back 		= (ImageView) findViewById(R.id.top_view_back);
        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                TrackingUtils.trackTouch(VIEW_NAME, "back");
            }
        });
        title 		= (TextView) findViewById(R.id.top_view_title);
        title.setFilters(new InputFilter[] {new InputFilter.LengthFilter(15)});
        title.setEllipsize(TextUtils.TruncateAt.END);


        btnTopMenu.setVisibility(View.VISIBLE);
        btnTopMenu.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu_ellipse));

        headerView = getLayoutInflater().inflate(R.layout.other_profile_header, null);

        avatar 				= (AvatarView) 		headerView.findViewById(R.id.other_profile_avatar);

        follow 		        =                	headerView.findViewById(R.id.other_profile_follow);
        followTitle 		= (TextView) 		headerView.findViewById(R.id.other_profile_follow_title);
        followIcon 		    = (ImageView) 		headerView.findViewById(R.id.other_profile_follow_icon);

//        location 			= (LinearLayout) 	headerView.findViewById(R.id.other_profile_location);
//        location_text 		= (TextView) 		headerView.findViewById(R.id.other_profile_location_text);
        profileWrapper = headerView.findViewById(R.id.profile_wrapper);
        items 			    = (LinearLayout) 	headerView.findViewById(R.id.other_profile_items);
        items_count 		= (TextView) 		headerView.findViewById(R.id.other_profile_items_count);
        following 			= (LinearLayout) 	headerView.findViewById(R.id.other_profile_following);
        following_count 	= (TextView) 		headerView.findViewById(R.id.other_profile_following_count);
        followers 			= (LinearLayout) 	headerView.findViewById(R.id.other_profile_followers);
        followers_count 	= (TextView) 		headerView.findViewById(R.id.other_profile_followers_count);
//		seller_feedback 	= (LinearLayout) 	headerView.findViewById(R.id.other_profile_seller_feedback);
//        seller_feedback_count 	= (TextView) 	headerView.findViewById(R.id.other_profile_seller_feedback_count);
//        seller_stars = new ImageView[]{
//                (ImageView) headerView.findViewById(R.id.other_profile_seller_feedback_star1),
//                (ImageView) headerView.findViewById(R.id.other_profile_seller_feedback_star2),
//                (ImageView) headerView.findViewById(R.id.other_profile_seller_feedback_star3),
//                (ImageView) headerView.findViewById(R.id.other_profile_seller_feedback_star4),
//                (ImageView) headerView.findViewById(R.id.other_profile_seller_feedback_star5)
//        };
//        seller_feedback_more 	= (ImageView) 	headerView.findViewById(R.id.other_profile_seller_feedback_more);
        sellerPlaceWrapper  = headerView.findViewById(R.id.other_profile_loc_wrapper);
        tvSellerPlace       = (TextView) headerView.findViewById(R.id.other_profile_loc_text);

        mVerifyEmailView = (ImageView) headerView.findViewById(R.id.iv_other_profile_verify_email);
        mVerifyFacebookView = (ImageView) headerView.findViewById(R.id.iv_other_profile_verify_facebook);
        mVerifyPhoneView = (ImageView) headerView.findViewById(R.id.iv_other_profile_verify_phone);
        mVerifyInfoView = (ImageView) headerView.findViewById(R.id.iv_other_profile_verify_info);

        mUnfollowDlg = findViewById(R.id.dlg_bottom_unfollow);
        mUnfollowConfirm = (TextView) findViewById(R.id.unfollow_confirm);
        mUnfollowCancel = (TextView) findViewById(R.id.unfollow_cancel);
        mBlackBgView = findViewById(R.id.unfollow_black_view);

        reviews = (LinearLayout) headerView.findViewById(R.id.other_profile_buyer_reviews);
        reviewsNum = (TextView) headerView.findViewById(R.id.other_profile_buyer_review_num);
        otherProfileReviewsStar = (ImageView) headerView.findViewById(R.id.other_profile_reviews_star);


        profileModel = new ProfileModel(this);
        profileModel.addResponseListener(this);

        avatar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user == null) return;

                // 点击头像展示大图
                IMAGES image = new IMAGES();
                image.url = user.portrait;
                image.image_width = 100;
                image.image_height = 100;
                ArrayList<IMAGES> images = new ArrayList<IMAGES>(1);
                images.add(image);

                Intent intent = new Intent(OtherProfileActivity.this, LargeImageActivity.class);
                intent.putExtra("images", images);
                intent.putExtra("position", 0);
                intent.putExtra("title", user.nick_name);
                startActivity(intent);

                TrackingUtils.trackTouch(VIEW_NAME, "itsphoto");
            }
        });

        follow.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//                if (SESSION.getInstance().isAuth()) {
                if (isFollowed()) {
                    mBlackBgView.setVisibility(View.VISIBLE);
                    mUnfollowDlg.setVisibility(View.VISIBLE);
                    AnimationUtil.showAnimation(mUnfollowDlg);

                    TrackingUtils.trackTouch(VIEW_NAME, "unfollow");
                } else {
                    profileModel.follow(userId);
                }
//                } else {
//                    Intent targetIntent = new Intent(OtherProfileActivity.this, OtherProfileActivity.class);
//                    targetIntent.putExtra(ACT_PARA_USER_ID, userId);
//
//                    Intent intent = new Intent(OtherProfileActivity.this, SignInOrUpActivity.class)
//                            .putExtra(ACT_PARA_REDIRECT_INTENT, targetIntent);
//                    startActivity(intent);
//
//                    TrackingUtils.trackTouch(OtherProfileActivity.this, VIEW_LOG_IN, "follow_login");
//                }
            }
        });

        mUnfollowConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mBlackBgView.setVisibility(View.GONE);
                mUnfollowDlg.setVisibility(View.GONE);
                AnimationUtil.backAnimation(mUnfollowDlg);

                profileModel.unfollow(userId);
                TrackingUtils.trackTouch(VIEW_NAME, "unfollow_yes");
            }
        });

        mUnfollowCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mBlackBgView.setVisibility(View.GONE);
                mUnfollowDlg.setVisibility(View.GONE);
                AnimationUtil.backAnimation(mUnfollowDlg);
                TrackingUtils.trackTouch(VIEW_NAME, "unfollow_no");
            }
        });

        mBlackBgView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mBlackBgView.setVisibility(View.GONE);
                mUnfollowDlg.setVisibility(View.GONE);
                AnimationUtil.backAnimation(mUnfollowDlg);
                TrackingUtils.trackTouch(VIEW_NAME, "unfollow_no");
            }
        });

        initListView();

//        location.setOnClickListener(new LocationListener());

//        items.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (profileModel.dataList.size() > 0) {
//                    listView.scrollBy(0, headerView.getHeight());     //
//                }
//            }
//        });

        following.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OtherProfileActivity.this, FollowingActivity.class);
                intent.putExtra("user_id", userId);
                intent.putExtra(ACT_PARA_FOLLOWING_COUNT, followingCount);
                startActivity(intent);
                TrackingUtils.trackTouch(VIEW_NAME, "itsfollowing");
            }
        });

        followers.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OtherProfileActivity.this, FollowerActivity.class);
                intent.putExtra("user_id", userId);
                intent.putExtra(ACT_PARA_FOLLOWER_COUNT, followerCount);
                startActivity(intent);
                TrackingUtils.trackTouch(VIEW_NAME, "itsfollowers");
            }
        });

//		seller_feedback.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(OtherProfileActivity.this, FeedbackListActivity.class);
//				intent.putExtra("user_id", userId);
//                intent.putExtra(ACT_PARA_SELLER_RATE_COUNT, sellerRateCount);
//				startActivity(intent);
//                TrackingUtils.trackTouch(OtherProfileActivity.this, VIEW_NAME, "itssellfeedback");
//            }
//		});

        reviews.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user == null) {
                    return;
                }

                startActivity(new Intent(OtherProfileActivity.this, ReviewListActivity.class)
                    .putExtra(ACT_PARA_USER_ID, userId)
                    .putExtra(ACT_PARA_SELLER_RATE_COUNT, user.reviewCount)
                    .putExtra(ACT_PARA_SELLER_RATE_AVG, user.reputationScore));
                TrackingUtils.trackTouch(VIEW_NAME, "seller_review");
            }
        });

        mVerifyInfoView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupWindow window = getVerifyTip();
                int popupEndPartWidth = getResources().getDimensionPixelSize(R.dimen.profile_info_popup_end_part_width);
                int infoIconViewWidth = mVerifyInfoView.getWidth();
                int xOffset = window.getWidth() - popupEndPartWidth - infoIconViewWidth / 2;

                window.showAsDropDown(v, -xOffset, 0);

                TrackingUtils.trackTouch(VIEW_NAME, "itsverifyinfoicon");
            }
        });

        reviewEnabled = FiveMilesApp.getAppConfig().isReviewEnabled();
        reviews.setVisibility(reviewEnabled ? View.VISIBLE : View.GONE);
        if (reviewEnabled) {
            headerView.addOnLayoutChangeListener(updateTipsAction);
        }

        TrackingUtils.trackView(VIEW_NAME);
    }

    private void initListView() {
        listView.setCallback(this);
        listView.addHeaderView(headerView);

        DisplayUtils.setProgressColorScheme(swipeRefreshView);
        swipeRefreshView.setOnRefreshListener(this);
        swipeRefreshView.setProgressViewOffset(true,
                getResources().getDimensionPixelSize(R.dimen.swipe_refresh_progress_offset_start),
                getResources().getDimensionPixelSize(R.dimen.swipe_refresh_progress_offset_end));

        View blankViewFooter = getLayoutInflater().inflate(R.layout.blank_view, null);
        blankView = blankViewFooter.findViewById(R.id.blank_view);
        blankView.setVisibility(View.GONE);
        ((TextView) blankViewFooter.findViewById(R.id.txt_blank_view_desc)).setText(R.string.hint_no_item_in_sale);

        listView.addFooterView(blankViewFooter);

        userItems = new SimpleAdapteeCollection<>();
        initListAdapter();
    }

    private void animateRefresh() {
        if (!swipeRefreshView.isRefreshing()) {
            swipeRefreshView.setRefreshing(true);
            onRefresh();
        }
    }

    private void initListAdapter() {
        itemListAdapter = new RendererAdapter<>(
                getLayoutInflater(),
                new MonoRendererBuilder(new ItemThumbRenderer()),
                userItems);
        listView.setAdapter(itemListAdapter);
    }

    @OnItemClick(R.id.other_profile_listview)
    @SuppressWarnings("unused")
    void onItemClick(int position) {
        int index = position - listView.getHeaderViewsCount();
        if (index >= 0 && index < userItems.size()) {
            Intent intent = new Intent(OtherProfileActivity.this, ItemActivity.class);
            intent.putExtra(ACT_PARA_ITEM_THUMB, userItems.get(index));
            startActivity(intent);
            TrackingUtils.trackTouch(VIEW_NAME, "itsproduct");
        }
    }

    private void showReviewTip() {
        if (shouldShowReviewTip()) {
            EduUtils.showEduTip(PROFILE_EDU_REVIEW_KEY, PROFILE_EDU_REVIEW_V, getReviewTip());
        }
    }

    private EduTip getReviewTip() {
        if (reviewTip == null) {
            reviewTip = new EduTip().text(R.string.tip_public_profile_leave_review)
                    .anchor(otherProfileReviewsStar)
                    .gravity(Gravity.BOTTOM | Gravity.END);
        }

        return reviewTip;
    }

    private void hideReviewTip() {
        if (reviewTip != null) {
            reviewTip.dismiss();
        }
    }

    private void updateReivewTip() {
        // update review tips position
        hideReviewTip();
        showReviewTip();
    }

    private boolean shouldShowReviewTip() {
        return reviewEnabled && (reviewTip == null || !reviewTip.isShowing()) &&
                EduUtils.shouldShow(PROFILE_EDU_REVIEW_KEY, PROFILE_EDU_REVIEW_V);
    }

    private PopupWindow getVerifyTip() {
        if (verifyTip == null) {
            View popupView = getLayoutInflater().inflate(R.layout.popup_profile_verify_info, null);
            final int popupWidth = getResources().getDimensionPixelSize(R.dimen.profile_info_popup_width);
            verifyTip = new PopupWindow(popupView, popupWidth, ViewGroup.LayoutParams.WRAP_CONTENT, true);
            verifyTip.setBackgroundDrawable(getResources().getDrawable(android.R.color.transparent));

            popupView.findViewById(R.id.tip_bg).getBackground().setAlpha(TIP_BG_ALPHA);

            /** other Profile的图标介绍文案与my profile的文案不一样 */
            TextView avatarVerifyDesc = (TextView) popupView.findViewById(R.id.profile_verify_popup_avatar_txt);
            avatarVerifyDesc.setText(R.string.other_profile_verify_popup_avatar_desc);
            TextView emailVerifyDesc = (TextView) popupView.findViewById(R.id.profile_verify_popup_email_txt);
            emailVerifyDesc.setText(R.string.other_profile_verify_popup_email_desc);
            TextView facebookVerifyDesc = (TextView) popupView.findViewById(R.id.profile_verify_popup_fb_txt);
            facebookVerifyDesc.setText(R.string.other_profile_verify_popup_fb_desc);
            TextView phoneVerifyDesc = (TextView) popupView.findViewById(R.id.profile_verify_popup_phone_txt);
            phoneVerifyDesc.setText(R.string.other_profile_verify_popup_phone_desc);

            popupView.findViewById(R.id.btn_get_verified).setVisibility(View.GONE);             // other profile中不需要显示get verified按钮
        }

        return verifyTip;
    }

    @Override
    protected void onStart() {
        super.onStart();

        otherProfileReviewsStar.post(new Runnable() {
            @Override
            public void run() {
                showReviewTip();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        hideReviewTip();
        unsubsUserBlocking();

        if (verifyTip != null && verifyTip.isShowing()) {
            verifyTip.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        onTop = true;
        onIntent(getIntent());
    }

    @Override
    protected void onPause() {
        super.onPause();
        onTop = false;

        if (swipeRefreshView.isRefreshing()) {
            swipeRefreshView.setRefreshing(false);
            listView.onLoadMoreComplete(true, true);
        }

        // 防止leak activity
        hideReviewTip();
    }

    private void onIntent(Intent intent) {
        if (intent.hasExtra(ACT_PARA_USER)) {
            user = (USER) intent.getSerializableExtra(ACT_PARA_USER);
            userId = user.id;
            onUserInfoUpdated();
//            profileModel.getUserDetail(userId);
        }
        else {
            userId = intent.getStringExtra(ACT_PARA_USER_ID);
//            if (ModelUtils.isNotEmpty(userId)) {
//                profileModel.getUserDetail(userId);
//            }
        }

        doOnGlobalLayout(listView, new Runnable() {
            @Override
            public void run() {
                // wait layout complete in order to show the refreshing animation
                animateRefresh();
            }
        });
    }

    @Override
    public void onBackPressed(){
        if (mUnfollowDlg != null && mUnfollowDlg.getVisibility() != View.GONE){
            mUnfollowDlg.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    private void onUserInfoUpdated() {
        if (user == null) return;

        userId = user.id;
        currUserId = userId;

        title.setText(user.nick_name);
        showAvatar(user, avatar);

        boolean anchorToItemList = getIntent().getBooleanExtra("showSellingItems", false);
        if(anchorToItemList) {
            scrollToItems(null);
        }
    }

    private void showAvatar(USER user, AvatarView iv) {
        if (user == null) {
            return;
        }

        DisplayUtils.showAvatar(iv,
                user,
                getResources().getDimensionPixelSize(R.dimen.profile_avatar_size),
                FiveMilesApp.imageOptionsAvatar);
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
            throws JSONException {
        if(url.startsWith(ApiInterface.USER_DETAIL)) {
            if (jo == null) {
                if (status.getRestErrorCode() == ERR_BLOCKED) {
                    new AlertDialog.Builder(this)
                            .setMessage(R.string.user_blocked_info)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .setCancelable(false)
                            .show();
                }

                return;
            }

            user = profileModel.user_detail.userInfo; // instance of class com.insthub.fivemiles.Protocol.USER
            onUserInfoUpdated();
            setUserStats();
//            profileModel.getUserItems(userId);
            setFollow();
            setLocation();
            updateVerificationStatus(user);


            DisplayUtils.showCount(reviews, reviewsNum, user.reviewCount);
            DisplayUtils.showStars(user, otherProfileReviewsStar);

        } else if(url.startsWith(ApiInterface.USER_SELLINGS)) {
            swipeRefreshView.setRefreshing(false);
            listView.onLoadMoreComplete(jo != null, profileModel.hasMoreSellings());

            // show blank view
            if (profileModel.dataList.isEmpty()) {
                listView.setLoadingMoreVisible(false);
                blankView.setVisibility(View.VISIBLE);
            } else {
                blankView.setVisibility(View.GONE);
            }

//            displaySellerDistance();
            populateUserItems();
        } else if(url.startsWith(ApiInterface.FOLLOW)) {
            profileModel.user_detail.userInfo.followed = true;
            setFollow();
//			ToastView toast = new ToastView(this, getString(R.string.msg_follow_succeeded));
//            toast.setGravity(Gravity.CENTER, 0, 0);
//            toast.show();
            profileModel.getUserDetail(userId);

            TrackingUtils.trackTouch(VIEW_NAME, "follow");
        } else if(url.startsWith(ApiInterface.UNFOLLOW)) {
            profileModel.user_detail.userInfo.followed = false;
            setFollow();
//			ToastView toast = new ToastView(this, getString(R.string.msg_unfollow_succeeded));
//            toast.setGravity(Gravity.CENTER, 0, 0);
//            toast.show();
            profileModel.getUserDetail(userId);
        }
    }

    private void updateVerificationStatus(USER user) {
        boolean isEmailVerified = (user != null) && user.isEmailVerified;
        boolean isFacebookVerified = (user != null) && user.isFacebookVerified;
        boolean isPhoneVerified = (user != null) && user.isPhoneVerified;

        DisplayUtils.showEmailVerificationIcon(mVerifyEmailView, isEmailVerified);
        DisplayUtils.showFacebookVerificationIcon(mVerifyFacebookView, isFacebookVerified);
        DisplayUtils.showPhoneVerificationIcon(mVerifyPhoneView, isPhoneVerified);
    }



    private void setUserStats() {
        itemCount = profileModel.user_detail.items_count;
        followingCount = profileModel.user_detail.followings_count;
        followerCount = profileModel.user_detail.followers_count;

        DisplayUtils.showCount(items, items_count, itemCount);
        DisplayUtils.showCount(following, following_count, followingCount);
        DisplayUtils.showCount(followers, followers_count, followerCount);

    }

    private void setFollow() {
        if(isFollowed()) {
            followTitle.setText(R.string.profile_following);
            followTitle.setTextColor(Color.WHITE);
            followIcon.setImageResource(R.drawable.ic_following);
            follow.setBackgroundResource(R.drawable.button_rectangle_orange);
        } else {
            followTitle.setText(R.string.profile_action_follow);
            followTitle.setTextColor(getResources().getColor(R.color.fm_text_orange));
            followIcon.setImageResource(R.drawable.ic_follow);
            follow.setBackgroundResource(R.drawable.line_button_orange_light);
        }
    }

    private void setLocation() {
        String locationStr = profileModel.user_detail.place;
        if (ModelUtils.isNotEmpty(locationStr)){
            sellerPlaceWrapper.setVisibility(View.VISIBLE);
            tvSellerPlace.setText(locationStr);
        } else {
            sellerPlaceWrapper.setVisibility(View.GONE);
        }
    }

//    private void displaySellerDistance() {
//        HOME_OBJECTS latestPostedProduct = getLatestPostedProduct();
//        if (latestPostedProduct == null) {
//            location.setVisibility(View.GONE);
//            return;
//        }
//
//        String loc = formatLocation(latestPostedProduct.country,
//                latestPostedProduct.province, latestPostedProduct.city);
//
//        if (isNotEmpty(loc)) {
//            location.setVisibility(View.VISIBLE);
//            location_text.setText(loc);
//        } else if (latestPostedProduct.latitude > 0 || latestPostedProduct.longitude > 0) {
//            location.setVisibility(View.VISIBLE);
//            location_text.setText(formatDistance(latestPostedProduct.distance));
//        } else {
//            location.setVisibility(View.GONE);
//        }
//    }

    private HOME_OBJECTS getLatestPostedProduct() {
        if (profileModel.dataList.isEmpty())
            return null;
        List<HOME_OBJECTS> products = new ArrayList<HOME_OBJECTS>(profileModel.dataList);
        Collections.sort(profileModel.dataList, new Comparator<HOME_OBJECTS>() {
            @Override
            public int compare(HOME_OBJECTS p1, HOME_OBJECTS p2) {
                return -1 * ModelUtils.compare(p1.created_at, p2.created_at);
            }
        });
        return products.get(0);
    }

    private void populateUserItems() {
        userItems.clear();
        for (HOME_OBJECTS o : profileModel.dataList) {
            itemListAdapter.add(o.toDomain());
        }
        itemListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoadMore() {
        TrackingUtils.trackTouch(VIEW_NAME, "itsitem_loadmore");

        if(!profileModel.hasMoreSellings()) {
            listView.onLoadMoreComplete(true, false);
            return;
        }

        if (swipeRefreshView.isRefreshing()) {
            listView.onLoadMoreComplete();
            return;
        }

        profileModel.getUserItemsMore(userId);
    }

    @Override
    public void onPauseRendering() {
        imageLoader.pause();
    }

    @Override
    public void onResumeRendering() {
        imageLoader.resume();
    }

    @Override
    public void onScroll(boolean isAtTop, ScrollDirection scrollDirection) {
        swipeRefreshView.setEnabled(isAtTop);
        if (isAtTop) {
            showReviewTip();
        } else {
            hideReviewTip();
        }
    }

    @Override
    public void onRefresh() {
        if (ModelUtils.isNotEmpty(userId)) {
            profileModel.getUserDetail(userId);
            profileModel.getUserItems(userId);
            listView.onLoadMoreComplete();
        } else {
            swipeRefreshView.setRefreshing(false);
        }
    }

    @SuppressWarnings("unused")
    public void scrollToItems(View v) {
        listView.smoothScrollToPositionFromTop(listView.getHeaderViewsCount() + 2,
                profileWrapper.getMeasuredHeight());
    }

    // FIXME 修改View控件，Action View和Share分开
    @OnClick(R.id.top_view_share)
    void showActionMenu() {
        int blockRes = profileModel.userBlocked ? R.string.menu_unblock_person : R.string.menu_block_person;

        new PopupMenu(this)
                .addOptions(new PopupMenu.Option(MENU_REPORT, getString(R.string.menu_report_person)),
                        new PopupMenu.Option(MENU_BLOCK, getString(blockRes)))
                .setOnSelectionListener(new PopupMenu.OnSelectionListener() {
                    @Override
                    public void onOptionSelected(PopupMenu.Option option) {
                        onMenuOptionSelected(option);
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        TrackingUtils.trackTouch(VIEW_NAME, "sellermore_cancel");
                    }
                })
                .show();
        TrackingUtils.trackTouch(VIEW_NAME, "sellermore");
    }

    private void onMenuOptionSelected(PopupMenu.Option option) {
        switch (option.getId()) {
            case MENU_REPORT:
                reportUser();
                break;
            case MENU_BLOCK:
                blockUser();
                break;
        }
    }

    private void reportUser() {
        if (ModelUtils.isEmpty(userId)) {
            return;
        }

        startActivity(new Intent(this, ReportActivity.class)
                .putExtra(ACT_PARA_REPORT_TYPE, ReportRepository.REPORT_TYPE_USER)
                .putExtra(ACT_PARA_REPORTED_OID, userId));
        TrackingUtils.trackTouch(VIEW_NAME, "seller_reportseller");
    }

    private void unsubsUserBlocking() {
        if (subsUserBlocking != null) {
            subsUserBlocking.unsubscribe();
        }
    }

    private void blockUser() {
        if (ModelUtils.isEmpty(userId)) {
            return;
        }

        int blockRes = profileModel.userBlocked ? R.string.msg_alert_unblock_person : R.string.msg_alert_block_person;
        final String action = profileModel.userBlocked ? "seller_unblock" : "seller_block";

        // TODO use material-design dialog
        new AlertDialog.Builder(this)
                .setMessage(blockRes)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        doBlockUser();
                        TrackingUtils.trackTouch(VIEW_NAME, action + "yes");
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        TrackingUtils.trackTouch(VIEW_NAME, action + "no");
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        TrackingUtils.trackTouch(VIEW_NAME, action + "no");
                    }
                })
                .show();

        TrackingUtils.trackTouch(VIEW_NAME, action);
    }

    private void doBlockUser() {
        unsubsUserBlocking();  // 每次只能有一个block/unblock操作
        subsUserBlocking = profileModel.blockUser(this, userId, new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                boolean blocked = !profileModel.userBlocked;
                toast(blocked ? R.string.msg_person_blocked : R.string.msg_person_unblocked);
                profileModel.userBlocked = blocked;
            }
        });
    }

}
