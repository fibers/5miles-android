package com.insthub.fivemiles.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.external.eventbus.EventBus;
import com.external.maxwin.view.IXListViewListener;
import com.external.maxwin.view.XListView;
import com.insthub.fivemiles.Adapter.UserAdapter;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.FollowerModel;
import com.insthub.fivemiles.Model.ProfileModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.USER;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.main.listing.ListItemActivity;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_CAN_GO_BACK;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_FOLLOWER;

public class FollowerActivity extends BaseActivity implements IXListViewListener, BusinessResponse {

	private ImageView back;
	private TextView title;
	private XListView listView;
    private FrameLayout unfollowConfirmBtn;
    private UserAdapter userAdapter;
	private FollowerModel followerModel;
    private ProfileModel profileModel;

    private boolean isEmpty;  // in empty page mode

    private String userId, followUserId, trackingNamePrefix;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.common_list_view);
		userId = getIntent().getStringExtra("user_id");
        trackingNamePrefix = ModelUtils.isMe(userId) ? "myfollowers" : "sellerfollowers";
		back = (ImageView) findViewById(R.id.top_view_back);
		title = (TextView) findViewById(R.id.top_view_title);
		listView = (XListView) findViewById(R.id.list);
        unfollowConfirmBtn = (FrameLayout) findViewById(R.id.list_unfollow_wrapper);

        ButterKnife.inject(this);

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		title.setText(R.string.profile_followers);

		listView.setXListViewListener(this, 1);
		listView.setPullLoadEnable(false);
		listView.setRefreshTime();
        if (!EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().register(this);
        }

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int index = position - listView.getHeaderViewsCount();
                if (index < 0 || index >= followerModel.userList.size()) {
                    return;
                }

                USER user = followerModel.userList.get(index);
                if(ModelUtils.isMe(user)) {
					Intent intent = new Intent(FollowerActivity.this, TabProfileActivity.class);
                    intent.putExtra(ACT_PARA_CAN_GO_BACK, true);
					startActivity(intent);
				} else {
					Intent intent = new Intent(FollowerActivity.this, OtherProfileActivity.class);
                    intent.putExtra("user", user);
					startActivity(intent);
				}

                TrackingUtils.trackTouch(VIEW_FOLLOWER, "followerperson");

			}
		});

		followerModel  = new FollowerModel(this);
		followerModel.addResponseListener(this);

        profileModel = new ProfileModel(this);
        profileModel.addResponseListener(this);

        userAdapter = new UserAdapter(this, followerModel.userList,true);
        listView.setAdapter(userAdapter);

        initContent();

        TrackingUtils.trackView(VIEW_FOLLOWER);
	}

    private void initContent() {
        ViewGroup emptyView = (ViewGroup) findViewById(R.id.empty_view_placeholder);
        int count = getIntent().getIntExtra(FiveMilesAppConst.ACT_PARA_FOLLOWER_COUNT, 0);
        isEmpty = count == 0;

        if (!isEmpty) {
            listView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            return;
        }

        listView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);

        if (ModelUtils.isOwner(userId)) {
            showMyEmptyView(emptyView);
        } else {
            showDefaultEmptyView(emptyView);
        }
    }

    private void showDefaultEmptyView(ViewGroup parent) {
        getLayoutInflater().inflate(R.layout.default_empty_view, parent);
    }

    private void showMyEmptyView(ViewGroup parent) {
        View v = getLayoutInflater().inflate(R.layout.my_empty_view, parent);
        TextView txtDesc = (TextView) v.findViewById(R.id.empty_view_desc);
        TextView txtAction = (TextView) v.findViewById(R.id.empty_view_button);

        txtDesc.setText(R.string.hint_empty_follower_list_desc);
        txtAction.setText(R.string.empty_follower_list_action);

        txtAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FollowerActivity.this, ListItemActivity.class);
                startActivity(intent);
                finish();

                TrackingUtils.trackTouch(VIEW_FOLLOWER, "followers_empty");
            }
        });
    }

    public void onEvent(Object event) {
        Message message = (Message) event;
        USER user = (USER) message.obj;
        if(message.what == MessageConstant.PROFILE_FOLLOW_CLK) {
            profileModel.follow(user.id);
            TrackingUtils.trackTouch(VIEW_FOLLOWER, trackingNamePrefix + "_follow");
        } else if(message.what == MessageConstant.PROFILE_UNFOLLOW_CLK) {
            //需要follow时需要弹出确认框,确认后再操作.
            followUserId = user.id;
            unfollowConfirmBtn.setVisibility(View.VISIBLE);
//                profileModel.unfollow(user.id);
            TrackingUtils.trackTouch(VIEW_FOLLOWER, trackingNamePrefix + "_unfollow");
        }
    }

	@Override
	public void onRefresh(int id) {
		followerModel.getFollowerList(userId);
	}

    @Override
    protected void onResume() {
        super.onResume();
        if (!isEmpty) {
            listView.refreshNow();
        }
    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

	@Override
	public void onLoadMore(int id) {
		followerModel.getFollowerListMore(userId);

        TrackingUtils.trackTouch(OtherProfileActivity.VIEW_NAME, "itsfollowers_loadmore");
	}

	private void setDataAdapter() {
		if(userAdapter == null) {
			userAdapter = new UserAdapter(this, followerModel.userList, true);
			listView.setAdapter(userAdapter);
		} else {
			userAdapter.list = followerModel.userList;
			userAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {
		listView.stopRefresh();
		listView.stopLoadMore();
		if(url.startsWith(ApiInterface.USER_FOLLOWERS)) {
			setDataAdapter();
			if(ModelUtils.isNull(followerModel.meta.next)) {
				listView.setPullLoadEnable(false);
			} else {
				listView.setPullLoadEnable(true);
			}
		}else if(url.startsWith(ApiInterface.FOLLOW)) {
            //follow request finished.
            setFollow(jo.optString("follow_user_id"), true);
            setDataAdapter();
        } else if (url.startsWith(ApiInterface.UNFOLLOW)) {
            //unfollow request finished.
            setFollow(jo.optString("unfollow_user_id"), false);
            setDataAdapter();
        }
    }

    @OnClick(R.id.list_unfollow_detail)
    void unfollowing() {
        profileModel.unfollow(followUserId);
        unfollowConfirmBtn.setVisibility(View.GONE);
        TrackingUtils.trackTouch(VIEW_FOLLOWER, trackingNamePrefix+"_unfollowyes");
    }

    @OnClick(R.id.list_unfollow_wrapper)
    void cancelPop(){
        unfollowConfirmBtn.setVisibility(View.GONE);
        TrackingUtils.trackTouch(VIEW_FOLLOWER, trackingNamePrefix+"_unfollowno");
    }

    @OnClick(R.id.list_unfollow_cancel)
    void unfollowCancel() {
        unfollowConfirmBtn.setVisibility(View.GONE);
        TrackingUtils.trackTouch(VIEW_FOLLOWER, trackingNamePrefix+"_unfollowno");
    }

    private void setFollow(String userId, boolean status) {

        for (USER user : followerModel.userList) {
            if (user.id.equals(userId)) {
                user.following = status;
                break;
            }
        }
    }

}
