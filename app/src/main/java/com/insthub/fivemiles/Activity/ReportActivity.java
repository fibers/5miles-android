package com.insthub.fivemiles.Activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.insthub.fivemiles.Model.ReportModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.view.CheckableGroup;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.ReportReason;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.OnTextChanged;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REPORTED_OID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REPORT_TYPE;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_REPORT_ITEM;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_REPORT_REVIEW;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_REPORT_SELLER;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;
import static com.thirdrock.repository.ReportRepository.REPORT_TYPE_ITEM;
import static com.thirdrock.repository.ReportRepository.REPORT_TYPE_REVIEW;
import static com.thirdrock.repository.ReportRepository.REPORT_TYPE_USER;

public class ReportActivity extends BaseActivity implements BusinessResponse {

    private ImageView 		back;
	private TextView 		title;
	private ViewGroup reportReasonsContainer;
	private ReportModel 	reportModel;
    //	private boolean 		isReportUser;
//	private String itemId, userId;
    private CheckableGroup reasonGroup = new CheckableGroup();
    private EditText edtComment;

    private int reportType;
    private String reportedObjId;
    private TextView btnSend;

    private String screenName;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report);
        ButterKnife.inject(this);

        reportType = getIntent().getIntExtra(ACT_PARA_REPORT_TYPE, REPORT_TYPE_USER);
        reportedObjId = getIntent().getStringExtra(ACT_PARA_REPORTED_OID);

        back 		= (ImageView) 	findViewById(R.id.top_view_back);
		title 		= (TextView) 	findViewById(R.id.top_view_title);
		reportReasonsContainer = (ViewGroup) 	findViewById(R.id.report_list);

		back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                cancel();
            }
        });

        edtComment = (EditText) findViewById(R.id.txt_report_comment);

        ViewGroup topActWrapper = (ViewGroup) findViewById(R.id.top_view_action_wrapper);
        topActWrapper.setVisibility(View.VISIBLE);
        getLayoutInflater().inflate(R.layout.top_action_button_orange_line, topActWrapper);
        btnSend = (TextView) topActWrapper.findViewById(R.id.top_action_button);
        btnSend.setEnabled(false);
        btnSend.setText(R.string.send);
        btnSend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                doSend();
            }
        });

        reportModel = new ReportModel(this);
		reportModel.addResponseListener(this);

        switch (reportType) {
            case REPORT_TYPE_ITEM:
                title.setText(R.string.title_report_item);
                edtComment.setHint(R.string.hint_reason_report_item);
                reportModel.getItemReasons();
                screenName = VIEW_REPORT_ITEM;
                break;
            case REPORT_TYPE_REVIEW:
                title.setText(R.string.title_report_review);
                edtComment.setHint(R.string.hint_reason_report_review);
                reportModel.getReviewReasons();
                screenName = VIEW_REPORT_REVIEW;
                break;
            default:
                title.setText(R.string.title_report_seller);
                edtComment.setHint(R.string.hint_reason_report_seller);
                reportModel.getUserReasons();
                screenName = VIEW_REPORT_SELLER;
                break;
        }

        TrackingUtils.trackView(screenName);
    }

    private void cancel() {
        finish();
        trackCancel();
    }

    private void trackCancel() {
        // GA tracking
        switch (reportType) {
            case REPORT_TYPE_ITEM:
                TrackingUtils.trackTouch(VIEW_REPORT_ITEM, "reportitemcancel");
                break;
            case REPORT_TYPE_USER:
                TrackingUtils.trackTouch(VIEW_REPORT_SELLER, "reportsellercancel");
                break;
            case REPORT_TYPE_REVIEW:
                TrackingUtils.trackTouch(VIEW_REPORT_REVIEW, "reportreviewcancel");
                break;
            default:
                break;
        }
    }

    private void doSend() {
        String msg = edtComment.getText().toString().trim();
        int reasonId = -1;
        int checkedIndex = reasonGroup.getCheckedIndex();
        ReportReason reason = reportModel.reasonsList.get(checkedIndex);
        if (reason != null) {
            reasonId = reason.getId();
        }

        if (ModelUtils.isEmpty(reportedObjId)) {
            return;
        }

        switch (reportType) {
            case REPORT_TYPE_ITEM:
                reportModel.reportItem(reportedObjId, reasonId, msg);
                TrackingUtils.trackTouch(VIEW_REPORT_ITEM, "reportitemsend");
                break;
            case REPORT_TYPE_REVIEW:
                reportModel.reportReview(reportedObjId, reasonId, msg);
                TrackingUtils.trackTouch(VIEW_REPORT_REVIEW, "reportreviewrsend");
                break;
            default:
                reportModel.reportUser(reportedObjId, reasonId, msg);
                TrackingUtils.trackTouch(VIEW_REPORT_SELLER, "reportsellersend");
                break;
        }

        TrackingUtils.trackTouch(screenName, "report_send");
    }

    @Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {
		if(url.startsWith(ApiInterface.REPORT_REASONS)) {
			populateReasons();
		} else if (url.matches(".*/report_(bad_user|bad_item|review)/.*")) {
            if (jo == null) {
                return;
            }

            setResult(RESULT_OK);
            finish();
            DisplayUtils.toast(R.string.hint_report_done);
        }
	}
	
	private void populateReasons() {
        reportReasonsContainer.removeAllViews();
        reasonGroup.clear();

        ReportReason reason;
        for (int i = 0; i < reportModel.reasonsList.size(); i++) {
            View v = getLayoutInflater().inflate(R.layout.report_item, null);
            final TextView txtReason = (TextView) v.findViewById(R.id.report_text);
            reason = reportModel.reasonsList.get(i);
            if (reason != null) {
                txtReason.setText(reason.getReasonContent());
            } else {
                L.e("Reason should not be null!");
            }

            final CheckBox chkSwitch = (CheckBox) v.findViewById(R.id.report_item_switch);
            reasonGroup.add(chkSwitch);

            v.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    chkSwitch.toggle();
                    updateSendButton();
                }
            });

            if (i == reportModel.reasonsList.size() - 1) {
                // hide last divider
                v.findViewById(R.id.report_item_divider).setVisibility(View.INVISIBLE);
            }

            reportReasonsContainer.addView(v);
        }
	}

    @OnTextChanged(R.id.txt_report_comment)
    void updateSendButton() {
        String comment = edtComment.getText().toString().trim();
        int checkedIndex = reasonGroup.getCheckedIndex();
        btnSend.setEnabled(checkedIndex >= 0 || isNotEmpty(comment));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            cancel();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        trackCancel();
    }
}
