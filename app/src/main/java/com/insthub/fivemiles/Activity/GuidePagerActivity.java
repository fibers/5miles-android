package com.insthub.fivemiles.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.external.androidquery.callback.AbstractAjaxCallback;
import com.facebook.Settings;
import com.google.ads.conversiontracking.AdWordsConversionReporter;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.SESSION;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.thirdrock.domain.Campaign;
import com.thirdrock.domain.EnumShareType;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.main.home.AdHelper;
import com.thirdrock.fivemiles.main.home.CategoryHelper;
import com.thirdrock.fivemiles.main.profile.ProfileRouteActivity;
import com.thirdrock.fivemiles.util.DeepLinkHelper;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.Ignore;
import com.thirdrock.framework.util.rx.SimpleObserver;

import org.json.JSONObject;

import java.io.File;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.fabric.sdk.android.Fabric;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.schedulers.Schedulers;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SHOW_FB_LIKES;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_S_KEYWORD;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;

/**
 * 启动页，为了兼容旧版本app的启动快捷方式，保留GuidePagerActivity类名不变
 * Created by ywu on 14/12/5.
 */
public class GuidePagerActivity extends Activity {

    public static final int SPLASH_DURATION = 800;
    public static final int AD_DURATION = 2000;
    public static final int PROMOTE_LIKE_LAUNCH_COUNT = 5;  // 第几次启动时提示like facebook主页

    private static boolean shown;

    @InjectView(R.id.adPic)
    ImageView imgAdvertise;

    int shortAnimDur;

    private AdHelper adHelper;
    private CategoryHelper catgryHelper;
    private Campaign currCampaign;
    private final Action0 enterAppAction = new Action0() {
        @Override
        public void call() {
            enterApp();
        }
    };
    private final Action0 showAdAction = new Action0() {
        @Override
        public void call() {
            showAd();
        }
    };

    private final Observer<Campaign> adObserver = new SimpleObserver<Campaign>() {
        @Override
        public void onNext(Campaign campaign) {
            onCampaignLoaded(campaign);
        }
    };
    private DisplayImageOptions adImgOpts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        Settings.sdkInitialize(FiveMilesApp.appCtx);

        if(redirectIfNeed(getIntent())){
            finish();
            return;
        }

        setContentView(R.layout.activity_launch);
        ButterKnife.inject(this);

        shortAnimDur = getResources().getInteger(android.R.integer.config_shortAnimTime);
        adHelper = new AdHelper();
        catgryHelper = new CategoryHelper();

        adImgOpts = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.color.fm_background)
                .showImageForEmptyUri(R.drawable.ad_launch_default)
                .showImageOnFail(R.drawable.ad_launch_default)
                .cacheInMemory(false)
                .cacheOnDisk(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        AdWordsConversionReporter.reportWithConversionId(FiveMilesApp.appCtx,
                "963768496", "GO9DCP326VwQsOHHywM", "0.00", false);
    }

    @Override
    protected void onStart() {
        super.onStart();

        FiveMilesApp.getInstance().loadAppConfig();
        preloadCampaign();  // load campaign for next launch (in background)
        loadCategories();

        if (shown) {
            // don't show more than once per process
            enterApp();
            return;
        }

        shown = true;
        Observable.timer(SPLASH_DURATION, TimeUnit.MILLISECONDS, Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(showAdAction)
                .subscribe();

    }

    @Override
    protected void onStop() {
        super.onStop();
        SESSION.getInstance().saveLaunchCount();

        Branch.getInstance(getApplicationContext()).resetUserSession();
        L.d("branch link session closed.");
        AbstractAjaxCallback.cancel();  // cancel all running & pending (BeeFramework) ajax jobs
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // ignore back button
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void enterApp() {

        // 如果通过deeplink进来，可能很快跳转到其他activity，所以放在enterApp()而不是onStart()中init branch
        handleDeeplink();

        Intent intent = new Intent(this, WelcomeActivity.class);
        SESSION session = SESSION.getInstance();

        if (session.isAuth()) {
            session.launchCount += 1;
            L.d("launch count after login: %d", session.launchCount);
            if (session.launchCount == PROMOTE_LIKE_LAUNCH_COUNT) {
                // 登陆后打开app的次数达到PROMOTE_LIKE_LAUNCH_COUNT的时候，提示like facebook主页
                L.d("should promote facebook likes!");
                intent.putExtra(ACT_PARA_SHOW_FB_LIKES, true);
            }
        }

        startActivity(intent);
        finish();
    }

    private Campaign getCurrCampaign() {
        if (currCampaign == null) {
            currCampaign = adHelper.loadCurrCampaign();
        }

        return currCampaign;
    }

    private void showAd() {
        Campaign campaign = getCurrCampaign();  // load cached campaign

        if (campaign == null || !campaign.isActive()) {
            showDefaultAd();
        } else {
            File adPicFile = adHelper.determinePicturePath(campaign);
            L.d("pic: %s", adPicFile);
            if (adPicFile != null && adPicFile.exists()) {
                L.d("using ad pic: %s", adPicFile);
                imgAdvertise.setScaleType(ImageView.ScaleType.CENTER_CROP);

                ImageLoader.getInstance().displayImage(Uri.fromFile(adPicFile).toString(),
                        imgAdvertise, adImgOpts, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                imgAdvertise.setAlpha(0f);
                                imgAdvertise.animate()
                                        .alpha(1f)
                                        .start();
                            }
                        });
            } else {
                showDefaultAd();
            }
        }

        // enter the app after a certain duration
        Observable.timer(AD_DURATION, TimeUnit.MILLISECONDS, Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(enterAppAction)
                .subscribe();
    }

    private void showDefaultAd() {
        imgAdvertise.setScaleType(ImageView.ScaleType.FIT_XY);

        imgAdvertise.setImageResource(R.drawable.ad_launch_default);
        imgAdvertise.setAlpha(0f);
        imgAdvertise.animate()
                .alpha(1f)
                .start();
    }

    // load campaign for next launch (in background)
    private void preloadCampaign() {
        adHelper.loadAd().subscribe(adObserver);
    }

    // loading data (in background) for the next launch
    private void onCampaignLoaded(Campaign campaign) {
        Campaign currCampaign = getCurrCampaign();

        if (currCampaign == null || (campaign != null && currCampaign.getId() < campaign.getId())) {
            currCampaign = campaign;
        }

        if (currCampaign == null) {
            return;
        }

        // save campaign metadata & picture for the next launch
        adHelper.saveCampaign(currCampaign);

        File adPicFile = adHelper.determinePicturePath(currCampaign);
        if (adPicFile != null && !adPicFile.exists()) {
            float ratio = ((float) currCampaign.getImageWidth()) / currCampaign.getImageHeight();
            int w = imgAdvertise.getMeasuredWidth();
            int h = (int) Math.ceil(w / ratio);

            adHelper.downloadAdPicture(currCampaign, w, h).subscribe(Ignore.getInstance());
        }
    }

    // load categories for search
    private void loadCategories() {
        catgryHelper.loadCategories();
    }

    private boolean redirectIfNeed(Intent intent) {

        Uri uri = intent.getData();
        if (uri == null) {
            return false;
        }

        String host = uri.getHost();
        String path = uri.getPath();

        if(isNotEmpty(host) && isNotEmpty(path)){

            if(path.startsWith("/")){
                path = path.substring(1);
            }

            if( host.equals("item")){
                startActivity(new Intent(this, ItemActivity.class)
                        .setAction(Intent.ACTION_VIEW)
                        .putExtra(ACT_PARA_ITEM_ID, path));
                return true;
            }else if(host.equals("person")){
                startActivity(new Intent(this, ProfileRouteActivity.class)
                        .setAction(Intent.ACTION_VIEW)
                        .putExtra(ACT_PARA_USER_ID, path));
                return true;
            }else if(host.equals("search")){
                startActivity(new Intent(this, SearchResultActivity.class).
                        setAction(Intent.ACTION_VIEW).
                        putExtra(ACT_PARA_S_KEYWORD, path));

                return true;
            }
        }

        // Handle facebook deeplink
//        Uri targetURL = AppLinks.getTargetUrlFromInboundIntent(this, intent);
//
//        if(targetURL != null){
//            Set<String> parameterNames = targetURL.getQueryParameterNames();
//
//            if(parameterNames.contains("item")){
//
//                startActivity(new Intent(this, ItemActivity.class)
//                        .setAction(Intent.ACTION_VIEW)
//                        .putExtra(ACT_PARA_ITEM_ID, targetURL.getQueryParameter("item")));
//                return true;
//
//            }else if(parameterNames.contains("person")){
//
//                startActivity(new Intent(this, ProfileRouteActivity.class)
//                        .setAction(Intent.ACTION_VIEW)
//                        .putExtra(ACT_PARA_USER_ID, targetURL.getQueryParameter("person")));
//                return true;
//
//            }else if(parameterNames.contains("search")){
//
//                startActivity(new Intent(this, SearchResultActivity.class).
//                        setAction(Intent.ACTION_VIEW).
//                        putExtra(ACT_PARA_S_KEYWORD, targetURL.getQueryParameter("search")));
//
//                return true;
//            }
//        }else {
//
//            Uri uri = intent.getData();
//            if (uri == null) {
//                return false;
//            }
//
//            String host = uri.getHost();
//            String path = uri.getPath();
//
//            L.d("redirect uri %s %s", host, path);
//
//            if (isNotEmpty(host) && host.matches("(www\\.)?5milesapp.com")) {
//
//                Matcher matcherItem = ItemActivity.LINK_PATTERN.matcher(path);
//                Matcher matcherPerson = ProfileRouteActivity.LINK_PATTERN.matcher(path);
//
//                if(matcherItem.matches()) {
//                    String itemID = matcherItem.group(1);
//                    startActivity(new Intent(this, ItemActivity.class)
//                            .setAction(Intent.ACTION_VIEW)
//                            .putExtra(ACT_PARA_ITEM_ID, itemID));
//                    return true;
//                } else if (matcherPerson.matches()){
//                    String userID = matcherItem.group(1);
//                    startActivity(new Intent(this, ItemActivity.class)
//                            .setAction(Intent.ACTION_VIEW)
//                            .putExtra(ACT_PARA_USER_ID, userID));
//                    return true;
//                }
//            }
//        }

        return false;
    }

    /**
     * 初始化一个Branch的Session，获取referring param。
     * 因为增加了匿名用户，所以可以在params返回后，直接跳转。
     */
    private void handleDeeplink() {
        Uri data = this.getIntent().getData();
        Branch.BranchReferralInitListener listener = new FMBranchReferInitListener();
        Branch.getInstance(FiveMilesApp.appCtx).initSession(listener, data);
    }

    private static class FMBranchReferInitListener implements Branch.BranchReferralInitListener {

        @Override
        public void onInitFinished(JSONObject referringParams, BranchError branchError) {
            if (branchError != null) {        // 出错了
                L.i("branch link init error " + branchError.getMessage());
            } else {
                L.d("branch link initiated and referring params is: " + referringParams);
            }

            if (SESSION.getInstance().isAuth()) {
                DeepLinkHelper.handleDeepLink(referringParams);
            } else {
                FiveMilesApp.getInstance().storeReferringParams(referringParams);
            }

            // 获取用于邀请好友的Branch short url
            if (ModelUtils.isEmpty(SESSION.getInstance().homeBranchUrl)) {

                DeepLinkHelper deepLinkHelper = new DeepLinkHelper(FiveMilesApp.appCtx);
                String userId = SESSION.getInstance().userId;
                SESSION.getInstance().homeBranchUrl = deepLinkHelper.generateBranchShortUrl(
                        userId, EnumShareType.INVITE, null, null);
            }
        }
    }
}
