package com.insthub.fivemiles.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.external.eventbus.EventBus;
import com.external.maxwin.view.IXListViewListener;
import com.external.maxwin.view.XListView;
import com.insthub.fivemiles.Adapter.UserAdapter;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.FollowingModel;
import com.insthub.fivemiles.Model.ProfileModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.USER;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.reco.SellersNearbyActivity;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_BACK_ENABLED;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_CAN_GO_BACK;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_FOLLOWING;

public class FollowingActivity extends BaseActivity implements IXListViewListener, BusinessResponse {

    private TextView title;
	private XListView listView;
    private FrameLayout unfollowConfirmBtn;
	private UserAdapter userAdapter;
	private FollowingModel followingModel;
    private ProfileModel profileModel;

    private boolean isEmpty;  // in empty page mode

    private String userId, followUserId, trackingNamePrefix;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.common_list_view);
		userId = getIntent().getStringExtra("user_id");
        trackingNamePrefix = ModelUtils.isMe(userId) ? "myfollowing" : "sellerfollowing";
        ImageView back = (ImageView) findViewById(R.id.top_view_back);
		title = (TextView) findViewById(R.id.top_view_title);
		listView = (XListView) findViewById(R.id.list);
        unfollowConfirmBtn = (FrameLayout) findViewById(R.id.list_unfollow_wrapper);

        ButterKnife.inject(this);
		
		back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                // TODO 整理逻辑
                TrackingUtils.trackTouch(trackingNamePrefix + "_view", trackingNamePrefix + "_back");
            }
        });
		
		title.setText(R.string.profile_following);
		
		listView.setXListViewListener(this, 1);
		listView.setPullLoadEnable(false);
		listView.setRefreshTime();
        if (!EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().register(this);
        }
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int index = position - listView.getHeaderViewsCount();
                if (index < 0 || index >= followingModel.userList.size()) {
                    return;
                }

                USER user = followingModel.userList.get(index);
                if(ModelUtils.isMe(user)) {
					Intent intent = new Intent(FollowingActivity.this, TabProfileActivity.class);
                    intent.putExtra(ACT_PARA_CAN_GO_BACK, true);
					startActivity(intent);
				} else {
					Intent intent = new Intent(FollowingActivity.this, OtherProfileActivity.class);
					intent.putExtra("user", user);
					startActivity(intent);
				}
                TrackingUtils.trackTouch(VIEW_FOLLOWING, "followingperson");
			}
		});
		
		followingModel  = new FollowingModel(this);
		followingModel.addResponseListener(this);

        profileModel = new ProfileModel(this);
        profileModel.addResponseListener(this);

        userAdapter = new UserAdapter(this, followingModel.userList, true);
        listView.setAdapter(userAdapter);

        isEmpty = 0 == getIntent().getIntExtra(FiveMilesAppConst.ACT_PARA_FOLLOWING_COUNT, 0);
        showContent();

        TrackingUtils.trackView(VIEW_FOLLOWING);
	}

    private void showContent() {
        ViewGroup emptyView = (ViewGroup) findViewById(R.id.empty_view_placeholder);

        if (!isEmpty) {
            listView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            return;
        }

        listView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);

        if (ModelUtils.isOwner(userId)) {
            showMyEmptyView(emptyView);
        } else {
            showDefaultEmptyView(emptyView);
        }
    }

    private void showDefaultEmptyView(ViewGroup parent) {
        getLayoutInflater().inflate(R.layout.default_empty_view, parent);
    }

    private void showMyEmptyView(ViewGroup parent) {
        View v = getLayoutInflater().inflate(R.layout.my_empty_view, parent);
        TextView txtDesc = (TextView) v.findViewById(R.id.empty_view_desc);
        TextView txtAction = (TextView) v.findViewById(R.id.empty_view_button);

        txtDesc.setText(R.string.hint_empty_following_list_desc);
        txtAction.setText(R.string.empty_view_action_go_explore);

        txtAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FollowingActivity.this, SellersNearbyActivity.class)
                        .putExtra(ACT_PARA_BACK_ENABLED, true);
                startActivity(intent);
                finish();

                TrackingUtils.trackTouch(VIEW_FOLLOWING, "following_empty");
            }
        });
    }

    public void onEvent(Object event) {
        Message message = (Message) event;
        USER user;
        if(message.what == MessageConstant.PROFILE_FOLLOW_CLK) {
            user = (USER) message.obj;
            profileModel.follow(user.id);
            TrackingUtils.trackTouch(VIEW_FOLLOWING, trackingNamePrefix + "_follow");
        } else if(message.what == MessageConstant.PROFILE_UNFOLLOW_CLK) {
            //需要follow时需要弹出确认框,确认后再操作.
            user = (USER) message.obj;
            followUserId = user.id;
            unfollowConfirmBtn.setVisibility(View.VISIBLE);
//                profileModel.unfollow(user.id);
            TrackingUtils.trackTouch(VIEW_FOLLOWING, trackingNamePrefix + "_unfollow");
        }

    }

    @Override
	public void onRefresh(int id) {
		followingModel.getFollowingList(userId);
	}

    @Override
    protected void onResume() {
        super.onResume();
        if (!isEmpty) {
            listView.refreshNow();
        }
    }
    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
	public void onLoadMore(int id) {
		followingModel.getFollowingListMore(userId);

        TrackingUtils.trackTouch(OtherProfileActivity.VIEW_NAME, "itsfollowing_loadmore");
	}
	
	private void setDataAdapter() {
		if(userAdapter == null) {
			userAdapter = new UserAdapter(this, followingModel.userList, true);
			listView.setAdapter(userAdapter);
		} else {
			userAdapter.list = followingModel.userList;
			userAdapter.notifyDataSetChanged();
		}

        isEmpty = userAdapter.list.isEmpty();
        showContent();
	}
	
	@Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {
		listView.stopRefresh();
		listView.stopLoadMore();
		if(url.startsWith(ApiInterface.USER_FOLLOWLING)) {
			setDataAdapter();
			if(ModelUtils.isNull(followingModel.meta.next)) {
				listView.setPullLoadEnable(false);
			} else {
				listView.setPullLoadEnable(true);
			}
		}else if(url.startsWith(ApiInterface.FOLLOW)) {
            //follow request finished.
            setFollow(jo.optString("follow_user_id"), true);
            setDataAdapter();
        } else if (url.startsWith(ApiInterface.UNFOLLOW)) {
            //unfollow request finished.
            setFollow(jo.optString("unfollow_user_id"), false);
            setDataAdapter();
        }
	}

    @OnClick(R.id.list_unfollow_detail)
    void unfollowing(){
        profileModel.unfollow(followUserId);
        unfollowConfirmBtn.setVisibility(View.GONE);
        TrackingUtils.trackTouch(VIEW_FOLLOWING, trackingNamePrefix + "_unfollowyes");
    }

    @OnClick(R.id.list_unfollow_wrapper)
    void cancelPop(){
        unfollowConfirmBtn.setVisibility(View.GONE);
        TrackingUtils.trackTouch(VIEW_FOLLOWING, trackingNamePrefix + "_unfollowno");
    }

    @OnClick(R.id.list_unfollow_cancel)
    void unfollowCancel(){
        unfollowConfirmBtn.setVisibility(View.GONE);
        TrackingUtils.trackTouch(VIEW_FOLLOWING, trackingNamePrefix + "_unfollowno");
    }


    private void setFollow(String userId, boolean status) {

        for (USER user : followingModel.userList) {
            if (user.id.equals(userId)) {
                user.following = status;
                break;
            }
        }
    }
}
