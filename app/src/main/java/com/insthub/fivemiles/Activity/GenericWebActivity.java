package com.insthub.fivemiles.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.main.profile.ProfileRouteActivity;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.ui.widget.ProgressBarIndeterminate;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_PAGE_TITLE;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_PAGE_URL;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_TOP_SHARE_TEXT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SHOW_SHARE;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;

/**
 * Display information about the app, through a web view.
 */
public class GenericWebActivity extends BaseActivity {

    private WebView webView;
    private ProgressBarIndeterminate prgLoading;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_web);

        String pageTitle = getIntent().getStringExtra(ACT_PARA_PAGE_TITLE);
        String pageUrl = getIntent().getStringExtra(ACT_PARA_PAGE_URL);

        ImageView back = (ImageView) findViewById(R.id.top_view_back);
        TextView title = (TextView) findViewById(R.id.top_view_title);

        if (isNotEmpty(pageTitle)) {
            title.setVisibility(View.VISIBLE);
            title.setText(pageTitle);
        } else {
            title.setVisibility(View.GONE);
            findViewById(R.id.top_view_logo).setVisibility(View.VISIBLE);
        }

        prgLoading = (ProgressBarIndeterminate) findViewById(R.id.prg_loading);
        webView = (WebView) findViewById(R.id.webview_webView);
        webView.getSettings().setJavaScriptEnabled(true);
        Map<String, String> extraHeaders = new HashMap<>(1);
        extraHeaders.put("Accept-Language", Locale.getDefault().getLanguage());
        webView.loadUrl(pageUrl, extraHeaders);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Uri uri = Uri.parse(url);
                Set<String> parameterNames = uri.getQueryParameterNames();
                if (url.matches("^https?://(www\\.)?5milesapp\\.com.*")) {

                    if (parameterNames.contains("target")
                            && uri.getQueryParameter("target").equals("_blank")) {

                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(uri);
                        startActivity(intent);
                        return true;
                    } else {
                        // 暂时交给系统处理，以便跳转app
                        if (handleInAppUri(uri)) {
                            if (!webView.canGoBack()) {
                                finish();
                            }
                            return true;
                        }
                    }
                }

                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                prgLoading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                prgLoading.setVisibility(View.GONE);
            }

        });

        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        doOnNewIntent(getIntent());
    }

    private void doOnNewIntent(Intent intent) {
        if (intent == null) {
            return;
        }

        View ivShareBtn = findViewById(R.id.top_view_share);
        boolean shouldShowShare = intent.getBooleanExtra(ACT_PARA_SHOW_SHARE, false);
        if (shouldShowShare) {
            String shareContentTxt = intent.getStringExtra(ACT_PARA_TOP_SHARE_TEXT);
            if (ModelUtils.isEmpty(shareContentTxt)) {
                shareContentTxt = getString(R.string.share_app);
            }
            final String finalShareContentTxt = shareContentTxt;

            ivShareBtn.setVisibility(View.VISIBLE);
            ivShareBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareApp(finalShareContentTxt);
                }
            });
        } else {
            ivShareBtn.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        doOnNewIntent(intent);
    }

    private boolean handleInAppUri(Uri uri) {
        String path = uri.getPath();

        if (isEmpty(path)) {
            return false;
        }

        Class handlerClz = null;
        if (path.matches(ItemActivity.LINK_PATTERN.pattern())) {
            handlerClz = ItemActivity.class;

        } else if (path.matches(ProfileRouteActivity.LINK_PATTERN.pattern())) {
            handlerClz = ProfileRouteActivity.class;
        }

        if (handlerClz != null) {
            startActivity(new Intent(this, handlerClz)
                    .setAction(Intent.ACTION_VIEW)
                    .setData(uri));
            return true;
        }

        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            if (webView.canGoBack()) {
                webView.goBack();
                return true;
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected boolean isLoginProtected() {
        return false;
    }

    // TODO 重构为工具方法
    private void shareApp(String pText) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND)
                .setType("text/plain")
                .putExtra(Intent.EXTRA_TEXT, pText);
        startActivity(Intent.createChooser(shareIntent, "Share To"));
    }

}
