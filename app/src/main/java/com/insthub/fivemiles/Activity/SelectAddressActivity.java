package com.insthub.fivemiles.Activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.thirdrock.fivemiles.R;
import com.insthub.fivemiles.Adapter.AddressAdapter;
import com.insthub.fivemiles.Model.CountryModel;
import com.insthub.fivemiles.Protocol.ApiInterface;

public class SelectAddressActivity extends BaseActivity implements BusinessResponse {

	private ImageView back;
	private TextView title;
	private ListView listView;
	private CountryModel countryModel;
	private AddressAdapter addressAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_address);
		
		back = (ImageView) findViewById(R.id.top_view_back);
		title = (TextView) findViewById(R.id.top_view_title);
		listView = (ListView) findViewById(R.id.address_listview);
		
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		title.setText(R.string.select_country);
		
		countryModel = new CountryModel(this);
		countryModel.addResponseListener(this);
		countryModel.getCountryDialcodesList();
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.putExtra("country_name", countryModel.countryList.get(position).country_name);
				intent.putExtra("dial_code", countryModel.countryList.get(position).dial_code);
				setResult(Activity.RESULT_OK, intent); 
				finish();
			}
		});
		
	}
	@Override
	public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
			throws JSONException {
		// TODO Auto-generated method stub
		if(url.startsWith(ApiInterface.COUNTRY_DIALCODES)) {
			setCountryAdapter();
		}
		
	}
	
	private void setCountryAdapter() {
		addressAdapter = new AddressAdapter(this, countryModel.countryList);
		listView.setAdapter(addressAdapter);
	}
	
}
