package com.insthub.fivemiles.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.InputFilter;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.external.eventbus.EventBus;
import com.insthub.fivemiles.Adapter.FilterOptionRenderer;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.SearchModel;
import com.insthub.fivemiles.Protocol.USER;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.pedrogomez.renderers.AdapteeCollection;
import com.pedrogomez.renderers.RendererAdapter;
import com.thirdrock.domain.CategoryInfo;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.User;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.common.item.ItemThumbRenderer;
import com.thirdrock.fivemiles.common.item.ListItemRenderer;
import com.thirdrock.fivemiles.framework.view.NumberInputFilter;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.main.home.Banner;
import com.thirdrock.fivemiles.search.CategoryViewModel;
import com.thirdrock.fivemiles.search.FilterSubCategoriesRenderer;
import com.thirdrock.fivemiles.search.FilterTopCategoriesRenderer;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.ui.helper.SlidingUpPanelHelper;
import com.thirdrock.framework.ui.renderer.MonoRendererBuilder;
import com.thirdrock.framework.ui.renderer.SimpleAdapteeCollection;
import com.thirdrock.framework.ui.widget.WaterfallListView;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.Utils;
import com.thirdrock.framework.util.animation.SimpleAnimationListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnFocusChange;
import butterknife.OnItemClick;
import butterknife.OnTextChanged;
import butterknife.OnTouch;

import static android.view.animation.Animation.RELATIVE_TO_SELF;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_CAN_GO_BACK;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_RF_TAG;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_S_BRAND_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_S_BRAND_NAME;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_S_CAMPAIGN_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_S_KEYWORD;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_CLOSED_SEARCH_AD;
import static com.insthub.fivemiles.Model.SearchModel.EnumFilter.DISTANCE;
import static com.insthub.fivemiles.Model.SearchModel.EnumFilter.SORTING;
import static com.insthub.fivemiles.Protocol.ApiInterface.AD_BANNER;
import static com.insthub.fivemiles.Protocol.ApiInterface.SEARCH_BRAND;
import static com.thirdrock.fivemiles.util.DisplayUtils.formatPriceDigits;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;
import static com.thirdrock.fivemiles.util.TrackingUtils.trackTouch;
import static com.thirdrock.framework.ui.widget.WaterfallListView.OnScrollListener.ScrollDirection;
import static com.thirdrock.framework.util.Utils.doOnGlobalLayout;
import static com.thirdrock.framework.util.Utils.doOnLayoutChange;

public class SearchResultActivity extends BaseActivity implements BusinessResponse,
        WaterfallListView.Callback, SwipeRefreshLayout.OnRefreshListener {

    private static final int Q_KEYWORD = 1;
    private static final int Q_CATEGORY = 2;
    private static final int Q_BRAND = 3;
    private static final String SEARCH_BY_TEXT_VIEW_NAME = "searchresult_view";
    private static final String SEARCH_BY_CATEGORY_VIEW_NAME = "categoryresult_view";
    private static final String PRODUCT_BRAND_VIEW_NAME = "productbrand_view";

    public static final String VIEW_NAME = "discovery_view";

    private static final float MAX_FILTER_ROWS = 12f;

    private ImageView back;
    private TextView title;

    private ImageView searchOption;

    @InjectView(R.id.search_bar)
    View searchBar;

    @InjectView(R.id.search_edit)
    EditText edtSearch;

    @InjectView(R.id.iv_clear_search_result)
    ImageView ivClear;

    @InjectView(R.id.swipe_refresh_view)
    SwipeRefreshLayout swipeRefreshLayout;

    @InjectView(R.id.search_result_list)
    WaterfallListView listView;

    @InjectView(R.id.filter_bar)
    View filterCtrlBar;

    @InjectView(R.id.filter_layer)
    View filterLayer;

    @InjectView(R.id.filter_content_pane)
    View filterContentPane;

    @InjectView(R.id.ic_drawer_handle)
    View icDrawerHandle;

    @InjectView(R.id.cbx_filter_distance)
    CheckBox cbxFilterDistance;

    @InjectView(R.id.cbx_filter_category)
    CheckBox cbxFilterCategory;

    @InjectView(R.id.cbx_filter_sort)
    CheckBox cbxSorting;

    @InjectView(R.id.cbx_filter_misc)
    CheckBox cbxFilterMisc;

    @InjectView(R.id.filter_opts_pane)
    View filterOptsPane;

    @InjectView(R.id.misc_filter_pane)
    View miscFilterPane;

    @InjectView(R.id.lst_filter_opts)
    ListView lstFilterOpts;

    @InjectView(R.id.lst_filter_opts_top_categories)
    ListView lstFilterOptsTopCatgories;

    @InjectView(R.id.lst_filter_opts_sub_categories)
    ListView lstFilterOptsSubCatgories;

    @InjectView(R.id.cbx_filter_verified_only)
    CheckBox cbxFilterVerifiedOnly;

    @InjectView(R.id.edt_filter_price1)
    EditText edtPriceFilter1;

    @InjectView(R.id.edt_filter_price2)
    EditText edtPriceFilter2;

    @InjectView(R.id.blank_view)
    View blankView;

    private View headerPlaceholder;
    private boolean animatingFilterCtrlBar;
    private int searchMode = Q_KEYWORD;
    private SlidingUpPanelHelper slidingFilterHelper;
    private Banner bannerUtil;

    private final Runnable headerPlaceholderUpdater = new Runnable() {
        @Override
        public void run() {
            headerPlaceholder.setVisibility(filterCtrlBar.getVisibility());

            ViewGroup.LayoutParams params = headerPlaceholder.getLayoutParams();
            params.height = filterCtrlBar.getMeasuredHeight();
            headerPlaceholder.setLayoutParams(params);
        }
    };

    private boolean switchingFilterPane;
    private Runnable postFilterCloseAction;
    private View currentFilterOpt;

    private GestureDetector gestureDetector;

    private final GestureDetector.OnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener() {
//        @Override
//        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
//            L.v("onFling ... %f", velocityY);
//            onScroll(velocityY > 0, velocityY);
//            return false;
//        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            onScroll(distanceY < 0, distanceY);
            return false;
        }

        private void onScroll(boolean scrollingUp, float v) {
            if (v == 0) {
                return;
            }

            animateFilterCtrlBar(scrollingUp);
        }
    };

    private static enum SearchOption {
        LIST_MODE,
        GRID_MODE
    }

    private SearchOption searchOptionValue = SearchOption.GRID_MODE;

    private String search;
    private String searchTitle;
    private int searchId;
    private boolean isCategorySearch;

    private String rfTag;

    //	private HomeAdapter     listAdapter;
    private SearchModel searchModel;
    private RendererAdapter<Item> gridAdapter;                  // grid view中使用的是itemthumb, Item是ItemThumb的子类，所以也可以传过去
    private RendererAdapter<Item> listAdapter;
    private AdapteeCollection<Item> itemCollForGrid;
    private AdapteeCollection<Item> itemCollForList;

    // filter options
    private AdapteeCollection<SearchModel.Option> filterOpts;
    private BaseAdapter filterOptAdapter;

    private AdapteeCollection<CategoryInfo> filterTopCategories;
    private RendererAdapter<CategoryInfo> filterTopCategoriesAdapter;

    private AdapteeCollection<CategoryInfo> filterSubCategories;
    private RendererAdapter<CategoryInfo> filterSubCategoriesAdapter;

    private String currScreenName;
    private String currScreenActionPrefix;

    private Map<Integer, List<CategoryInfo>> subCategoriesMap = new HashMap<Integer, List<CategoryInfo>>();
    private Map<Integer, CategoryInfo> allCategoriesMap = new HashMap<Integer, CategoryInfo>();
    private List<CategoryInfo> allCategoriesList = new ArrayList<CategoryInfo>();

    private int selectedTopCategoryId;
    private int selectedSubCategoryId;

    private int currentTopCategoryPosition;
    private int currentSubCategoryPosition;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_result);

        ButterKnife.inject(this);

        gestureDetector = new GestureDetector(this, gestureListener);

        back = (ImageView) findViewById(R.id.top_view_back);
        title = (TextView) findViewById(R.id.top_view_title);

        searchOption = (ImageView) findViewById(R.id.search_option);

        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                trackTouch(currScreenName, currScreenActionPrefix + "back");
            }
        });

        searchModel = new SearchModel(this);
        searchModel.addResponseListener(this);


        initListView();

        // 首次加载数据，模拟下拉刷新
        doOnGlobalLayout(listView, new Runnable() {
            @Override
            public void run() {
                // wait layout complete in order to show the refreshing animation
                animateRefresh();
            }
        });

        if (!EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().register(this);
        }

        initFilterPane();

        onNewIntent(getIntent());

        initCategoriesData();

        TrackingUtils.trackView(currScreenName);
    }

    private void initFilterPane() {
        filterOpts = new SimpleAdapteeCollection<>();
        filterOptAdapter = new RendererAdapter<>(
                getLayoutInflater(),
                new MonoRendererBuilder(new FilterOptionRenderer()),
                filterOpts
        );
        lstFilterOpts.setAdapter(filterOptAdapter);

        filterTopCategories = new SimpleAdapteeCollection<CategoryInfo>();
        FilterTopCategoriesRenderer topCategoriesRenderer = new FilterTopCategoriesRenderer(this);
        filterTopCategoriesAdapter = new RendererAdapter<CategoryInfo>(
                getLayoutInflater(),
                new MonoRendererBuilder(topCategoriesRenderer),
                filterTopCategories
        );
        lstFilterOptsTopCatgories.setAdapter(filterTopCategoriesAdapter);

        filterSubCategories = new SimpleAdapteeCollection<CategoryInfo>();
        FilterSubCategoriesRenderer subCategoriesRenderer = new FilterSubCategoriesRenderer(this);
        filterSubCategoriesAdapter = new RendererAdapter<CategoryInfo>(
                getLayoutInflater(),
                new MonoRendererBuilder(subCategoriesRenderer),
                filterSubCategories
        );
        lstFilterOptsSubCatgories.setAdapter(filterSubCategoriesAdapter);

        cbxFilterVerifiedOnly.setChecked(false);

        edtPriceFilter1.setFilters(new InputFilter[]{
                new NumberInputFilter(0, 9999999)
        });

        edtPriceFilter2.setFilters(new InputFilter[]{
                new NumberInputFilter(0, 9999999)
        });

        slidingFilterHelper = new SlidingUpPanelHelper(filterContentPane, R.id.filter_drawer_handle);
        slidingFilterHelper.setOnPanelStateListener(new SlidingUpPanelHelper.OnPanelStateListener() {
            @Override
            public void onPanelExpanded() {

            }

            @Override
            public void onPanelCollapsed() {
                onFilterPaneClosed();
            }
        });
    }

    private void initListView() {
        listView.setCallback(this);
        bannerUtil = new Banner(this, PREF_KEY_CLOSED_SEARCH_AD);

        View header = getLayoutInflater().inflate(R.layout.home_banner_placeholder, null);
        headerPlaceholder = header.findViewById(R.id.placeholder);
        headerPlaceholder.setVisibility(View.GONE);
        listView.addHeaderView(header);
        listView.addHeaderView(bannerUtil.getBanner());

        DisplayUtils.setProgressColorScheme(swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setProgressViewOffset(true,
                getResources().getDimensionPixelSize(R.dimen.swipe_refresh_progress_offset_start),
                getResources().getDimensionPixelSize(R.dimen.swipe_refresh_progress_offset_end));

        itemCollForList = new SimpleAdapteeCollection<>();
        listAdapter = new RendererAdapter<>(
                getLayoutInflater()
                , new MonoRendererBuilder(new ListItemRenderer(getApplicationContext()))
                , itemCollForList);

        itemCollForGrid = new SimpleAdapteeCollection<>();
        gridAdapter = new RendererAdapter<>(
                getLayoutInflater(),
                new MonoRendererBuilder(new ItemThumbRenderer()),
                itemCollForGrid);

        listView.setAdapter(gridAdapter);
    }


    private void initCategoriesData(){

        allCategoriesList = CategoryViewModel.getLocalCategories();
        for(CategoryInfo category : allCategoriesList){
            Integer categoryId = Integer.valueOf(category.getId());
            List<CategoryInfo> subCategories = category.getChildren();

            subCategoriesMap.put(categoryId, subCategories);
            allCategoriesMap.put(categoryId, category);

            for(CategoryInfo subCategory : subCategories){
                Integer subCategoryId = Integer.valueOf(subCategory.getId());
                allCategoriesMap.put(subCategoryId, subCategory);
            }
        }
    }

    @OnItemClick(R.id.lst_filter_opts_top_categories)
    public void onItemClickTopCategory(AdapterView<?> parent, View view, int position, long id) {

        CategoryInfo topCategory = filterTopCategories.get(position);
        if(currentTopCategoryPosition == position){
            setSelection(lstFilterOptsSubCatgories, currentSubCategoryPosition);
        }else{
            lstFilterOptsSubCatgories.setItemChecked(currentSubCategoryPosition, false);
        }

        selectedTopCategoryId = topCategory.getId();

        if(selectedTopCategoryId == CategoryInfo.ID_ALL_CATEGORY ){
            searchModel.searchFilter.setCategory(CategoryInfo.ID_ALL_CATEGORY);
            searchModel.searchFilter.setRootCategory(CategoryInfo.ID_ALL_CATEGORY);

            filterSubCategories.clear();
            filterSubCategoriesAdapter.notifyDataSetChanged();

            animateRefresh();

        }else {
            filterSubCategories.clear();

            if(searchMode == Q_CATEGORY){
                filterSubCategories.addAll(subCategoriesMap.get(topCategory.getId()));
            }else if(searchMode == Q_KEYWORD){

                CategoryInfo categoryAllForTopCategory = new CategoryInfo();
                categoryAllForTopCategory.setId(CategoryInfo.ID_ALL_CATEGORY);
                categoryAllForTopCategory.setTitle(getString(R.string.category_option_all)
                        + " for " + topCategory.getTitle());
                filterSubCategories.add(categoryAllForTopCategory);
                filterSubCategories.addAll(subCategoriesMap.get(topCategory.getId()));
            }

            filterSubCategoriesAdapter.notifyDataSetChanged();
        }

    }

    @OnItemClick(R.id.lst_filter_opts_sub_categories)
    public void onItemClickSubCategory(AdapterView<?> parent, View view, int position, long id){

        currentSubCategoryPosition = position;
        currentTopCategoryPosition = lstFilterOptsTopCatgories.getCheckedItemPosition();

        CategoryInfo subCategory = filterSubCategories.get(position);
        selectedSubCategoryId = subCategory.getId();

        searchModel.searchFilter.setCategory(selectedSubCategoryId);
        searchModel.searchFilter.setRootCategory(selectedTopCategoryId);

        animateRefresh();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        rfTag = intent.getStringExtra(ACT_PARA_RF_TAG);

        isCategorySearch = intent.getBooleanExtra("fromList", false);
        if (isCategorySearch) {
            searchMode = Q_CATEGORY;
            int catgryId = intent.getIntExtra("category_id", -1);       // 使用-1是为了log错误信息
            if (catgryId < 0) {
                L.e("error retrieve category_id from extras of intent");
                catgryId = FiveMilesAppConst.DEFAULT_CATEGORY_ID;
            }
            selectedTopCategoryId = intent.getIntExtra("top_category_id", 1000);
            selectedSubCategoryId = catgryId;
            search = String.valueOf(catgryId);
            searchTitle = intent.getStringExtra("category_title");
            currScreenName = SEARCH_BY_CATEGORY_VIEW_NAME;
            currScreenActionPrefix = "category_";
        } else if (intent.hasExtra(ACT_PARA_S_BRAND_NAME)) {
            searchMode = Q_BRAND;
            searchId = intent.getIntExtra(ACT_PARA_S_BRAND_ID, 0);
            searchTitle = intent.getStringExtra(ACT_PARA_S_BRAND_NAME);
            currScreenName = PRODUCT_BRAND_VIEW_NAME;
            currScreenActionPrefix = "brandpage_";
        } else if(Intent.ACTION_VIEW.equals(intent.getAction()) && intent.hasExtra(ACT_PARA_S_KEYWORD)){
            searchMode = Q_KEYWORD;
            search = intent.getStringExtra(ACT_PARA_S_KEYWORD);
            currScreenName = SEARCH_BY_TEXT_VIEW_NAME;
            currScreenActionPrefix = "search_";
        } else {
            searchMode = Q_KEYWORD;
            search = intent.getStringExtra("search_text");
            currScreenName = SEARCH_BY_TEXT_VIEW_NAME;
            currScreenActionPrefix = "search_";
            selectedTopCategoryId = -1;
            selectedSubCategoryId = -1;
        }

        // get ad banner
        if (intent.hasExtra(ACT_PARA_S_CAMPAIGN_ID)) {
            String cid = intent.getStringExtra(ACT_PARA_S_CAMPAIGN_ID);
            searchModel.getBanner(cid);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
            listView.onLoadMoreComplete();
        }
    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @OnItemClick(R.id.search_result_list)
    @SuppressWarnings("unused")
    public void onListItemClick(int position) {
        int index = position - listView.getHeaderViewsCount();
        if (index >= 0 && index < searchModel.items.size()) {
            Item item;
            if (searchOptionValue == SearchOption.GRID_MODE) {
                item = itemCollForGrid.get(index);
            } else {
                item = itemCollForList.get(index);
            }

            Intent intent = new Intent(SearchResultActivity.this, ItemActivity.class);
            intent.putExtra(FiveMilesAppConst.ACT_PARA_ITEM_THUMB, item);
            startActivity(intent);
            trackTouch(currScreenName, currScreenActionPrefix + "product");
        }
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
            throws JSONException {
        swipeRefreshLayout.setRefreshing(false);
        listView.onLoadMoreComplete(jo != null, searchModel.hasMore());

        if (url.contains("/search/") || url.contains("/search_new/") || url.startsWith(SEARCH_BRAND)) {
            if (!url.contains("offset")) {  // except 'get more' requests
                setFilterCtrlBarVisible(true);
                setFilterCtrlBarEnabled(true);
            }

            if (jo != null) {
                populateAdapter();
            }

            blankView.setVisibility(searchModel.items == null || searchModel.items.isEmpty() ?
                    View.VISIBLE : View.GONE);
        } else if (url.startsWith(AD_BANNER)) {
            if (jo != null) {
                bannerUtil.showCampaign(searchModel.bannerCampaign);
            }
        }
    }

    private void populateAdapter() {
        if (searchOptionValue == SearchOption.GRID_MODE) {
            itemCollForGrid.clear();
            itemCollForGrid.addAll(searchModel.items);
            gridAdapter.notifyDataSetChanged();
        } else {
            itemCollForList.clear();
            itemCollForList.addAll(searchModel.items);
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoadMore() {
        if (!searchModel.hasMore()) {
            listView.onLoadMoreComplete(true, false);
            return;
        }

        if (swipeRefreshLayout.isRefreshing()) {
            listView.onLoadMoreComplete();
            return;
        }

        switch (searchMode) {
            case Q_CATEGORY:
                searchModel.searchCategoryMore(search, selectedTopCategoryId, rfTag);

                TrackingUtils.trackTouch(VIEW_NAME, "category_loadmore");
                break;
            case Q_BRAND:
                searchModel.searchBrandMore(searchId, rfTag);

                TrackingUtils.trackTouch(VIEW_NAME, "brand_loadmore");
                break;
            default:
                searchModel.searchByKeywordMore(search, rfTag);

                TrackingUtils.trackTouch(VIEW_NAME, "search_loadmore");
                break;
        }
    }

    @Override
    public void onPauseRendering() {
        ImageLoader.getInstance().pause();
    }

    @Override
    public void onResumeRendering() {
        ImageLoader.getInstance().resume();
    }

    @Override
    public void onScroll(boolean isAtTop, ScrollDirection scrollDirection) {
        swipeRefreshLayout.setEnabled(isAtTop);
    }

    @Override
    public void onRefresh() {
        setFilterCtrlBarEnabled(false);
        doSearch();
        listView.onLoadMoreComplete();
    }

    private void doSearch() {
        switch (searchMode) {
            case Q_CATEGORY:
                title.setVisibility(View.VISIBLE);
                searchBar.setVisibility(View.GONE);
                title.setText(searchTitle);
                searchModel.searchCategory(search, selectedTopCategoryId,rfTag);

                TrackingUtils.trackTouch(VIEW_NAME, "category_refresh");
                break;
            case Q_BRAND:
                title.setVisibility(View.VISIBLE);
                searchBar.setVisibility(View.GONE);
                title.setText(searchTitle);
                searchModel.searchBrand(searchId, rfTag);

                TrackingUtils.trackTouch(VIEW_NAME, "brand_refresh");
                break;
            default:
                title.setVisibility(View.GONE);
                searchBar.setVisibility(View.VISIBLE);
                edtSearch.setText(search);

                if (isNotEmpty(search)) {
                    searchModel.searchByKeyword(search, rfTag);
                }

                TrackingUtils.trackTouch(VIEW_NAME, "search_refresh");
                break;
        }

        closeKeyBoard();
        closeFilterPane();
    }

    private void animateRefresh() {
        if (!swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(true);
            onRefresh();
        }
    }

    public void switchSearchOption(View view) {
        closeFilterPane();

        if (searchOptionValue == SearchOption.LIST_MODE) {
            // list mode -> grid mode
            searchOption.setImageResource(R.drawable.search_option_list);
            searchOptionValue = SearchOption.GRID_MODE;

            itemCollForGrid.clear();
            listView.setAdapter(gridAdapter);
            listView.setColumnCount(2);
            listView.resetToTop();
        } else if (searchOptionValue == SearchOption.GRID_MODE) {
            // grid mode -> list mode
            searchOption.setImageResource(R.drawable.search_option_grid);
            searchOptionValue = SearchOption.LIST_MODE;

//            listAdapter.data.clear();
            itemCollForList.clear();
            listView.setAdapter(listAdapter);
            listView.setColumnCount(1);
            listView.resetToTop();
        }

        populateAdapter();
        trackTouch(currScreenName, currScreenActionPrefix + "switch");
    }

    public void onEvent(Object event) {
        Message message = (Message) event;
        try {
            if (message.what == MessageConstant.VIEW_SEARCH_LIST_OWNER) {
                User user = (User) message.obj;
                viewOrderOwner(user);
            } else if (message.what == MessageConstant.VIEW_SEARCH_LIST_MAP) {
                Item item = (Item) message.obj;
                viewOrderMap(item);
            }
        } catch (Exception e) {
            L.e(e);
        }
    }

    private void viewOrderMap(Item item) {
        Intent intent = new Intent(this, ItemLocationMapActivity.class);
        intent.putExtra(ACT_PARA_ITEM, item);
        startActivity(intent);
    }

    private void viewOrderOwner(User user) {
        if (ModelUtils.isMe(user)) {
            Intent intent = new Intent(this, TabProfileActivity.class);
            intent.putExtra(ACT_PARA_CAN_GO_BACK, true);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, OtherProfileActivity.class);
            intent.putExtra(ACT_PARA_USER, USER.fromDomain(user));
            startActivity(intent);
        }
        trackTouch(currScreenName, currScreenActionPrefix + "product_seller");
    }

    private void setFilterCtrlBarEnabled(boolean enabled) {
        Utils.setViewEnabledRecursively(filterCtrlBar, enabled);
    }

    private void setFilterCtrlBarVisible(boolean visible) {
        filterCtrlBar.setVisibility(visible ? View.VISIBLE : View.GONE);
        doOnLayoutChange(filterCtrlBar, headerPlaceholderUpdater);
    }

    private void setFilterLayerVisible(boolean visible) {
        filterLayer.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @OnCheckedChanged({R.id.cbx_filter_distance, R.id.cbx_filter_category, R.id.cbx_filter_sort, R.id.cbx_filter_misc})
    void trackFilterState(CheckBox v) {
        String action;
        if (v == cbxFilterDistance) {
            action = "distance";
        } else if (v == cbxFilterCategory) {
            action = "category";
        } else if (v == cbxSorting) {
            action = "sortorder";
        } else {
            action = "filter";
        }

        action += v.isChecked() ? "on" : "off";
        trackTouch(currScreenName, action);
    }

    @OnClick({R.id.cbx_filter_distance, R.id.cbx_filter_category, R.id.cbx_filter_sort, R.id.cbx_filter_misc})
    void onFilterOptClicked(final CheckBox v) {
        if (switchingFilterPane) {
            return;
        }

        Runnable action = new Runnable() {
            @Override
            public void run() {
                resetFilterPanes(v);
                setFilterPaneVisible(v);
                switchingFilterPane = false;
            }
        };

        if (v != currentFilterOpt && slidingFilterHelper.isExpanded()) {
            switchingFilterPane = true;
            closeFilterPane();
            postFilterCloseAction = action;
        } else {
            action.run();
        }

        if (v.isChecked()) {
            currentFilterOpt = v;
        }
    }

    private void resetFilterCheckboxes(View v) {
        if (v != cbxFilterDistance && cbxFilterDistance.isChecked()) {
            cbxFilterDistance.setChecked(false);
        }

        if (v != cbxFilterCategory && cbxFilterCategory.isChecked()) {
            cbxFilterCategory.setChecked(false);
        }

        if (v != cbxSorting && cbxSorting.isChecked()) {
            cbxSorting.setChecked(false);
        }

        if (v != cbxFilterMisc && cbxFilterMisc.isChecked()) {
            cbxFilterMisc.setChecked(false);
        }
    }

    private void setFilterPaneVisible(CheckBox v) {
        boolean toExpand = v.isChecked();

        if (toExpand) {
            setFilterLayerVisible(true);
            expandFilterPane();
        } else {
            closeFilterPane();
        }

        resetFilterCheckboxes(v);
    }

    private void resetFilterPanes(CheckBox v) {
        if (!v.isChecked()) {
            return;
        }

        if (v == cbxFilterMisc) {
            filterOptsPane.setVisibility(View.GONE);
            miscFilterPane.setVisibility(View.VISIBLE);
        } else {

            filterOptsPane.setVisibility(View.VISIBLE);
            miscFilterPane.setVisibility(View.GONE);

            if( v==cbxFilterCategory){
                lstFilterOpts.setVisibility(View.GONE);
                lstFilterOptsTopCatgories.setVisibility(View.VISIBLE);
                lstFilterOptsSubCatgories.setVisibility(View.VISIBLE);
            }else{
                lstFilterOpts.setVisibility(View.VISIBLE);
                lstFilterOptsTopCatgories.setVisibility(View.GONE);
                lstFilterOptsSubCatgories.setVisibility(View.GONE);
            }
        }

        populateFilterPane(v);
    }

    private void populateFilterPane(CheckBox v) {
        if (v == cbxFilterDistance) {
            populateDistanceFilter();
        } else if (v == cbxFilterCategory) {
            populateCategoryFilter();
        } else if (v == cbxSorting) {
            populateSorting();
        } else {
            populateMiscFilter();
        }

        icDrawerHandle.setVisibility(v != cbxFilterMisc ? View.VISIBLE : View.GONE);
    }

    private void populateMiscFilter() {
        if (searchModel.searchFilter == null) {
            return;
        }

        cbxFilterVerifiedOnly.setChecked(searchModel.searchFilter.isVerifiedUserOnly());
        Integer minPrice = searchModel.searchFilter.getMinPrice();
        if (minPrice != null && minPrice > 0) {
            edtPriceFilter1.setText(formatPriceDigits(minPrice));
        }

        Integer maxPrice = searchModel.searchFilter.getMaxPrice();
        if (maxPrice != null && maxPrice > 0) {
            edtPriceFilter2.setText(formatPriceDigits(maxPrice));
        }
    }

    private void populateSorting() {
        filterOpts.clear();
        lstFilterOpts.clearChoices();

        filterOpts.add(new SearchModel.Option(SORTING, getString(R.string.order_by_relevancy), "0", true));
        filterOpts.add(new SearchModel.Option(SORTING, getString(R.string.order_by_distance), "1"));
        filterOpts.add(new SearchModel.Option(SORTING, getString(R.string.order_by_time), "2"));
        filterOpts.add(new SearchModel.Option(SORTING, getString(R.string.order_by_price), "3"));
        filterOpts.add(new SearchModel.Option(SORTING, getString(R.string.order_by_price_desc), "4"));
        filterOptAdapter.notifyDataSetChanged();
        DisplayUtils.setListViewHeightBasedOnChildren(lstFilterOpts, MAX_FILTER_ROWS);

        int defaultSelection = 0;  // default is 'most relevant'
        if (searchModel.searchFilter == null) {
            selectFilterOpt(defaultSelection);
            return;
        }

        int i = searchModel.searchFilter.getOrderBy();
        i = (i >= 0 && i < filterOpts.size()) ? i : defaultSelection;
        selectFilterOpt(i);
    }

    private void populateCategoryFilter() {

        filterTopCategories.clear();
        lstFilterOptsTopCatgories.clearChoices();

        filterSubCategories.clear();
        lstFilterOptsSubCatgories.clearChoices();

        if(searchMode == Q_CATEGORY){

            List<CategoryInfo> currentSubCategories = subCategoriesMap.get(selectedTopCategoryId);

            filterTopCategories.addAll(allCategoriesList);
            filterSubCategories.addAll(currentSubCategories);

            filterTopCategoriesAdapter.notifyDataSetChanged();
            filterSubCategoriesAdapter.notifyDataSetChanged();

            currentTopCategoryPosition = getPositionByCategoryId(filterTopCategories, selectedTopCategoryId);
            currentSubCategoryPosition = getPositionByCategoryId(filterSubCategories, selectedSubCategoryId);

            setSelection(lstFilterOptsTopCatgories, currentTopCategoryPosition);
            setSelection(lstFilterOptsSubCatgories, currentSubCategoryPosition);

        }else if(searchMode == Q_KEYWORD){

            CategoryInfo categoryAll = new CategoryInfo();
            categoryAll.setId(CategoryInfo.ID_ALL_CATEGORY);
            categoryAll.setTitle(getString(R.string.category_option_all));

            filterTopCategories.add(categoryAll);
            filterTopCategories.addAll(allCategoriesList);
            filterTopCategoriesAdapter.notifyDataSetChanged();

            int selectedTopCategoryPosition = getPositionByCategoryId(filterTopCategories, selectedTopCategoryId);
            setSelection(lstFilterOptsTopCatgories, selectedTopCategoryPosition);

            if(selectedTopCategoryId != -1){

                List<CategoryInfo> currentSubCategories = subCategoriesMap.get(selectedTopCategoryId);
                CategoryInfo currentTopCategory = allCategoriesMap.get(selectedTopCategoryId);

                CategoryInfo categoryAllForTopCategory = new CategoryInfo();
                categoryAllForTopCategory.setId(CategoryInfo.ID_ALL_CATEGORY);
                categoryAllForTopCategory.setTitle(getString(R.string.category_option_all) + " for " + currentTopCategory.getTitle());

                filterSubCategories.add(categoryAllForTopCategory);
                filterSubCategories.addAll(currentSubCategories);
                filterSubCategoriesAdapter.notifyDataSetChanged();

                int selectedSubCategoryPosition = getPositionByCategoryId(filterSubCategories, selectedSubCategoryId);
                setSelection(lstFilterOptsSubCatgories, selectedSubCategoryPosition);
            }
        }
    }

    private void populateDistanceFilter() {
        filterOpts.clear();
        lstFilterOpts.clearChoices();

        filterOpts.add(new SearchModel.Option(DISTANCE, getString(R.string.distance_option, 5), "1"));
        filterOpts.add(new SearchModel.Option(DISTANCE, getString(R.string.distance_option, 10), "2"));
        filterOpts.add(new SearchModel.Option(DISTANCE, getString(R.string.distance_option, 20), "3"));
        filterOpts.add(new SearchModel.Option(DISTANCE, getString(R.string.distance_option, 30), "4"));
        filterOpts.add(new SearchModel.Option(DISTANCE, getString(R.string.distance_option, 50), "5"));
        filterOpts.add(new SearchModel.Option(DISTANCE, getString(R.string.distance_option_no_limit), "0", true));
        filterOptAdapter.notifyDataSetChanged();
        DisplayUtils.setListViewHeightBasedOnChildren(lstFilterOpts, MAX_FILTER_ROWS);

        if (searchModel.searchFilter == null) {
            // clear selection
            lstFilterOpts.clearFocus();
            return;
        }

        int i = searchModel.searchFilter.getDistanceRange() - 1;
        if (i < filterOpts.size()) {
            selectFilterOpt(i < 0 ? filterOpts.size() - 1 : i);  // -1=0-1 is 'no limit'
        }
    }

    private void selectFilterOpt(final int index) {
        lstFilterOpts.clearFocus();
        lstFilterOpts.post(new Runnable() {
            @Override
            public void run() {
                lstFilterOpts.setSelection(index);
                lstFilterOpts.setItemChecked(index, true);
            }
        });
    }

    private void setSelection(final ListView listView, final int index){
        listView.clearFocus();

        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.setSelection(index);
                listView.setItemChecked(index, true);
            }
        });
    }


    @OnItemClick(R.id.lst_filter_opts)
    void onFilterChanged(int position) {
        if (position < 0 || position >= filterOpts.size()) {
            return;
        }

        SearchModel.Option opt = filterOpts.get(position);
        if (opt == null) {
            return;
        }

        searchModel.updateFilter(opt);
        trackFilterOpt(opt);
        switch (opt.type) {
            case CATEGORY:
                if (searchMode == Q_CATEGORY) {
                    searchTitle = opt.name;
                    search = opt.value;
                }
                showCurrentFilterOpt(cbxFilterCategory, opt);
                break;
            case DISTANCE:
                showCurrentFilterOpt(cbxFilterDistance, opt);
                break;
            case SORTING:
                showCurrentFilterOpt(cbxSorting, opt);
                break;
        }

        animateRefresh();
    }

    private void trackFilterOpt(SearchModel.Option opt) {
        trackTouch(currScreenName, opt.type.name() + "refine");
    }

    private void showCurrentFilterOpt(CheckBox v, SearchModel.Option opt) {
        v.setText(opt.name);
    }

    @OnClick(R.id.cbx_filter_verified_only)
    void trackVerifiedFilter(CheckBox v) {
        boolean toBeChecked = !v.isChecked();  // click happened before checked state change
        trackTouch(currScreenName, toBeChecked ? "verifyrefineon" : "verifyrefineoff");
    }

    @OnFocusChange({R.id.edt_filter_price1, R.id.edt_filter_price2})
    void trackPriceFilter(EditText edt, boolean hasFocus) {
        if (hasFocus) {
            return;
        }

        String p = edt.getText().toString().trim();
        if (isEmpty(p)) {
            return;
        }

        Long v = null;
        try {
            v = Long.parseLong(p);
        } catch (Exception e) {
            // ignored
        }

        trackTouch(currScreenName, edt == edtPriceFilter1 ? "minprice" : "maxprice", v);
    }

    @OnClick(R.id.btn_misc_filter_confirm)
    void onConfirmMiscFilter() {
        String strPrice1 = edtPriceFilter1.getText().toString().trim();
        String strPrice2 = edtPriceFilter2.getText().toString().trim();

        Integer p1 = searchModel.parsePrice(strPrice1);
        Integer p2 = searchModel.parsePrice(strPrice2);

        // 暂时不支持0价格
        if (p1 != null && p1 == 0) {
            DisplayUtils.toast(R.string.input_price);
            edtPriceFilter1.requestFocus();
            return;
        }

        if (p2 != null && p2 == 0) {
            DisplayUtils.toast(R.string.input_price);
            edtPriceFilter2.requestFocus();
            return;
        }

        searchModel.updateMiscFilter(cbxFilterVerifiedOnly.isChecked(), strPrice1, strPrice2);
        animateRefresh();
        trackTouch(currScreenName, "filterconfirm");
    }

    @OnTextChanged(R.id.search_edit)
    void onKeywordTextChanged() {
        String keyword = edtSearch.getText().toString().trim();

        if (keyword.length() > 0) {
            ivClear.setVisibility(View.VISIBLE);
        } else {
            ivClear.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.search_edit)
    void onEdtSearchClicked() {
        trackTouch(currScreenName, "searchagain");  // 在result页面发起的查询，都是again
    }

    @OnEditorAction(R.id.search_edit)
    boolean onSearchEditorAction(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            String keyword = edtSearch.getText().toString().trim();
            if (isEmpty(keyword)) {
                DisplayUtils.toast(R.string.msg_input_something);
            } else {
                updateKeyword(keyword);
            }
            return true;
        }
        return false;
    }

    private void updateKeyword(String keyword) {
        search = keyword;

        // 关键词变更，需从server获取相关品类，并重置品类filter状态
        searchModel.setRefindFlag(0);
        cbxFilterCategory.setText(R.string.filter_title_category);

        animateRefresh();
        trackTouch(currScreenName, "search_action");
    }

    @OnClick(R.id.iv_clear_search_result)
    void cleanSearchEdit() {
        edtSearch.setText("");
        search = "";
        ivClear.setVisibility(View.GONE);
        showKeyBoard(edtSearch);
        trackTouch(currScreenName, "search_cancel");
    }

    private void expandFilterPane() {
        if (slidingFilterHelper.isCollapsed()) {
            slidingFilterHelper.expand();
        }
    }

    @OnClick(R.id.filter_layer)
    void closeFilterPane() {
        if (slidingFilterHelper.isExpanded()) {
            slidingFilterHelper.collapse();
        }
    }

    private void onFilterPaneClosed() {
        closeKeyBoard();

        if (postFilterCloseAction != null) {
            runOnUiThread(postFilterCloseAction);
            postFilterCloseAction = null;
        } else {
            setFilterLayerVisible(false);
            resetFilterCheckboxes(null);  // un-check all of them
        }
    }

    private void closeKeyBoard() {
        View focusedView = getWindow().getCurrentFocus();
        if (focusedView != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            focusedView.clearFocus();
            imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
        }
    }

    private void showKeyBoard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, 0);
    }

    @OnTouch(R.id.search_result_list)
    public boolean onTouchList(MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        return false;
    }

    private void animateFilterCtrlBar(boolean scrollingUp) {
        if (animatingFilterCtrlBar) {
            return;
        }

        final boolean currVisible = filterCtrlBar.getVisibility() == View.VISIBLE;
        Animation anim = null;

        if (scrollingUp && !currVisible) {
            // show filter
            anim = new TranslateAnimation(RELATIVE_TO_SELF, 0, RELATIVE_TO_SELF, 0,
                    RELATIVE_TO_SELF, -1, RELATIVE_TO_SELF, 0);
            anim.setStartOffset(500);
        } else if (!scrollingUp && currVisible && isFilterPlaceholderOffScreen()) {
            // hide filter when scrolling down a bit
            anim = new TranslateAnimation(RELATIVE_TO_SELF, 0, RELATIVE_TO_SELF, 0,
                    RELATIVE_TO_SELF, 0, RELATIVE_TO_SELF, -1);
        }

        if (anim == null) {
            return;
        }

        anim.setDuration(300);
        anim.setAnimationListener(new SimpleAnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                animatingFilterCtrlBar = false;
                setFilterCtrlBarVisible(!currVisible);
//                L.v("animation end, filter visible? %s", filterCtrlBar.getVisibility() == View.VISIBLE);
            }
        });

        animatingFilterCtrlBar = true;
        filterCtrlBar.startAnimation(anim);
    }

    private boolean isFilterPlaceholderOffScreen() {
        View v = listView.getChildAt(0);
        if (v == null) {
            return false;
        }

        int h = headerPlaceholder.getMeasuredHeight();
        int y = v.getTop();
        return y <= -h;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && slidingFilterHelper.isExpanded()) {
            slidingFilterHelper.collapse();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected boolean isLoginProtected() {
        return true;
    }

    private int getPositionByCategoryId(AdapteeCollection<CategoryInfo> collection, int categoryId){
        int i = 0;
        for(; i<collection.size(); i++){
            CategoryInfo categoryInfo = collection.get(i);
            if(categoryInfo.getId() == categoryId){
                break;
            }
        }
        return i;
    }

}
