package com.insthub.fivemiles;


public class MessageConstant {
    public static final int SIGN_UP_SUCCESS 		= 0;
    public static final int POST_REPLY 				= 1;	
    public static final int ADD_PHOTO 				= 3;
    public static final int REMOVE_PHOTO 			= 4;	
    public static final int REFRESH_HOME = 5;
    public static final int DELETE_SYSTEM 			= 11;
    public static final int PUSH_ORDERDETIAL 		= 13;
    public static final int UNREAD_BUYING 			= 14;
    public static final int UNREAD_SELLING 			= 15;
    public static final int VIEW_SEARCH_LIST_OWNER  = 16;
    public static final int VIEW_SEARCH_LIST_MAP    = 17;
    public static final int UNREAD_SYSTEM 			= 18;
    public static final int MSG_BUY_CLK 			= 19;
    public static final int MSG_SELL_CLK 			= 20;
    public static final int VIEW_PROFILE 			= 21;
    public static final int SIGNOUT       			= 22;
    public static final int OPEN_SYS_MSG = 23;
    public static final int SYS_MSG_ACTION = 24;
    public static final int APP_HAS_UPDATE = 25;
    public static final int PROFILE_FOLLOW_CLK =  26;
    public static final int PROFILE_UNFOLLOW_CLK =  27;
    public static final int SELLERS_NEARBY_FOLLOW_CLK = 28;
    public static final int SELLERS_NEARBY_UNFOLLOW_CLK = 29;
    public static final int GEO_LOCATION_UPDATED = 30;
    public static final int HOME_TOOLBAR_SEARCH_CLK = 31;

    /* --------------------------------------------
     * Push constants
     */
    public static final String PUSH_ID = "push_id";

    public static final int PUSH_ID_SYS_MSG = 1;

    public static final String PUSH_TAG = "push_tag";
    public static final String PUSH_TAG_SYS_MSG = "push_tag_sys_msg";

    public static final int PUSH_ACT_NEW_OFFER = 2001;  // notification for new offer/chat
    public static final int PUSH_ACT_NEW_SYS_MSG = 2003;  // notification for new system message
    public static final int PUSH_ACT_REFRESH_PROFILE = 2004;  // notification for profile updated
    public static final int PUSH_ACT_REFRESH_HOME = 2005;  // notification for home page refresh
    public static final int PUSH_ACT_REFRESH_MSG = 2006;  // show & refresh message view
    public static final int PUSH_ACT_SEARCH = 2008;  // open search page
    public static final int PUSH_ACT_SHOW_HOME = 2009;  // show home tab, but not refreshing

    /* --------------------------------------------
     * Model/View observable events
     */
    public static final int PROP_DISTANCE_UPDATED = 1;
}
