package com.thirdrock.fivemiles.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.insthub.fivemiles.Activity.EmailLoginActivity;
import com.insthub.fivemiles.Activity.MainTabActivity;
import com.insthub.fivemiles.Activity.MyProfileActivity;
import com.insthub.fivemiles.Activity.RegisterActivity;
import com.insthub.fivemiles.Activity.RegisterUserActivity;
import com.insthub.fivemiles.Activity.WelcomeActivity;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.activity.AbsActivity;
import com.thirdrock.fivemiles.reco.SellersNearbyActivity;
import com.thirdrock.fivemiles.util.DisplayUtils;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REDIRECT_INTENT;
import static com.insthub.fivemiles.FiveMilesAppConst.EMPTY_STRING;
import static com.thirdrock.fivemiles.login.GeolocationViewModel.Property;


public class ZipcodeVerifyActivity extends AbsActivity<GeolocationViewModel, GeolocationViewModel.Property> {

    @Inject
    GeolocationViewModel viewModel;

    @InjectView(R.id.top_view_close)
    ImageView btnTopClose;

    @InjectView(R.id.edt_zipcode_no)
    EditText edtZipcodeNum;

    @InjectView(R.id.txt_opt_terms)
    TextView txtOptTerms;

    @InjectView(R.id.txt_enforce_terms)
    TextView txtEnforceTerms;

    @InjectView(R.id.btn_verify_postcode)
    TextView btnVerifyPostcode;

    private String from;
    public static String VIEW_ID = "zipcode_view";
    // VIEW_NAME用于GA
    private static String VIEW_NAME = EMPTY_STRING;

    private boolean isFromSellersNearby = false;
    private boolean closeBtnEnabled = false;

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {
        from = getIntent().getStringExtra("from");
        boolean closeBtnNotAllowed = !isAllowCloseBtn();
        if(from != null && closeBtnNotAllowed) {
            btnTopClose.setVisibility(View.GONE);
            txtEnforceTerms.setVisibility(View.VISIBLE);
            txtOptTerms.setVisibility(View.GONE);
            isFromSellersNearby = true;
            VIEW_NAME = "zipcodepop_view";
        } else {
            btnTopClose.setVisibility(View.VISIBLE);
            txtEnforceTerms.setVisibility(View.GONE);
            txtOptTerms.setVisibility(View.VISIBLE);
            isFromSellersNearby = false;
            VIEW_NAME = "changezipcode_view";
        }

        // TODO 使用RxEventUtils重写
        edtZipcodeNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // empty
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // empty
            }

            @Override
            public void afterTextChanged(Editable s) {
                String currentTxt = edtZipcodeNum.getText().toString();
                if (currentTxt.equals("")) {
                    btnVerifyPostcode.setBackgroundResource(R.drawable.button_gray);
                    btnVerifyPostcode.setLinksClickable(false);
                } else {
                    btnVerifyPostcode.setBackgroundResource(R.drawable.button_orange);
                    btnVerifyPostcode.setLinksClickable(true);
                }
            }
        });

        DisplayUtils.showKeyBoard(edtZipcodeNum);
    }

    private boolean isAllowCloseBtn(){
        boolean closeBtnNotAllowed = from.equals(WelcomeActivity.VIEW_NAME)
                || from.equals(RegisterActivity.VIEW_NAME)
                || from.equals(EmailLoginActivity.VIEW_NAME);

        return !closeBtnNotAllowed;
    }

    @Override
    protected String getScreenName() {
        return VIEW_NAME;
    }

    @Override
    protected int getContentView() {
        return R.layout.zipcode_verify;
    }

    @Override
    protected GeolocationViewModel getViewModel() {
        return viewModel;
    }

    // 直接点击关闭
    @OnClick(R.id.top_view_close)
    @SuppressWarnings("unused")
    void onClickTopViewClose() {

        if(closeBtnEnabled){
            //当输入一次zipcode错误后显示的关闭按钮点击后,则显示下一个activity.
            zipcodeUpdated();
        }else{
            ZipcodeVerifyActivity.this.setResult(RESULT_CANCELED, new Intent());
            ZipcodeVerifyActivity.this.finish();

            if (isFromSellersNearby) {
                trackTouch("zipcodeclose");
            } else {
                trackTouch("zipcodeback");
            }
        }
    }


    @OnClick(R.id.btn_verify_postcode)
    @SuppressWarnings("unused")
    void onVerifyPostcode() {
        String zipCode = edtZipcodeNum.getText().toString();
        viewModel.registerZipcode(zipCode);

        if (isFromSellersNearby) {
            trackTouch("zipcodeconfirm");
        } else {
            trackTouch("zipcodesave");
        }
    }

    @Override
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {
        switch (property) {
            case register_zipcode:
                zipcodeUpdated();
                DisplayUtils.toast(R.string.right_zipcode);
                trackTouch("zipcoderight");
                break;
        }
    }

    @Override
    public void onError(Property property, Throwable e) {

        if (property.equals(Property.register_zipcode)) {

            DisplayUtils.toast(R.string.wrong_zipcode);
            trackTouch("zipcodewrong");

            boolean isAllowCloseBtn = isAllowCloseBtn();
            if (!closeBtnEnabled && !isAllowCloseBtn) {
                btnTopClose.setImageResource(R.drawable.ic_navi_close);
                btnTopClose.setVisibility(View.VISIBLE);
                closeBtnEnabled = true;
            }
        }
    }

    private void zipcodeUpdated() {

        if (from != null) {
            switch (from) {
                case RegisterActivity.VIEW_NAME:

                    Intent registerIntent = new Intent(ZipcodeVerifyActivity.this, RegisterUserActivity.class);
                    startActivity(registerIntent);
                    break;
                case WelcomeActivity.VIEW_NAME:

                    Intent sellersNearbyIntent = new Intent(ZipcodeVerifyActivity.this, SellersNearbyActivity.class)
                            .putExtra("from", VIEW_ID);
                    if (getIntent() != null) {
                        sellersNearbyIntent.putExtra(ACT_PARA_REDIRECT_INTENT, getIntent().getParcelableExtra(ACT_PARA_REDIRECT_INTENT));
                    }

                    startActivity(sellersNearbyIntent);
                    break;
                case MyProfileActivity.VIEW_NAME:

                    SESSION session = SESSION.getInstance();
                    String country = TextUtils.isEmpty(session.getCountry())? EMPTY_STRING : session.getCountry();
                    String city = TextUtils.isEmpty(session.getCity())? EMPTY_STRING : session.getCity();
                    String region = TextUtils.isEmpty(session.getRegion())? EMPTY_STRING : session.getRegion();

                    Intent resultIntent = new Intent()
                            .putExtra("city", city)
                            .putExtra("region", region)
                            .putExtra("country", country);

                    setResult(RESULT_OK, resultIntent);
                    finish();
                    break;
                case EmailLoginActivity.VIEW_NAME:

                    Intent mainTabIntent = new Intent(this, MainTabActivity.class)
                            .putExtra("from", VIEW_ID)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mainTabIntent);
                    break;
            }
        }
    }
}
