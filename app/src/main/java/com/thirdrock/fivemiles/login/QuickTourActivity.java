package com.thirdrock.fivemiles.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.BeeFramework.Utils.AnimationUtil;
import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.external.eventbus.EventBus;
import com.insthub.fivemiles.Activity.EmailLoginActivity;
import com.insthub.fivemiles.Activity.MainTabActivity;
import com.insthub.fivemiles.Activity.RegisterActivity;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.HomeModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.HOME_OBJECTS;
import com.insthub.fivemiles.Utils.LocationManagerUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.pedrogomez.renderers.AdapteeCollection;
import com.pedrogomez.renderers.RendererAdapter;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.common.item.ItemThumbRenderer;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.ui.renderer.MonoRendererBuilder;
import com.thirdrock.framework.ui.renderer.SimpleAdapteeCollection;
import com.thirdrock.framework.ui.widget.WaterfallListView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REDIRECT_INTENT;
import static com.thirdrock.framework.ui.widget.WaterfallListView.OnScrollListener.ScrollDirection;
import static com.thirdrock.framework.util.Utils.doOnGlobalLayout;


public class QuickTourActivity extends BaseActivity implements BusinessResponse,
        WaterfallListView.Callback, SwipeRefreshLayout.OnRefreshListener {

    private HomeModel homeModel;

    @InjectView(R.id.swipe_refresh_view)
    SwipeRefreshLayout swipeRefreshLayout;

    @InjectView(R.id.list_items)
    WaterfallListView lstItems;

    private View bgMask, pnlCtrls;
    private LocationManagerUtil locationMgr;

    private Intent redirectIntent;

    public static final String VIEW_NAME = "checkitout_view";
    private AdapteeCollection<ItemThumb> itemThumbs;
    private RendererAdapter<ItemThumb> itemListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_tour);
        ButterKnife.inject(this);

        redirectIntent = getIntent().getParcelableExtra(ACT_PARA_REDIRECT_INTENT);

        homeModel = new HomeModel(this);
        homeModel.addResponseListener(this);

        initListView();

        findViewById(R.id.top_view_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ((TextView) findViewById(R.id.top_view_title)).setText(R.string.title_activity_nearby);
        bgMask = findViewById(R.id.mask_bg);
        pnlCtrls = findViewById(R.id.signin_ctrl_wrapper);

        FacebookLoginFragment fbFrm = new FacebookLoginFragment();
        fbFrm.setLoginListener(new FacebookLoginFragment.LoginListener() {
            @Override
            public void onLoginSuccess() {
                startActivity(new Intent(QuickTourActivity.this, MainTabActivity.class)
                        .putExtra("from", VIEW_NAME)
                        .putExtra(ACT_PARA_REDIRECT_INTENT, redirectIntent));
                TrackingUtils.trackTouch(VIEW_NAME, "checkitout_facebook");
                finish();

                Message msg = new Message();
                msg.what = MessageConstant.SIGN_UP_SUCCESS;
                EventBus.getDefault().post(msg);
            }
        });
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fb_login_placeholder, fbFrm);
        ft.commit();

        locationMgr = LocationManagerUtil.getInstance();

        TrackingUtils.trackView(VIEW_NAME);
    }

    private void initListView() {
        lstItems.setCallback(this);

        DisplayUtils.setProgressColorScheme(swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setProgressViewOffset(true,
                getResources().getDimensionPixelSize(R.dimen.swipe_refresh_progress_offset_start),
                getResources().getDimensionPixelSize(R.dimen.swipe_refresh_progress_offset_end));

        itemThumbs = new SimpleAdapteeCollection<>();
        initListAdapter();
    }

    private void initListAdapter() {
        itemListAdapter = new RendererAdapter<>(
                getLayoutInflater(),
                new MonoRendererBuilder(new ItemThumbRenderer()),
                itemThumbs);
        lstItems.setAdapter(itemListAdapter);
    }

    private void animateRefresh() {
        if (!swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(true);
            onRefresh();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationMgr.stopLocationUpdates();

        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
            lstItems.onLoadMoreComplete();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationMgr.getLocation();

        doOnGlobalLayout(lstItems, new Runnable() {
            @Override
            public void run() {
                // wait layout complete in order to show the refreshing animation
                animateRefresh();
            }
        });
    }

    @SuppressWarnings("unused")
    public void onEvent(Object event) {
        Message msg = (Message) event;
        if (msg.what == MessageConstant.SIGN_UP_SUCCESS) {
            finish();
        }
    }

    private void populateAdapter() {
        itemThumbs.clear();
        for (HOME_OBJECTS o : homeModel.dataList) {
            itemListAdapter.add(o.toDomain());
        }
        itemListAdapter.notifyDataSetChanged();
    }

    @SuppressWarnings("unused")
    public void redirectToLogin(View v) {
        startActivity(new Intent(this, EmailLoginActivity.class)
                .putExtra(ACT_PARA_REDIRECT_INTENT, redirectIntent));
        TrackingUtils.trackTouch(VIEW_NAME, "checkitout_signin");
    }

    @SuppressWarnings("unused")
    public void redirectToRegister(View v) {
        startActivity(new Intent(this, RegisterActivity.class)
                .putExtra(ACT_PARA_REDIRECT_INTENT, redirectIntent));
        TrackingUtils.trackTouch(VIEW_NAME, "checkitout_signup");
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) throws JSONException {
        if (url.startsWith(ApiInterface.COMMODITY_HOME)) {
            swipeRefreshLayout.setRefreshing(false);
            lstItems.onLoadMoreComplete(jo != null, homeModel.hasMore());

            if (jo == null) {
                return;
            }

            populateAdapter();
        }
    }

    @Override
    public void onLoadMore() {
        if (homeModel.hasMore()) {
            homeModel.commodityHomeMore();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onPauseRendering() {
        ImageLoader.getInstance().pause();
    }

    @Override
    public void onResumeRendering() {
        ImageLoader.getInstance().resume();
    }

    @Override
    public void onScroll(boolean isAtTop, ScrollDirection scrollDirection) {
        swipeRefreshLayout.setEnabled(isAtTop);
    }

    @Override
    public void onRefresh() {
        homeModel.commodityHome();
        lstItems.onLoadMoreComplete();
    }

    @OnItemClick(R.id.list_items)
    @SuppressWarnings("unused")
    void onItemClick(int position) {
        showSigninPanel();
        TrackingUtils.trackTouch(VIEW_NAME, "checkitout_product");
    }

    private void showSigninPanel() {
        bgMask.setVisibility(View.VISIBLE);
        pnlCtrls.setVisibility(View.VISIBLE);
        AnimationUtil.showAnimation(pnlCtrls);
    }

    public void closeSigninPanel(View v) {
        bgMask.setVisibility(View.GONE);
        pnlCtrls.setVisibility(View.GONE);
        AnimationUtil.backAnimation(pnlCtrls);
        TrackingUtils.trackTouch(VIEW_NAME, "checkitout_back");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (pnlCtrls.getVisibility() == View.VISIBLE) {
                closeSigninPanel(null);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    // add redirection target before start next activity
    @Override
    public void startActivity(Intent intent) {
        intent.putExtra(ACT_PARA_REDIRECT_INTENT, getIntent().getParcelableExtra(ACT_PARA_REDIRECT_INTENT));
        super.startActivity(intent);
    }

    @Override
    protected boolean isLoginProtected() {
        return false;
    }
}
