package com.thirdrock.fivemiles.login;

import com.insthub.fivemiles.FiveMilesApp;
import com.thirdrock.domain.GeoLocation;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.LocationUtils;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.protocol.AddressResponse;
import com.thirdrock.repository.GeoLocationReportRepository;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * View Model for geolocation activity
 * Created by ywu on 15/2/2.
 */
public class GeolocationViewModel extends AbsViewModel<GeolocationViewModel.Property> {

    public enum Property {
        register_zipcode
    }

    @Inject
    GeoLocationReportRepository geoRep;

    private Observer<Void> registerZipcodeObserver;

    public GeolocationViewModel() {

        registerZipcodeObserver = newMajorJobObserver(Property.register_zipcode);
    }

    private GeoLocation geoLocation;
    public void registerZipcode(final String zipcode){
        emitMajorJobStarted();
        LocationUtils.getGeolocationByZipcode(zipcode)
                .flatMap(new Func1<AddressResponse, Observable<Void>>() {
                    @Override
                    public Observable<Void> call(AddressResponse addressResponse) {

                        geoLocation = LocationUtils.parseAddressResponse(addressResponse, 0, 0);

                        if (geoLocation == null) {
                            return Observable.error(new Exception(FiveMilesApp.appCtx.getString(R.string.wrong_zipcode)));
                        }

                        return geoRep.sendPostalCodeInfo(geoLocation, zipcode);
                    }
                }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(registerZipcodeObserver);
    }
}
