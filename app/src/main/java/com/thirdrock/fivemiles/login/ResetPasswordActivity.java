package com.thirdrock.fivemiles.login;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.insthub.fivemiles.Activity.EmailLoginActivity;
import com.insthub.fivemiles.Activity.RegisterActivity;
import com.insthub.fivemiles.Model.LoginModel;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class ResetPasswordActivity extends BaseActivity implements BusinessResponse {

    private EditText edtEmail;
    private View hintEmailSent;
    private LoginModel loginModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        TextView txtTitle = (TextView) findViewById(R.id.top_view_title);
        txtTitle.setText(R.string.reset_title);

        hintEmailSent = findViewById(R.id.hint_email_sent);

        // navigation back
        findViewById(R.id.top_view_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        edtEmail = (EditText) findViewById(R.id.login_email);
        edtEmail.setImeOptions(EditorInfo.IME_ACTION_GO);
        edtEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId != EditorInfo.IME_ACTION_GO) {
                    return false;
                }

                sendReset(null);
                return true;
            }
        });

        loginModel = new LoginModel(this);
        loginModel.addResponseListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        hintEmailSent.setVisibility(View.GONE);
    }

    @SuppressWarnings("unused")
    public void sendReset(View view) {
        CharSequence email = edtEmail.getText();

        if (TextUtils.isEmpty(email)) {
            DisplayUtils.toast(R.string.hint_email_required);
            return;
        }
        else if (!RegisterActivity.EML_PATTERN.matcher(email).matches()) {
            DisplayUtils.toast(R.string.error_invalid_email);
            return;
        }

        loginModel.resetPassword(email.toString());
        TrackingUtils.trackTouch(EmailLoginActivity.VIEW_NAME, "resetpassword");
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) throws JSONException {
        if (jo == null) {
            return;
        }

        hintEmailSent.setVisibility(View.VISIBLE);
    }

    @Override
    protected boolean isLoginProtected() {
        return false;
    }
}
