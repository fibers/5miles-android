package com.thirdrock.fivemiles.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.BeeFramework.model.BusinessResponse;
import com.BeeFramework.view.MyProgressDialog;
import com.external.androidquery.callback.AjaxStatus;
import com.external.eventbus.EventBus;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.LoginModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.domain.GeoLocation;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.EventUtils;
import com.thirdrock.fivemiles.util.LocationUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.util.L;

import org.json.JSONException;
import org.json.JSONObject;

import static com.insthub.fivemiles.FiveMilesAppConst.FB_PERMISSION_EMAIL;
import static com.insthub.fivemiles.FiveMilesAppConst.FB_PERMISSION_FRIENDS;
import static com.insthub.fivemiles.FiveMilesAppConst.FB_PERMISSION_PUBLIC;
import static com.insthub.fivemiles.FiveMilesAppConst.GETUI_PROVIDER;
import static com.insthub.fivemiles.MessageConstant.GEO_LOCATION_UPDATED;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class FacebookLoginFragment extends Fragment implements BusinessResponse {

    private LoginButton fbLoginBtn;
    private UiLifecycleHelper fbHelper;
    private volatile int pendingFbJobs;
    private volatile Bundle fbLoginData;
    private GeoLocation geolocation;

    private boolean active;  // 此fragment当前处于前景
    private LoginModel loginModel;

    private LoginListener listener;

    private MyProgressDialog fbAuthProg;

    public static interface LoginListener {
        void onLoginSuccess();
    }

    public FacebookLoginFragment() {
        // Required empty public constructor
    }

    public void setLoginListener(LoginListener listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        EventUtils.register(this);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_facebook_login, container, false);
        fbLoginBtn = (LoginButton) view.findViewById(R.id.fb_login_btn);
        fbLoginBtn.setFragment(this);
        fbLoginBtn.setReadPermissions(FB_PERMISSION_PUBLIC, FB_PERMISSION_EMAIL, FB_PERMISSION_FRIENDS);
        fbLoginBtn.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            @Override
            public void onUserInfoFetched(GraphUser graphUser) {
                if(!active) {
                    return;
                }
                onFbUserInfoFetched(graphUser);
            }
        });

        fbHelper = new UiLifecycleHelper(getActivity(), new Session.StatusCallback() {
            @Override
            public void call(Session session, SessionState sessionState, Exception e) {
                if (!active) {
                    return;
                }
                onFbSessionStateChange(session, sessionState, e);
            }
        });
        fbHelper.onCreate(savedInstanceState);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoginBtnClicked();
            }
        });
        view.findViewById(R.id.fb_login_btn_clickable).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoginBtnClicked();
            }
        });

        loginModel = new LoginModel(getActivity());
        loginModel.addResponseListener(this);

        if (!EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().register(this);
        }

        LocationUtils.getGeolocationByGPS();

        return view;
    }

    private void onLoginBtnClicked() {
        Session fbSession = Session.getActiveSession();
        if (fbSession != null && fbSession.isOpened()) {
//            pendingFbJobs = 3;
            loadFbBasicInfo();
//            loginWithFb();
        }
        else {
            fbLoginBtn.performClick();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        active = false;
        fbHelper.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        active = true;
        fbHelper.onResume();
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isregister(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
        fbHelper.onDestroy();
    }

    public void onEvent(Object event) {
        Message message = (Message)event;

        switch (message.what) {
            case GEO_LOCATION_UPDATED:
                geolocation = (GeoLocation) message.obj;
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        fbHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fbHelper.onActivityResult(requestCode, resultCode, data);
    }

    /*
      Callback when Facebook session state changed
    */
    private void onFbSessionStateChange(Session fbSession, SessionState fbSessionState, Exception ex) {
        L.d("FB session state change, state: %s", fbSessionState);

        if (fbSession != null && fbSession.isOpened()) {
            showProgress();
//            pendingFbJobs = 3;
            return;
        }

        if (ex != null) {
            DisplayUtils.toast(getString(R.string.err_fb_login_failed_with_reason,
                    ex.getMessage()));
        } else if (fbSessionState == SessionState.CLOSED_LOGIN_FAILED) {
            DisplayUtils.toast(R.string.err_fb_login_failed);
        }
    }

    private void showProgress() {
        if (fbAuthProg != null && fbAuthProg.isShowing()) {
            return;
        }

        fbAuthProg = new MyProgressDialog(getActivity(), getString(R.string.action_sign_in_short));
        fbAuthProg.show();
    }

    /*
      Callback when Facebook user basic profile is fetched.
    */
    private void onFbUserInfoFetched(GraphUser user) {
        if (user == null) {
            L.e("Facebook user info is null");
            pendingFbJobs = 0;
            return;
        }

//        loadFbProfilePicture(user.getId());
//        loadFbExtraInfo(user.getId());

        L.d("FB userInfo fetched, id: %s, name: %s", user.getId(), user.getName());

        Bundle data = getPendingFbLoginData();
        data.putString("fbUid", user.getId());
        data.putString("fbName", user.getName());
        data.putString("fbToken", Session.getActiveSession().getAccessToken());
        data.putLong("fbTokenExpires", Session.getActiveSession().getExpirationDate().getTime());
        data.putString("fbBasicInfo", user.getInnerJSONObject().toString());

//        pendingFbJobs--;
        loginWithFb();
    }

    private void loadFbBasicInfo() {
        Session fbSession = Session.getActiveSession();
        if (fbSession == null || !fbSession.isOpened()) {
            return;
        }

        showProgress();

        Request.newMeRequest(fbSession, new Request.GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser user, Response response) {
                onFbUserInfoFetched(user);
            }
        }).executeAsync();
    }

    private void loadFbExtraInfo(String uid) {
        Session fbSession = Session.getActiveSession();

        Bundle params = new Bundle();
        params.putBoolean("redirect", false);
        params.putString("fields", "age_range,currency");

//        Settings.addLoggingBehavior(LoggingBehavior.REQUESTS);
//        Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_RAW_RESPONSES);

        new Request(fbSession, "/me", params, HttpMethod.GET, new Request.Callback() {
            @Override
            public void onCompleted(Response resp) {
                if (resp.getError() == null) {
                    GraphObject go = resp.getGraphObject();
                    if (go != null && go.getInnerJSONObject() != null) {
                        JSONObject data = go.getInnerJSONObject();
                        getPendingFbLoginData().putString("fbExtraInfo", data.toString());
                    }
                } else {
                    FacebookRequestError err = resp.getError();
                    L.e("Facebook api error: [%s] %s", err.getErrorCode(), err.getErrorMessage());
                }

                pendingFbJobs--;
                loginWithFb();
            }
        }).executeAsync();
    }

    private void loadFbProfilePicture(String uid) {
        Session fbSession = Session.getActiveSession();

        Bundle params = new Bundle();
        params.putBoolean("redirect", false);

//        Settings.addLoggingBehavior(LoggingBehavior.REQUESTS);
//        Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_RAW_RESPONSES);

        new Request(fbSession, "/me/picture", params, HttpMethod.GET, new Request.Callback() {
            @Override
            public void onCompleted(Response resp) {
                if (resp.getError() == null) {
                    GraphObject go = resp.getGraphObject();
                    if (go != null && go.getProperty("data") != null) {
                        JSONObject data = (JSONObject) go.getProperty("data");
                        String picUrl = data.optString("url", null);
                        L.d("FB get profile picture: %s", picUrl);

                        TrackingUtils.trackAFEvent("loginwithfacebook", null);

                        if (picUrl != null) {
                            getPendingFbLoginData().putString("fbProfilePicture", picUrl);
                        }
                    }
                } else {
                    FacebookRequestError err = resp.getError();
                    L.e("Facebook api error: [%s] %s", err.getErrorCode(), err.getErrorMessage());
                }

                pendingFbJobs--;
                loginWithFb();
            }
        }).executeAsync();
    }

    /*
     * 检查Facebook认证流程是否已经结束，认证成功则触发登录逻辑
     */
    private void loginWithFb() {
        if (pendingFbJobs > 0) {
            return;
        }

        Bundle data = getPendingFbLoginData();
        loginModel.loginFacebook(data.getString("fbUid"),
                data.getString("fbName"),
                data.getString("fbToken"),
                data.getLong("fbTokenExpires"),geolocation);

        FiveMilesApp.loadAdvertisingInfo();  // refresh advertising info
    }

    private Bundle getPendingFbLoginData() {
        if (fbLoginData == null) {
            fbLoginData = new Bundle();
        }
        return fbLoginData;
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) throws JSONException {
        if (url.startsWith(ApiInterface.LOGIN_FACEBOOK)) {
            if (fbAuthProg != null && fbAuthProg.isShowing()) {
                fbAuthProg.dismiss();
                fbAuthProg = null;
            }

            if (jo == null) {
                return;
            }
            onFbLoginSuccess();
        }
    }

    private void onFbLoginSuccess()
    {
        // save user info in session
        SESSION session = SESSION.getInstance();
        session.isAnonymous = false;
        session.userId = loginModel.signupResp.uid;
        session.setFbUid(loginModel.signupResp.fbUid);
        session.token = loginModel.signupResp.token;
        session.nickname = loginModel.signupResp.nickname;
        session.avatarUrl = loginModel.signupResp.portrait;
        session.email = loginModel.signupResp.email;
        session.createdAt = loginModel.signupResp.createdAt;
        session.lastLogin = loginModel.signupResp.lastLogin;

        Session fbSession = Session.getActiveSession();
        if (fbSession != null && fbSession.isOpened()) {
            session.setFbTokenExpires(fbSession.getExpirationDate().getTime());
        }

        session.save();

        boolean sessionTokenNotEmpty = !TextUtils.isEmpty(session.token);
        if (sessionTokenNotEmpty) {
            Message msg = new Message();
            msg.what = MessageConstant.SIGN_UP_SUCCESS;
            EventBus.getDefault().post(msg);

            if (listener != null) {
                listener.onLoginSuccess();
            }
        }

        // 成功注册后，若个推已返回client id，注册推送
        boolean isClientIdExist = ModelUtils.isNotEmpty(SESSION.getInstance().clientId);
        if (isClientIdExist) {
            loginModel.registerDevice(GETUI_PROVIDER, SESSION.getInstance().clientId);
        }
    }

}
