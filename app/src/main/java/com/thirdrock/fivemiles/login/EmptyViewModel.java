package com.thirdrock.fivemiles.login;

import com.thirdrock.framework.ui.viewmodel.AbsViewModel;

import javax.inject.Inject;

/**
 * ViewModel that works as place holder, do nothing.
 * Created by jubin on 16/3/15.
 */
public class EmptyViewModel extends AbsViewModel<EmptyViewModel.Property>{
    public enum Property {
        place_holder
    }

    @Inject
    public EmptyViewModel() {
        // empty
    }
}
