package com.thirdrock.fivemiles.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.external.eventbus.EventBus;
import com.insthub.fivemiles.Activity.EmailLoginActivity;
import com.insthub.fivemiles.Activity.MainTabActivity;
import com.insthub.fivemiles.Activity.RegisterActivity;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.MessageConstant;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.activity.AbsFragmentActivity;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;

import javax.inject.Inject;

import butterknife.OnClick;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REDIRECT_INTENT;

/**
 * Created by jubin on 16/3/15.
 */
@Deprecated
public class SignInOrUpActivity extends AbsFragmentActivity {
    private Intent redirectIntent;

    @Inject
    EmptyViewModel viewModel;

    @Override
    protected String getScreenName() {
        // screen name是为GA使用的
        return FiveMilesAppConst.VIEW_LOG_IN;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_sign_in_or_up;
    }

    @Override
    protected AbsViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {

        redirectIntent = getIntent().getParcelableExtra(ACT_PARA_REDIRECT_INTENT);

        FacebookLoginFragment fbFrm = new FacebookLoginFragment();
        fbFrm.setLoginListener(new FacebookLoginFragment.LoginListener() {
            @Override
            public void onLoginSuccess() {
                startActivity(new Intent(SignInOrUpActivity.this, MainTabActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .putExtra("from", FiveMilesAppConst.VIEW_LOG_IN)
                        .putExtra(ACT_PARA_REDIRECT_INTENT, redirectIntent));
                trackTouch("loginwithfacebook");
                finish();

                Message msg = new Message();
                msg.what = MessageConstant.SIGN_UP_SUCCESS;
                EventBus.getDefault().post(msg);
            }
        });
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fb_login_placeholder, fbFrm);
        ft.commit();
    }

    @Override
    public void onPropertyChanged(Enum property, Object oldValue, Object newValue) throws Exception {
        // empty
    }

    @OnClick(R.id.btn_sign_in)
    void redirectToSignIn() {
        startActivity(new Intent(this, EmailLoginActivity.class)
                .putExtra(ACT_PARA_REDIRECT_INTENT, redirectIntent));
        trackTouch("signin");
        TrackingUtils.trackAFEvent("signin_confirm", null);
    }

    @OnClick(R.id.btn_register)
    public void redirectToRegister(View v) {
        startActivity(new Intent(this, RegisterActivity.class)
                .putExtra(ACT_PARA_REDIRECT_INTENT, redirectIntent));
        trackTouch("signup");
        TrackingUtils.trackAFEvent("signup_confirm", null);
    }

    @OnClick(R.id.top_view_back)
    public void onBack() {
        finish();
        trackTouch("close");
    }

    @Override
    protected boolean isLoginProtected() {
        return false;
    }
}
