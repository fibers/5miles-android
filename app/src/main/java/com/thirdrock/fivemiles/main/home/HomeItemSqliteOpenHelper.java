package com.thirdrock.fivemiles.main.home;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.insthub.fivemiles.FiveMilesApp;

/**
 * 处理首页瀑布流中items的SQLiteOpenHelper，
 * Created by jubin on 22/5/15.
 */
public class HomeItemSqliteOpenHelper extends SQLiteOpenHelper {
    private static final String FM_DB_HOME_ITEM = "fm_home_item";
    private static final int THUMB_DB_V = 2;

    public static final String F_ITEM_ID = "id";  // index of the item thumb
    public static final String F_ITEM_DATA = "item_data";  // json string of the item thumb
    public static final String TBL_HOME_NEARBY_ITEMS = "tbl_home_item_thumbs";
    public static final String TBL_HOME_FOLLOWINGS = "tbl_home_followings";

    private static final String SQL_C_TBL_NEARBY_ITEMS = "CREATE TABLE " + TBL_HOME_NEARBY_ITEMS + " (" +
            F_ITEM_ID + " INT PRIMARY KEY ASC," +
            F_ITEM_DATA + " VARCHAR);";

    private static final String SQL_C_TBL_FOLLOWINGS = "CREATE TABLE " + TBL_HOME_FOLLOWINGS + " (" +
            F_ITEM_ID + " INT PRIMARY KEY ASC," +
            F_ITEM_DATA + " VARCHAR);";

    private static final HomeItemSqliteOpenHelper instance =
            new HomeItemSqliteOpenHelper(FiveMilesApp.appCtx);


    private HomeItemSqliteOpenHelper(Context context) {
        super(context, FM_DB_HOME_ITEM, null, THUMB_DB_V);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_C_TBL_NEARBY_ITEMS);
        db.execSQL(SQL_C_TBL_FOLLOWINGS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == 1) {
            db.execSQL(SQL_C_TBL_FOLLOWINGS);
        }
    }

    /**
     * Singleton instance
     * @return HomeItemSqliteOpenHelper singleton instance
     */
    public static HomeItemSqliteOpenHelper getInstance() {
        return instance;
    }

}
