package com.thirdrock.fivemiles.main.listing;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Pair;

import com.BeeFramework.Utils.ImageUtil;
import com.insthub.fivemiles.Utils.LocationManagerUtil;
import com.thirdrock.domain.EnumDeliveryType;
import com.thirdrock.domain.GeoLocation;
import com.thirdrock.domain.ImageInfo;
import com.thirdrock.domain.Item;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.rest.SingleResponseRestObserver;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.Ignore;
import com.thirdrock.protocol.CloudSignature;
import com.thirdrock.protocol.ListItemReq;
import com.thirdrock.repository.ItemRepository;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subjects.AsyncSubject;

import static com.insthub.fivemiles.FiveMilesAppConst.IMAGE_MAX_HEIGHT;
import static com.insthub.fivemiles.FiveMilesAppConst.IMAGE_MAX_WIDTH;
import static com.insthub.fivemiles.FiveMilesAppConst.ITEM_MAX_IMAGES;
import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_APP_STATE;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_AUTO_SHARE;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;
import static com.thirdrock.fivemiles.util.ModelUtils.isValidPrice;

/**
 * View model for ListItemActivity
 * Created by ywu on 14-10-12.
 */
public class ListItemViewModel extends AbsViewModel<ListItemViewModel.Property> {

    public enum Property {
        images, validation, item_id, sharing_content
    }

    private class ItemIdObserver extends SingleResponseRestObserver<String> {
        private ItemIdObserver() {
            super("post_item");
        }

        @Override
        public void onCompleted() {
            emitMajorJobCompleted();
            onListingCompleted();
        }

        @Override
        public void onError(Throwable e) {
            emit(Property.item_id, e);
            emitMajorJobCompleted();
            onListingFailed();
        }

        @Override
        public void doOnNext(String newItemId) {
            emit(Property.item_id, null, newItemId);
        }
    }

    @Inject
    ItemRepository itemRepository;

    // view states accessible from view
    boolean editMode;
    int categoryId = -1;
    String currency, brand;
    EnumDeliveryType deliveryType;
    final List<String> itemImages = new ArrayList<>();

    // internal states
    private Context context;
    private boolean autoShare = true;
    private Item item;
    private File audioFile;
    private volatile String audioCloudUrl;  // link to the audio on cloud
    private boolean listingInProgress;
    private List<Observable<Void>> pendingJobs = new LinkedList<>();
    private final Map<String, String> compressedImages = Collections.synchronizedMap(
            new HashMap<String, String>());
    private final Map<String, ImageInfo> imageInfoMap = Collections.synchronizedMap(
            new HashMap<String, ImageInfo>());

    @Inject
    public ListItemViewModel(Context context) {
        this.context = context;
        loadSharingState();
    }

    public boolean acceptMoreImage() {
        return itemImages.size() < ITEM_MAX_IMAGES;
    }

    public int getAvailableImageCount() {
        return ITEM_MAX_IMAGES - itemImages.size();
    }

    public void setEditItem(Item item) {
        editMode = true;
        this.item = item;
        categoryId = item.getCategoryId();
        currency = item.getCurrencyCode();

        for (ImageInfo img : item.getImages()) {
            imageInfoMap.put(img.getUrl(), img);
            itemImages.add(img.getUrl());
        }

        audioCloudUrl = item.getMediaLink();
    }

    /**
     * let the properties from ListItemViewModel into item object.
     * @return item
     */
    public Item parseToItem(){
        Item item = new Item();
        item.setCategoryId(categoryId);
        item.setCurrency(currency);
        ArrayList<ImageInfo> imgArrayList = new ArrayList<>();
        ArrayList<String> imgReadyToLoad = new ArrayList<>();//当用户操作过快导致尚未将后台压缩上传的图片URL返回时,存入Item.cachedUrl.

        for(String id : itemImages){

            if(imageInfoMap.get(id) != null) {
                imgArrayList.add(imageInfoMap.get(id));
            } else {
                imgReadyToLoad.add(id);
            }
        }
        item.setImages(imgArrayList);
        item.setImgReadyToLoad(imgReadyToLoad);
        item.setMediaLink(audioCloudUrl);
        item.setBrand(brand);
        return item;
    }

    /**
     * psrse the Item object into this.
     * @param item item
     */
    public void parseFromItem(Item item){
        this.categoryId = item.getCategoryId();
        this.currency = item.getCurrencyCode();

        this.imageInfoMap.clear();
        this.itemImages.clear();
        for (ImageInfo img : item.getImages()) {
            this.imageInfoMap.put(img.getUrl(), img);
            this.itemImages.add(img.getUrl());
        }
        //如果存在尚未上传的图片,则重新append并上传
        List<String> imgReadyList =  item.getImgReadyToLoad();
        for(String id : imgReadyList) {
            appendImage(id);
        }
        this.audioCloudUrl = item.getMediaLink();
        brand = item.getBrand();
    }


    public void appendImage(String imgPath) {
        // show image immediately
        itemImages.add(imgPath);
        emit(Property.images, null, itemImages);

        // compress & upload in background
        submitImageUploadJob(imgPath);
    }

    @SuppressWarnings("unchecked")
    private void submitImageUploadJob(String imgPath) {
        if (imageInfoMap.containsKey(imgPath)) {
            return;  // already uploaded
        }

        L.d("append image upload job %s", imgPath);
        pendingJobs.add(uploadImage(imgPath));
    }

    private Observable<Void> uploadImage(final String imgPath) {
        Observable<String> compress =
                Observable.create(new Observable.OnSubscribe<String>() {
                    @Override
                    public void call(Subscriber<? super String> subscriber) {
                        String compressedImgPath = compressedImages.get(imgPath);
                        if (compressedImgPath == null) {
                            compressedImgPath = ImageUtil.adjustImage(imgPath, IMAGE_MAX_HEIGHT, IMAGE_MAX_WIDTH);
                            compressedImages.put(imgPath, compressedImgPath);
                        }

                        subscriber.onNext(compressedImgPath);
                        subscriber.onCompleted();
                    }
                });

        Observable<CloudSignature> sign = itemRepository.signCloudUpload();

        // compress and sign the image, and then upload, make it hot using a Subject
        AsyncSubject<Void> hotUploadJob = AsyncSubject.create();

        Observable.zip(compress, sign, new Func2<String, CloudSignature, Pair<String, CloudSignature>>() {
            @Override
            public Pair<String, CloudSignature> call(String zImgPath, CloudSignature sign) {
                return new Pair<>(zImgPath, sign);
            }
        }).flatMap(new Func1<Pair<String, CloudSignature>, Observable<Map>>() {
            @Override
            public Observable<Map> call(Pair<String, CloudSignature> data) {
                // upload image
                return itemRepository.uploadToCloud(context, data.first, data.second);
            }
        }).map(new Func1<Map, Void>() {
            @Override
            public Void call(Map map) {
                String url = "";
                int width = 0, height = 0;
                if (map != null) {
                    if (map.containsKey("url")) {
                        url = (String) map.get("url");
                    }
                    if (map.containsKey("width")) {
                        width = (int) map.get("width");
                    }
                    if (map.containsKey("height")) {
                        height = (int) map.get("height");
                    }
                }
                ImageInfo imgInfo = new ImageInfo(url, width, height);
                imageInfoMap.put(imgPath, imgInfo);
                L.d("image upload result: %s -> %s", imgPath, imgInfo.getUrl());
                return null;
            }
        }).subscribeOn(Schedulers.io()).subscribe(hotUploadJob);

        return hotUploadJob;
    }

    public void removeImage(int i) {
        if (i >= 0 && i < itemImages.size()) {
            imageInfoMap.remove(itemImages.get(i));
            itemImages.remove(i);
            emit(Property.images, null, itemImages);
        }
    }

    public void uploadAudio(File audioFile) {
        this.audioFile = audioFile;

        if (audioFile != null) {
            L.d("append audio upload job");
            audioCloudUrl = null;  // clear previous audion resource
            pendingJobs.add(doUploadAudio(audioFile));
        }
    }

    public void removeAudio() {
        L.d("clear audio file state");
        audioFile = null;
        audioCloudUrl = null;
    }

    private Observable<Void> doUploadAudio(final File audioFile) {
        // compress and sign the image, and then upload, make it hot using a Subject
        AsyncSubject<Void> hotUploadJob = AsyncSubject.create();

        itemRepository.signCloudUpload().flatMap(new Func1<CloudSignature, Observable<Map>>() {
            @Override
            public Observable<Map> call(CloudSignature signature) {
                return itemRepository.uploadToCloud(context,
                        audioFile.getAbsolutePath(),
                        signature);
            }
        }).map(new Func1<Map, Void>() {
            @Override
            public Void call(Map map) {
                if (map != null && map.containsKey("url")) {
                    audioCloudUrl = (String) map.get("url");
                }
                L.d("audio upload result: %s", audioCloudUrl);
                return null;
            }
        }).subscribeOn(Schedulers.io()).subscribe(hotUploadJob);

        return hotUploadJob;
    }

    // list the item into market
    public void listNewItem(String title, String desc, String price, String originPrice, boolean autoShare, GeoLocation geolocation) {
        if (listingInProgress) {
            return;
        }

        L.d("begin listing/editing item");
        String theTitle = title.trim();
        String theDesc = desc.trim();
        String thePrice = price.trim();
        String theOriginPrice = originPrice.trim();

        // check upload jobs, redo failed jobs
        listingInProgress = true;
        emitMajorJobStarted();
        ensureUploaded();

        // list the item
        listItemWhenPendingJobDone(theTitle, theDesc, thePrice, theOriginPrice, autoShare, geolocation);
    }

    // make sure that all media files are uploaded, retry failed ones
    private void ensureUploaded() {
        cancelPendingJobs();  // 此时尚未完成的job会被取消并重新执行，但可保证失败的job不会影响下一步执行

        for (String imgPath : itemImages) {
            submitImageUploadJob(imgPath);
        }

        if (audioFile != null && audioCloudUrl == null) {
            uploadAudio(audioFile);
        }
    }

    private void cancelPendingJobs() {
        if (pendingJobs.isEmpty()) {
            return;
        }

        // un-subscribe all observables, to avoid memory leak
        Observable.merge(pendingJobs)
                .ignoreElements()
                .subscribeOn(Schedulers.computation())
                .subscribe(Ignore.getInstance())
                .unsubscribe();

        pendingJobs.clear();
    }

    // list the new item after pending jobs (images/audio uploads) complete
    @SuppressWarnings("unchecked")
    private void listItemWhenPendingJobDone(final String title,
                                            final String desc,
                                            final String price,
                                            final String originPrice,
                                            final boolean autoShare, final GeoLocation geolocation) {
        Func1<Void, Observable<String>> listingJob;
        Observable<Void> pendingJob = pendingJobs.isEmpty() ?
                Observable.<Void>just(null) :
                Observable.merge(pendingJobs);

        // editing existed item
        if (editMode) {
            listingJob = new Func1<Void, Observable<String>>() {
                @Override
                public Observable<String> call(Void aVoid) {
                    ListItemReq req = prepareReq(title, desc, price, originPrice, false, geolocation);
                    req.itemId = item.getId();
                    return itemRepository.editItem(req).map(new Func1<Void, String>() {
                        @Override
                        public String call(Void aVoid) {
                            return item.getId();
                        }
                    });
                }
            };
        } else {
            // listing new item
            TrackingUtils.trackAFEvent("publish", null);

            listingJob = new Func1<Void, Observable<String>>() {
                @Override
                public Observable<String> call(Void aVoid) {
                    ListItemReq req = prepareReq(title, desc, price, originPrice, autoShare, geolocation);
                    return itemRepository.listItem(req);
                }
            };
        }

        // use takeLast to make sure invoke only once
        pendingJob.takeLast(1).
                flatMap(listingJob).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(new ItemIdObserver().reset());
    }

    private ListItemReq prepareReq(String title, String desc, String price, String originPrice, boolean autoShare, GeoLocation geolocation) {
        ListItemReq req = new ListItemReq();
        req.title = title;
        req.description = desc;
        req.currency = currency;
        req.price = Double.valueOf(price);
        req.deliveryType = deliveryType;
        req.categoryId = categoryId;
        req.brand = brand;
        if(geolocation != null) {
            req.city = geolocation.getCity();
            req.region = geolocation.getRegion();
            req.country = geolocation.getCountry();
            req.latitude = geolocation.getLatitude();
            req.longitude = geolocation.getLongitude();
        }else{
            req.latitude = LocationManagerUtil.getLatitudeInSession();
            req.longitude = LocationManagerUtil.getLongitudeInSession();
        }
        req.mediaLink = audioCloudUrl;
        req.autoShare = autoShare;

        if (isNotEmpty(originPrice)) {
            req.originPrice = Double.valueOf(originPrice);
        }

        List<ImageInfo> imgs = new ArrayList<>();
        for (String imgPath : itemImages) {
            ImageInfo info = imageInfoMap.get(imgPath);
            if (info != null) {
                imgs.add(info);
            }
        }
        req.setImageList(imgs);
        L.d("list/edit item mediaLink: %s", req.mediaLink);

        if (imgs.isEmpty()) {
            throw new RuntimeException("At least one image is required");
        }
        return req;
    }

    public int validateItem(String title, String desc, String price, String originPrice) {
        int err = 0;
        String theTitle = title.trim();
        String thePrice = price.trim();
        String theOriginPrice = originPrice.trim();

        if (itemImages.isEmpty()) {
            err = R.string.select_photo;
        } else if (isEmpty(theTitle)) {
            err = R.string.input_title;
        } else if (categoryId < 0) {
            err = R.string.select_category_;
        } else if (!isValidPrice(thePrice)) {
            err = R.string.error_price_required;
        } else if (isNotEmpty(theOriginPrice) && !isValidPrice(theOriginPrice)) {
            err = R.string.error_origin_price_invalid;
        }

        return err;
    }

    public void reset() {
        categoryId = -1;
        itemImages.clear();
        compressedImages.clear();
        imageInfoMap.clear();
        cancelPendingJobs();
        listingInProgress = false;
    }

    // mark the current request as succeed
    private void onListingCompleted() {
        reset();
    }

    // mark the current request as failed
    private void onListingFailed() {
        cancelPendingJobs();
        listingInProgress = false;
    }

    public boolean isDirty() {
        return categoryId > -1 || !itemImages.isEmpty() ||
                audioFile != null || isNotEmpty(brand);
    }

    private void loadSharingState() {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_APP_STATE,
                Context.MODE_PRIVATE);
        autoShare = prefs.getBoolean(PREF_KEY_AUTO_SHARE, true);
    }

    private void saveSharingState() {
        context.getSharedPreferences(PREFS_APP_STATE, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(PREF_KEY_AUTO_SHARE, this.autoShare)
                .apply();
    }

    public boolean isAutoShare() {
        return autoShare;
    }

    public void setAutoShare(boolean autoShare) {
        this.autoShare = autoShare;
        saveSharingState();
    }
}
