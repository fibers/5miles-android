package com.thirdrock.fivemiles.main.home;

import com.thirdrock.domain.ItemThumb;

import rx.Observable;

/**
 * Observable wrapper for ItemThumb
 * Created by ywu on 15/2/17.
 */
public class ObservableItemThumb extends ItemThumb {

    private Observable<ItemThumb> observable;

    public ObservableItemThumb(Observable<ItemThumb> observable) {
        this.observable = observable;
    }

    public Observable<ItemThumb> getObservable() {
        return observable;
    }
}
