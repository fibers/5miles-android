package com.thirdrock.fivemiles.main.listing;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.StatFs;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.InputFilter;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.external.activeandroid.util.Log;
import com.facebook.AppEventsLogger;
import com.facebook.Session;
import com.facebook.SessionDefaultAudience;
import com.facebook.SessionState;
import com.facebook.widget.LoginButton;
import com.insthub.fivemiles.Activity.CategoryActivity;
import com.insthub.fivemiles.Adapter.AddPicAdapter;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.SESSION;
import com.insthub.fivemiles.Utils.ResourceDownloader;
import com.insthub.fivemiles.View.HorizontalListView;
import com.thirdrock.domain.CategoryInfo;
import com.thirdrock.domain.GeoLocation;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.Item__JsonHelper;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.common.brand.BrandActivity;
import com.thirdrock.fivemiles.framework.activity.AbsActivity;
import com.thirdrock.fivemiles.framework.activity.SelectionListActivity;
import com.thirdrock.fivemiles.framework.view.AudioProgressView;
import com.thirdrock.fivemiles.framework.view.BottomConfirmDialog;
import com.thirdrock.fivemiles.framework.view.NumberInputFilter;
import com.thirdrock.fivemiles.framework.view.RecordingDecibelPopupWindow;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.main.home.CategoryHelper;
import com.thirdrock.fivemiles.util.Currencies;
import com.thirdrock.fivemiles.util.Deliveries;
import com.thirdrock.fivemiles.util.Delivery;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.EventUtils;
import com.thirdrock.fivemiles.util.FacebookLinker;
import com.thirdrock.fivemiles.util.FileUtils;
import com.thirdrock.fivemiles.util.LocationUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.Recorder;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.SimpleObserver;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTouch;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.schedulers.Schedulers;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_EDIT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_IMAGE_PATHS;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELECTION_DATA_LIST;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELECTION_SCREEN_NAME;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELECTION_TITLE;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_RESULT_SELECTION_ITEM;
import static com.insthub.fivemiles.FiveMilesAppConst.FB_PERMISSION_PUBLISH;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_DELIVERY;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_LIST_ITEM;
import static com.insthub.fivemiles.MessageConstant.ADD_PHOTO;
import static com.insthub.fivemiles.MessageConstant.GEO_LOCATION_UPDATED;
import static com.insthub.fivemiles.MessageConstant.REMOVE_PHOTO;
import static com.thirdrock.fivemiles.common.gallery.Action.ACTION_MULTIPLE_PICK;
import static com.thirdrock.fivemiles.common.gallery.Action.MAXIMUM;
import static com.thirdrock.fivemiles.main.listing.ListItemViewModel.Property;
import static com.thirdrock.fivemiles.util.DisplayUtils.formatPriceDigits;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;
import static com.thirdrock.fivemiles.util.ModelUtils.isValidPrice;

/**
 * Activity for publish item
 * Created by ywu on 14-10-11.
 */
public class ListItemActivity extends AbsActivity<ListItemViewModel, Property> implements Recorder.OnStateChangedListener {

    private static final int CAMERA_REQUEST = 1;
    private static final int PHOTO_REQUEST = 2;
    private static final int CATEGORY_REQUEST = 3;
    private static final int SHARE_REQUEST = 5;
    private static final int BRAND_REQUEST = 6;
    private static final int DELIVERY_REQUEST = 7;
    private static final int MIN_AUDIO_LEN = 3000;  // 最短的语音时长 milliseconds
    private static final int RECORD_EXTRA_TIME = 1000;  // 额外的录音时间，避免截断用户语音 milliseconds

    private AppEventsLogger fbEventsLogger;

    @Inject
    ListItemViewModel viewModel;

    @Inject
    Currencies currencies;

    @Inject
    Deliveries deliveries;

    @InjectView(R.id.top_container)
    View viewRootCtnr;

    @InjectView(R.id.add_photo_list)
    HorizontalListView lstImages;

    @InjectView(R.id.add_title)
    EditText edtTitle;

    @InjectView(R.id.add_select_category)
    TextView txtCategory;

    @InjectView(R.id.add_select_brand)
    TextView txtBrand;

    @InjectView(R.id.add_select_delivery)
    TextView txtDelivery;

    @InjectView(R.id.add_price)
    EditText edtPrice;

    @InjectView(R.id.add_origin_price)
    EditText edtOriginPrice;

    @InjectView(R.id.add_describe)
    EditText edtDesc;

    @InjectView(R.id.share_to_facebook_container)
    View viewFbShare;

    @InjectView(R.id.share_to_facebook_switch)
    CheckBox cbxFbShare;

    @InjectView(R.id.item_audio_wrapper)
    View audioWrapper;

    @InjectView(R.id.btn_play_audio)
    View btnPlayAudio;

    @InjectView(R.id.item_audio_playback_elapse)
    TextView txtAudioElapse;

    @InjectView(R.id.item_audio_playback_duration)
    TextView txtAudioDuration;

    @InjectView(R.id.item_audio_prog)
    AudioProgressView progAudioPlayback;

    @InjectView(R.id.fb_login_btn)
    LoginButton btnFbLogin;

    private AddPicAdapter imageListAdapter;
    private File tmpCameraImageFile;
    private BottomConfirmDialog confirmDialog;
    private ValueAnimator audioLoadingAnimation;  // animations for loading audio
    private Subscription subsDownloadAudio;
    private CategoryHelper catgryHelper;
    private GeoLocation geolocation;

    private class FbPermissionCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            if (exception != null ||
                    !session.isOpened() ||
                    !session.getPermissions().contains(FB_PERMISSION_PUBLISH)) {
                // this means the user did not grant us write permissions, so we don't do implicit publishes
                if (exception != null) {
                    L.e("asking Facebook publish permission failed", exception);
                }
                submitListing(false);
            } else {
                submitListing(true);
            }
        }
    }

    private FacebookLinker fbLinker;
    private boolean linkingFb;

    @Override
    public ListItemViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected String getScreenName() {
        return VIEW_LIST_ITEM;
    }

    @Override
    protected int getContentView() {
        return R.layout.tab_add;
    }

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {

        fbEventsLogger = AppEventsLogger.newLogger(this);

        EventUtils.register(this);

        catgryHelper = new CategoryHelper();

        initImageList();
        initPriceEditor();
        initCurrency();
        initDelivery();
        initAudio(savedInstanceState);

        if (ACT_EDIT.equals(getIntent().getAction())) {
            Item item = (Item) getIntent().getSerializableExtra(ACT_PARA_ITEM);
            viewModel.setEditItem(item);
            populate(item);
        }

        viewFbShare.setVisibility(viewModel.editMode ? View.GONE : View.VISIBLE);  // 编辑模式不再分享
        cbxFbShare.setChecked(viewModel.isAutoShare());
        cbxFbShare.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                onFbShareSwitch(isChecked);
            }
        });

        fbLinker = new FacebookLinker(this,
                btnFbLogin,
                null,
                Arrays.asList(FB_PERMISSION_PUBLISH),
                new FacebookLinker.Callback() {
                    @Override
                    public boolean shouldSubmitLinkRequest() {
                        return linkingFb;
                    }

                    @Override
                    public void onCanceledOrFailed(int requestCode) {
                        linkingFb = false;
                        cbxFbShare.setChecked(false);
                        submitListing(false);
                    }

                    @Override
                    public void onComplete(int requestCode) {
                        linkingFb = false;
                        submitListing(true);
                    }
                });
        fbLinker.onCreate(savedInstanceState);

        LocationUtils.getGeolocationByGPS();
    }

    private void populate(Item item) {
        if (isNotEmpty(item.getTitle())) {
            edtTitle.setText(item.getTitle().trim());
        }

        if (isNotEmpty(item.getDescription())) {
            edtDesc.setText(item.getDescription().trim());
        }

        if(isValidPrice(item.getPrice())) {
            edtPrice.setText(formatPriceDigits(item.getPrice()));
        }

        if (isValidPrice(item.getOriginPrice())) {
            edtOriginPrice.setText(formatPriceDigits(item.getOriginPrice()));
        }

        int categoryId = item.getCategoryId();
        viewModel.categoryId = categoryId;

        CategoryInfo catgryInfo = catgryHelper.getCategoryById(categoryId);
        if (catgryInfo != null) {
            txtCategory.setText(catgryInfo.getTitle());
        }

        if (isNotEmpty(item.getBrand())) {
            txtBrand.setText(item.getBrand().trim());
            viewModel.brand = item.getBrand().trim();
        }

        setDelivery(Deliveries.getDelivery(this, item.getDeliveryType()));

        // show images
        imageListAdapter.notifyDataSetChanged();

        // 如果有音频资源，则预先下载
        if (ModelUtils.isNotEmpty(item.getMediaLink())) {
            if (audioLoadingAnimation == null) {
                initAudioLoadingAnimation();
            }
            btnPlayAudio.setEnabled(false);
            audioLoadingAnimation.start();

            subsDownloadAudio = ResourceDownloader.downloadResource(item.getMediaLink())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SimpleObserver<File>() {
                        @Override
                        public void onNext(File file) {
                            mRecorder.setSampleFile(file);
                            updateAudioUi();
                            btnPlayAudio.setEnabled(true);
                            audioLoadingAnimation.cancel();
                            btnPlayAudio.getBackground().setAlpha(0xff);
                        }
                    });
        }
    }

    private void initImageList() {
        imageListAdapter = new AddPicAdapter(this, viewModel.itemImages);
        lstImages.setAdapter(imageListAdapter);
    }

    private void updateImageList() {
        imageListAdapter.notifyDataSetChanged();
        if (viewModel.itemImages.size() >= FiveMilesAppConst.ITEM_MAX_IMAGES) {
            lstImages.setSelection(imageListAdapter.getCount() - 2);
        } else {
            lstImages.setSelection(imageListAdapter.getCount() - 1);
        }
    }

    private void initPriceEditor() {
        edtPrice.setFilters(new InputFilter[]{
                new NumberInputFilter(0, 9999999.99)
        });

        edtOriginPrice.setFilters(new InputFilter[]{
                new NumberInputFilter(0, 9999999.99)
        });
    }

    private void initCurrency() {
        viewModel.currency = currencies.getDefaultCurrency();
        edtPrice.setHint(getString(R.string.hint_listing_price, viewModel.currency));
        edtOriginPrice.setHint(getString(R.string.hint_origin_price, viewModel.currency));
    }

    private void initDelivery() {
        setDelivery(deliveries.getDefaultDelivery());
    }

    @OnClick({R.id.add_select_category, R.id.ic_select_category})
    @SuppressWarnings("unused")
    void onSelectCategory() {
        startActivityForResult(new Intent(this, CategoryActivity.class), CATEGORY_REQUEST);
    }

    @OnClick({R.id.add_select_brand, R.id.ic_select_brand})
    @SuppressWarnings("unused")
    void onSelectBrand() {
        startActivityForResult(new Intent(this, BrandActivity.class), BRAND_REQUEST);
        trackTouch("brand_select");
    }

    @OnClick({R.id.add_select_delivery, R.id.ic_select_delivery})
    @SuppressWarnings("unused")
    void onSelectDelivery() {
        Intent intent = new Intent(this, SelectionListActivity.class);
        intent.putExtra(ACT_PARA_SELECTION_TITLE, getString(R.string.title_delivery));
        intent.putExtra(ACT_PARA_SELECTION_DATA_LIST, Deliveries.getAvailableDeliveries(this));
        intent.putExtra(ACT_PARA_SELECTION_SCREEN_NAME, VIEW_DELIVERY);
        startActivityForResult(intent, DELIVERY_REQUEST);
        trackTouch("delivery_select");
    }

    void onFbShareSwitch(boolean checked) {
        viewModel.setAutoShare(checked);
        TrackingUtils.trackTouch(VIEW_LIST_ITEM, checked ? "onfacebook" : "offfacebook");
    }

    @OnClick(R.id.tab_add_title_cancel)
    @SuppressWarnings("unused")
    void onCancel() {
        if (isDirty()) {
            closeKeyboard();
            showConfirmDialog();
        } else {
            naviBack();
        }
    }

    @Override
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {
        switch (property) {
            case images:
                updateImageList();
                break;
            case item_id:
                onItemListed((String) newValue);
                break;
            default:
                break;
        }
    }

    @Override
    public void onError(Property property, Throwable e) {
        switch (property) {
            case sharing_content:
                naviBack();
                break;
            default:
                super.onError(property, e);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventUtils.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        fbLinker.onStop();
        EventUtils.unregister(this);
        if (mRecorder != null) {
            mRecorder.stop();
        }
        if (audioLoadingAnimation != null && audioLoadingAnimation.isRunning()) {
            audioLoadingAnimation.cancel();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fbLinker.onDestroy();
        EventUtils.unregister(this);
        if (mSDCardMountEventReceiver != null) {
            unregisterReceiver(mSDCardMountEventReceiver);
            mSDCardMountEventReceiver = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        fbEventsLogger.logEvent(VIEW_LIST_ITEM);

        fbLinker.onResume();
        FacebookLinker.validateToken(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        fbLinker.onPause();
    }


    @SuppressWarnings("unused")
    public void onEvent(Object event) {
        Message message = (Message) event;
        if (message.what == ADD_PHOTO) {
            if (viewModel.acceptMoreImage()) {
                showPhotoDialog();
            }
        } else if (message.what == REMOVE_PHOTO) {
            int position = message.arg1;
            viewModel.removeImage(position);
            trackTouch("photo_delete");
        } else if (message.what == GEO_LOCATION_UPDATED){
            geolocation = (GeoLocation) message.obj;
        }
    }

    // TODO extract photo utils
    private void showPhotoDialog() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.photo_dialog_view, null);

        final Dialog mDialog = new Dialog(this, R.style.dialog);
        mDialog.setContentView(view);
        mDialog.setCanceledOnTouchOutside(true);

        View photoAlbum = view.findViewById(R.id.photo_album);
        View camera = view.findViewById(R.id.photo_camera);
        mDialog.show();

        photoAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                int count = viewModel.getAvailableImageCount();
                if (count > 0) {
                    trackTouch("photo_album");

                    Intent intent = new Intent();
                    intent.setAction(ACTION_MULTIPLE_PICK);
                    intent.putExtra(MAXIMUM, count);
                    startActivityForResult(Intent.createChooser(intent,
                            getString(R.string.title_choose_photo)), PHOTO_REQUEST);
                }
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                int count = viewModel.getAvailableImageCount();
                if (count <= 0) {
                    return;
                }

                trackTouch("photo_take");
                try {
                    tmpCameraImageFile = FileUtils.getTempCameraImage();

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tmpCameraImageFile));
                    intent.putExtra("return-data", false);
                    startActivityForResult(intent, CAMERA_REQUEST);
                } catch (IOException e) {
                    L.e(e);
                }
            }
        });
    }

    @Override
    protected void doOnActivityResult(int requestCode, int resultCode, Intent data) {
        fbLinker.onActivityResult(requestCode, resultCode, data);

        if (SHARE_REQUEST == requestCode) {
            naviBack();
            return;
        }

        Session session = Session.getActiveSession();
        if (session != null) {
            session.onActivityResult(this, requestCode, resultCode, data);
        }

        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case PHOTO_REQUEST:
                onPhotoChoosen(data);
                break;
            case CAMERA_REQUEST:
                onPhotoTaken(data);
                break;
            case CATEGORY_REQUEST:
                onCategorySelected(data);
                break;
            case BRAND_REQUEST:
                onBrandSelected(data);
                break;
            case DELIVERY_REQUEST:
                onDeliverySelected(data);
                break;
            default:
                break;
        }
    }

    private void onBrandSelected(Intent data) {
        String brand = data.getStringExtra(BrandActivity.RESULT_KEY_BRAND_NAME);
        txtBrand.setText(brand);
        viewModel.brand = brand;
    }

    // TODO extract photo utils
    private void onPhotoChoosen(Intent data) {
        String[] imgPaths = data.getStringArrayExtra(ACT_PARA_IMAGE_PATHS);
        if (imgPaths == null) {
            return;
        }

        for (String imgPath : imgPaths) {
            viewModel.appendImage(imgPath);
        }
    }

    private void onPhotoTaken(Intent data) {
        if (tmpCameraImageFile != null && tmpCameraImageFile.exists()) {
            viewModel.appendImage(tmpCameraImageFile.getAbsolutePath());
            FileUtils.refreshGallery(this, tmpCameraImageFile);  // make image visible in gallery
        }
    }

    private void onCategorySelected(Intent data) {
        viewModel.categoryId = data.getIntExtra("categoryId", 0);
        txtCategory.setText(data.getStringExtra("categoryTitle"));
    }

    private void onDeliverySelected(Intent data) {
        Delivery delivery = (Delivery) data.getSerializableExtra(ACT_RESULT_SELECTION_ITEM);
        setDelivery(delivery);
    }

    private void setDelivery(Delivery delivery) {
        deliveries.setDefaultDelivery(delivery);  // cache selected delivery method
        viewModel.deliveryType = delivery.getValue();
        txtDelivery.setText(delivery.getName());
    }

    @OnClick(R.id.add_publish)
    @SuppressWarnings("unused")
    void listNewItem() {
        int err = viewModel.validateItem(
                edtTitle.getText().toString(),
                edtDesc.getText().toString(),
                edtPrice.getText().toString(),
                edtOriginPrice.getText().toString());

        if (0 != err) {
            onError(Property.validation, err);
            return;
        }

        if (!viewModel.isAutoShare() || viewModel.editMode) {
            submitListing(false);
        } else if (SESSION.getInstance().isFbVerified()) {
            if (SESSION.getInstance().isFbLogin()) {
                askPermissionBeforeListing();
            } else {
                submitListing(true);
            }
        } else {
            // link facebook first
            linkFbBeforeListing();
        }
    }

    private void submitListing(boolean autoShare) {
        viewModel.listNewItem(
                edtTitle.getText().toString(),
                edtDesc.getText().toString(),
                edtPrice.getText().toString(),
                edtOriginPrice.getText().toString(),
                autoShare, geolocation);
    }

    // on item listed successfully
    private void onItemListed(final String newItemId) {
        trackTouch("publish");

        if (!viewModel.editMode) {
            // goto item view
            viewNewItem(newItemId);
        }

        finish();
    }

    private void viewNewItem(String newItemId) {
        // do navigation
        Intent intent = new Intent(ListItemActivity.this, ItemActivity.class);
        intent.putExtra(ACT_PARA_ITEM_ID, newItemId);
        startActivity(intent);
        L.d("launch item activity with item id after listing: " + newItemId);
    }

    private void askPermissionBeforeListing() {
        final Session session = Session.getActiveSession();
        if (session == null || !session.isOpened() || session.isPermissionGranted(FB_PERMISSION_PUBLISH)) {
            // if we already have publish permissions, then go ahead and publish
            submitListing(true);
            return;
        }

        new AlertDialog.Builder(this)
                .setTitle(R.string.share_with_friends_title)
                .setMessage(R.string.share_with_friends_message)
                .setPositiveButton(R.string.share_with_friends_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // if they choose to publish, then we request for publish permissions
                        Session.NewPermissionsRequest req =
                                new Session.NewPermissionsRequest(ListItemActivity.this, FB_PERMISSION_PUBLISH)
                                        .setDefaultAudience(SessionDefaultAudience.EVERYONE)
                                        .setCallback(new FbPermissionCallback());
                        session.requestNewPublishPermissions(req);
                    }
                })
                .setNegativeButton(R.string.share_with_friends_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cbxFbShare.setChecked(false);
                        submitListing(false);
                    }
                })
                .show();
    }

    private void linkFbBeforeListing() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.share_with_friends_title)
                .setMessage(R.string.share_with_friends_message)
                .setPositiveButton(R.string.share_with_friends_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        linkingFb = true;
                        fbLinker.link();
                    }
                })
                .setNegativeButton(R.string.share_with_friends_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cbxFbShare.setChecked(false);
                        submitListing(false);
                    }
                })
                .show();
    }

    private void naviBack() {
        finish();
    }

    private boolean isDirty() {
        return isNotEmpty(edtTitle.getText()) ||
                isNotEmpty(edtDesc.getText()) ||
                viewModel.isDirty();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onCancel();
            return true;
        }
        return false;
    }

    private void closeKeyboard() {
        InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = getWindow().getCurrentFocus();
        if (v != null) {
            im.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    // TODO extract dialog utils
    private void showConfirmDialog() {
        if (confirmDialog == null) {
            confirmDialog = new BottomConfirmDialog(this);
        }

        confirmDialog.setTopText(R.string.cancel_to_edit);
        confirmDialog.setBottomText(R.string.continue_to_edit);
        confirmDialog.setOnConfirmClickListner(new BottomConfirmDialog.OnConfirmClickListner() {
            @Override
            public void onConfirmClick() {
                confirmDialog.dismiss();
                TrackingUtils.trackTouch(VIEW_LIST_ITEM, "sell_cancel_tohome");
                naviBack();
                confirmDialog = null;
            }

            @Override
            public void onCancelClick() {
                TrackingUtils.trackTouch(VIEW_LIST_ITEM, "keep_editing");
                confirmDialog.dismiss();
            }
        });

        TrackingUtils.trackTouch(VIEW_LIST_ITEM, "sell_cancel");
        confirmDialog.show(viewRootCtnr, Gravity.CENTER | Gravity.BOTTOM);
    }

    // ----------------------------------------------
    //
    // TODO refactoring legacy code for audio recording
    //
    //
    private static final long MAX_AUDIO_LEN = 60000;  // max audion length

    private DefaultGestureListener mDefaultGestureListener;
    private GestureDetector mGestureDetector;
    @InjectView(R.id.voice_container) View mVoiceContainer;
    @InjectView(R.id.record) View recordBtn;
    @InjectView(R.id.record_btn) ImageView icRecorder;
    @InjectView(R.id.press_to_talk_tv) View pressToTalkTv;
    private Recorder mRecorder;
    boolean mSampleInterrupted = false;
    String mErrorUiMessage = null;

    // Some error messages are displayed in the UI, not a dialog. This happens when a recording
    // is interrupted for some reason.
    PowerManager.WakeLock mWakeLock;
    RemainingTimeCalculator mRemainingTimeCalculator;

    static final String AUDIO_3GPP = "audio/3gpp";
    static final String AUDIO_AMR = "audio/amr";
    static final String AUDIO_MPEG4 = "audio/m4a";
    String mRequestedType = AUDIO_MPEG4;

    static final String RECORDER_STATE_KEY = "recorder_state";
    static final String SAMPLE_INTERRUPTED_KEY = "sample_interrupted";
    static final String MAX_FILE_SIZE_KEY = "max_file_size";

    static final int BITRATE_AMR = 5900; // bits/sec
    static final int BITRATE_3GPP = 5900;
    static final int BITRATE_MPEG4 = 5900;
    long mMaxFileSize = -1;        // can be specified in the intent

    private BroadcastReceiver mSDCardMountEventReceiver = null;

    private RecordingDecibelPopupWindow mRecordingDecibelPopupWindow;

    static class DefaultGestureHandler extends Handler {
        private WeakReference<ListItemActivity> mActivity;

        private DefaultGestureHandler(ListItemActivity pActivity) {
            mActivity = new WeakReference<>(pActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            ListItemActivity listItemActivity = mActivity.get();
            if (listItemActivity != null) {
                switch (msg.what) {
                    case 1:
                        listItemActivity.mRecorder.stopRecording();
                        if (listItemActivity.mRecorder.mSampleFile != null) {
                            listItemActivity.viewModel.uploadAudio(listItemActivity.mRecorder.mSampleFile);
                        }
                        if (listItemActivity.mRecordingDecibelPopupWindow != null
                                && listItemActivity.mRecordingDecibelPopupWindow.isShowing()) {
                            listItemActivity.mRecordingDecibelPopupWindow.dismiss();
                        }
                        listItemActivity.updateAudioUi();
                        break;
                    case 2:
                        if (listItemActivity.mRecordingDecibelPopupWindow != null) {
                            TextView tvTimeRemaining = listItemActivity.mRecordingDecibelPopupWindow.timeRemainingTextView;
                            if (tvTimeRemaining != null) {
                                tvTimeRemaining.setText(String.valueOf(msg.arg1) + "s");
                            }
                        }
                        break;
                    case 3:
                        listItemActivity.mRecorder.stopRecording();
                        if (listItemActivity.mRecorder.mSampleFile != null) {
                            listItemActivity.viewModel.uploadAudio(listItemActivity.mRecorder.mSampleFile);
                            DisplayUtils.toast(R.string.voice_time_limit_description);
                        }
                        if (listItemActivity.mRecordingDecibelPopupWindow != null
                                && listItemActivity.mRecordingDecibelPopupWindow.isShowing()) {
                            listItemActivity.mRecordingDecibelPopupWindow.dismiss();
                        }
                        listItemActivity.updateAudioUi();
                        break;
                }
            }
        }
    }

    class DefaultGestureListener extends GestureDetector.SimpleOnGestureListener {

        private Handler handler = new DefaultGestureHandler(ListItemActivity.this);
        // FIXME handler leak
//        private Handler handler = new Handler() {
//            @Override
//            public void handleMessage(Message msg) {
//                switch (msg.what) {
//                    case 1:
//                        mRecorder.stopRecording();
//                        if (mRecorder.mSampleFile != null) {
//                            viewModel.uploadAudio(mRecorder.mSampleFile);
//                        }
//                        if (mRecordingDecibelPopupWindow != null && mRecordingDecibelPopupWindow.isShowing()) {
//                            mRecordingDecibelPopupWindow.dismiss();
//                        }
//                        updateAudioUi();
//                        break;
//                    case 2:
//                        if (mRecordingDecibelPopupWindow != null)
//                            mRecordingDecibelPopupWindow.timeRemainingTextView.setText(String.valueOf(msg.arg1) + "s");
//                        break;
//                    case 3:
//                        mRecorder.stopRecording();
//                        if (mRecorder.mSampleFile != null) {
//                            viewModel.uploadAudio(mRecorder.mSampleFile);
//                            DisplayUtils.toast(R.string.voice_time_limit_description);
//                        }
//                        if (mRecordingDecibelPopupWindow != null && mRecordingDecibelPopupWindow.isShowing()) {
//                            mRecordingDecibelPopupWindow.dismiss();
//                        }
//                        updateAudioUi();
//                        break;
//                }
//                super.handleMessage(msg);
//            }
//        };

        private Runnable runnable = new Runnable() {
            public void run() {
                if (mRecorder.progress() * 1000 >= MAX_AUDIO_LEN) {
                    Message message = new Message();
                    message.what = 3;
                    handler.sendMessage(message);
                    handler.removeCallbacks(runnable);
                }
                if (mRecorder.progress() * 1000 >= MAX_AUDIO_LEN - 10000) {
                    Message message = new Message();
                    message.what = 2;
                    message.arg1 = (int) ((MAX_AUDIO_LEN - mRecorder.progress() * 1000) / 1000);
                    handler.sendMessage(message);
                    handler.postDelayed(this, 1000);
                } else {
                    handler.postDelayed(this, 1000);
                }
            }
        };

        @Override
        public void onShowPress(MotionEvent e) {
            handler.postDelayed(runnable, 1000);
            mRemainingTimeCalculator.reset();
            if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                mSampleInterrupted = true;
                mErrorUiMessage = getResources().getString(R.string.insert_sd_card);
                updateAudioUi();
            } else if (!mRemainingTimeCalculator.diskSpaceAvailable()) {
                mSampleInterrupted = true;
                mErrorUiMessage = getResources().getString(R.string.storage_is_full);
                updateAudioUi();
            } else {
                stopAudioPlayback();
                if (AUDIO_AMR.equals(mRequestedType)) {
                    mRemainingTimeCalculator.setBitRate(BITRATE_AMR);
                    mRecorder.startRecording(MediaRecorder.OutputFormat.AMR_NB, ".amr",
                            ListItemActivity.this);
                    Log.d("TabAddActivity.class", "startRecording amr");
                } else if (AUDIO_3GPP.equals(mRequestedType)) {
                    mRemainingTimeCalculator.setBitRate(BITRATE_3GPP);
                    mRecorder.startRecording(MediaRecorder.OutputFormat.THREE_GPP, ".3gpp",
                            ListItemActivity.this);
                    Log.d("TabAddActivity.class", "startRecording 3gpp");
                } else if (AUDIO_MPEG4.equals(mRequestedType)) {
                    mRemainingTimeCalculator.setBitRate(BITRATE_MPEG4);
                    mRecorder.startRecording(MediaRecorder.OutputFormat.MPEG_4, ".m4a",
                            ListItemActivity.this);
                    Log.d("TabAddActivity.class", "startRecording m4a");
                } else {
                    throw new IllegalArgumentException("Invalid output file type requested");
                }
                if (mMaxFileSize != -1) {
                    mRemainingTimeCalculator.setFileSizeLimit(
                            mRecorder.mSampleFile, mMaxFileSize);
                }
                mRecordingDecibelPopupWindow.timeRemainingTextView.setText(R.string.recording);
                mRecordingDecibelPopupWindow.showAtLocation(viewRootCtnr, Gravity.CENTER, 0, 0);
                updateAudioUi();
            }
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            mRecorder.clear();
            showToast();
            return false;
        }
    }

    private void showToast() {
        DisplayUtils.toast(R.string.hold_to_record);
    }

    public void updateAudioUi() {
        switch (mRecorder.state()) {
            case Recorder.IDLE_STATE:
                if (mRecorder.sampleLength() == 0) {
                    recordBtn.setVisibility(View.VISIBLE);
                    icRecorder.setImageResource(R.drawable.ic_mic);
                    pressToTalkTv.setVisibility(View.VISIBLE);
                    audioWrapper.setVisibility(View.GONE);
                } else {
                    recordBtn.setVisibility(View.GONE);
                    pressToTalkTv.setVisibility(View.GONE);
                    audioWrapper.setVisibility(View.VISIBLE);
                    btnPlayAudio.setBackgroundResource(R.drawable.square_button_play);
                    txtAudioDuration.setText((mRecorder.mSampleLength > (MAX_AUDIO_LEN / 1000) ?
                            ((MAX_AUDIO_LEN / 1000) + "''") : ((mRecorder.mSampleLength + "''"))));
                }
                break;
            case Recorder.RECORDING_STATE:
                recordBtn.setVisibility(View.VISIBLE);
                icRecorder.setImageResource(R.drawable.ic_mic_pressed);
                pressToTalkTv.setVisibility(View.VISIBLE);
                audioWrapper.setVisibility(View.GONE);
                break;
            case Recorder.PLAYING_STATE:
                recordBtn.setVisibility(View.GONE);
                pressToTalkTv.setVisibility(View.GONE);
                audioWrapper.setVisibility(View.VISIBLE);
                btnPlayAudio.setBackgroundResource(R.drawable.square_button_playing);
                break;
        }
    }

    /*
    * Called when Recorder changed it's state.
    */
    public void onStateChanged(int state) {
        if (state == Recorder.PLAYING_STATE || state == Recorder.RECORDING_STATE) {
            mSampleInterrupted = false;
            mErrorUiMessage = null;
            mWakeLock.acquire(); // we don't want to go to sleep while recording or playing
        } else {
            if (mWakeLock.isHeld())
                mWakeLock.release();
        }
        updateAudioUi();
    }

    /*
   * Called when MediaPlayer encounters an error.
   */
    public void onError(int error) {
        Resources res = getResources();

        String message = null;
        switch (error) {
            case Recorder.SDCARD_ACCESS_ERROR:
                message = res.getString(R.string.error_sdcard_access);
                break;
            case Recorder.IN_CALL_RECORD_ERROR:
                // TODO: update error message to reflect that the recording could not be performed during a call.
            case Recorder.INTERNAL_ERROR:
                message = res.getString(R.string.error_app_internal);
                break;
        }

        L.d("audioPlayback error");
        if (mRecorder != null) {
            mRecorder.pausePlayback();
        }

        if (message != null) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.app_name)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, null)
                    .setCancelable(false)
                    .show();
        }
    }

    class RemainingTimeCalculator {
        public static final int UNKNOWN_LIMIT = 0;

        // which of the two limits we will hit (or have fit) first
        private int mCurrentLowerLimit = UNKNOWN_LIMIT;

        private File mSDCardDirectory;

        // State for tracking file size of recording.
        private File mRecordingFile;
        private long mMaxBytes;

        // Rate at which the file grows
        private int mBytesPerSecond;

        // time at which number of free blocks last changed
        private long mBlocksChangedTime;
        // number of available blocks at that time
        private long mLastBlocks;

        // time at which the size of the file has last changed
        private long mFileSizeChangedTime;
        // size of the file at that time
        private long mLastFileSize;

        public RemainingTimeCalculator() {
            mSDCardDirectory = Environment.getExternalStorageDirectory();
        }

        /**
         * If called, the calculator will return the minimum of two estimates:
         * how long until we run out of disk space and how long until the file
         * reaches the specified size.
         *
         * @param file     the file to watch
         * @param maxBytes the limit
         */
        public void setFileSizeLimit(File file, long maxBytes) {
            mRecordingFile = file;
            mMaxBytes = maxBytes;
        }

        /**
         * Resets the interpolation.
         */
        public void reset() {
            mCurrentLowerLimit = UNKNOWN_LIMIT;
            mBlocksChangedTime = -1;
            mFileSizeChangedTime = -1;
        }

        /**
         * Is there any point of trying to start recording?
         */
        public boolean diskSpaceAvailable() {
            StatFs fs = new StatFs(mSDCardDirectory.getAbsolutePath());
            // keep one free block
            return fs.getAvailableBlocks() > 1;
        }

        /**
         * Sets the bit rate used in the interpolation.
         *
         * @param bitRate the bit rate to set in bits/sec.
         */
        public void setBitRate(int bitRate) {
            mBytesPerSecond = bitRate / 8;
        }
    }

    private void stopAudioPlayback() {
        // Shamelessly copied from MediaPlaybackService.java, which
        // should be public, but isn't.
        Intent i = new Intent("com.android.music.musicservicecommand");
        i.putExtra("command", "pause");

        sendBroadcast(i);
    }

    @OnTouch(R.id.record)
    @SuppressWarnings("unused")
    boolean onRecordTouched(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                finishRecording();
                break;
            case MotionEvent.ACTION_MOVE:
                if (Math.abs(event.getY()) > mVoiceContainer.getHeight()) {
                    finishRecording();
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                if (Math.abs(event.getY()) > mVoiceContainer.getHeight()) {
                   finishRecording();
                }
                break;
        }
        mGestureDetector.onTouchEvent(event);
        return false;
    }

    private void finishRecording() {
        if (mDefaultGestureListener.handler != null && mDefaultGestureListener.runnable != null) {
            mDefaultGestureListener.handler.removeCallbacks(mDefaultGestureListener.runnable);
        }
        if (mRecordingDecibelPopupWindow != null && mRecordingDecibelPopupWindow.isShowing()) {
            mRecordingDecibelPopupWindow.dismiss();
        }

        long len = mRecorder.getRecordedTime();
        if (len < MIN_AUDIO_LEN) {
            L.d("recorded %d", len);
            mRecorder.clear();

            if (len > 300) {
                // 避免与single tab重复提示
                DisplayUtils.toast(R.string.voice_too_short);
            }
            return;
        }

        // 多录几秒钟，避免截断用户语音
        showMainProgress(R.string.info_saving_audio);
        btnPlayAudio.setEnabled(false);

        Observable.timer(RECORD_EXTRA_TIME, TimeUnit.MILLISECONDS, Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(new Action0() {
                    @Override
                    public void call() {
                        stopRecording();
                        stopMainProgress();
                        btnPlayAudio.setEnabled(true);
                        updateAudioUi();
                    }
                })
                .subscribe();
    }

    private void stopRecording() {
        if (mRecorder.mState == Recorder.RECORDING_STATE) {
            Message message = new Message();
            message.what = 1;
            mDefaultGestureListener.handler.sendMessage(message);
            mDefaultGestureListener.handler.removeCallbacks(mDefaultGestureListener.runnable);
            trackTouch("record");
        }
    }

    @OnClick(R.id.delete)
    void onDeleteAudio() {
        mRecorder.delete();
        viewModel.removeAudio();
        TrackingUtils.trackTouch(VIEW_LIST_ITEM, "record_delete");
    }

    @OnClick(R.id.btn_play_audio)
    void onPlayAudio() {
        if (mRecorder.mState == Recorder.PLAYING_STATE) {
            mRecorder.pausePlayback();
            btnPlayAudio.setBackgroundResource(R.drawable.square_button_play);
        } else {
            mRecorder.startPlayback();
            btnPlayAudio.setBackgroundResource(R.drawable.square_button_playing);
            trackTouch("record_listen");
        }
    }

    /*
     * Registers an intent to listen for ACTION_MEDIA_EJECT/ACTION_MEDIA_MOUNTED notifications.
     */
    private void registerExternalStorageListener() {
        if (mSDCardMountEventReceiver == null) {
            mSDCardMountEventReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals(Intent.ACTION_MEDIA_EJECT)) {
                        mRecorder.delete();
                    } else if (action.equals(Intent.ACTION_MEDIA_MOUNTED)) {
                        mSampleInterrupted = false;
                        updateAudioUi();
                    }
                }
            };
            IntentFilter iFilter = new IntentFilter();
            iFilter.addAction(Intent.ACTION_MEDIA_EJECT);
            iFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
            iFilter.addDataScheme("file");
            registerReceiver(mSDCardMountEventReceiver, iFilter);
        }
    }

    private void initAudio(Bundle savedInstanceState) {
        mRecorder = new Recorder(progAudioPlayback, txtAudioElapse, btnPlayAudio);
        mRemainingTimeCalculator = new RemainingTimeCalculator();
        mRecorder.setOnStateChangedListener(this);

        registerExternalStorageListener();
        if (savedInstanceState != null) {
            Bundle recorderState = savedInstanceState.getBundle(RECORDER_STATE_KEY);
            if (recorderState != null) {
                mRecorder.restoreState(recorderState);
                mSampleInterrupted = recorderState.getBoolean(SAMPLE_INTERRUPTED_KEY, false);
                mMaxFileSize = recorderState.getLong(MAX_FILE_SIZE_KEY, -1);
            }
        }

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "5miles");

        mDefaultGestureListener = new DefaultGestureListener();
        mGestureDetector = new GestureDetector(this, mDefaultGestureListener);
        mGestureDetector.setIsLongpressEnabled(true);

        mRecordingDecibelPopupWindow = new RecordingDecibelPopupWindow(this);
        mRecordingDecibelPopupWindow.setBackgroundDrawable(getResources().getDrawable(android.R.color.transparent));
        Recorder.OnRecordDecibelListener mOnRecordDecibelListener = new Recorder.OnRecordDecibelListener() {
            @Override
            public void onDecibelChanged(double decibel) {
                mRecordingDecibelPopupWindow.updateDecibelView(decibel);
            }
        };
        mRecorder.setOnRecordDecibelListener(mOnRecordDecibelListener);
    }

    private void initAudioLoadingAnimation() {
        audioLoadingAnimation = ValueAnimator.ofInt(0x80, 0xe6);  // alpha of playback button, 50%~90%
        audioLoadingAnimation.setDuration(750);
        audioLoadingAnimation.setRepeatCount(ValueAnimator.INFINITE);
        audioLoadingAnimation.setRepeatMode(ValueAnimator.REVERSE);
        audioLoadingAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                int alpha = (Integer) animator.getAnimatedValue();
                btnPlayAudio.getBackground().setAlpha(alpha);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {

        try {
            Item item = viewModel.parseToItem();
            outState.putString("jsonItem", Item__JsonHelper.serializeToJson(item));
            if(tmpCameraImageFile != null && tmpCameraImageFile.exists()) {
                outState.putString("cameraImageFilePath", tmpCameraImageFile.getAbsolutePath());
            }
            L.d("ListItemActivity onSaveState" + Item__JsonHelper.serializeToJson(item));

        } catch (Exception ee) {
            ee.printStackTrace();
        }
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedState) {

        try {
            String cameraImageFilePath = savedState.getString("cameraImageFilePath",null);
            if(cameraImageFilePath != null) {
                tmpCameraImageFile = new File(cameraImageFilePath);
            }
            String jsonItem = savedState.getString("jsonItem");
            if(jsonItem != null) {
                Item parsedItem = Item__JsonHelper.parseFromJson(jsonItem);
                viewModel.parseFromItem(parsedItem);
                populate(parsedItem);
            }

            L.d("ListItemActivity onRestoreState" + jsonItem);

        } catch (Exception ee) {
            ee.printStackTrace();
        }

        super.onRestoreInstanceState(savedState);
    }
}
