package com.thirdrock.fivemiles.main.home;

import android.view.View;

import com.thirdrock.domain.ItemThumb;
import com.thirdrock.fivemiles.common.item.ItemThumbRenderer;
import com.thirdrock.framework.util.L;

import rx.Observable;
import rx.Observer;

/**
 * Renderer from Effective UI
 * Created by ywu on 15/2/17.
 */
public class AsyncItemThumbRenderer extends ItemThumbRenderer implements Observer<ItemThumb> {

    @Override
    public void render() {
        ItemThumb item = getContent();

        if (item instanceof ObservableItemThumb) {
            renderAsync(((ObservableItemThumb) item).getObservable());
        } else {
            render(item);
        }
    }

    private void renderAsync(Observable<ItemThumb> observable) {
        L.v("async rendering");
        cleanViews();
        observable.subscribe(this);
    }

    private void cleanViews() {
        drawRandomColorBg();  // 在数据加载完之前，先替换原有图片为纯色背景
        txtTitle.setText("");
        txtDistance.setText("");
        txtPrice.setText("");
        txtOriginPrice.setText("");
        icSold.setVisibility(View.GONE);
        if (imgNewTag != null) {
            imgNewTag.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNext(ItemThumb item) {
        onRecycle(item);
        render(item);
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }
}
