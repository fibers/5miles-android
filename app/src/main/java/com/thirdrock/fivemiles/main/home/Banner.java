package com.thirdrock.fivemiles.main.home;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.thirdrock.domain.Campaign;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.SysMsgActionUtils;
import com.thirdrock.protocol.SystemAction;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;
import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_APP_STATE;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_CLOSED_AD;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_HOME;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;
import static com.thirdrock.fivemiles.util.TrackingUtils.trackTouch;

/**
 * Banner utils.
 * Created by ywu on 14/12/9.
 */
public class Banner extends SimpleImageLoadingListener {

    public interface OnClosedListener {
        void onClosed();
    }

    private final Context context;
    private final DisplayImageOptions adImgOpts;
    private final SysMsgActionUtils actionUtils;

    private final View banner;

    @InjectView(R.id.banner_wrapper)
    View container;

    @InjectView(R.id.img_banner_ad)
    ImageView imgBannerAd;

    private Campaign campaign;
    private long prevClosedCampaign;
    private OnClosedListener onClosedListener;
    private String savedCampaignKey;  // key for save/load previous campaign

    public Banner(Context context) {
        this(context, PREF_KEY_CLOSED_AD);
    }

    /**
     * 传入的Context必须是Activity或Fragment，否则startActivity()方法要加FLAG_ACTIVITY_NEW_TASK
     */
    public Banner(Context context, String savedCampaignKey) {
        this.context = context;
        this.savedCampaignKey = savedCampaignKey;

        actionUtils = new SysMsgActionUtils(context);

        adImgOpts = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading)
                .showImageForEmptyUri(R.drawable.loading)
                .showImageOnFail(R.drawable.loading)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        banner = LayoutInflater.from(context).inflate(R.layout.ad_banner, null);
        ButterKnife.inject(this, banner);
        container.setVisibility(View.GONE);

        loadPrevClosedCampaignId();
    }

    public View getBanner() {
        return banner;
    }

    public void setOnClosedListener(OnClosedListener onClosedListener) {
        this.onClosedListener = onClosedListener;
    }

    public void showCampaign(Campaign campaign) {
        this.campaign = campaign;
        container.setVisibility(View.GONE);
        loadAdImage();
    }

    @OnClick(R.id.btn_close)
    void onClose() {
        container.setVisibility(View.GONE);
        saveClosedCampaignId();

        if (onClosedListener != null) {
            onClosedListener.onClosed();
        }
        trackTouch(VIEW_HOME, "banneroff");
    }

    @OnClick(R.id.banner_wrapper)
    void onClickAd() {
        if (campaign == null || isEmpty(campaign.getClickAction())) {
            return;
        }

        SystemAction action = campaign.getActionType();
        String actionData = campaign.getActionData();
        actionUtils.handleSystemAction(action, actionData, campaign.getExtraActionData());
        trackTouch(VIEW_HOME, "banner");
    }

    @Override
    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
        container.setVisibility(View.GONE);
    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        container.setVisibility(loadedImage == null ? View.GONE : View.VISIBLE);
    }

    private void loadAdImage() {
        if (campaign == null || !shouldShowBanner()) {
            return;
        }

        float ratio = ((float) campaign.getImageWidth()) / campaign.getImageHeight();
        int w = banner.getMeasuredWidth();
        int h = (int) Math.ceil(w / ratio);

        String url = CloudHelper.toCropUrl(campaign.getImageUrl(), w, h, "fill", true);
        ImageLoader.getInstance().displayImage(url, imgBannerAd, adImgOpts, this);
    }

    private boolean shouldShowBanner() {
        return campaign != null && campaign.isActive() && prevClosedCampaign != campaign.getVersion();
    }

    private void loadPrevClosedCampaignId() {
        // FIXME 兼容旧版本按int存储的情况 #187
        Object v = context.getSharedPreferences(PREFS_APP_STATE, MODE_PRIVATE)
                .getAll().get(savedCampaignKey);

        if (v instanceof Integer) {
            prevClosedCampaign = (Integer) v;
        } else if (v instanceof Long) {
            prevClosedCampaign = (Long) v;
        } else {
            prevClosedCampaign = 0;
        }
    }

    private void saveClosedCampaignId() {
        if (campaign == null) {
            return;
        }

        prevClosedCampaign = campaign.getVersion();
        context.getSharedPreferences(PREFS_APP_STATE, MODE_PRIVATE)
                .edit().putLong(savedCampaignKey, prevClosedCampaign).apply();
    }
}
