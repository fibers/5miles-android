package com.thirdrock.fivemiles.main.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.insthub.fivemiles.FiveMilesApp;
import com.thirdrock.fivemiles.R;

import static com.insthub.fivemiles.FiveMilesAppConst.EMPTY_STRING;
import static com.insthub.fivemiles.FiveMilesAppConst.HOME_HOUSING_URL;
import static com.insthub.fivemiles.FiveMilesAppConst.HOME_JOB_URL;
import static com.insthub.fivemiles.FiveMilesAppConst.HOME_SERVICE_URL;

/**
 * View pager for home nearby and following fragment
 * Created by jubin on 7/4/15.
 */
public class HomeFragmentPagerAdapter extends FragmentPagerAdapter {
    public static final int FRAG_COUNT = 6;

    public HomeFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new HomeNearbyFragment();
            case 1:
                return HomeWebFragment.newInstance(HOME_SERVICE_URL);
            case 2:
                return HomeWebFragment.newInstance(HOME_HOUSING_URL);
            case 3:
                return HomeWebFragment.newInstance(HOME_JOB_URL);
            case 4:
                return new HomeFollowingFragment();
            case 5:
                return HomeWebFragment.newInstance("http://m.5milesapp.com/app/api");
            default:
                return new HomeNearbyFragment();
        }
    }

    @Override
    public int getCount() {
        return FRAG_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int positon) {
        String title = EMPTY_STRING;
        switch (positon) {
            case 0:
                title = FiveMilesApp.appCtx.getString(R.string.home_tab_tiltle_for_sale);
                break;
            case 1:
                title = FiveMilesApp.appCtx.getString(R.string.home_tab_title_service);
                break;
            case 2:
                title = FiveMilesApp.appCtx.getString(R.string.home_tab_title_housing);
                break;
            case 3:
                title = FiveMilesApp.appCtx.getString(R.string.home_tab_title_jobs);
                break;
            case 4:
                title = FiveMilesApp.appCtx.getString(R.string.home_tab_title_following);
                break;
            case 5:
                title = "Test";
                break;
        }
        return title;
    }
}
