package com.thirdrock.fivemiles.main.home;

import android.database.Cursor;

import com.pedrogomez.renderers.AdapteeCollection;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.domain.ItemThumb__JsonHelper;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.Emit1;
import com.thirdrock.framework.util.rx.SimpleObserver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * 固定size(内存中保持的item数量)的collection，支持overflow至本地sqlite
 * TODO generalize this collection
 * Created by ywu on 15/2/17.
 */
public class ItemThumbCollection implements AdapteeCollection<ItemThumb> {
    public static final int DEFAULT_MAX_CACHE_SIZE = 2000;
    private static final int PRE_LOAD_DISTANCE = 100;
    private static final int PRE_LOAD_SIZE = 200;

    private final ItemThumbDao itemDao;
    private Cursor cursor;

    private final ExecutorService cachingExecutor = Executors.newSingleThreadExecutor();
    private volatile List<ItemThumb> cache;
    private volatile int totalSize, maxCacheSize, cacheOffset;
    private volatile boolean populatingCache;

//    public ItemThumbCollection() {
//        this(ItemThumbDao.TBL_HOME_NEARBY_ITEMS, DEFAULT_MAX_CACHE_SIZE);
//    }

    public ItemThumbCollection(String tableName) {
        this(tableName, DEFAULT_MAX_CACHE_SIZE);
    }

    private ItemThumbCollection(String tableName, int maxCacheSize) {
        this.itemDao = new ItemThumbDao(tableName);
        this.maxCacheSize = maxCacheSize;
        this.cache = new ArrayList<>(maxCacheSize);
    }

    @Override
    public int size() {
        if (totalSize > 0) {
            return totalSize;
        }

        Cursor c = ensureCursor();
        totalSize = c.getCount();
        L.v("size got from db %d", totalSize);
        return totalSize;
    }

    @Override
    public ItemThumb get(int index) {
        ensureCache(index);

        int i = index - cacheOffset;
        if (i >= 0 && i < cache.size()) {
            return cache.get(i);
        }

        return new ObservableItemThumb(schedule(
                Observable.create(new OnSubscribeItemQuery(ensureCursor(), index))));
    }

    @Override
    public void add(ItemThumb item) {
        throw new IllegalAccessError("should not invoke this method");
    }

    @Override
    public void remove(ItemThumb item) {
        throw new IllegalAccessError("should not invoke this method");
    }

    @Override
    public void addAll(Collection<ItemThumb> items) {
        throw new IllegalAccessError("should not invoke this method");
    }

    @Override
    public void removeAll(Collection<ItemThumb> items) {
        throw new IllegalAccessError("should not invoke this method");
    }

    @Override
    public void clear() {
        throw new IllegalAccessError("should not invoke this method");
    }

    /**
     * Remove all items, before applying new data set
     */
    public Observable<Void> reset(final Collection<ItemThumb> items) {
        L.d("reset with %d new items", items.size());
        return schedule(Observable.create(new Emit1<Void>() {
            @Override
            protected Void call() throws Exception {
                itemDao.truncate();
                doInsert(0, items);
                return null;
            }
        })).doOnCompleted(new Action0() {
            // on main thread
            @Override
            public void call() {
                totalSize = items.size();
                clearCache();
                cache.addAll(items);
            }
        }).doOnError(new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                L.e("reset() throw an exception " + throwable);
                TrackingUtils.trackException(throwable);
            }
        });
    }

    /**
     * Append items to the list
     */
    public Observable<Void> append(final Collection<ItemThumb> items) {
        L.d("append %d items", items.size());
        return schedule(Observable.create(new Emit1<Void>() {
            @Override
            protected Void call() throws Exception {
                doInsert(totalSize, items);
                return null;
            }
        })).doOnCompleted(new Action0() {
            // on main thread
            @Override
            public void call() {
                totalSize += items.size();
                cache.addAll(items);
            }
        });
    }

    private void doInsert(int offset, Collection<ItemThumb> items) {
        L.d("insert items at %d", offset);
        itemDao.insert(offset, items);
        closeCursor();
    }

    private void ensureCache(int index) {
        int from = 0, to = 0, diff;
        boolean append = false;  // append to end of cache, or prepend to the cache
        int endOfCache = cacheOffset + cache.size() - 1;  // -1 when cache is empty

        if (endOfCache - index <= PRE_LOAD_DISTANCE) {
            // near end of the cache
            from = Math.min(endOfCache + 1, totalSize - 1);
            to = Math.min(from + PRE_LOAD_SIZE, totalSize - 1);
            append = true;
        } else if (index - cacheOffset <= PRE_LOAD_DISTANCE) {
            // near head of the cache
            to = Math.max(cacheOffset - 1, 0);
            from = Math.max(to - PRE_LOAD_SIZE, 0);
        }

        diff = to - from;
        if (diff > 0 && !populatingCache) {
            L.v("ensure cache %d %s", diff, populatingCache);
            final int fromIndex = from;
            final int toIndex = to;
            final boolean appendCache = append;

            // populates the memory cache
            populatingCache = true;
            schedule(Observable.create(new Emit1<List<ItemThumb>>() {
                @Override
                protected List<ItemThumb> call() throws Exception {
                    L.v("loading items from db %d:%d append? %s", fromIndex, toIndex, appendCache);
                    return loadItemsFromDb(fromIndex, toIndex);
                }
            })).subscribe(new SimpleObserver<List<ItemThumb>>() {
                @Override
                public void onNext(List<ItemThumb> newItems) {
                    if (appendCache) {
                        cache.addAll(newItems);
                    } else {
                        cache.addAll(0, newItems);
                    }

                    int free = maxCacheSize - cache.size();
                    if (free < 0) {
                        cache = cache.subList(appendCache ? 0 : maxCacheSize,
                                appendCache ? -free - 1 : cache.size());
                        cacheOffset -= appendCache ? free : 0;
                    }

                    populatingCache = false;
                }
            });
        }
    }

    private List<ItemThumb> loadItemsFromDb(int from, int to) {
        ArrayList<ItemThumb> newItems = new ArrayList<>(to - from);

        Cursor c = null;
        try {
            c = itemDao.getItemThumbs(from, to);
            while (c.moveToNext()) {
                ItemThumb item = parseItemThumb(c);
                newItems.add(item);
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return newItems;
    }

    private ItemThumb parseItemThumb(Cursor c) {
        try {
            String json = c.getString(0);
            return ItemThumb__JsonHelper.parseFromJson(json);
        } catch (IOException e) {
            throw new RuntimeException("parse item failed", e);
        }
    }

    // release memory when the view is invisble
    public void pause() {
        clearCache();
        closeDb();
    }

    public void destroy() {
        totalSize = 0;
        closeCursor();
        closeDb();
        clearCache();

        cachingExecutor.shutdownNow();
    }

    private Cursor ensureCursor() {
        if (cursor == null) {
            cursor = itemDao.getItemThumbs();
        }

        return cursor;
    }

    private void clearCache() {
        cache.clear();
        cacheOffset = 0;
    }

    private void closeCursor() {
        if (cursor == null) {
            return;
        }

        cursor.close();
        cursor = null;
    }

    private void closeDb() {
        cursor = null;
//        itemDao.close();
    }

    private <T> Observable<T> schedule(Observable<T> job) {
        // db操作在分离的线程执行，更新内存缓存的操作都在main线程执行，避免线程同步
        return job.subscribeOn(Schedulers.from(cachingExecutor))
                .observeOn(AndroidSchedulers.mainThread());
    }

    static class OnSubscribeItemQuery extends Emit1<ItemThumb> {
        final Cursor cursor;
        final int index;

        OnSubscribeItemQuery(Cursor cursor, int index) {
            this.cursor = cursor;
            this.index = index;
        }

        @Override
        protected ItemThumb call() throws Exception {
            cursor.moveToPosition(index);
            String json = cursor.getString(0);
            return ItemThumb__JsonHelper.parseFromJson(json);
        }
    }
}
