package com.thirdrock.fivemiles.main.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.insthub.fivemiles.FiveMilesApp;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.di.RepositoryModule;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.SimpleObserver;
import com.thirdrock.repository.GcmRepository;
import com.thirdrock.repository.impl.BodyParserFactoryImpl;
import com.thirdrock.repository.impl.GcmRepositoryImpl;

import java.io.IOException;

import rx.Observer;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static android.content.pm.PackageManager.NameNotFoundException;
import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_GCM_INFO;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_APP_VERSION;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_GCM_SENDER_ID;
import static com.thirdrock.repository.GcmRepository.GCM_BASE_URL;

/**
 * 含Gcm相关操作，与UI无关的放在后台线程中
 * Created by jubin on 23/4/15.
 */
public class GcmInfoHelper {
//    private static final String[] TOPICS = {"global"};

    private static GcmInfoHelper instance;
    private GcmRepository repository;

    private Observer<String> gcmRegIdObserver = new SimpleObserver<String>(){

        @Override
        public void onNext(String pGcmSenderId) {
            storeLastUpdate(pGcmSenderId);
            L.w("register gcm succeeded!");
        }

        private void storeLastUpdate(String gcmSenderId) {
            SharedPreferences prefs = getGcmPreferences();
            if (prefs == null) {
                return;
            }

            int currAppVersion = getAppVersion(FiveMilesApp.appCtx);
            prefs.edit().putString(PREF_KEY_GCM_SENDER_ID, gcmSenderId)
                        .putInt(PREF_KEY_APP_VERSION, currAppVersion)
                        .apply();
        }
    };

    private GcmInfoHelper() {
        repository = new GcmRepositoryImpl(GCM_BASE_URL,
                RepositoryModule.getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }

    public static GcmInfoHelper getInstance() {
        if (instance == null) {
            synchronized (GcmInfoHelper.class) {
                if (instance == null) {
                    instance = new GcmInfoHelper();
                }
            }
        }

        return instance;
    }

    /**
     * 如果gcm registration id过期或者没有上传成功，则上传gcm registration id
     * @see <a href="https://developers.google.com/cloud-messaging/android/client">GCM Client Doc</a>
     */
    public void registerGcmInBackgroundIfNecessary() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (shouldSendGcmRegId()) {
//                if (true) {     // true
                    final String gcmSenderId = FiveMilesApp.appCtx.getString(R.string.gcm_project_num);

                    try {
                        String scope = GoogleCloudMessaging.INSTANCE_ID_SCOPE;
                        InstanceID instanceID = InstanceID.getInstance(FiveMilesApp.appCtx);
                        String gcmToken = instanceID.getToken(gcmSenderId, scope, null);

                        sendGcmTokenToServer(gcmSenderId, gcmToken);

//                        subscribeTopics(gcmToken);
                    } catch (IOException e) {
                        L.d("io error with project id " + gcmSenderId);
                        L.d(e);
                    }
                }
            }
        }).start();
    }

//    private void subscribeTopics(String gcmToken) {
//        for (String topic : TOPICS) {
//            GcmPubSub pubSub = GcmPubSub.getInstance(FiveMilesApp.appCtx);
//            try {
//                pubSub.subscribe(gcmToken, "/topics/" + topic, null);
//            } catch (IOException e) {
//                L.e("Failed to complete token refresh");
//                e.printStackTrace();
//            }
//        }
//    }

    private void sendGcmTokenToServer(final String gcmSenderId, String gcmToken) {
        repository.registerGcm(gcmToken)
                .map(new Func1<Void, String>() {
                    @Override
                    public String call(Void aVoid) {
                        return gcmSenderId;
                    }
                })
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(gcmRegIdObserver);
    }

    private boolean shouldSendGcmRegId() {
        boolean playServiceNotAvailable = !isPlayServicesAvailable();
        if (playServiceNotAvailable) {
            return false;
        }

        SharedPreferences prefs = getGcmPreferences();
        if (prefs == null) {
            return true;
        }

        String registrationId = prefs.getString(PREF_KEY_GCM_SENDER_ID, "");
        if (ModelUtils.isEmpty(registrationId)) {
            L.i("Registration not found.");
            return true;
        }

        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = prefs.getInt(PREF_KEY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(FiveMilesApp.appCtx);
        if (registeredVersion != currentVersion) {
            L.i("App version changed.");
            return true;
        }

        return false;
    }

    private int getAppVersion(Context pContext) {
        if (pContext == null) {
            L.e("can't get app version code without context");
            return Integer.MAX_VALUE;
        }

        try {
            PackageInfo packageInfo = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private SharedPreferences getGcmPreferences() {
        return FiveMilesApp.appCtx.getSharedPreferences(PREFS_GCM_INFO, Context.MODE_PRIVATE);
    }

    private boolean isPlayServicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(FiveMilesApp.appCtx);
        return ConnectionResult.SUCCESS == resultCode;
    }

    public void unRegisterGcmInBackground(final String pGcmSenderId) {
        boolean playServiceAvailable = isPlayServicesAvailable();

        if (playServiceAvailable) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    repository.unRegisterGcm(pGcmSenderId)
                            .subscribeOn(Schedulers.io())
                            .observeOn(Schedulers.io());
                }
            }).start();
        }
    }
}
