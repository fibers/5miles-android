package com.thirdrock.fivemiles.main.home;

import android.content.Context;

import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.Utils.ImageUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;
import com.thirdrock.domain.Campaign;
import com.thirdrock.domain.Campaign__JsonHelper;
import com.thirdrock.domain.EnumCampaignType;
import com.thirdrock.fivemiles.di.RepositoryModule;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.framework.util.L;
import com.thirdrock.repository.ItemRepository;
import com.thirdrock.repository.impl.BodyParserFactoryImpl;
import com.thirdrock.repository.impl.ItemRepositoryImpl;

import org.apache.http.Header;

import java.io.File;
import java.io.IOException;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static android.content.Context.MODE_PRIVATE;
import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_APP_STATE;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_CURR_LAUNCH_AD;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;
import static com.thirdrock.framework.rest.AbsRestClient.DEFAULT_RETRIES;
import static com.thirdrock.framework.rest.AbsRestClient.DEFAULT_RETRY_INTV;
import static com.thirdrock.framework.rest.AbsRestClient.DEFAULT_TIMEOUT;

/**
 * Advertise utils
 * Created by ywu on 14/12/9.
 */
public class AdHelper {
    private Context context;

    private AsyncHttpClient httpClient;

    private ItemRepository itemRepo;

    public AdHelper() {
        this.context = FiveMilesApp.getInstance().getApplicationContext();

        // 此处使用SyncHttpClient，把线程控制的任务交给RxJava
        httpClient = new SyncHttpClient();
        httpClient.setTimeout(DEFAULT_TIMEOUT);
        httpClient.setMaxRetriesAndTimeout(DEFAULT_RETRIES, DEFAULT_RETRY_INTV);

        itemRepo = new ItemRepositoryImpl(
                RepositoryModule.getBaseUrl(),
                RepositoryModule.getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }

    /**
     * Download the campaign picture.
     *
     * @param campaign campaign metadata
     * @param w desired picture width
     * @param h desired picture height
     * @return local path of the downloaded picture
     */
    public Observable<File> downloadAdPicture(Campaign campaign, int w, int h) {
        final File path = determinePicturePath(campaign);
        final String url = CloudHelper.toCropUrl(campaign.getImageUrl(), w, h, "fill", true);

        return Observable.create(new Observable.OnSubscribe<File>() {
            @Override
            public void call(final Subscriber<? super File> subscriber) {
                if (subscriber.isUnsubscribed()) {
                    return;
                }

                httpClient.get(url, new FileAsyncHttpResponseHandler(path) {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                        if (!subscriber.isUnsubscribed()) {
                            subscriber.onError(throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, File file) {
                        if (!subscriber.isUnsubscribed()) {
                            L.d("save campaign picture (%s) to %s", url, file);
                            subscriber.onNext(file);
                            subscriber.onCompleted();
                        }
                    }
                });
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Choose a destination path for the advertise picture.
     *
     * @param campaign campaign metadata
     * @return target local path
     */
    public File determinePicturePath(Campaign campaign) {
        File dir = new File(ImageUtils.getCacheImagePath());
        if (!dir.exists()) {
            dir.mkdirs();
        }

        if (campaign == null) {
            return null;
        }

        String suffix = getFileSuffix(campaign.getImageUrl());
        return new File(dir, "ad_" + campaign.getType().ordinal() + "_"
                + campaign.getVersion() + suffix);
    }

    private String getFileSuffix(String name) {
        int lastDot = name.lastIndexOf(".");
        return lastDot >= 0 ? name.substring(lastDot) : "";
    }

    public Observable<Campaign> loadAd() {
        // get one active campaigner from backend
        return itemRepo.getCampaigns(EnumCampaignType.LAUNCH)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<List<Campaign>, Campaign>() {
                    @Override
                    public Campaign call(List<Campaign> campaigns) {
                        if (campaigns == null || campaigns.isEmpty()) {
                            return null;
                        } else {
                            Campaign campaign = campaigns.get(0);
                            campaign.setType(EnumCampaignType.LAUNCH);
                            return campaign;
                        }
                    }
                });
    }

    // save campaign for next launch time
    public void saveCampaign(Campaign campaign) {
        if (campaign == null) {
            return;
        }

        L.d("saving campaign %d", campaign.getId());
        try {
            context.getSharedPreferences(PREFS_APP_STATE, MODE_PRIVATE)
                    .edit().putString(PREF_KEY_CURR_LAUNCH_AD, Campaign__JsonHelper.serializeToJson(campaign))
                    .apply();
        } catch (IOException e) {
            L.e(e);
        }
    }

    /**
     * Load cached campaign data
     */
    public Campaign loadCurrCampaign() {
        try {
            String json = context.getSharedPreferences(PREFS_APP_STATE, MODE_PRIVATE)
                    .getString(PREF_KEY_CURR_LAUNCH_AD, "");
            return isNotEmpty(json) ? Campaign__JsonHelper.parseFromJson(json) : null;
        } catch (IOException e) {
            L.e(e);
            return null;
        }
    }
}
