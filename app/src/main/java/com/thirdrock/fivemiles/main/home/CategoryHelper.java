package com.thirdrock.fivemiles.main.home;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.widget.ImageView;

import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.Utils.ResourceDownloader;
import com.pedrogomez.renderers.AdapteeCollection;
import com.thirdrock.domain.CategoryInfo;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.di.RepositoryModule;
import com.thirdrock.fivemiles.util.FileUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.SimpleObserver;
import com.thirdrock.protocol.CategoriesResp;
import com.thirdrock.protocol.CategoriesResp__JsonHelper;
import com.thirdrock.repository.CategoriesRepository;
import com.thirdrock.repository.impl.BodyParserFactoryImpl;
import com.thirdrock.repository.impl.CategoriesRepositoryImpl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.inject.Singleton;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_APP_DATA;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_CATGRY_RESP;
import static com.insthub.fivemiles.FiveMilesAppConst.getCategoryIconPath;

/**
 * Helper class for category operations, such as load and store category info
 * Created by jubin on 9/3/15.
 */
@Singleton
public class CategoryHelper {
    private CategoriesRepository categoriesRepo;
    private Context mContext;

    private final Observer<CategoriesResp> catgryObserver = new SimpleObserver<CategoriesResp>() {
        @Override
        public void onNext(CategoriesResp resp) {
            onCategoriesRespLoaded(resp);
        }
    };

    public CategoryHelper() {
        this.mContext = FiveMilesApp.getInstance().getApplicationContext();

        categoriesRepo = new CategoriesRepositoryImpl(
                RepositoryModule.getBaseUrl(),
                RepositoryModule.getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }

    /**
     * 首先检查是否需要加载本地分类，若需要，则首先加载本地分类。然后发起网络调用，成功则覆盖本地分类。
     */
    public void loadCategories() {
        tryLoadLocalCategories();
        loadRemoteCategories();
    }

    private void tryLoadLocalCategories() {
        if (hasCachedCategories()) {
            return;
        }

        L.d("loading local categories...");

        try {
            String localCategoriesStr = FileUtils.readFileContent(mContext.getResources().openRawResource(R.raw.categories));
            saveCategoriesResp(localCategoriesStr);
        } catch (IOException e) {
            L.e("load raw categories failed(local)");
        }
    }

    /**
     * 从服务器更新categories信息，在工作线程运行，主线程监听
     */
    public void loadRemoteCategories() {
        categoriesRepo.getCategoriesResp()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(catgryObserver);
        L.d("start loading remote categories...");
    }

    public boolean saveCategoriesResp(CategoriesResp resp) {
        if (resp == null) {
            return false;
        }

        try {
            String oldStr = getCategoriesStr();

            String respStr = CategoriesResp__JsonHelper.serializeToJson(resp);
            saveCategoriesResp(respStr);

            L.d("categories resp saved: " + respStr);
            return !TextUtils.equals(oldStr, respStr);
        } catch (IOException e) {
            L.e("saving categories failed with exception: " + e);
        }

        return false;
    }

    /**
     * 使用SP的apply()方法，会使对SP的修改异步存储。后面的修改会覆盖前面的，所以local的categories信息要
     * 先加载
     *
     * @param respStr json
     */
    public void saveCategoriesResp(String respStr) {
        if (ModelUtils.isEmpty(respStr)) {
            return;
        }

        if (this.mContext != null) {
            mContext.getSharedPreferences(PREFS_APP_DATA, Context.MODE_PRIVATE)
                    .edit().putString(PREF_KEY_CATGRY_RESP, respStr)
                    .apply();
        }
    }

    public String getCategoriesStr() {
        if (!hasCachedCategories()) {
            tryLoadLocalCategories();
        }

        return mContext.getSharedPreferences(PREFS_APP_DATA, Context.MODE_PRIVATE)
                .getString(PREF_KEY_CATGRY_RESP, "");
    }

    private boolean hasCachedCategories() {
        return mContext.getSharedPreferences(PREFS_APP_DATA, Context.MODE_PRIVATE)
                .contains(PREF_KEY_CATGRY_RESP);
    }

    // load categories in background for search
    private void onCategoriesRespLoaded(CategoriesResp resp) {
        if (resp == null) {
            return;
        }

        boolean updated = saveCategoriesResp(resp);
        cacheCategoryIcons(resp, updated);
    }

    /**
     * @param updated updated since previous caching
     */
    private void cacheCategoryIcons(CategoriesResp resp, boolean updated) {
        for (CategoryInfo c : resp.getCategories()) {
            cacheCategoryIcon(c, updated);
        }
    }

    private void cacheCategoryIcon(CategoryInfo c, boolean updated) {
        File localFile = getCategoryIconPath(c);
        if (!updated && localFile.exists()) {
            L.d("categories don't has an update, and icon is already cached, abort caching #%d", c.getId());
            return;
        }

        ResourceDownloader.downloadResource(c.getIconUrl(), localFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    public void populateCategoryCollection(AdapteeCollection<CategoryInfo> collection) {
        collection.clear();
        collection.addAll(getCategoryList());
    }

    private List<CategoryInfo> getCategoryList() {
        List<CategoryInfo> categoryList = null;

        String categoriesStr = getCategoriesStr();
        try {
            CategoriesResp resp = CategoriesResp__JsonHelper.parseFromJson(categoriesStr);
            if (resp != null) {
                categoryList = resp.getCategories();
            }
        } catch (IOException e) {
            L.e("error parsing default categories: %s", categoriesStr);
        }

        return categoryList;
    }

    public CategoryInfo getCategoryById(int id) {
        List<CategoryInfo> categoryList = getCategoryList();
        for (CategoryInfo category : categoryList) {
            if (category != null && category.getId() == id) {
                return category;
            }
        }
        return null;
    }

    public static void displayCategoryIcon(final CategoryInfo c, final ImageView iv) {
        File iconPath = getCategoryIconPath(c);

        if (iconPath.exists()) {
            displayCategoryIcon(iv, iconPath);
            return;
        }

        ResourceDownloader.downloadResource(c.getIconUrl(), iconPath)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<File>() {
                    @Override
                    public void onNext(File file) {
                        displayCategoryIcon(iv, file);
                    }
                });
    }

    private static void displayCategoryIcon(ImageView iv, File iconPath) {
        Bitmap bm = BitmapFactory.decodeFile(iconPath.getAbsolutePath());
        iv.setImageBitmap(bm);
    }

}
