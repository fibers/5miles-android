package com.thirdrock.fivemiles.main.home;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * View model for home following fragment
 * Created by jubin on 9/4/15.
 */
public class HomeFollowingViewModel extends HomeNearbyViewModel {

    @Inject
    public HomeFollowingViewModel() {
        // work around the 'no injectable members' problem
    }

    public void loadFollowingItemThumbs(float lat, float lon) {
        if (loading) {
            return;
        }

        loading = true;
        itemRepository.getFollowingItemThumbs(lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itemObserver.reset());
    }

    public void loadMoreFollowingItemThumbs(float lat, float lon) {
        if (loading) {
            return;
        }

        loading = true;
        itemRepository.getMoreFollowingItemThumbs(lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(incrItemObserver.reset());
    }

    public boolean hasMoreFollowingItems() {
        return itemRepository.hasMoreFollowingItems();
    }
}
