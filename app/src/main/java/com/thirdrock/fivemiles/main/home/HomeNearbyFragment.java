package com.thirdrock.fivemiles.main.home;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.external.activeandroid.util.Log;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Utils.LocationManagerUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.pedrogomez.renderers.RendererAdapter;
import com.thirdrock.domain.Campaign;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.fragment.AbsFragment;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.EventUtils;
import com.thirdrock.framework.ui.renderer.MonoRendererBuilder;
import com.thirdrock.framework.ui.widget.WaterfallListView;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.SimpleObserver;

import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnItemClick;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM_THUMB;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_HOME;
import static com.insthub.fivemiles.MessageConstant.REFRESH_HOME;
import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLatitudeInSession;
import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLongitudeInSession;
import static com.thirdrock.fivemiles.main.home.HomeItemSqliteOpenHelper.TBL_HOME_NEARBY_ITEMS;
import static com.thirdrock.fivemiles.main.home.HomeNearbyViewModel.Property;
import static com.thirdrock.framework.util.Utils.doOnGlobalLayout;

/**
 * Nearby tab of home
 * Created by jubin on 2/4/15.
 */
public class HomeNearbyFragment extends AbsFragment<HomeNearbyViewModel, Property> implements
        LocationManagerUtil.OnLocationListener, WaterfallListView.Callback, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    HomeNearbyViewModel viewModel;

    @InjectView(R.id.home_nearby_swipe_refresh_view)
    SwipeRefreshLayout swipeRefreshView;

    @InjectView(R.id.home_nearby_list_items)
    WaterfallListView lstItems;

    private Banner bannerUtil;

    private LocationManagerUtil locationMgr;
    private float lastLatitude, lastLongitude;

    private RendererAdapter<ItemThumb> itemListAdapter;
    private ItemThumbCollection itemThumbs;
    private Subscription subsItemThumbs;

    private final Action0 onItemAdapterUpdated = new Action0() {
        @Override
        public void call() {
            itemListAdapter.notifyDataSetChanged();

            L.d("showItems hasMore=%s", viewModel.hasMoreNearbyItems());
            lstItems.onLoadMoreComplete(true, viewModel.hasMoreNearbyItems());
            swipeRefreshView.setRefreshing(false);
        }
    };

    private final SimpleObserver<ItemThumb> onItemThumbObserved = new SimpleObserver<ItemThumb>() {
        @Override
        public void onNext(ItemThumb thumb) {
            viewItemDetail(thumb);
        }
    };

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {
        locationMgr = LocationManagerUtil.getInstance();
        locationMgr.getLocation();

        Log.d("the time zone is " + TimeZone.getDefault().getID());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initListView(savedInstanceState);
    }

    private void initListView(Bundle savedInstanceState) {
        lstItems.setCallback(this);

        DisplayUtils.setProgressColorScheme(swipeRefreshView);
        swipeRefreshView.setOnRefreshListener(this);
        swipeRefreshView.setProgressViewOffset(true,
                getResources().getDimensionPixelSize(R.dimen.swipe_refresh_progress_offset_start),
                getResources().getDimensionPixelSize(R.dimen.swipe_refresh_progress_offset_end));

        // banner advertisement
        bannerUtil = new Banner(getActivity());
        lstItems.addHeaderView(bannerUtil.getBanner());

        itemThumbs = new ItemThumbCollection(TBL_HOME_NEARBY_ITEMS);
        initListAdapter(savedInstanceState);

        // 首次加载数据，模拟下拉刷新
        doOnGlobalLayout(lstItems, new Runnable() {
            @Override
            public void run() {
                // wait layout complete in order to show the refreshing animation
                animateRefresh();
                loadBanner();
            }
        });
    }

    private void initListAdapter(Bundle savedInstanceState) {
        itemListAdapter = new RendererAdapter<>(
                getLayoutInflater(savedInstanceState),
                new MonoRendererBuilder(new AsyncItemThumbRenderer()),
                itemThumbs);
        lstItems.setAdapter(itemListAdapter);
    }

    private void animateRefresh() {
        if (!swipeRefreshView.isRefreshing()) {
            swipeRefreshView.setRefreshing(true);
            onRefresh();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventUtils.register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        locationMgr.setOnLocationListener(this);
        locationMgr.requestLocationUpdates();
    }

    @Override
    public void onPause() {
        super.onPause();
        locationMgr.setOnLocationListener(null);
        locationMgr.stopLocationUpdates();

        if (swipeRefreshView.isRefreshing()) {
            swipeRefreshView.setRefreshing(false);
            lstItems.onLoadMoreComplete();
        }

        itemThumbs.pause();
        unsubsItemThumbs();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        itemThumbs.destroy();
        EventUtils.unregister(this);
    }

    private void loadBanner() {
        viewModel.loadBanner();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_home_nearby;
    }

    @Override
    protected String getScreenName() {
        return VIEW_HOME;
    }

    @Override
    protected HomeNearbyViewModel getViewModel() {
        return viewModel;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {
        switch (property) {
            case items:
                showItems((List<ItemThumb>) newValue, false);
                break;
            case more_items:
                showItems((List<ItemThumb>) newValue, true);
                break;
            case banner:
                showBanner((Campaign) newValue);
                break;
            default:
                break;
        }
    }

    private void showItems(List<ItemThumb> newItemList, boolean incr) {
        Observable<Void> updateJob;

        if (incr) {
            updateJob = itemThumbs.append(newItemList);
        } else {
            updateJob = itemThumbs.reset(newItemList);
        }

        unsubsItemThumbs();
        subsItemThumbs = updateJob.doOnCompleted(onItemAdapterUpdated).subscribe();
    }

    private void unsubsItemThumbs() {
        if (subsItemThumbs == null || subsItemThumbs.isUnsubscribed()) {
            return;
        }
        subsItemThumbs.unsubscribe();
        subsItemThumbs = null;
    }

    private void showBanner(Campaign campaign) {
        if (campaign == null) {
            return;
        }

        bannerUtil.showCampaign(campaign);
    }

    @Override
    public void onError(Property property, Throwable e) {
        super.onError(property, e);
        onMinorJobError(property, e);
    }

    @Override
    public void onMinorJobError(Property property, Throwable e) {
        if (property == Property.more_items) {
            lstItems.onLoadMoreComplete(false, true);
        }
        swipeRefreshView.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        lastLatitude = getLatitudeInSession();
        lastLongitude = getLongitudeInSession();
        viewModel.loadNearbyItemThumbs(lastLatitude, lastLongitude);

        lstItems.onLoadMoreComplete();
        trackTouch("top_refresh");
    }

    @Override
    public void onLoadMore() {
        L.d("onLoadMore hasMore=%s", viewModel.hasMoreNearbyItems());

        if (viewModel.hasMoreNearbyItems()) {
            L.d("home load, loading more");
            lastLatitude = getLatitudeInSession();
            lastLongitude = getLongitudeInSession();
            viewModel.loadMoreNearbyItemThumbs(lastLatitude, lastLongitude);

            swipeRefreshView.setRefreshing(false);
            trackTouch("bottom_load");
        }
    }

    @Override
    public void onPauseRendering() {
        ImageLoader.getInstance().pause();
    }

    @Override
    public void onResumeRendering() {
        ImageLoader.getInstance().resume();
    }

    @Override
    public void onScroll(boolean isAtTop, WaterfallListView.OnScrollListener.ScrollDirection scrollDirection) {
        swipeRefreshView.setEnabled(isAtTop);
    }

    @Override
    public void onLocationUpdated() {
        if (lastLatitude > 0 || lastLongitude > 0) {
            // 如果之前已经定位成功了，则只重新计算距离，不刷新列表，避免打断用户的浏览
            notifyLocationUpdated();
        } else {
            // 如果之前没有获得有效的位置，则刷新列表
            scrollToHomeAndRefresh();
        }
    }

    private void notifyLocationUpdated() {
        if (itemThumbs == null) {
            return;
        }

        int start = lstItems.getFirstVisiblePosition();
        int end = lstItems.getLastVisiblePosition();
        for (int i = start; i <= end && i < itemThumbs.size(); i++) {
            ItemThumb item = itemThumbs.get(i);
            item.notifyObservers(MessageConstant.PROP_DISTANCE_UPDATED);
        }
    }

    private void scrollToHomeAndRefresh() {
        L.d("pull and fresh");
//        FIXME onStop/onStart之后notifyDataSetInvalidated不起作用，暂时重建adapter
//        itemListAdapter.notifyDataSetInvalidated();
        initListAdapter(null);
        animateRefresh();
    }

    @OnItemClick(R.id.home_nearby_list_items)
    @SuppressWarnings("unused")
    void onItemClick(int position) {
        int index = position - lstItems.getHeaderViewsCount();
        if (index >= 0 && index < itemThumbs.size()) {
            ItemThumb item = itemThumbs.get(index);

            if (item instanceof ObservableItemThumb) {
                ((ObservableItemThumb) item).getObservable().subscribe(onItemThumbObserved);
            } else {
                viewItemDetail(item);
            }
            trackTouch("home_product");
        }
    }

    private void viewItemDetail(ItemThumb item) {
        if (getActivity() == null) {
            L.w("getActivity() return null in viewItemDetail(ItemThumb) with item: " + item);
        } else {
            Intent intent = new Intent(getActivity(), ItemActivity.class);
            intent.putExtra(ACT_PARA_ITEM_THUMB, item);
            startActivity(intent);
        }
    }

    @SuppressWarnings("unused")
    public void onEvent(Object event) {
        Message message = (Message) event;

        switch (message.what) {
            case REFRESH_HOME:
                scrollToHomeAndRefresh();
                break;
        }
    }
}