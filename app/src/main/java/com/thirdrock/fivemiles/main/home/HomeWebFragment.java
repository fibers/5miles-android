package com.thirdrock.fivemiles.main.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.insthub.fivemiles.Activity.SearchResultActivity;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.DisplayUtils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * WebView的Container，没有很多交互，暂时不必继承自AbsFragment
 */
public class HomeWebFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private String mUrl;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private WebView mWebView;

    public static HomeWebFragment newInstance(String url) {
        HomeWebFragment fragment = new HomeWebFragment();
        fragment.mUrl = url;
        return fragment;
    }

    public HomeWebFragment() {
        // Required empty public constructor
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootFragmentView = inflater.inflate(R.layout.fragment_home_web, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootFragmentView.findViewById(R.id.home_web_swipe_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mWebView = (WebView) rootFragmentView.findViewById(R.id.home_frgmnt_webview);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mSwipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                // 否则progress bar会一直转
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.addJavascriptInterface(new WebAppInterface(getActivity()), "Android");

        Map<String, String> extraHeaders = new HashMap<>(1);
        extraHeaders.put("Accept-Language", Locale.getDefault().getLanguage());
        mWebView.loadUrl(mUrl, extraHeaders);

        return rootFragmentView;
    }

    @Override
    public void onRefresh() {
        mWebView.loadUrl(mUrl);
    }


    private class WebAppInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        WebAppInterface(Context pContext) {
            mContext = pContext;
        }

        @JavascriptInterface
        public void searchCategory(int catId, String catTitle) {
            Intent searchCategoryIntent = new Intent(mContext, SearchResultActivity.class)
                    .putExtra("category_id", catId)
                    .putExtra("category_title", catTitle);
            startActivity(searchCategoryIntent);
        }

        @JavascriptInterface
        public void uiToast(String toastMsg) {
            DisplayUtils.toast(toastMsg);
        }
    }
}
