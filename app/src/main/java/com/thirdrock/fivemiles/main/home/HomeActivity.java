package com.thirdrock.fivemiles.main.home;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.facebook.AppEventsLogger;
import com.insthub.fivemiles.Activity.GenericWebActivity;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.activity.AbsFragmentActivity;
import com.thirdrock.fivemiles.login.EmptyViewModel;
import com.thirdrock.fivemiles.login.EmptyViewModel.Property;
import com.thirdrock.fivemiles.util.EventUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.util.L;

import butterknife.InjectView;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_PAGE_URL;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SHOW_SHARE;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_TOP_SHARE_TEXT;
import static com.insthub.fivemiles.FiveMilesAppConst.HOME_REVIEW_PAGE_URL;
import static com.insthub.fivemiles.FiveMilesAppConst.HOME_REVIEW_SHARE_URL;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_HOME;
import static com.insthub.fivemiles.MessageConstant.HOME_TOOLBAR_SEARCH_CLK;

public class HomeActivity extends AbsFragmentActivity<EmptyViewModel, Property> {

    public static final int IDX_NEARBY_PAGE = 0;
    public static final int IDX_FOLLOWING_PAGE = 1;

    @InjectView(R.id.home_pager)
    ViewPager viewPager;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.tabs)
    TabLayout tabLayout;

    private AppEventsLogger fbEventsLogger;

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {
        fbEventsLogger = AppEventsLogger.newLogger(this);

        setSupportActionBar(toolbar);

        FragmentManager fm = getSupportFragmentManager();
        HomeFragmentPagerAdapter pagerAdapter = new HomeFragmentPagerAdapter(fm);
        viewPager.setAdapter(pagerAdapter);

        tabLayout.setupWithViewPager(viewPager);
        EventUtils.registerSticky(this);
    }

    @Override
    protected void onResume() {
        fbEventsLogger.logEvent(VIEW_HOME);

        ItemThumbDao.openDatabase();

        super.onResume();

        // 作为首页的一个tab，只有一次create、start，pv需在resume里记录
        TrackingUtils.trackView(VIEW_HOME);
    }

    @Override
    protected void onPause() {
        // 只有onPause是确定会被调用的，所以放在这里。这里的设计是此DB生命周期和Acitivy一致
        ItemThumbDao.closeDatabase();

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventUtils.unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item != null) {
            switch (item.getItemId()) {
                case R.id.home_action_search:
                    EventUtils.post(HOME_TOOLBAR_SEARCH_CLK);
                    return true;
                case R.id.home_action_show_reviews:
                    Intent h5Intent = new Intent(HomeActivity.this, GenericWebActivity.class)
                            .putExtra(ACT_PARA_PAGE_URL, HOME_REVIEW_PAGE_URL)
                            .putExtra(ACT_PARA_SHOW_SHARE, true)
                            .putExtra(ACT_PARA_TOP_SHARE_TEXT, HOME_REVIEW_SHARE_URL);
                    startActivity(h5Intent);
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }
        return false;
    }

    @Override
    protected String getScreenName() {
        return VIEW_HOME;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_home;
    }

    @Override
    protected EmptyViewModel getViewModel() {
        return new EmptyViewModel();
    }

    @Override
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {
        // empty
    }

    // TODO IDX_NEARBY_PAGE和IDX_FOLLOWING_PAGE使用同一个what，不同的obj
    // TODO 检查是否留用
    public void onEvent(Object event) {
        Message message = (Message) event;
        if (message == null) {
            L.w("message from EventBus should not be null");
            return;
        }

        switch (message.what) {
            case IDX_NEARBY_PAGE:
                viewPager.setCurrentItem(IDX_NEARBY_PAGE);
                break;
            case IDX_FOLLOWING_PAGE:
                viewPager.setCurrentItem(IDX_FOLLOWING_PAGE);
                break;
            default:
                break;
        }
    }
}
