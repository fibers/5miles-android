package com.thirdrock.fivemiles.main.home;

import com.thirdrock.domain.Campaign;
import com.thirdrock.domain.EnumCampaignType;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.framework.rest.RestObserver;
import com.thirdrock.framework.rest.SingleResponseRestObserver;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.repository.ItemRepository;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * View model for home nearby fragment
 * Created by ywu on 14-10-7.
 */
public class HomeNearbyViewModel extends AbsViewModel<HomeNearbyViewModel.Property> {

    public enum Property {
        items, more_items, banner
    }

    protected class ItemThumbsObserver extends SingleResponseRestObserver<List<ItemThumb>> {
        private boolean increment;

        private ItemThumbsObserver() {
            this(false);
        }

        private ItemThumbsObserver(boolean increment) {
            this.increment = increment;
            setRequestId(increment ? "home_more" : "home");
        }

        @Override
        public void onCompleted() {
            loading = false;
        }

        @Override
        public void onError(Throwable e) {
            loading = false;
            if (!increment) {
                emit(Property.items, e);
            } else {
                emitMinorJobError(Property.more_items, e);
            }
        }

        @Override
        public void doOnNext(List<ItemThumb> itemThumbs) {
            if (!increment) {
                resetIdCache(itemThumbs);
                emit(Property.items, null, itemThumbs);
                return;
            }

            // 增量更新，需要排重
            List<ItemThumb> result = new LinkedList<>();
            for (ItemThumb item : itemThumbs) {
                if (ensureItemUnique(item)) {
                    result.add(item);
                }
            }
            emit(Property.more_items, null, result);
        }
    }

    @Inject
    protected ItemRepository itemRepository;

    protected final ItemThumbsObserver itemObserver, incrItemObserver;

    protected final RestObserver<Campaign> bannerObserver;

    protected final Map<String, Byte> itemIdCache = new HashMap<>();
    protected boolean loading;

    public HomeNearbyViewModel() {
        itemObserver = new ItemThumbsObserver();
        incrItemObserver = new ItemThumbsObserver(true);

        bannerObserver = newMinorApiJobObserver("campaigns_banner", Property.banner);
    }

    public void loadNearbyItemThumbs(float lat, float lon) {
        if (loading) {
            return;
        }

        loading = true;
        itemRepository.getNearbyItemThumbs(lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itemObserver.reset());
    }

    public void loadMoreNearbyItemThumbs(float lat, float lon) {
        if (loading) {
            return;
        }

        loading = true;
        itemRepository.getMoreNearbyItemThumbs(lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(incrItemObserver.reset());
    }

    public boolean hasMoreNearbyItems() {
        return itemRepository.hasMoreNearbyItems();
    }

    protected void resetIdCache(List<ItemThumb> items) {
        itemIdCache.clear();
        for (ItemThumb item : items) {
            itemIdCache.put(item.getId(), (byte) 1);
        }
    }

    protected boolean ensureItemUnique(ItemThumb item) {
        String id = item.getId();
        boolean unique = !itemIdCache.containsKey(id);
        if (unique) {
            itemIdCache.put(id, (byte) 1);
        }

        return unique;
    }

    public void loadBanner() {
        // get one active campaigner from backend
        itemRepository.getCampaigns(EnumCampaignType.BANNER)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<List<Campaign>, Campaign>() {
                    @Override
                    public Campaign call(List<Campaign> campaigns) {
                        return campaigns == null || campaigns.isEmpty() ? null : campaigns.get(0);
                    }
                })
                .subscribe(bannerObserver.reset());
    }
}
