package com.thirdrock.fivemiles.main.profile;

import android.content.Intent;
import android.os.Bundle;

import com.BeeFramework.activity.BaseActivity;
import com.insthub.fivemiles.Activity.OtherProfileActivity;
import com.insthub.fivemiles.Activity.TabProfileActivity;
import com.thirdrock.framework.R;

import java.util.regex.Pattern;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_CAN_GO_BACK;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PUSH_USER_PREFIX;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;
import static com.thirdrock.fivemiles.util.ModelUtils.isMe;

/**
 * 处理个人页deep link，转发给个人页或他人页
 */
public class ProfileRouteActivity extends BaseActivity {

    public static final Pattern LINK_PATTERN = Pattern.compile("/?person/([^/]+)(?:/.*)?");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transparent);
        onIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        onIntent(intent);
    }

    private void onIntent(Intent pIntent) {
        if (isFinishing() || pIntent == null) {
            return;
        }

        String action = pIntent.getAction();
        if (intentActionMatch(action) && pIntent.hasExtra(ACT_PARA_USER_ID)) {
            String userID = pIntent.getStringExtra(ACT_PARA_USER_ID);
            viewProfile(userID);
        }

        finish();
    }

    // push过来的actin是ACT_PUSH_USER_PREFIX，TODO Intent.ACTION_VIEW验证，看下这里是否可以合并
    private boolean intentActionMatch(String action) {
        return Intent.ACTION_VIEW.equals(action) || action.startsWith(ACT_PUSH_USER_PREFIX);
    }

    private void viewProfile(String uid) {
        if (isEmpty(uid)) {
            return;
        }

        Intent intent;
        if (isMe(uid)) {
            intent = new Intent(this, TabProfileActivity.class);
            intent.putExtra(ACT_PARA_CAN_GO_BACK, true);
        } else {
            intent = new Intent(this, OtherProfileActivity.class);
            intent.putExtra(ACT_PARA_USER_ID, uid);
        }
        startActivity(intent);
    }
}
