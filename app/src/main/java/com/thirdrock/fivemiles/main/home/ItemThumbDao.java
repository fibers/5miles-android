package com.thirdrock.fivemiles.main.home;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.thirdrock.domain.ItemThumb;
import com.thirdrock.domain.ItemThumb__JsonHelper;
import com.thirdrock.framework.util.L;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static com.thirdrock.fivemiles.main.home.HomeItemSqliteOpenHelper.F_ITEM_DATA;
import static com.thirdrock.fivemiles.main.home.HomeItemSqliteOpenHelper.F_ITEM_ID;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;

/**
 * Home tab item thumbs data accessor
 * Created by ywu on 15/2/17.
 */
public class ItemThumbDao {

    private final String SQL_TRUNCATE_TBL;
    private final String SQL_Q_BY_ID;
    private final String SQL_Q_ALL;

    private String tableName;

    private static HomeItemSqliteOpenHelper sqliteOpenHelper = HomeItemSqliteOpenHelper.getInstance();

    public ItemThumbDao(String tableName) {
        if (isEmpty(tableName)) {
            L.e("Table name can't be null");
            throw new IllegalStateException("table name should not be null");
        }

        this.tableName = tableName;

        SQL_TRUNCATE_TBL = "DELETE FROM " + tableName + ";";
        SQL_Q_BY_ID = "SELECT item_data FROM " + tableName + " WHERE " + F_ITEM_ID + " BETWEEN ? AND ?;";
        SQL_Q_ALL = "SELECT item_data FROM " + tableName + ";";
    }

    public synchronized void truncate() {
        SQLiteDatabase db = sqliteOpenHelper.getWritableDatabase();
        db.execSQL(SQL_TRUNCATE_TBL);
    }

    public synchronized void reset(List<ItemThumb> items) {
        SQLiteDatabase db = sqliteOpenHelper.getWritableDatabase();

        try {
            db.beginTransaction();
            truncate();
            insert(0, items);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public synchronized void insert(int offset, Collection<ItemThumb> items) {
        SQLiteDatabase db = sqliteOpenHelper.getWritableDatabase();
        int i = offset;
        for (ItemThumb item : items) {
            insert(db, i++, item);
        }
    }

    public synchronized void insert(int offset, List<ItemThumb> items) {
        insert(offset, (Collection<ItemThumb>) items);
    }

    private void insert(SQLiteDatabase db, int index, ItemThumb item) {
        L.v("insert item at %d", index);
        try {
            ContentValues values = new ContentValues(2);
            values.put(F_ITEM_ID, index);
            values.put(F_ITEM_DATA, ItemThumb__JsonHelper.serializeToJson(item));
            db.insert(tableName, null, values);
        } catch (IOException e) {
            throw new IllegalArgumentException("insert item thumb failed", e);
        }
    }

    public synchronized Cursor getItemThumbs(int from, int to) {
        SQLiteDatabase db = sqliteOpenHelper.getReadableDatabase();
        return db.rawQuery(SQL_Q_BY_ID, new String[] { String.valueOf(from), String.valueOf(to) });
    }

    public synchronized Cursor getItemThumbs() {
        SQLiteDatabase db = sqliteOpenHelper.getReadableDatabase();
        return db.rawQuery(SQL_Q_ALL, null);
    }

    // 参考http://www.vogella.com/tutorials/AndroidSQLite/article.html中的CommentsDataSource
    public static void openDatabase() {
        sqliteOpenHelper.getWritableDatabase();
    }

    public static void closeDatabase() {
        sqliteOpenHelper.close();
    }
}
