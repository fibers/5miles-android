package com.thirdrock.fivemiles.framework.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.thirdrock.framework.util.L;

/**
 * Created by ywu on 14/11/21.
 */
public class SquareFrameLayout extends FrameLayout {
    public SquareFrameLayout(Context context) {
        super(context);
    }

    public SquareFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int size = Math.min(widthMeasureSpec, heightMeasureSpec);
        super.onMeasure(size, size);

        int w = MeasureSpec.getSize(widthMeasureSpec);
        int h = MeasureSpec.getSize(heightMeasureSpec);
//        size = h > 0 ? Math.min(w, h) : w;
        L.d("fitting square size: %d", w);
        setMeasuredDimension(w, w);
    }
}
