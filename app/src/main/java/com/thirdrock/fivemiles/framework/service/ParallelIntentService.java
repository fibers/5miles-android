package com.thirdrock.fivemiles.framework.service;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;

import com.thirdrock.framework.util.L;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * IntentService that allow jobs running in parallel.
 * Created by ywu on 14-8-17.
 */
public abstract class ParallelIntentService extends Service {

    /**
     * extra param key for reporting execution exception
     */
    public static final String EXTRA_EXCEPTION = "ParallelIntents.ResultException";

    /**
     * extra param key of the ResultReceiver
     */
    private static final String EXTRA_RESULT_RECEIVER = "ParallelIntents.ResultReceiver";

    private final Object[] lock = new Object[0];
    private ExecutorService execSvc;
    private volatile int runningJobs = 0;

    public static Intent getIntent(Context ctx,
                                   Class<? extends ParallelIntentService> cls,
                                   String action) {
        Intent intent = new Intent(ctx, cls);
        intent.setAction(action);
        return intent;
    }

    public static Intent getIntent(Context ctx,
                                   Class<? extends ParallelIntentService> cls,
                                   String action,
                                   ResultReceiver resultReceiver) {
        Intent intent = getIntent(ctx, cls, action);
        if (resultReceiver != null) {
            intent.putExtra(EXTRA_RESULT_RECEIVER, resultReceiver);
        }
        return intent;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        execSvc = createExecutorService();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        execSvc.shutdownNow();
        synchronized (lock) {
            runningJobs = 0;
        }
    }

    /**
     * create instance of ExecutorService, default is Executors.newCachedThreadPool()
     */
    protected ExecutorService createExecutorService() {
        return Executors.newCachedThreadPool();
    }

    @Override
    public final int onStartCommand(final Intent intent, int flags, int startId) {
        synchronized (lock) {
            execSvc.execute(new Runnable() {
                @Override
                public void run() {
                    handleCommand(intent);
                }
            });
            runningJobs++;
        }
        return START_NOT_STICKY;
    }

    private void handleCommand(Intent intent) {
        Bundle result = null;
        ResultReceiver callback = null;

        try {
            callback = intent.getParcelableExtra(EXTRA_RESULT_RECEIVER);
            result = onExecute(intent);

            if (result == null) {
                result = new Bundle();
            }

            if (callback != null) {
                callback.send(Activity.RESULT_OK, result);
            }
        } catch (Exception e) {
            L.e(e);
            //L.i("callback: " + callback + " result: " + result);
            if (result == null) {
                result = new Bundle();
            }

            if (callback != null) {
                result.putSerializable(EXTRA_EXCEPTION, e);
                callback.send(Activity.RESULT_CANCELED, result);
            }
        }
        finally {
            synchronized (lock) {
                runningJobs--;
                stopIfDone();
            }
        }
    }

    private void stopIfDone() {
        synchronized (lock) {
            if (runningJobs == 0) {
                stopSelf();
                onStop();
            }
        }
    }

    protected void onStop() {
    }

    /**
     * Processing a single intent, returning a bundle as result
     * @param intent Request intent
     * @return Bundle object contains result
     */
    abstract protected Bundle onExecute(Intent intent) throws Exception;
}
