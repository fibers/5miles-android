package com.thirdrock.fivemiles.framework.view;

import android.widget.CompoundButton;

import com.thirdrock.framework.util.L;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ywu on 14/11/17.
 */
public class CheckableGroup implements CompoundButton.OnCheckedChangeListener {

    final List<CompoundButton> checkables = new LinkedList<CompoundButton>();
    int checkedIndex = -1;

    public void add(CompoundButton checkable) {
        checkables.add(checkable);
        checkable.setOnCheckedChangeListener(this);
    }

    public void clear() {
        checkables.clear();
    }

    @Override
    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
        if (!isChecked) {
            checkedIndex = -1;
            L.d("clean checked");
            return;
        }

        int i = 0;
        for (CompoundButton chk : checkables) {
            if (chk == v) {
                checkedIndex = i;
                L.d("checked %d", i);
                i++;
                continue;
            }

            if (chk.isChecked()) {
                chk.setOnCheckedChangeListener(null);
                chk.setChecked(false);
                chk.setOnCheckedChangeListener(this);
            }
            i++;
        }
    }

    public int getCheckedIndex() {
        return checkedIndex;
    }
}
