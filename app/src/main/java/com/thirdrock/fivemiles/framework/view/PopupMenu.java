package com.thirdrock.fivemiles.framework.view;

import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.BeeFramework.Utils.AnimationUtil;
import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.animation.SimpleAnimationListener;

import java.util.LinkedList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

/**
 * A popup menu with a list of selection, implemented as a dialog,
 * Popup effect is sliding up from bottom now.
 * <p/>
 * Created by ywu on 15/4/14.
 */
public class PopupMenu extends GenericDialog {

    public static final int POSTIVE_BUTTON = 1;
    public static final int NEGATIVE_BUTTON = 2;

    public static class Option {
        int id;
        String name;

        public Option(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return name;
        }

        @Override
        public boolean equals(Object o) {
            return this == o || o instanceof Option && ((Option) o).id == this.id;
        }

        @Override
        public int hashCode() {
            return id;
        }
    }

    public static interface OnSelectionListener {
        void onOptionSelected(Option option);
    }

    public static interface OnClickListener {
        void onClick(GenericDialog dialog, int which);
    }

    private Context context;
    private OnSelectionListener selectionListener;
    private OnClickListener postiveButtonListener, negativeButtonListener;
    private final List<Option> options = new LinkedList<>();

    @InjectView(R.id.menu)
    View menuWrapper;

    @InjectView(R.id.menu_items)
    ViewGroup optionsViewGroup;

    @InjectView(R.id.postive_button)
    TextView postiveButton;

    @InjectView(R.id.negative_button)
    TextView negativeButton;

    public PopupMenu(Context context) {
        this(context, null);
    }

    public PopupMenu(Context context, List<Option> options) {
        super(context, R.layout.popup_menu);

        if (options != null) {
            this.options.addAll(options);
        }
    }

    @Override
    public void onViewInit(Context context, View contentView) {
        this.context = context;
        ButterKnife.inject(this, contentView);

        setCanceledOnTouchOutside(true);

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickNegativeButton();
            }
        });

        setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                PopupMenu.this.onClickNegativeButton();
            }
        });

        // make it full screen
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
    }

    private void populateOptions() {
        if (options.size() == 0) {
            return;
        }

        optionsViewGroup.removeAllViews();
        for (Option opt : options) {
            populateOption(opt);
        }
    }

    private void populateOption(final Option opt) {
        // add a separator if need
        if (optionsViewGroup.getChildCount() > 0) {
            addItemSeparator();
        }

        TextView txtOpt = new TextView(new ContextThemeWrapper(context, R.style.PopupMenuItem));
        txtOpt.setText(opt.getName());
        optionsViewGroup.addView(txtOpt);

        txtOpt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClicked(opt);
            }
        });
    }

    private void onItemClicked(final Option opt) {
        animateDismiss(new Runnable() {
            @Override
            public void run() {
                if (selectionListener != null) {
                    selectionListener.onOptionSelected(opt);
                }
            }
        });
    }

    private void addItemSeparator() {
        View hr = new View(new ContextThemeWrapper(context, R.style.PopupMenuItemSeparator));
        optionsViewGroup.addView(hr);
        ViewGroup.LayoutParams lp = hr.getLayoutParams();
        lp.height = context.getResources().getDimensionPixelSize(R.dimen.popup_item_divider_height);
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        hr.setLayoutParams(lp);
    }

    public PopupMenu addOption(Option opt) {
        if (opt != null) {
            options.add(opt);
        }

        return this;
    }

    public PopupMenu addOptions(Option... opts) {
        if (opts != null) {
            for (Option opt : opts) {
                addOption(opt);
            }
        }

        return this;
    }

    public PopupMenu addOptions(List<Option> opts) {
        if (opts != null) {
            this.options.addAll(opts);
        }

        return this;
    }

    public PopupMenu setOnSelectionListener(OnSelectionListener listener) {
        selectionListener = listener;
        return this;
    }

    public PopupMenu setPostiveButton(int textId, OnClickListener listener) {
        postiveButton.setText(textId);
        return setOnPostiveButtonListener(listener);
    }

    public PopupMenu setOnPostiveButtonListener(OnClickListener listener) {
        postiveButtonListener = listener;
        postiveButton.setVisibility(View.VISIBLE);
        return this;
    }

    public PopupMenu setNegativeButton(int textId, OnClickListener listener) {
        negativeButton.setText(textId);
        return setOnNegativeButtonListener(listener);
    }

    public PopupMenu setOnNegativeButtonListener(OnClickListener listener) {
        negativeButtonListener = listener;
        return this;
    }

    @Override
    public PopupMenu show() {
        showMenu();
        super.show();
        return this;
    }

    private void showMenu() {
        populateOptions();
        menuWrapper.setVisibility(View.VISIBLE);
        AnimationUtil.showAnimation(menuWrapper);
    }

    private void hideMenu(Animation.AnimationListener animCallback) {
        Animation anim = new TranslateAnimation(RELATIVE_TO_SELF, 0, RELATIVE_TO_SELF, 0,
                RELATIVE_TO_SELF, 0, RELATIVE_TO_SELF, 1);
        anim.setDuration(200);
        if (animCallback != null) {
            anim.setAnimationListener(animCallback);
        }
        menuWrapper.setVisibility(View.GONE);
        menuWrapper.startAnimation(anim);
    }

    @OnClick(R.id.negative_button)
    void onClickNegativeButton() {
        onClickButton(NEGATIVE_BUTTON, negativeButtonListener);
    }

    @OnClick(R.id.postive_button)
    void onClickPostiveButton() {
        onClickButton(POSTIVE_BUTTON, postiveButtonListener);
    }

    private void onClickButton(final int which, final OnClickListener listener) {
        hideMenu(new SimpleAnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                PopupMenu.this.dismiss();
                if (listener != null) {
                    listener.onClick(PopupMenu.this, which);
                }
            }
        });
    }

    private void animateDismiss(final Runnable animCallback) {
        hideMenu(new SimpleAnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                animCallback.run();
                PopupMenu.this.dismiss();
            }
        });
    }
}
