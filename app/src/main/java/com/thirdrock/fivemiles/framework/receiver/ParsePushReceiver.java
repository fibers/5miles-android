package com.thirdrock.fivemiles.framework.receiver;

import android.content.Context;
import android.content.Intent;

import com.insthub.fivemiles.Activity.MainTabActivity;
import com.insthub.fivemiles.Activity.MakeOfferActivity;
import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.util.PushActionUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.Push;
import com.thirdrock.protocol.Push__JsonHelper;
import com.thirdrock.protocol.SystemAction;

import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;

/**
 * Receiver for push from Parse
 * Created by jubin on 17/12/14.
 */
public class ParsePushReceiver extends ParsePushBroadcastReceiver {
    private static final String DATA_TARGET_INTENT = "ParsePushBroadcastReceiver.data.targetIntent";
    private static final String DATA_ORIGIN_INTENT = "ParsePushBroadcastReceiver.data.originIntent";
    private Context context;
    private PushActionUtils actionUtils;

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        this.context = context;
        actionUtils = new PushActionUtils(context);

        String json = intent.getStringExtra("com.parse.Data");
        if (isEmpty(json)) {
            L.d("push data is null");
            return;
        }

        try {
            Push push = Push__JsonHelper.parseFromJson(json);
            if (push != null && push.getType() != null) {
                Intent wrapperIntent = new Intent(ACTION_PUSH_OPEN);   // 发送action为"com.parse.push.intent.OPEN"的广播，从而调用onPushOpen
                wrapperIntent.putExtra(DATA_ORIGIN_INTENT, intent);

                handlePayloadPush(push, wrapperIntent);
            }
        } catch (Exception e) {
            L.e(e);
        }
    }

    /**
     * 接收action为"com.parse.push.intent.OPEN"的broadcast
     *
     * @param context
     * @param intent  action为"com.parse.push.intent.OPEN"，里面包含onPushReceive中的参数intent和要打开的activity的目标intent
     */
    @Override
    protected void onPushOpen(Context context, Intent intent) {
        if (intent == null) {
            L.w("intent should not be null");
            return;
        }
        Intent originIntent = intent.getParcelableExtra(DATA_ORIGIN_INTENT);    // originIntent即为onPushReceive中的Intent参数值
        ParseAnalytics.trackAppOpenedInBackground(originIntent);

        Intent targetIntent = intent.getParcelableExtra(DATA_TARGET_INTENT);
        if (targetIntent == null) {
            return;
        }

        targetIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(targetIntent);
    }

    private void handlePayloadPush(Push push, Intent wrapperIntent) {
        if (push == null || push.getType() == null) {
            L.w("push or push.getType() return null with push: " + push);
            return;
        }

        switch (push.getType()) {
            case offer:
                handleOfferPush(push, wrapperIntent);
                break;
            case system:
                handleActionPush(push, wrapperIntent);
                break;
            default:
                L.w("unknown Push message type: %s", push.getType());
                break;
        }
    }

    private void handleOfferPush(Push push, Intent wrapperIntent) {
        int offerLineId = push.getOfferLineId();
        String msgId = push.getMsgId();

        Intent intent = null;
        if (!MakeOfferActivity.isOfferLineOnTop(offerLineId)) {
            intent = actionUtils.makeOfferIntent(push);
        }

        if (wrapperIntent != null) {
            wrapperIntent.putExtra(DATA_TARGET_INTENT, intent);
        }

        actionUtils.showNotification(push,
                context.getString(R.string.push_title_offer),
                wrapperIntent);

        actionUtils.notifyNewOffer(offerLineId, msgId);
    }

    private void handleActionPush(Push push, Intent wrapperIntent) {
        SystemAction act = push.getActionType();

        if (act == null) {
            L.w("invalid action Push, actionType is null");
            return;
        }

        switch (act) {
            case message:
                handleSysMsgActionPush(push, wrapperIntent);
                break;
            case item:
                handleItemActionPush(push, wrapperIntent);
                break;
            default:
                doHandleActionPush(push, wrapperIntent);
                break;
        }
    }

    private void handleSysMsgActionPush(Push push, Intent wrapperIntent) {
        Intent intent = null;
        if (!MainTabActivity.isSysMsgViewOnTop()) {
            intent = actionUtils.makeIntent(push);
        }

        if (wrapperIntent != null) {
            wrapperIntent.putExtra(DATA_TARGET_INTENT, intent);
        }

        showNotification(push,
                context.getString(R.string.app_name),
                wrapperIntent);

        actionUtils.notifySysMsgRefresh();  // 刷新系统消息列表
    }

    private void handleItemActionPush(Push push, Intent wrapperIntent) {
        String itemId = push.getActionData();
        if (isEmpty(itemId)) {
            L.w("invalid item Push, id is empty");
            return;
        }

        Intent intent = actionUtils.makeIntent(push);

        if (ItemActivity.isItemOnTop(itemId)) {
            // 如果用户正在浏览该商品, 则不再打开新页面
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);  // FIXME single_top not work
        }

        wrapperIntent.putExtra(DATA_TARGET_INTENT, intent);
        showNotification(push,
                context.getString(R.string.app_name),
                wrapperIntent);

        actionUtils.notifySysMsgRefresh();  // 刷新系统消息列表
    }

    // default handler
    private void doHandleActionPush(Push push, Intent wrapperIntent) {
        Intent intent = actionUtils.makeIntent(push);
        wrapperIntent.putExtra(DATA_TARGET_INTENT, intent);
        showNotification(push, context.getString(R.string.app_name), wrapperIntent);
        actionUtils.notifySysMsgRefresh();  // 刷新系统消息列表
    }

    private void showNotification(Push push, String defaultTitle, Intent intent) {
        actionUtils.showNotification(push, defaultTitle, intent, true);
    }
}
