package com.thirdrock.fivemiles.framework.service;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;

import com.BeeFramework.emoji.emojiParser;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.insthub.fivemiles.ConstantS;
import com.insthub.fivemiles.SESSION;
import com.insthub.fivemiles.Utils.LocationManagerUtil;
import com.thirdrock.domain.EnumShareType;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.DeepLinkHelper;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

/**
 * IntentService that allow uploading files to cloud store in parallel.
 * Cloud store service: Cloudinary
 */
public class CloudUploadService extends ParallelIntentService {

    private static final String ACT_UPLOAD_IMG = "CloudUploadService.UploadImage";
    private static final String ACT_UPLOAD_VOICE = "CloudUploadService.UploadVoice";
    private static final String ACT_POST_FB = "CloudUploadService.PostFacebook";
    private static final String ACT_POST_FB_SHOP = "CloudUploadService.PostFacebook.Shop";

    private static final String EXT_FILE = "CloudUploadService.Key.File";
    private static final String EXT_FB_ITEM_ID = "CloudUploadService.Key.FbItemId";
    private static final String EXT_FB_ITEM_TITLE = "CloudUploadService.Key.FbItemTitle";
    private static final String EXT_FB_ITEM_PRICE = "CloudUploadService.Key.FbItemPrice";
    private static final String EXT_FB_ITEM_CURR = "CloudUploadService.Key.FbItemCurrency";

    private static final String EXT_FB_SHOP_ID = "CloudUploadService.Key.FbShopId";
    private static final String EXT_FB_SHOP_NAME = "CloudUploadService.Key.FbShopName";

    public static void startPostToFacebook(Context ctx,
                                           String itemId,
                                           String itemTitle,
                                           String price,
                                           String currency,
                                           ResultReceiver callback) {
        Intent intent = getIntent(ctx,
                CloudUploadService.class,
                ACT_POST_FB,
                callback);
        intent.putExtra(EXT_FB_ITEM_ID, itemId.trim());
        intent.putExtra(EXT_FB_ITEM_TITLE, itemTitle.trim());
        intent.putExtra(EXT_FB_ITEM_PRICE, price.trim());
        intent.putExtra(EXT_FB_ITEM_CURR, currency.trim());
        ctx.startService(intent);
    }

    public static void postMyShopToFacebook(Context ctx, String shopId, String shopName){
        Intent intent = getIntent(ctx, CloudUploadService.class, ACT_POST_FB_SHOP);
        intent.putExtra(EXT_FB_SHOP_ID, shopId);
        intent.putExtra(EXT_FB_SHOP_NAME, shopName);

        ctx.startService(intent);
    }

    @Override
    protected Bundle onExecute(Intent intent) throws Exception {
        String action = intent.getAction();
//        String file = intent.getStringExtra(EXT_FILE);
        Bundle result = null;

//        if (ACT_UPLOAD_IMG.equals(action)) {
//            result = onUploadImage(file);
//        } else if (ACT_UPLOAD_VOICE.equals(action)) {
//            result = onUploadVoice(file);
//        } else if (ACT_POST_FB.equals(action)) {
        if (ACT_POST_FB.equals(action)) {
            String itemId = intent.getStringExtra(EXT_FB_ITEM_ID);
            String itemTitle = intent.getStringExtra(EXT_FB_ITEM_TITLE);
            String price = intent.getStringExtra(EXT_FB_ITEM_PRICE);
            String currency = intent.getStringExtra(EXT_FB_ITEM_CURR);
            publishToFacebook(itemId, itemTitle, price, currency);
        }

        if (ACT_POST_FB_SHOP.equals(action)){
            String shopId = intent.getStringExtra(EXT_FB_SHOP_ID);
            String shopName = intent.getStringExtra(EXT_FB_SHOP_NAME);
            publishShopToFacebook(shopId, shopName);
        }

        return result;
    }

    private void publishShopToFacebook(String shopId, String shopName) {
        // 下个版本做成后台分享，这里无需优化了
        Session fbSession = Session.getActiveSession();
        if (!(fbSession != null && fbSession.isOpened())) {     // isClosed == false 并不意味着 isOpened == true
            L.d("Facebook fbSession is not open");
            return;
        }

//        Looper.prepare();      // Branch需要
//
//        String userId = SESSION.getInstance().user_id;
//        String eventId = shopId;
//        DeepLinkHelper deepLinkHelper = new DeepLinkHelper(getApplicationContext());
//        String branchAliasUrl = deepLinkHelper.generateBranchShortUrl(userId, EnumShareType.SHOP, eventId, new DeepLinkHelper.OnDeepLinkCreateListener() {
//            @Override
//            public void onLinkCreateEvent(boolean isSuccess, String url, String errorMsg) {
//                if (isSuccess){
//                    L.i("branch link is created successfully and the url is " + url);
//                }else{
//                    L.e("branch link creation failed and error message is " + errorMsg);
//                }
//            }
//        });

        String post = getFacebookShopPost(shopName);
        if (post == null)   return;

        String userId = SESSION.getInstance().userId;
        String shopUrl = ConstantS.DESKTOP_HOME_URL + "/person/" + userId;

        Bundle postParams = new Bundle();
        postParams.putString("message", post);
        postParams.putString("link", shopUrl);

        Response resp = new Request(fbSession, "me/feed", postParams, HttpMethod.POST).executeAndWait();  // 同步操作

        Handler mainHandler = new Handler(getMainLooper());
        FacebookRequestError error = resp.getError();
        if (error != null) {
            mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    DisplayUtils.bottomToastLong(getApplicationContext(), R.string.share_store_success);
                }
            });

            L.e("Facebook api error: [%s] %s", error.getErrorCode(), error.getErrorMessage());
            return;
        }else {
            mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    DisplayUtils.bottomToastLong(getApplicationContext(), R.string.share_store_success);
                }
            });

        }

        GraphObject graphObj = resp.getGraphObject();
        try {
            if (graphObj != null) {
                JSONObject graphJo = graphObj.getInnerJSONObject();
                String postId = graphJo.getString("id");
                L.d("Facebook post success, post_id: " + postId);
            }
        } catch (JSONException e) {
            L.e(e);
        }
    }

    /**
     * 分享单品到Facebook
     */
    private void publishToFacebook(String itemId, String itemTitle, String price, String currency) {
        Session session = Session.getActiveSession();
        if (!(session != null && session.isOpened())) {
            return;
        }

        Looper.prepare();   // Branch需要

        DeepLinkHelper deepLinkHelper = new DeepLinkHelper(getApplicationContext());
        String userId = SESSION.getInstance().userId;
        String branchAliasUrl = deepLinkHelper.generateBranchShortUrl(userId, EnumShareType.ITEM, itemId, null);

        String post = getFacebookPost(itemId, itemTitle, price, currency, branchAliasUrl);
        if (post == null) {
            return;
        }

        Bundle postParams = new Bundle();
        postParams.putString("message", post);
        postParams.putString("link", branchAliasUrl);

        Response resp = new Request(session, "me/feed", postParams, HttpMethod.POST)
                .executeAndWait();

        FacebookRequestError error = resp.getError();
        if (error != null) {
            L.e("Facebook api error: [%s] %s",
                    error.getErrorCode(),
                    error.getErrorMessage());
            return;
        }

        GraphObject graphObj = resp.getGraphObject();
        JSONObject graphJo;
        String postId;
        try {
            if (graphObj != null) {
                graphJo = graphObj.getInnerJSONObject();
                postId = graphJo.getString("id");
                L.d("Facebook post success, post_id: " + postId);
            }
        } catch (JSONException e) {
            L.e(e);
        }
    }

    private String getFacebookPost(String itemId, String itemTitle, String price, String currency, String url) {
        String post = null;
        String city = null;
        Context ctx = getApplicationContext();
        Geocoder geocoder = new Geocoder(ctx, Locale.getDefault());

        try {
            List<Address> addresses = geocoder.getFromLocation(LocationManagerUtil.getLatitudeInSession(),
                    LocationManagerUtil.getLongitudeInSession(), 1);
            if (addresses.size() > 0) {
                Address addr = addresses.get(0);
                city = addr.getLocality();
            }

            city = ModelUtils.isNotEmpty(city) ? city : "my city";
            post = ctx.getString(R.string.share_item,
                        city,
                        emojiParser.emojiText(itemTitle),
                        currency + price,
                        url);
        } catch (Exception e) {
            L.e(e);
        }

        return post;
    }

    private String getFacebookShopPost(String pShopName){
        String post = getApplicationContext().getString(R.string.share_my_shop, pShopName);

        return post;
    }

}
