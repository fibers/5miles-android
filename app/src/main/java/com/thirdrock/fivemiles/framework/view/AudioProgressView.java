package com.thirdrock.fivemiles.framework.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import com.thirdrock.fivemiles.util.DisplayUtils;

/**
 * Created by ywu on 14/11/24.
 */
public class AudioProgressView extends View {

    private static final int BG_LINE_COLOR = Color.parseColor("#cccccc");
    private static final int FG_LINE_START_COLOR = Color.parseColor("#ffd92a");
    private static final int FG_LINE_END_COLOR = Color.parseColor("#ff8830");
    private static final int HANDLE_COLOR = FG_LINE_END_COLOR;
    private static final int LINE_WIDTH_DP = 2;
    private static final int HANDLE_SIZE_DP = 18;

    private int handleSize;
    private float lineWidth, handleRadius;
    private Paint paint;
    private float progress = 0;

    public AudioProgressView(Context context) {
        super(context);
        init();
    }

    public AudioProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AudioProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        paint = new Paint();
        lineWidth = DisplayUtils.toPixels(getContext(), LINE_WIDTH_DP);
        handleSize = DisplayUtils.toPixels(getContext(), HANDLE_SIZE_DP);
        handleRadius = handleSize / 2f;
    }

    /**
     * Set progress and refresh the view
     * @param progress 0 ~ 100
     */
    public void setProgress(float progress) {
        if (progress < 0) {
            this.progress = 0;
        } else if (progress > 100) {
            this.progress = 100;
        } else {
            this.progress = progress;
        }

        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int hSpec = MeasureSpec.makeMeasureSpec(handleSize + 2, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, hSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int totalWidth = getWidth();
        int totalHeight = getHeight();
        float cY = totalHeight / 2f;  // vertical center y
        float fgWidth = totalWidth - handleRadius * 2;
        float endX = handleRadius + fgWidth * progress / 100;  // where progress ends

        // background line
        paint.reset();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(BG_LINE_COLOR);
        paint.setStrokeWidth(lineWidth);
        canvas.drawLine(0, cY, totalWidth, cY, paint);

        // foreground line indicating the progress
        paint.setShader(new LinearGradient(0, cY, endX, cY, FG_LINE_START_COLOR, FG_LINE_END_COLOR, Shader.TileMode.CLAMP));
        canvas.drawLine(0, cY, endX, cY, paint);

        // handle on the end of the progress
        paint.reset();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(HANDLE_COLOR);
        canvas.drawCircle(endX, cY, handleRadius, paint);
    }
}
