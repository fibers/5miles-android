package com.thirdrock.fivemiles.framework.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.ui.renderer.IndexableArrayAdapter;

import java.io.Serializable;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELECTION_DATA_LIST;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELECTION_SCREEN_NAME;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELECTION_TITLE;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELECTION_USE_FAST_SCROLL;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_RESULT_SELECTION_INDEX;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_RESULT_SELECTION_ITEM;

/**
 * Generic selection activity, for selecting item from a list.
 * Created by ywu on 14/11/18.
 */
public class SelectionListActivity extends Activity {
    public static final String VIEW_NAME = "deliveryselect_view";

    @InjectView(R.id.top_view_title)
    TextView title;

    @InjectView(R.id.list_view)
    ListView listView;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection_list);
        ButterKnife.inject(this);

        boolean fastScrollEnabled = getIntent().getBooleanExtra(ACT_PARA_SELECTION_USE_FAST_SCROLL, false);

        ArrayAdapter adapter;
        if (fastScrollEnabled) {
            adapter = new IndexableArrayAdapter(this, R.layout.common_text_list_item, R.id.common_list_item_text);
            listView.setFastScrollEnabled(true);
        } else {
            adapter = new ArrayAdapter(this, R.layout.common_text_list_item, R.id.common_list_item_text);
        }
        listView.setAdapter(adapter);

        List data = (List) getIntent().getSerializableExtra(ACT_PARA_SELECTION_DATA_LIST);
        adapter.addAll(data);

        title.setText(getIntent().getStringExtra(ACT_PARA_SELECTION_TITLE));

        if (getIntent().hasExtra(ACT_PARA_SELECTION_SCREEN_NAME)) {
            TrackingUtils.trackView(getIntent().getStringExtra(ACT_PARA_SELECTION_SCREEN_NAME));
        }

        TrackingUtils.trackView(VIEW_NAME);
    }

    @OnItemClick(R.id.list_view)
    @SuppressWarnings("unused")
    void onItemClicked(int position) {
        Intent intent = new Intent();
        intent.putExtra(ACT_RESULT_SELECTION_INDEX, position);
        intent.putExtra(ACT_RESULT_SELECTION_ITEM, (Serializable) listView.getAdapter().getItem(position));
        setResult(Activity.RESULT_OK, intent);
        finish();

        TrackingUtils.trackTouch(VIEW_NAME, "delivery_change");
    }

    @OnClick(R.id.top_view_back)
    @SuppressWarnings("unused")
    void onNaviBack() {
        finish();
        TrackingUtils.trackTouch(VIEW_NAME, "delivery_back");
    }
}
