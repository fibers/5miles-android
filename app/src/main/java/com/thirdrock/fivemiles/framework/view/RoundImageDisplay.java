package com.thirdrock.fivemiles.framework.view;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;

/**
 * Created by ywu on 14-8-9.
 */
public class RoundImageDisplay implements BitmapDisplayer {

    protected final int topLeftRadius, topRightRadius, bottomRightRadius, bottomLeftRadius;
    protected final int margin;

    /**
     * Constructing rounded corner image display
     *
     * @param topLeftRadius     radius of top left corner, in pixels
     * @param topRightRadius    radius of top left corner, in pixels
     * @param bottomRightRadius radius of top left corner, in pixels
     * @param bottomLeftRadius  radius of top left corner, in pixels
     * @param margin            margin in pixels
     */
    public RoundImageDisplay(int topLeftRadius,
                             int topRightRadius,
                             int bottomRightRadius,
                             int bottomLeftRadius,
                             int margin) {
        this.topLeftRadius = topLeftRadius > 0 ? topLeftRadius : 0;
        this.topRightRadius = topRightRadius > 0 ? topRightRadius : 0;
        this.bottomRightRadius = bottomRightRadius > 0 ? bottomRightRadius : 0;
        this.bottomLeftRadius = bottomLeftRadius > 0 ? bottomLeftRadius : 0;
        this.margin = margin > 0 ? margin : 0;
    }

    public RoundImageDisplay(int topLeftRadius,
                             int topRightRadius,
                             int bottomRightRadius,
                             int bottomLeftRadius) {
        this(topLeftRadius, topRightRadius, bottomRightRadius, bottomLeftRadius, 0);
    }

    @Override
    public void display(Bitmap bitmap, ImageAware imageAware, LoadedFrom loadedFrom) {
        imageAware.setImageDrawable(new RoundedDrawable(bitmap,
                topLeftRadius, topRightRadius, bottomRightRadius, bottomLeftRadius, margin));
    }

    private static class RoundedDrawable extends Drawable {

        protected final int topLeftRadius, topRightRadius, bottomRightRadius, bottomLeftRadius;

        protected final int margin;

        protected final RectF mRect = new RectF(), mBitmapRect;
        protected final BitmapShader bitmapShader;
        protected final Paint paint;

        public RoundedDrawable(Bitmap bitmap,
                               int topLeftRadius,
                               int topRightRadius,
                               int bottomRightRadius,
                               int bottomLeftRadius,
                               int margin) {
            this.topLeftRadius = topLeftRadius;
            this.topRightRadius = topRightRadius;
            this.bottomRightRadius = bottomRightRadius;
            this.bottomLeftRadius = bottomLeftRadius;
            this.margin = margin;

            bitmapShader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            mBitmapRect = new RectF(margin, margin, bitmap.getWidth() - margin, bitmap.getHeight() - margin);

            paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setShader(bitmapShader);
        }

        @Override
        protected void onBoundsChange(Rect bounds) {
            super.onBoundsChange(bounds);
            mRect.set(margin, margin, bounds.width() - margin, bounds.height() - margin);

            // Resize the original bitmap to fit the new bound
            Matrix shaderMatrix = new Matrix();
            shaderMatrix.setRectToRect(mBitmapRect, mRect, Matrix.ScaleToFit.FILL);
            bitmapShader.setLocalMatrix(shaderMatrix);

        }

        @Override
        public void draw(Canvas canvas) {
            float[] radii = {0, 0, 0, 0, 0, 0, 0, 0};

            if (topLeftRadius > 0) {
                radii[0] = radii[1] = topLeftRadius;
            }

            if (topRightRadius > 0) {
                radii[2] = radii[3] = topRightRadius;
            }

            if (bottomRightRadius > 0) {
                radii[4] = radii[5] = bottomRightRadius;
            }

            if (bottomLeftRadius > 0) {
                radii[6] = radii[7] = bottomLeftRadius;
            }

            Path path = new Path();
            path.addRoundRect(mRect, radii, Path.Direction.CCW);
            canvas.drawPath(path, paint);
        }

        @Override
        public int getOpacity() {
            return PixelFormat.TRANSLUCENT;
        }

        @Override
        public void setAlpha(int alpha) {
            paint.setAlpha(alpha);
        }

        @Override
        public void setColorFilter(ColorFilter cf) {
            paint.setColorFilter(cf);
        }
    }
}

