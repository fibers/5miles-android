package com.thirdrock.fivemiles.framework.view;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Filter for input range [min, max]
 * Created by ywu on 14-10-11.
 */
public class NumberInputFilter implements InputFilter {

    private double min = Double.MIN_VALUE, max = Double.MAX_VALUE;

    /**
     * Filter for input range [min, max]
     * @param min inclusive
     * @param max inclusive
     */
    public NumberInputFilter(double min, double max) {
        this.min = Math.min(min, max);
        this.max = Math.max(min, max);
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            // Remove the string out of destination that is to be replaced
            String newVal = dest.toString().substring(0, dstart) +
                    dest.toString().substring(dend, dest.toString().length());

            // Add the new string in
            newVal = newVal.substring(0, dstart) + source.toString() + newVal.substring(dstart, newVal.length());

            int dotIndex = newVal.lastIndexOf('.');
            if (dotIndex >= 0) {
                int fractionLen = newVal.substring(dotIndex + 1).length();
                if (fractionLen > 2) {
                    return "";
                }
            }

            double input = Double.parseDouble(newVal);
            if (isInRange(input)) {
                return null;  // accept the original text
            }
        }
        catch (NumberFormatException nfe) {
            // ignore
        }
        return "";
    }

    private boolean isInRange(double input) {
        return input >= min && input <= max;
    }
}
