package com.thirdrock.fivemiles.framework.view;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thirdrock.fivemiles.R;

/**
 * Created by KunPeng on 2014/8/27.
 */
public class BottomConfirmDialog extends PopupWindow {
    public TextView confirm,cancel;
    private View container;
    private View.OnClickListener mOnClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v.getId()==R.id.confirm){
                if(mOnConfirmClickListner!=null){
                    mOnConfirmClickListner.onConfirmClick();
                }
            }else if(v.getId()==R.id.cancel){
                if(mOnConfirmClickListner!=null){
                    mOnConfirmClickListner.onCancelClick();
                }
            }
        }
    };

    public BottomConfirmDialog(Context context) {
        super(context);
        LayoutInflater mInflater= LayoutInflater.from(context);
       View view= mInflater.inflate(R.layout.bottom_dialog,null);
        confirm= (TextView) view.findViewById(R.id.confirm);
        cancel= (TextView) view.findViewById(R.id.cancel);
        setContentView(view);
        setFocusable(true);
        setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.fm_mask_bg)));
        setOutsideTouchable(true);
        setWidth(RelativeLayout.LayoutParams.MATCH_PARENT);
        setHeight(RelativeLayout.LayoutParams.MATCH_PARENT);
        confirm.setOnClickListener(mOnClickListener);
        cancel.setOnClickListener(mOnClickListener);
        container=view.findViewById(R.id.container);
        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomConfirmDialog.this.dismiss();
            }
        });
    }
    public void show(View parentView,int gravity){
        this.showAtLocation(parentView,gravity,0,0);
    }

    public void setTopText(int resId){
        confirm.setText(resId);
    }
    public void setBottomText(int resId){
        cancel.setText(resId);
    }

    public OnConfirmClickListner getOnConfirmClickListner() {
        return mOnConfirmClickListner;
    }

    public void setOnConfirmClickListner(OnConfirmClickListner mOnConfirmClickListner) {
        this.mOnConfirmClickListner = mOnConfirmClickListner;
    }

    private OnConfirmClickListner mOnConfirmClickListner;
    public interface OnConfirmClickListner{
       public void onConfirmClick();
       public void onCancelClick();
    }



}
