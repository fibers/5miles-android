package com.thirdrock.fivemiles.framework.fragment;

/*
 * Copyright (C) 2014 Pedro Vicente Gómez Sánchez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.BeeFramework.view.MyProgressDialog;
import com.insthub.fivemiles.Activity.SignOutAction;
import com.insthub.fivemiles.FiveMilesApp;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.activity.AbsActivity;
import com.thirdrock.fivemiles.framework.activity.AbsFragmentActivity;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.framework.ui.viewmodel.ModelObserver;

import butterknife.ButterKnife;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REF;

/**
 * 参考了AbsActivity和Pedro Vicente Gómez Sánchez的BaseFragment。
 * Base fragment created to be extended by every fragment in this application. This class provides
 * dependency injection configuration, ButterKnife Android library configuration and some methods
 * common to every fragment.
 *
 * @author Ju Bin
 */
public abstract class AbsFragment<M extends AbsViewModel<E>, E extends Enum>
        extends Fragment implements ModelObserver<E> {

    private SignOutAction signOutAction;
    private boolean viewModelBound;
    private MyProgressDialog mainProgress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doOnCreate(savedInstanceState);
        signOutAction = new SignOutAction(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(getFragmentLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injectViews(view);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        injectDependencies();
    }

    @Override
    public void onStart() {
        super.onStart();
        bindViewModel();
    }

    @Override
    public void onStop() {
        super.onStop();
        unbindViewModel();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        FiveMilesApp.getRefWatcher().watch(this);
    }

    /**
     * Override #doOnActivityResult instead.
     */
    @Override
    public final void onActivityResult(int requestCode, int resultCode, Intent data) {
        bindViewModel();
        doOnActivityResult(requestCode, resultCode, data);
    }

    /**
     * do your onActivityResult stuff here.
     */
    protected void doOnActivityResult(int requestCode, int resultCode, Intent data) {
    }

    /**
     * Every fragment has to inflate a layout in the onCreateView method. We have added this method to
     * avoid duplicate all the inflate code in every fragment. You only have to return the layout to
     * inflate in this method when extends BaseFragment.
     */
    protected abstract int getFragmentLayout();

    /**
     * Replace every field annotated using @Inject annotation with the provided dependency specified
     * inside a Dagger module value.
     */
    private void injectDependencies() {
        AbsFragmentActivity hostActivity = (AbsFragmentActivity)getActivity();
        if (hostActivity != null) {
            hostActivity.inject(this);
        }
    }

    /**
     * Replace every field annotated with ButterKnife annotations like @InjectView with the proper
     * value.
     *
     * @param view to extract each widget injected in the fragment.
     */
    private void injectViews(final View view) {
        ButterKnife.inject(this, view);
    }

    /**
     * Provide screen name of this activity, for tracking
     */
    protected abstract String getScreenName();

    /**
     * Do your onCreate stuff here
     */
    protected void doOnCreate(Bundle savedInstanceState) {
        // empty
    }

    @Override
    public void onError(E property, int errResId) {
        DisplayUtils.toast(errResId);
    }

    @Override
    public void onError(E property, Throwable e) {
        AbsActivity.handleError(getActivity(), e, signOutAction);
    }

    @Override
    public void onMinorJobError(E property, Throwable e) {
    }

    @Override
    public void onMajorJobStarted() {
        showMainProgress(getMainProgressMessageResource());
    }

    @Override
    public void onMajorJobCompleted() {
        stopMainProgress();
    }

    protected int getMainProgressMessageResource() {
        return R.string.msg_loading;
    }

    protected void showMainProgress(String text) {
        if (mainProgress != null && mainProgress.isShowing()) {
            return;
        }

        mainProgress = new MyProgressDialog(getActivity(), text);
        mainProgress.show();
    }

    protected void showMainProgress(int msgResId) {
        showMainProgress(getString(msgResId));
    }

    protected void stopMainProgress() {
        if (mainProgress == null) {
            return;
        }

        mainProgress.dismiss();
        mainProgress = null;
    }

    @Override
    public void onJobStarted(E property, Object data) {

    }

    @Override
    public void onJobCompleted(E property, Object data) {

    }

    /**
     * Provide the instance of ViewModel for this activity
     */
    protected abstract M getViewModel();

    protected void bindViewModel() {
        if (!viewModelBound) {
            getViewModel().subscribe(this);
            getViewModel().setContext(this);
            viewModelBound = true;
        }
    }

    protected void unbindViewModel() {
        if (viewModelBound) {
            getViewModel().unsubscribe(this);
            viewModelBound = false;
        }
    }

    protected void trackTouch(String action) {
        TrackingUtils.trackTouch(getScreenName(), action);
    }

    @Override
    public void startActivity(Intent intent) {
        intent.putExtra(ACT_PARA_REF, getScreenName());
        super.startActivity(intent);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra(ACT_PARA_REF, getScreenName());
        super.startActivityForResult(intent, requestCode);
    }
}