package com.thirdrock.fivemiles.framework.view;

import android.content.Context;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.thirdrock.framework.util.L;

/**
 * ImageView to display top-crop scale of an image view.
 *
 * @author Chris Arriola
 * TODO 支持更多的裁剪方式，不仅限于裁剪底部
 */
public class TopCropImageView extends ImageView {

    public TopCropImageView(Context context) {
        super(context);
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    public TopCropImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        final Matrix matrix = getImageMatrix();

        float scale;
        final int viewWidth = getMeasuredWidth();
        final int viewHeight = getMeasuredHeight();
        final int drawableWidth = getDrawable().getIntrinsicWidth();
        final int drawableHeight = getDrawable().getIntrinsicHeight();

        if (drawableWidth * viewHeight > drawableHeight * viewWidth) {
            scale = (float) viewHeight / (float) drawableHeight;
        } else {
            scale = (float) viewWidth / (float) drawableWidth;
        }

//        scale = (float) viewWidth / (float) drawableWidth;
        if (scale > 0) {
            matrix.setScale(scale, scale);
            L.d("layout ImageView vw: %s, dw: %s, scale: %s", viewWidth, drawableWidth, scale);
            setImageMatrix(matrix);
        }
        super.onLayout(changed, left, top, right, bottom);
    }
}