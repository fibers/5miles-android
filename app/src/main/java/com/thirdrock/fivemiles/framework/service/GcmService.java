package com.thirdrock.fivemiles.framework.service;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;
import com.insthub.fivemiles.Activity.MainTabActivity;
import com.insthub.fivemiles.Activity.MakeOfferActivity;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.PushActionUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.Push;
import com.thirdrock.protocol.Push__JsonHelper;
import com.thirdrock.protocol.SystemAction;

import java.io.IOException;

/**
 * 参考PushReceiver
 * TODO 将处理Push的逻辑放在一个类中，最好是个intentservice，可以参考gcm的demo。
 */
public class GcmService extends GcmListenerService {
    private PushActionUtils actionUtils;

    public GcmService() {
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        actionUtils = new PushActionUtils(GcmService.this);

        String jsonMsg = data.getString("message");
        L.d("From: " + from);
        L.d("Message: " + jsonMsg);

        if (ModelUtils.isNotEmpty(jsonMsg)) {
            try {
                Push push = Push__JsonHelper.parseFromJson(jsonMsg);
                if (push != null) {     // json为{}的时候，push会成null
                    handlePayloadPush(push);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void handlePayloadPush(Push push) {
        switch (push.getType()) {
            case offer:
                handleOfferPush(push);
                break;
            case system:
                handleActionPush(push);
                break;
            default:
                L.w("unknown Push message type: %s", push.getType());
                break;
        }
    }

    private void handleOfferPush(Push push) {
        int offerLineId = push.getOfferLineId();
        String msgId = push.getMsgId();

        Intent intent = null;
        if (!MakeOfferActivity.isOfferLineOnTop(offerLineId)) {
            intent = actionUtils.makeOfferIntent(push);
        }
        showNotification(push,
                GcmService.this.getString(R.string.push_title_offer),
                intent);

        actionUtils.notifyNewOffer(offerLineId, msgId);
    }

    private void handleActionPush(Push push) {
        SystemAction act = push.getActionType();

        if (act == null) {
            L.w("invalid action Push, actionType is null");
            return;
        }

        switch (act) {
            case message:
                handleSysMsgActionPush(push);
                break;
            case item:
                handleItemActionPush(push);
                break;
            default:
                doHandleActionPush(push);
                break;
        }
    }

    private void showNotification(Push push, String defaultTitle, Intent intent) {
        actionUtils.showNotification(push, defaultTitle, intent);
    }

    private void handleSysMsgActionPush(Push push) {
        Intent intent = null;
        if (!MainTabActivity.isSysMsgViewOnTop()) {
            intent = actionUtils.makeIntent(push);
        }
        showNotification(push,
                GcmService.this.getString(R.string.app_name),
                intent);

        actionUtils.notifySysMsgRefresh();  // 刷新系统消息列表
    }

    private void handleItemActionPush(Push push) {
        String itemId = push.getActionData();
        if (ModelUtils.isEmpty(itemId)) {
            L.w("invalid item Push, id is empty");
            return;
        }

        Intent intent = actionUtils.makeIntent(push);

        if (ItemActivity.isItemOnTop(itemId)) {
            // 如果用户正在浏览该商品, 则不再打开新页面
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);  // FIXME single_top not work
        }

        showNotification(push,
                GcmService.this.getString(R.string.app_name),
                intent);

        actionUtils.notifySysMsgRefresh();  // 刷新系统消息列表
    }

    // default handler
    private void doHandleActionPush(Push push) {
        Intent intent = actionUtils.makeIntent(push);
        showNotification(push, GcmService.this.getString(R.string.app_name), intent);
        actionUtils.notifySysMsgRefresh();  // 刷新系统消息列表
    }
}
