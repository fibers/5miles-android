package com.thirdrock.fivemiles.framework.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.BeeFramework.view.MyProgressDialog;
import com.external.androidquery.callback.AbstractAjaxCallback;
import com.insthub.fivemiles.Activity.SignOutAction;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.di.ActivityModule;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.framework.ui.viewmodel.ModelObserver;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.session.Action;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import butterknife.ButterKnife;
import dagger.ObjectGraph;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REF;
/**
 * Super class for all fragment activities of the app.
 * Created by ywu on 15/1/30.
 */
public abstract class AbsFragmentActivity<M extends AbsViewModel<E>, E extends Enum>
        extends AppCompatActivity implements ModelObserver<E> {

    private SignOutAction signOutAction;

    private ObjectGraph activityScopeGraph;
    private List<Object> activityScopeModules = new LinkedList<>();
    private boolean viewModelBound;

    private MyProgressDialog mainProgress;

    /**
     * Override #doOnCreate instead.
     */
    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        injectDependencies();
        injectViews();
        doOnCreate(savedInstanceState);
        signOutAction = new SignOutAction(this);

        ensureAuth(getIntent());
        TrackingUtils.trackView(getScreenName());
    }

    @Override
    protected final void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ensureAuth(intent);
    }

    private void ensureAuth(Intent currentIntent) {
        boolean loginProtected = isLoginProtected();
        if (loginProtected && !SESSION.getInstance().isAuth()) {
            SignOutAction.redirectToLogin(this, currentIntent);
            finish();
        }
    }

    /**
     * is this fragment login protected, default is true
     * @return whether login is required
     */
    protected boolean isLoginProtected() {
        return true;
    }

    /**
     * Provide screen name of this activity, for tracking
     */
    protected abstract String getScreenName();

    /**
     * Provide the resource id of the content view
     */
    protected abstract int getContentView();

    /**
     * Do your onCreate stuff here
     */
    protected void doOnCreate(Bundle savedInstanceState) {
    }

    /**
     * Do your onNewIntent stuff here
     */
    @SuppressWarnings("unused")
    protected void doOnNewIntent(Intent intent) {
    }

    /**
     * Method used to resolve dependencies provided by Dagger modules. Inject an object to provide
     * every @Inject annotation contained.
     *
     * @param object to inject.
     */
    public void inject(Object object) {
        if (activityScopeGraph != null) {
            activityScopeGraph.inject(object);
        } else {
            L.e("activityScopeGraph should not be null when inject object");
        }
    }

    /**
     * Prepare the list of Dagger activityScopeModules with Activity scope needed to this Activity.
     * by calling #addModules method
     */
    protected void prepareModules() {
    }

    /**
     * Add all DI modules of this Activity scope.
     *
     * @param modules modules
     */
    @SuppressWarnings("unused")
    protected final void addModules(Object... modules) {
        Collections.addAll(activityScopeModules, modules);
    }

    /**
     * Create a new Dagger ObjectGraph to add new dependencies using a plus operation and inject the
     * declared one in the activity. This new graph will be destroyed once the activity lifecycle
     * finish.
     * <p/>
     * This is the key of how to use Activity scope dependency injection.
     */
    private void injectDependencies() {
        prepareModules();

        FiveMilesApp tvShowsApplication = (FiveMilesApp) getApplication();
        activityScopeModules.add(new ActivityModule(this));
        activityScopeGraph = tvShowsApplication.plus(activityScopeModules);

        inject(this);
    }

    /**
     * Replace every field annotated with ButterKnife annotations like @InjectView with the proper
     * value.
     */
    private void injectViews() {
        ButterKnife.inject(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindViewModel();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindViewModel();
        AbstractAjaxCallback.cancel();  // cancel all running & pending (BeeFramework) ajax jobs
    }

    /**
     * Override #doOnActivityResult instead.
     */
    @Override
    protected final void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        bindViewModel();
        doOnActivityResult(requestCode, resultCode, data);
    }

    /**
     * do your onActivityResult stuff here.
     */
    protected void doOnActivityResult(int requestCode, int resultCode, Intent data) {
    }

    @Override
    public void onError(E property, int errResId) {
        DisplayUtils.toast(errResId);
    }

    @Override
    public void onError(E property, Throwable e) {
        AbsActivity.handleError(this, e, signOutAction);
    }

    @Override
    public void onMinorJobError(E property, Throwable e) {
    }

    @Override
    public void onMajorJobStarted() {
        showMainProgress(getMainProgressMessageResource());
    }

    @Override
    public void onMajorJobCompleted() {
        stopMainProgress();
    }

    protected int getMainProgressMessageResource() {
        return R.string.msg_loading;
    }

    protected void showMainProgress(String text) {
        if (mainProgress != null && mainProgress.isShowing()) {
            return;
        }

        mainProgress = new MyProgressDialog(this, text);
        mainProgress.show();
    }

    protected void showMainProgress(int msgResId) {
        showMainProgress(getString(msgResId));
    }

    protected void stopMainProgress() {
        if (mainProgress == null) {
            return;
        }

        mainProgress.dismiss();
        mainProgress = null;
    }

    @Override
    public void onJobStarted(E property, Object data) {

    }

    @Override
    public void onJobCompleted(E property, Object data) {

    }

    /**
     * Provide the instance of ViewModel for this activity
     */
    protected abstract M getViewModel();

    protected void bindViewModel() {
        if (!viewModelBound) {
            getViewModel().subscribe(this);
            getViewModel().setContext(this);
            viewModelBound = true;
        }
    }

    protected void unbindViewModel() {
        if (viewModelBound) {
            getViewModel().unsubscribe(this);
            viewModelBound = false;
        }
    }

    protected void trackTouch(String action) {
        TrackingUtils.trackTouch(getScreenName(), action);
    }

    protected void trackTouch(String pViewName, String pAction) {
        TrackingUtils.trackTouch(pViewName, pAction);
    }

    @Override
    public void startActivity(Intent intent) {
        intent.putExtra(ACT_PARA_REF, getScreenName());
        super.startActivity(intent);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra(ACT_PARA_REF, getScreenName());
        super.startActivityForResult(intent, requestCode);
    }

    @SuppressWarnings("unused")
    protected String getReferralScreen() {
        return getIntent().getStringExtra(ACT_PARA_REF);
    }

    @SuppressWarnings("unused")
    protected void queuePendingAction(String key, Action action) {
        queuePendingAction(key, action, false);
    }

    protected void queuePendingAction(String key, Action action, boolean persist) {
        SESSION.getInstance().queuePendingAction(key, action);
        if (persist) {
            SESSION.getInstance().save();
        }
    }
}
