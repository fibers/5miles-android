package com.thirdrock.fivemiles.framework.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thirdrock.fivemiles.R;

/**
 * Created by KunPeng on 2014/8/27.
 */
public class RecordingDecibelPopupWindow extends PopupWindow {
    private ImageView icDecibel;
    public TextView timeRemainingTextView;

    public RecordingDecibelPopupWindow(Context context) {
        super(context);
        LayoutInflater mInflater = LayoutInflater.from(context);
        View view = mInflater.inflate(R.layout.recording_decibel_popupwindow, null);
        icDecibel = (ImageView) view.findViewById(R.id.record_icon);
        timeRemainingTextView = (TextView) view.findViewById(R.id.record_remaingtime);
        setContentView(view);
        setFocusable(false);
        setWidth(RelativeLayout.LayoutParams.WRAP_CONTENT);
        setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
    }

    public void updateDecibelView(double decibel) {
        if (0 < decibel && decibel <= 20) {
            icDecibel.setImageResource(R.drawable.ic_decibel_1);
        } else if (20 < decibel && decibel <= 30) {
            icDecibel.setImageResource(R.drawable.ic_decibel_2);
        } else if (30 < decibel && decibel <= 40) {
            icDecibel.setImageResource(R.drawable.ic_decibel_3);
        } else if (40 < decibel && decibel <= 45) {
            icDecibel.setImageResource(R.drawable.ic_decibel_4);
        } else if (45 < decibel && decibel <= 50) {
            icDecibel.setImageResource(R.drawable.ic_decibel_5);
        } else if (50 < decibel && decibel <= 60) {
            icDecibel.setImageResource(R.drawable.ic_decibel_6);
        } else if (60 < decibel && decibel <= 70) {
            icDecibel.setImageResource(R.drawable.ic_decibel_7);
        } else if (80 < decibel) {
            icDecibel.setImageResource(R.drawable.ic_decibel_8);
        }
    }

}
