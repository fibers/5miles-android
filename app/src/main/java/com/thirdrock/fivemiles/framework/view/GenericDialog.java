package com.thirdrock.fivemiles.framework.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;

import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;

/**
 * Generic purpose dialog, accept custom content layout
 * Created by ywu on 14-10-14.
 */
public class GenericDialog {

    public interface ViewController {
        void onInit(GenericDialog dialog, View contentView);
    }

    private Dialog dialog;

    public GenericDialog(Context context, int contentViewRid) {
        this(context, R.style.dialog, contentViewRid);
    }

    public GenericDialog(Context context, int contentViewRid, ViewController controller) {
        this(context, R.style.dialog, contentViewRid, controller);
    }

    public GenericDialog(Context context, int dialogStyleRid, int contentViewRid) {
        this(context, dialogStyleRid, contentViewRid, null);
    }

    public GenericDialog(Context context, int dialogStyleRid, int contentViewRid, ViewController controller) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(contentViewRid, null);
        view.setClickable(true);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        dialog = new Dialog(context, dialogStyleRid);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(true);

        if (controller != null) {
            controller.onInit(this, view);
        }
        onViewInit(context, view);
    }

    protected Dialog getDialog() {
        return dialog;
    }

    protected void onViewInit(Context context, View contentView) {
    }

    public View findViewById(int rid) {
        return dialog.findViewById(rid);
    }

    public boolean isShowing() {
        return dialog != null && dialog.isShowing();
    }

    public GenericDialog show() {
        try {
            dialog.show();
        } catch (Exception e) {
            L.e(e);
        }

        return this;
    }

    public GenericDialog setCanceledOnTouchOutside(boolean cancel) {
        dialog.setCanceledOnTouchOutside(cancel);
        return this;
    }

    public GenericDialog setCancelable(boolean flag) {
        dialog.setCancelable(flag);
        return this;
    }

    public GenericDialog setOnDismissListener(DialogInterface.OnDismissListener listener) {
        dialog.setOnDismissListener(listener);
        return this;
    }

    public GenericDialog setOnCancelListener(DialogInterface.OnCancelListener listener) {
        dialog.setOnCancelListener(listener);
        return this;
    }

    public void dismiss() {
        if (isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                L.e(e);
            }
        }
    }
}
