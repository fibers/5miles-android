package com.thirdrock.fivemiles.framework.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.thirdrock.fivemiles.R;

/**
 * Created by jubin on 6/2/15.
 */
public class SimpleDialog {
    private Context mContext;
    private TextView mTitle, mMessage;
    private AlertDialog.Builder builder;

    public SimpleDialog(Context context) {
        this.mContext = context;

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View mContentView = inflater.inflate(R.layout.dialog_simple, null);
        mTitle = (TextView) mContentView.findViewById(R.id.dlg_simple_title);
        mMessage = (TextView) mContentView.findViewById(R.id.dlg_simple_message);

        builder = new AlertDialog.Builder(mContext)
                    .setView(mContentView);
    }

    public SimpleDialog setTitle(int resId) {
        CharSequence title = mContext.getResources().getString(resId);
        return setTitle(title);
    }

    public SimpleDialog setTitle(CharSequence title) {
        if (mTitle != null) {
            mTitle.setVisibility(View.VISIBLE);
            mTitle.setText(title);
        }

        return this;
    }

    public SimpleDialog setMessage(int resId) {
        CharSequence msg = mContext.getResources().getString(resId);
        return setMessage(msg );
    }

    public SimpleDialog setMessage(CharSequence msg) {
        if (mMessage != null) {
            mMessage.setVisibility(View.VISIBLE);
            mMessage.setText(msg);
        }

        return this;
    }

    public SimpleDialog setPositivieButton(int resId, DialogInterface.OnClickListener listener){
        builder.setPositiveButton(resId, listener);
        return this;
    }

    public SimpleDialog setPositivieButton(CharSequence text, DialogInterface.OnClickListener listener){
        builder.setPositiveButton(text, listener);
        return this;
    }

    public SimpleDialog setNegativeButton(int resId, DialogInterface.OnClickListener listener){
        builder.setNegativeButton(resId, listener);
        return this;
    }

    public SimpleDialog setNegativeButton(CharSequence text, DialogInterface.OnClickListener listener){
        builder.setNegativeButton(text, listener);
        return this;
    }

    public SimpleDialog setNeutralButton(int resId, DialogInterface.OnClickListener listener){
        builder.setNeutralButton(resId, listener);
        return this;
    }

    public SimpleDialog setNeutralButton(CharSequence text, DialogInterface.OnClickListener listener){
        builder.setNeutralButton(text, listener);
        return this;
    }

    public void show(){
        builder.show();
    }
}
