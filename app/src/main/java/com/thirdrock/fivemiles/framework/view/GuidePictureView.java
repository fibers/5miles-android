package com.thirdrock.fivemiles.framework.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.BeeFramework.Utils.ImageUtil;
import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;

/**
 * Draw shadow for the phone in welcome pages.
 * Created by ywu on 14/12/10.
 */
public class GuidePictureView extends View {

    private int color = 0x27000000;
    private int rawImageWigth, rawImageHeight;
    private int rectangleWidthPx;  // width (in pixels) of the start of the shadow (a small rectangle)
    private int shadowMarginTop, shadowMarginStart;
    private int imageSrc;

    private Paint paint;
    private final RectF rect = new RectF();
    private final Path polygon = new Path();

    public GuidePictureView(Context context) {
        super(context);
        init(null);
    }

    public GuidePictureView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public GuidePictureView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs,
                    R.styleable.GuidePictureView, 0, 0);

            try {
                color = a.getColor(R.styleable.GuidePictureView_shadowColor, 0x27000000);
                rectangleWidthPx = a.getDimensionPixelSize(R.styleable.GuidePictureView_shadowRectWidth, 0);
                shadowMarginTop = a.getDimensionPixelSize(R.styleable.GuidePictureView_shadowMarginTop, 0);
                shadowMarginStart = a.getDimensionPixelSize(R.styleable.GuidePictureView_shadowMarginStart, 0);
                imageSrc = a.getResourceId(R.styleable.GuidePictureView_src, R.drawable.guide_1_main);
            } finally {
                a.recycle();
            }
        }

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), imageSrc, options);
        rawImageWigth = options.outWidth;
        rawImageHeight = options.outHeight;
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int w = getMeasuredWidth();
        int h = getMeasuredHeight();
        int rectW = Math.min(rectangleWidthPx, w);
        int imageWidth = Math.round(w * 0.7f);
        int imageHeight = Math.round(imageWidth * rawImageHeight / rawImageWigth);

        Bitmap bitmap = ImageUtil.decodeBitmapFromResource(getResources(), imageSrc, imageWidth, imageHeight);
        int rawW = bitmap.getWidth();
        int rawH = bitmap.getHeight();

        L.d("loaded bitmap: %sx%s desired: %sx%s", rawW, rawH, imageWidth, imageHeight);
        if (rawW > imageWidth && rawW > 0 && rawH > 0) {
            bitmap = Bitmap.createScaledBitmap(bitmap, imageWidth, imageHeight, false);
            L.d("dest bitmap: %sx%s", bitmap.getWidth(), bitmap.getHeight());
        }

        // shadow
        float imgRight = w - w * 0.15f;
        float imgTop = (h > bitmap.getHeight()) ? h - bitmap.getHeight() : 0;
        float shadowX0 = imgRight + shadowMarginStart;
        float shadowY0 = imgTop + shadowMarginTop;

        rect.set(shadowX0, shadowY0, shadowX0 + rectW, h);

        polygon.reset();
        polygon.moveTo(shadowX0 + rectW, shadowY0);
        polygon.lineTo(w, shadowY0 + (w - shadowX0 - rectW));  // 按等边三角形处理
        polygon.lineTo(w, h);
        polygon.lineTo(shadowX0 + rectW, h);
        polygon.close();

        paint.reset();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        canvas.drawRect(rect, paint);
        canvas.drawPath(polygon, paint);

        paint.reset();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        canvas.drawBitmap(bitmap, w * 0.15f, imgTop, paint);
    }
}
