package com.thirdrock.fivemiles.framework.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.view.GenericDialog;

import java.util.Collection;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * Generic dialog has a list of selections
 * Created by ywu on 15/1/21.
 */
public class SelectionListDialog extends GenericDialog {

    public interface OnSelectionListener {
        void onItemSelected(int position);
    }

    public interface OnCancelListener {
        void onCancel();
    }

    private Context context;
    private ArrayAdapter listAdapter;
    private OnSelectionListener selectionListener;
    private OnCancelListener cancelListener;

    @InjectView(R.id.dialog_message)
    TextView txtMsg;

    @InjectView(R.id.list_view)
    ListView listView;

    public SelectionListDialog(Context context) {
        super(context, R.layout.dialog_selection_list);

        setCanceledOnTouchOutside(false);
        setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (cancelListener != null) {
                    cancelListener.onCancel();
                }
            }
        });
    }

    @Override
    public void onViewInit(Context context, View contentView) {
        this.context = context;
        ButterKnife.inject(this, contentView);

        listAdapter = new ArrayAdapter(context, R.layout.dialog_selection_list_item, R.id.common_list_item_text);
        listView.setAdapter(listAdapter);

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 拦截click事件，避免点击背景关闭对话框
            }
        });
    }

    public SelectionListDialog setMessage(String message) {
        txtMsg.setText(message);
        return this;
    }

    public SelectionListDialog setMessage(int resId){
        txtMsg.setText(resId);
        return this;
    }

    @SuppressWarnings("unchecked")
    public SelectionListDialog setOptions(Collection options) {
        listAdapter.addAll(options);
        return this;
    }

    @OnItemClick(R.id.list_view)
    void onItemSelected(int position) {
        dismiss();

        if (selectionListener != null) {
            selectionListener.onItemSelected(position);
        }
    }

    @OnClick(R.id.btn_cancel)
    void onCancel() {
        dismiss();

        if (cancelListener != null) {
            cancelListener.onCancel();
        }
    }

    public SelectionListDialog setOnSelectionListener(OnSelectionListener listener) {
        selectionListener = listener;
        return this;
    }

    public SelectionListDialog setOnCancelListener(OnCancelListener listener) {
        this.cancelListener = listener;
        return this;
    }
}
