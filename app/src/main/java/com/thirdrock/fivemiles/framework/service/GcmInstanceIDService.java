package com.thirdrock.fivemiles.framework.service;

import android.content.Context;

import com.google.android.gms.iid.InstanceIDListenerService;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.fivemiles.main.home.GcmInfoHelper;

import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_GCM_INFO;

public class GcmInstanceIDService extends InstanceIDListenerService {

    public void onTokenRefresh() {
        // clear gcm info and shouldUpdateGcmInfo will return true in GcmHelper if play service available
        getSharedPreferences(PREFS_GCM_INFO, Context.MODE_PRIVATE)
              .edit().clear();

        if (SESSION.getInstance().isAuth()) {
            GcmInfoHelper.getInstance().registerGcmInBackgroundIfNecessary();
        }
    }

}
