package com.thirdrock.fivemiles.framework.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;

import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.igexin.sdk.PushConsts;
import com.igexin.sdk.PushManager;
import com.insthub.fivemiles.Activity.MainTabActivity;
import com.insthub.fivemiles.Activity.MakeOfferActivity;
import com.insthub.fivemiles.ConstantS;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.Model.LoginModel;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.PushActionUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.Push;
import com.thirdrock.protocol.Push__JsonHelper;
import com.thirdrock.protocol.SystemAction;

import org.json.JSONException;
import org.json.JSONObject;

import static com.insthub.fivemiles.FiveMilesAppConst.GETUI_PROVIDER;

public class PushReceiver extends BroadcastReceiver implements BusinessResponse {
    private Context context;
    private PushActionUtils actionUtils;

    @Override
    public void onReceive(final Context context, Intent intent) {
        this.context = context;
        actionUtils = new PushActionUtils(context);

        Bundle bundle = intent.getExtras();
        switch (bundle.getInt(PushConsts.CMD_ACTION)) {
            case PushConsts.GET_MSG_DATA:
                // 获取透传数据
                handlePushPayload(bundle);
                break;
            case PushConsts.GET_CLIENTID:
                // 获取ClientID(CID)
                // 第三方应用需要将CID上传到第三方服务器，并且将当前用户帐号和CID进行关联，以便日后通过用户帐号查找CID进行消息推送
                String cid = bundle.getString("clientid");

                String oldId = SESSION.getInstance().clientId;
                boolean shouldUpdateClientId = !TextUtils.equals(oldId, cid);
                if (shouldUpdateClientId) {
                    saveClientId(context, cid);
                    SESSION.getInstance().isGeTuiRegistered = false;
                }

//                if ( && SESSION.getInstance().isGeTuiRegistered) {
//                    // client id is up-to-date and registered to GeTui
//                    break;              // break out of switch
//                }

                boolean getuiNotRegistered = !SESSION.getInstance().isGeTuiRegistered;
                if (getuiNotRegistered) {
                    LoginModel loginModel = new LoginModel(context);
                    loginModel.addResponseListener(this);
                    loginModel.registerDevice(GETUI_PROVIDER, cid);
                }

                L.d("GeTui push client_id: %s", cid);
                break;
            case PushConsts.THIRDPART_FEEDBACK:
                break;
            default:
                break;
        }
    }

    private void handlePushPayload(Bundle bundle) {
        // String appid = bundle.getString("appid");
        byte[] payload = bundle.getByteArray("payload");

        String taskid = bundle.getString("taskid");
        String messageid = bundle.getString("messageid");

        // smartPush第三方回执调用接口，actionid范围为90000-90999，可根据业务场景执行
        boolean result = PushManager.getInstance().sendFeedbackMessage(context, taskid, messageid, 90001);
        L.d("Push message feedback, success: %s", result);

        if (payload == null) {
            L.e("Push payload is null");
            return;
        }

        String data = new String(payload);
        L.d("Got Payload:" + data);
        try {
            Push push = Push__JsonHelper.parseFromJson(data);
            if (push != null && push.getType() != null) {
                handlePushPayload(push);
            }
        } catch (Exception e) {
            L.e(e);
        }
    }

    private void handlePushPayload(Push push) {
        if (push == null || push.getType() == null) {
            L.w("push or push.getType() return null with push: " + push);
            return;
        }

        switch (push.getType()) {
            case offer:
                handleOfferPush(push);
                break;
            case system:
                handleActionPush(push);
                break;
            default:
                L.w("unknown Push message type: %s", push.getType());
                break;
        }
    }

    private void handleOfferPush(Push push) {
        int offerLineId = push.getOfferLineId();
        String msgId = push.getMsgId();

        Intent intent = null;
        if (!MakeOfferActivity.isOfferLineOnTop(offerLineId)) {
            intent = actionUtils.makeOfferIntent(push);
        }
        showNotification(push,
                context.getString(R.string.push_title_offer),
                intent);

        actionUtils.notifyNewOffer(offerLineId, msgId);
    }

    private void handleActionPush(Push push) {
        SystemAction act = push.getActionType();

        if (act == null) {
            L.w("invalid action Push, actionType is null");
            return;
        }

        switch (act) {
            case message:
                handleSysMsgActionPush(push);
                break;
            case item:
                handleItemActionPush(push);
                break;
            default:
                doHandleActionPush(push);
                break;
        }
    }

    // default handler
    private void doHandleActionPush(Push push) {
        Intent intent = actionUtils.makeIntent(push);
        showNotification(push, context.getString(R.string.app_name), intent);
        actionUtils.notifySysMsgRefresh();  // 刷新系统消息列表
    }

    private void handleSysMsgActionPush(Push push) {
        Intent intent = null;
        if (!MainTabActivity.isSysMsgViewOnTop()) {
            intent = actionUtils.makeIntent(push);
        }
        showNotification(push,
                context.getString(R.string.app_name),
                intent);

        actionUtils.notifySysMsgRefresh();  // 刷新系统消息列表
    }

    private void handleItemActionPush(Push push) {
        String itemId = push.getActionData();
        if (ModelUtils.isEmpty(itemId)) {
            L.w("invalid item Push, id is empty");
            return;
        }

        Intent intent = actionUtils.makeIntent(push);

        if (ItemActivity.isItemOnTop(itemId)) {
            // 如果用户正在浏览该商品, 则不再打开新页面
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);  // FIXME single_top not work
        }

        showNotification(push,
                context.getString(R.string.app_name),
                intent);

        actionUtils.notifySysMsgRefresh();  // 刷新系统消息列表
    }

    private void showNotification(Push push, String defaultTitle, Intent intent) {
        actionUtils.showNotification(push, defaultTitle, intent);
    }

    private void saveClientId(Context context, String cid) {
        SharedPreferences shared = context.getSharedPreferences(FiveMilesAppConst.USERINFO, 0);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(ConstantS.CLIENT_ID, cid);
        editor.apply();

        SESSION.getInstance().clientId = cid;
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status)
            throws JSONException {
        // simply ignores
    }
}