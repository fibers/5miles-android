package com.thirdrock.fivemiles.reco;

import com.thirdrock.framework.rest.RestObserver;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.repository.UserRepository;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.thirdrock.repository.UserRepository.FOLLOW_URL;
import static com.thirdrock.repository.UserRepository.SELLERS_NEARBY_URL;

/**
 * ViewModel for Sellers_Nearby & Good_Sellers page
 * Created by jubin on 14/4/15.
 */
public class SellersNearbyViewModel extends AbsViewModel<SellersNearbyViewModel.Property>{
    public enum Property{
        sellers, batch_follow
    }

    private Subscription subsSellers, subsBatchFollow;
    private RestObserver<Object> sellersObserver;
    private RestObserver<Object> batchFollowObserver;

    @Inject
    UserRepository repository;

    public SellersNearbyViewModel() {
        sellersObserver = newMajorApiJobObserver(SELLERS_NEARBY_URL, Property.sellers);
        batchFollowObserver = newMajorApiJobObserver(FOLLOW_URL, Property.batch_follow);
    }

    public void onStop() {
        unsubscribe(subsSellers, subsBatchFollow);
    }

    public void getNearbySellers(float lat, float lon) {
        emitMajorJobStarted();
        subsSellers = repository.getNearbySellersList(lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(sellersObserver.reset());
    }

    public void batchFollow(Iterable<String> userIdColl) {
        emitMajorJobStarted();
        subsBatchFollow = repository.batchfollow(userIdColl)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(batchFollowObserver.reset());
    }
}
