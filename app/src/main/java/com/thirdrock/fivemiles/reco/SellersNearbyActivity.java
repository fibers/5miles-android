package com.thirdrock.fivemiles.reco;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.insthub.fivemiles.Activity.MainTabActivity;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.SESSION;
import com.insthub.fivemiles.Utils.LocationManagerUtil;
import com.pedrogomez.renderers.RendererAdapter;
import com.thirdrock.domain.SellerInfo;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.activity.AbsFragmentActivity;
import com.thirdrock.fivemiles.util.EventUtils;
import com.thirdrock.fivemiles.util.LocationUtils;
import com.thirdrock.framework.ui.renderer.MonoRendererBuilder;
import com.thirdrock.framework.ui.renderer.SimpleAdapteeCollection;
import com.thirdrock.framework.util.L;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_GOTO_HOME;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_BACK_ENABLED;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_HOME_TAB;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REDIRECT_INTENT;
import static com.insthub.fivemiles.FiveMilesAppConst.USERS_NEARBY_BOUND_IN_MILES;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_SELLERS_NEARBY;
import static com.thirdrock.fivemiles.main.home.HomeActivity.IDX_FOLLOWING_PAGE;
import static com.thirdrock.fivemiles.reco.SellersNearbyViewModel.Property;

public class SellersNearbyActivity extends AbsFragmentActivity<SellersNearbyViewModel, Property> {
    private Set<String> sellersToFollowSet;

    @Inject
    SellersNearbyViewModel viewModel;

    @InjectView(R.id.txt_sellers_nearby_title)
    TextView txtTitle;

    @InjectView(R.id.lst_sellers_nearby)
    ListView lstNearbySellerList;

    @InjectView(R.id.txt_sellers_nearby_batch_follow)
    TextView txtBatFollow;

    @InjectView(R.id.txt_sellers_nearby_skip)
    TextView txtSkip;

    @InjectView(R.id.txt_sellers_nearby_back)
    TextView txtBack;

    TextView txtDesc;

    // 在SellerNearbyActivity中
    private Intent redirectIntent;

    private boolean isBackEnabled;

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {
        sellersToFollowSet = new HashSet<>();
        redirectIntent = getIntent().getParcelableExtra(ACT_PARA_REDIRECT_INTENT);

        EventUtils.register(this);
        SESSION.getInstance().setSellersNearbyHasShown(true);    // 已显示

        View headerView = View.inflate(SellersNearbyActivity.this, R.layout.sellers_nearby_lst_view_header, null);
        txtDesc = (TextView) headerView.findViewById(R.id.txt_sellers_nearby_desc);
        lstNearbySellerList.addHeaderView(headerView);

        handleIntent(getIntent());
    }

    @Override
    protected void doOnNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (intent == null) {
            L.w("intent should not be null in handleIntent");
            return;
        }

        isBackEnabled = intent.getBooleanExtra(ACT_PARA_BACK_ENABLED, false);
        if (isBackEnabled) {
            txtBack.setVisibility(View.VISIBLE);
            txtSkip.setVisibility(View.GONE);
        } else {
            txtBack.setVisibility(View.GONE);
            txtSkip.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        float latitude = LocationManagerUtil.getLatitudeInSession();
        float longitude = LocationManagerUtil.getLongitudeInSession();
        viewModel.getNearbySellers(latitude, longitude);

        boolean gpsNotEnabled = !LocationManagerUtil.getInstance().isEnabled();
        if (gpsNotEnabled) {
            trackTouch("nogps");
        } else if (latitude == 0 && longitude == 0) {
            trackTouch("wronggps");
        } else {
            trackTouch("rightgps");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        viewModel.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventUtils.unregister(this);
    }

    @Override
    protected String getScreenName() {
        return VIEW_SELLERS_NEARBY;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_sellers_nearby;
    }

    @Override
    protected SellersNearbyViewModel getViewModel() {
        return viewModel;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {
        switch (property) {
            case sellers:
                List<SellerInfo> nearbySellerList = (List<SellerInfo>) newValue;
                handleSellerList(nearbySellerList);
                initListView(nearbySellerList); // 这个页面只会请求一次，后面我下拉刷新也会去掉，为了简单，我使用了这种写法。
                break;

            case batch_follow:
                Intent followingTabIntent = new Intent(SellersNearbyActivity.this, MainTabActivity.class)
                        .setAction(ACT_GOTO_HOME)
                        .putExtra(ACT_PARA_HOME_TAB, IDX_FOLLOWING_PAGE)
                        .putExtra(ACT_PARA_REDIRECT_INTENT, redirectIntent);
                startActivity(followingTabIntent);
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onError(Property property, Throwable e) {
        enterMainPage();    // 出错则进入MainTabActivity
    }

    private void initListView(List<SellerInfo> nearbySellerList) {
        SimpleAdapteeCollection<SellerInfo> sellerList = new SimpleAdapteeCollection<>();
        sellerList.addAll(nearbySellerList);

        RendererAdapter<SellerInfo> rendererAdapter = new RendererAdapter<>(getLayoutInflater()
                , new MonoRendererBuilder(new SellersNearbyItemRenderer(SellersNearbyActivity.this))
                , sellerList);

        lstNearbySellerList.setAdapter(rendererAdapter);
    }

    /**
     * 设置seller的选中状态，标题和描述，以及Follow按钮文本
     * 此方法会修SellerInfo的isToFollow字段的值
     */
    private void handleSellerList(List<SellerInfo> pSellerList) {
        if (pSellerList == null) {
            L.w("param seller list should not be null");
            return;
        }

        boolean allSellersNearby = true;
        for (SellerInfo sellerInfo : pSellerList) {
            // 更新要Follow的用户列表，默认全部Follow
            if (sellerInfo != null) {
                sellerInfo.setToFollow(true);
                String sellerId = sellerInfo.getId();
                sellersToFollowSet.add(sellerId);
            }

            boolean locationNotInBound = !LocationUtils.isLocationInBound(sellerInfo, USERS_NEARBY_BOUND_IN_MILES);
            if (locationNotInBound) {
                allSellersNearby = false;
            }
        }
        // 默认所有Seller都被选中
        int sellersCount = (sellersToFollowSet == null)? 0 : sellersToFollowSet.size();
        String followWithNum = getString(R.string.follow_with_num, sellersCount);
        txtBatFollow.setText(followWithNum);

        // 设置标题和描述
        if (allSellersNearby) {
            txtTitle.setText(R.string.title_sellers_nearby);
            txtDesc.setText(R.string.desc_sellers_nearby);
            trackTouch("showsellersnearby");
        } else {
            txtTitle.setText(R.string.title_sellers_nearby_sorry);
            txtDesc.setText(R.string.desc_sellers_nearby_sorry);
            trackTouch("showrecommend");
        }
    }

    @OnClick(R.id.txt_sellers_nearby_skip)
    void skip() {
        enterMainPage();
        trackTouch("skip_follow");
    }

    @OnClick(R.id.txt_sellers_nearby_back)
    void navBack() {
        super.onBackPressed();
        trackTouch("follow_back");
    }

    private void enterMainPage() {
        Intent intent = new Intent(SellersNearbyActivity.this, MainTabActivity.class)
                .setAction(ACT_GOTO_HOME)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .putExtra(ACT_PARA_REDIRECT_INTENT, redirectIntent);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.txt_sellers_nearby_batch_follow)
    void batchFollow() {
        viewModel.batchFollow(sellersToFollowSet);
        trackTouch("followyes");
    }

    public void onEvent(Object event) {
        Message message = (Message) event;
        if (message == null) {
            L.w("message from EventBus should not be null");
            return;
        }

        String sellerId, sellersCount;
        switch (message.what) {
            case MessageConstant.SELLERS_NEARBY_FOLLOW_CLK:
                sellerId = (String) message.obj;
                sellersToFollowSet.add(sellerId);
                sellersCount = getString(R.string.follow_with_num, sellersToFollowSet.size());
                txtBatFollow.setText(sellersCount);

                trackTouch("follow_seller");
                break;

            case MessageConstant.SELLERS_NEARBY_UNFOLLOW_CLK:
                sellerId = (String) message.obj;
                sellersToFollowSet.remove(sellerId);
                sellersCount = getString(R.string.follow_with_num, sellersToFollowSet.size());
                txtBatFollow.setText(sellersCount);

                trackTouch("unfollow_seller");
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (isBackEnabled) {
           navBack();
        } else {
            skip();
        }
    }
}
