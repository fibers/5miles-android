package com.thirdrock.fivemiles.reco;

import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.external.eventbus.EventBus;
import com.insthub.fivemiles.Activity.OtherProfileActivity;
import com.insthub.fivemiles.MessageConstant;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.pedrogomez.renderers.Renderer;
import com.thirdrock.domain.ImageInfo;
import com.thirdrock.domain.Location;
import com.thirdrock.domain.SellerInfo;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.ui.widget.AvatarView;
import com.thirdrock.framework.util.L;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_ID;
import static com.thirdrock.fivemiles.util.LocationUtils.formatLocationWithDistance;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;

/**
 * Renderer for sellers nearby page
 * Created by jubin on 15/4/15.
 */
public class SellersNearbyItemRenderer extends Renderer<SellerInfo>{
    private Context context;

    private DisplayImageOptions avatarImgOpts;
    private DisplayImageOptions itemImgOpts;

    private LayoutInflater inflater;

    private AvatarView lytAvatar;

    @InjectView(R.id.sellers_nearby_lstitem_item_list)
    ViewGroup vgItemThumbList;

    @InjectView(R.id.txt_sellers_nearby_lstitem_username)
    TextView txtUsername;

    @InjectView(R.id.iv_sellers_nearby_lstitem_location_icon)
    ImageView ivLocIcon;

    @InjectView(R.id.txt_sellers_nearby_lstitem_location)
    TextView txtDistance;

    @InjectView(R.id.iv_sellers_nearby_lstitem_follow_status)
    ImageView ivFollowStatus;

    @InjectView(R.id.txt_sellers_nearby_lstitem_item_count)
    TextView txtItemCount;

    @InjectView(R.id.lyt_sellers_nearby_lstitem_item_list_wrapper)
    View lytItemListWrapper;

    @InjectView(R.id.sellers_nearby_lstitem_separator)
    View sepLine;

    public SellersNearbyItemRenderer(Context pContext) {
        this.context = pContext;

        avatarImgOpts = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.head_loading)
                .showImageForEmptyUri(R.drawable.no_avatar)
                .showImageOnFail(R.drawable.no_avatar)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        itemImgOpts = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading)
                .showImageForEmptyUri(R.drawable.loading)
                .showImageOnFail(R.drawable.loading)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup parent) {
        this.inflater = inflater;
        if (inflater == null) return null;

        return inflater.inflate(R.layout.sellers_nearby_lst_item, parent, false);
    }

    @Override
    protected void setUpView(View rootView) {
        ButterKnife.inject(this, rootView);
        lytAvatar = (AvatarView) rootView.findViewById(R.id.sellers_nearby_lstitem_avatar);
    }

    @Override
    protected void hookListeners(View rootView) {
        // empty
    }

    @Override
    public void onRecycle(SellerInfo newObj) {
        super.onRecycle(newObj);
        vgItemThumbList.removeAllViews();
    }

    @Override
    public void render() {
        SellerInfo sellerInfo = getContent();
        renderNearbySeller(sellerInfo);
    }

    protected void renderNearbySeller(SellerInfo sellerInfo) {
        renderAvatar(sellerInfo);
        renderUsername(sellerInfo);
        renderDistance(sellerInfo);
        renderFollowIcon(sellerInfo);       // 默认全部选中
        renderItemList(sellerInfo);
    }

    private void renderAvatar(SellerInfo sellerInfo) {
        if (sellerInfo == null) {
            L.w("sellerinfo should not be null while rendering");
            return;
        }

        DisplayUtils.showAvatar(lytAvatar, sellerInfo.getPortrait(), sellerInfo.isVerified(),
                context.getResources().getDimensionPixelSize(R.dimen.sellers_nearby_row_avatar_size), avatarImgOpts);
    }

    private void renderUsername(SellerInfo sellerInfo) {
        if (sellerInfo == null) {
            L.w("sellerinfo should not be null while rendering");
            return;
        }

        txtUsername.setText(sellerInfo.getNickName());
    }

    private void renderDistance(SellerInfo sellerInfo) {
        if (sellerInfo == null) {
            L.w("sellerinfo should not be null while rendering");
            return;
        }

        Location location = sellerInfo.getLocation();
        String place = sellerInfo.getPlace();
        String locationStr = formatLocationWithDistance(location, place);

        if (isNotEmpty(locationStr)) {
            ivLocIcon.setVisibility(View.VISIBLE);
            txtDistance.setVisibility(View.VISIBLE);
            txtDistance.setText(locationStr);
        } else {
            ivLocIcon.setVisibility(View.GONE);
            txtDistance.setVisibility(View.GONE);
        }
    }

    private void renderFollowIcon(SellerInfo sellerInfo) {
        if (sellerInfo == null) {
            L.w("sellerinfo should not be null while rendering");
            return;
        }

        // 推荐用户中有自己，不显示Follow按钮
        if (ModelUtils.isMe(sellerInfo.getId())) {
            ivFollowStatus.setVisibility(View.GONE);
        } else {
            ivFollowStatus.setVisibility(View.VISIBLE);
            boolean isToFollow = sellerInfo.isToFollow();
            renderFollowStatus(isToFollow);
        }
    }

    private void renderFollowStatus(boolean isToFollow) {
        if (isToFollow) {
            ivFollowStatus.setImageResource(R.drawable.ic_following);
            ivFollowStatus.setBackgroundResource(R.drawable.button_rectangle_orange);
        } else {
            ivFollowStatus.setImageResource(R.drawable.ic_follow);
            ivFollowStatus.setBackgroundResource(R.drawable.line_button_orange_light);
        }
    }

    private void renderItemList(SellerInfo sellerInfo) {
        if (sellerInfo == null) {
            L.w("sellerinfo should not be null while rendering");
            return;
        }

        if (sellerInfo.getItemNum() > 0) {
            sepLine.setVisibility(View.VISIBLE);
            lytItemListWrapper.setVisibility(View.VISIBLE);
            renderItemCount(sellerInfo);
            renderItemThumbs(sellerInfo);
        } else {
            sepLine.setVisibility(View.GONE);
            lytItemListWrapper.setVisibility(View.GONE);
        }

    }

    private void renderItemCount(SellerInfo sellerInfo) {
        if (sellerInfo == null) {
            L.w("sellerinfo should not be null while rendering");
            return;
        }

        String itemCountStr;
        int itemCount = sellerInfo.getItemNum();
        if (context == null) {
            L.w("context here is null and ");
            itemCountStr =  String.valueOf(itemCount);
        } else {
            itemCountStr =  context.getString(R.string.item_count, itemCount);
        }


        txtItemCount.setText(itemCountStr);
    }

    private void renderItemThumbs(SellerInfo sellerInfo) {
        if (inflater == null || sellerInfo == null) {
            L.e("inflater:" + inflater + ", seller info:" + sellerInfo
                    + "should not be null when rendering start");
            return;
        }

        List<ImageInfo> imageInfoList = sellerInfo.getItemImages();
        if (imageInfoList == null)  return;

        for (final ImageInfo imageInfo : imageInfoList) {
            View itemThumbWrapper =
                    inflater.inflate(R.layout.sellers_nearby_item_thumb, vgItemThumbList, false);

            if (itemThumbWrapper != null) {
                ImageView ivItemImage =
                        (ImageView) itemThumbWrapper.findViewById(R.id.iv_sellers_nearby_item_thumb);
                ImageLoader.getInstance().displayImage(imageInfo.getUrl(), ivItemImage, itemImgOpts);

                ivItemImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewItem(imageInfo);
                    }
                });
                vgItemThumbList.addView(itemThumbWrapper);
            }
        }
    }

    @OnClick(R.id.iv_sellers_nearby_lstitem_follow_status)
    void switchFollowStatus() {
        SellerInfo sellerInfo = getContent();
        if (sellerInfo != null) {
            // 点击switch follow状态
            boolean isToFollow = !sellerInfo.isToFollow();
            sellerInfo.setToFollow(isToFollow);
            renderFollowStatus(isToFollow);

            Message msg = Message.obtain();
            if (isToFollow) {
                msg.what = MessageConstant.SELLERS_NEARBY_FOLLOW_CLK;
            } else {
                msg.what = MessageConstant.SELLERS_NEARBY_UNFOLLOW_CLK;
            }
            msg.obj = sellerInfo.getId();
            EventBus.getDefault().post(msg);
        }
    }

    @OnClick({R.id.sellers_nearby_lstitem_avatar, R.id.txt_sellers_nearby_lstitem_username})
    void viewProfile() {
        SellerInfo sellerInfo = getContent();
        if (sellerInfo != null && context != null) {

            Intent viewProfileIntent = new Intent(context, OtherProfileActivity.class);
            viewProfileIntent.putExtra(ACT_PARA_USER_ID, sellerInfo.getId());
            context.startActivity(viewProfileIntent);
        }
    }

    // FIXME 现在服务器只传回了图片地址，无法跳转，这里做一个Log，用于测试
    private void viewItem(ImageInfo imageInfo) {
        if (imageInfo == null) {
            L.d("sellers nearby, item image info is null");
        } else {
            L.d("sellers nearby, image url: " + imageInfo.getUrl());
        }
    }
}
