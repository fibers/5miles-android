package com.thirdrock.fivemiles.search;

import com.insthub.fivemiles.FiveMilesApp;
import com.thirdrock.domain.AppConfig;
import com.thirdrock.domain.CategoryInfo;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.FileUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.CategoriesResp;
import com.thirdrock.protocol.CategoriesResp__JsonHelper;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by fibers on 15/7/17.
 */
public class CategoryViewModel extends AbsViewModel<CategoryViewModel.Property>{

    enum Property{
    }

    @Inject
    public CategoryViewModel(){

    }

    public static List<CategoryInfo> getLocalCategories(){

        List<CategoryInfo> categoryInfoList = null;

        AppConfig appConfig = FiveMilesApp.getAppConfig();
        if(appConfig != null){
            categoryInfoList = appConfig.getCategoryInfoList();
        }

        if(categoryInfoList == null) {
            InputStream isNewCategories = FiveMilesApp.appCtx.getResources().openRawResource(R.raw.new_categories);
            try {
                String newCategories = FileUtils.readFileContent(isNewCategories);
                if (ModelUtils.isNotEmpty(newCategories)) {
                    CategoriesResp categoriesResp = CategoriesResp__JsonHelper.parseFromJson(newCategories);
                    categoryInfoList = categoriesResp.getCategories();
                }
            } catch (Exception e) {
                L.e("Load local categories failed.");
            }
        }

        return categoryInfoList == null ? new ArrayList<CategoryInfo>() : categoryInfoList;
    }

}
