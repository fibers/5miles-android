package com.thirdrock.fivemiles.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pedrogomez.renderers.Renderer;
import com.thirdrock.domain.CategoryInfo;
import com.thirdrock.fivemiles.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by fibers on 15/7/21.
 */
public class CategoryRenderer extends Renderer<CategoryInfo> {


    @InjectView(R.id.tv_category_title_search)
    TextView tvCategoryTitle;

    private final Context context;

    public CategoryRenderer(Context context){
        this.context = context;

    }

    @Override
    protected void setUpView(View rootView) {
    }

    @Override
    protected void hookListeners(View rootView) {

    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup parent) {
        View inflatedView = inflater.inflate(R.layout.search_list_item, parent, false);
        ButterKnife.inject(this, inflatedView);

        return inflatedView;
    }

    @Override
    public void render() {
        CategoryInfo categoryInfo = getContent();
        renderCategoryTitle(categoryInfo);
    }

    private void renderCategoryTitle(CategoryInfo categoryInfo){
        tvCategoryTitle.setText(categoryInfo.getTitle());
    }


}
