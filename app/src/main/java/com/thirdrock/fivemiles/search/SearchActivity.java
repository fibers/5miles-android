package com.thirdrock.fivemiles.search;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.insthub.fivemiles.Activity.SearchResultActivity;
import com.insthub.fivemiles.Activity.WelcomeActivity;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.SESSION;
import com.pedrogomez.renderers.AdapteeCollection;
import com.pedrogomez.renderers.RendererAdapter;
import com.thirdrock.domain.CategoryInfo;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.activity.AbsActivity;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.EventUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.ui.renderer.MonoRendererBuilder;
import com.thirdrock.framework.ui.renderer.SimpleAdapteeCollection;
import com.thirdrock.framework.util.L;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnFocusChange;
import butterknife.OnItemClick;
import butterknife.OnTextChanged;

import static android.widget.LinearLayout.LayoutParams;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_S_CATEGORY;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_S_KEYWORD;
import static com.insthub.fivemiles.MessageConstant.PUSH_ACT_SEARCH;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;



/**
 * Created by fibers on 15/7/17.
 */
public class SearchActivity extends AbsActivity<CategoryViewModel, CategoryViewModel.Property> {

    @Inject
    CategoryViewModel categoryViewModel;

    @InjectView(R.id.rg_tabs_search)
    RadioGroup rgTabs;

    @InjectView(R.id.iv_magnifier_search)
    ImageView ivMagnifier;

    @InjectView(R.id.et_keyword_search)
    EditText etKeyword;

    @InjectView(R.id.iv_clear_search)
    ImageView ivClear;

    @InjectView(R.id.tv_cancel_search)
    TextView tvCancel;

    @InjectView(R.id.lv_sub_categories_search)
    ListView subCategoriesList;

    private AdapteeCollection<CategoryInfo> categoryInfoCollection;

    private RendererAdapter<CategoryInfo> categoryInfoAdapter;

    private Runnable pendingAction;

    private Map<Integer, List<CategoryInfo>> topCategoryMap = new HashMap<Integer, List<CategoryInfo>>();

    private List<CategoryInfo> categoryInfoList;

    private Map<Integer, Integer> categoryParentMap = new HashMap<Integer, Integer>();

    private TextView tvSelectedCategory;


    @Override
    protected void doOnCreate(Bundle savedInstanceState) {

        boolean userNotAuthed = !SESSION.getInstance().isAuth();
        if (userNotAuthed) {
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
            return;
        }

        rgTabs.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                int radioButtonCount = group.getChildCount();
                for (int i = 0; i < radioButtonCount; i++) {

                    RadioButton radioButton = (RadioButton) group.getChildAt(i);
                    int radioButtonId = radioButton.getId();
                    int drawableResourceId;

                    if (radioButtonId == checkedId) {
                        drawableResourceId = getTopCategoryCheckedIconId(radioButtonId);
                    } else {
                        drawableResourceId = getTopCategoryIconId(radioButtonId);
                    }
                    Drawable drawableTop = getResources().getDrawable(drawableResourceId);
                    radioButton.setCompoundDrawablesWithIntrinsicBounds(null, drawableTop, null, null);
                }

                if (topCategoryMap != null) {
                    categoryInfoCollection.clear();
                    categoryInfoCollection.addAll(
                            topCategoryMap.get(Integer.valueOf(checkedId)));
                    categoryInfoAdapter.notifyDataSetChanged();
                }
            }
        });


        subCategoriesList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                closeKeyBoard();
            }
        });

        categoryInfoList = CategoryViewModel.getLocalCategories();

        for (CategoryInfo category : categoryInfoList) {
            List<CategoryInfo> subCategories = category.getChildren();
            topCategoryMap.put(Integer.valueOf(category.getId()), subCategories);

            for(CategoryInfo subCategory : subCategories){
                categoryParentMap.put(Integer.valueOf(subCategory.getId()), Integer.valueOf(category.getId()));
            }
        }

        categoryInfoCollection = new SimpleAdapteeCollection<CategoryInfo>();
        CategoryRenderer renderer = new CategoryRenderer(this);
        categoryInfoAdapter = new RendererAdapter<CategoryInfo>(
                getLayoutInflater(),
                new MonoRendererBuilder(renderer),
                categoryInfoCollection
        );
        subCategoriesList.setAdapter(categoryInfoAdapter);

        addTopCategories();
        setCheckedCategory();

        EventUtils.register(this);
    }


    @OnItemClick(R.id.lv_sub_categories_search)
    public void onItemClickSubCategory(AdapterView<?> parent, View view, int position, long id) {

        setSearchBoxNormalStyle();
        CategoryInfo categoryInfo = categoryInfoAdapter.getItem(position);

        TextView tvCategoryTitle = (TextView) view.findViewById(R.id.tv_category_title_search);
        tvSelectedCategory = tvCategoryTitle;
        tvCategoryTitle.setSelected(true);
        doSearchByCategory(categoryInfo, null);
    }

    @OnTextChanged(R.id.et_keyword_search)
    public void onTextChangedKeyword(CharSequence s, int start, int before, int count) {
        String keyword = etKeyword.getText().toString().trim();

        if (keyword.length() > 0) {
            ivClear.setVisibility(View.VISIBLE);
        } else {
            ivClear.setVisibility(View.GONE);
        }
    }

    @OnFocusChange(R.id.et_keyword_search)
    public void onFocusChangeKeyword(View v, boolean hasFocus) {
        if (hasFocus) {
            setSearchBoxInputStyle();
        } else {
            setSearchBoxNormalStyle();
        }
    }

    @OnEditorAction(R.id.et_keyword_search)
    public boolean onEditorActionKeyword(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            if (ModelUtils.isEmpty(etKeyword.getText())) {
                DisplayUtils.toast(R.string.msg_input_something);
            } else {
                TrackingUtils.trackTouch(FiveMilesAppConst.VIEW_SEARCH, "search_start");
                doSearchByKeyword(etKeyword.getText().toString(), null);
            }
            return true;
        }
        return false;
    }


    @OnClick(R.id.iv_clear_search)
    public void onClickClear(View view) {
        etKeyword.setText("");
    }

    @OnClick(R.id.tv_cancel_search)
    public void onClickCancel(View view) {
        setSearchBoxNormalStyle();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (tvSelectedCategory != null) {
            tvSelectedCategory.setSelected(false);
        }
    }

    @Override
    protected String getScreenName() {
        return FiveMilesAppConst.VIEW_SEARCH;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_search;
    }

    @Override
    protected CategoryViewModel getViewModel() {
        return categoryViewModel;
    }


    public void onEvent(Object e) {
        Message msg = (Message) e;
        if (msg.what == PUSH_ACT_SEARCH) {
            doSearch(msg.getData());
        }
    }

    private void doSearch(Bundle data) {
        if (data.containsKey(ACT_PARA_S_CATEGORY)) {
            int categoryId = data.getInt(ACT_PARA_S_CATEGORY);
            if (categoryId >= 0 && categoryInfoCollection != null) {
                CategoryInfo catg = getCatgryById(categoryId);
                if (catg != null) {
                    doSearchByCategory(catg, data);
                }
            }
        } else {
            String keyword = data.getString(ACT_PARA_S_KEYWORD);
            if (isNotEmpty(keyword)) {
                doSearchByKeyword(keyword, data);
            }
        }
    }

    private CategoryInfo getCatgryById(int catgryId) {
        for (int i = 0; i < categoryInfoCollection.size(); i++) {
            CategoryInfo catg = categoryInfoCollection.get(i);
            if (catg != null && catgryId == catg.getId()) {
                return catg;
            }
        }

        return null;
    }


    private void showKeyBoard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, 0);
    }

    private void closeKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etKeyword.getWindowToken(), 0);
    }

    private void putAllExtra(Bundle data, Intent intent) {
        if (data == null || intent == null) {
            return;
        }

        intent.putExtras(data);
    }

    private void doSearchByCategory(CategoryInfo category, Bundle data) {

        setSearchBoxNormalStyle();

        Intent intent = new Intent(this, SearchResultActivity.class);
        if (category != null) {
            intent.putExtra("top_category_id", categoryParentMap.get(category.getId()));
            intent.putExtra("category_id", category.getId());
            intent.putExtra("category_title", category.getTitle());
            L.d("category search with id: [%d]  tilte: [%s]", category.getId(), category.getTitle());
        } else {
            intent.putExtra("category_id", FiveMilesAppConst.DEFAULT_CATEGORY_ID);
            intent.putExtra("category_title", getString(R.string.default_category_title));
            L.e("category is wrong in doSearchByCategory");
        }

        intent.putExtra("fromList", true);
        putAllExtra(data, intent);
        startActivity(intent);

        TrackingUtils.trackTouch(FiveMilesAppConst.VIEW_SEARCH, "category");
    }


    private void doSearchByKeyword(String keyword, Bundle data) {
        if (ModelUtils.isEmpty(keyword)) {
            return;
        }

        setSearchBoxNormalStyle();

//        pendingAction = new Runnable() {
//            @Override
//            public void run() {
//                // clean search keyword #228
//                etKeyword.setText("");
//            }
//        };

        Intent intent = new Intent(this, SearchResultActivity.class);
        intent.putExtra("search_text", keyword);
        intent.putExtra("fromList", false);
        putAllExtra(data, intent);
        startActivity(intent);

        TrackingUtils.trackTouch(FiveMilesAppConst.VIEW_SEARCH, "search_action");
    }

    private void addTopCategories() {

        for (int i = 0; i < categoryInfoList.size(); i++) {

            CategoryInfo categoryInfo = categoryInfoList.get(i);

            RadioButton radioButton = new RadioButton(this);
            radioButton.setId(categoryInfo.getId());
            radioButton.setText(categoryInfo.getTitle().toUpperCase());
            radioButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().
                    getDimension(R.dimen.search_top_category_text_size));
            radioButton.setTextColor(getResources().
                    getColorStateList(R.color.search_top_category_text_color));

            if (Build.VERSION.SDK_INT >= 16) {
                radioButton.setBackground(getResources().
                        getDrawable(R.drawable.search_top_category_background));
            } else {
                radioButton.setBackgroundDrawable(getResources().
                        getDrawable(R.drawable.search_top_category_background));
            }

            radioButton.setButtonDrawable(null);
            radioButton.setWidth(getResources().
                    getDimensionPixelSize(R.dimen.search_top_catgory_width));
            radioButton.setHeight(getResources().
                    getDimensionPixelSize(R.dimen.search_top_catgory_height));
            radioButton.setGravity(Gravity.CENTER);
            radioButton.setCompoundDrawablePadding(0);
            radioButton.setPadding(0
                    , getResources().getDimensionPixelSize(R.dimen.search_top_category_padding_top)
                    , 0
                    , getResources().getDimensionPixelSize(R.dimen.search_top_category_padding_bottom));

            rgTabs.addView(radioButton);
        }
    }

    private int getTopCategoryIconId(int categoryId) {
        switch (categoryId) {
            case 1000:
                return R.drawable.ic_top_category_1000;
            case 1001:
                return R.drawable.ic_top_category_1001;
            case 1002:
                return R.drawable.ic_top_category_1002;
            case 1003:
                return R.drawable.ic_top_category_1003;
            default:
                return R.drawable.ic_top_category_1000;
        }
    }

    private int getTopCategoryCheckedIconId(int categoryId) {
        switch (categoryId) {
            case 1000:
                return R.drawable.ic_top_category_1000_checked;
            case 1001:
                return R.drawable.ic_top_category_1001_checked;
            case 1002:
                return R.drawable.ic_top_category_1002_checked;
            case 1003:
                return R.drawable.ic_top_category_1003_checked;
            default:
                return R.drawable.ic_top_category_1000_checked;
        }
    }


    void setCheckedCategory() {
        int radioButtonCount = rgTabs.getChildCount();
        if (radioButtonCount > 0) {
            rgTabs.check(rgTabs.getChildAt(0).getId());
        }
    }

    void setSearchBoxInputStyle() {

        LayoutParams layoutParams = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT
        );

        int marginTop = getResources().getDimensionPixelSize(R.dimen.search_magnifier_margin_top);
        layoutParams.setMargins(0, marginTop, 0, 0);

        ivMagnifier.setLayoutParams(layoutParams);
        etKeyword.requestFocus();
        tvCancel.setVisibility(View.VISIBLE);

        showKeyBoard(etKeyword);
    }

    void setSearchBoxNormalStyle() {

        LayoutParams layoutParams = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT
        );

        int marginLeft = getResources().getDimensionPixelSize(R.dimen.search_magnifier_margin_left);
        int marginTop = getResources().getDimensionPixelSize(R.dimen.search_magnifier_margin_top);
        layoutParams.setMargins(marginLeft, marginTop, 0, 0);

        ivMagnifier.setLayoutParams(layoutParams);
        etKeyword.setText("");
        etKeyword.clearFocus();
        tvCancel.setVisibility(View.GONE);

        closeKeyBoard();
    }


    @Override
    public void onPropertyChanged(CategoryViewModel.Property property, Object oldValue, Object newValue) throws Exception {

    }
}
