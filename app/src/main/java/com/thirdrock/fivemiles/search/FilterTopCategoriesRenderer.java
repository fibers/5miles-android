package com.thirdrock.fivemiles.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pedrogomez.renderers.Renderer;
import com.thirdrock.domain.CategoryInfo;
import com.thirdrock.fivemiles.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by fibers on 15/7/28.
 */
public class FilterTopCategoriesRenderer extends Renderer<CategoryInfo> {

    @InjectView(R.id.tv_top_category_title_search_result)
    TextView tvTopCategoryTitle;

    private final Context context;

    public FilterTopCategoriesRenderer(Context context){
        this.context = context;

    }

    @Override
    protected void setUpView(View rootView) {
    }

    @Override
    protected void hookListeners(View rootView) {

    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup parent) {
        View inflatedView = inflater.inflate(
                R.layout.search_result_top_categories_filter_list_item,
                parent, false);
        ButterKnife.inject(this, inflatedView);

        return inflatedView;
    }

    @Override
    public void render() {
        CategoryInfo categoryInfo = getContent();
        renderCategoryTitle(categoryInfo);
    }

    private void renderCategoryTitle(CategoryInfo categoryInfo){
        tvTopCategoryTitle.setText(categoryInfo.getTitle());
    }
}
