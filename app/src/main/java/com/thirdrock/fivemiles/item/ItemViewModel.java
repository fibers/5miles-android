package com.thirdrock.fivemiles.item;

import android.content.Context;
import android.util.Pair;

import com.insthub.fivemiles.SESSION;
import com.insthub.fivemiles.Utils.ResourceDownloader;
import com.thirdrock.domain.EnumShareType;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.domain.OfferLine;
import com.thirdrock.fivemiles.util.DeepLinkHelper;
import com.thirdrock.fivemiles.util.LocationUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.exception.RestException;
import com.thirdrock.framework.rest.RestObserver;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.GetLikersResp;
import com.thirdrock.protocol.RenewItemResp;
import com.thirdrock.repository.ItemRepository;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLatitudeInSession;
import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLongitudeInSession;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;
import static com.thirdrock.fivemiles.util.ModelUtils.isMine;

/**
 * Created by ywu on 14/11/20.
 */
public class ItemViewModel extends AbsViewModel<ItemViewModel.Property> {

    public static final int MAX_RELATED_ITEMS = 10;

    public enum Property {
        item, liked, unliked, audio, suggestions, other_items, share_content, likers, delete
        , renew, mark_sold, relist, unlist
    }

    // when item detail retrieved
    private class ItemObserver extends MajorApiJobObserver<Item> {

        private ItemObserver() {
            super("item_detail", Property.item);
        }

        @Override
        public void doOnNext(Item item) {
            ItemViewModel.this.item = item;
            ItemViewModel.this.loadingItem = false;
            super.doOnNext(item);
        }

        @Override
        public void onCompleted() {
            ItemViewModel.this.loadingItem = false;
            super.onCompleted();
        }

        @Override
        public void onError(Throwable e) {
            ItemViewModel.this.loadingItem = false;
            super.onError(e);
        }
    }

    // when item liked
    private class LikeObserver extends MinorApiJobObserver<Void> {

        private LikeObserver() {
            super("like", Property.liked, true);
        }

        @Override
        public void doOnNext(Void aVoid) {
            item.setLiked(true);
            super.doOnNext(aVoid);
        }
    }

    // when item removed from liked
    private class UnlikeObserver extends MinorApiJobObserver<Void> {
        private UnlikeObserver() {
            super("unlike", Property.unliked, false);
        }

        @Override
        public void doOnNext(Void aVoid) {
            item.setLiked(false);
            super.doOnNext(aVoid);
        }
    }

    // when selling item list retrieved
    private class OtherItemObserver extends MinorApiJobObserver<List<ItemThumb>> {
        private OtherItemObserver() {
            super("user_sellings", Property.other_items);
        }

        @Override
        public void doOnNext(List<ItemThumb> items) {
            List<ItemThumb> result = items;
            if (item != null) {
                result = new ArrayList<>(items.size());
                for (int i = 0; i < items.size(); i++) {
                    if (result.size() == MAX_RELATED_ITEMS) {
                        break;
                    }

                    ItemThumb relatedItem = items.get(i);
                    if (!relatedItem.equals(item)) {
                        result.add(relatedItem);
                    }
                }
            }

            super.doOnNext(result);
        }
    }

    private final RestObserver<Item> itemObserver;
    private final RestObserver<Void> likeObserver, unlikeObserver;

    // when item audio downloaded
    private final Observer<File> audioObserver = newMinorJobObserver(Property.audio);

    // when item likers resp retrieved
    private final RestObserver<GetLikersResp> likersObserver;

    // when suggestion list retrieved
    private final RestObserver<List<ItemThumb>> suggestionObserver;

    // when selling item list retrieved
    private final RestObserver<List<ItemThumb>> otherItemObserver;

    // when sharing content is ready
    private final Observer<Pair<String, String>> shareObserver = newMajorJobObserver(Property.share_content);

    // when an item is deleted
    private final RestObserver<Void> deleteObserver;

    // when an item is updated
    private final RestObserver<RenewItemResp> renewObserver;

    // when an item is marked as sold
    private final RestObserver<Void> markSoldObserver;

    // when an item is relisted
    private final RestObserver<Void> relistObserver;

    // when an item is unlisted
    private final RestObserver<Void> unlistObserver;

    @Inject
    ItemRepository itemRepo;

    Item item;
    boolean loadingItem;
    private Subscription subsItem, subsLike, subsAudio, subsLikers, subsSuggestion, subsOtherItems
            , subsShare, subsDelete, subsRenew, subsMarkSold, subsRelist, subsUnlist;

    public ItemViewModel() {
        itemObserver = new ItemObserver();

        likeObserver = new LikeObserver();
        unlikeObserver = new UnlikeObserver();

        likersObserver = newMinorApiJobObserver("item_likers", Property.likers);

        suggestionObserver = newMinorApiJobObserver("suggest", Property.suggestions);
        otherItemObserver = new OtherItemObserver();

        deleteObserver = newMajorApiJobObserver("delete_item", Property.delete);
        renewObserver = newMajorApiJobObserver("renew_item", Property.renew);
        markSoldObserver = newMajorApiJobObserver("mark_sold", Property.mark_sold);

        relistObserver = newMajorApiJobObserver("relist_item", Property.relist);
        unlistObserver = newMajorApiJobObserver("unlist_item", Property.unlist);
    }

    public void getItem(String id) {
        loadingItem = true;
        subsItem = itemRepo.getItemById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itemObserver.reset());
    }

    public void getItem(String id, HashMap<String,String> params){
        loadingItem = true;
        subsItem = itemRepo.getItemById(id,params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itemObserver.reset());

    }

    public void onStop() {
        unsubscribe(subsItem, subsLike, subsAudio, subsSuggestion, subsOtherItems, subsShare
                , subsDelete, subsLikers, subsRenew, subsMarkSold, subsRelist, subsUnlist);
    }

    public void toggleLiked() {
        if (item == null) {
            return;
        }

        RestObserver<Void> observer = item.isLiked() ? unlikeObserver : likeObserver;

        subsLike = itemRepo.like(item.getId(), !item.isLiked())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer.reset());
    }

    public OfferLine getMyOfferLine() {
        OfferLine mine = null;

        for (OfferLine offerLine : item.getOfferLines()) {
            if (isMine(offerLine)) {
                mine = offerLine;
                break;
            }
        }

        return mine;
    }

    public void downloadAudio() {
        if (item == null || isEmpty(item.getMediaLink())) {
            return;
        }

        subsAudio = ResourceDownloader.downloadResource(item.getMediaLink())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(audioObserver);
    }

    public void getLikers() {
        if (item == null) {
            return;
        }

        subsLikers = itemRepo.getLikers(item.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(likersObserver.reset());
    }

    public void getSimilarItems() {
        if (item == null) {
            return;
        }

        subsSuggestion = itemRepo.suggestByItem(item.getId(), getLatitudeInSession(), getLongitudeInSession())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(suggestionObserver.reset());
    }

    public void getOtherItemsOfSeller() {
        if (item == null) {
            return;
        }

        subsOtherItems = itemRepo.getUserItems(item.getOwner().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(otherItemObserver.reset());
    }

    public void prepareShareContent(final Context context) {
        if (item == null) {
            return;
        }

        Observable<String> getCity = LocationUtils.getCity();
        Observable<String> getLink = Observable.create(new Observable.OnSubscribe<String>() {

            @Override
            public void call(final Subscriber<? super String> subscriber) {
                if (subscriber.isUnsubscribed()) {
                    return;
                }

                DeepLinkHelper deepLinkHelper = new DeepLinkHelper(context);
                String userId = SESSION.getInstance().userId;
                String itemId = item.getId();

                deepLinkHelper.generateBranchShortUrl(userId, EnumShareType.ITEM, itemId, new DeepLinkHelper.OnDeepLinkCreateListener() {

                    @Override
                    public void onLinkCreateEvent(boolean isSuccess, String url, String errorMsg) {
                        if (isSuccess) {
                            subscriber.onNext(url);
                        } else {
                            subscriber.onError(new RestException(500, errorMsg));
                        }
                    }
                });
            }
        }).subscribeOn(AndroidSchedulers.mainThread());

        emitMajorJobStarted();
        subsShare = Observable.zip(getLink, getCity, new Func2<String, String, Pair<String, String>>() {
            @Override
            public Pair<String, String> call(String url, String city) {
                return new Pair<>(url, city);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(shareObserver);
    }

    public void renewItem() {
        if (item == null){
            L.w("item to renew is null");
            return;
        }

        emitMajorJobStarted();
        subsRenew = itemRepo.renewItem(item.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(renewObserver.reset());
    }

    public void deleteItem() {
        if (item == null) {
            return;
        }

        emitMajorJobStarted();
        subsDelete = itemRepo.deleteItem(item.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(deleteObserver.reset());
    }

    public void markSold(){
        if (item == null){
            return;
        }

        emitMajorJobStarted();
        subsMarkSold = itemRepo.markSold(item.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(markSoldObserver.reset());
    }

    public void relistItem(){
        if (item == null){
            return;
        }

        emitMajorJobStarted();
        subsRelist = itemRepo.relistItem(item.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(relistObserver.reset());
    }

    public void unlistItem(){
        if (item == null){
            return;
        }

        emitMajorJobStarted();
        subsUnlist = itemRepo.unlistItem(item.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(unlistObserver.reset());
    }

    public boolean isOwner() {
        return item != null && item.getOwner() != null
                && ModelUtils.isOwner(item.getOwner().getId());
    }
}
