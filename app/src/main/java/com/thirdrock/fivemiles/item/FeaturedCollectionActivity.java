package com.thirdrock.fivemiles.item;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.insthub.fivemiles.FiveMilesAppConst;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.pedrogomez.renderers.AdapteeCollection;
import com.pedrogomez.renderers.RendererAdapter;
import com.thirdrock.domain.FeaturedCollection;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.common.item.CollectionItemRenderer;
import com.thirdrock.fivemiles.framework.activity.AbsActivity;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.framework.ui.renderer.MonoRendererBuilder;
import com.thirdrock.framework.ui.renderer.SimpleAdapteeCollection;
import com.thirdrock.framework.util.L;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_COLLECTION_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM_ID;
import static com.thirdrock.fivemiles.item.FeaturedCollectionViewModel.Property;

public class FeaturedCollectionActivity extends AbsActivity<FeaturedCollectionViewModel, Property> {

    static class Banner {
        @InjectView(R.id.banner_wrapper)
        View wrapper;

        @InjectView(R.id.img_banner_ad)
        ImageView image;
    }

    @Inject
    FeaturedCollectionViewModel viewModel;

    @InjectView(R.id.list_items)
    ListView lstItems;

    @InjectView(R.id.top_view_logo)
    View icLogo;

    private View blankViewWrapper;

    private RendererAdapter<ItemThumb> itemListAdapter;
    private AdapteeCollection<ItemThumb> itemThumbs;
    private Banner banner;

    private final DisplayImageOptions bannerImgOpts = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.loading)
            .showImageForEmptyUri(R.drawable.loading)
            .showImageOnFail(R.drawable.loading)
            .cacheInMemory(false)
            .cacheOnDisk(true)
            .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
            .build();

    @Override
    protected String getScreenName() {
        return FiveMilesAppConst.VIEW_FEATURED_COLLECTION;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_featured_collection;
    }

    @Override
    protected FeaturedCollectionViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {
        icLogo.setVisibility(View.VISIBLE);

        initListView();

        Intent intent = getIntent();
        if (intent.hasExtra(ACT_PARA_COLLECTION_ID)) {
            loadFeaturedCollection(intent.getStringExtra(ACT_PARA_COLLECTION_ID));
        }
    }

    private void initListView() {
        initBanner();
        initListAdapter();
    }

    private void initBanner() {
        View bannerView = getLayoutInflater().inflate(R.layout.featured_banner, null);
        ButterKnife.inject(banner = new Banner(), bannerView);
        lstItems.addHeaderView(bannerView, null, false);

        View blankView = getLayoutInflater().inflate(R.layout.featured_collection_blank_view, null);
        blankViewWrapper = blankView.findViewById(R.id.blank_view_wrapper);
        lstItems.addHeaderView(blankView, null, false);
    }

    private void initListAdapter() {
        itemThumbs = new SimpleAdapteeCollection<>();
        itemListAdapter = new RendererAdapter<>(
                getLayoutInflater(),
                new MonoRendererBuilder(new CollectionItemRenderer()),
                itemThumbs);
        lstItems.setAdapter(itemListAdapter);
    }

    private void loadFeaturedCollection(String id) {
        viewModel.loadFeaturedCollection(id);
    }

    @OnClick(R.id.top_view_back)
    void onNaviBack() {
        finish();
    }

    @Override
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {
        switch (property) {
            case featured_collection:
                showCollection((FeaturedCollection) newValue);
                break;
            default:
                break;
        }
    }

    private void showCollection(FeaturedCollection collection) {
        itemThumbs.clear();
        itemThumbs.addAll(collection.getItems());
        itemListAdapter.notifyDataSetChanged();

        blankViewWrapper.setVisibility(itemThumbs.size() > 0 ? View.GONE : View.VISIBLE);

        showCollectionBanner(collection);
    }

    private void showCollectionBanner(FeaturedCollection collection) {
        float ratio = ((float) collection.getBannerWidth()) / collection.getBannerHeight();
        int w = banner.wrapper.getMeasuredWidth();
        int h = (int) Math.ceil(w / ratio);

        String url = CloudHelper.toCropUrl(collection.getBannerUrl(), w, h, "fill", true);
        ImageLoader.getInstance().displayImage(url, banner.image, bannerImgOpts);
    }

    @OnItemClick(R.id.list_items)
    void onItemClicked(int position) {
        L.d("item clicked: %d", position);
        int index = position - lstItems.getHeaderViewsCount();
        if (index >= 0 && index < itemThumbs.size()) {
            ItemThumb item = itemThumbs.get(index);

            Intent intent = new Intent(this, ItemActivity.class);
            intent.putExtra(ACT_PARA_ITEM_ID, item.getId());
//            intent.putExtra(ACT_PARA_THUMB_URL, item.getLatestThumbUrl());
            startActivity(intent);

            trackTouch("recommendproduct");
        }
    }
}
