package com.thirdrock.fivemiles.item;

import com.thirdrock.domain.FeaturedCollection;
import com.thirdrock.framework.rest.RestObserver;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.repository.ItemRepository;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ywu on 14/12/29.
 */
public class FeaturedCollectionViewModel extends AbsViewModel<FeaturedCollectionViewModel.Property> {

    public static enum Property {
        featured_collection
    }

    @Inject
    ItemRepository itemRepo;

    private final RestObserver<FeaturedCollection> collectionObserver;

    private Subscription subsCollection;

    public FeaturedCollectionViewModel() {
        collectionObserver = newMajorApiJobObserver("featured_items", Property.featured_collection);
    }

    public void onStop() {
        unsubscribe(subsCollection);
    }

    public void loadFeaturedCollection(String id) {
        subsCollection = itemRepo.getFeaturedCollection(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(collectionObserver.reset());
    }
}
