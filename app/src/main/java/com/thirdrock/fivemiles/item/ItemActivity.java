package com.thirdrock.fivemiles.item;

import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.external.viewpagerindicator.CirclePageIndicator;
import com.facebook.AppEventsLogger;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.insthub.fivemiles.Activity.ItemLocationMapActivity;
import com.insthub.fivemiles.Activity.MakeOfferActivity;
import com.insthub.fivemiles.Activity.MyProfileActivity;
import com.insthub.fivemiles.Activity.OtherProfileActivity;
import com.insthub.fivemiles.Activity.ReportActivity;
import com.insthub.fivemiles.Activity.SearchResultActivity;
import com.insthub.fivemiles.Activity.TabProfileActivity;
import com.insthub.fivemiles.Adapter.GalleryImageAdapter;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Protocol.IMAGES;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.thirdrock.domain.EnumItemState;
import com.thirdrock.domain.ImageInfo;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.domain.Location;
import com.thirdrock.domain.OfferLine;
import com.thirdrock.domain.User;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.common.item.ItemThumbRenderer;
import com.thirdrock.fivemiles.framework.activity.AbsFragmentActivity;
import com.thirdrock.fivemiles.framework.dialog.SimpleDialog;
import com.thirdrock.fivemiles.framework.view.AudioProgressView;
import com.thirdrock.fivemiles.main.listing.ListItemActivity;
import com.thirdrock.fivemiles.offer.BuyItemActivity;
import com.thirdrock.fivemiles.review.ReviewListActivity;
import com.thirdrock.fivemiles.util.AudioPlaybackWatcher;
import com.thirdrock.fivemiles.util.Deliveries;
import com.thirdrock.fivemiles.util.Delivery;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.EduScreen;
import com.thirdrock.fivemiles.util.EduUtils;
import com.thirdrock.fivemiles.util.EventUtils;
import com.thirdrock.fivemiles.util.LocationUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.fivemiles.util.PendingActionHelper;
import com.thirdrock.fivemiles.util.RateHelper;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.exception.RestException;
import com.thirdrock.framework.ui.widget.AvatarView;
import com.thirdrock.framework.ui.widget.CountingTimerView;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.SimpleObserver;
import com.thirdrock.protocol.GetLikersResp;
import com.thirdrock.protocol.RenewItemResp;
import com.thirdrock.repository.ReportRepository;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.google.maps.android.SphericalUtil.computeDistanceBetween;
import static com.insthub.fivemiles.Activity.ItemLocationMapActivity.DEFAULT_RANGE_RADIUS;
import static com.insthub.fivemiles.Activity.ItemLocationMapActivity.MAP_PADDING_RADIUS;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_EDIT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_CAN_GO_BACK;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM_THUMB;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_OFFER_LINE_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REPORTED_OID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REPORT_TYPE;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_RF_TAG;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELLER_RATE_AVG;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELLER_RATE_COUNT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_S_BRAND_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_S_BRAND_NAME;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.DAY_IN_SECOND;
import static com.insthub.fivemiles.FiveMilesAppConst.ERR_BLOCKED;
import static com.insthub.fivemiles.FiveMilesAppConst.HOUR_IN_SECOND;
import static com.insthub.fivemiles.FiveMilesAppConst.ITEM_EDU_VERIFY_RENEW_KEY;
import static com.insthub.fivemiles.FiveMilesAppConst.ITEM_EDU_VERIFY_RENEW_V;
import static com.insthub.fivemiles.FiveMilesAppConst.MINUTE_IN_SECOND;
import static com.insthub.fivemiles.FiveMilesAppConst.MONTH_IN_SECOND;
import static com.insthub.fivemiles.FiveMilesAppConst.SECOND_IN_MILLS;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_ITEM;
import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLatitudeInSession;
import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLongitudeInSession;
import static com.thirdrock.domain.EnumItemState.LISTING;
import static com.thirdrock.domain.EnumItemState.SOLD;
import static com.thirdrock.domain.EnumItemState.UNLISTED;
import static com.thirdrock.fivemiles.item.ItemViewModel.Property;
import static com.thirdrock.fivemiles.util.Currencies.formatCurrency;
import static com.thirdrock.fivemiles.util.DisplayUtils.showText;
import static com.thirdrock.fivemiles.util.DisplayUtils.toast;
import static com.thirdrock.fivemiles.util.LocationUtils.calcDistanceOnScreen;
import static com.thirdrock.fivemiles.util.LocationUtils.findCoordinate;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;
import static com.thirdrock.fivemiles.util.ModelUtils.isMine;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;
import static com.thirdrock.fivemiles.util.ModelUtils.isValidPrice;
import static com.thirdrock.fivemiles.util.RateHelper.RateEvent.markSold;
import static com.thirdrock.framework.util.Utils.doOnGlobalLayout;

public class ItemActivity extends AbsFragmentActivity<ItemViewModel, Property>
        implements SwipeRefreshLayout.OnRefreshListener {

    public static final Pattern LINK_PATTERN = Pattern.compile("/?item/(.+)");
    public static boolean isLive;
    private static boolean isOnTop;
    private static String currItemId;
    private boolean isDestroyed;    // isDestroyed()方法要求最低版本为17，目前为14，自己实现下。

    private int minItemMapRadiusPx;  // min radius of the item location range, in px

    private RateHelper mRateHelper;

    @InjectView(R.id.swipe_refresh_view)
    SwipeRefreshLayout swipeRefreshLayout;

    @InjectView(R.id.owner_name)
    TextView txtOwner;

    @InjectView(R.id.avatar)
    AvatarView imgAvatar;

    @InjectView(R.id.item_brand)
    TextView txtBrand;

    @InjectView(R.id.item_brand_wrapper)
    View brandWrapper;

    @InjectView(R.id.item_pic_pager)
    ViewPager picPager;

    @InjectView(R.id.item_page_indicator)
    CirclePageIndicator pageIndicator;

    @InjectView(R.id.item_price)
    TextView txtPrice;

    @InjectView(R.id.item_origin_price)
    TextView txtOriginPrice;

    @InjectView(R.id.btn_like_buy_ask_wrapper)
    View likeBuyAskButtonWrapper;

    @InjectView(R.id.btn_like)
    Button btnLike;

    @InjectView(R.id.btn_buy_enabled)
    View txtBuyEnabled;

    @InjectView(R.id.btn_buy_disabled)
    View txtBuyDisabled;

    @InjectView(R.id.renew_edit_button_wrapper)
    View renewEditButtonWrapper;

    @InjectView(R.id.btn_renew_enabled)
    TextView txtRenewButton;

    @InjectView(R.id.btn_renew_disabled)
    TextView txtRenewButtonDisabled;

    @InjectView(R.id.top_view_share)
    View btnShare;

    @InjectView(R.id.ic_item_state_sold)
    View icItemSold;

    @InjectView(R.id.ic_item_state_other)
    TextView txtOtherState;

    @InjectView(R.id.item_location_wrapper)
    View locationWrapper;

    @InjectView(R.id.item_location_text)
    TextView txtLocation;

    @InjectView(R.id.item_audio_wrapper)
    View audioWrapper;

    @InjectView(R.id.btn_play_audio)
    View btnPlayAudio;

    @InjectView(R.id.item_audio_playback_elapse)
    TextView txtAudioElapse;

    @InjectView(R.id.item_audio_playback_duration)
    TextView txtAudioDuration;

    @InjectView(R.id.item_audio_prog)
    AudioProgressView progAudioPlayback;

    @InjectView(R.id.item_title)
    TextView txtTitle;

    @InjectView(R.id.item_desc)
    TextView txtDesc;

    @InjectView(R.id.item_update_time_text)
    TextView txtUpdateTime;

    @InjectView(R.id.item_delivery)
    TextView txtDelivery;

    @InjectView(R.id.item_offer_wrapper)
    View offerWrapper;

    @InjectView(R.id.item_offers)
    ViewGroup offersContainer;

    @InjectView(R.id.item_likes_wrapper)
    View likerWrapper;

    @InjectView(R.id.item_like_list)
    ViewGroup likerList;

    @InjectView(R.id.item_likes_count)
    TextView txtLikerCount;

    @InjectView(R.id.related_item_ctrls)
    View relatedItemTab;

    @InjectView(R.id.rdb_similar_items)
    CompoundButton rdbSimliarItems;

    @InjectView(R.id.report)
    View itmReport;

    @InjectView(R.id.section_above_report_item)
    View reportDivider;

    @InjectView(R.id.item_edit_dialog_wrapper)
    View editDlgWrapper;

    @InjectView(R.id.related_items_wrapper)
    ViewGroup relatedItemLayout;

    @InjectView(R.id.item_edit_details)
    View txtEditDetailsMenuItem;

    @InjectView(R.id.item_mark_sold)
    View txtMarkSoldMenuItem;

    @InjectView(R.id.item_unlist)
    View txtUnlistMenuItem;

    @InjectView(R.id.item_relist)
    View txtRelistMenuItem;

    @InjectView(R.id.item_delete)
    View txtDeleteMenuItem;

    @InjectView(R.id.item_tag_new)
    View imgNewTag;

    @InjectView(R.id.item_map)
    View mapPlaceholder;

    @InjectView(R.id.item_sticky_scroll_view)
    View stickyScrollView;

    @Inject
    ItemViewModel viewModel;

    @InjectView(R.id.item_owner_reviews_star)
    ImageView itemOwnerReviewsStar;

    private AppEventsLogger fbEventsLogger;

    private String itemId;
    private HashMap<String,String> inParam;//other params bring into item requests.
    private ItemThumb itemThumb;  // optional thumb info, for preview
    private Bitmap thumbBitmap;
    private GalleryImageAdapter galleryAdapter;
    private List<IMAGES> images = new ArrayList<>();
    private DisplayImageOptions avatarImgOpts;
    private boolean reviewEnabled;

    // map view
    private GoogleMap itemMap;
    private Circle itemMapCircle;

    private MediaPlayer audioPlayer;
    private ValueAnimator audioLoadingAnimation;  // animations for loading audio
    private AudioPlaybackWatcher audioWatcher;

    // renew & edit ||  buy & ask button line
    private CountingTimerView viewCountDown;
    private LinearLayout lytUpdateTimeWrapper;
    private FrameLayout lytAskEnabled;
    private FrameLayout lytAskDisabled;

    // report
    private LinearLayout lytReportWrapper;

    private EduScreen verifyAndRenewEduScreen;

    private PendingActionHelper pendingActionHelper;

    private Subscription subsUpdateCountDown;

    private final Runnable doInitPicPager = new Runnable() {
        @Override
        public void run() {
            galleryAdapter = new GalleryImageAdapter(ItemActivity.this, images, thumbBitmap);
            picPager.setAdapter(galleryAdapter);
            pageIndicator.setViewPager(picPager, 0);
        }
    };

    private final Runnable doLayoutPicPager = new Runnable() {
        @Override
        public void run() {
            // make picture frame a square TODO make a custom layout
            int desiredSize = swipeRefreshLayout.getMeasuredWidth();
            L.d("item picture desired square size: %d", desiredSize);

            ViewGroup.LayoutParams params = picPager.getLayoutParams();
            params.width = desiredSize;
            params.height = desiredSize;
            picPager.setLayoutParams(params);
            picPager.requestLayout();
            pageIndicator.requestLayout();
        }
    };

    private final Runnable doAnimateLoadItem = new Runnable() {
        @Override
        public void run() {
            loadItem();
            swipeRefreshLayout.setRefreshing(true);
        }
    };

    private void showItemOnMap(LatLng itemLatLng, double radius) {
        if (itemMapCircle != null) {
            itemMapCircle.remove();
        }

        CircleOptions circle = new CircleOptions()
                .center(itemLatLng)
                .radius(radius)
                .fillColor(getResources().getColor(R.color.fm_blue_map_overlay))
                .strokeColor(getResources().getColor(android.R.color.transparent))
                .strokeWidth(1);
        itemMapCircle = itemMap.addCircle(circle);
    }

    public static boolean isItemOnTop(String itemId) {
        return isOnTop && TextUtils.equals(itemId, currItemId);
    }

    @Override
    protected String getScreenName() {
        return VIEW_ITEM;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_item;
    }

    @Override
    protected ItemViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {

        fbEventsLogger = AppEventsLogger.newLogger(this);

        isDestroyed = false;
        minItemMapRadiusPx = getResources().getDimensionPixelSize(R.dimen.item_map_circle_radius);

        DisplayUtils.setProgressColorScheme(swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setProgressViewOffset(true,
                getResources().getDimensionPixelSize(R.dimen.swipe_refresh_progress_offset_start),
                getResources().getDimensionPixelSize(R.dimen.swipe_refresh_progress_offset_end));
        swipeRefreshLayout.setEnabled(false);  // 暂时禁止下拉刷新

        findViewById(R.id.top_view_title).setVisibility(View.GONE);
        findViewById(R.id.top_view_logo).setVisibility(View.VISIBLE);

        viewCountDown = (CountingTimerView) findViewById(R.id.update_time_text);
        lytUpdateTimeWrapper = (LinearLayout) findViewById(R.id.btn_renew_timer);

        lytAskEnabled = (FrameLayout) findViewById(R.id.btn_ask_enabled_wrapper);
        lytAskDisabled = (FrameLayout) findViewById(R.id.btn_ask_disabled_wrapper);

        lytReportWrapper = (LinearLayout) findViewById(R.id.report_wrapper);

        avatarImgOpts = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.head_loading)
                .showImageForEmptyUri(R.drawable.no_avatar)
                .showImageOnFail(R.drawable.no_avatar)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        txtOriginPrice.setPaintFlags(
                txtOriginPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        initPicPager();
        layoutPicPager();
        initMapView();
        onIntent(getIntent());
        lytUpdateTimeWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayUtils.toast(R.string.toast_wait_renew_again);
                trackTouch("Renew_countdown");
            }
        });

        pendingActionHelper = new PendingActionHelper(this);
        mRateHelper = new RateHelper(ItemActivity.this);
        // 控制是否显示reviews
        reviewEnabled = FiveMilesApp.getAppConfig().isReviewEnabled();
    }

    private void initMapView() {
        boolean isNotFinishing = !isFinishing();
        boolean isNotDestroyed = !isDestroyed;
        // isDestroyed()的最低支持版本是17
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            isNotDestroyed = !isDestroyed();
        }
        if (isNotFinishing && isNotDestroyed) {
            SupportMapFragment mapFragment = SupportMapFragment.newInstance(
                    new GoogleMapOptions()
                            .liteMode(true)
                            .mapType(GoogleMap.MAP_TYPE_NORMAL)
                            .mapToolbarEnabled(false)
                            .compassEnabled(false)
                            .zoomControlsEnabled(false)
            );
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.item_map, mapFragment)
                    .commitAllowingStateLoss();     // 异步里面commit() transaction会导致IllegalStateException

            InternalOnMapReadyCallback onMapReadyCallback = new InternalOnMapReadyCallback(ItemActivity.this);
            mapFragment.getMapAsync(onMapReadyCallback);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        isLive = true;
        EventUtils.register(this);
        pendingActionHelper.handlePendingActions(VIEW_ITEM);
    }

    @Override
    protected void onResume() {
        super.onResume();

        fbEventsLogger.logEvent(VIEW_ITEM);

        isOnTop = true;

        stopMainProgress();  // close progress dialog if any
        animateLoadItem();
    }

    static class InternalOnMapReadyCallback implements OnMapReadyCallback {
        private WeakReference<ItemActivity> mActivity;

        private InternalOnMapReadyCallback(ItemActivity pActivity) {
            this.mActivity = new WeakReference<>(pActivity);
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            final ItemActivity itemActivity = mActivity.get();
            if (itemActivity != null) {
                itemActivity.itemMap = googleMap;
                itemActivity.itemMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        itemActivity.onLocationClicked();
                    }
                });

                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        itemActivity.onLocationClicked();
                        return true;
                    }
                });

                googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        if (itemActivity.viewModel.item == null
                                || itemActivity.viewModel.item.getLocation() == null) {
                            return;
                        }

                        L.d("map camera position changed, zoom level: %f", cameraPosition.zoom);
                        if (itemActivity.viewModel != null && itemActivity.viewModel.item != null) {
                            Location itemLocation = itemActivity.viewModel.item.getLocation();
                            if (itemLocation != null) {
                                LatLng itemLatLng = new LatLng(itemLocation.getLatitude(), itemLocation.getLongitude());
                                double radius = itemActivity.computeItemRadius(itemLatLng);
                                itemActivity.showItemOnMap(itemLatLng, radius);
                            }
                        }
                    }
                });
            }
        }
    }

//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        itemMap = googleMap;
//
//        itemMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//            @Override
//            public void onMapClick(LatLng latLng) {
//                onLocationClicked();
//            }
//        });
//
//        itemMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(Marker marker) {
//                onLocationClicked();
//                return true;
//            }
//        });
//
//        itemMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
//            @Override
//            public void onCameraChange(CameraPosition cameraPosition) {
//                if (viewModel.item == null || viewModel.item.getLocation() == null) {
//                    return;
//                }
//
//                L.d("map camera position changed, zoom level: %f", cameraPosition.zoom);
//                Location itemLocation = viewModel.item.getLocation();
//                LatLng itemLatLng = new LatLng(itemLocation.getLatitude(), itemLocation.getLongitude());
//                showItemOnMap(itemLatLng, computeItemRadius(itemLatLng));
//            }
//        });
//
//        updateMapView(itemThumb);
//    }

    private double computeItemRadius(LatLng itemLatLng) {
        double radius;
        Projection prj = itemMap.getProjection();
        Point itemPnt = prj.toScreenLocation(itemLatLng);
        LatLng edgeLatLng = findCoordinate(itemLatLng, DEFAULT_RANGE_RADIUS, 90);
        Point edgePnt = prj.toScreenLocation(edgeLatLng);

        double radiusOnScreen = calcDistanceOnScreen(itemPnt, edgePnt);

        if (radiusOnScreen < minItemMapRadiusPx) {
            // 确保商品半径在屏幕至少达到一定的宽度（px）
            edgePnt = new Point(itemPnt);
            edgePnt.x += minItemMapRadiusPx;
            edgeLatLng = prj.fromScreenLocation(edgePnt);
            radius = computeDistanceBetween(itemLatLng, edgeLatLng);
        } else {
            radius = DEFAULT_RANGE_RADIUS;
        }

        return radius;
    }

    private void onIntent(Intent intent) {

        if (intent.hasExtra(ACT_PARA_ITEM_THUMB)) {
            itemThumb = (ItemThumb) intent.getSerializableExtra(ACT_PARA_ITEM_THUMB);
            itemId = itemThumb.getId();
            String rfTag = itemThumb.getRfTag();
            if(rfTag != null){
                inParam = new HashMap<>();
                inParam.put("rf_tag",rfTag);
            }

            showItemThumb();
        } else {
            itemId = intent.getStringExtra(ACT_PARA_ITEM_ID);
        }

        //add rf_tag for stand alone page like makeoffer page back to item page
        if(intent.hasExtra(ACT_PARA_RF_TAG)){
            inParam = new HashMap<>();
            inParam.put("rf_tag", intent.getStringExtra(ACT_PARA_RF_TAG));
        }

        if (Intent.ACTION_VIEW.equals(intent.getAction()) && intent.hasExtra(ACT_PARA_ITEM_ID)) {
            itemId = intent.getStringExtra(ACT_PARA_ITEM_ID);
            animateLoadItem();
        }
    }

    private void animateLoadItem() {
        doOnGlobalLayout(swipeRefreshLayout, doAnimateLoadItem);
    }

    @Override
    protected void doOnNewIntent(Intent intent) {
        super.onNewIntent(intent);
        onIntent(intent);
    }

    @Override
    public void onRefresh() {
        loadItem();
        trackTouch("product_refresh");
    }

    private void loadItem() {
        if (isNotEmpty(itemId)) {
            if(inParam != null){
                viewModel.getItem(itemId,inParam);
            }else{
                viewModel.getItem(itemId);
            }

        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private boolean shouldShowVerifyAndRenewEdu() {
        // 卖家视角，且商品处于LISTING或UNLISTED状态才会显示教学
        if (viewModel.isOwner()
                && (viewModel.item.getState() == LISTING || viewModel.item.getState() == UNLISTED)){
            return EduUtils.shouldShow(ITEM_EDU_VERIFY_RENEW_KEY, ITEM_EDU_VERIFY_RENEW_V);
        }

        return false;
    }

    private void showVerifyAndRenewEdu() {
        if (!shouldShowVerifyAndRenewEdu() ||
                (verifyAndRenewEduScreen != null && verifyAndRenewEduScreen.isShowing())) {
            return;
        }

        verifyAndRenewEduScreen = EduUtils.showEduScreen(ItemActivity.this
                , R.style.app_edu
                , R.layout.edu_item_seller_page
                , ITEM_EDU_VERIFY_RENEW_KEY
                , ITEM_EDU_VERIFY_RENEW_V
                , new EduScreen.ViewController() {
            @Override
            public void onInit(EduScreen screen, final View contentView) {
                // 点击"go to verify"按钮，去验证
                contentView.findViewById(R.id.edu_item_btn_verify).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent myProfileIntent = new Intent(ItemActivity.this, MyProfileActivity.class);

                        startActivity(myProfileIntent);
                        trackTouch("Go_to_verify");
                    }
                });

                // 点击教学中的"renew"按钮，关闭教学页
                contentView.findViewById(R.id.edu_item_btn_renew).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideVerifyAndRenewEdu();
                    }
                });

                // 根据pager的大小调整Renew按钮的位置
                final View itemPicPagerPlaceholder = contentView.findViewById(R.id.item_pic_pager_placeholder);
                final ViewGroup.LayoutParams params = itemPicPagerPlaceholder.getLayoutParams();

                picPager.post(new Runnable() {
                    @Override
                    public void run() {
                        params.height = picPager.getMeasuredHeight();
                        L.d("item pic pager height is changed to " + params.height);
                        itemPicPagerPlaceholder.requestLayout();            // 不加似乎也可以？为了保证位置正确，还是调一下，并且也没有副作用
                    }
                });
            }
        });
    }

    private void hideVerifyAndRenewEdu() {
        if (verifyAndRenewEduScreen != null && verifyAndRenewEduScreen.isShowing()) {
            verifyAndRenewEduScreen.dismiss();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isOnTop = false;

        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        pendingActionHelper.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isLive = false;
        EventUtils.unregister(this);

        if (audioLoadingAnimation != null && audioLoadingAnimation.isRunning()) {
            audioLoadingAnimation.cancel();
        }

        releaseAudioPlayer();

        if (subsUpdateCountDown != null) {
            subsUpdateCountDown.unsubscribe();
        }

        hideVerifyAndRenewEdu();
        viewModel.onStop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        // 参考：https://techblog.badoo.com/blog/2014/08/28/android-handler-memory-leaks/
        swipeRefreshLayout.removeCallbacks(null);   // 消除handler带来的memory leak
        stickyScrollView.removeCallbacks(null);
        if (mapPlaceholder != null) {
            mapPlaceholder.removeCallbacks(null);
        }
        if (picPager != null) {
            picPager.removeCallbacks(null);
        }
        if (itemMap != null) {
            itemMap.setOnCameraChangeListener(null);    // 避免OnCameraChangeListener泄漏ItemActivity
        }

        isDestroyed = true;
    }

    @Override
    protected void doOnActivityResult(int requestCode, int resultCode, Intent data) {
        pendingActionHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            trackTouch("product_back");
        }

        return super.onKeyDown(keyCode, event);
    }

    @SuppressWarnings("unused")
    public void onEvent(Object event) {
        Message message = (Message) event;
        if (message.what == MessageConstant.PUSH_ORDERDETIAL) {
            itemId = (String) message.obj;
            animateLoadItem();
        }
    }

    private void initPicPager() {
        doOnGlobalLayout(picPager, doInitPicPager);
    }

    private void initPicPagerWithThumb(final Bitmap thumb) {
        this.thumbBitmap = thumb;
        doOnGlobalLayout(picPager, new Runnable() {
            @Override
            public void run() {
                galleryAdapter = new GalleryImageAdapter(ItemActivity.this, thumb);
                picPager.setAdapter(galleryAdapter);
                pageIndicator.setViewPager(picPager, 0);
            }
        });
    }

    private void layoutPicPager() {
        doOnGlobalLayout(swipeRefreshLayout, doLayoutPicPager);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {
        switch (property) {
            case item:
                Item item = (Item)newValue;
                onItemUpdated(item);
                fbEventsLogger.logEvent("category_" + item.getCategoryId());
                break;
            case liked:
                updateLiked(viewModel.item);
                promptLiked(viewModel.item);
                viewModel.getLikers();
                break;
            case unliked:
                updateLiked(viewModel.item);
                promptLiked(viewModel.item);
                viewModel.getLikers();
                trackTouch("unlike");
                break;
            case audio:
                initAudioPlayer((File) newValue);
                break;
            case likers:
                showLikers((GetLikersResp) newValue);
                break;
            case suggestions:
                showRelatedItems((List<ItemThumb>) newValue);
                break;
            case other_items:
                showRelatedItems((List<ItemThumb>) newValue);
                break;
            case share_content:
                shareItem((Pair<String, String>) newValue);
                break;
            case delete:
                finish();
                break;
            case renew:
                onItemRenewed((RenewItemResp) newValue);
                break;
            case mark_sold:
                if(inParam != null){
                    viewModel.getItem(itemId, inParam);
                }else{
                    viewModel.getItem(itemId);
                }

                // show rate app after first mark as sold if necessary
                if (mRateHelper.shouldShowRateDialog(markSold)) {
                    mRateHelper.showRateApp();
                }
                mRateHelper.markNotFirstMarkSold(); // TODO 改善逻辑，应该不必每次都写
                break;
            case relist:
                if(inParam != null){
                    viewModel.getItem(itemId,inParam);
                }else{
                    viewModel.getItem(itemId);
                }
                break;
            case unlist:
                if(inParam != null){
                    viewModel.getItem(itemId,inParam);
                }else{
                    viewModel.getItem(itemId);
                }
                break;
        }
    }

    private void onItemRenewed(RenewItemResp renewItemResp) {
        if (renewItemResp == null) {
            toast(R.string.toast_renew_failed);
            return;
        }
        toast(R.string.toast_renew_success);

        long renewTtl = renewItemResp.getRenewTtl();
        startUpdateCountDown(renewTtl);

        long currentSeconds = Calendar.getInstance().getTimeInMillis() / SECOND_IN_MILLS;
        renderUpdateTime(currentSeconds);

        trackTouch("Renew");
    }

    @Override
    public void onError(Property property, Throwable e) {
        if (e instanceof RestException && ((RestException) e).getErrorCode() == ERR_BLOCKED) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.user_blocked_info)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setCancelable(false)
                    .show();
            return;
        }

        super.onError(property, e);
        switch (property) {
            case item:
                onItemUpdateFailed();
                break;
        }
    }

    @Override
    public void onMinorJobError(Property property, Throwable e) {
        switch (property) {
            case likers:
                clearLikerList();
                break;
            case suggestions:
                showRelatedItems(Collections.<ItemThumb>emptyList());
                break;
            case other_items:
                showRelatedItems(Collections.<ItemThumb>emptyList());
                break;
        }
    }

    @OnClick(R.id.btn_edit_wrapper)
    @SuppressWarnings("unused")
    void onEdit() {
        switch (viewModel.item.getState()) {
            case LISTING:
                txtEditDetailsMenuItem.setVisibility(View.VISIBLE);
                txtMarkSoldMenuItem.setVisibility(View.VISIBLE);
                txtUnlistMenuItem.setVisibility(View.VISIBLE);
                txtRelistMenuItem.setVisibility(View.GONE);
                txtDeleteMenuItem.setVisibility(viewModel.item.isDeletable()? View.VISIBLE : View.GONE);
                break;
            case UNAPPROVED:
            case UNAVAILABLE:
                txtEditDetailsMenuItem.setVisibility(View.GONE);
                txtMarkSoldMenuItem.setVisibility(View.GONE);
                txtUnlistMenuItem.setVisibility(View.GONE);
                txtRelistMenuItem.setVisibility(View.GONE);
                txtDeleteMenuItem.setVisibility(viewModel.item.isDeletable()? View.VISIBLE : View.GONE);
                break;
            case SOLD:
                txtEditDetailsMenuItem.setVisibility(View.GONE);
                txtMarkSoldMenuItem.setVisibility(View.GONE);
                txtUnlistMenuItem.setVisibility(View.GONE);
                txtRelistMenuItem.setVisibility(View.VISIBLE);
                txtDeleteMenuItem.setVisibility(viewModel.item.isDeletable()? View.VISIBLE : View.GONE);
                break;
            case RECEIVED:
            case SELLER_RATED:
                // edit button should not show here
                L.d("btn_edit_wrapper should not show in SOLD state");
                break;
            case UNLISTED:
                txtEditDetailsMenuItem.setVisibility(View.VISIBLE);
                txtMarkSoldMenuItem.setVisibility(View.VISIBLE);
                txtUnlistMenuItem.setVisibility(View.GONE);
                txtRelistMenuItem.setVisibility(View.VISIBLE);
                txtDeleteMenuItem.setVisibility(viewModel.item.isDeletable()? View.VISIBLE : View.GONE);
                break;
        }

        editDlgWrapper.setVisibility(View.VISIBLE);
        trackTouch("edit");
    }

    @OnClick(R.id.item_edit_dialog_wrapper)
    void hideEditDialog() {
        if (editDlgWrapper != null) {
            editDlgWrapper.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.item_edit_details)
    void editDetails() {
        if (viewModel.item == null) {
            return;
        }

        hideEditDialog();

        Intent intent = new Intent(ItemActivity.this, ListItemActivity.class);
        intent.setAction(ACT_EDIT);
        intent.putExtra(ACT_PARA_ITEM, viewModel.item);
        startActivity(intent);
        trackTouch("edit_details");
    }

    @OnClick(R.id.item_mark_sold)
    void markItemAsSold() {
        if (!viewModel.isOwner()) {
            return;
        }

        hideEditDialog();

        String dlgMsg;
        List<OfferLine> offerLineList = viewModel.item.getOfferLines();

        if (offerLineList != null && offerLineList.size() > 0){
            dlgMsg = getString(R.string.dlg_mark_sold_alert, offerLineList.size());
        } else {
            dlgMsg = getResources().getString(R.string.dlg_unlist_alert_mark_sold);
        }

        new SimpleDialog(ItemActivity.this)
                .setTitle(R.string.dlg_title_mark_sold)
                .setMessage(dlgMsg)
                .setPositivieButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (viewModel.isOwner()) {
                            viewModel.markSold();
                        }
                        trackTouch("markassold_yes");
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        trackTouch("markassold_no");
                    }
                })
                .show();

        trackTouch("markassold");
    }

    @OnClick(R.id.item_unlist)
    void unlistItem() {
        if (!viewModel.isOwner()) {
            return;
        }

        hideEditDialog();

        String dlgMsg;
        List<OfferLine> offerLineList = viewModel.item.getOfferLines();
        if (offerLineList != null && offerLineList.size() > 0) {
            dlgMsg = getString(R.string.dlg_unlist_alert, offerLineList.size());
        } else {
            dlgMsg = getResources().getString(R.string.dlg_delete_r_u_sure);
        }

        new SimpleDialog(ItemActivity.this)
                .setTitle(R.string.dlg_title_unlist)
                .setMessage(dlgMsg)
                .setPositivieButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (viewModel.isOwner()) {
                            viewModel.unlistItem();
                        }
                        trackTouch("unlist_yes");
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        trackTouch("unlist_no");
                    }
                })
                .show();
        trackTouch("unlist");
    }

    @OnClick(R.id.item_relist)
    void relistItem() {
        if (!viewModel.isOwner()) {
            return;
        }

        hideEditDialog();

        if (viewModel.isOwner()){
            viewModel.relistItem();
        }
        trackTouch("relist");
    }

    @OnClick(R.id.item_delete)
    void deleteItem(){
        if (!viewModel.isOwner()) {
            return;
        }

        hideEditDialog();

        new SimpleDialog(ItemActivity.this)
                .setTitle(R.string.dlg_tilte_delete)
                .setMessage(R.string.dlg_delete_r_u_sure)
                .setPositivieButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (viewModel.isOwner()) { // 检查，防止误删
                            viewModel.deleteItem();
                        }
                        trackTouch("delete_productyes");
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        trackTouch("delete_productno");
                    }
                })
                .show();
        trackTouch("delete_product");
    }

    @OnClick(R.id.item_cancel)
    void cancelEditItem() {
        hideEditDialog();
    }

    @OnClick(R.id.btn_like)
    @SuppressWarnings("unused")
    void onLike() {
        viewModel.toggleLiked();
        if (viewModel.item != null && !viewModel.item.isLiked()) {
            trackTouch("like");
        }
    }

    @OnClick(R.id.btn_ask_enabled_wrapper)
    @SuppressWarnings("unused")
    void onAsk() {
        boolean isNotOwner = !viewModel.isOwner();
        if (isNotOwner) {
            trackTouch("ask");
            openOfferLine(viewModel.getMyOfferLine());
        }
    }

    @OnClick({R.id.avatar, R.id.owner_name})
    @SuppressWarnings("unused")
    void onAvatarClicked() {
        if (viewModel.item != null) {
            viewProfile(viewModel.item.getOwner());
        }
    }

    @OnClick(R.id.item_owner_reviews_star)
    void onUserReputationClicked() {
        if (viewModel.item == null) {
            return;
        }

        startActivity(new Intent(this, ReviewListActivity.class)
                .putExtra(ACT_PARA_USER_ID, viewModel.item.getOwner().getId())
                .putExtra(ACT_PARA_SELLER_RATE_COUNT, viewModel.item.getReviewNum())
                .putExtra(ACT_PARA_SELLER_RATE_AVG, viewModel.item.getReviewScore()));
    }

    @OnClick({R.id.item_location_wrapper})
    @SuppressWarnings("unused")
    void onLocationClicked() {
        viewLocation();
        trackTouch("map");
    }

    @OnClick(R.id.btn_play_audio)
    @SuppressWarnings("unused")
    void onAudioButtonClicked() {
        playOrPauseAudio();
    }

    @OnCheckedChanged(R.id.rdb_similar_items)
    @SuppressWarnings("unused")
    void onSimilarItemsTab(CompoundButton v, boolean checked) {
        if (checked) {
            viewModel.getSimilarItems();
            trackTouch("similar");
        }
    }

    @OnCheckedChanged(R.id.rdb_other_items)
    @SuppressWarnings("unused")
    void onOtherItemsTab(CompoundButton v, boolean checked) {
        if (checked) {
            viewModel.getOtherItemsOfSeller();
            trackTouch("seller's other");
        }
    }

    @OnClick(R.id.top_view_share)
    @SuppressWarnings("unused")
    void onShare() {
        viewModel.prepareShareContent(FiveMilesApp.appCtx);     // Branch中使用getApplicationContext()
    }

    @OnClick(R.id.item_brand)
    @SuppressWarnings("unused")
    void onBrandClicked() {
        viewBrand();
        trackTouch("product_brand");
    }

    @OnClick(R.id.top_view_back)
    @SuppressWarnings("unused")
    void onNaviBack() {
        finish();
        trackTouch("product_back");
    }

    @OnClick(R.id.report_wrapper)
    @SuppressWarnings("unused")
    void onReport() {
        reportItem();
    }

    private void showItemThumb() {
        if (itemThumb == null) {
            return;
        }

        showBasicInfo(itemThumb);

        ImageInfo thumbImg = itemThumb.getThumbImage();
        if (thumbImg != null) {
            ImageLoader.getInstance().loadImage(thumbImg.getUrl(),
                    new ImageSize(thumbImg.getWidth(), thumbImg.getHeight()),
                    new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            initPicPagerWithThumb(loadedImage);
                            layoutPicPager();
                        }
                    });
        }
    }

    private void onItemUpdated(Item item) {
        if (item == null) {
            L.d("item is null when updated");
            DisplayUtils.toast(R.string.toast_fail_load_item_details);
            return;
        }

        onItemUpdateSuccess(item);

        showBasicInfo(item);
        showText(txtDesc, item.getDescription());
        showText(txtOwner, item.getOwner().getNickname());

        updateMapView(item);
        renderItemState(item);
        renderNewTag(item);

        renderPriceByState(item);
        renderButtonRow(item);
        renderDelivery(item);
        renderUpdateTime(item);

        if (isNotEmpty(item.getBrand())) {
            showText(txtBrand, item.getBrand().trim() + " >");
            brandWrapper.setVisibility(View.VISIBLE);
        } else {
            brandWrapper.setVisibility(View.GONE);
        }

        btnShare.setVisibility(View.VISIBLE);
        renderReportDeleteButtons(item);        // report seller&item or delete

        showOffers(item);
        renderAvatar(item);
        updateLiked(item);
        updatePicPager(item);
        renderAudioPlayer(item);

        // load data async
        if (rdbSimliarItems.isChecked()) {
            viewModel.getSimilarItems();
        } else {
            viewModel.getOtherItemsOfSeller();
        }

        viewModel.getLikers();

        if (itemThumb != null) {
            viewModel.item.setThumbImage(itemThumb.getThumbImage());
        }
        itemThumb = null;
        thumbBitmap = null;

        showVerifyAndRenewEdu();

        if (reviewEnabled) {
            DisplayUtils.showStars(item.getReviewScore(), itemOwnerReviewsStar);
        }
    }


    private void renderReportDeleteButtons(Item item) {
        // contents according to ownership
        if (item.getState() == SOLD){
            reportDivider.setVisibility(View.GONE);
            lytReportWrapper.setVisibility(View.GONE);
        } else {                                            // 显示report || delete
            if (viewModel.isOwner()) {
                reportDivider.setVisibility(View.GONE);
                lytReportWrapper.setVisibility(View.GONE);
            } else {
                reportDivider.setVisibility(View.VISIBLE);
                lytReportWrapper.setVisibility(View.VISIBLE);
                itmReport.setVisibility(View.VISIBLE);
            }
        }
    }

    private void showBasicInfo(final ItemThumb item) {
        showText(txtTitle, item.getTitle());
        renderDistance(item);
    }

    private void onItemUpdateSuccess(Item item) {
        if (!TextUtils.equals(itemId, item.getId())) {
            // new item, tracking page view
            TrackingUtils.trackView(getScreenName());
            releaseAudioPlayer();
        }
        itemId = item.getId();
        currItemId = item.getId();

        swipeRefreshLayout.setRefreshing(false);
    }

    private void onItemUpdateFailed() {
        swipeRefreshLayout.setRefreshing(false);
    }

    private void renderItemState(ItemThumb item) {

        switch (item.getState()) {
            case LISTING:
                icItemSold.setVisibility(View.GONE);
                txtOtherState.setVisibility(View.GONE);
                break;
            case SOLD:
                icItemSold.setVisibility(View.VISIBLE);
                txtOtherState.setVisibility(View.GONE);
                break;
            case RECEIVED:
            case SELLER_RATED:
                L.w("RECEIVED or SELLER_RATED is abandoned and should not show here");
                break;
            case UNAPPROVED:
                icItemSold.setVisibility(View.GONE);
                txtOtherState.setVisibility(View.VISIBLE);
                txtOtherState.setText(R.string.ic_item_state_unapproved);
                break;
            case UNAVAILABLE:
                icItemSold.setVisibility(View.GONE);
                txtOtherState.setVisibility(View.VISIBLE);
                txtOtherState.setText(R.string.ic_item_state_unavailable);
                break;
            case UNLISTED:
                icItemSold.setVisibility(View.GONE);
                txtOtherState.setVisibility(View.VISIBLE);
                txtOtherState.setText(R.string.ic_item_state_unlisted);
                break;
            default:
                icItemSold.setVisibility(View.GONE);
                txtOtherState.setVisibility(View.GONE);
                break;
        }
    }

    private void renderNewTag(ItemThumb item) {
        imgNewTag.setVisibility(item.isNew() ? View.VISIBLE : View.GONE);
    }

    private void renderPriceByState(ItemThumb itemThumb) {
        showPrices(itemThumb);
    }

    /**
     * render renew & edit for seller, buy & offer for buyer
     * 买家看不到UNAVAILABLE, UNAPPROVE, UNLISTED
     */
    private void renderButtonRow(Item item) {
        if(item == null) {
            return;
        }

        if(viewModel.isOwner()) {
            // renew & edit
            //update:当产品sold时,展示一致.
            renewEditButtonWrapper.setVisibility(View.VISIBLE);
            likeBuyAskButtonWrapper.setVisibility(View.GONE);

            renderRenewButtonByState(item);
            // Edit button 都可以点击，用于delete
        } else {
            if(item.isSold()) {
                // sold state
                // update: 当买家访问已买商品,显示like+buy+ask,后两者不可点击,like有正常动作.
                renewEditButtonWrapper.setVisibility(View.GONE);
                likeBuyAskButtonWrapper.setVisibility(View.VISIBLE);
                renderBuyButtonByState(item);
                renderAskButtonByState(item);
            } else {
                // buy & ask
                renewEditButtonWrapper.setVisibility(View.GONE);
                likeBuyAskButtonWrapper.setVisibility(View.VISIBLE);

                renderBuyButtonByState(item);
                renderAskButtonByState(item);
            }
        }
    }

    private void renderAskButtonByState(ItemThumb itemThumb) {
        if (itemThumb == null){
            L.d("itemThumb is null");
            return;
        }

        if (itemThumb.getState() == EnumItemState.LISTING){
            lytAskEnabled.setVisibility(View.VISIBLE);
            lytAskDisabled.setVisibility(View.GONE);
        } else {
            lytAskEnabled.setVisibility(View.GONE);
            lytAskDisabled.setVisibility(View.VISIBLE);
        }
    }

    private void renderBuyButtonByState(ItemThumb itemThumb) {
        if (itemThumb == null) {
            L.d("itemThumb is null");
            return;
        }

        if (itemThumb.getState() == EnumItemState.LISTING){
            txtBuyEnabled.setVisibility(View.VISIBLE);
            txtBuyDisabled.setVisibility(View.GONE);
        } else {
            txtBuyEnabled.setVisibility(View.GONE);
            txtBuyDisabled.setVisibility(View.VISIBLE);
        }
    }

    /**
     * UNAPPROVED, UNAVAILABLE update按钮显示为灰色，LISTIING和UNLISTED根据ttl显示倒计时或Renew
     * @param item ItemThumb对象
     */
    private void renderRenewButtonByState(Item item) {
        if (EnumItemState.LISTING == item.getState() || EnumItemState.UNLISTED == item.getState()){     // 只有处于listing和unlisted状态的商品才可以被编辑
            long renewTtl = item.getRenewTtl();

            if (renewTtl > 0) {
                txtRenewButton.setVisibility(View.GONE);
                txtRenewButtonDisabled.setVisibility(View.GONE);
                lytUpdateTimeWrapper.setVisibility(View.VISIBLE);
                startUpdateCountDown(renewTtl);              // 开始倒计时
            } else {
                txtRenewButton.setVisibility(View.VISIBLE);
                txtRenewButtonDisabled.setVisibility(View.GONE);
                lytUpdateTimeWrapper.setVisibility(View.GONE);
            }
        } else {
            txtRenewButton.setVisibility(View.GONE);
            txtRenewButtonDisabled.setVisibility(View.VISIBLE);
            lytUpdateTimeWrapper.setVisibility(View.GONE);
        }

    }

    private void showPrices(ItemThumb item) {
        showText(txtPrice, formatCurrency(item.getCurrencyCode(), item.getPrice()));

        if (isValidPrice(item.getOriginPrice())) {
            showText(txtOriginPrice, formatCurrency(item.getCurrencyCode(), item.getOriginPrice()));
        } else {
            txtOriginPrice.setVisibility(View.GONE);
        }
    }

    private void updateLiked(Item item) {
        btnLike.setBackgroundResource(item.isLiked() ? R.drawable.square_button_liked :
                R.drawable.square_button_like);
    }

    private void promptLiked(Item item) {
        toast(item.isLiked() ? R.string.item_add_to_likes : R.string.item_rm_from_likes);
    }

    private void updatePicPager(Item item) {
        images.clear();
        for (ImageInfo img : item.getImages()) {
            images.add(new IMAGES(img));
        }

        initPicPager();  // pagerAdapter#notifyDataSetChanged not work
        layoutPicPager();
    }

    private void renderAvatar(Item item) {
        DisplayUtils.showAvatar(imgAvatar,
                item.getOwner(),
                getResources().getDimensionPixelSize(R.dimen.item_avatar_size),
                avatarImgOpts);
    }

    private void openOfferLine(OfferLine offerLine) {
        if (viewModel.item == null) {
            return;
        }

        Intent intent = new Intent(this, MakeOfferActivity.class);

        if (offerLine != null) {
            intent.putExtra(ACT_PARA_OFFER_LINE_ID, offerLine.getId());
        }

        intent.putExtra(ACT_PARA_ITEM, viewModel.item);

        startActivity(intent);
        trackTouch("offering");
    }

    private void renderDistance(ItemThumb item) {
        txtLocation.setText(LocationUtils.formatItemLocationWithDistance(item));
    }

    private void updateMapView(ItemThumb item) {
        if (item != null && item.getLocation() != null && itemMap != null) {
            Location itemLocation = item.getLocation();
            itemMap.clear();
            locationWrapper.setVisibility(View.VISIBLE);
            // item location
            LatLng itemLatLng = new LatLng(itemLocation.getLatitude(), itemLocation.getLongitude());
            showItemOnMap(itemLatLng, computeItemRadius(itemLatLng));

            // current user location
            LatLng userLatLng = new LatLng(getLatitudeInSession(), getLongitudeInSession());
            itemMap.addMarker(new MarkerOptions()
                    .position(userLatLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_item_location))
                    .draggable(false));

            final LatLngBounds bounds = LocationUtils.calcLatLngBounds(itemLatLng, userLatLng, MAP_PADDING_RADIUS);
            L.d("moving camera to %s", bounds);

            int padding = FiveMilesApp.appCtx.getResources().getDimensionPixelOffset(R.dimen.item_map_padding);
            itemMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
        }
    }

    private void renderDelivery(Item item) {
        Delivery delivery = Deliveries.getDelivery(this, item.getDeliveryType());
        txtDelivery.setText(delivery.getName());
    }

    private void renderUpdateTime(Item item) {
        long lastUpdateTime = item.getLastUpdated();
        renderUpdateTime(lastUpdateTime);
    }

    // TODO move to utils
    private void renderUpdateTime(long updateTime) {
        long currentTime = Calendar.getInstance().getTimeInMillis() / SECOND_IN_MILLS;
        long interval = currentTime - updateTime;
        if (interval < 0) {
            L.w("update time is larger than current time");
        }

        String updateTimeStr;
        if (interval < MINUTE_IN_SECOND) {
            updateTimeStr = getString(R.string.time_elapse_now);
        } else if (interval < HOUR_IN_SECOND) {
            long minutes = interval / MINUTE_IN_SECOND;
            updateTimeStr = getString(R.string.update_time_minutes_ago, minutes);
        } else if (interval < DAY_IN_SECOND) {
            long hours = interval / HOUR_IN_SECOND;
            updateTimeStr = getString(R.string.update_time_hours_ago, hours);
        } else if (interval < MONTH_IN_SECOND) {
            long days = interval / DAY_IN_SECOND;
            updateTimeStr = getString(R.string.update_time_days_ago, days);
        } else {
            long months = interval / MONTH_IN_SECOND;
            updateTimeStr = getString(R.string.update_time_months_ago, months);
        }
        txtUpdateTime.setText(updateTimeStr);
    }

    private void viewProfile(User user) {
        if (user == null) {
            return;
        }

        Intent intent;
        if (ModelUtils.isOwner(user.getId())) {
            intent = new Intent(this, TabProfileActivity.class);
            intent.putExtra(ACT_PARA_CAN_GO_BACK, true);
            startActivity(intent);
        } else {
            intent = new Intent(this, OtherProfileActivity.class);
            intent.putExtra(ACT_PARA_USER_ID, user.getId());
            startActivity(intent);
        }
        trackTouch("product_seller");
    }

    private void viewLocation() {
        if (viewModel.item == null) {
            return;
        }

        // 不直接用Activity Context，防止内存泄漏
        Intent intent = new Intent(FiveMilesApp.appCtx, ItemLocationMapActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ACT_PARA_ITEM, viewModel.item);
        FiveMilesApp.appCtx.startActivity(intent);
        trackTouch("map");
    }

    private void showOffers(Item item) {
        List<OfferLine> offers = item.getOfferLines();
        offerWrapper.setVisibility(offers.isEmpty() ? View.GONE : View.VISIBLE);
        offersContainer.removeAllViews();

        for (int i = 0; i < offers.size(); i++) {
            OfferLine offer = offers.get(i);
            View v = renderOfferLine(offer, viewModel.isOwner() || isMine(offer), i < offers.size() - 1);
            if (v != null) {
                offersContainer.addView(v);
            }
        }
    }

    private View renderOfferLine(final OfferLine offer, boolean canClick, boolean showSeparator) {
        if (offer == null) {
            return null;
        }

        View v = getLayoutInflater().inflate(R.layout.offer_line_item, offersContainer, false);
        TextView txtDesc = (TextView) v.findViewById(R.id.offer_desc);
        txtDesc.setText(offer.getDescription());
        v.findViewById(R.id.sep_offer).setVisibility(showSeparator ? View.VISIBLE : View.GONE);
        v.findViewById(R.id.ic_more).setVisibility(canClick ? View.VISIBLE : View.GONE);

        if (canClick) {
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openOfferLine(offer);
                }
            });
        }
        return v;
    }

    private void renderAudioPlayer(Item item) {
        // 如果有音频资源，则预先下载
        if (isNotEmpty(item.getMediaLink())) {
            setPlaybackEnabled(false);
            txtAudioDuration.setText("0''");
            txtAudioElapse.setText("0''");
            audioWrapper.setVisibility(View.VISIBLE);
            viewModel.downloadAudio();
        } else {
            audioWrapper.setVisibility(View.GONE);
        }
    }

    private void initAudioLoadingAnimation() {
        audioLoadingAnimation = ValueAnimator.ofInt(0x80, 0xe6);  // alpha of playback button, 50%~90%
        audioLoadingAnimation.setDuration(750);
        audioLoadingAnimation.setRepeatCount(ValueAnimator.INFINITE);
        audioLoadingAnimation.setRepeatMode(ValueAnimator.REVERSE);
        audioLoadingAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                int alpha = (Integer) animator.getAnimatedValue();
                btnPlayAudio.getBackground().setAlpha(alpha);
            }
        });
    }

    private void setPlaybackEnabled(boolean enabled) {
        btnPlayAudio.setEnabled(enabled);

        if (!enabled) {
            if (audioLoadingAnimation == null) {
                initAudioLoadingAnimation();
            }
            audioLoadingAnimation.start();
        } else {
            audioLoadingAnimation.cancel();
            btnPlayAudio.getBackground().setAlpha(0xff);
        }
    }

    private void playOrPauseAudio() {
        if (audioPlayer == null) {
            return;
        }

        if (audioPlayer.isPlaying()) {
            audioPlayer.pause();
            btnPlayAudio.setBackgroundResource(R.drawable.square_button_play);
            trackTouch("record_stop");
        } else {
            audioPlayer.start();
            audioWatcher.start();
            btnPlayAudio.setBackgroundResource(R.drawable.square_button_playing);
            trackTouch("record_play");
        }
    }

    private void initAudioPlayer(File file) {
        if (file == null || !file.exists()) {
            return;
        }

        try {
            if (audioPlayer == null) {
                audioPlayer = new MediaPlayer();

                audioWatcher = new AudioPlaybackWatcher(audioPlayer,
                        progAudioPlayback,
                        txtAudioElapse,
                        btnPlayAudio);
            } else {
                audioPlayer.reset();
            }

            audioPlayer.setDataSource(file.getAbsolutePath());
            audioPlayer.prepare();

            int dur = Math.min(audioPlayer.getDuration() / 1000, 60);  // 最多60秒
            txtAudioDuration.setText(dur + "''");
            txtAudioElapse.setText("0''");

            L.d("audio downloaded, duration: %d", dur);
            setPlaybackEnabled(true);
        } catch (Exception e) {
            L.e(e);
        }
    }

    private void releaseAudioPlayer() {
        if (audioWatcher != null) {
            audioWatcher.reset();
        }

        try {
            if (audioPlayer != null) {
                audioPlayer.release();
                audioPlayer = null;
            }

        } catch (Exception e) {
            L.e(e);
        }

        progAudioPlayback.setProgress(0);
        btnPlayAudio.setBackgroundResource(R.drawable.square_button_play);
    }

    private void clearLikerList() {
        likerList.removeAllViews();
        likerWrapper.setVisibility(View.GONE);
    }

    private void showLikers(GetLikersResp likersResp) {
        clearLikerList();
        if (likersResp == null || likersResp.getUsers() == null || likersResp.getUsers().isEmpty()) {
            return;
        }

        likerWrapper.setVisibility(View.VISIBLE);
        if (likersResp.getMeta() != null) {
            int totalCount = likersResp.getMeta().getTotalCount();
            txtLikerCount.setText(String.valueOf(totalCount));
        }

        List<User> users = likersResp.getUsers();
        for (User u : users) {
            View v = renderLiker(u);
            if (v != null) {
                likerList.addView(v);
            }
        }
    }

    private View renderLiker(final User user) {
        View v = getLayoutInflater().inflate(R.layout.item_liker_list_item, likerList, false);

        AvatarView avatar = (AvatarView) v.findViewById(R.id.avatar);
        DisplayUtils.showAvatar(avatar,
                user,
                getResources().getDimensionPixelSize(R.dimen.item_liker_avatar_size),
                avatarImgOpts);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewProfile(user);
                trackTouch("like_avatar");
            }
        });
        return v;
    }

    private void showRelatedItems(List<ItemThumb> items) {
        // 取消隐藏逻辑，避免因为卖家没有其他商品而隐藏推荐商品
        relatedItemTab.setVisibility(View.VISIBLE);

        relatedItemLayout.removeAllViews();
        for (ItemThumb item : items) {
            View v = renderRelatedItem(item);
            if (v != null) {
                relatedItemLayout.addView(v);
            }
        }
    }

    private View renderRelatedItem(final ItemThumb item) {
        ItemThumbRenderer renderer = new ItemThumbRenderer();
        renderer.onCreate(item, getLayoutInflater(), relatedItemLayout);
        renderer.render();
        View v = renderer.getRootView();

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ItemActivity.this, ItemActivity.class);
                intent.putExtra(ACT_PARA_ITEM_THUMB, item);
                startActivity(intent);

                if (rdbSimliarItems.isChecked()) {
                    trackTouch("similar_product");
                } else {
                    trackTouch("seller's_other_product");
                }
            }
        });

        return v;
    }

    private void reportItem() {
        if (isEmpty(itemId)) {
            return;
        }

        Intent intent = new Intent(this, ReportActivity.class);
        intent.putExtra(ACT_PARA_REPORT_TYPE, ReportRepository.REPORT_TYPE_ITEM);
        intent.putExtra(ACT_PARA_REPORTED_OID, itemId);
        startActivity(intent);
        trackTouch("reporitem");
    }

    private void shareItem(Pair<String, String> data) {
        if (viewModel.item == null) {
            return;
        }

        Item item = viewModel.item;

        String url = isNotEmpty(data.first) ? data.first : "";
        String city = isNotEmpty(data.second) ? data.second : "my city";

        String title = item.getTitle();
        String price = formatCurrency(item.getCurrencyCode(), item.getPrice());

        Intent intent = new Intent(Intent.ACTION_SEND); // 启动分享发送的属性
        intent.setType("text/plain"); // 分享发送的数据类型

        String content = viewModel.isOwner() ? getString(R.string.share_item, city, title, price, url) :
                getString(R.string.share_others_item, title, price, url);

        intent.putExtra(Intent.EXTRA_TEXT, content); // 分享的内容
        startActivity(Intent.createChooser(intent, getString(R.string.share_title)));// 目标应用选择对话框的标题
        trackTouch("share_product");
    }

    // explore the given brand
    private void viewBrand() {
        if (viewModel.item == null) {
            return;
        }

        if (viewModel.item.getBrandId() > 0) {
            Intent intent = new Intent(this, SearchResultActivity.class);
            intent.putExtra(ACT_PARA_S_BRAND_ID, viewModel.item.getBrandId());
            intent.putExtra(ACT_PARA_S_BRAND_NAME, viewModel.item.getBrand());
            if (getIntent() != null && getIntent().hasExtra(ACT_PARA_RF_TAG)) {
                intent.putExtra(ACT_PARA_RF_TAG, getIntent().getStringExtra(ACT_PARA_RF_TAG));
            }
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_buy_enabled)
    @SuppressWarnings("unused")
    void onBuyItem() {
        fbEventsLogger.logEvent("make_offer");

        int myOfferLineId = 0;
        OfferLine myOfferLine = viewModel.getMyOfferLine();
        if (myOfferLine != null) {
            myOfferLineId = myOfferLine.getId();
        }

        startActivity(new Intent(this, BuyItemActivity.class)
                .putExtra(ACT_PARA_ITEM, viewModel.item)
                .putExtra(ACT_PARA_OFFER_LINE_ID, myOfferLineId));

        trackTouch("buy");
    }

    @OnClick(R.id.btn_renew_enabled)
    @SuppressWarnings("unused")
    void onRenew() {
        viewModel.renewItem();
    }

    private void startUpdateCountDown(final long timeInSeconds) {
        if (timeInSeconds < 0) {
            return;
        }
        L.d("start update count down for " + timeInSeconds + " seconds.");

        txtRenewButton.setVisibility(View.GONE);
        lytUpdateTimeWrapper.setVisibility(View.VISIBLE);
        viewCountDown.setTimeInMills(timeInSeconds * SECOND_IN_MILLS, true);

        int timeSecondsIntValue;
        if (timeInSeconds > Integer.MAX_VALUE) {
            timeSecondsIntValue = Integer.MAX_VALUE;
        } else {
            timeSecondsIntValue = (int) timeInSeconds;
        }

        subsUpdateCountDown = Observable.interval(1, TimeUnit.SECONDS)
                .take(timeSecondsIntValue)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<Long>() {
                    @Override
                    public void onNext(Long seconds) {
                        int countDown = (int) (timeInSeconds - seconds);
                        viewCountDown.setTimeInSeconds(countDown, true);
                    }

                    @Override
                    public void onCompleted() {
                        lytUpdateTimeWrapper.setVisibility(View.GONE);
                        txtRenewButton.setVisibility(View.VISIBLE);
                        subsUpdateCountDown = null;
                        L.d("finish update count down.");
                    }
                });
    }

    @Override
    public void onBackPressed() {
        if (editDlgWrapper != null && View.VISIBLE == editDlgWrapper.getVisibility()) {
            editDlgWrapper.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }
}