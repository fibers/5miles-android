package com.thirdrock.fivemiles.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.thirdrock.domain.Country;
import com.thirdrock.domain.CountryList;
import com.thirdrock.domain.CountryList__JsonHelper;
import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_APP_STATE;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;

/**
 * Countries data helper
 * Created by ywu on 15/1/9.
 */
public class Countries {
    public static final String PREF_KEY_COUNTRY_CODE = "country";

    private final Context context;
    private final ArrayList<Country> countries = new ArrayList<>();
    private final Map<String, Country> countryMap = new HashMap<>();
    private Country defaultCountry;

    @Inject
    public Countries(Context context) {
        this.context = context;
        loadCountryList();
        loadDefaultCountry();
    }

    private void loadCountryList() {
        try {
            String content = FileUtils.readFileContent(
                    context.getResources().openRawResource(R.raw.country_code));
            CountryList countryList = CountryList__JsonHelper.parseFromJson(content);

            countries.addAll(countryList.getCountries());
            for (Country country : countries) {
                countryMap.put(country.getCountryCode(), country);
            }
        } catch (Exception e) {
            L.e("load raw country list failed", e);
        }
    }

    private void loadDefaultCountry() {
        Country country = getSavedCountry();
        if (country == null) {
            country = getCountryBySysLocale();
        }

        defaultCountry = country != null ? country : getCountryByCode("US");
    }

    private Country getSavedCountry() {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_APP_STATE,
                Context.MODE_PRIVATE);
        String countryCode = prefs.getString(PREF_KEY_COUNTRY_CODE, null);
        if (isNotEmpty(countryCode)) {
            return countryMap.get(countryCode);
        }

        return null;
    }

    private Country getCountryBySysLocale() {
        try {
            return countryMap.get(context.getResources().getConfiguration().locale.getCountry());
        } catch (Exception e) {
            L.e("get system default country failed", e);
        }

        return null;
    }

    public Country getDefaultCountry() {
        return defaultCountry;
    }

    public void setDefaultCountry(Country country) {
        this.defaultCountry = country;

        if (country != null) {
            context.getSharedPreferences(PREFS_APP_STATE, Context.MODE_PRIVATE)
                    .edit()
                    .putString(PREF_KEY_COUNTRY_CODE, country.getCountryCode())
                    .apply();
        }
    }

    public ArrayList<Country> getCountries() {
        return countries;
    }

    /**
     * @param countryCode uppercase ISO 3166 2-letter code.
     */
    public Country getCountryByCode(String countryCode) {
        return countryMap.get(countryCode);
    }
}
