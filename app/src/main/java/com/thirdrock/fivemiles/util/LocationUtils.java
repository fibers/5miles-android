package com.thirdrock.fivemiles.util;

import android.content.Context;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Message;
import android.text.TextUtils;

import com.external.eventbus.EventBus;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.SphericalUtil;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.domain.GeoLocation;
import com.thirdrock.domain.ILocation;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.di.RepositoryModule;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.Emit1;
import com.thirdrock.protocol.AddressComponent;
import com.thirdrock.protocol.AddressResponse;
import com.thirdrock.protocol.Geometry;
import com.thirdrock.repository.GeoLocationReportRepository;
import com.thirdrock.repository.impl.BodyParserFactoryImpl;
import com.thirdrock.repository.impl.GeoLocationReportRepositoryImpl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.insthub.fivemiles.MessageConstant.GEO_LOCATION_UPDATED;
import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLatitudeInSession;
import static com.insthub.fivemiles.Utils.LocationManagerUtil.getLongitudeInSession;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotAllEmpty;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;


/**
 * Util class handing locations
 * Created by ywu on 14-8-17.
 */
public final class LocationUtils {

    private static final DecimalFormat DIST_FMT_LONG;
    private static final DecimalFormat DIST_FMT_XLONG;
    private static final DecimalFormat DIST_FMT_SHORT;

    public static final double MILE_PER_METER = 0.000621371192;     // 1.0 m == 0.000621371192 miles

    // TODO 改用依赖注入的方式
    static {
        Context ctx = FiveMilesApp.getInstance().getApplicationContext();
        DIST_FMT_LONG = new DecimalFormat(ctx.getString(R.string.distance_fmt_long));
        DIST_FMT_XLONG = new DecimalFormat(ctx.getString(R.string.distance_fmt_xlong));
        DIST_FMT_SHORT = new DecimalFormat(ctx.getString(R.string.distance_fmt_short));
    }

    private LocationUtils() {
    }

    /**
     * @param distInMeters distance in meters
     * @return distance in string
     * TODO use localized distance measures
     */
    public static String formatDistance(double distInMeters) {
        DecimalFormat fmt;
        double distInMiles = distInMeters * MILE_PER_METER;

        if (distInMiles < 0.1) {
            distInMiles = 0.1;
            fmt = DIST_FMT_SHORT;
        } else if (distInMiles < 1.0) {
            fmt = DIST_FMT_SHORT;
        } else if (distInMiles < 10.0) {
            fmt = DIST_FMT_LONG;
        } else {
            fmt = DIST_FMT_XLONG;
        }

        return fmt.format(distInMiles);
    }

    /**
     * Calculate distance in meters and return a formatted string
     *
     * @param latitude  latitude
     * @param longitude longitude
     * @return distance in string
     * TODO use localized distance measures
     */
    public static String formatDistance(double latitude, double longitude) {
        return formatDistance(computeDistance(latitude, longitude));
    }

    public static float computeDistance(double lat, double lon) {
        double currLat = getLatitudeInSession();
        double currLon = getLongitudeInSession();

        float results[] = new float[1];
        Location.distanceBetween(currLat, currLon, lat, lon, results);
        return results[0];
    }

    /**
     * Get city name according to current location
     */
    public static Observable<String> getCity() {
        Locale defaultLocale = Locale.getDefault();
        return getCity(defaultLocale);
    }

    /**
     * Get City name according to current location in designated language
     */
    public static Observable<String> getCity(final Locale pLocale) {
        final Locale locale = (pLocale == null)? Locale.getDefault() : pLocale;

        final Context ctx = FiveMilesApp.getInstance();
        return Observable.create(new Emit1<String>() {
            @Override
            protected String call() throws Exception {
                Geocoder geocoder = new Geocoder(ctx, locale);
                String city = null;

                try {
                    List<Address> addresses = geocoder.getFromLocation(getLatitudeInSession(), getLongitudeInSession(), 1);
                    if (addresses.size() > 0) {
                        city = addresses.get(0).getLocality();
                    }
                } catch (Exception e) {
                    L.e(e);
                }
                return city;
            }
        });
    }

    public static void registUserLocation(GeoLocation location){
        geoRep.updateLocation(location)
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    static GeoLocationReportRepository geoRep = new GeoLocationReportRepositoryImpl(
            RepositoryModule.getBaseUrl(),
            RepositoryModule.getDefaultHeaderBuilder(),
            BodyParserFactoryImpl.getInstance());

    public static void getGeolocationByGPS() {
        SESSION session = SESSION.getInstance();
        if(TextUtils.isEmpty(session.getCity())&&TextUtils.isEmpty(session.getRegion())&&TextUtils.isEmpty(session.getCountry())){
            getRemoteGeoLocation(GEO_LOCATION_UPDATED);
        }else{
            GeoLocation geolocation = new GeoLocation();
            geolocation.setCity(session.getCity());
            geolocation.setRegion(session.getRegion());
            geolocation.setCountry(session.getCountry());
            geolocation.setLatitude(session.getLatitude());
            geolocation.setLongitude(session.getLongitude());
            Message msg = Message.obtain();
            msg.what = GEO_LOCATION_UPDATED;
            msg.obj = geolocation;
            EventBus.getDefault().post(msg);
        }


    }

    public static Observable<AddressResponse> getGeolocationByZipcode(final String zipcode) {

        return geoRep.getAddressFromGoogle(zipcode);

    }

    /**
     * Parse Google address response. if the request is for zipcode, then latitude and longitude set 0.
     */
    public static GeoLocation parseAddressResponse(AddressResponse response, float latitude, float longitude) {
        GeoLocation geoLocation = new GeoLocation();

        if(!response.results.isEmpty()) {

            Locale locale = Locale.getDefault();
            String country = locale.getCountry();
            ArrayList<com.thirdrock.protocol.Address> results = response.results;
            ArrayList<AddressComponent> preferAddress = null, defaultAddress = null;
            Geometry preferGeometry = null, defaultGeometry = null;

            for(int i = 0, length = results.size(); i < length; i++) {
                com.thirdrock.protocol.Address address = results.get(i);
                AddressComponent countryComponent = address.address_components.get(address.address_components.size() - 1);
                if(countryComponent.short_name.equals(country)) {
                    preferAddress = address.address_components;
                    preferGeometry = address.geometry;
                    break;
                } else if(isPriorityCountry(countryComponent.short_name) || i == 0) {
                    defaultAddress = address.address_components;
                    defaultGeometry = address.geometry;
                }
            }

            if(preferAddress == null && defaultAddress != null) {
                preferAddress = defaultAddress;
                preferGeometry = defaultGeometry;
            }

            if (preferAddress != null) {
                for(AddressComponent addComponent : preferAddress) {
                    String type = addComponent.types.get(0);
                    switch (type) {
                        case "administrative_area_level_1":
                            geoLocation.setRegion(addComponent.short_name);
                            break;
                        case "country":
                            geoLocation.setCountry(addComponent.long_name);
                            break;
                        case "locality":
                            geoLocation.setCity(addComponent.long_name);
                            break;
                    }
                }
            }

            float realLatitude, realLongitude;
            if (latitude == 0 && preferGeometry != null && preferGeometry.location != null) {
                realLatitude = preferGeometry.location.lat;
            } else {
                realLatitude = latitude;
            }

            if (longitude == 0 && preferGeometry != null && preferGeometry.location != null) {
                realLongitude = preferGeometry.location.lng;
            } else {
                realLongitude = longitude;
            }

            geoLocation.setLatitude(realLatitude);
            geoLocation.setLongitude(realLongitude);

            SESSION session = SESSION.getInstance();
            session.setCity(geoLocation.getCity());
            session.setCountry(geoLocation.getCountry());
            session.setRegion(geoLocation.getRegion());
            session.setLatitude(realLatitude);
            session.setLongitude(realLongitude);
            session.save();
            return geoLocation;
        } else {
            return null;
        }

    }

    private static String currentPriorityCountry;

    private static boolean isPriorityCountry(String destination) {
        String[] countryList = {"US", "CA", "BR", "AU", "ES", "MX", "IN"};
        for(String country : countryList) {

            if(destination.equals(country) || country.equals(currentPriorityCountry)) {

                currentPriorityCountry = destination;
                return true;
            }
        }
        return false;
    }

    private static void getRemoteGeoLocation(final int type) {

        final float latitude = getLatitudeInSession();
        final float longitude = getLongitudeInSession();
        if(latitude == 0 && longitude == 0) return;

        //TODO: 需要将相关操作放到activity中.
        Subscriber<AddressResponse> subscriber = new Subscriber<AddressResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                L.e("get Remote GEO Location failed ");
            }

            @Override
            public void onNext(AddressResponse addressResponse) {
                GeoLocation geoLocation = parseAddressResponse(addressResponse, latitude, longitude);

                Message msg = Message.obtain();
                msg.what = type;
                msg.obj = geoLocation;
                EventBus.getDefault().post(msg);

            }
        };

        geoRep.getAddressFromGoogle(latitude, longitude)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(subscriber);

    }

    public static String getDisplayLocation(String country, String region, String city) {
        ArrayList<String> location = new ArrayList<>();
        String result = "";
        if(city != null && !city.equals("")) location.add(city);
        if(region != null && !region.equals("")) location.add(region);
        if(country != null && !country.equals("")) location.add(country);
        Iterator<String> iLocation = location.iterator();
        int index =0;
        if(iLocation.hasNext()){
            result+=iLocation.next();
            index =1;
        }
        while(iLocation.hasNext()&&index<2){
            result+=",";
            result+=iLocation.next();
            index++;
        }
        return result;
    }

    /**
     * Get a human readable form for the given location
     *
     * @param country  country
     * @param province state/region/province
     * @param city     city name
     * @return human readable location
     */
    public static String formatLocation(String country, String province, String city) {
        Context ctx = FiveMilesApp.getInstance();
        if (isNotEmpty(province, city)) {
            return ctx.getString(R.string.location_city_state, city, province);
        } else if (isNotEmpty(city)) {
            return city;
        } else {
            return isNotEmpty(country) ? country : "";
        }
    }

    /**
     * Get the human readable location of the given item
     *
     * @param item item
     * @return human readable location
     */
    public static String formatItemLocation(ItemThumb item) {
        if (item == null) {
            return "";
        }

        com.thirdrock.domain.Location location = item.getLocation();

        if (hasAddress(item)) {
            return formatLocation(item.getCountry(), item.getProvince(), item.getCity());
        } else if (isValidLocation(location)) {
            float distance = LocationUtils.computeDistance(location.getLatitude(),
                    location.getLongitude());
            return formatDistance(distance);
        } else {
            return "";
        }
    }

    public static String formatItemLocationWithDistance(ItemThumb item) {
        com.thirdrock.domain.Location location = item.getLocation();
        float distance = computeDistance(location.getLatitude(), location.getLongitude());
        String city = formatLocation(item.getCountry(), item.getProvince(), item.getCity());

        return formatDistance(distance) + (isNotEmpty(city) ? ", " + city : "");
    }

    public static String formatLocationWithDistance(com.thirdrock.domain.Location location, String place) {
        float distance = -1.0f;
        if (location != null) {
            distance = computeDistance(location.getLatitude(), location.getLongitude());
        }
        String distanceStr = distance > 0? formatDistance(distance) : "";

        return distanceStr + (isNotEmpty(place)? ", " + place  : "");
    }

    /**
     * Calculate the distance between current and target location, then get whether or not the distance is
     * in bound
     * @param pLoc target location
     * @param pBound bound in miles
     * @return true if the distance between current and target location is in bounds
     */
    public static boolean isLocationInBound(ILocation pLoc, double pBound) {
        if (pLoc != null) {
            double distance = computeDistance(pLoc.getLatitude(), pLoc.getLongitude());
            double distanceInMiles = distance * MILE_PER_METER;
            return distanceInMiles < pBound;
        }
        return false;
    }

    public static boolean isValidLocation(com.thirdrock.domain.Location loc) {
        return loc != null && (loc.getLatitude() > 0 || loc.getLongitude() > 0);
    }

    /*
     * Whether the item has human readable location information
     */
    private static boolean hasAddress(ItemThumb item) {
        return item != null && isNotAllEmpty(item.getCity(), item.getProvince(), item.getCountry());
    }

    /**
     * Create a bounds contains the two given locations
     *
     * @param l1 location 1
     * @param l2 location 2
     * @param paddingMeter minimum padding of the map in meters
     */
    public static LatLngBounds calcLatLngBounds(LatLng l1, LatLng l2, double paddingMeter) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        if (l1 != null) {
            includeBounds(l1, paddingMeter, builder);
        }
        if (l2 != null) {
            includeBounds(l2, paddingMeter, builder);
        }
        return builder.build();
    }

    /**
     * Create a bounds contains the two given locations
     *
     * @param l1 location 1
     * @param radiusMeter radius in meters
     */
    public static LatLngBounds calcLatLngBounds(LatLng l1, double radiusMeter) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        includeBounds(l1, radiusMeter, builder);
        return builder.build();
    }

    private static void includeBounds(LatLng point, double radiusMeter, LatLngBounds.Builder builder) {
        if (point == null || builder == null) {
            return;
        }
        // calc borders
        LatLng n = findCoordinate(point, radiusMeter, 0);
        LatLng e = findCoordinate(point, radiusMeter, 90);
        LatLng s = findCoordinate(point, radiusMeter, 180);
        LatLng w = findCoordinate(point, radiusMeter, 270);

        builder.include(n).include(e).include(s).include(w);
    }

    /**
     * TODO 在Location类里直接转为LatLng，需要在domain里面引用google service
     * 将domain的Location类转为google play service
     * @param pLocation domain类的位置
     * @return Google Play service中的LatLng
     */
    public static LatLng locationToLatLng(com.thirdrock.domain.Location pLocation) {
        if (pLocation == null) {
            L.w("pLoation is null in loactionToLatLng");
            return null;
        }

        return new LatLng(pLocation.getLatitude(), pLocation.getLongitude());
    }

    /**
     * 给定一个原点，半径，和角度（正北顺时针偏转角度），计算出其坐标。
     * <p/>
     * 原理是分别计算出给定向量在经纬度方向上的分量，并折算为偏转角度，从而得出新的坐标。
     * 此处计算使用平面三角做近似计算，不考虑球面因素，并按地球为完美球体计算。
     *
     * @param origin [Object] 原点坐标
     * @param radius [Number] 辐射半径，单位：米
     * @param angle  [偏转角度] 正北顺时针偏转角度，单位：度数 0~360
     * @return 终点的坐标值
     */
    public static LatLng findCoordinate(LatLng origin, double radius, double angle) {
        if (origin != null) {
            return SphericalUtil.computeOffset(origin, radius, angle);
        }
        return null;
    }

    public static double calcDistanceOnScreen(Point p1, Point p2) {
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }
}
