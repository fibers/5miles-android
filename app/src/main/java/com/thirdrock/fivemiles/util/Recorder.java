package com.thirdrock.fivemiles.util;

/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.insthub.fivemiles.FiveMilesAppConst;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.view.AudioProgressView;
import com.thirdrock.framework.util.L;

import java.io.File;
import java.io.IOException;

/**
 * Recoder
 * Created by 昆鹏 on 2014/8/20.
 */
public class Recorder implements OnCompletionListener, OnErrorListener {
    static final String SAMPLE_PREFIX = "recording";
    static final String SAMPLE_PATH_KEY = "sample_path";
    static final String SAMPLE_LENGTH_KEY = "sample_length";

    public static final int IDLE_STATE = 0;
    public static final int RECORDING_STATE = 1;
    public static final int PLAYING_STATE = 2;

    public int mState = IDLE_STATE;

//    public static final int NO_ERROR = 0;
    public static final int SDCARD_ACCESS_ERROR = 1;
    public static final int INTERNAL_ERROR = 2;
    public static final int IN_CALL_RECORD_ERROR = 3;

    public interface OnStateChangedListener {
        void onStateChanged(int state);
        void onError(int error);
    }

    OnStateChangedListener mOnStateChangedListener = null;

    long mSampleStart = 0;       // time at which latest record or play operation started
    public int mSampleLength = 0;      // length of current sample
    public File mSampleFile = null;

    MediaRecorder mRecorder = null;
    public MediaPlayer mPlayer = null;
    private AudioPlaybackWatcher playbackWatcher;

    private AudioProgressView progAudioPlayback;
    private TextView txtAudioElapse;
    private View btnPlayAudio;

    public Recorder(AudioProgressView progAudioPlayback, TextView txtAudioElapse, View btnPlayAudio) {
        this.progAudioPlayback = progAudioPlayback;
        this.txtAudioElapse = txtAudioElapse;
        this.btnPlayAudio = btnPlayAudio;
    }

    // set sample file when editing
    public void setSampleFile(File sampleFile) {
        this.mSampleFile = sampleFile;
        try {
            initPlayer();
        } catch (IOException e) {
            L.e(e);
        }
    }

//    public void saveState(Bundle recorderState) {
//        recorderState.putString(SAMPLE_PATH_KEY, mSampleFile.getAbsolutePath());
//        recorderState.putInt(SAMPLE_LENGTH_KEY, mSampleLength);
//    }
//
//    public int getMaxAmplitude() {
//        if (mState != RECORDING_STATE)
//            return 0;
//        return mRecorder.getMaxAmplitude();
//    }

    public void restoreState(Bundle recorderState) {
        String samplePath = recorderState.getString(SAMPLE_PATH_KEY);
        if (samplePath == null)
            return;
        int sampleLength = recorderState.getInt(SAMPLE_LENGTH_KEY, -1);
        if (sampleLength == -1)
            return;

        File file = new File(samplePath);
        if (!file.exists())
            return;
        if (mSampleFile != null
                && mSampleFile.getAbsolutePath().compareTo(file.getAbsolutePath()) == 0)
            return;

        delete();
        mSampleFile = file;
        mSampleLength = sampleLength;

        signalStateChanged(IDLE_STATE);
    }

    public void setOnStateChangedListener(OnStateChangedListener listener) {
        mOnStateChangedListener = listener;
    }

    public int state() {
        return mState;
    }

    public int progress() {
        if (mState == RECORDING_STATE || mState == PLAYING_STATE)
            return (int) ((System.currentTimeMillis() - mSampleStart) / 1000);
        return 0;
    }

    public int sampleLength() {
        return mSampleLength;
    }

//    public File sampleFile() {
//        return mSampleFile;
//    }

    /**
     * Resets the recorder state. If a sample was recorded, the file is deleted.
     */
    public void delete() {
        stop();

        if (mSampleFile != null)
            mSampleFile.delete();

        mSampleFile = null;
        mSampleStart = 0;
        mSampleLength = 0;

        signalStateChanged(IDLE_STATE);
    }

    /**
     * Resets the recorder state. If a sample was recorded, the file is left on disk and will
     * be reused for a new recording.
     */
    public void clear() {
        stop();

        mSampleStart = 0;
        mSampleLength = 0;

        signalStateChanged(IDLE_STATE);
    }

    public void startRecording(int outputfileformat, String extension, Context context) {
        stop();

        if (mSampleFile == null) {
            File sampleDir = Environment.getExternalStorageDirectory();
            if (sampleDir.canWrite()) {// Workaround for broken sdcard support on the device.
                sampleDir = new File(FiveMilesAppConst.FILEPATH + "voice/");
            } else {
                setError(SDCARD_ACCESS_ERROR);
            }
            if (!sampleDir.exists())
                sampleDir.mkdirs();
            try {
                mSampleFile = File.createTempFile(SAMPLE_PREFIX, extension, sampleDir);
            } catch (IOException e) {
                setError(SDCARD_ACCESS_ERROR);
                return;
            }
        }

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(outputfileformat);
        if(outputfileformat==MediaRecorder.OutputFormat.AMR_NB||outputfileformat==MediaRecorder.OutputFormat.THREE_GPP){
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        }else {
            if (outputfileformat == MediaRecorder.OutputFormat.MPEG_4) {
                mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            }
        }
        mRecorder.setOutputFile(mSampleFile.getAbsolutePath());
        // Handle IOException
        try {
            mRecorder.prepare();
        } catch (IOException exception) {
            setError(INTERNAL_ERROR);
            mRecorder.reset();
            mRecorder.release();
            mRecorder = null;
            return;
        }
        // Handle RuntimeException if the recording couldn't start
        try {
            mRecorder.start();
            updateMicStatus();
        } catch (RuntimeException exception) {
            AudioManager audioMngr = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            boolean isInCall = ((audioMngr.getMode() == AudioManager.MODE_IN_CALL) ||
                    (audioMngr.getMode() == AudioManager.MODE_IN_COMMUNICATION));
            if (isInCall) {
                setError(IN_CALL_RECORD_ERROR);
            } else {
                setError(INTERNAL_ERROR);
            }
            mRecorder.reset();
            mRecorder.release();
            mRecorder = null;
            return;
        }

        L.d("start recording");
        mSampleStart = System.currentTimeMillis();
        setState(RECORDING_STATE);
    }

    /**
     * 到调用时为止，已经录音的时长, in milliseconds
     */
    public long getRecordedTime() {
        return mSampleStart > 0 ? System.currentTimeMillis() - mSampleStart : 0;
    }

    public void stopRecording() {
        if (mRecorder == null)
            return;
        try {
            mRecorder.stop();
            mSampleLength = (int) ((System.currentTimeMillis() - mSampleStart) / 1000);
        } catch (Exception e) {
            // audio too short http://bit.ly/1wciVIL
            DisplayUtils.toast(R.string.voice_too_short);

            if (mSampleFile != null) {
                mSampleFile.delete();
            }
            mSampleFile = null;
            mSampleLength = 0;
        }

        mRecorder.release();
        mRecorder = null;
        setState(IDLE_STATE);
    }

    private void initPlayer() throws IOException {
        if (playbackWatcher != null) {
            playbackWatcher.reset();
        }

        if (mPlayer != null) {
            return;
        }
        mPlayer = new MediaPlayer();
        mPlayer.setDataSource(mSampleFile.getAbsolutePath());
        mPlayer.setOnErrorListener(this);
        mPlayer.setOnCompletionListener(this);
        mPlayer.prepare();

        if (mSampleLength == 0) {
            mSampleLength = mPlayer.getDuration() / 1000;
        }

        playbackWatcher = new AudioPlaybackWatcher(mPlayer, progAudioPlayback, txtAudioElapse, btnPlayAudio);
    }

    public void startPlayback() {
        try {
            initPlayer();
            mPlayer.start();
            playbackWatcher.start();
        } catch (IllegalArgumentException e) {
            setError(INTERNAL_ERROR);
            mPlayer = null;
            return;
        } catch (IOException e) {
            setError(SDCARD_ACCESS_ERROR);
            mPlayer = null;
            return;
        }

        mSampleStart = System.currentTimeMillis();
        setState(PLAYING_STATE);
    }

    public void pausePlayback() {
        L.d("audioPlayback paused");
        if (mPlayer == null) // we were not in playback
            return;
        try{
            if(mPlayer.isPlaying())
                mPlayer.pause();
        }catch(IllegalStateException e){
            setError(INTERNAL_ERROR);
            e.printStackTrace();
        }
        setState(IDLE_STATE);
    }

    private void releaseAudioPlayer() {
        if (playbackWatcher != null) {
            playbackWatcher.reset();
        }

        try {
            if (mPlayer != null) {
                mPlayer.release();
                mPlayer = null;
            }

        } catch (Exception e) {
            L.e(e);
        }
    }

    public void stop() {
        stopRecording();
        pausePlayback();
        releaseAudioPlayer();
    }

    public boolean onError(MediaPlayer mp, int what, int extra) {
        L.d("audioPlayback error");
        stop();
        setError(SDCARD_ACCESS_ERROR);
        return true;
    }

    public void onCompletion(MediaPlayer mp) {
        setState(IDLE_STATE);
    }

    private void setState(int state) {
        if (state == mState)
            return;

        mState = state;
        signalStateChanged(mState);
    }

    private void signalStateChanged(int state) {
        if (mOnStateChangedListener != null)
            mOnStateChangedListener.onStateChanged(state);
    }

    private void setError(int error) {
        if (mOnStateChangedListener != null)
            mOnStateChangedListener.onError(error);
    }

    private void updateMicStatus() {
        if (mRecorder != null) {
            /*
      更新话筒状态

     */
            int BASE = 1;
            double ratio = (double)mRecorder.getMaxAmplitude() / BASE;
            double db = 0;// 分贝
            if (ratio > 1)
                db = 20 * Math.log10(ratio);
//            Log.d(TAG,"decibel："+db);
            int SPACE = 100;
            mHandler.postDelayed(mUpdateMicStatusTimer, SPACE);
            if(mOnRecordDecibelListener!=null)
            mOnRecordDecibelListener.onDecibelChanged(db);
        }

    }
    private final Handler mHandler = new Handler();
    private Runnable mUpdateMicStatusTimer = new Runnable() {
        public void run() {
            updateMicStatus();
        }
    };
//    private final String TAG = "Recorder";
    public interface OnRecordDecibelListener{
        void onDecibelChanged(double decibel);
    }
//
//    public OnRecordDecibelListener getOnRecordDecibelListener() {
//        return mOnRecordDecibelListener;
//    }

    public void setOnRecordDecibelListener(OnRecordDecibelListener mOnRecordDecibelListener) {
        this.mOnRecordDecibelListener = mOnRecordDecibelListener;
    }

    private OnRecordDecibelListener mOnRecordDecibelListener;
}

