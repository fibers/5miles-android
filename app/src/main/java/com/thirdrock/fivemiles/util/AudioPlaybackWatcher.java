package com.thirdrock.fivemiles.util;

import android.media.MediaPlayer;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.view.AudioProgressView;
import com.thirdrock.framework.util.L;

/**
 * Audio playback watcher
 * Created by ywu on 14/11/28.
 */
public class AudioPlaybackWatcher extends Handler implements Runnable,
        MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

    static final int UPDATE_INTERVAL = 25;  // in milliseconds

    private MediaPlayer audioPlayer;
    private AudioProgressView progAudioPlayback;
    private TextView txtAudioElapse;
    private View btnPlayAudio;

    boolean reset, completed, error;
    private int prevPos;

    public AudioPlaybackWatcher(MediaPlayer audioPlayer, AudioProgressView progAudioPlayback, TextView txtAudioElapse, View btnPlayAudio) {
        this.audioPlayer = audioPlayer;
        this.progAudioPlayback = progAudioPlayback;
        this.txtAudioElapse = txtAudioElapse;
        this.btnPlayAudio = btnPlayAudio;
        audioPlayer.setOnCompletionListener(this);
        audioPlayer.setOnErrorListener(this);
    }

    @Override
    public void run() {
        if (audioPlayer == null || reset) {
            return;
        }

        int dur = audioPlayer.getDuration();
        int pos = audioPlayer.getCurrentPosition();
        int durSec = Math.min(dur / 1000, 60);  // 最多60秒
        int posSec = Math.min(pos / 1000, 60);  // 最多60秒

        if (completed) {
            L.v("audioPlayback completed");
            progAudioPlayback.setProgress(100);
            txtAudioElapse.setText(durSec + "''");
        } else if (dur > 0 && pos > prevPos) {
            L.v("audioPlayback %d/%d", pos, dur);
            progAudioPlayback.setProgress(100f * pos / dur);
            txtAudioElapse.setText(posSec + "''");
            prevPos = pos;
        }

        if (!(completed || error)) {
            postDelayed(this, UPDATE_INTERVAL);
        }
    }

    public void start() {
        reset = false;
        completed = false;
        error = false;
        post(this);
    }

    public void reset() {
        reset = true;
        completed = false;
        error = false;
        prevPos = 0;

        progAudioPlayback.setProgress(0);
        btnPlayAudio.setBackgroundResource(R.drawable.square_button_play);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        L.d("audioPlayback completed");
        completed = true;
        prevPos = 0;

        audioPlayer.seekTo(0);
        btnPlayAudio.setBackgroundResource(R.drawable.square_button_play);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        L.d("audioPlayback error");
        error = true;
        return true;
    }
}
