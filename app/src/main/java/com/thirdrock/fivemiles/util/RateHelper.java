package com.thirdrock.fivemiles.util;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;

import com.insthub.fivemiles.Activity.ComplainActivity;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.dialog.SelectionListDialog;
import com.thirdrock.framework.util.L;

import java.util.Arrays;

import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_APP_STATE;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_APP_RATED;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_IS_FIRST_FIVE_STAR;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_IS_FIRST_MARK_SOLD;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_RATE_POPUP;

/**
 * Provide helper method about rate app
 * Created by jubin on 19/5/15.
 */
public class RateHelper {
    private Context mContext;
    private SelectionListDialog rateAppDlg;
    public enum RateEvent{
        markSold, fiveStarReview
    }

    public RateHelper(Context activityContext) {
        if (activityContext == null){
            throw new IllegalArgumentException("param activityContext should not be null");
        }

        this.mContext = activityContext;
    }

    public boolean shouldShowRateDialog(RateEvent event) {
        if (event == null) {
            L.e("event should not be null in shouldShowRateDlg");
            return false;
        }

        SharedPreferences mAppStatePrefs = getAppStatePrefs();
        boolean isAppRated = mAppStatePrefs.getBoolean(PREF_KEY_APP_RATED, false);
        if (isAppRated) {
            return false;
        }

        switch (event) {
            case markSold:
                return mAppStatePrefs.getBoolean(PREF_KEY_IS_FIRST_MARK_SOLD, true);
            case fiveStarReview:
                return mAppStatePrefs.getBoolean(PREF_KEY_IS_FIRST_FIVE_STAR, true);
            default:
                L.w("should not reach here in switch case statement");
                break;
        }

        return false;
    }

    public void showRateApp() {
        final int IDX_RATE_NOW = 0;
        final int IDX_COMPLAIN = 1;
        if (rateAppDlg != null && rateAppDlg.isShowing()){
            return;
        }

        if (rateAppDlg == null) {
            rateAppDlg = new SelectionListDialog(mContext)
                    .setMessage(R.string.rate_app_dlg_txt)
                    .setOptions(Arrays.asList(mContext.getString(R.string.rate_app_dlg_btn_yes),
                            mContext.getString(R.string.rate_app_dlg_btn_no)))
                    .setOnSelectionListener(new SelectionListDialog.OnSelectionListener() {
                        @Override
                        public void onItemSelected(int position) {
                            switch (position){
                                case IDX_RATE_NOW:
                                    onRateApp();
                                    rateAppDlg.dismiss();
                                    TrackingUtils.trackTouch(VIEW_RATE_POPUP, "rate5miles");
                                    break;
                                case IDX_COMPLAIN:
                                    onComplain();
                                    rateAppDlg.dismiss();
                                    TrackingUtils.trackTouch(VIEW_RATE_POPUP, "rate5miles");
                                    break;
                            }
                        }
                    });
        }
        rateAppDlg.show();
    }

    public void markNotFirstMarkSold() {
        // apply()是异步方法，有潜在数据不一致的风险。不过目前测试没有问题，如果发现问题，可以将状态放到Session中维护
        boolean isFirstMarkSold = getAppStatePrefs().getBoolean(PREF_KEY_IS_FIRST_MARK_SOLD, true);
        if (isFirstMarkSold) {
            getAppStatePrefs().edit().putBoolean(PREF_KEY_IS_FIRST_MARK_SOLD, false).apply();
        }
    }

    public void markNotFirstFiveStar() {
        // apply()是异步方法，有潜在数据不一致的风险。不过目前测试没有问题，如果发现问题，可以将状态放到Session中维护
        boolean isFirstFiveStar = getAppStatePrefs().getBoolean(PREF_KEY_IS_FIRST_FIVE_STAR, true);
        if (isFirstFiveStar) {
            getAppStatePrefs().edit().putBoolean(PREF_KEY_IS_FIRST_FIVE_STAR, false).apply();
        }
    }

    private void onRateApp(){
        Uri uri = Uri.parse("market://details?id=" + mContext.getPackageName());
        Intent marketAppIntent = new Intent(Intent.ACTION_VIEW, uri);      // 目前唯一的market就是Google Play

        try {
            mContext.startActivity(marketAppIntent);
        } catch (ActivityNotFoundException e) {
            String playUrlStr = "http://play.google.com/store/apps/details?id=" + mContext.getPackageName();
            Intent playWebIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(playUrlStr));
            mContext.startActivity(playWebIntent);
        }

        markAppRated();
    }

    private void onComplain() {
        Intent intent = new Intent(mContext, ComplainActivity.class);
        mContext.startActivity(intent);

        markAppRated();
    }

    private void markAppRated() {
        getAppStatePrefs().edit().putBoolean(PREF_KEY_APP_RATED, true).apply();
    }

    private SharedPreferences getAppStatePrefs() {
        return mContext.getSharedPreferences(PREFS_APP_STATE, Context.MODE_PRIVATE);
    }
}
