package com.thirdrock.fivemiles.util;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;

/**
 * User education screen, implemented as a dialog.
 * Created by ywu on 14-10-10.
 */
public class EduScreen {
    public static final int NULL_OK_VIEW = -1;

    public interface ViewController {
        void onInit(EduScreen screen, View contentView);
    }

    private Dialog dialog;

    public EduScreen(Context context, int contentViewRid) {
        this(context, R.style.dialog_edu, contentViewRid, null);
    }

    public EduScreen(Context context, int contentViewRid, int okViewRid) {
        this(context, R.style.dialog_edu, contentViewRid, okViewRid, null);
    }

    public EduScreen(Context context, int style, int contentViewRid, ViewController viewController) {
        this(context, style, contentViewRid, NULL_OK_VIEW, viewController);
    }

    public EduScreen(Context context, int style, int contentViewRid, int okViewRid, ViewController viewController) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(contentViewRid, null);
        view.setClickable(true);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        if (okViewRid != NULL_OK_VIEW) {
            View okView = view.findViewById(okViewRid);
            if (okView != null) {
                okView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
            }
        }

        dialog = new Dialog(context, style);
        dialog.setContentView(view);
//        dialog.getWindow().setWindowAnimations(R.style.DialogNoAnimation);
        dialog.setCanceledOnTouchOutside(true);

        if (viewController != null) {
            viewController.onInit(this, view);
        }
    }

    public boolean isShowing() {
        return dialog != null && dialog.isShowing();
    }

    public EduScreen show() {
        dialog.show();
        return this;
    }

    @SuppressWarnings("unused")
    public EduScreen setCanceledOnTouchOutside(boolean cancel) {
        dialog.setCanceledOnTouchOutside(cancel);
        return this;
    }

    @SuppressWarnings("unused")
    public EduScreen setCancelable(boolean flag) {
        dialog.setCancelable(flag);
        return this;
    }

    public void dismiss() {
        if (isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                L.e(e);
            }
        }
    }
}
