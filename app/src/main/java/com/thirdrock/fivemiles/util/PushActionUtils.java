package com.thirdrock.fivemiles.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.view.View;

import com.external.eventbus.EventBus;
import com.insthub.fivemiles.Activity.MakeOfferActivity;
import com.insthub.fivemiles.MessageConstant;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.Push;
import com.thirdrock.protocol.SystemAction;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static android.app.PendingIntent.getActivity;
import static android.app.PendingIntent.getBroadcast;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_MSG_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_OFFER_LINE_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_RF_TAG;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PUSH_OFFER_PREFIX;
import static com.insthub.fivemiles.MessageConstant.PUSH_ID;
import static com.insthub.fivemiles.MessageConstant.PUSH_TAG;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;

/**
 * Push notification handler
 * Created by ywu on 15/2/26.
 */
public class PushActionUtils extends SysMsgActionUtils {

    private final int photoSizePx;

    public PushActionUtils(Context context) {
        super(context);
        photoSizePx = context.getResources().getDimensionPixelSize(R.dimen.notification_icon_size);
    }

//    @Override
//    public void handleSystemAction(SystemAction action, String actionData) {
//        throw new IllegalAccessError("use handlePushAction instead");
//    }

    /**
     * Build intent for the given push data.
     *
     * @param push push data
     * @return responsive intent
     */
    public Intent makeIntent(Push push) {
        if (push == null) {
            L.w("invalid action Push, push is null");
            return null;
        }

        SystemAction act = push.getActionType();
        Intent intent = null;

        if (act == null) {
            L.w("invalid action Push, actionType is null");
            return null;
        }

        switch (act) {
            case message:
                intent = makeSysMsgIntent();
                break;
            case profile:
                intent = makeProfileIntent(push.getActionData());
                break;
            case home:
                intent = makeHomeIntent();
                break;
            case item:
                intent = makeItemIntent(push.getActionData());
                break;
            case link:
                intent = makeLinkAction(push.getActionData());
                break;
            case search:
                intent = makeSearchIntent(act, push.getActionData(), push.getExtraActionData());
                break;
            case category:
                intent = makeSearchIntent(act, push.getActionData(), push.getExtraActionData());
                break;
            case featured_collection:
                intent = makeFeaturedCollectionIntent(push.getActionData());
                break;
            case reviews:
                intent = makeReviewsIntent();
                break;
            default:
                L.w("unknown action Push: %s", act);
                break;
        }

        if (intent != null) {
            intent.putExtra(ACT_PARA_RF_TAG, push.getRfTag());
        }
        return intent;
    }

    public void notifySysMsgRefresh() {
        Message msg = Message.obtain();
        msg.what = MessageConstant.PUSH_ACT_NEW_SYS_MSG;
        msg.obj = "system";
        EventBus.getDefault().post(msg);
    }

    public void showNotification(Push push, String defaultTitle, Intent intent) {
        showNotification(push, defaultTitle, intent, false);
    }

    public void showNotification(Push push, String defaultTitle, Intent intent, boolean broadcast) {
        if (isEmpty(push.getText())) {
            L.w("notification text is empty: %s", push.getType());
            return;
        }

        String tag = null;
        int id = 0;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        if (intent != null) {
            tag = intent.getStringExtra(PUSH_TAG);
            id = intent.getIntExtra(PUSH_ID, 0);

            String title = push.getTitle();
            if (isEmpty(title)) {
                title = defaultTitle;
            }

            PendingIntent pendingIntent = broadcast ?
                    getBroadcast(context, 0, intent, FLAG_UPDATE_CURRENT) :
                    getActivity(context, 0, intent, FLAG_UPDATE_CURRENT);

            builder.setSmallIcon(R.drawable.push)
                    .setColor(context.getResources().getColor(R.color.fm_orange))
                    .setContentTitle(title)
                    .setContentText(push.getText())
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(push.getText()))
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setDefaults(getPushFlags(push));
        } else {
            // sound or vibrate only
            builder.setDefaults(getPushFlags(push));
        }

        if (push.shouldSound()) {
            builder.setSound(getNotificationSound());
        }

        doShowNotification(push, tag, id, builder, intent);
    }

    private void doShowNotification(Push push, final String tag, final int id, final NotificationCompat.Builder builder, final Intent intent) {
        if (isEmpty(push.getImage()) || intent == null) {
            showNotification(tag, id, builder);  // 如果没有intent则仅铃声、震动通知，此时使用PushId=0避免铃声没有播完就被cancel
            return;
        }

        String img = push.getImage();
        img = CloudHelper.toCropUrl(img, photoSizePx, photoSizePx, "thumb,g_face", null);
        ImageLoader.getInstance().loadImage(img, new SimpleImageLoadingListener() {

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                showNotification(tag, id, builder);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                builder.setLargeIcon(loadedImage);
                showNotification(tag, id, builder);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                showNotification(tag, id, builder);
            }
        });
    }

    private void showNotification(String tag, int id, NotificationCompat.Builder builder) {
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE))
                .notify(tag, id, builder.build());
    }

    private int getPushFlags(Push push) {
        int f = 0;

        if (push.shouldVibrate()) {
            f |= Notification.DEFAULT_VIBRATE;
        }

        return f;
    }

    private Uri getNotificationSound() {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                context.getPackageName() + "/" + R.raw.notification);
    }

    public Intent makeOfferIntent(Push push) {
        if (push == null) {
            L.w("invalid action Push, push is null");
            return null;
        }

        int offerLineId = push.getOfferLineId();
        String msgId = push.getMsgId();

        return new Intent(context, MakeOfferActivity.class)
                .setAction(ACT_PUSH_OFFER_PREFIX + offerLineId) // 保持intent唯一，避免不同offerLine的通知被互相覆盖
                .putExtra(ACT_PARA_OFFER_LINE_ID, offerLineId)
                .putExtra(ACT_PARA_MSG_ID, msgId)
                .putExtra(PUSH_TAG, ACT_PUSH_OFFER_PREFIX)
                .putExtra(PUSH_ID, offerLineId)
                .putExtra(ACT_PARA_RF_TAG, push.getRfTag())
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

    }

    public void notifyNewOffer(int offerLineId, String msgId) {
        Message msg = Message.obtain();
        msg.what = MessageConstant.PUSH_ACT_NEW_OFFER;
        msg.arg1 = offerLineId;
        msg.obj = msgId;
        EventBus.getDefault().post(msg);
    }
}
