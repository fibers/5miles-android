package com.thirdrock.fivemiles.util;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.BeeFramework.view.ToastView;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.Protocol.USER;
import com.insthub.fivemiles.Protocol.USER_INFO;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.thirdrock.domain.User;
import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.ui.widget.AvatarView;
import com.thirdrock.framework.util.L;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Util class to display messages
 * Created by ywu on 14-8-9.
 */
public final class DisplayUtils {

    private static final DecimalFormat PRICE_FMT;
    private static final NumberFormat COUNT_FMT;

    private static final int[] BG_COLORS = {
            R.drawable.bg_color_1,
            R.drawable.bg_color_2,
            R.drawable.bg_color_3,
            R.drawable.bg_color_4,
            R.drawable.bg_color_5,
            R.drawable.bg_color_6,
            R.drawable.bg_color_7,
            R.drawable.bg_color_8,
            R.drawable.bg_color_9,
            R.drawable.bg_color_10,
            R.drawable.bg_color_11,
            R.drawable.bg_color_12,
            R.drawable.bg_color_13,
            R.drawable.bg_color_14,
            R.drawable.bg_color_15,
            R.drawable.bg_color_16,
            R.drawable.bg_color_17,
            R.drawable.bg_color_18,
            R.drawable.bg_color_19,
            R.drawable.bg_color_20,
            R.drawable.bg_color_21,
            R.drawable.bg_color_22,
            R.drawable.bg_color_23,
            R.drawable.bg_color_24
    };

    private static final int[] SCORE_ICON_MAPPING = {
            R.drawable.review_0,
            R.drawable.review_1,
            R.drawable.review_2,
            R.drawable.review_3,
            R.drawable.review_4,
            R.drawable.review_5,
            R.drawable.review_6,
            R.drawable.review_7,
            R.drawable.review_8,
            R.drawable.review_9,
            R.drawable.review_10
    };

    private static int nextBgColorIndex;


    static {
        PRICE_FMT = new DecimalFormat("#0.##");
        COUNT_FMT = NumberFormat.getNumberInstance();
        COUNT_FMT.setMaximumFractionDigits(1);
    }

    private DisplayUtils() {
    }

    /**
     * Converts dips to pixels.
     *
     * @param ctx Context to retrieve screen density
     * @param dps to be converted
     * @return pixels
     * @see {http://developer.android.com/guide/practices/screens_support.html#dips-pels}
     */
    public static int toPixels(Context ctx, float dps) {
        float scale = ctx.getResources().getDisplayMetrics().density;
        return (int) (dps * scale + 0.5f);
    }

    /**
     * Show a short toast message, at center of the screen
     */
    private static void toast(Context ctx, String msg) {
        ToastView toast = new ToastView(ctx, msg);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    /**
     * Show a short toast message, at center of the screen
     */
    private static void toast(Context ctx, int resId) {
        ToastView toast = new ToastView(ctx, resId);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    /**
     * Show a short toast message, at center of the screen
     */
    public static void toast(int resId) {
        Context appCtx = FiveMilesApp.appCtx;
        toast(appCtx, resId);
    }

    /**
     * Show a short toast message, at center of the screen
     */
    public static void toast(String msg) {
        Context appCtx = FiveMilesApp.appCtx;
        toast(appCtx, msg);
    }

//    /**
//     * Show a short long message, at center of the screen
//     */
//    public static void centerToastLong(Context ctx, int resId) {
//        ToastView toast = new ToastView(ctx, resId, Toast.LENGTH_LONG);
//        toast.setGravity(Gravity.CENTER, 0, 0);
//        toast.show();
//    }

//    /**
//     * Show a short toast message, at bottom of the screen
//     */
//    public static void bottomToast(Context ctx, CharSequence msg) {
//        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
//    }
//
//    /**
//     * Show a short toast message, at bottom of the screen
//     */
//    public static void bottomToast(Context ctx, int resId) {
//        Toast.makeText(ctx, resId, Toast.LENGTH_SHORT).show();
//    }

    /**
     * Show a long toast message, at bottom of the screen
     */
    public static void bottomToastLong(Context ctx, int resId){
        Toast.makeText(ctx, resId, Toast.LENGTH_LONG).show();
    }

//    /**
//     * Show a long toast message, at bottom of the screen
//     */
//    public static void bottomToastLong(Context ctx, CharSequence msg) {
//        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
//    }
//
//    public static int getScreenWidthDp(Context context) {
//        Configuration cfg = context.getResources().getConfiguration();
//        return cfg.screenWidthDp;
//    }

    public static void cancelNotification(int nid) {
        Context ctx = FiveMilesApp.appCtx;
        ((NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE))
                .cancel(nid);
    }

    public static void cancelNotification(String tag, int nid) {
        Context ctx = FiveMilesApp.appCtx;
        ((NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE))
                .cancel(tag, nid);
    }

    public static void cancelAllNotifications() {
        Context ctx = FiveMilesApp.appCtx;
        ((NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE))
                .cancelAll();
    }

    /**
     * 统一逻辑，当某个数量大于0时，让该区域可以点击
     */
    public static void showCount(ViewGroup container, TextView textView, int count) {
        showCount(container, textView, count, true);
    }

    public static void showCount(ViewGroup container, TextView textView, int count, boolean forceClickable) {
        textView.setText("" + count);
        container.setClickable(forceClickable || count > 0);
    }

    /**
     * 展示评分（5分制）
     *
     */
    public static void showStars(USER user, ImageView ivStars) {
        showStars(user.reputationScore, ivStars);
    }

    public static void showStars(USER_INFO user, ImageView ivStars) {
        showStars(user.reputationScore, ivStars);
    }

    public static void showStars(double reviewScore, ImageView ivStars) {
        //review分数为小数点后一位: 0,0.5,1,1,5 ...5 而star取值为其两倍:0,1,2,...10
        double star_number = Math.ceil(reviewScore * 2);

        if (star_number > (SCORE_ICON_MAPPING.length - 1)) {
            star_number = (SCORE_ICON_MAPPING.length - 1);
        } else if (star_number < 0) {
            //当star为负数,则不再显示星标.
            ivStars.setVisibility(View.INVISIBLE);
            return;
        }
        ivStars.setVisibility(View.VISIBLE);
        ivStars.setImageResource(SCORE_ICON_MAPPING[(int) star_number]);
    }

    public static void showStars(List<ImageView> ivs, int rating) {
        showStars(ivs, rating, R.drawable.star_blank, R.drawable.star_fill);
    }

    public static void showStars(List<ImageView> ivs, int rating, int blankStarRes, int fillStarRes) {
        if (rating < 0) {
            rating = 0;
        }

        for (int i = 0; i < ivs.size(); i++) {
            ivs.get(i).setImageResource(i < rating ? fillStarRes : blankStarRes);
        }
    }

    public static String formatPriceDigits(double price) {
        return PRICE_FMT.format(price);
    }

//    public static void setListViewHeightBasedOnChildren(ListView listView) {
//        int totalHeight = computeListViewHeightBasedOnChildren(listView);
//
//        ViewGroup.LayoutParams params = listView.getLayoutParams();
//        params.height = totalHeight;
//        listView.setLayoutParams(params);
//    }

    /**
     * Set dynamic height according to children's total height
     * @param listView the ListView
     * @param maxRows max rows should be visible
     */
    public static void setListViewHeightBasedOnChildren(ListView listView, float maxRows) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int height = 0;
        int totalRows = listAdapter.getCount();
        int visibleRows = Math.min(totalRows, (int) Math.floor(maxRows));
        int rowHeight = 0;

        for (int i = 0; i < visibleRows; i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            rowHeight = listItem.getMeasuredHeight();
            height += rowHeight;
        }

        if (visibleRows < totalRows) {
            height += Math.ceil(rowHeight / 1.75);
        }

        height += listView.getDividerHeight() * (visibleRows - 1);

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = height;
        listView.setLayoutParams(params);
    }

//    /**
//     * Set dynamic height according to children's total height
//     * @param listView the ListView
//     * @param maxHeight max height in pixel
//     */
//    public static void setListViewHeightBasedOnChildren(ListView listView, int maxHeight) {
//        int totalHeight = computeListViewHeightBasedOnChildren(listView);
//
//        ViewGroup.LayoutParams params = listView.getLayoutParams();
//        params.height = Math.min(totalHeight, maxHeight);
//        listView.setLayoutParams(params);
//    }
//
//    public static int computeListViewHeightBasedOnChildren(ListView listView) {
//        ListAdapter listAdapter = listView.getAdapter();
//        if (listAdapter == null) {
//            return 0;
//        }
//
//        int totalHeight = 0;
//        for (int i = 0; i < listAdapter.getCount(); i++) {
//            View listItem = listAdapter.getView(i, null, listView);
//            listItem.measure(0, 0);
//            totalHeight += listItem.getMeasuredHeight();
//        }
//
//        totalHeight += listView.getDividerHeight() * (listAdapter.getCount() - 1);
//        return totalHeight;
//    }

    /**
     * 获取一个随机的纯色背景资源，可作为图片加载的placeholder
     */
    public static int getNextBackgroundResource() {
        int rid = BG_COLORS[nextBgColorIndex++];
        if (nextBgColorIndex == BG_COLORS.length) {
            nextBgColorIndex = 0;
        }
        return rid;
    }

    public static void showText(TextView tv, CharSequence text) {
        if (tv == null) {
            return;
        }

        if (ModelUtils.isNotEmpty(text)) {
            tv.setText(text.toString().trim());
            tv.setVisibility(View.VISIBLE);
        } else {
            tv.setVisibility(View.GONE);
        }
    }

    public static String toConciseCount(int count) {
        String result = String.valueOf(count);

        if (count > 999) {
            result = COUNT_FMT.format(count / 1000.0) + "k";
        }

        return result;
    }

    /**
     * 格式化近似的计数值，超过max以+代替
     */
    public static String toApproximalCount(int count, int max) {
        count = Math.min(count, max);
        String result = String.valueOf(count);

        if (count > max) {
            result += "+";
        }

        return result;
    }

    private static Typeface getTypeface(Context context, String fontFile) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/" + fontFile);
    }

    public static void setTypeface(Context context, TextView tv, String fontFile) {
        tv.setTypeface(getTypeface(context, fontFile));
    }

    public static void showAvatar(AvatarView iv, User user, int widthPx, DisplayImageOptions opts) {
        showAvatar(iv, user.getAvatarUrl(), user.isVerified(), widthPx, opts);
    }

    @Deprecated
    public static void showAvatar(AvatarView iv, USER user, int widthPx, DisplayImageOptions opts) {
        showAvatar(iv, user.portrait, user.isVerified, widthPx, opts);
    }

    @Deprecated
    public static void showAvatar(AvatarView iv, USER_INFO user, int widthPx, DisplayImageOptions opts) {
        showAvatar(iv, user.portrait, user.isVerified, widthPx, opts);
    }

    public static void showAvatar(AvatarView iv, String url, boolean isVerified, int size, DisplayImageOptions opts) {
        url = CloudHelper.toCropUrl(url, size, size, "thumb,g_face", null);
        ImageLoader.getInstance().displayImage(url, iv.getAvatarImageView(), opts);
        showVerification(iv, isVerified);
    }

    private static void showVerification(AvatarView iv, boolean verified) {
        iv.setVerificationIndicator(verified ? R.drawable.ic_verified : R.drawable.ic_unverified);
    }

    public static void showEmailVerificationIcon(ImageView icon, boolean isVerified){
        if (icon == null){
            L.e("email verification icon is null");
            return;
        }

        if (isVerified){
            icon.setImageResource(R.drawable.ic_verified_email);
        }else {
            icon.setImageResource(R.drawable.ic_unverified_email);
        }
    }

    public static void showFacebookVerificationIcon(ImageView icon, boolean isVerified){
        if (icon == null){
            L.e("facebook verification icon is null");
            return;
        }

        if (isVerified){
            icon.setImageResource(R.drawable.ic_verified_facebook);
        }else {
            icon.setImageResource(R.drawable.ic_unverified_facebook);
        }
    }
    public static void showPhoneVerificationIcon(ImageView icon, boolean isVerified){
        if (icon == null){
            L.e("phone verification icon is null");
            return;
        }

        if (isVerified){
            icon.setImageResource(R.drawable.ic_verified_phone);
        }else {
            icon.setImageResource(R.drawable.ic_unverified_phone);
        }
    }

    public static void setProgressColorScheme(SwipeRefreshLayout v) {
        v.setColorSchemeResources(R.color.fm_orange,
                android.R.color.holo_green_light,
                android.R.color.holo_blue_light);
    }

    public static void closeKeyBoard(Window window) {
        View focusedView = window.getCurrentFocus();
        if (focusedView != null) {
            InputMethodManager imm = (InputMethodManager) FiveMilesApp.getInstance().
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            focusedView.clearFocus();
            imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
        }
    }

    public static void showKeyBoard(final View v) {

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) FiveMilesApp.appCtx.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(v, 0);
            }
        },500);
    }


}