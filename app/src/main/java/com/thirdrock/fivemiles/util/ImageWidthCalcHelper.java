package com.thirdrock.fivemiles.util;

/**
 * 为减少碎片，客户端讲图片尺寸限制在几个级别上
 * Created by jubin on 19/1/15.
 */
public class ImageWidthCalcHelper {
    // 图片按宽度分为以下几档，减少cloudinary上的图片副本
    public static final int IMG_W_XS = 100;
    public static final int IMG_W_S = 200;
    public static final int IMG_W_M = 400;
    public static final int IMG_W_L = 650;
    public static final int IMG_W_XL = 800;
    public static final int IMG_W_XXL = 1080;
    public static final int IMG_W_XXXL = 1440;

    /**
     * 超过IMG_W_XXXL的返回原来的尺寸
     * @param width Cloudinary上缓存图片的宽度
     * @return proper width
     */
    public static int getOnlineImageWidth(int width){

        if (width <= IMG_W_XS) {
            width = IMG_W_XS;
        } else if (width <= IMG_W_S) {
            width = IMG_W_S;
        } else if (width <= IMG_W_M) {
            width = IMG_W_M;
        } else if (width <= IMG_W_L) {
            width = IMG_W_L;
        } else if (width <= IMG_W_XL) {
            width = IMG_W_XL;
        } else if (width <= IMG_W_XXL){
            width = IMG_W_XXL;
        } else if (width <= IMG_W_XXXL) {
            width= IMG_W_XXXL;
        }

        return width;
    }

}
