package com.thirdrock.fivemiles.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.appsflyer.AppsFlyerLib;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.analytics.ExceptionParser;
import com.google.android.gms.analytics.ExceptionReporter;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.exception.NetworkException;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.Utils;
import com.thirdrock.framework.util.rx.Emit1;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.thirdrock.framework.util.Utils.getNetworkStateDesc;

/**
 * Helper class for GA tracking
 * Created by ywu on 14-9-10.
 */
public final class TrackingUtils {

    private static final Uri FB_ATTRIBUTION_CONTENT_URI = Uri.parse("content://com.facebook.katana.provider.AttributionIdProvider");
    private static final String FB_ATTRIBUTION_COLUMN_AID = "aid";

    private static Tracker gaTraker;


    private TrackingUtils() {

    }

    private static Context getContext() {
        return FiveMilesApp.getInstance().getApplicationContext();
    }

    public static Tracker getGaTraker() {
        initGaTracker();
        return gaTraker;
    }

    public static void initGaTracker() {
        if (gaTraker == null) {
            if(!Utils.isDebug()){
                gaTraker = GoogleAnalytics.getInstance(getContext())
                        .newTracker(R.xml.analytics);
            }else{
                gaTraker = GoogleAnalytics.getInstance(getContext())
                        .newTracker(getContext().getString(R.string.ga_trackingId));
            }

            ExceptionReporter reporter = new ExceptionReporter(gaTraker,
                    Thread.getDefaultUncaughtExceptionHandler(),
                    getContext());
            ExceptionParser parser = new StandardExceptionParser(getContext(), null) {
                @Override
                public String getDescription(String threadName, Throwable t) {
                    return "[" + threadName + "]" + Log.getStackTraceString(t);
                }
            };
            reporter.setExceptionParser(parser);
            Thread.setDefaultUncaughtExceptionHandler(reporter);
        }
    }

//    public static void trackAppActive() {
    // Facebook
//        AppEventsLogger.activateApp(getContext(),
//                getContext().getString(R.string.facebook_app_id));
//    }


    public static void trackView(String viewName) {
        if (ModelUtils.isEmpty(viewName)) {
            return;
        }

        try {
            Tracker tracker = getGaTraker();
            tracker.setScreenName(viewName);
            tracker.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {
            L.e(e);
        }
    }

    public static void trackTouch(String viewName, String touchAction) {
        trackTouch(viewName, touchAction, null);
    }

    public static void trackTouch(String viewName, String touchAction, Long value) {
        try {
            Tracker tracker = getGaTraker();
            HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
                    .setCategory(viewName)
                    .setAction(touchAction)
                    .setLabel(touchAction);

            if (value != null) {
                builder.setValue(value);
            }

            tracker.send(builder.build());
        } catch (Exception e) {
            L.e(e);
        }
    }

    public static void trackEvent(String catg, String action, String label, Long value) {
        try {
            Tracker tracker = getGaTraker();
            HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder()
                    .setCategory(catg)
                    .setAction(action);
            if (label != null) {
                eventBuilder.setLabel(label);
            }

            if (value != null) {
                eventBuilder.setValue(value);
            }

            tracker.send(eventBuilder.build());
        } catch (Exception e) {
            L.e(e);
        }
    }

    public static void trackException(Throwable e) {
        if (e == null) {
            return;
        }

        StackTraceElement[] stack = e.getStackTrace();
        if (stack != null && stack.length > 0) {
            trackException(e.getClass() + "@" + String.valueOf(stack[0]), false);
        }
    }

    public static void trackException(String desc, boolean isFatal) {
        Tracker tracker = getGaTraker();
        HitBuilders.ExceptionBuilder exceptionBuilder = new HitBuilders.ExceptionBuilder()
                .setDescription(desc)
                .setFatal(isFatal);
        tracker.send(exceptionBuilder.build());
    }

    public static void trackNetworkException(NetworkException e) {
        trackNetworkException(e.getStatusCode(), e.getCause());
    }

    public static void trackNetworkException(int statusCode, Throwable cause) {
        // no private information in exception message for this case
        trackException(statusCode + "|" +
                cause + "|" +
                SESSION.getInstance().userId + "|" +
                getNetworkStateDesc(), false);
    }

    /**
     * AppsFlyer event tracking
     */
    public static void trackAFEvent(String event, String value) {
        value = TextUtils.isEmpty(value) ? "" : value;
        try {
            AppsFlyerLib.sendTrackingWithEvent(getContext(), event, value);
        } catch (Exception e) {
            L.e(e);
        }
    }

    public static void trackTiming(String catg, String name, long value) {
        try {
            Tracker tracker = getGaTraker();
            tracker.send(new HitBuilders.TimingBuilder(catg, name, value).build());
        } catch (Exception e) {
            L.e(e);
        }
    }

    /**
     * Get google advertising id
     */
    public static Observable<AdvertisingIdClient.Info>getAdvertisingId() {
        return Observable.create(new Emit1<AdvertisingIdClient.Info>() {
            @Override
            protected AdvertisingIdClient.Info call() throws Exception {
                return AdvertisingIdClient.getAdvertisingIdInfo(FiveMilesApp.getInstance());
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<String> getFbAttributionId() {
        return Observable.create(new Emit1<String>() {
            @Override
            protected String call() throws Exception {
                ContentResolver resolver = FiveMilesApp.getInstance().getContentResolver();

                String [] projection = {FB_ATTRIBUTION_COLUMN_AID};
                Cursor c = resolver.query(FB_ATTRIBUTION_CONTENT_URI, projection, null, null, null);
                if (c == null || !c.moveToFirst()) {
                    return null;
                }
                String attributionId = c.getString(c.getColumnIndex(FB_ATTRIBUTION_COLUMN_AID));
                c.close();
                return attributionId;
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
