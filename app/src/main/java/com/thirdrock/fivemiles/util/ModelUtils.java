package com.thirdrock.fivemiles.util;

import com.insthub.fivemiles.Protocol.COMMENT;
import com.insthub.fivemiles.Protocol.USER;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.OfferLine;
import com.thirdrock.domain.User;

import java.io.Closeable;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;

import static java.lang.Double.isNaN;

/**
 * Util class, should be replaced
 * Created by lishangqiang on 2014/8/15.
 */
public final class ModelUtils {

    private ModelUtils() {

    }

//    public static JSONArray touch(JSONArray data) {
//        if (data != null)
//            return data;
//        else
//            return new JSONArray();
//    }

    /**
     * 安全比较两个对象的顺序：比较之前先判断是否为null，避免NullPointerException
     */
    public static <T extends Comparable> int compare(T o1, T o2) {
        if (o1 == null || o2 == null) {
            if (o1 != null)
                return 1;
            if (o2 != null)
                return -1;
            return 0;
        }
        return o1.compareTo(o2);
    }

    /**
     * 安全判断字符串是否为空
     */
    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

    public static boolean isNotEmpty(CharSequence str) {
        return !isEmpty(str);
    }

    /**
     * 是否存在任一不为空的字符串
     */
    public static boolean isNotAllEmpty(CharSequence... strs) {
        boolean result = false;

        for (CharSequence s : strs) {
            if (isNotEmpty(s)) {
                result = true;
                break;
            }
        }

        return result;
    }

    /**
     * None of the strings is empty
     */
    public static boolean isNotEmpty(CharSequence... strs) {
        boolean result = true;

        for (CharSequence s : strs) {
            if (isEmpty(s)) {
                result = false;
                break;
            }
        }

        return result;
    }

    /**
     * 安全判断两个对象是否满足equals条件：判断之前先检查值是否为null，避免NullPointerException
     */
    public static boolean equals(Object o1, Object o2) {
        if (o1 == null || o2 == null) {
            if (o1 != null)
                return false;
            if (o2 != null) {
                return false;
            }

            return true;
        }
        return o1.equals(o2);
    }

    /**
     * 安全判断字符串是否为空：如果该变量为null，或者值为"null"，或者为空，都返回true
     */
    public static boolean isNull(String str) {
        return str == null || str.equals("null") || isEmpty(str);
    }

    /**
     * 安全判断字符串是否为空
     */
    public static boolean isNotNull(String str) {
        return ! isNull(str);
    }

    /**
     * 当前用户是否商品的owner
     */
    public static boolean isOwner(String ownerId) {
        if (SESSION.getInstance().isAuth()) {
            return SESSION.getInstance().userId.equals(ownerId);
        }
        return false;
    }

    public static boolean isMe(String userId) {
        if (SESSION.getInstance().isAuth()) {
            return equals(userId, SESSION.getInstance().userId);
        }
        return false;
    }

    public static boolean isMe(USER user) {
        return isMe(user.id);
    }

    public static boolean isMe(User user) {
        return user != null && isMe(user.getId());
    }

    public static boolean isMine(Item item) {
        if (SESSION.getInstance().isAuth()) {
            return item != null && isMe(item.getOwner());
        }
        return false;
    }

    public static boolean isMine(OfferLine offer) {
        if (SESSION.getInstance().isAuth()) {

        }

        return offer != null && offer.getBuyer() != null && isMe(offer.getBuyer());
    }

    /**
     * 当前用户是否能够对某件商品进行评论
     */
    public static boolean canComment(String ownerId, boolean isSold) {
        return !isSold && !isOwner(ownerId);
    }

    public static boolean canReplyComent(String ownerId, boolean isSold, COMMENT comment) {
        return !isSold && isOwner(ownerId) && !isOwner(comment.author.id);
    }

//    public static int indexOf(int[] data, int element) {
//        for (int i = 0; i < data.length; i++) {
//            if (data[i] == element)
//                return i;
//        }
//        return -1;
//    }

    public static String encodeUrl(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        }
        catch (Exception e) {
            throw new AppException(e);
        }
    }

    public static String combineUrlAndParams(String url, Map<String, Object> params) {
        if (params == null)
            return url;

        StringBuilder buf = new StringBuilder().append(url);
        int i = 0;
        for (String paramName : params.keySet()) {
            // 正确添加?和&符号
            if (i == 0 && url.indexOf('?') < 0) {
                buf.append("?");
            }
            else {
                buf.append("&");
            }
            i += 1;
            Object paramValue = params.get(paramName);
            buf.append(paramName).append("=").append(paramValue);
        }
        url = buf.toString();
        return url;
    }

    public static int toScore(double score) {
        int value = (int) (Math.round(score));
        if (value > 5)
            value = 5;
        if (value < 0)
            value = 0;
        return value;
    }

    public static String trim(String str) {
        return isEmpty(str) ? "" : str.trim();
    }

    public static void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isValidPrice(Double price) {
        return !(price == null || isNaN(price) || price.compareTo(0d) < 0);
    }

    public static boolean isValidPrice(CharSequence price) {
        if (isNotEmpty(price)) {
            try {
                Double v = Double.valueOf(price.toString());
                return isValidPrice(v);
            }
            catch (Exception e) {
                // ignore
            }
        }

        return false;
    }

    public static boolean equals(double d1, double d2) {
        return Math.abs(d1 - d2) < 1e-8;
    }
}
