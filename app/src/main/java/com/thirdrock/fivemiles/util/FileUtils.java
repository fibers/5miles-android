package com.thirdrock.fivemiles.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.insthub.fivemiles.FiveMilesAppConst;
import com.thirdrock.framework.util.L;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Utilities for file system operation
 * TODO 考虑更好的临时文件管理策略
 * Created by ywu on 14-10-12.
 */
public final class FileUtils {

    private FileUtils() {

    }

    public static Uri getTempCameraImageUri() throws IOException {
        return Uri.fromFile(getTempCameraImage());
    }

    public static File getTempCameraImage() throws IOException {
//        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
//        return File.createTempFile("5miles_album", ".jpg", dir);
        File dir = new File(FiveMilesAppConst.CAMERACACHE);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        return File.createTempFile("item", ".jpg", dir);
    }

    public static void refreshGallery(Context ctx, File imgFile) {
        try {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(imgFile);
            mediaScanIntent.setData(contentUri);
            ctx.sendBroadcast(mediaScanIntent);
        }
        catch (Exception e) {
            L.e(e);
        }
    }

    public static String readFileContent(InputStream is) throws IOException {
        StringBuilder buf = new StringBuilder();

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        try {
            String ln;
            while ((ln = reader.readLine()) != null) {
                buf.append(ln);
            }
        } finally {
            reader.close();
        }

        return buf.toString();
    }
}
