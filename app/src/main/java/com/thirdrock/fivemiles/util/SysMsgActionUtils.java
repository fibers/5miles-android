package com.thirdrock.fivemiles.util;

import android.content.Context;
import android.content.Intent;
import android.os.Message;

import com.external.eventbus.EventBus;
import com.insthub.fivemiles.Activity.GenericWebActivity;
import com.insthub.fivemiles.Activity.MainTabActivity;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.MessageConstant;
import com.thirdrock.fivemiles.item.FeaturedCollectionActivity;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.main.profile.ProfileRouteActivity;
import com.thirdrock.fivemiles.review.ReviewListActivity;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.Push;
import com.thirdrock.protocol.SystemAction;

import java.util.Map;

import static android.text.TextUtils.isDigitsOnly;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_GOTO_HOME;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_COLLECTION_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_MSG_LIST;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_S_CAMPAIGN_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_S_CATEGORY;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_S_KEYWORD;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_TAB_TAG_SPEC;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_PAGE_URL;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PUSH_FEATURED_COLLECTION_PREFIX;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PUSH_ITEM_PREFIX;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PUSH_LINK_PREFIX;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PUSH_USER_PREFIX;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_REFRESH_HOME_TAB;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_REVIEWS;
import static com.insthub.fivemiles.MessageConstant.PUSH_ID;
import static com.insthub.fivemiles.MessageConstant.PUSH_ID_SYS_MSG;
import static com.insthub.fivemiles.MessageConstant.PUSH_TAG;
import static com.insthub.fivemiles.MessageConstant.PUSH_TAG_SYS_MSG;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;

/**
 * System message clicks handler
 * Created by ywu on 15/2/26.
 */
public class SysMsgActionUtils {

    protected final Context context;

    public SysMsgActionUtils(Context context) {
        this.context = context;
    }

    public void handleSystemAction(SystemAction action, String actionData, Map<String, String> extra) {
        switch (action) {
            case message:
                handleSysMsgAction();
                break;
            case profile:
                handleProfileAction(actionData);
                break;
            case home:
                handleHomeAction();
                break;
            case item:
                handleItemAction(actionData);
                break;
            case link:
                handleLinkAction(actionData);
                break;
            case search:
                handleSearchAction(action, actionData, extra);
                break;
            case category:
                handleSearchAction(action, actionData, extra);
                break;
            case featured_collection:
                handleFeaturedCollection(actionData);
                break;
            case reviews:
                handleReviews();
                break;
            default:
                L.w("unknown action: %s", action);
                break;
        }
    }

    protected void handleSysMsgAction() {
        context.startActivity(makeSysMsgIntent());
    }

    protected Intent makeSysMsgIntent() {
        return new Intent(context, MainTabActivity.class)
                .setAction(ACT_REFRESH_HOME_TAB)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .putExtra(ACT_PARA_TAB_TAG_SPEC, MainTabActivity.TAB_TAG_SPEC_MESSAGE)
                .putExtra(ACT_PARA_MSG_LIST, "system")
                .putExtra(PUSH_TAG, PUSH_TAG_SYS_MSG)
                .putExtra(PUSH_ID, PUSH_ID_SYS_MSG);
    }

    protected void handleProfileAction(String uid) {
        if (isEmpty(uid)) {
            L.w("invalid profile action, id is empty");
            return;
        }

        context.startActivity(makeProfileIntent(uid));
    }

    protected Intent makeProfileIntent(String uid) {
        return new Intent(context, ProfileRouteActivity.class)
                .setAction(ACT_PUSH_USER_PREFIX + uid)
                .putExtra(ACT_PARA_USER_ID, uid)
                .putExtra(PUSH_TAG, ACT_PUSH_USER_PREFIX).putExtra(PUSH_ID, uid.hashCode());
    }

    protected void handleHomeAction() {
        Message msg = Message.obtain();
        msg.what = MessageConstant.PUSH_ACT_REFRESH_HOME;
        msg.arg1 = PUSH_ID_SYS_MSG;
        EventBus.getDefault().post(msg);
    }

    protected Intent makeHomeIntent() {
        return new Intent(context, MainTabActivity.class)
                .setAction(ACT_GOTO_HOME)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .putExtra(PUSH_TAG, PUSH_TAG_SYS_MSG)
                .putExtra(PUSH_ID, PUSH_ID_SYS_MSG);
    }

    protected void handleItemAction(String id) {
        if (isEmpty(id)) {
            L.w("invalid item action, id is empty");
            return;
        }

        context.startActivity(makeItemIntent(id));
    }

    protected Intent makeItemIntent(String id) {
        return new Intent(context, ItemActivity.class)
                .setAction(ACT_PUSH_ITEM_PREFIX + id)
                .putExtra(ACT_PARA_ITEM_ID, id)
                .putExtra(PUSH_TAG, ACT_PUSH_ITEM_PREFIX)
                .putExtra(PUSH_ID, id.hashCode());
    }

    protected void handleLinkAction(String link) {
        context.startActivity(makeLinkAction(link));
    }

    protected Intent makeLinkAction(String link) {
        return new Intent(context, GenericWebActivity.class)
                .setAction(ACT_PUSH_LINK_PREFIX + link)
                .putExtra(ACT_PARA_PAGE_URL, link)
                .putExtra(PUSH_TAG, ACT_PUSH_LINK_PREFIX)
                .putExtra(PUSH_ID, link.hashCode());
    }

    protected void handleSearchAction(SystemAction action, String actionData, Map<String, String> extra) {
        if (isEmpty(actionData)) {
            L.w("invalid search action, keyword is empty");
            return;
        }

        context.startActivity(makeSearchIntent(action, actionData, extra));
    }

    protected Intent makeSearchIntent(SystemAction action, String actionData, Map<String, String> extra) {
        int catgId = -1;
        if (action == SystemAction.category && isDigitsOnly(actionData)) {
            catgId = Integer.parseInt(actionData);
        }

        Intent intent = new Intent(context, MainTabActivity.class)
                .setAction(ACT_REFRESH_HOME_TAB)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .putExtra(ACT_PARA_TAB_TAG_SPEC, MainTabActivity.TAB_TAG_SPEC_SEARCH)
                .putExtra(PUSH_TAG, PUSH_TAG_SYS_MSG)
                .putExtra(PUSH_ID, PUSH_ID_SYS_MSG);

        if (catgId > -1) {
            intent.putExtra(ACT_PARA_S_CATEGORY, catgId);
        } else {
            intent.putExtra(ACT_PARA_S_KEYWORD, actionData);
        }

        if (extra.containsKey(Push.EXTRA_KEY_CAMPAIGN_ID)) {
            intent.putExtra(ACT_PARA_S_CAMPAIGN_ID, extra.get(Push.EXTRA_KEY_CAMPAIGN_ID));
        }
        return intent;
    }

    private void handleFeaturedCollection(String id) {
        if (isEmpty(id)) {
            L.w("invalid featured collection push, id is empty");
            return;
        }
        context.startActivity(makeFeaturedCollectionIntent(id));
    }

    protected Intent makeFeaturedCollectionIntent(String id) {
        return new Intent(context, FeaturedCollectionActivity.class)
                .setAction(ACT_PUSH_FEATURED_COLLECTION_PREFIX + id)
                .putExtra(ACT_PARA_COLLECTION_ID, id)
                .putExtra(PUSH_TAG, ACT_PUSH_FEATURED_COLLECTION_PREFIX)
                .putExtra(PUSH_ID, id.hashCode());
    }

    private void handleReviews() {
        context.startActivity(makeReviewsIntent());
    }

    protected Intent makeReviewsIntent() {
        return new Intent(context, ReviewListActivity.class)
                .setAction(ACT_REVIEWS)
                .putExtra(ACT_PARA_USER_ID, FiveMilesAppConst.USER_ID_ME);
    }
}