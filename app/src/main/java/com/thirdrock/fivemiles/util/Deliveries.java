package com.thirdrock.fivemiles.util;

import android.content.Context;

import com.thirdrock.domain.EnumDeliveryType;
import com.thirdrock.fivemiles.R;

import java.util.ArrayList;

import javax.inject.Inject;

import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_APP_STATE;

/**
 * Delivery methods utils.
 * Created by ywu on 14/11/20.
 */
public final class Deliveries {

    public static final String PREF_KEY_DELIVERY = "delivery";

    private static final ArrayList<Delivery> deliveries = new ArrayList<>();

    private Context context;

    private Delivery defaultDelivery;

    @Inject
    public Deliveries(Context context) {
        this.context = context;
    }

    public static ArrayList<Delivery> getAvailableDeliveries(Context ctx) {
        if (deliveries.isEmpty()) {
            deliveries.add(new Delivery(EnumDeliveryType.BOTH, ctx.getString(R.string.delivery_both)));
            deliveries.add(new Delivery(EnumDeliveryType.EXCHANGE_IN_PERSON, ctx.getString(R.string.delivery_exchange)));
            deliveries.add(new Delivery(EnumDeliveryType.POST, ctx.getString(R.string.delivery_by_post)));
        }

        return deliveries;
    }

    public Delivery getDefaultDelivery() {
        if (defaultDelivery == null) {
            loadDefaultDelivery();
        }

        return defaultDelivery;
    }

    public void setDefaultDelivery(Delivery delivery) {
        if (delivery == null) {
            return;
        }

        defaultDelivery = delivery;
        context.getSharedPreferences(PREFS_APP_STATE, Context.MODE_PRIVATE)
                .edit()
                .putInt(PREF_KEY_DELIVERY, delivery.value.ordinal())
                .apply();
    }

    private void loadDefaultDelivery() {
        int deliveryTypeId = context.getSharedPreferences(PREFS_APP_STATE, Context.MODE_PRIVATE)
                .getInt(PREF_KEY_DELIVERY, EnumDeliveryType.BOTH.ordinal());
        defaultDelivery = getDelivery(context, deliveryTypeId);
    }

    public static Delivery getDelivery(Context ctx, EnumDeliveryType type) {
        switch (type) {
            case BOTH:
                return new Delivery(type, ctx.getString(R.string.delivery_both));
            case EXCHANGE_IN_PERSON:
                return new Delivery(type, ctx.getString(R.string.delivery_exchange));
            case POST:
                return new Delivery(type, ctx.getString(R.string.delivery_by_post));
            default:
                return new Delivery(type, "unknown");
        }
    }

    public static Delivery getDelivery(Context ctx, int typeId) {
        return getDelivery(ctx, EnumDeliveryType.valueOf(typeId));
    }
}
