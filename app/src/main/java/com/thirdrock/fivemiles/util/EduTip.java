package com.thirdrock.fivemiles.util;

import android.content.res.Resources;
import android.support.v4.widget.PopupWindowCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.insthub.fivemiles.FiveMilesApp;
import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * Call dismiss() in onPause to avoid activity leak
 * Created by ywu on 15/4/16.
 */
public class EduTip {
    private static final int BG_ALPHA = 0xe6;  // alpha of tip window background, 90%

    private PopupWindow tipWindow;

    private int contentViewId = R.layout.default_edu_tip;
    private int backgroundId = R.id.tip_bg;
    private int textViewId = R.id.txt_tips;
    private int closeViewId = R.id.btn_close;
    private int widthId = R.dimen.tip_default_width;
    private int textId;

    private View anchorView;
    private int offsetX, offsetY, gravity;
    private int autoCloseTimeout;
    private Subscription subsAutoCloseAction;

    private PopupWindow.OnDismissListener onDismissListener;
    private View.OnClickListener onCloseListener;

    public EduTip() {
        offsetY = getResources().getDimensionPixelSize(R.dimen.tip_offset_y);
        autoCloseTimeout = getResources().getInteger(R.integer.edu_tips_auto_close_timeout);
    }

    public EduTip contentView(int contentViewId) {
        this.contentViewId = contentViewId;
        return this;
    }

    public EduTip contentView(int contentViewId, int textViewId, int closeViewId) {
        this.contentViewId = contentViewId;
        this.textViewId = textViewId;
        this.closeViewId = closeViewId;
        return this;
    }

    /**
     * the background view id in the root layout (NOT the background drawable id)
     * to set alpha of the background
     */
    public EduTip backgroundId(int bgId) {
        this.backgroundId = bgId;
        return this;
    }

    public EduTip width(int widthId) {
        this.widthId = widthId;
        return this;
    }

    public EduTip anchor(View anchorView) {
        return anchor(anchorView, null, null, 0);
    }

    public EduTip anchor(View anchorView, Integer xoff, Integer yoff) {
        return anchor(anchorView, xoff, yoff, 0);
    }

    /**
     * @param gravity @see android.view.Gravity
     */
    public EduTip anchor(View anchorView, Integer xoff, Integer yoff, int gravity) {
        this.anchorView = anchorView;

        if (xoff != null) {
            this.offsetX = xoff;
        }

        if (yoff != null) {
            this.offsetY = yoff;
        }

        this.gravity = gravity;
        return this;
    }

    public EduTip gravity(int gravity) {
        this.gravity = gravity;
        return this;
    }

    public EduTip text(int textId) {
        this.textId = textId;
        return this;
    }

    public boolean isShowing() {
        return tipWindow != null && tipWindow.isShowing();
    }

    public EduTip show() {
        if (isShowing()) {
            return this;
        }

        if (tipWindow == null) {
            buildTipWindow();
        }

        PopupWindowCompat.showAsDropDown(tipWindow, anchorView, offsetX, offsetY, gravity);
        if (autoCloseTimeout > 0) {
            scheduleAutoClose();
        }
        return this;
    }

    private void buildTipWindow() {
        View rootView = getLayoutInflater().inflate(contentViewId, null);

        if (backgroundId > 0) {
            rootView.findViewById(backgroundId).getBackground().setAlpha(BG_ALPHA);
        }

        TextView textView = (TextView) rootView.findViewById(textViewId);
        textView.setText(textId);

        View closeView = rootView.findViewById(closeViewId);
        closeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close(v);
            }
        });

        tipWindow = new PopupWindow(rootView, getResources().getDimensionPixelSize(widthId), WRAP_CONTENT);
        // 使得tips只能通过点击x关闭
        tipWindow.setFocusable(false);  // 使得点击能够透传至下层窗口
        tipWindow.setTouchable(true);  // 但又使得tips窗口能够点击
        tipWindow.setOutsideTouchable(false);  // 禁止通过触碰空白处关闭tips

        if (onDismissListener != null) {
            tipWindow.setOnDismissListener(onDismissListener);
        }
    }

    private void scheduleAutoClose() {
        subsAutoCloseAction = Observable.timer(autoCloseTimeout, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        subsAutoCloseAction = null;
                        close(null);
                    }
                });
    }

    /**
     * close tip window by user or program
     */
    private void close(View v) {
        if (!isShowing()) {
            return;
        }

        if (onCloseListener != null) {
            try {
                onCloseListener.onClick(v);
            } catch (Exception e) {
                L.e("tips window close callback failed", e);
            }
        }

        dismiss();
    }

    public void dismiss() {
        if (isShowing()) {
            tipWindow.dismiss();

            if (subsAutoCloseAction != null) {
                subsAutoCloseAction.unsubscribe();
                subsAutoCloseAction = null;
            }
        }
    }

    public EduTip setOnDimissListener(PopupWindow.OnDismissListener listener) {
        this.onDismissListener = listener;
        if (tipWindow != null) {
            tipWindow.setOnDismissListener(listener);
        }
        return this;
    }

    public EduTip setOnCloseListener(View.OnClickListener listener) {
        this.onCloseListener = listener;
        return this;
    }

    protected Resources getResources() {
        return FiveMilesApp.getInstance().getResources();
    }

    protected LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(FiveMilesApp.getInstance());
    }
}
