package com.thirdrock.fivemiles.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;

import java.text.NumberFormat;
import java.util.Collections;
import java.util.Currency;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_APP_STATE;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_LIST_ITEM;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;

/**
 * Helper class handling Currency
 * Created by ywu on 14/10/23.
 */
public class Currencies {

    public static final String PREF_KEY_CURRENCY = "currency";

    private static final NumberFormat CURRENCY_FMT = NumberFormat.getCurrencyInstance();
    private static final NumberFormat NUMBER_FMT = NumberFormat.getNumberInstance();

    private final Context context;

    private final List<String> currencies = new LinkedList<>();  // all supported currency codes

    private String defaultCurrency;

    @Inject
    public Currencies(Context context) {
        this.context = context;
        loadSupportedCurrencies();
    }

//    /**
//     * Get the list of all supported currencies.
//     *
//     * @return list of currency code
//     */
//    public List<String> getCurrencyList() {
//        return Collections.unmodifiableList(currencies);
//    }

    /**
     * Check if the given currency is supported
     *
     * @param currency currency code
     * @return supported or not
     */
    public boolean isSupported(String currency) {
        return currencies.contains(currency);
    }

    /**
     * Get default currency for the current user
     *
     * @return default currency
     */
    public String getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(String currency) {
        if (TextUtils.isEmpty(currency) ||
                TextUtils.equals(defaultCurrency, currency)) {
            return;
        }

        defaultCurrency = currency;
        context.getSharedPreferences(PREFS_APP_STATE, Context.MODE_PRIVATE)
                .edit()
                .putString(PREF_KEY_CURRENCY, currency)
                .apply();
    }

    private void loadSupportedCurrencies() {
        Collections.addAll(currencies,
                context.getResources().getStringArray(R.array.currency_codes));

        loadDefaultCurrency();
    }

    private void loadDefaultCurrency() {
        defaultCurrency = getSavedCurrency();
        if (TextUtils.isEmpty(defaultCurrency)) {
            defaultCurrency = getSysCurrency();
        }

        if (TextUtils.isEmpty(defaultCurrency) || !isSupported(defaultCurrency)) {
            TrackingUtils.trackEvent(VIEW_LIST_ITEM, "unsupported_currency", defaultCurrency, null);
            defaultCurrency = "USD";
        }
    }

    private String getSavedCurrency() {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_APP_STATE,
                Context.MODE_PRIVATE);
        return prefs.getString(PREF_KEY_CURRENCY, null);
    }

    private String getSysCurrency() {
        try {
            return Currency.getInstance(context.getResources().getConfiguration().locale).getCurrencyCode();
        } catch (Exception e) {
            L.e("get system default currency failed", e);
        }

        return null;
    }

    public static String formatCurrency(String currencyCode, Double amount) {
        currencyCode = isNotEmpty(currencyCode) ? currencyCode : "";
        String result;

        try {
            Currency currency = Currency.getInstance(currencyCode);
            CURRENCY_FMT.setCurrency(currency);
            CURRENCY_FMT.setMaximumFractionDigits(2);
            CURRENCY_FMT.setMinimumFractionDigits(0);
            result = CURRENCY_FMT.format(amount);
        } catch (IllegalArgumentException e) {
            L.e(e);
            NUMBER_FMT.setMaximumFractionDigits(0);
            NUMBER_FMT.setMinimumFractionDigits(0);
            result = currencyCode + NUMBER_FMT.format(amount);
        }

        return result;
    }

    public static String getCurrencySymbol(String currencyCode) {
        currencyCode = isNotEmpty(currencyCode) ? currencyCode : "USD";
        String symbol;

        try {
            Currency currency = Currency.getInstance(currencyCode);
            symbol = currency.getSymbol();
        } catch (IllegalArgumentException e) {
            L.e(e);
            symbol = "$";
        }

        return symbol;
    }
}
