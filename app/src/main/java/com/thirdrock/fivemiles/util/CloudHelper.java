package com.thirdrock.fivemiles.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.insthub.fivemiles.FiveMilesApp;
import com.madx.updatechecker.lib.UpdateRunnable;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.Emit1;
import com.thirdrock.framework.util.rx.SimpleObserver;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_APP_STATE;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_HAS_UPDATE;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_LAST_UPDATE_CHECK_TIME;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_LAST_UPDATE_CHECK_V;
import static com.insthub.fivemiles.MessageConstant.APP_HAS_UPDATE;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;
import static com.thirdrock.framework.util.Utils.getPackageInfo;

/**
 * Helper class for cloud image process
 * Created by ywu on 14-8-17.
 */
public final class CloudHelper {

    public static final String IMG_CLOUD_PREFIX = "http://res.cloudinary.com/fivemiles/image/";
    public static final String PARAM_OPT_AUTO_WEBP = "f_auto";
    private static final String REGEXP_IMG = "(/fivemiles/image/[^/]+/)(.*?)(\\.[\\w]+)$";

    private static final long UPDATE_CHECK_INTV = 86400000;
    private static boolean appHasUpdate;

    private CloudHelper() {}

    /**
     * 转换原始Cloudinary图片url，使用crop操作，以便得到合适尺寸的图片
     * @param originUrl 原始url
     * @param width 期望宽度
     * @param height 期望高度，可为null，根据宽度计算，保持长宽比
     * @param crop 裁剪模式，可为null，默认 'fill'
     * @param factor 放大因子，为保证retina屏幕效果，可获取2X图片，按1X显示，可为null，默认1
     * @return result image url
     * @see {http://cloudinary.com/documentation/image_transformations#crop_modes}
     */
    public static String toCropUrl(String originUrl,
                                   int width,
                                   @Nullable Integer height,
                                   String crop,
                                   Float factor) {
        if (originUrl == null || "null".equalsIgnoreCase(originUrl) || width <= 0) {
            return null;
        }

        int originWidth = width;
        Integer originHeight = height;

        if (height == null || height <= 0) {
            height = null;
        }

        if (width > ImageWidthCalcHelper.IMG_W_XXXL){
            // 超大图片直接用原图
            return toCropUrl(originUrl, crop);
        }else {
            width = ImageWidthCalcHelper.getOnlineImageWidth(width);
        }

        if (height != null) {
            height = Math.round(originHeight * width / (float)originWidth);
        }

        if (factor == null || factor <= 0) {
            factor = 1f;
        }

        if (factor > 1) {
            width = (int) Math.floor(width * factor);
            if (height != null) {
                height = (int) Math.floor(height * factor);
            }
        }

        String params = "w_" + width;
        if (height != null) {
            params += ",h_" + height;
        }
        params += "," + getOptimizeParam() + "," + getCropMode(crop);

        String url = originUrl.replaceAll("/w_[^/]*/", "/");
        url = url.replaceAll(REGEXP_IMG, "$1" + params + "/$2$3");
        L.v("loading cloud image w:%d -> %s", originWidth, url);
        return url;
    }

    /**
     * 转换原始Cloudinary图片url，使用crop操作，以便得到合适尺寸的图片
     * @param originUrl 原始url
     * @param width 期望宽度
     * @param height 期望高度，可为null，根据宽度计算，保留对比度
     * @param crop 裁剪模式，可为null，默认 'fill'
     * @param forceResize 是否强制缩放 强制缩放则不考虑预设的三种跨度
     * @return result image url
     * @see {http://cloudinary.com/documentation/image_transformations#crop_modes}
     */
    public static String toCropUrl(String originUrl,
                                   int width,
                                   Integer height,
                                   String crop,
                                   boolean forceResize) {
        if (!forceResize) {
            return toCropUrl(originUrl, width, height, crop, null);
        }

        if (originUrl == null || "null".equalsIgnoreCase(originUrl) || width <= 0) {
            return null;
        }

        if (height == null || height <= 0) {
            height = null;
        }

        String params = "w_" + width;
        if (height != null) {
            params += ",h_" + height;
        }
        params += "," + getOptimizeParam() + "," + getCropMode(crop);

        String url = originUrl.replaceAll("/w_[^/]*/", "/");
        url = url.replaceAll(REGEXP_IMG, "$1" + params + "/$2$3");
        L.v("loading cloud image w:%d -> %s", width, url);
        return url;
    }

    /**
     * 转换原始Cloudinary图片url，使用crop操作，以便得到合适尺寸的图片
     * @param originUrl 原始url
     * @param width 期望宽度, in dps
     * @param height 期望高度, in dps，可为null，根据宽度计算，保留对比度
     * @param crop 裁剪模式，可为null，默认 'pad'
     * @param factor 放大因子，为保证retina屏幕效果，可获取2X图片，按1X显示，可为null，默认1
     * @return result image url
     * @see {http://cloudinary.com/documentation/image_transformations#crop_modes}
     */
    public static String toCropUrlInPids(String originUrl,
                                         float width,
                                         Float height,
                                         String crop,
                                         Float factor) {
        // converts width/height to pixels
        int widthInPixels = DisplayUtils.toPixels(FiveMilesApp.getInstance(), width);
        Integer heightInPixels = height != null ?
                DisplayUtils.toPixels(FiveMilesApp.getInstance(), height) : null;
        return toCropUrl(originUrl, widthInPixels, heightInPixels, crop, factor);
    }

    /**
     * 转换原始Cloudinary图片url，只使用crop操作，保留原始尺寸
     * @param originUrl 原始url
     * @param crop 裁剪模式，可为null，默认 'fill'
     * @return result image url
     * @see {http://cloudinary.com/documentation/image_transformations#crop_modes}
     */
    public static String toCropUrl(String originUrl, String crop) {
        // TODO 加入f_auto参数
        return originUrl;
//        if (originUrl == null || "null".equalsIgnoreCase(originUrl)) {
//            return null;
//        }
//
//        String params = getCropMode(crop);
//        String url = originUrl.replaceAll("/[wc]_[^/]*/", "/");
//        return url.replaceAll(REGEXP_IMG, "$1" + params + "/$2$3");
    }

    // http://cloudinary.com/blog/transparent_webp_format_cdn_delivery_based_on_visitors_browsers
    private static String getOptimizeParam() {
        return PARAM_OPT_AUTO_WEBP;
    }

    private static String getCropMode(String crop) {
        if (isEmpty(crop)) {
            crop = "fill";
        }

        return "c_" + crop.replaceAll(",g_face", "");  // 暂不使用脸部识别功能
    }

    public static void checkUpdate(final Context context) {
        Observable.create(new Emit1<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                SharedPreferences prefs = context.getSharedPreferences(PREFS_APP_STATE, Context.MODE_PRIVATE);
                int appVer = getPackageInfo().versionCode;
                int lastAppVer = prefs.getInt(PREF_KEY_LAST_UPDATE_CHECK_V, 0);  // 上次检查时用户正在使用的app版本
                long lastChecked = prefs.getLong(PREF_KEY_LAST_UPDATE_CHECK_TIME, 0);
                long now = System.currentTimeMillis();
                long dataAge = now - lastChecked;
                boolean hasUpdate = prefs.getBoolean(PREF_KEY_HAS_UPDATE, false);

                if (appVer != lastAppVer || dataAge > UPDATE_CHECK_INTV) {
                    hasUpdate = doCheckUpdate(context);

                    prefs.edit()
                            .putBoolean(PREF_KEY_HAS_UPDATE, hasUpdate)
                            .putInt(PREF_KEY_LAST_UPDATE_CHECK_V, appVer)
                            .putLong(PREF_KEY_LAST_UPDATE_CHECK_TIME, now)
                            .apply();
                }

                return hasUpdate;
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean hasUpdate) {
                        L.w("app has new version: %s", hasUpdate);
                        appHasUpdate = hasUpdate;

                        Bundle data = new Bundle();
                        data.putBoolean(PREF_KEY_HAS_UPDATE, hasUpdate);
                        EventUtils.post(APP_HAS_UPDATE, data);
                    }
                });
    }

    private static boolean doCheckUpdate(Context context) {
        UpdateRunnable updateChecker = new UpdateRunnable(context).force(true);
        return updateChecker.isUpdateAvailable();
    }

    public static boolean isAppHasUpdate() {
        return appHasUpdate;
    }
}
