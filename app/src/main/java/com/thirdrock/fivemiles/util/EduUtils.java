package com.thirdrock.fivemiles.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;

import com.insthub.fivemiles.FiveMilesApp;

import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_APP_STATE;
import static com.thirdrock.fivemiles.util.EduScreen.NULL_OK_VIEW;
import static com.thirdrock.fivemiles.util.EduScreen.ViewController;

/**
 * Utilities for user education.
 * Created by ywu on 14-10-10.
 */
public final class EduUtils {

    private EduUtils() {

    }

    private static Context getContext() {
        return FiveMilesApp.getInstance();
    }

    private static SharedPreferences getSharedPreferences() {
        return getContext().getSharedPreferences(PREFS_APP_STATE, Context.MODE_PRIVATE);
    }

    /**
     * Check whether the specified education screen should be displayed
     *
     * @param screenKey screen key
     * @param screenVersion desired screen version
     * @return should show or not
     */
    public static boolean shouldShow(String screenKey, int screenVersion) {
        SharedPreferences shared = getSharedPreferences();
        int currVer = shared.getInt(screenKey, 0);
        return screenVersion > currVer;
    }

    /**
     * Opposite of #shouldShow
     */
    public static boolean shouldNotShow(String screenKey, int screenVersion) {
        return !shouldShow(screenKey, screenVersion);
    }

    /**
     * Mark the specified education screen as already been displayed
     *
     * @param screenKey screen key
     * @param screenVersion screen version that has been shown
     */
    public static void markShown(String screenKey, int screenVersion) {
        getSharedPreferences().edit().putInt(screenKey, screenVersion).apply();
    }

    /**
     * Show the given education screen.
     * @param context context
     * @param contentViewRid content view resource id
     * @param screenKey screen key
     * @param screenVersion desired screen version
     * @return EduScreen
     */
    public static EduScreen showEduScreen(Context context,
                                          int contentViewRid,
                                          String screenKey,
                                          int screenVersion) {

        markShown(screenKey, screenVersion);
        return new EduScreen(context, contentViewRid).show();
    }

    /**
     * Show the given education screen.
     * @param context context
     * @param contentViewRid content view resource id
     * @param okViewRid ok view resource id
     * @param screenKey screen key
     * @param screenVersion desired screen version
     * @return EduScreen
     */
    public static EduScreen showEduScreen(Context context,
                                          int contentViewRid,
                                          int okViewRid,
                                          String screenKey,
                                          int screenVersion) {

        markShown(screenKey, screenVersion);
        return new EduScreen(context, contentViewRid, okViewRid).show();
    }

    /**
     * Show the given education screen.
     * @param context context
     * @param style screen style
     * @param contentViewRid content view resource id
     * @param screenKey screen key
     * @param screenVersion desired screen version
     * @param viewController controller to customize the screen
     * @return EduScreen
     */
    public static EduScreen showEduScreen(Context context,
                                          int style,
                                          int contentViewRid,
                                          String screenKey,
                                          int screenVersion,
                                          ViewController viewController) {

       return showEduScreen(context, style, contentViewRid, NULL_OK_VIEW, screenKey, screenVersion, viewController);
    }

    /**
     * Show the given education screen.
     * @param context context
     * @param style screen style
     * @param contentViewRid content view resource id
     * @param okViewRid ok view resource id
     * @param screenKey screen key
     * @param screenVersion desired screen version
     * @param viewController controller to customize the screen
     * @return EduScreen
     */
    public static EduScreen showEduScreen(Context context,
                                          int style,
                                          int contentViewRid,
                                          int okViewRid,
                                          String screenKey,
                                          int screenVersion,
                                          ViewController viewController){
        markShown(screenKey, screenVersion);
        return new EduScreen(context, style, contentViewRid, okViewRid, viewController).show();
    }

    /**
     * Show the given user tips.
     */
    public static EduTip showEduTip(final String screenKey, final int screenVersion, EduTip tip) {
        return tip.show().setOnCloseListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 点击关闭才记录
                markShown(screenKey, screenVersion);
            }
        });
    }
}
