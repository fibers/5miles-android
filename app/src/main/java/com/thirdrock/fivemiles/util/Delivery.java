package com.thirdrock.fivemiles.util;

import com.thirdrock.domain.EnumDeliveryType;

import java.io.Serializable;

/**
 * Delivery method.
 * Created by ywu on 14/11/20.
 */
public class Delivery implements Serializable {

    EnumDeliveryType value;

    String name = "";

    public Delivery(EnumDeliveryType value, String name) {
        this.value = value;
        this.name = name;
    }

    public EnumDeliveryType getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof Delivery)) {
            return false;
        }

        Delivery that = (Delivery) o;
        return that.value == this.value;
    }

    @Override
    public int hashCode() {
        return value.ordinal();
    }
}
