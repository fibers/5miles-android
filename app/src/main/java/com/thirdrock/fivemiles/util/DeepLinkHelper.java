package com.thirdrock.fivemiles.util;

import android.content.Context;
import android.content.Intent;
import com.insthub.fivemiles.Activity.OtherProfileActivity;
import com.insthub.fivemiles.ConstantS;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.Utils.MD5Encoder;
import com.thirdrock.domain.EnumShareType;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.framework.util.L;

import org.json.JSONException;
import org.json.JSONObject;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_ID;

/**
 * Helper class for branch deep link
 * Created by jubin on 23/12/14.
 */
public class DeepLinkHelper {
    public static final String BRANCH_DOMAIN_NAME = "https://bnc.lt/";
    private Context appContext;
    private String branchAliasPart1;
    private String branchAliasPart2;

    public DeepLinkHelper(Context pAppContext, String pBranchAliasPart1, String pBranchAliasPart2) {
        this.appContext = pAppContext;
        this.branchAliasPart1 = pBranchAliasPart1;
        this.branchAliasPart2 = pBranchAliasPart2;
    }

    public DeepLinkHelper(Context pAppContext) {

        this(pAppContext,
                pAppContext.getString(R.string.branch_url_prefix),
                "v2");        // upper case will be transformed to lower case in Branch
    }

    /**
     * 为了提升性能，这里不必等Branch的callback即返回url alias
     *
     * @param userId    当前用户的id
     * @param shareType 分享单品，店铺或邀请朋友
     * @param eventId   跟shareType有关，若shareType为ITEM则为itemId，SHOP则为shopId，INVITE则为null
     * @return Branch url alias
     */
    public String generateBranchShortUrl(String userId, EnumShareType shareType, String eventId
            , final OnDeepLinkCreateListener onDeepLinkCreateListener) {
        final String alias = this.getBranchShortUrlAlias(userId, shareType, eventId);
        String channel = "android";
        String feature = Branch.FEATURE_TAG_SHARE;

        // you can access this data from any instance that installs or opens the app from this link (amazing...)
        JSONObject dataToInclude = new JSONObject();
        try {
            if (shareType == null) {
                shareType = EnumShareType.UNKNOWN;
            }
            if (shareType == EnumShareType.UNKNOWN || shareType == EnumShareType.INVITE) {
                eventId = null;               // 使用JSONObject.isNULL(String name)可以判空
            }

            String desktopUrl = ConstantS.DESKTOP_HOME_URL;
            if (shareType == EnumShareType.ITEM){
                desktopUrl = ConstantS.DESKTOP_HOME_URL + "/item/" + eventId;
            }else if (shareType == EnumShareType.SHOP){
                desktopUrl = ConstantS.DESKTOP_HOME_URL + "/person/" + userId;    // demo: http://5milesapp.com/person/a3rK41wRDp
            }

            dataToInclude.put(ConstantS.USER_ID, userId);
            dataToInclude.put(ConstantS.SHARE_TYPE, shareType.toString().toLowerCase());
            dataToInclude.put(ConstantS.EVENT_ID, eventId);
            dataToInclude.put(ConstantS.BRANCH_DESKTOP_URL, desktopUrl);
            dataToInclude.put(ConstantS.BRANCH_OG_URL, desktopUrl);       // 测试环境，facebook抓不到图，测试此变量需在生产环境

            // 兼容iOS
            switch (shareType){
                case ITEM:
                    dataToInclude.put("itemid", eventId);
                    dataToInclude.put("userid", userId);
                    break;
                case SHOP:
                    dataToInclude.put("userid", userId);
                    break;
                default:
                    break;
            }
        } catch (JSONException ex) {
            L.e(ex);
        }

        Branch branch = Branch.getInstance(appContext);
        branch.getShortUrl(alias, channel, feature, null, dataToInclude, new Branch.BranchLinkCreateListener() {

            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (onDeepLinkCreateListener == null) {
                    return;
                }
                if (error == null) {
                    onDeepLinkCreateListener.onLinkCreateEvent(true, url, null);
                } else if (error instanceof Branch.BranchDuplicateUrlError) {           // 重复了就直接返回alias
                    onDeepLinkCreateListener.onLinkCreateEvent(true, BRANCH_DOMAIN_NAME + alias, null);
                } else {
                    onDeepLinkCreateListener.onLinkCreateEvent(false, null, error.getMessage());
                }
            }
        });
        branch.resetUserSession();  // getShortUrl will automatically init Session, so close it here

        return BRANCH_DOMAIN_NAME + alias;
    }
    //32位md5 输入：currentUserIDcurrentEventStringcurrentEventID
    private String getBranchShortUrlAlias(String userId, EnumShareType shareType, String eventId) {
        String branchAliasPart3 = this.getBranchALiasPart3(userId, shareType, eventId);

        return branchAliasPart1 + '-' + branchAliasPart2 + '-' + branchAliasPart3;
    }

    private String getBranchALiasPart3(String userId, EnumShareType shareType, String eventId) {
        if (shareType == null) {
            shareType = EnumShareType.UNKNOWN;
            L.e("Enum shareType is null in getBranchAliasPart3()");
        }

        String md5InputStr = userId + shareType.name() + eventId;
        String fullMD5EncodedStr = MD5Encoder.encode(md5InputStr);

        return fullMD5EncodedStr.substring(0, 6);
    }

    public interface OnDeepLinkCreateListener {
        void onLinkCreateEvent(boolean isSuccess, String url, String errorMsg);
    }

    /**
     * 目前只用Activity来处理DeepLink
     * 为兼容v2.3的Android和iOS，增加了很多代码。这些代码不必优化，因为v2.3的用户量还不大，后面用户逐渐升级上去后直接删掉就好了。
     * https://github.com/3rdStone/5miles-apps/issues/34
     * @param referringParams Branch中携带的params
     */
    public static void handleDeepLink(JSONObject referringParams){
        L.d("branch link: handling deep link");
        if (referringParams != null && referringParams.length() > 0) {
            try {
                if (referringParams.has(ConstantS.SHARE_TYPE) && !referringParams.isNull(ConstantS.SHARE_TYPE)){        // 含有share_type，v2.4
                    String shareTypeStr = referringParams.getString(ConstantS.SHARE_TYPE);
                    EnumShareType shareType = EnumShareType.valueOf(shareTypeStr.toUpperCase());

                    switch (shareType) {
                        case INVITE:
                            L.d("branch invite activated");
                            // empty: invite 直接进MainTabActivity，不需要任何操作
                            break;

                        case ITEM:
                            String itemId = referringParams.getString(ConstantS.EVENT_ID);
                            Intent itemIntent = new Intent(FiveMilesApp.appCtx, ItemActivity.class);
                            itemIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            itemIntent.putExtra(ACT_PARA_ITEM_ID, itemId);
                            FiveMilesApp.appCtx.startActivity(itemIntent);
                            break;

                        case SHOP:
                            String shopId = referringParams.getString(ConstantS.EVENT_ID);
                            Intent shopIntent = new Intent(FiveMilesApp.appCtx, OtherProfileActivity.class);
                            shopIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            shopIntent.putExtra(ACT_PARA_USER_ID, shopId);      // user id == shop id
                            FiveMilesApp.appCtx.startActivity(shopIntent);
                            break;

                        case UNKNOWN:
                            L.w("unknow share type shows!");
                            break;
                        default:
                    }
                } else if (referringParams.has("shareType") && !referringParams.isNull("shareType")){           // 若不含share_type，只有shareType，则为v2.3 Android
                    String shareTypeStr = referringParams.getString("shareType");

                    EnumShareType shareType = EnumShareType.valueOf(shareTypeStr.toUpperCase());

                    switch (shareType) {
                        case INVITE:
                            L.d("branch invite activated");
                            // empty: invite 直接进MainTabActivity，不需要任何操作
                            break;

                        case ITEM:
                            String itemId = referringParams.getString("eventId");
                            Intent intent = new Intent(FiveMilesApp.appCtx, ItemActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(ACT_PARA_ITEM_ID, itemId);
                            FiveMilesApp.appCtx.startActivity(intent);
                            break;

                        case SHOP:
                            String shopId = referringParams.getString("eventId");
                            Intent shopIntent = new Intent(FiveMilesApp.appCtx, OtherProfileActivity.class);
                            shopIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            shopIntent.putExtra(ACT_PARA_USER_ID, shopId);      // user id == shop id
                            FiveMilesApp.appCtx.startActivity(shopIntent);
                            break;

                        case UNKNOWN:
                            break;
                        default:
                    }
                } else {
                    if (referringParams.has("itemid")){             // 含有itemid，分享单品
                        if (referringParams.isNull("itemid"))   return;

                        String itemId = referringParams.getString("itemid");
                        Intent intent = new Intent(FiveMilesApp.appCtx, ItemActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(ACT_PARA_ITEM_ID, itemId);
                        FiveMilesApp.appCtx.startActivity(intent);
                    } else if (referringParams.has("userid")) {     // 不含itemid，含有userid，分享店铺
                        if (referringParams.isNull("userid"))   return;

                        String shopId = referringParams.getString("userid");
                        Intent shopIntent = new Intent(FiveMilesApp.appCtx, OtherProfileActivity.class);
                        shopIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        shopIntent.putExtra(ACT_PARA_USER_ID, shopId);      // user id == shop id
                        FiveMilesApp.appCtx.startActivity(shopIntent);
                    }
                }
            } catch (Exception e) {
                L.e("exception caught retrieving Branch params, e.getMessage(): " + e.getMessage());
            } finally {
                FiveMilesApp.getInstance().clearReferringParams();
            }
        }
    }

}
