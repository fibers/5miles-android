package com.thirdrock.fivemiles.util;

import android.app.Activity;
import android.content.Intent;

import com.insthub.fivemiles.SESSION;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.common.phone.VerifyPhoneActivity;
import com.thirdrock.fivemiles.framework.dialog.SelectionListDialog;
import com.thirdrock.fivemiles.framework.view.GenericDialog;
import com.thirdrock.framework.util.session.Action;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Queue;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_VERIFICATION_REMIND_TIME;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_REMIND_VERIFICATION;
import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_APP_STATE;
import static com.insthub.fivemiles.FiveMilesAppConst.PREF_KEY_DONT_REMIND_VERIFICATION;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_ITEM;

/**
 * Utils for handling pending actions on activity resume
 * Created by ywu on 15/1/21.
 */
public class PendingActionHelper {

    private static final int REQ_VERIFY_PHONE = 1;

    private static final int IDX_VERIFY_NOW = 0;
    private static final int IDX_VERIFY_NEVER = 2;

    private static final int DUR_2_DAYS = 86400000 * 2;

    private Activity activity;

    private GenericDialog dlgVerificationReminder;

    public PendingActionHelper(Activity activity) {
        this.activity = activity;
    }

    public void handlePendingActions(String key) {
        Queue<Action> actions = SESSION.getInstance().getPendingActions(key);
        Iterator<Action> itActions = actions.iterator();
        while (itActions.hasNext()) {
            Action action = itActions.next();
            if (handleAction(action)) {
                itActions.remove();
            }
        }

        SESSION.getInstance().save();
    }

    private boolean handleAction(Action action) {
        boolean handled = false;
        String act = action.getAction();

        if (ACT_REMIND_VERIFICATION.equals(act)) {
            handled = remindVerification(action);
        }
//        else if (ACT_GOTO_HOME.equals(act)) {
//            handled = refreshHome(action);
//        }

        return handled;
    }

//    private boolean refreshHome(Action action) {
//        EventUtils.post(REFRESH_HOME, 0);
//        return true;
//    }

    private boolean remindVerification(Action action) {
        if (shouldNotRemindVerification()) {
            return true;  // drop this action
        }

        if (!isTimeToRemindVerification(action)) {
            return false;  // skip this action till next time
        }

        dlgVerificationReminder = new SelectionListDialog(activity)
                .setMessage(getString(R.string.msg_remind_verification))
                .setOptions(Arrays.asList(
                        getString(R.string.lbl_verify_now),
                        getString(R.string.lbl_verify_later),
                        getString(R.string.lbl_verify_never)))
                .setOnSelectionListener(new SelectionListDialog.OnSelectionListener() {
                    @Override
                    public void onItemSelected(int position) {
                        if (position == IDX_VERIFY_NEVER) {
                            removeVerificationReminder();
                        } else if (position == IDX_VERIFY_NOW) {
                            startActivityForResult(new Intent(activity, VerifyPhoneActivity.class), REQ_VERIFY_PHONE);
                            remindVerificationLater();
                        } else {
                            remindVerificationLater();
                        }
                    }
                })
                .setOnCancelListener(new SelectionListDialog.OnCancelListener() {
                    @Override
                    public void onCancel() {
                        remindVerificationLater();
                    }
                }).show();

        return true;
    }

    private void removeVerificationReminder() {
        removePendingActions(VIEW_ITEM);
        activity.getSharedPreferences(PREFS_APP_STATE, Activity.MODE_PRIVATE).edit()
                .putBoolean(PREF_KEY_DONT_REMIND_VERIFICATION, true).apply();
    }

    private boolean shouldNotRemindVerification() {
        boolean neverRemind = activity.getSharedPreferences(PREFS_APP_STATE, Activity.MODE_PRIVATE)
                .getBoolean(PREF_KEY_DONT_REMIND_VERIFICATION, false);

        return neverRemind || SESSION.getInstance().isVerified() ||
                (dlgVerificationReminder != null && dlgVerificationReminder.isShowing());
    }

    private boolean isTimeToRemindVerification(Action action) {
        long lastRemindTime = action.getLong(ACT_PARA_VERIFICATION_REMIND_TIME);
        long age = System.currentTimeMillis() - lastRemindTime;
        return lastRemindTime == 0 || age >= DUR_2_DAYS;
    }

    private void remindVerificationLater() {
        queuePendingAction(VIEW_ITEM, new Action(ACT_REMIND_VERIFICATION, ACT_REMIND_VERIFICATION)
                .putLong(ACT_PARA_VERIFICATION_REMIND_TIME, System.currentTimeMillis()));
    }

    private void queuePendingAction(String key, Action action) {
        SESSION.getInstance().queuePendingAction(key, action).save();
    }

    private void removePendingActions(String key) {
        SESSION.getInstance().removePendingActions(key).save();
    }

    private void startActivityForResult(Intent intent, int requestCode) {
        activity.startActivityForResult(intent, requestCode);
    }

    public void onPause() {
        if (dlgVerificationReminder != null) {
            dlgVerificationReminder.dismiss();
            remindVerificationLater();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // empty
    }

    private String getString(int resId) {
        return activity.getString(resId);
    }
}
