package com.thirdrock.fivemiles.util;

import android.content.Context;

import com.insthub.fivemiles.Protocol.HOME_OBJECTS;
import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;

/**
 * Helper class handling item state
 * Created by jubin on 7/2/15.
 */
public class StateUtils {
//    public static String getItemDesc(Context context, EnumItemState state) {
//        switch (state) {
//            case LISTING:
//                return context.getString(R.string.item_state_desc_listing);
//            case SOLD:
//                return context.getString(R.string.item_state_desc_sold);
//            case RECEIVED:
//            case SELLER_RATED:
//                L.w(state.toString() + " is abandoned and should not show here");
//                return context.getString(R.string.item_state_desc_sold);    // 使用Sold状态替代
//            case UNAPPROVED:
//                return context.getString(R.string.item_state_desc_unapproved);
//            case UNAVAILABLE:
//                return context.getString(R.string.item_state_desc_unavailable);
//            case UNLISTED:
//                return context.getString(R.string.item_state_desc_unlisted);
//            default:
//                break;
//        }
//
//        return context.getString(R.string.item_state_desc_listing);
//    }

    /**
     * 兼容老代码
     * TODO 替换HOME_OBJECT
     */
    public static String getItemDesc(Context context, HOME_OBJECTS.State state) {
        switch (state) {
            case LISTING:
                return context.getString(R.string.item_state_desc_listing);
            case SOLD:
                return context.getString(R.string.item_state_desc_sold);
            case RECEIVED:
            case SELLER_RATED:
                L.w(state.toString() + " is abandoned and should not show here");
                return context.getString(R.string.item_state_desc_sold);    // 使用Sold状态替代
            case UNAPPROVED:
                return context.getString(R.string.item_state_desc_unapproved);
            case UNAVAILABLE:
                return context.getString(R.string.item_state_desc_unavailable);
            case UNLISTED:
                return context.getString(R.string.item_state_desc_unlisted);
            default:
                break;
        }

        return context.getString(R.string.item_state_desc_listing);
    }

    // TODO 替换HOME_OBJECT
    public static int getStatusImageId(HOME_OBJECTS.State state) {
        switch (state) {
            case LISTING:
                return R.drawable.item_selling;
            case RECEIVED:
            case SELLER_RATED:
                // RECEIVED & SELLER_RATED can be treated as SOLD
                L.w(state.toString() + " is abandoned and should not show here");
            case UNAPPROVED:
            case SOLD:
            case UNAVAILABLE:
            case UNLISTED:
                return R.drawable.item_not_on_sale;
            default:
                break;
        }

        return R.drawable.item_selling;
    }
}
