package com.thirdrock.fivemiles.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.exception.NetworkException;
import com.thirdrock.framework.rest.RestObserver;

import static com.thirdrock.framework.rest.RestObserver.ACT_REQ_FAILURE;
import static com.thirdrock.framework.rest.RestObserver.ACT_RESP_RECEIVED;
import static com.thirdrock.framework.rest.RestObserver.EXTRA_THROWABLE;

/**
 * Api return time statistics
 * Created by ywu on 15/3/6.
 */
public class ApiStatsReceiver extends BroadcastReceiver {

    public static final String API_EVENTS = "api_stats";

    @Override
    public void onReceive(Context context, Intent intent) {
        String act = intent.getAction();

        if (ACT_REQ_FAILURE.equals(act)) {
            onRequestFailure(intent);
        } else if (ACT_RESP_RECEIVED.equals(act)) {
            onResponseReceived(context, intent);
        }
    }

    private void onRequestFailure(Intent intent) {
        Throwable e = (Throwable) intent.getSerializableExtra(EXTRA_THROWABLE);

        if (e instanceof NetworkException) {
            TrackingUtils.trackNetworkException((NetworkException) e);
        } else {
            TrackingUtils.trackException(e);
        }
    }

    private void onResponseReceived(Context context, Intent intent) {
        String req = intent.getStringExtra(RestObserver.EXTRA_REQ_ID);
        long delay = intent.getLongExtra(RestObserver.EXTRA_RESP_DELAY, 0);
        long threshold = context.getResources().getInteger(R.integer.track_api_delay_threshold);

        if (!TextUtils.isEmpty(req) && delay > threshold) {
            TrackingUtils.trackTiming(API_EVENTS, req, delay);
        }
    }
}
