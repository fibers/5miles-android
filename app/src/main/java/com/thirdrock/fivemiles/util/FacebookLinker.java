package com.thirdrock.fivemiles.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.insthub.fivemiles.Activity.MyProfileActivity;
import com.insthub.fivemiles.Activity.SignOutAction;
import com.insthub.fivemiles.Activity.WelcomeActivity;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.Model.LoginModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.SESSION;
import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_LINK_FB;
import static com.insthub.fivemiles.FiveMilesAppConst.FB_PERMISSION_EMAIL;
import static com.insthub.fivemiles.FiveMilesAppConst.FB_PERMISSION_FRIENDS;
import static com.insthub.fivemiles.FiveMilesAppConst.FB_PERMISSION_PUBLIC;

/**
 * Helper for linking facebook account
 * Created by ywu on 15/1/6.
 */
public class FacebookLinker extends UiLifecycleHelper implements BusinessResponse,
        LoginButton.UserInfoChangedCallback {
    public static final int REQ_LINK = 1;
    public static final int REQ_UNLINK = 2;
    public static final int FETCH_USER_INFO = 3;

    private static AlertDialog relinkConfirmDlg;

    private Activity activity;
    private LoginButton btnFbLogin;
    private List<String> readPermissions, publishPermissions;

    private LoginModel loginModel;
    private Callback callback;

    public interface Callback {
        /**
         * 控制何时需要处理Facebook回调，避免重复提交请求
         */
        boolean shouldSubmitLinkRequest();

        void onCanceledOrFailed(int requestCode);

        void onComplete(int requestCode);
    }

    public FacebookLinker(Activity activity, LoginButton loginButton, Callback callback) {
        this(activity, loginButton, Arrays.asList(FB_PERMISSION_PUBLIC, FB_PERMISSION_EMAIL, FB_PERMISSION_FRIENDS), null, callback);
    }

    public FacebookLinker(Activity activity, LoginButton loginButton, List<String> readPermissions,
                          List<String> publishPermissions, Callback callback) {
        super(activity, null);
        this.activity = activity;
        this.btnFbLogin = loginButton;
        this.readPermissions = readPermissions;
        this.publishPermissions = publishPermissions;
        this.callback = callback;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (publishPermissions != null) {
            btnFbLogin.setPublishPermissions(publishPermissions);
        }

        if (readPermissions != null) {
            btnFbLogin.setReadPermissions(readPermissions);
        }

        loginModel = new LoginModel(activity);
        loginModel.addResponseListener(this);
    }

    public void link() {
        Session fbSession = Session.getActiveSession();
        if (fbSession != null && fbSession.isOpened()) {
            loadFbBasicInfo();
        } else {
            btnFbLogin.performClick();
        }

        btnFbLogin.setUserInfoChangedCallback(this);
    }

    public void unlink() {
        loginModel.unlinkFacebook();
    }

    /*
      Callback when Facebook user basic profile is fetched.
    */
    @Override
    public void onUserInfoFetched(GraphUser user) {
        if (callback != null && !callback.shouldSubmitLinkRequest()) {
            return;
        }

        if (user != null) {
            L.d("FB userInfo fetched, id: %s, name: %s", user.getId(), user.getName());
            submitLinkReq(Session.getActiveSession(), user);

            FiveMilesApp.loadAdvertisingInfo();  // refresh advertising info
        } else {
            SessionState fbSessionState = Session.getActiveSession().getState();
            // 当facebook session为OPENING时，不提示错误，因为此后fb会尝试连接登录；只有登录失败才真正提示错误
            if (callback != null && fbSessionState == SessionState.CLOSED_LOGIN_FAILED) {
                callback.onCanceledOrFailed(FETCH_USER_INFO);
            }
        }
    }

    private void loadFbBasicInfo() {
        Session fbSession = Session.getActiveSession();
        if (fbSession == null || !fbSession.isOpened()) {
            return;
        }

        Request.newMeRequest(fbSession, new Request.GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser user, Response response) {
                onUserInfoFetched(user);
            }
        }).executeAsync();
    }

    private void submitLinkReq(Session session, GraphUser user) {
        loginModel.linkFacebook(user.getId(),
                session.getAccessToken(),
                session.getExpirationDate());
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) throws JSONException {
        int req = url.startsWith(ApiInterface.LINK_FACEBOOK) ? REQ_LINK : REQ_UNLINK;
        SESSION.getInstance()
                .fbVerified(req == REQ_LINK)
                .fbTokenExpires(req == REQ_LINK ?
                        Session.getActiveSession().getExpirationDate().getTime() : 0)
                .save();

        if (callback != null) {
            if (jo != null) {
                callback.onComplete(req);
            } else {
                callback.onCanceledOrFailed(req);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_CANCELED &&
                Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE == requestCode) {

            if (callback != null) {
                callback.onCanceledOrFailed(REQ_LINK);
            }
        }
    }

    /**
     * 如果当前会话存在facebook token，则检查token是否过期，做相应处理
     *
     * @param context Context
     */
    public static void validateToken(final Context context) {
        final SESSION session = SESSION.getInstance();
        boolean expired = session.isFbTokenExpired();

        if (!expired) {
            // is not facebook verified or token is valid
            return;
        }

        if (session.isFbLogin()) {
            // facebook登录，则注销重新登录
            new SignOutAction(context).signOut(WelcomeActivity.class);
        } else if (session.isTimeToRemindFbToken() &&
                (relinkConfirmDlg == null || !relinkConfirmDlg.isShowing())) {
            // 否则提示重新link facebook
            relinkConfirmDlg = new AlertDialog.Builder(context)
                    .setTitle(R.string.title_fb_token_expires)
                    .setMessage(R.string.msg_fb_token_expires)
                    .setPositiveButton(R.string.lbl_fb_link_now, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            session.fbTokenRemindDisabled(false).save();
                            context.startActivity(new Intent(context, MyProfileActivity.class)
                                    .setAction(ACT_LINK_FB));
                            relinkConfirmDlg = null;
                        }
                    })
                    .setNegativeButton(R.string.share_with_friends_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            session.fbTokenRemindDisabled(true).save();
                            relinkConfirmDlg = null;
                        }
                    })
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            session.fbTokenRemindDisabled(true).save();
                            relinkConfirmDlg = null;
                        }
                    })
                    .show();
        }
    }

}
