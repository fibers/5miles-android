package com.thirdrock.fivemiles.di;

import com.external.androidquery.callback.AbstractAjaxCallback;
import com.insthub.fivemiles.FiveMilesApp;
import com.thirdrock.domain.AppConfig;
import com.thirdrock.framework.rest.HeaderBuilder;
import com.thirdrock.framework.util.Utils;
import com.thirdrock.repository.CategoriesRepository;
import com.thirdrock.repository.GcmRepository;
import com.thirdrock.repository.GeoLocationReportRepository;
import com.thirdrock.repository.ItemRepository;
import com.thirdrock.repository.OfferRepository;
import com.thirdrock.repository.PrefsRepository;
import com.thirdrock.repository.ProfileRepository;
import com.thirdrock.repository.ReviewRepository;
import com.thirdrock.repository.UserRepository;
import com.thirdrock.repository.impl.BodyParserFactoryImpl;
import com.thirdrock.repository.impl.CategoriesRepositoryImpl;
import com.thirdrock.repository.impl.GcmRepositoryImpl;
import com.thirdrock.repository.impl.GeoLocationReportRepositoryImpl;
import com.thirdrock.repository.impl.ItemRepositoryImpl;
import com.thirdrock.repository.impl.OfferRepositoryImpl;
import com.thirdrock.repository.impl.PrefsRepositoryImpl;
import com.thirdrock.repository.impl.ProfileRepositoryImpl;
import com.thirdrock.repository.impl.ReviewRepositoryImpl;
import com.thirdrock.repository.impl.UserRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.insthub.fivemiles.Protocol.ApiInterface.API_HOST_GRAY;
import static com.insthub.fivemiles.Protocol.ApiInterface.API_HOST_PROD;
import static com.insthub.fivemiles.Protocol.ApiInterface.API_HOST_TEST;
import static com.thirdrock.repository.GcmRepository.GCM_BASE_URL;

/**
 * Created by ywu on 14-10-7.
 */
@Module(library = true)
public final class RepositoryModule {

    public static String getApiBase() {
        // isA? A : (isB? B : C)
        return Utils.isDebug() ? API_HOST_TEST : (Utils.isGray() ? API_HOST_GRAY : API_HOST_PROD);
    }

    public static String getBaseUrl() {
        String base = getApiBase();
        AppConfig cfg = FiveMilesApp.getAppConfig();
        if (cfg == null) {
            throw new IllegalStateException("AppConfig is null, check your DI configurations");
        }
        return base.replaceAll("/api/v\\d+", "/api/" + cfg.getApiVersion());
    }

    public static HeaderBuilder getDefaultHeaderBuilder() {
        return new HeaderBuilder() {
            @Override
            protected void prepareHeaders() {
                append(AbstractAjaxCallback.getDefaultHeaders());
            }
        };
    }

    @Singleton
    @Provides
    ItemRepository provideItemRepository() {
        return new ItemRepositoryImpl(getBaseUrl(), getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }

    @Singleton
    @Provides
    PrefsRepository providePrefsRepository() {
        return new PrefsRepositoryImpl(getBaseUrl(), getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }

    @Singleton
    @Provides
    OfferRepository provideOfferRepository() {
        return new OfferRepositoryImpl(getBaseUrl(), getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }

    @Singleton
    @Provides
    CategoriesRepository provideCategoriesRepository() {
        return new CategoriesRepositoryImpl(getBaseUrl(), getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }

    /**
     * Note: ReviewRepository维护特定用户的状态数据，因此请不要使用singleton
     */
    @Provides
    ReviewRepository provideReviewRepository() {
        return new ReviewRepositoryImpl(getBaseUrl(), getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }

    @Provides
    ProfileRepository provideProfileRepository(){
        return new ProfileRepositoryImpl(getBaseUrl(), getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }

    @Singleton
    @Provides
    UserRepository provideUserRepository() {
        return new UserRepositoryImpl(getBaseUrl(), getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }

    @Singleton
    @Provides
    GcmRepository provideGcmRepository() {
        return new GcmRepositoryImpl(GCM_BASE_URL, getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }

    @Singleton
    @Provides
    GeoLocationReportRepository provideGeoLocationReportRepository() {
        return new GeoLocationReportRepositoryImpl(
                RepositoryModule.getBaseUrl(),
                RepositoryModule.getDefaultHeaderBuilder(),
                BodyParserFactoryImpl.getInstance());
    }
}
