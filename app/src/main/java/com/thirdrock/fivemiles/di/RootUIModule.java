package com.thirdrock.fivemiles.di;

import com.insthub.fivemiles.Activity.MakeOfferActivity;
import com.thirdrock.fivemiles.common.brand.BrandActivity;
import com.thirdrock.fivemiles.common.phone.VerifyPhoneActivity;
import com.thirdrock.fivemiles.item.FeaturedCollectionActivity;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.login.ZipcodeVerifyActivity;
import com.thirdrock.fivemiles.main.home.HomeActivity;
import com.thirdrock.fivemiles.main.home.HomeFollowingFragment;
import com.thirdrock.fivemiles.main.home.HomeNearbyFragment;
import com.thirdrock.fivemiles.main.listing.ListItemActivity;
import com.thirdrock.fivemiles.offer.BuyItemActivity;
import com.thirdrock.fivemiles.profile.MyItemsActivity;
import com.thirdrock.fivemiles.profile.setting.NotificationsActivity;
import com.thirdrock.fivemiles.reco.SellersNearbyActivity;
import com.thirdrock.fivemiles.review.ReviewActivity;
import com.thirdrock.fivemiles.review.ReviewListActivity;
import com.thirdrock.fivemiles.search.SearchActivity;

import dagger.Module;

/**
 * Root DI scope for all UI related modules
 * Created by ywu on 14-10-11.
 */
@Module(complete = false,
        injects = {
                HomeActivity.class,
                HomeNearbyFragment.class,
                HomeFollowingFragment.class,
                ListItemActivity.class,
                NotificationsActivity.class,
                BrandActivity.class,
                ItemActivity.class,
                FeaturedCollectionActivity.class,
                VerifyPhoneActivity.class,
                BuyItemActivity.class,
                SellersNearbyActivity.class,
                ReviewActivity.class,
                ReviewListActivity.class,
                MakeOfferActivity.class,
                ZipcodeVerifyActivity.class,
                MyItemsActivity.class,
                SearchActivity.class
        })
public class RootUIModule {
}
