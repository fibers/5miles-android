package com.thirdrock.fivemiles.di;

import android.content.Context;

import com.insthub.fivemiles.FiveMilesApp;
import com.thirdrock.framework.FrameworkModule;

import dagger.Module;
import dagger.Provides;

/**
 * Root DI scope of this app
 * Created by ywu on 14-10-7.
 */
@Module(library = true, overrides = true,
        includes = {
                FrameworkModule.class,
                RepositoryModule.class,
                RootUIModule.class
        },
        injects = {
                FiveMilesApp.class
        })
public final class RootModule {

    private final Context appCtx;

    public RootModule(Context appCtx) {
        this.appCtx = appCtx;
    }

    @Provides
    @SuppressWarnings("unused")
    Context provideApplicationContext() {
        return appCtx;
    }
}
