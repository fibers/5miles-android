package com.thirdrock.fivemiles.review;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.insthub.fivemiles.SESSION;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.Review;
import com.thirdrock.domain.User;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.common.phone.VerifyPhoneActivity;
import com.thirdrock.fivemiles.framework.activity.AbsActivity;
import com.thirdrock.fivemiles.framework.view.GenericDialog;
import com.thirdrock.fivemiles.framework.view.PopupMenu;
import com.thirdrock.fivemiles.util.DisplayUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_OPPOSITE_USER;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_TX_REVIEW;
import static com.thirdrock.fivemiles.review.ReviewViewModel.Property;
import static com.thirdrock.fivemiles.util.DisplayUtils.closeKeyBoard;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;


public class ReviewActivity extends AbsActivity<ReviewViewModel, Property> {

    public static final int RESULT_FIVE_STAR = 105;
    private final int REQ_VERIFY_PHONE = 1;
    private static final int FIVE_STAR = 5;

    @Inject
    ReviewViewModel viewModel;

    @InjectView(R.id.review_hint)
    TextView txtReviewHint;

    @InjectView(R.id.btn_send_review)
    View btnSend;

    @InjectView(R.id.edt_review_comment)
    EditText edtComment;

    @InjectViews({R.id.feedback_star1, R.id.feedback_star2, R.id.feedback_star3, R.id.feedback_star4, R.id.feedback_star5})
    List<ImageView> icStars;

    private User oppositeUser;
    private Item item;
    private int rating = FIVE_STAR;  // default is 5 stars

    @Override
    protected String getScreenName() {
        return VIEW_TX_REVIEW;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_review;
    }

    @Override
    protected ReviewViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {
        oppositeUser = (User) getIntent().getSerializableExtra(ACT_PARA_OPPOSITE_USER);
        item = (Item) getIntent().getSerializableExtra(ACT_PARA_ITEM);
        viewModel.refreshProfile();

        txtReviewHint.setText(getString(R.string.hint_leave_review, oppositeUser.getNickname()));

        if (item != null && oppositeUser != null) {
            restoreReview();
        }
    }

    private void restoreReview() {
        Review review = viewModel.loadReview(oppositeUser.getId(), item.getId());

        if (review.getRating() > 0) {
            setStar(review.getRating());
        }

        edtComment.setText(review.getComment());
    }

    @Override
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {
        switch (property) {
            case review_sent:
                onReviewSent();
                break;
        }
    }

    @OnTextChanged(R.id.edt_review_comment)
    void updateSendButton() {
        String comment = edtComment.getText().toString().trim();
        btnSend.setEnabled(rating > 0 && isNotEmpty(comment));
    }

    @OnClick({R.id.feedback_star1, R.id.feedback_star2, R.id.feedback_star3, R.id.feedback_star4, R.id.feedback_star5})
    void onRatingChaged(View v) {
        switch (v.getId()) {
            case R.id.feedback_star1:
                setStar(1);
                break;
            case R.id.feedback_star2:
                setStar(2);
                break;
            case R.id.feedback_star3:
                setStar(3);
                break;
            case R.id.feedback_star4:
                setStar(4);
                break;
            case R.id.feedback_star5:
                setStar(5);
                break;
        }
    }

    private void setStar(int stars) {
        rating = stars;
        DisplayUtils.showStars(icStars, stars);
        updateSendButton();
    }

    @OnClick(R.id.btn_send_review)
    void onClickSendButton() {
        closeKeyBoard(getWindow());

        if (rating < 4 && !SESSION.getInstance().isPhoneVerified()) {
            requireVerifyPhone();
            return;
        }

        sendReview();

        trackTouch("review_send");
    }

    private void requireVerifyPhone() {
        // TODO use material-design dialog
        new AlertDialog.Builder(this)
                .setTitle(R.string.dlg_title_verify_phone_before_feedback)
                .setMessage(R.string.msg_verify_phone_before_feedback)
                .setPositiveButton(R.string.lbl_verify_now, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        verifyPhone();
                        trackTouch("reviewneedverify_yes");
                    }
                })
                .setNegativeButton(R.string.lbl_verify_later, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        trackTouch("reviewneedverify_no");
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        trackTouch("reviewneedverify_no");
                    }
                })
                .show();
        trackTouch("reviewneedverify");
    }

    private void verifyPhone() {
        startActivityForResult(new Intent(this, VerifyPhoneActivity.class), REQ_VERIFY_PHONE);
    }

    @Override
    protected void doOnActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != REQ_VERIFY_PHONE || resultCode != Activity.RESULT_OK) {
            return;
        }

        sendReview();
    }

    private void sendReview() {
        viewModel.postReview(oppositeUser.getId(), item, rating, edtComment.getText().toString().trim());
        if (rating == FIVE_STAR) {
            setResult(RESULT_FIVE_STAR);
        }
        trackTouch("review_send");
    }

    private void onReviewSent() {
        stopMainProgress();
        DisplayUtils.toast(R.string.msg_review_sent);
        finish();
    }

    @OnClick(R.id.navi_cancel)
    void cancelWithConfirm() {
        trackTouch("review_cancel");

        if (!isDirty()) {
            finish();
            return;
        }

        new PopupMenu(this)
                .setPostiveButton(R.string.menu_back_to_chat, new PopupMenu.OnClickListener() {
                    @Override
                    public void onClick(GenericDialog dialog, int which) {
                        viewModel.saveReview(oppositeUser.getId(), item.getId(),
                                rating, edtComment.getText().toString().trim());
                        finish();
                        trackTouch("review_backtochat");
                    }
                })
                .setNegativeButton(R.string.continue_to_edit, new PopupMenu.OnClickListener() {
                    @Override
                    public void onClick(GenericDialog dialog, int which) {
                        trackTouch("review_keepediting");
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        trackTouch("review_keepediting");
                    }
                })
                .show();

        trackTouch("review_cancel");
    }

    private boolean isDirty() {
        String comment = edtComment.getText().toString().trim();
        return rating > 0 || isNotEmpty(comment);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && isDirty()) {
            cancelWithConfirm();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
