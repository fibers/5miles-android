package com.thirdrock.fivemiles.review;

import android.content.res.Resources;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.BeeFramework.Utils.TimeUtil;
import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.SESSION;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.pedrogomez.renderers.Renderer;
import com.thirdrock.domain.Review;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.ui.widget.AvatarView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Rendering review list cell
 * Created by ywu on 15/4/19.
 */
public class ReviewRenderer extends Renderer<Review> {

    public interface Callback {
        void onClickReport(Review review);
        void onClickReply(Review review);
        void onClickComment(Review review);
    }

    private static final DisplayImageOptions AVATAR_IMG_OPTS = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.head_loading)
            .showImageForEmptyUri(R.drawable.no_avatar)
            .showImageOnFail(R.drawable.no_avatar)
            .cacheInMemory(false)
            .cacheOnDisk(true)
            .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
            .build();

    @InjectView(R.id.avatar)
    AvatarView avatarView;

    @InjectView(R.id.tv_reviewer_name_myreviews)
    TextView tvReviewerName;

    @InjectView(R.id.ic_review_stars)
    ImageView ivStars;

    @InjectView(R.id.txt_time)
    TextView txtTime;

    @InjectView(R.id.txt_comment)
    TextView txtComment;

    @InjectView(R.id.tv_reply_myreviews)
    TextView tvReply;

    @InjectView(R.id.tv_report_myreviews)
    TextView tvReport;

    @InjectView(R.id.tv_reply_content_myreviews)
    ArrowTextView tvReplyContent;

    private boolean canReport;
    private Callback callback;

    public ReviewRenderer() {
        this(false, null);
    }

    public ReviewRenderer(Callback callback) {
        this(callback != null, callback);
    }

    public ReviewRenderer(boolean canReport, Callback callback) {
        this.canReport = canReport;
        this.callback = callback;
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.review_list_item, parent, false);
    }

    @Override
    protected void setUpView(View rootView) {
        ButterKnife.inject(this, rootView);
    }

    @Override
    protected void hookListeners(View rootView) {

    }

    @OnClick(R.id.tv_report_myreviews)
    void onClickReport() {
        if (canReport && callback != null) {
            callback.onClickReport(getContent());
        }
    }

    @OnClick(R.id.tv_reply_myreviews)
    void onClickReply() {
        if (canReport && callback != null) {
            callback.onClickReply(getContent());
        }
    }

    @OnClick(R.id.txt_comment)
    void onClickComment() {
        if (txtComment.getSelectionStart() == -1 && txtComment.getSelectionEnd() == -1) {
            // This condition will satisfy only when it is not an autolinked text
            // Fired only when you touch the part of the text that is not hyperlinked
            if (callback != null) {
                callback.onClickComment(getContent());
            }
        }
    }

    @Override
    public void render() {
        Review review = getContent();

        tvReviewerName.setText(review.getReviewer().getNickname());

        DisplayUtils.showAvatar(avatarView, review.getReviewer(),
                getResources().getDimensionPixelSize(R.dimen.list_avatar_size), AVATAR_IMG_OPTS);
        txtComment.setText(review.getComment());
        txtTime.setText(TimeUtil.toConciseTime(review.getCreated()));
        DisplayUtils.showStars(review.getRating(), ivStars);


        if(canReport){
            tvReply.setVisibility(View.VISIBLE);
            if (ModelUtils.isEmpty(review.getReplyContent())){
                tvReply.setText(R.string.reply);
            }else{
                tvReply.setText(R.string.edit_reply);
            }

            tvReport.setVisibility(View.VISIBLE);
            if(review.isReported()){
                tvReport.setText(R.string.lbl_reported);
            }else{
                tvReport.setText(R.string.lbl_report);
            }
        }else{
            tvReply.setVisibility(View.GONE);
            tvReport.setVisibility(View.GONE);
        }

        if(ModelUtils.isEmpty(review.getReplyContent())){
            tvReplyContent.setVisibility(View.GONE);
        }else{
            tvReplyContent.setVisibility(View.VISIBLE);

            String replyContent = SESSION.getInstance().nickname + "'s " +
                    getResources().getString(R.string.reply) +
                    " : " + review.getReplyContent();
            SpannableString spannableString = new SpannableString(replyContent);
            spannableString.setSpan(
                    new ForegroundColorSpan(getResources().getColor(R.color.review_reviewer_name_color)),
                    0, SESSION.getInstance().nickname.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(
                    new ForegroundColorSpan(getResources().getColor(R.color.fm_text_dark_gray)),
                    SESSION.getInstance().nickname.length(), replyContent.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvReplyContent.setText(spannableString);
        }
    }

    private Resources getResources() {
        return FiveMilesApp.getInstance().getResources();
    }
}
