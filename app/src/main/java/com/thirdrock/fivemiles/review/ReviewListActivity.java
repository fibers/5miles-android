package com.thirdrock.fivemiles.review;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.insthub.fivemiles.Activity.OtherProfileActivity;
import com.insthub.fivemiles.Activity.ReportActivity;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.SESSION;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.pedrogomez.renderers.AdapteeCollection;
import com.pedrogomez.renderers.RendererAdapter;
import com.thirdrock.domain.Reputation;
import com.thirdrock.domain.Review;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.activity.AbsActivity;
import com.thirdrock.fivemiles.main.listing.ListItemActivity;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.framework.ui.renderer.MonoRendererBuilder;
import com.thirdrock.framework.ui.renderer.SimpleAdapteeCollection;
import com.thirdrock.framework.ui.widget.WaterfallListView;
import com.thirdrock.repository.ReportRepository;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REPORTED_OID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_REPORT_TYPE;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELLER_RATE_AVG;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELLER_RATE_COUNT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.USER_ID_ME;
import static com.thirdrock.fivemiles.review.ReviewListViewModel.Property;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;
import static com.thirdrock.fivemiles.util.ModelUtils.isMe;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;
import static com.thirdrock.framework.ui.widget.WaterfallListView.OnScrollListener.ScrollDirection;

public class ReviewListActivity extends AbsActivity<ReviewListViewModel, Property>
        implements WaterfallListView.Callback, ReviewRenderer.Callback {

    private final int REQ_REPORT_REVIEW = 1;

    static class SummaryView {
        @InjectView(R.id.txt_reviews_count)
        TextView txtReviewCount;

        @InjectView(R.id.ic_summary_stars)
        ImageView ivStars;
    }

    @Inject
    ReviewListViewModel viewModel;

    @InjectView(R.id.lst_reviews)
    WaterfallListView lstReviews;

    @InjectView(R.id.blank_view)
    View blankView;

    @InjectView(R.id.blank_view_button)
    View btnBlankViewAction;

    @InjectView(R.id.txt_no_following_items)
    TextView txtBlankViewMessage;

    private SummaryView summaryView;

    private RendererAdapter<Review> reviewListAdapter;
    private AdapteeCollection<Review> reviewCollection;

    private String userId;  // review receiver's id
    private int reviewCount;
    private double reputation;

    private Review currReview;  // the review currently operating
    private String screenName;

    @Override
    protected String getScreenName() {
        return screenName;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_review_list;
    }

    @Override
    protected ReviewListViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {
        setUserId(getIntent().getStringExtra(ACT_PARA_USER_ID));
        reviewCount = getIntent().getIntExtra(ACT_PARA_SELLER_RATE_COUNT, 0);
        reputation = getIntent().getDoubleExtra(ACT_PARA_SELLER_RATE_AVG, 0);
        screenName = (isMe(userId) ? "my" : "seller") + FiveMilesAppConst.VIEW_REVIEW_LIST;

        ((TextView) findViewById(R.id.top_view_title)).setText(R.string.title_review_list);

        initListView();
        updateSummary();
    }

    private void setUserId(String uid) {
        userId = USER_ID_ME.equals(uid) ? SESSION.getInstance().userId : uid;
    }

    private void initListView() {
        lstReviews.setCallback(this);

        // reputation summary header
        View reputationHeader = View.inflate(ReviewListActivity.this, R.layout.reviews_header_reputation, null);
        ButterKnife.inject(summaryView = new SummaryView(), reputationHeader);
        lstReviews.addHeaderView(reputationHeader);

        // review adapter
        reviewCollection = new SimpleAdapteeCollection<>();
        reviewListAdapter = new RendererAdapter<>(
                getLayoutInflater(),
                new MonoRendererBuilder(new ReviewRenderer(isMe(userId), this)),
                reviewCollection);
        lstReviews.setAdapter(reviewListAdapter);

        showMainProgress(R.string.msg_loading);
        viewModel.getUserReputation(userId);
    }

    @Override
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {
        switch (property) {
            case reputation:
                stopMainProgress();
                showReviews((Reputation) newValue);
                break;
            case post_review_reply:
                stopMainProgress();
                DisplayUtils.toast(R.string.reply_sent);

                Review review = (Review)newValue;
                for(int i=0; i<reviewCollection.size(); i++){
                    Review tempReview = reviewCollection.get(i);
                    if(tempReview.getId() == review.getId()){
                        tempReview.setReplyContent(review.getReplyContent());
                        break;
                    }
                }
                reviewListAdapter.notifyDataSetChanged();

                break;
        }
    }

    @Override
    public void onMinorJobError(Property property, Throwable e) {
        switch (property) {
            case reputation:
                stopMainProgress();
                showBlankView(true);
                break;

            case post_review_reply:
                stopMainProgress();
                DisplayUtils.toast(R.string.reply_failure);
                break;

        }
    }

    private void showReviews(Reputation reputation) {
        this.reputation = reputation.getReputationScore();
        this.reviewCount = reputation.getReviewCount();

        updateSummary();
        reviewCollection.clear();
        reviewCollection.addAll(reputation.getReviews());
        reviewListAdapter.notifyDataSetChanged();
        showBlankView(reviewCollection.size() == 0);

        lstReviews.onLoadMoreComplete(true, viewModel.hasMoreReivews());
    }

    private void showBlankView(boolean visible) {
        stopMainProgress();
        blankView.setVisibility(visible ? View.VISIBLE : View.GONE);

        if (!visible) {
            return;
        }

        boolean isMe = isMe(userId);
        txtBlankViewMessage.setText(isMe ? R.string.msg_no_reviews : R.string.msg_no_reviews_public);
        btnBlankViewAction.setVisibility(isMe ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.top_view_back)
    void onGoBack() {
        finish();
        // TODO 将isMe提成变量，减少方法调用
        if (isMe(userId)) {
            trackTouch("myreviews_back");
        } else {
            trackTouch("sellerreview_back");
        }
    }

    @OnClick(R.id.top_view)
    void scrollToTop() {
        lstReviews.smoothScrollToPosition(0);
    }

    @Override
    public void onLoadMore() {
        if (isMe(userId)) {
            trackTouch("myreviews_loadmore");
        } else {
            trackTouch("sellerreview_loadmore");
        }

        if (!viewModel.hasMoreReivews()) {
            lstReviews.onLoadMoreComplete(true, false);
            return;
        }

        viewModel.getUserReputation(userId);
    }

    @Override
    public void onPauseRendering() {
        ImageLoader.getInstance().pause();
    }

    @Override
    public void onResumeRendering() {
        ImageLoader.getInstance().resume();
    }

    @Override
    public void onScroll(boolean isAtTop, ScrollDirection scrollDirection) {

    }

    @OnItemClick(R.id.lst_reviews)
    @SuppressWarnings("unused")
    void onClickReview(int position) {

        int index = position - lstReviews.getHeaderViewsCount();
        if (index < 0 || index >= reviewCollection.size()) {
            return;
        }

        Review review = reviewCollection.get(index);
        onClickComment(review);
    }

    @Override
    public void onClickComment(Review review) {
        if (review != null && review.getReviewer() != null) {
            String reviewerId = review.getReviewer().getId();
            if (isNotEmpty(reviewerId)) {
                startActivity(new Intent(this, OtherProfileActivity.class)
                        .putExtra(ACT_PARA_USER_ID, reviewerId));
            }
        }

        String gaTrackTouchEventName = isMe(userId) ? "click_myreview" : "click_sellerreview";
        trackTouch(gaTrackTouchEventName);
    }

    @Override
    public void onClickReply(Review review) {

        final String reviewID = Integer.toString(review.getId());
        String reviewerName = review.getReviewer().getNickname();
        String replyContent = review.getReplyContent();

        if (isEmpty(reviewID) || isEmpty(reviewerName)) {
            return;
        }

        final EditText input = new EditText(this);
        input.setMaxLines(10);
        input.setHint(getString(R.string.reply) + " " + reviewerName);

        if (isNotEmpty(replyContent)) {
            input.setText(replyContent);
            input.setSelection(replyContent.length());
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(input)
                .setNegativeButton(R.string.cancel, null);
        builder.setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                viewModel.postReply(reviewID, input.getText().toString());
            }
        });

        AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
    }

    @Override
    public void onClickReport(Review review) {
        currReview = review;

        Intent intent = new Intent(this, ReportActivity.class)
                .putExtra(ACT_PARA_REPORT_TYPE, ReportRepository.REPORT_TYPE_REVIEW)
                .putExtra(ACT_PARA_REPORTED_OID, String.valueOf(review.getId()));
        startActivityForResult(intent, REQ_REPORT_REVIEW);

        trackTouch("report_review");
    }

    @OnClick(R.id.blank_view_button)
    void goListItem() {
        startActivity(new Intent(this, ListItemActivity.class));
        trackTouch("myreview_empty");
    }

    private void updateSummary() {
        summaryView.txtReviewCount.setText(getString(R.string.comments_count, reviewCount));
        DisplayUtils.showStars(reputation, summaryView.ivStars);
    }

    @Override
    protected void doOnActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == REQ_REPORT_REVIEW) {
            if (currReview != null) {
                currReview.setReported(true);
                reviewListAdapter.notifyDataSetChanged();
            }
        }
    }

}
