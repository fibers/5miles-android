package com.thirdrock.fivemiles.review;

import com.thirdrock.domain.Reputation;
import com.thirdrock.domain.Review;
import com.thirdrock.framework.rest.RestObserver;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.repository.ReviewRepository;

import javax.inject.Inject;

import rx.Subscription;

/**
 * View model for review list activity
 * Created by ywu on 15/4/19.
 */
public class ReviewListViewModel extends AbsViewModel<ReviewListViewModel.Property> {
    enum Property {
        reputation, post_review_reply
    }

    @Inject
    ReviewRepository reviewRepo;

    private final RestObserver<Reputation> reputationObserver = newMinorApiJobObserver("reviews", Property.reputation);

    private final RestObserver<Review> postReplyObserver = newMinorApiJobObserver("post_review_reply", Property.post_review_reply);

    private Subscription subsReputation;

    private Subscription subsPostReply;

    @Override
    protected void onModelObserverUnsubscribed() {
        unsubscribe(subsReputation);
        unsubscribe(subsPostReply);
    }

    public void getUserReputation(String uid) {
        subsReputation = schedule(reviewRepo.getUserReputation(uid))
                .subscribe(reputationObserver.reset());
    }

    public boolean hasMoreReivews() {
        return reviewRepo.hasMoreReviews();
    }

    public void postReply(String reviewID, String content){
        subsPostReply = schedule(reviewRepo.postReply(reviewID, content))
                .subscribe(postReplyObserver.reset());
    }
}
