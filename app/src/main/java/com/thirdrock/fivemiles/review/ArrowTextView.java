package com.thirdrock.fivemiles.review;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.widget.TextView;

import com.thirdrock.fivemiles.R;

import static com.insthub.fivemiles.View.Util.DensityUtil.dip2px;


/**
 * Created by fibers on 15/7/15.
 */
public class ArrowTextView extends TextView {


    public ArrowTextView(Context context) {
        super(context);
    }

    public ArrowTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ArrowTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        Path path = new Path();
        path.moveTo(0, dip2px(getContext(), 10));
        path.lineTo(dip2px(getContext(), 14), dip2px(getContext(), 10));
        path.lineTo(dip2px(getContext(), 20), 0);
        path.lineTo(dip2px(getContext(), 26), dip2px(getContext(), 10));
        path.lineTo(getWidth(), dip2px(getContext(), 10));
        path.lineTo(getWidth(), getHeight());
        path.lineTo(0, getHeight());
        path.close();

        Paint paint = new Paint();
        paint.setColor(getResources().getColor(R.color.fm_background));
        canvas.drawPath(path, paint);

        super.onDraw(canvas);
    }
}
