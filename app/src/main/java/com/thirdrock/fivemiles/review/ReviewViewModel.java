package com.thirdrock.fivemiles.review;

import android.content.Context;
import android.content.SharedPreferences;

import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.Model.ProfileModel;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.Review;
import com.thirdrock.domain.Review__JsonHelper;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.rest.RestObserver;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.framework.util.L;
import com.thirdrock.repository.ReviewRepository;

import java.io.IOException;

import javax.inject.Inject;

import rx.Subscription;

import static com.insthub.fivemiles.FiveMilesAppConst.PREFS_USER_PRIVATE_DATA;
import static com.thirdrock.fivemiles.util.ModelUtils.isEmpty;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;

/**
 * View model for review activity
 * Created by ywu on 15/4/15.
 */
public class ReviewViewModel extends AbsViewModel<ReviewViewModel.Property> {
    enum Property {
        review_sent
    }

    @Inject
    ReviewRepository reviewRepo;

    private ProfileModel profileModel;

    private final RestObserver<Void> onReviewSent = newMajorApiJobObserver("post_review", Property.review_sent);

    private Subscription subsReviewSent;

    /**
     * @param context Note: this is NOT an Activity Context
     */
    @Inject
    public ReviewViewModel(Context context) {
        profileModel = new ProfileModel(context);
    }

    @Override
    protected void onModelObserverUnsubscribed() {
        unsubscribe(subsReviewSent);
    }

    /**
     * Refresh verification status from backend, and wrote into session
     */
    public void refreshProfile() {
        profileModel.getMeInfo(true);
    }

    public void postReview(String targetUid, Item item, int rating, String comment) {
        String itemId = item.getId();
        boolean isSeller = ModelUtils.isMine(item);

        emitMajorJobStarted();
        subsReviewSent = schedule(reviewRepo.postReview(targetUid, itemId, rating, comment, isSeller))
                .subscribe(onReviewSent.reset());

        // save to disk
        saveReview(targetUid, item.getId(), rating, comment);
    }

    public Review loadReview(String targetUid, String itemId) {
        Review review = new Review();
        String key = getReviewKey(targetUid, itemId);
        if (isEmpty(key)) {
            return review;
        }

        try {
            String reviewJson = getPrefs().getString(key, "{}");
            review = Review__JsonHelper.parseFromJson(reviewJson);
        } catch (IOException e) {
            L.e("save review failed", e);
        }

        return review;
    }

    public void saveReview(String targetUid, String itemId, int rating, String comment) {
        String key = getReviewKey(targetUid, itemId);
        if (isEmpty(key)) {
            return;
        }

        try {
            String reviewJson = Review__JsonHelper.serializeToJson(new Review(rating, comment));
            getPrefs().edit().putString(key, reviewJson).apply();
        } catch (IOException e) {
            L.e("save review failed", e);
        }
    }

    private static String getReviewKey(String targetUid, String itemId) {
        if (!isNotEmpty(targetUid, itemId)) {
            return null;
        }

        return "review:" + targetUid + ":" + itemId;
    }

    private static SharedPreferences getPrefs() {
        return FiveMilesApp.getInstance().getSharedPreferences(PREFS_USER_PRIVATE_DATA, Context.MODE_PRIVATE);
    }

    public static boolean hasReview(String targetUid, String itemId) {
        String key = getReviewKey(targetUid, itemId);
        return !isEmpty(key) && getPrefs().contains(key);
    }
}
