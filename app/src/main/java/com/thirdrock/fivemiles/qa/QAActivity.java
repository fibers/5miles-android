package com.thirdrock.fivemiles.qa;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.BeeFramework.activity.BaseActivity;
import com.BeeFramework.model.BusinessResponse;
import com.external.androidquery.callback.AjaxStatus;
import com.external.eventbus.EventBus;
import com.external.maxwin.view.IXListViewListener;
import com.external.maxwin.view.XListView;
import com.insthub.fivemiles.Activity.OtherProfileActivity;
import com.insthub.fivemiles.Activity.TabProfileActivity;
import com.insthub.fivemiles.Adapter.CommentAdapter;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.Model.OrderModel;
import com.insthub.fivemiles.Protocol.ApiInterface;
import com.insthub.fivemiles.Protocol.COMMENT;
import com.insthub.fivemiles.Protocol.HOME_OBJECTS;
import com.insthub.fivemiles.Protocol.USER;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.ModelUtils;

import org.json.JSONObject;

import java.util.List;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_HOME_OBJ;

@Deprecated
public class QAActivity extends BaseActivity implements BusinessResponse, IXListViewListener {

    private static final int COMMENTS_PER_PAGE = 25;
    private View commentEditView;
    private EditText edtComment;
    private XListView commentListView;

    private OrderModel orderModel;
    private HOME_OBJECTS homeObject;
    private int replyTo;
    private COMMENT replyComment;
    private CommentAdapter commentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qa);

        orderModel = new OrderModel(this);
        orderModel.addResponseListener(this);
        homeObject = (HOME_OBJECTS) getIntent().getSerializableExtra(ACT_PARA_HOME_OBJ);

        commentListView = (XListView) findViewById(R.id.qa_comment_list);
        commentEditView = findViewById(R.id.order_detail_send_comment_view);
        commentEditView.setVisibility(View.GONE);
        edtComment = (EditText) findViewById(R.id.order_detail_send_comment_edit);

        Button btnPostComment = (Button) findViewById(R.id.order_detail_send_comment_send);
        btnPostComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postComment();
            }
        });
        initCommentList();
    }

    @SuppressWarnings("unchecked")
    private void initCommentList() {
        commentListView.setRefreshTime();
        commentListView.setPullRefreshEnable(true);
        commentListView.setPullLoadEnable(true);
        commentListView.loadMoreInvisible();
        commentListView.loadMoreHide();
        commentListView.setXListViewListener(this, 1);
        onRefresh(0);

//        commentListView.setOnLoadMoreListener(this);
//        commentListView.setOnRefreshListener(this);
//        commentListView.refreshNow();
    }

    private void updateCommentList(List<COMMENT> comments) {
        if (commentAdapter == null) {
            commentAdapter = new CommentAdapter(this, homeObject, comments, 0);
            commentListView.setAdapter(commentAdapter);
        }
        else {
            commentAdapter.setCommentList(comments);
            commentAdapter.notifyDataSetChanged();
        }
        setCommentCtlVisible(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus bus = EventBus.getDefault();
        if (!bus.isregister(this)) {
            bus.register(this);
        }

        replyTo = 0;
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus bus = EventBus.getDefault();
        if (bus.isregister(this)) {
            bus.unregister(this);
        }
    }

    /**
     * EventBus callback
     */
    @SuppressWarnings("unused")
    public void onEvent(Object event) {
        Message msg = (Message) event;
        int what = msg.what;

        switch (msg.what) {
            case MessageConstant.POST_REPLY:
                replyTo = msg.arg1;
                replyComment = (COMMENT) msg.obj;
                setReplyCtlVisible(true);
                break;
            case MessageConstant.VIEW_PROFILE:
                viewProfile((USER) msg.obj);
                break;
        }
    }

    private void viewProfile(USER user) {
        Intent intent;
        if (ModelUtils.isOwner(user.id)) {
            intent = new Intent(this, TabProfileActivity.class);
            intent.putExtra(FiveMilesAppConst.ACT_PARA_CAN_GO_BACK, true);
            startActivity(intent);
        } else {
            intent = new Intent(this, OtherProfileActivity.class);
            intent.putExtra("user", user);
            startActivity(intent);
        }
    }

    /**
     * 显示评论输入框
     */
    @SuppressWarnings("unused")
    public void showCommentView(View v) {
        setCommentCtlVisible(true);

        edtComment.setFocusable(true);
        edtComment.setFocusableInTouchMode(true);
        edtComment.requestFocus();

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edtComment, InputMethodManager.RESULT_SHOWN);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    private void resetCommentView() {
        edtComment.clearFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtComment.getWindowToken(), 0);

        edtComment.setText("");
        replyTo = 0;
    }

    private void setCommentCtlVisible(boolean visible) {
        if (ModelUtils.canComment(homeObject.owner.id, homeObject.isSold()) && visible) {
            commentEditView.setVisibility(View.VISIBLE);
        }
        else {
            commentEditView.setVisibility(View.GONE);
        }
    }

    private void setReplyCtlVisible(boolean visible) {
        if (ModelUtils.canReplyComent(homeObject.owner.id, homeObject.isSold(), replyComment) && visible) {
            commentEditView.setVisibility(View.VISIBLE);

            edtComment.setFocusable(true);
            edtComment.setFocusableInTouchMode(true);
            edtComment.requestFocus();

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtComment, InputMethodManager.RESULT_SHOWN);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
        else {
            resetCommentView();
            commentEditView.setVisibility(View.GONE);
        }
    }

    private void postComment() {
        if (TextUtils.isEmpty(edtComment.getText())) {
            return;
        }

        if (replyTo > 0) {
            orderModel.postReply(homeObject.id, edtComment.getText().toString(), replyTo);
            setReplyCtlVisible(false);  // 回复后关闭编辑框
        } else {
            orderModel.postQuestion(homeObject.id, edtComment.getText().toString());
            resetCommentView();
        }
    }

    @SuppressWarnings("unused")
    public void onNaviBack(View view) {
        this.finish();
    }

    @Override
    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) {
        if (url.startsWith(ApiInterface.POST_QUESTION) ||
                url.startsWith(ApiInterface.POST_REPLY)) {
            onRefresh(0);
//            commentListView.refreshNow();
        }
        else if (url.startsWith(ApiInterface.ITEM_COMMENTS)) {
            commentListView.stopLoadMore();
            commentListView.stopRefresh();
//            commentListView.onLoadMoreComplete();
//            commentListView.onRefreshComplete();
            updateCommentList(orderModel.commentList);

            if (TextUtils.isEmpty(orderModel.commentsNextUrl)) {
               commentListView.loadMoreHide();
//                commentListView.setHintFooterView();
            } else{
//                commentListView.setShowFooterView();
                commentListView.loadMoreShow();
            }
        }
    }

    @Override
    public void onRefresh(int id) {
        orderModel.refreshItemComments(homeObject.id, COMMENTS_PER_PAGE);
    }

    @Override
    public void onLoadMore(int id) {
        orderModel.loadMoreItemComments(homeObject.id);
    }

}
