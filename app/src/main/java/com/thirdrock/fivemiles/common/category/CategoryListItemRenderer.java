package com.thirdrock.fivemiles.common.category;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pedrogomez.renderers.Renderer;
import com.thirdrock.domain.CategoryInfo;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.main.home.CategoryHelper;
import com.thirdrock.framework.util.L;

import java.util.Observable;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * Created by jubin on 5/3/15.
 */
public class CategoryListItemRenderer extends Renderer<CategoryInfo> {
    private Context mContext;
    private int mLytResId;
    private boolean isRenderingSameCat;

    @InjectView(R.id.category_list_item_icon)
    ImageView imgIcon;

    @InjectView(R.id.category_list_item_title)
    TextView txtTitle;

    public CategoryListItemRenderer(Context context){
        this(context, R.layout.category_list_item);
    }

    public CategoryListItemRenderer(Context context, int lytResId){
        this.mContext = context;
        this.mLytResId = lytResId;
    }

    @Override
    protected void setUpView(View rootView) {
        ButterKnife.inject(this, rootView);
    }

    @Override
    protected void hookListeners(View rootView) {
        // empty
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup parent) {
        if (inflater == null) {
            L.e("inflater should not be null");
            return null;
        }
        return inflater.inflate(mLytResId, parent, false);
    }

    @Override
    public void render() {
        if (isRenderingSameCat) {
            // 如果view没有被回收则不需要重新渲染，避免闪烁
            return;
        }
        CategoryInfo categoryInfo = getContent();

        renderCategoryIcon(categoryInfo);
        renderCategoryText(categoryInfo);
    }

    private void renderCategoryText(CategoryInfo categoryInfo) {
        if (categoryInfo == null){
            L.e("can't render category icon since categoryInfo is null");
            return;
        }
        txtTitle.setText(categoryInfo.getTitle());
    }

    private void renderCategoryIcon(CategoryInfo categoryInfo) {
        if (categoryInfo == null){
            L.e("can't render category icon since categoryInfo is null");
            return;
        }

        CategoryHelper.displayCategoryIcon(categoryInfo, imgIcon);
    }

    @Override
    public void onRecycle(CategoryInfo content){
        CategoryInfo oldCategory = getContent();
        isRenderingSameCat = content.equals(oldCategory);
        super.onRecycle(content);
    }
}
