package com.thirdrock.fivemiles.common.brand;

import android.content.Context;

import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.FileUtils;
import com.thirdrock.framework.util.L;
import com.thirdrock.protocol.LocalBrandSet;
import com.thirdrock.protocol.LocalBrandSet__JsonHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Helper class for brand data preparing/migration
 * Created by ywu on 14/11/11.
 */
@Singleton
public class BrandDataHelper {

    private Context context;

    @Inject
    public BrandDataHelper(Context context) {
        this.context = context;
    }

    public LocalBrandSet loadRawData() {
        LocalBrandSet brands = null;

        try {
            String content = FileUtils.readFileContent(
                    context.getResources().openRawResource(R.raw.brands));
            brands = LocalBrandSet__JsonHelper.parseFromJson(content);
        } catch (Exception e) {
            L.e("load raw brand list failed", e);
        }

        return brands;
    }
}
