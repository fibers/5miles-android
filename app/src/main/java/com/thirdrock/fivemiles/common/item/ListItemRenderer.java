package com.thirdrock.fivemiles.common.item;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.external.eventbus.EventBus;
import com.insthub.fivemiles.MessageConstant;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.pedrogomez.renderers.Renderer;
import com.thirdrock.domain.ImageInfo;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.User;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.ui.widget.AvatarView;
import com.thirdrock.framework.util.L;

import java.util.Observable;
import java.util.Observer;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.thirdrock.fivemiles.util.Currencies.formatCurrency;
import static com.thirdrock.fivemiles.util.DisplayUtils.getNextBackgroundResource;
import static com.thirdrock.fivemiles.util.LocationUtils.formatItemLocation;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;
import static com.thirdrock.framework.util.Utils.doOnGlobalLayout;

/**
 * Created by jubin on 30/1/15.
 */
public class ListItemRenderer extends Renderer<Item> implements Observer {
    private final int layoutResId;
    private final int itemImgSize;
    private final int avatarImgSize;

    private RoundedImageView imgItemImage;
    private AvatarView lytAvatar;

    @InjectView(R.id.search_order_item_title)
    TextView txtTiltle;

    @InjectView(R.id.search_order_item_price)
    TextView txtPrice;

    @InjectView(R.id.search_order_item_origin_price)
    TextView txtOriginPrice;

    @InjectView(R.id.search_order_item_location)
    LinearLayout lytLoaction;

    @InjectView(R.id.search_order_item_distance)
    TextView txtDistance;

    @InjectView(R.id.iv_stars_search_result)
    ImageView ivStars;

    private boolean renderingSameItem;

    private String itemImgUrl;                        // 用于比较缓存
    private String avatarImgUrl;                          // 同上

    private DisplayImageOptions itemImgOpts;
    private DisplayImageOptions avatarImgOpts;

    public ListItemRenderer(Context context) {
        this(context, R.layout.search_order_item);
    }

    public ListItemRenderer(Context context, int pLayoutResId) {
        this.layoutResId = pLayoutResId;
        this.itemImgSize = context.getResources().getDimensionPixelSize(R.dimen.search_result_thumb_size);
        this.avatarImgSize = context.getResources().getDimensionPixelSize(R.dimen.list_avatar_size);

        itemImgOpts = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading)
                .showImageForEmptyUri(R.drawable.loading)
                .showImageOnFail(R.drawable.loading)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        avatarImgOpts = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.head_loading)
                .showImageForEmptyUri(R.drawable.no_avatar)
                .showImageOnFail(R.drawable.no_avatar)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();
    }

    @Override
    protected void setUpView(View rootView) {
        ButterKnife.inject(this, rootView);
        imgItemImage = (RoundedImageView) rootView.findViewById(R.id.search_order_item_image);
        lytAvatar = (AvatarView) rootView.findViewById(R.id.search_order_item_portrait);
        txtOriginPrice.setPaintFlags(
                txtOriginPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    protected void hookListeners(View rootView) {
        // leave empty
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(layoutResId, parent, false);
    }

    @Override
    public void onRecycle(Item content) {
        Item oldItem = getContent();
        if (oldItem != null) {
            oldItem.deleteObserver(this);
            L.d("recycle renderer %s -> %s", oldItem.getTitle(), content.getTitle());
        }

        renderingSameItem = content.equals(oldItem);
        if (renderingSameItem) {
            content.setThumbImage(oldItem.getThumbImage());
        }
        content.addObserver(this);
        super.onRecycle(content);
    }

    @Override
    public void render() {
        if (renderingSameItem) {
            // 如果view没有被回收则不需要重新渲染，避免闪烁
            return;
        }

        Item item = getContent();
        L.d("setup renderer for item %s", item.getTitle());

        // rendering item image
        renderItemImage(item);

        // rendering avatar image
        renderAvatarImage(item);

        // calc & render item distance
        renderLocation(item);

        // rendering title/prices
        renderTitleAndPrices(item);

        // rendering reputation
        renderReputation(item);

    }

    private void renderReputation(Item item){
        if(item.getOwner() != null){
            User user = item.getOwner();
            if(user.getReputationScore() == 0){
                ivStars.setVisibility(View.GONE);
            }else{
                DisplayUtils.showStars(item.getOwner().getReputationScore(), ivStars);
            }
        }
    }

    private void renderTitleAndPrices(Item item) {
        txtTiltle.setText(item.getTitle());

        String currency = item.getCurrencyCode();
        txtPrice.setText(formatCurrency(currency, item.getPrice()));

        Double originPrice = item.getOriginPrice();
        if (ModelUtils.isValidPrice(originPrice)) {
            txtOriginPrice.setText(formatCurrency(currency, item.getOriginPrice()));
            txtOriginPrice.setVisibility(View.VISIBLE);
        } else {
            txtOriginPrice.setVisibility(View.GONE);
        }
    }

    protected void renderItemImage(final Item item) {
        L.d("renderItemImage [%s] %s", item.getId(), item.getTitle());

        // 图片加载之前先显示一个随机的纯色图片
        imgItemImage.setImageResource(getNextBackgroundResource());

        // 异步加载图片
        doOnGlobalLayout(getRootView(), new Runnable() {
            @Override
            public void run() {
                loadItemImage(item);
            }
        });
    }

    protected void loadItemImage(Item item) {
        ImageInfo itemImageInfo = item.getDefaultImage();                      // 使用上传的第一张图片作为缩略图显示
        L.d("loadItemImage %s %s", item.getId(), item.getTitle());
        if (itemImageInfo == null) {
            return;
        }

        // load clipped version of image
        int containerWidth = imgItemImage.getMeasuredWidth();
        int fitSize = containerWidth > 0 ? containerWidth : itemImgSize;
        String fitImgUrl = CloudHelper.toCropUrl(itemImageInfo.getUrl(), fitSize, fitSize, "fill", null);

        ImageInfo thumbImgInfo = new ImageInfo(fitImgUrl, fitSize, fitSize);
        item.setThumbImage(thumbImgInfo);

        displayItemImage(item.getId(), fitImgUrl, fitSize, fitSize);
    }

    /**
     * @param id id只用于log
     */
    protected void displayItemImage(final String id, String url, int fitWidth, int fitHeight) {
        itemImgUrl = url;                 // 用于比较异步加载的图片

        ImageLoader.getInstance().loadImage(url,
                new ImageSize(fitWidth, fitHeight),
                itemImgOpts,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if (!TextUtils.equals(itemImgUrl, imageUri)) {
                            // 不能与局部变量对比，避免异步结果返回后，当前渲染的图片已经改变(view已被其他item重用)
                            L.d("image load complete %s %s !eq %s", id, itemImgUrl, imageUri);
                            return;
                        }

                        L.d("image load complete %s %s", id, imageUri);
                        imgItemImage.setImageBitmap(loadedImage);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        L.d("image load failed %s %s", id, imageUri);
                    }

                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        L.d("image load started %s %s", id, imageUri);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        L.d("image load cancelled %s %s", id, imageUri);
                    }
                });
    }

    private void renderAvatarImage(final Item item) {
        L.d("renderAvatarImage [%s] %s", item.getId(), item.getTitle());

        // 异步加载图片
        doOnGlobalLayout(getRootView(), new Runnable() {
            @Override
            public void run() {
                loadAvatarImage(item);
            }
        });
    }

    private void loadAvatarImage(Item item) {
        String avatarUrl = item.getOwner().getAvatarUrl();
        L.d("loadAvatarImage [%s] %s", item.getId(), avatarUrl);
        if (avatarUrl == null) {
            return;
        }

        // load clipped version of image
        int containerWidth = lytAvatar.getMeasuredWidth();
        int fitSize = containerWidth > 0 ? containerWidth : avatarImgSize;
        String fitImgUrl = CloudHelper.toCropUrl(avatarUrl, fitSize, fitSize, "fill", null);

        displayAvatarImage(item.getId(), fitImgUrl, fitSize, fitSize);
    }

    private void displayAvatarImage(final String id, String avatarUrl, int fitWidth, int fitHeight) {
        avatarImgUrl = avatarUrl;                 // 用于比较异步加载的图片
        ImageLoader.getInstance().displayImage(avatarImgUrl, lytAvatar.getAvatarImageView(), avatarImgOpts);
    }

    private void renderLocation(Item item) {
        String loc = formatItemLocation(item);

        if (isNotEmpty(loc)) {
            lytLoaction.setVisibility(View.VISIBLE);
            txtDistance.setVisibility(View.VISIBLE);
            txtDistance.setText(loc);
        } else {
            lytLoaction.setVisibility(View.GONE);
            txtDistance.setVisibility(View.GONE);
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        if (!(data instanceof Integer) || getContent() == null) {
            return;
        }

        Integer event = (Integer) data;
        switch (event) {
            case MessageConstant.PROP_DISTANCE_UPDATED:
                renderLocation(getContent());
                break;
        }
    }

    @OnClick(R.id.search_order_item_location)
    void onLocationClicked() {
        Message msg = new Message();
        msg.what = MessageConstant.VIEW_SEARCH_LIST_MAP;
        msg.obj = getContent();
        EventBus.getDefault().post(msg);
    }

    @OnClick(R.id.search_order_item_portrait)
    void onProfileClicked() {
        Message msg = new Message();
        msg.what = MessageConstant.VIEW_SEARCH_LIST_OWNER;
        msg.obj = getContent().getOwner();
        EventBus.getDefault().post(msg);
    }
}
