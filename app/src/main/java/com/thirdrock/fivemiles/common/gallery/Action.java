package com.thirdrock.fivemiles.common.gallery;

public class Action {

	public static final String ACTION_PICK = "thirdrock.ACTION_PICK";
	public static final String ACTION_MULTIPLE_PICK = "thirdrock.ACTION_MULTIPLE_PICK";
    public static final String MAXIMUM="SELECT_MAXIMUM";
}
