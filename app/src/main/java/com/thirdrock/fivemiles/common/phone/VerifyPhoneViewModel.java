package com.thirdrock.fivemiles.common.phone;

import com.thirdrock.framework.rest.RestObserver;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.repository.ItemRepository;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ywu on 15/1/9.
 */
public class VerifyPhoneViewModel extends AbsViewModel<VerifyPhoneViewModel.Property> {

    public static enum Property {
        passcode_sent, passcode_verified
    }

    @Inject
    ItemRepository itemRepository;

    private final RestObserver<Void> codeSentObserver, codeVerifiedObserver;

    private Subscription subsCodeSent, subsCodeVerified;

    public VerifyPhoneViewModel() {
        codeSentObserver = newMinorApiJobObserver("send_passcode", Property.passcode_sent);
        codeVerifiedObserver = newMinorApiJobObserver("verify_passcode", Property.passcode_verified);
    }

    public void sendPasscode(String countryNo, String phone) {
        subsCodeSent = itemRepository.sendPasscode(countryNo, phone)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(codeSentObserver.reset());
    }

    public void verifyPasscode(String countryNo, String phone, String code) {
        subsCodeVerified = itemRepository.verifyPasscode(countryNo, phone, code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(codeVerifiedObserver.reset());
    }

    public void onDestroy() {
        unsubscribe(subsCodeSent, subsCodeVerified);
    }

}
