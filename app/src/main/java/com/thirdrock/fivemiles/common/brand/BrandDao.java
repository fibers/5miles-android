package com.thirdrock.fivemiles.common.brand;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

import com.insthub.fivemiles.FiveMilesAppConst;
import com.thirdrock.protocol.LocalBrandSet;

import java.util.LinkedHashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Brand data access utils.
 * Created by ywu on 14/11/11.
 */
@Singleton
public class BrandDao extends SQLiteOpenHelper {

    public static final String F_BRAND_NAME = "name";

    static final int BRAND_DB_V = 1;
    static final int BRAND_OWNER_SYS = 0;
    static final int BRAND_OWNER_USR = 1;

    private static final String TBL_NAME = "tbl_brands";
    private static final String F_BRAND_ID = "id";
    private static final String F_BRAND_OWNER = "owner";  // system or user who create the brand db record
    private static final String SQL_C_TBL = "CREATE TABLE " + TBL_NAME + " (" +
//            F_BRAND_ID + " INT PRIMARY KEY ASC," +  // TODO 品牌需要id吗，用户输入的品牌怎么处理?
            F_BRAND_NAME + " VARCHAR PRIMARY KEY ASC," +
            F_BRAND_OWNER + " INT" +
            ");";

    private BrandDataHelper dataHelper;

    public static class QueryResult {
        public LinkedHashMap<String, Integer> index;
        public Cursor cursor;

        public QueryResult(LinkedHashMap<String, Integer> index, Cursor cursor) {
            this.index = index;
            this.cursor = cursor;
        }
    }

    @Inject
    public BrandDao(Context context, BrandDataHelper helper) {
        super(context, FiveMilesAppConst.FM_DB_BRAND, null, BRAND_DB_V);
        this.dataHelper = helper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_C_TBL);
        LocalBrandSet localBrands = dataHelper.loadRawData();
        if (localBrands != null) {
            try {
                db.beginTransaction();  // for bulk insert
                populateBrands(db, localBrands);
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
        }
    }

    private void populateBrands(SQLiteDatabase db, LocalBrandSet brands) {
        if (brands.getVersion() != BRAND_DB_V) {
            throw new IllegalStateException("brand db version conflicts, expected " +
                    BRAND_DB_V + ", actual " + brands.getVersion());
        }

        for (String brand : brands.getBrandList()) {
            doInsertBrand(db, brand, BRAND_OWNER_SYS);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long createBrandByUser(String name) {
        SQLiteDatabase db = getWritableDatabase();
        if (db.isReadOnly()) {
            throw new IllegalStateException("db is not writable");
        }

        return doInsertBrandByUser(db, name);
    }

    private long doInsertBrandByUser(SQLiteDatabase db, String name) {
        return doInsertBrand(db, name, BRAND_OWNER_USR);
    }

    private long doInsertBrand(SQLiteDatabase db, String name, int owner) {
        ContentValues values = new ContentValues(2);
        values.put(F_BRAND_NAME, name);
        values.put(F_BRAND_OWNER, owner);
        return db.insert(TBL_NAME, null, values);
    }

    public QueryResult findAllBrandsWithIndex() {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TBL_NAME);
        String[] projection = new String[] { "rowid _id", F_BRAND_NAME };
        String orderBy = F_BRAND_NAME + " COLLATE NOCASE ASC";

        return new QueryResult(getBrandIndex(null, null),
                getReadableDatabase().query(TBL_NAME, projection, null, null, null, null, orderBy));
    }

    public QueryResult searchBrandsWithIndex(String keyword) {
        keyword = keyword == null ? "" : keyword.trim();
        String sel = F_BRAND_NAME + " LIKE ? ESCAPE '\\'";
        String[] selArgs = new String[] { "%" + escape(keyword) + "%" };

        return new QueryResult(getBrandIndex(sel, selArgs), searchBrands(keyword, -1));
    }

    public Cursor searchBrands(String keyword, int limit) {
        keyword = keyword == null ? "" : keyword.trim();

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TBL_NAME);

        String[] projection = new String[] { "rowid _id", F_BRAND_NAME };
        String sel = F_BRAND_NAME + " LIKE ?  ESCAPE '\\'";
        String[] selArgs = new String[] { escape(keyword) + "%" };
        String orderBy = F_BRAND_NAME + " COLLATE NOCASE ASC";
        String strLimit = limit > 0 ? String.valueOf(limit) : null;

        return qb.query(getReadableDatabase(), projection, sel, selArgs, null, null, orderBy, strLimit);
    }

    public boolean isBrandExists(String brand) {
        brand = brand == null ? "" : brand.trim();

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TBL_NAME);

        String[] projection = new String[] { "rowid" };
        String sel = F_BRAND_NAME + " = ?";
        String[] selArgs = new String[] { brand };

        Cursor cursor = null;
        try {
            cursor = qb.query(getReadableDatabase(), projection, sel, selArgs, null, null, null);
            return cursor.getCount() > 0;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private LinkedHashMap<String, Integer> getBrandIndex(String sel, String[] selArgs) {
        LinkedHashMap<String, Integer> index = new LinkedHashMap<String, Integer>();

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TBL_NAME);

        String[] projection = new String[] { "upper(substr(name, 1, 1)) k, count(rowid) c" };
        String orderBy = "k COLLATE NOCASE ASC";

        Cursor cursor = null;
        try {
            cursor = qb.query(getReadableDatabase(), projection, sel, selArgs, "k", null, orderBy);
            int currIndex = 0;
            while (cursor.moveToNext()) {
                String key = cursor.getString(0);
                int count = cursor.getInt(1);

                index.put(key, currIndex);
                currIndex += count;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return index;
    }

    private String escape(String keyword) {
        return keyword.replaceAll("([%_])", "\\\\$1");
    }
}
