package com.thirdrock.fivemiles.common.brand;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ywu on 14/11/12.
 */
public class BrandSuggestionAdapter extends ArrayAdapter<String> {

    private List<String> brands;

    public BrandSuggestionAdapter(Context context, Cursor c) {
        super(context, R.layout.common_text_list_item, R.id.common_list_item_text);
        setCursor(c);
    }

    public void setCursor(Cursor c) {
        if (c == null || c.isClosed()) {
            return;
        }

        try {
            brands = new ArrayList<String>(c.getCount());
            while (c.moveToNext()) {
                brands.add(c.getString(1));
            }

            setNotifyOnChange(false);
            clear();

            setNotifyOnChange(true);
            addAll(brands);
        } catch (Exception e) {
            L.e(e);
        } finally {
            c.close();
        }
    }

    @Override
    public Filter getFilter() {
        return new BrandSuggestionFilter();
    }

    private class BrandSuggestionFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && !TextUtils.isEmpty(constraint.toString().trim())) {
                results.values = brands != null ? brands : Collections.emptyList();
                results.count = brands != null ? brands.size() : 0;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null && results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
