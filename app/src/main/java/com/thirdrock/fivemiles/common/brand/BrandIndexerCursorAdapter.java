package com.thirdrock.fivemiles.common.brand;

import android.content.Context;
import android.database.Cursor;
import android.widget.SectionIndexer;
import android.widget.SimpleCursorAdapter;

import com.thirdrock.fivemiles.R;

import java.util.LinkedHashMap;

import static com.thirdrock.fivemiles.common.brand.BrandDao.QueryResult;

/**
 * Created by ywu on 14/11/12.
 */
public class BrandIndexerCursorAdapter extends SimpleCursorAdapter implements SectionIndexer {

    private LinkedHashMap<String, Integer> index;
    private String[] sections;

    public BrandIndexerCursorAdapter(Context context, QueryResult queryResult) {
        super(context,
                R.layout.brand_list_item,
                queryResult.cursor,
                new String[]{BrandDao.F_BRAND_NAME},
                new int[]{R.id.common_list_item_text},
                0);
        buildSections(queryResult.index);
    }

    private void buildSections(LinkedHashMap<String, Integer> index) {
        this.index = index;
        this.sections = this.index.keySet().toArray(new String[this.index.size()]);
    }

    @Override
    public String[] getSections() {
        return sections;
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return index.get(sections[sectionIndex]);
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    /**
     * Update date set & re-index.
     */
    public void setQueryResult(QueryResult result) {
        buildSections(result.index);
        changeCursor(result.cursor);
        notifyDataSetInvalidated();
    }

    @Override
    public Object getItem(int position) {
        Cursor c = (Cursor) super.getItem(position);
        return c.getString(1);
    }
}
