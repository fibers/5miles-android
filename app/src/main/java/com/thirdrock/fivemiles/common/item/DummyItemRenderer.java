package com.thirdrock.fivemiles.common.item;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pedrogomez.renderers.Renderer;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.fivemiles.R;

/**
 * Created by ywu on 15/2/14.
 */
public class DummyItemRenderer extends Renderer<ItemThumb> {
    @Override
    protected void setUpView(View rootView) {

    }

    @Override
    protected void hookListeners(View rootView) {

    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.transparent, null);
    }

    @Override
    public void render() {

    }
}
