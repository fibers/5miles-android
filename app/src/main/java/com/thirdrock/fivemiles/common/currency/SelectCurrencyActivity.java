package com.thirdrock.fivemiles.common.currency;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.BeeFramework.activity.BaseActivity;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.insthub.fivemiles.Model.NewProductModel;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.TrackingUtils;

public class SelectCurrencyActivity extends BaseActivity {

	private ImageView back;
	private TextView title;
	private ListView listView;
	private CurrencyAdapter currencyAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_currency);
		back = (ImageView) findViewById(R.id.top_view_back);
		title = (TextView) findViewById(R.id.top_view_title);
		listView = (ListView) findViewById(R.id.currency_listview);
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		title.setText(getString(R.string.title_choose_currencies));
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.putExtra("currency_name",newProductModel.codeList.get(position));
				setResult(Activity.RESULT_OK, intent);
				finish();
			}
		});
        setCurrencyAdapter();

        TrackingUtils.trackView(FiveMilesAppConst.VIEW_CURRENCY);
	}
	private  NewProductModel  newProductModel;

	private void setCurrencyAdapter() {
         newProductModel = new NewProductModel(this);
        newProductModel.getCurrencyCode();
        currencyAdapter = new CurrencyAdapter(SelectCurrencyActivity.this, newProductModel.codeList);
		listView.setAdapter(currencyAdapter);
	}
	
}
