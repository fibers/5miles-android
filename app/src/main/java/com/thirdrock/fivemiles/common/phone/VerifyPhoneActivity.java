package com.thirdrock.fivemiles.common.phone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.insthub.fivemiles.SESSION;
import com.thirdrock.domain.Country;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.activity.AbsActivity;
import com.thirdrock.fivemiles.framework.activity.SelectionListActivity;
import com.thirdrock.fivemiles.util.Countries;
import com.thirdrock.fivemiles.util.DisplayUtils;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.framework.exception.NetworkException;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.SimpleObserver;

import org.apache.http.HttpStatus;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELECTION_DATA_LIST;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELECTION_SCREEN_NAME;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELECTION_TITLE;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELECTION_USE_FAST_SCROLL;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_RESULT_SELECTION_ITEM;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_RESULT_VERIFIED_PHONE;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_COUNTRY;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_VERIFY_PHONE;
import static com.thirdrock.fivemiles.common.phone.VerifyPhoneViewModel.Property;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;

public class VerifyPhoneActivity extends AbsActivity<VerifyPhoneViewModel, Property> {

    private static final int REQ_SEL_COUNTRY = 1;
    private static final int MAX_PASSCODE_SEND_INTV = 60;  // seconds wait before send passcode again

    private static final String VIEW_NAME = "Verifyphone_view";

    @Inject
    VerifyPhoneViewModel viewModel;

    @Inject
    Countries countries;

    @InjectView(R.id.top_view_title)
    TextView txtTitle;

    @InjectView(R.id.country)
    TextView txtCountry;

    @InjectView(R.id.phone_country_no)
    TextView txtCountryNo;

    @InjectView(R.id.edt_phone_no)
    EditText edtPhoneNo;

    @InjectView(R.id.edt_passcode)
    EditText edtPasscode;

    @InjectView(R.id.btn_send_passcode)
    TextView btnSendPasscode;

    @InjectView(R.id.btn_verify_passcode)
    View btnVerifyPasscode;

    private Subscription subsPasscodeCountdown;

    @Override
    protected String getScreenName() {
        return VIEW_VERIFY_PHONE;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_verify_phone;
    }

    @Override
    protected VerifyPhoneViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {
        txtTitle.setText(R.string.title_verify_phone);

        Country country = countries.getDefaultCountry();
        showCountry(country);
    }

    private void showCountry(Country country) {
        txtCountry.setText(country.getDisplayName());
        txtCountryNo.setText(country.getAreaNumber());
    }

    @Override
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {
        switch (property) {
            case passcode_sent:
                onPasscodeSent();
                break;
            case passcode_verified:
                onPasscodeVerified();
                break;
        }
    }

    @Override
    public void onMinorJobError(Property property, Throwable e) {
        switch (property) {
            case passcode_sent:
                stopPasscodeCountdown();
                onSendPasscodeFailed(e);
                break;
            case passcode_verified:
                onVerfifyPasscodeFailed(e);
                break;
        }
    }

    @Override
    protected void doOnActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_SEL_COUNTRY && resultCode == Activity.RESULT_OK) {
            onCountrySelected((Country) data.getSerializableExtra(ACT_RESULT_SELECTION_ITEM));
        }
    }

    private void onCountrySelected(Country country) {
        L.d("selected: %s", country.getCountryCode());
        showCountry(country);

        // save user choice
        countries.setDefaultCountry(country);
        TrackingUtils.trackTouch(VIEW_COUNTRY, "select_country");
    }

    @OnClick(R.id.country_wrapper)
    @SuppressWarnings("unused")
    void onSelectCountry() {
        Intent intent = new Intent(this, SelectionListActivity.class);
        intent.putExtra(ACT_PARA_SELECTION_TITLE, getString(R.string.title_country));
        intent.putExtra(ACT_PARA_SELECTION_DATA_LIST, countries.getCountries());
        intent.putExtra(ACT_PARA_SELECTION_SCREEN_NAME, VIEW_COUNTRY);
        intent.putExtra(ACT_PARA_SELECTION_USE_FAST_SCROLL, true);
        startActivityForResult(intent, REQ_SEL_COUNTRY);

        TrackingUtils.trackTouch(VIEW_COUNTRY, "search_country");
    }

    @OnClick(R.id.top_view_back)
    @SuppressWarnings("unused")
    void goBack() {
        finish();
        trackTouch("verifyphone_back");
    }

    @OnTextChanged(R.id.edt_phone_no)
    @SuppressWarnings("unused")
    void onPhoneNoChanged(CharSequence phone) {
        btnSendPasscode.setEnabled(isNotEmpty(phone));
    }

    @OnClick(R.id.btn_send_passcode)
    @SuppressWarnings("unused")
    void onSendPasscode() {
        String countryNo = txtCountryNo.getText().toString();
        String phone = edtPhoneNo.getText().toString();

        if (isNotEmpty(countryNo, phone)) {
            viewModel.sendPasscode(countryNo, phone);
            btnSendPasscode.setEnabled(false);

            trackTouch("sendpasscode");
        }
    }

    private void onPasscodeSent() {
        setSendPasscodeButtonEnabled(false);
        edtPasscode.requestFocus();
    }

    private void setSendPasscodeButtonEnabled(boolean isEnabled) {
        btnSendPasscode.setEnabled(isEnabled);

        if (!isEnabled) {
            btnSendPasscode.setText(getString(R.string.resend_passcode_countdown, MAX_PASSCODE_SEND_INTV));
            startPasscodeCountdown();
        } else {
            btnSendPasscode.setText(R.string.send_passcode);
        }
    }

    private void startPasscodeCountdown() {
        subsPasscodeCountdown = Observable.interval(1, TimeUnit.SECONDS)
                .take(MAX_PASSCODE_SEND_INTV)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<Long>() {
                    @Override
                    public void onNext(Long seconds) {
                        btnSendPasscode.setText(getString(R.string.resend_passcode_countdown,
                                MAX_PASSCODE_SEND_INTV - seconds));
                    }

                    @Override
                    public void onCompleted() {
                        setSendPasscodeButtonEnabled(true);
                        subsPasscodeCountdown = null;
                    }
                });
    }

    private void stopPasscodeCountdown() {
        if (subsPasscodeCountdown != null) {
            subsPasscodeCountdown.unsubscribe();
            subsPasscodeCountdown = null;
        }

        setSendPasscodeButtonEnabled(true);
    }

    private void onSendPasscodeFailed(Throwable t) {
        boolean handled = false;

        if (t instanceof NetworkException) {
            int statusCode = ((NetworkException) t).getStatusCode();
            if (statusCode == HttpStatus.SC_UNPROCESSABLE_ENTITY) {
                DisplayUtils.toast(R.string.err_phone_number_exists);
                edtPhoneNo.requestFocus();
                handled = true;
            }
        }

        if (!handled) {
            onError(Property.passcode_sent, t);
        }
    }

    @OnTextChanged(R.id.edt_passcode)
    @SuppressWarnings("unused")
    void onPasscodeChanged(CharSequence code) {
        btnVerifyPasscode.setEnabled(code.toString().trim().length() > 3);
    }

    @OnClick(R.id.btn_verify_passcode)
    @SuppressWarnings("unused")
    void onVerifyPasscode() {
        String countryNo = txtCountryNo.getText().toString();
        String phone = edtPhoneNo.getText().toString();
        String code = edtPasscode.getText().toString();

        if (isNotEmpty(countryNo, phone, code)) {
            viewModel.verifyPasscode(countryNo, phone, code);
            btnVerifyPasscode.setEnabled(false);

            trackTouch("passcodeconfirm");
        }
    }

    private void onPasscodeVerified() {
        DisplayUtils.toast(R.string.msg_verfify_phone_done);

        String countryNo = txtCountryNo.getText().toString();
        String phone = edtPhoneNo.getText().toString();

        Intent data = new Intent();
        data.putExtra(ACT_RESULT_VERIFIED_PHONE, countryNo + phone);
        setResult(RESULT_OK, data);
        finish();

        SESSION.getInstance().phoneVerified(true).save();
    }

    private void onVerfifyPasscodeFailed(Throwable t) {
        boolean handled = false;
        btnVerifyPasscode.setEnabled(true);

        if (t instanceof NetworkException) {
            int statusCode = ((NetworkException) t).getStatusCode();
            if (statusCode == HttpStatus.SC_BAD_REQUEST) {
                // passcode is invalid
                DisplayUtils.toast(R.string.err_passcode_invalid);
                edtPasscode.setText("");
                edtPasscode.requestFocus();
                handled = true;
            }
        }

        if (!handled) {
            super.onError(Property.passcode_verified, t);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (subsPasscodeCountdown != null) {
            subsPasscodeCountdown.unsubscribe();
            subsPasscodeCountdown = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.onDestroy();
    }
}
