package com.thirdrock.fivemiles.common.brand;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.activity.AbsActivity;
import com.thirdrock.framework.util.L;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnTextChanged;

import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_BRAND;
import static com.thirdrock.fivemiles.common.brand.BrandDao.QueryResult;
import static com.thirdrock.fivemiles.common.brand.BrandViewModel.Property;

/**
 * A login screen that offers login via email/password.
 */
public class BrandActivity extends AbsActivity<BrandViewModel, Property> {

    public static final String RESULT_KEY_BRAND_NAME = "brand_name";

    @Inject
    BrandViewModel viewModel;

    @InjectView(R.id.top_view_title)
    TextView txtTitle;

    @InjectView(R.id.search_edit)
    AutoCompleteTextView edtSearch;

    @InjectView(R.id.btn_search)
    ImageView btnSearch;

    @InjectView(R.id.add_brand)
    TextView txtAddBrand;

    @InjectView(R.id.brand_list)
    AbsListView lstBrands;

    private BrandIndexerCursorAdapter lstBrandsAdapter;
    private BrandSuggestionAdapter searchSuggestionAdapter;

    private String newBrandCandidate;

    @Override
    protected String getScreenName() {
        return VIEW_BRAND;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_brand;
    }

    @Override
    public BrandViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {
        txtTitle.setText(R.string.title_brands);

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    doSearch(edtSearch.getText());
                    return true;
                }
                return false;
            }
        });

        edtSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String brand = searchSuggestionAdapter.getItem(position);
                finishWithBrand(brand);
            }
        });

        edtSearch.requestFocus();
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.loadAllBrands();
    }

    @Override
    protected void onStop() {
        super.onStop();
        viewModel.onStop();
    }

    @OnClick(R.id.top_view_back)
    @SuppressWarnings("unused")
    void onNaviBack() {
        finish();

        trackTouch("brand_back");
    }

    @OnClick(R.id.add_brand)
    @SuppressWarnings("unused")
    void onAddBrand() {
        if (newBrandCandidate != null) {
            viewModel.addBrand(newBrandCandidate);
            finishWithBrand(newBrandCandidate);
            newBrandCandidate = null;

            trackTouch("brand_add");
        }
    }

    @OnItemClick(R.id.brand_list)
    @SuppressWarnings("unused")
    void onBrandSelected(int position) {
        Object obj = lstBrands.getAdapter().getItem(position);
        if (obj instanceof String) {
            String name = (String) obj;
            finishWithBrand(name);

            trackTouch("brandselectconfirm");
        }
    }

    private void finishWithBrand(String brand) {
        L.d("selected brand: %s", brand);

        Intent result = new Intent();
        result.putExtra(RESULT_KEY_BRAND_NAME, brand);
        setResult(RESULT_OK, result);
        finish();
    }

    @OnClick(R.id.btn_search)
    @SuppressWarnings("unused")
    void onCleanSearch() {
        String keyword = edtSearch.getText().toString().trim();

        if (TextUtils.isEmpty(keyword)) {
            return;
        }

        edtSearch.setText("");
        btnSearch.setImageResource(R.drawable.search_top);
        cleanNewBrand();

        viewModel.loadAllBrands();

        trackTouch("brand_search");
    }

    @OnTextChanged(R.id.search_edit)
    @SuppressWarnings("unused")
    void onKeywordChanged() {
        String keyword = edtSearch.getText().toString().trim();

        if (TextUtils.isEmpty(keyword)) {
            btnSearch.setImageResource(R.drawable.search_top);
            return;
        }

        btnSearch.setImageResource(R.drawable.ic_clean_search);
        viewModel.suggestBrand(keyword);
    }

    private void doSearch(CharSequence text) {
        edtSearch.dismissDropDown();
        String keyword = text.toString().trim();
        viewModel.searchBrands(keyword);

        newBrandCandidate = keyword;
        viewModel.checkBrandExistence(keyword);
    }

    @Override
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {
        switch (property) {
            case all_brands:
                showBrandList((QueryResult) newValue);
                break;
            case brands_search:
                showBrandList((QueryResult) newValue);
                break;
            case suggestions:
                showSearchSuggestion((Cursor) newValue);
                break;
            case brand_existence:
                onBrandExists((Boolean) newValue);
                break;
        }
    }

    private void onBrandExists(Boolean exists) {
        if (exists || TextUtils.isEmpty(newBrandCandidate)) {
            cleanNewBrand();
            return;
        }

        txtAddBrand.setVisibility(View.VISIBLE);
        txtAddBrand.setText(getString(R.string.add_new_brand, newBrandCandidate));
    }

    private void cleanNewBrand() {
        newBrandCandidate = null;
        txtAddBrand.setText("");
        txtAddBrand.setVisibility(View.GONE);
    }

    private void showBrandList(QueryResult result) {
        if (lstBrandsAdapter == null) {
            lstBrandsAdapter = new BrandIndexerCursorAdapter(this, result);
            lstBrands.setAdapter(lstBrandsAdapter);
        } else {
            lstBrandsAdapter.setQueryResult(result);
        }
    }

    private void showSearchSuggestion(Cursor cursor) {
        if (searchSuggestionAdapter == null) {
            searchSuggestionAdapter = new BrandSuggestionAdapter(this, cursor);
            edtSearch.setAdapter(searchSuggestionAdapter);
        } else {
            searchSuggestionAdapter.setCursor(cursor);
        }
    }
}
