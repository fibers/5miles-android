package com.thirdrock.fivemiles.common.item;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.MessageConstant;
import com.insthub.fivemiles.View.ScaleImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.pedrogomez.renderers.Renderer;
import com.thirdrock.domain.ImageInfo;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.ModelUtils;
import com.thirdrock.framework.util.L;

import java.util.Observable;
import java.util.Observer;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;

import static com.thirdrock.fivemiles.util.Currencies.formatCurrency;
import static com.thirdrock.fivemiles.util.DisplayUtils.getNextBackgroundResource;
import static com.thirdrock.fivemiles.util.ImageWidthCalcHelper.IMG_W_L;
import static com.thirdrock.fivemiles.util.LocationUtils.formatItemLocation;
import static com.thirdrock.fivemiles.util.LocationUtils.formatItemLocationWithDistance;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;

/**
 * 用于Render瀑布流，两列形式
 * Created by ywu on 14-10-7.
 */
public class ItemThumbRenderer extends Renderer<ItemThumb> implements Observer {

    @InjectView(R.id.home_item_title)
    protected TextView txtTitle;

    @InjectView(R.id.home_location_item_price)
    protected TextView txtPrice;

    @InjectView(R.id.home_location_item_origin_price)
    protected TextView txtOriginPrice;

    @InjectView(R.id.home_location_item_distance)
    protected TextView txtDistance;

//    @InjectView(R.id.ic_item_audio)
//    ImageView imgAudio;

    @InjectView(R.id.home_location_item_mark)
    protected View icLocation;

    @InjectView(R.id.ic_item_state_sold)
    protected View icSold;

    @InjectView(R.id.home_item_tag_new)
    @Optional
    protected View imgNewTag;  // 子类CollectionItemRenderer无此组件

    protected ScaleImageView imgItemImage;

    private final int layoutResId;
    private final boolean showDistance;
    private boolean renderingSameItem, dirty, imageLoaded, shouldDrawBg = true;
    private String itemImageUrl;

    private ItemImgListener itemImgListener;

    private static class ItemImgListener extends SimpleImageLoadingListener {
        ItemThumbRenderer renderer;

        private ItemImgListener(ItemThumbRenderer renderer) {
            this.renderer = renderer;
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (!TextUtils.equals(renderer.itemImageUrl, imageUri)) {
                // 避免异步结果返回后，当前渲染的图片已经改变(view已被其他item重用)
                L.v("image load complete %s !eq %s", renderer.itemImageUrl, imageUri);
                return;
            }

            L.v("image load complete %s", imageUri);
            renderer.imgItemImage.setImageBitmap(loadedImage);
            renderer.imageLoaded = true;
            renderer.shouldDrawBg = true;
        }
    }

    public ItemThumbRenderer() {
        this(R.layout.tab_location_item, false);
    }

    public ItemThumbRenderer(int layoutResId, boolean showDistance) {
        this.layoutResId = layoutResId;
        this.showDistance = showDistance;
    }

    @Override
    protected void setUpView(View rootView) {
        ButterKnife.inject(this, rootView);
        imgItemImage = (ScaleImageView) rootView.findViewById(R.id.home_location_item_image);
        itemImgListener = new ItemImgListener(this);
    }

    @Override
    protected void hookListeners(View rootView) {
        txtOriginPrice.setPaintFlags(txtOriginPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(layoutResId, parent, false);
    }

    @Override
    public void onRecycle(ItemThumb content) {
        ItemThumb oldItem = getContent();
        if (oldItem != null) {
            oldItem.deleteObserver(this);
            L.v("recycle renderer %s -> %s", oldItem.getTitle(), content.getTitle());
        }

        shouldDrawBg = shouldDrawBg || imageLoaded;  //  初始化或已显示商品图片时背景需要重画

        renderingSameItem = content.equals(oldItem);
        if (renderingSameItem) {
            dirty = content.isDirty(oldItem);

            // copy the thumb info, for preview in item page
            content.setThumbImage(oldItem.getThumbImage());
        } else {
            imageLoaded = false;
        }
        content.addObserver(this);
        super.onRecycle(content);
    }

    @Override
    public void render() {
        ItemThumb item = getContent();
        render(item);
    }

    protected void render(ItemThumb item) {
        // rendering item image
        if (!imageLoaded || dirty) {
            // 此处不判断renderingSameItem，以便加载失败的图片得到再次加载的机会
            renderItemImage(item, item.getDefaultImage());
        }

        if (renderingSameItem && !dirty) {
            // 如果view没有被回收则不需要重新渲染，避免闪烁
            L.v("item is already rendered, skipped: " + item.getTitle());
            return;
        }

        L.v("setup renderer for item %s", item.getTitle());

        // calc & render item distance
        renderLocation(item);

        // rendering title/prices
        txtTitle.setText(item.getTitle());

        String currency = item.getCurrencyCode();
        txtPrice.setText(formatCurrency(currency, item.getPrice()));

        Double originPrice = item.getOriginPrice();
        if (ModelUtils.isValidPrice(originPrice)) {
            txtOriginPrice.setText(formatCurrency(currency, item.getOriginPrice()));
            txtOriginPrice.setVisibility(View.VISIBLE);
        } else {
            txtOriginPrice.setVisibility(View.GONE);
        }

        icSold.setVisibility(item.isSold() ? View.VISIBLE : View.GONE);
        if (imgNewTag != null) {
            imgNewTag.setVisibility(item.isNew() ? View.VISIBLE : View.GONE);
        }

        // update layout of wrap-content views
        txtPrice.requestLayout();
    }

    protected void renderItemImage(final ItemThumb item, final ImageInfo img) {
        L.v("renderItemImage [%s] %s", item.getId(), item.getTitle());
        View rootView = getRootView();

        int w = 0, h = 0;
        if (img != null) {
            w = img.getWidth();
            h = img.getHeight();
        }

        w = w > 0 ? w : IMG_W_L;
        h = h > 0 ? h : IMG_W_L;

        imgItemImage.setImageWidth(w);
        imgItemImage.setImageHeight(h);
        imgItemImage.requestLayout();

        // 图片加载之前先显示一个随机的纯色图片
        drawRandomColorBg();

        // 异步加载图片
        if (rootView.getMeasuredWidth() > 0) {
            loadItemImage(item, img);
        } else {
            rootView.post(new Runnable() {
                @Override
                public void run() {
                    loadItemImage(item, img);
                }
            });
        }

        // 暂不显示语音图标
//        imgAudio.setVisibility(ModelUtils.isNotEmpty(item.getMediaLink()) ? View.VISIBLE : View.GONE);
    }

    protected void drawRandomColorBg() {
        if (shouldDrawBg) {
            imgItemImage.setImageResource(getNextBackgroundResource());
            shouldDrawBg = false;
        }
    }

    protected void loadItemImage(ItemThumb item, ImageInfo img) {
        if (img == null) {
            return;
        }

        L.v("loadItemImage %s w:%d", item.getId(), getRootView().getMeasuredWidth());

        // load clipped version of image
        String fitImgUrl;
        int fitWidth, fitHeight;

        if (img.hasFitUrl()) {
            fitImgUrl = img.getFitUrl();
            fitWidth = img.getFitWidth();
            fitHeight = img.getFitHeight();
        } else {
            int parentWidth = getRootView().getMeasuredWidth();
            fitWidth = (parentWidth > 0 ? parentWidth : IMG_W_L) / 2;
            fitHeight = (int) (fitWidth * img.getHeightRatio());
            fitImgUrl = CloudHelper.toCropUrl(img.getUrl(), fitWidth, null, "fit", null);

            img.setFitUrl(fitImgUrl);
            img.setFitWidth(fitWidth);
            img.setFitHeight(fitHeight);
        }

//        if (fitImgUrl != null) {
//            // url中加入id，避免出现并发的重复任务(图片url相同，但属于不同的item)而被cancel
//            fitImgUrl += (fitImgUrl.contains("?") ? "&" : "?") + "id=" + item.getId();
//        }
        item.setThumbImage(new ImageInfo(fitImgUrl, fitWidth, fitHeight));
        displayItemImage(item, fitImgUrl, fitWidth, fitHeight);
    }

    protected void displayItemImage(final ItemThumb item, String url, int fitWidth, int fitHeight) {
        itemImageUrl = url;
        ImageLoader.getInstance().loadImage(itemImageUrl, new ImageSize(fitWidth, fitHeight),
                FiveMilesApp.imageOptionsHome, itemImgListener);
    }

    private void renderLocation(ItemThumb item) {
        String loc = showDistance ? formatItemLocationWithDistance(item) : formatItemLocation(item);

        if (isNotEmpty(loc)) {
            txtDistance.setVisibility(View.VISIBLE);
            icLocation.setVisibility(View.VISIBLE);
            txtDistance.setText(loc);
        } else {
            txtDistance.setVisibility(View.GONE);
            icLocation.setVisibility(View.GONE);
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        if (!(data instanceof Integer) || getContent() == null) {
            return;
        }

        Integer event = (Integer) data;
        switch (event) {
            case MessageConstant.PROP_DISTANCE_UPDATED:
                renderLocation(getContent());
                break;
        }
    }
}
