package com.thirdrock.fivemiles.common.item;

import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.thirdrock.domain.ImageInfo;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.fivemiles.R;

import butterknife.ButterKnife;

import static com.thirdrock.fivemiles.util.DisplayUtils.getNextBackgroundResource;

/**
 * Rendering item in a chosen collection
 * Created by ywu on 14/12/30.
 */
public class CollectionItemRenderer extends ItemThumbRenderer {

    private final DisplayImageOptions itemPicOpts;
    private ImageView imgItemPic;

    public CollectionItemRenderer() {
        super(R.layout.featured_collection_item, true);
        itemPicOpts = new DisplayImageOptions.Builder()
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();
    }

    @Override
    protected void setUpView(View rootView) {
        ButterKnife.inject(this, rootView);
        imgItemPic = (ImageView) rootView.findViewById(R.id.home_location_item_image);
    }

    protected void renderItemImage(ItemThumb item, ImageInfo img) {
        // 图片加载之前先显示一个随机的纯色图片
        imgItemPic.setImageResource(getNextBackgroundResource());

        // 异步加载图片
        loadItemImage(item, img);
    }

    @Override
    protected void displayItemImage(ItemThumb item, String url, int fitWidth, int fitHeight) {
        ImageLoader.getInstance().displayImage(url, imgItemPic, itemPicOpts);
    }
}
