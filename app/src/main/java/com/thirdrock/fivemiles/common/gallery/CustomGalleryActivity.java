package com.thirdrock.fivemiles.common.gallery;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.insthub.fivemiles.FiveMilesAppConst;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.util.TrackingUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_IMAGE_PATHS;
import static com.insthub.fivemiles.FiveMilesAppConst.EMPTY_STRING;

public class CustomGalleryActivity extends Activity {

	GridView gridGallery;
	Handler handler;
	GalleryAdapter adapter;

	ImageView imgNoMedia;
	Button btnGalleryOk;

	String action;
	private ImageLoader imageLoader;
    private int maximum;
    private ImageView back;
    private TextView cancel;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.gallery);
        Intent intent=getIntent();
		action = intent.getAction();
		if (action == null) {
			finish();
		}
        maximum=intent.getIntExtra(Action.MAXIMUM,-1);
		initImageLoader();
		init();

        TrackingUtils.trackView(FiveMilesAppConst.VIEW_PHOTO_ALBUM);
	}

	private void initImageLoader() {
		try {
			String CACHE_DIR = Environment.getExternalStorageDirectory()
					.getAbsolutePath() + "/.temp_tmp";
			new File(CACHE_DIR).mkdirs();

			File cacheDir = StorageUtils.getOwnCacheDirectory(getBaseContext(),
					CACHE_DIR);

			DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
					.cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY)
					.bitmapConfig(Bitmap.Config.RGB_565).build();
			ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(
					getBaseContext())
					.defaultDisplayImageOptions(defaultOptions)
					.discCache(new UnlimitedDiscCache(cacheDir))
					.memoryCache(new WeakMemoryCache());

			ImageLoaderConfiguration config = builder.build();
			imageLoader = ImageLoader.getInstance();
			imageLoader.init(config);

		} catch (Exception e) {

		}
	}

	private void init() {

		handler = new Handler();
		gridGallery = (GridView) findViewById(R.id.gridGallery);
		gridGallery.setFastScrollEnabled(true);
//        gridGallery.setSelector(R.drawable.);
        gridGallery.setDrawSelectorOnTop(true);
        gridGallery.setHorizontalSpacing((int) getResources().getDimension(
                R.dimen.grid_horizontal_space));
        DisplayMetrics dm=this.getResources().getDisplayMetrics();
        if(dm.widthPixels>1024) {
            gridGallery.setNumColumns(4);
        }else{
            gridGallery.setNumColumns(3);
        }
        gridGallery.setVerticalSpacing((int) getResources().getDimension(
                R.dimen.grid_horizontal_space));
        adapter = new GalleryAdapter(getApplicationContext(), imageLoader);
		PauseOnScrollListener listener = new PauseOnScrollListener(imageLoader,
				true, true);
		gridGallery.setOnScrollListener(listener);

		if (action.equalsIgnoreCase(Action.ACTION_MULTIPLE_PICK)) {

			findViewById(R.id.llBottomContainer).setVisibility(View.VISIBLE);
			gridGallery.setOnItemClickListener(mItemMulClickListener);
			adapter.setMultiplePick(true);

		} else if (action.equalsIgnoreCase(Action.ACTION_PICK)) {

			findViewById(R.id.llBottomContainer).setVisibility(View.GONE);
			gridGallery.setOnItemClickListener(mItemSingleClickListener);
			adapter.setMultiplePick(false);

		}

		gridGallery.setAdapter(adapter);
		imgNoMedia = (ImageView) findViewById(R.id.imgNoMedia);

		btnGalleryOk = (Button) findViewById(R.id.btn_gallery_confirm);
        if(maximum>0){
            String string=getResources().getString(R.string.button_confirm)+"("+adapter.getSelected().size()+ File.separator+maximum+")";
            btnGalleryOk.setText(string);
        }else{
               String string=getResources().getString(R.string.button_confirm)+"("+adapter.getSelected().size()+")";
                btnGalleryOk.setText(string);
        }
		btnGalleryOk.setOnClickListener(mClickListener);

		new Thread() {

			@Override
			public void run() {
				Looper.prepare();
				handler.post(new Runnable() {

					@Override
					public void run() {
						adapter.addAll(getGalleryPhotos());
						checkImageStatus();
					}
				});
				Looper.loop();
			};

		}.start();
        back=(ImageView) findViewById(R.id.tab_album_title_back);
        back.setOnClickListener(mClickListener);
        cancel=(TextView) findViewById(R.id.tab_album_title_cancel);
        cancel.setOnClickListener(mClickListener);
	}

	private void checkImageStatus() {
		if (adapter.isEmpty()) {
			imgNoMedia.setVisibility(View.VISIBLE);
		} else {
			imgNoMedia.setVisibility(View.GONE);
		}
	}

	View.OnClickListener mClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
            if(v.getId()==R.id.btn_gallery_confirm) {
                ArrayList<CustomGallery> selected = adapter.getSelected();

                String[] allPath = new String[selected.size()];
                for (int i = 0; i < allPath.length; i++) {
                    allPath[i] = selected.get(i).sdcardPath;
                }
                Intent data = new Intent().putExtra(ACT_PARA_IMAGE_PATHS, allPath);
                setResult(RESULT_OK, data);
                finish();
            }else  if(v.getId()==R.id.tab_album_title_back){
                Intent data = new Intent().putExtra(ACT_PARA_IMAGE_PATHS, EMPTY_STRING);
                setResult(RESULT_CANCELED, data);
                finish();
            }else if(v.getId()==R.id.tab_album_title_cancel){
                Intent data = new Intent().putExtra(ACT_PARA_IMAGE_PATHS, EMPTY_STRING);
                setResult(RESULT_CANCELED, data);
                finish();
            }
		}
	};
	AdapterView.OnItemClickListener mItemMulClickListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> l, View v, int position, long id) {
            if(adapter.getSelected().size()>=maximum&&maximum>0&&!adapter.getData().get(position).isSeleted){
                Toast.makeText(CustomGalleryActivity.this,CustomGalleryActivity.this.getResources().getString(R.string.up_to_maximum,String.valueOf(maximum)),Toast.LENGTH_SHORT).show();
            }else{
                adapter.changeSelection(v, position);
                if(maximum>0){
                    String string=getResources().getString(R.string.button_confirm)+"("+adapter.getSelected().size()+ File.separator+maximum+")";
                    btnGalleryOk.setText(string);
                }else{
                    String string=getResources().getString(R.string.button_confirm)+"("+adapter.getSelected().size()+")";
                    btnGalleryOk.setText(string);
                }
            }
		}
	};

	AdapterView.OnItemClickListener mItemSingleClickListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> l, View v, int position, long id) {
			CustomGallery item = adapter.getItem(position);
			Intent data = new Intent().putExtra("single_path", item.sdcardPath);
			setResult(RESULT_OK, data);
			finish();
		}
	};

	private ArrayList<CustomGallery> getGalleryPhotos() {
		ArrayList<CustomGallery> galleryList = new ArrayList<CustomGallery>();

		try {
			final String[] columns = { MediaStore.Images.Media.DATA,
					MediaStore.Images.Media._ID };
			final String orderBy = MediaStore.Images.Media._ID;

			Cursor imagecursor = managedQuery(
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns,
					null, null, orderBy);

			if (imagecursor != null && imagecursor.getCount() > 0) {

				while (imagecursor.moveToNext()) {
					CustomGallery item = new CustomGallery();

					int dataColumnIndex = imagecursor
							.getColumnIndex(MediaStore.Images.Media.DATA);

					item.sdcardPath = imagecursor.getString(dataColumnIndex);

					galleryList.add(item);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// show newest photo at beginning of the list
		Collections.reverse(galleryList);
		return galleryList;
	}

}
