package com.thirdrock.fivemiles.common.brand;

import android.database.Cursor;

import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.framework.util.L;
import com.thirdrock.framework.util.rx.Emit1;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.thirdrock.fivemiles.common.brand.BrandDao.QueryResult;

/**
 * Created by ywu on 14/11/11.
 */
public class BrandViewModel extends AbsViewModel<BrandViewModel.Property> {

    public static final int MAX_SUGGEST_COUNT = 500;

    public static enum Property {
        all_brands, brands_search, suggestions, brand_existence
    }

    @Inject
    BrandDao brandDao;

    private Subscription allBrandsSubs, brandsSearchSubs, suggestionSubs, existenceSubs;

    private final Observable<QueryResult> allBrandsQuery = Observable.create(new Emit1<QueryResult>() {
        @Override
        protected QueryResult call() {
            return brandDao.findAllBrandsWithIndex();
        }
    });

    private final Observer<QueryResult> allBrandsObserver = newMajorJobObserver(Property.all_brands);
    private final Observer<QueryResult> brandsSearchObserver = newMajorJobObserver(Property.brands_search);
    private final Observer<Cursor> suggestionObserver = newMinorJobObserver(Property.suggestions);
    private final Observer<Boolean> existenceObserver = newMinorJobObserver(Property.brand_existence);

    public void loadAllBrands() {
        emitMajorJobStarted();
        allBrandsSubs = allBrandsQuery
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(allBrandsObserver);
    }

    public void searchBrands(final String keyword) {
        emitMajorJobStarted();
        brandsSearchSubs = Observable.create(new Emit1<QueryResult>() {
            @Override
            protected QueryResult call() {
                return brandDao.searchBrandsWithIndex(keyword);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(brandsSearchObserver);
    }

    public void suggestBrand(final String keyword) {
        emitJobStarted(Property.suggestions, keyword);
        suggestionSubs = Observable.create(new Emit1<Cursor>() {
            @Override
            protected Cursor call() {
                return brandDao.searchBrands(keyword, MAX_SUGGEST_COUNT);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(suggestionObserver);
    }

    public void onStop() {
        try {
            unsubscribe(allBrandsSubs, brandsSearchSubs, suggestionSubs);
            brandDao.close();
        } catch (Exception e) {
            L.e(e);
        }
    }

    public void checkBrandExistence(final String brand) {
        emitJobStarted(Property.brand_existence, brand);
        existenceSubs = Observable.create(new Emit1<Boolean>() {
            @Override
            protected Boolean call() {
                return brandDao.isBrandExists(brand);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(existenceObserver);
    }

    public void addBrand(String newBrand) {
        brandDao.createBrandByUser(newBrand);
    }

}
