package com.thirdrock.fivemiles.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.insthub.fivemiles.FiveMilesAppConst;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.pedrogomez.renderers.AdapteeCollection;
import com.pedrogomez.renderers.RendererAdapter;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.MyItems;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.activity.AbsActivity;
import com.thirdrock.fivemiles.item.ItemActivity;
import com.thirdrock.fivemiles.main.listing.ListItemActivity;
import com.thirdrock.fivemiles.profile.MyItemsViewModel.Property;
import com.thirdrock.framework.ui.renderer.MonoRendererBuilder;
import com.thirdrock.framework.ui.renderer.SimpleAdapteeCollection;
import com.thirdrock.framework.ui.widget.WaterfallListView;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_SELLING_COUNT;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_USER_ID;
import static com.thirdrock.fivemiles.util.ModelUtils.isNotEmpty;

public class MyItemsActivity extends AbsActivity<MyItemsViewModel, MyItemsViewModel.Property>
        implements WaterfallListView.Callback, MyItemsRenderer.OnClickButtonState {

    @Inject
    MyItemsViewModel myItemsViewModel;

    @InjectView(R.id.list_myitems)
    WaterfallListView listView;

    @InjectView(R.id.blank_view)
    View blankView;

    @InjectView(R.id.blank_view_button)
    TextView tvBlankViewAction;

    @InjectView(R.id.txt_blank_view_desc)
    TextView tvBlankViewDesc;

    @InjectView(R.id.top_view_title)
    TextView tvTopTitle;

    private RendererAdapter<Item> itemsListAdapter;
    private AdapteeCollection<Item> itemsCollection;

    private String screenName;
    private String userID;
    private int itemCount;


    @Override
    protected void doOnCreate(Bundle savedInstanceState) {

        userID = getIntent().getStringExtra(ACT_PARA_USER_ID);
        itemCount = getIntent().getIntExtra(ACT_PARA_SELLING_COUNT, 0);
        screenName = FiveMilesAppConst.VIEW_MY_ITEMS;

        tvTopTitle.setText(R.string.profile_items_for_sale);

        initListView();
    }


    @Override
    protected MyItemsViewModel getViewModel() {
        return myItemsViewModel;
    }

    @Override
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {

        switch (property) {
            case my_items:
                stopMainProgress();
                showItems((MyItems) newValue);
                break;
            case myitems_renew_item:
                stopMainProgress();
                break;
        }
    }

    @Override
    public void onMinorJobError(Property property, Throwable e) {
        switch (property) {
            case my_items:
                stopMainProgress();
                showBlankView(true);
                break;
            case myitems_renew_item:
                stopMainProgress();
                break;
        }
    }

    @Override
    public void onClickButtonState(Item item) {
        myItemsViewModel.renewItem(item.getId());
    }

    @Override
    public void onLoadMore() {
        if (!myItemsViewModel.hasMoreMyItems()) {
            listView.onLoadMoreComplete(true, false);
        }

        myItemsViewModel.getMyItems();
    }


    @Override
    public void onScroll(boolean isAtTop, WaterfallListView.OnScrollListener.ScrollDirection scrollDirection) {

    }

    @Override
    protected String getScreenName() {
        return screenName;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_my_items;
    }

    @OnClick(R.id.top_view_back)
    void onClickNavBack() {
        finish();
    }

    @OnClick(R.id.top_view)
    void onClickNavBar() {
        listView.smoothScrollToPosition(0);
    }

    @OnClick(R.id.blank_view_button)
    void onClickBlankViewButton() {
        startActivity(new Intent(this, ListItemActivity.class));
        trackTouch("myitems_empty");
    }

    @OnItemClick(R.id.list_myitems)
    void onClickItem(int position) {

        int index = position - listView.getHeaderViewsCount();
        if (index < 0 || index >= itemsCollection.size()) {
            return;
        }

        Item item = itemsCollection.get(index);
        String itemID = item.getId();

        if (isNotEmpty(itemID)) {
            startActivity(new Intent(this, ItemActivity.class)
                    .putExtra(ACT_PARA_ITEM_ID, itemID));
            trackTouch("myitems_product");
        }

    }

    @Override
    public void onPauseRendering() {
        ImageLoader.getInstance().pause();
    }

    @Override
    public void onResumeRendering() {
        ImageLoader.getInstance().resume();
    }


    private void initListView() {

        listView.setCallback(this);

        itemsCollection = new SimpleAdapteeCollection<Item>();

        MyItemsRenderer renderer = new MyItemsRenderer(this);
        renderer.setListener(this);
        itemsListAdapter = new RendererAdapter<Item>(
                getLayoutInflater(),
                new MonoRendererBuilder(renderer),
                itemsCollection
        );

        listView.setAdapter(itemsListAdapter);

        showMainProgress(R.string.msg_loading);
        myItemsViewModel.getMyItems();
    }


    private void showItems(MyItems myItems) {

        itemsCollection.clear();
        itemsCollection.addAll(myItems.getItems());
        itemsListAdapter.notifyDataSetChanged();
        showBlankView(itemsCollection.size() == 0);

        listView.onLoadMoreComplete(true, myItemsViewModel.hasMoreMyItems());
    }

    private void showBlankView(boolean visible) {
        stopMainProgress();
        blankView.setVisibility(visible ? View.VISIBLE : View.GONE);

        if (!visible) {
            return;
        }

        tvBlankViewDesc.setText(R.string.hint_empty_selling_list_desc);
        tvBlankViewAction.setText(R.string.empty_selling_list_action);
        tvBlankViewAction.setVisibility(View.VISIBLE);
    }


}
