package com.thirdrock.fivemiles.profile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.pedrogomez.renderers.Renderer;
import com.thirdrock.domain.EnumItemState;
import com.thirdrock.domain.Item;
import com.thirdrock.fivemiles.R;

import org.apache.commons.lang3.StringUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by fibers on 15/7/3.
 */
public class MyItemsRenderer extends Renderer<Item> {

    public interface OnClickButtonState {
        void onClickButtonState(Item item);
    }

    private OnClickButtonState listener;

    @InjectView(R.id.iv_avatar_myitems)
    ImageView ivThumbnail;

    @InjectView(R.id.tv_title_myitems)
    TextView tvTitle;

    @InjectView(R.id.tv_price_myitems)
    TextView tvPrice;

    @InjectView(R.id.btn_state_myitems)
    Button btnState;

    private final Context context;


    public MyItemsRenderer(Context context){
        this.context = context;


    }

    public void setListener(OnClickButtonState listener) {
        this.listener = listener;
    }

    @Override
    protected void setUpView(View rootView) {
    }

    @Override
    protected void hookListeners(View rootView) {

    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup parent) {
        View inflatedView = inflater.inflate(R.layout.myitems_list_item, parent, false);
        ButterKnife.inject(this, inflatedView);

        return inflatedView;
    }

    @Override
    public void render() {
        Item item = getContent();
        renderAvatar(item);
        renderTitle(item);
        renderPrice(item);
        renderState(item);
    }


    @OnClick(R.id.btn_state_myitems)
    void onClickButtonState(){
        if(listener != null){
            Item item = getContent();
            EnumItemState state = item.getState();
            listener.onClickButtonState(item);

            item.setRenewTtl(Long.MAX_VALUE);
            btnState.setText(StringUtils.capitalize(state.toString().toLowerCase()));
            btnState.setEnabled(false);
        }

    }

    private void renderAvatar(Item item){
        String url = item.getImages().get(0).getUrl();
        ImageLoader.getInstance().displayImage(url, ivThumbnail);
    }

    private void renderTitle(Item item){
        tvTitle.setText(item.getTitle());
    }

    private void renderPrice(Item item){
        tvPrice.setText(item.getLocalPrice());
    }

    private void renderState(Item item){
        EnumItemState state = item.getState();
        long renewTTL = item.getRenewTtl();

        if(state == EnumItemState.LISTING && renewTTL == 0){
            btnState.setText(R.string.btn_renew);
            btnState.setEnabled(true);
        }else{
            btnState.setText(StringUtils.capitalize(state.toString().toLowerCase()));
            btnState.setEnabled(false);
        }
    }


}
