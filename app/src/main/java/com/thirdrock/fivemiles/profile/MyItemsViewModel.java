package com.thirdrock.fivemiles.profile;

import com.thirdrock.domain.MyItems;
import com.thirdrock.framework.rest.RestObserver;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.protocol.RenewItemResp;
import com.thirdrock.repository.ItemRepository;
import com.thirdrock.repository.ProfileRepository;

import javax.inject.Inject;

import rx.Subscription;

/**
 * Created by fibers on 15/7/6.
 */
public class MyItemsViewModel extends AbsViewModel<MyItemsViewModel.Property> {

    enum Property{
        my_items, myitems_renew_item
    }

    @Inject
    ProfileRepository profileRepo;

    @Inject
    ItemRepository itemRepo;

    private final RestObserver<RenewItemResp> observerRenewItem = newMinorApiJobObserver("renew_item", Property.myitems_renew_item);

    private final RestObserver<MyItems> observerMyItems = newMinorApiJobObserver("my_sellings", Property.my_items);

    private Subscription subscriptionMyItems;

    private Subscription subscriptionRenewItem;

    @Override
    protected void onModelObserverUnsubscribed() {
        unsubscribe(subscriptionMyItems);
        unsubscribe(subscriptionRenewItem);

    }

    public void getMyItems(){
        subscriptionMyItems = schedule(profileRepo.getMyItems())
                .subscribe(observerMyItems.reset());
    }

    public boolean hasMoreMyItems(){
        return profileRepo.hasMoreMyItems();
    }

    public void renewItem(String itemID) {
        subscriptionRenewItem = schedule(itemRepo.renewItem(itemID))
                .subscribe(observerRenewItem.reset());
    }

}
