package com.thirdrock.fivemiles.profile.setting;

import com.thirdrock.domain.Subscription;
import com.thirdrock.framework.rest.SingleResponseRestObserver;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.repository.PrefsRepository;

import java.util.List;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * View model for NotificationsActivity
 * Created by ywu on 14/11/3.
 */
public class NotificationsViewModel extends AbsViewModel<NotificationsViewModel.Property> {

    public enum Property {
        subscriptions, subscriptionResult
    }

    // observer for result of a single subscription
    private class SubsResultObserver extends SingleResponseRestObserver<Subscription> {

        Subscription originSubscription;

        SubsResultObserver(Subscription originSubscription) {
            super("subscribe");
            this.originSubscription = originSubscription;
        }

        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
            emit(Property.subscriptionResult, e);

            // restore subscription state
            emit(Property.subscriptionResult, originSubscription, originSubscription);
            emitJobCompleted(Property.subscriptionResult, originSubscription);
        }

        @Override
        public void doOnNext(Subscription subscription) {
            emit(Property.subscriptionResult, originSubscription, subscription);
            emitJobCompleted(Property.subscriptionResult, subscription);
        }
    }

    // observer for getting subscriptions from server
    private class SubsObserver extends SingleResponseRestObserver<List<Subscription>> {
        SubsObserver() {
            super("user_subscriptions");
        }

        @Override
        public void onCompleted() {
            emitMajorJobCompleted();
        }

        @Override
        public void onError(Throwable e) {
            emitMajorJobCompleted();
            emit(Property.subscriptions, e);
        }

        @Override
        public void doOnNext(List<Subscription> subscriptions) {
            emit(Property.subscriptions, null, subscriptions);
        }
    }

    private final PrefsRepository repository;

    private final SubsObserver subsObserver;

    @Inject
    public NotificationsViewModel(PrefsRepository repository) {
        this.repository = repository;
        subsObserver = new SubsObserver();
    }

    public void onSubscriptionChanged(Subscription subscription, boolean enabled) {
        Subscription newSubs = subscription.clone();
        newSubs.setEnabled(enabled);

        emitJobStarted(Property.subscriptionResult, newSubs);
        repository.subscribe(newSubs)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SubsResultObserver(subscription).reset());
    }

    /**
     * load subscription state from server if necessary
     */
    public void loadSubscriptions() {
        emitMajorJobStarted();
        repository.getSubscriptions()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subsObserver.reset());
    }
}
