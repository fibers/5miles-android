package com.thirdrock.fivemiles.profile.setting;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.thirdrock.domain.EnumPushMethod;
import com.thirdrock.domain.EnumTopic;
import com.thirdrock.domain.Subscription;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.activity.AbsActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;

import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_NOTIFICATIONS;
import static com.thirdrock.fivemiles.profile.setting.NotificationsViewModel.Property;

public class NotificationsActivity extends AbsActivity<NotificationsViewModel, Property> {

    @Inject
    NotificationsViewModel viewModel;

    @InjectView(R.id.top_view_title)
    TextView txtTitle;

    @InjectView(R.id.push_prefs_container)
    ViewGroup vgPushPrefs;

    @InjectView(R.id.email_prefs_container)
    ViewGroup vgEmailPrefs;

    private final Map<String, SubscriptionView> subsViews = new HashMap<>();

    @Override
    protected String getScreenName() {
        return VIEW_NOTIFICATIONS;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_notifications;
    }

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {
        txtTitle.setText(R.string.lbl_setting_notifications);

        for (EnumPushMethod method : EnumPushMethod.values()) {
            ViewGroup container = method == EnumPushMethod.EMAIL ? vgEmailPrefs : vgPushPrefs;

            for (EnumTopic topic : EnumTopic.values()) {
                if (topic == EnumTopic.UNKNOWN || method == EnumPushMethod.UNKNOWN) {
                    continue;
                }

                Subscription subs = new Subscription(topic, method);
                String key = subs.getKey();
                subsViews.put(key, new SubscriptionView(subs).addView(container));
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // load subscription state from server
        viewModel.loadSubscriptions();
    }

    @Override
    protected NotificationsViewModel getViewModel() {
        return viewModel;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {
        if (newValue == null) {
            return;
        }

        switch (property) {
            case subscriptionResult:
                onSubscriptionResult((Subscription) newValue);
                break;
            case subscriptions:
                onSubscriptionsLoaded((List<Subscription>) newValue);
                break;
        }

    }

    private void onSubscriptionsLoaded(List<Subscription> subscriptions) {
        for (Subscription subs : subscriptions) {
            onSubscriptionResult(subs);
        }
    }

    private void onSubscriptionResult(Subscription resultSubs) {
        SubscriptionView view = subsViews.get(resultSubs.getKey());
        view.update(resultSubs);
    }

    @Override
    public void onJobStarted(Property property, Object data) {
        if (property != Property.subscriptionResult || data == null) {
            return;
        }

        Subscription subs = (Subscription) data;
        SubscriptionView view = subsViews.get(subs.getKey());
        view.onJobStarted();
    }

    @Override
    public void onJobCompleted(Property property, Object data) {
        if (property != Property.subscriptionResult || data == null) {
            return;
        }

        Subscription subs = (Subscription) data;
        SubscriptionView view = subsViews.get(subs.getKey());
        view.onJobCompleted();
    }

    @OnClick(R.id.top_view_back)
    @SuppressWarnings("unused")
    void onNaviBack() {
        finish();
    }

    private class SubscriptionView implements CompoundButton.OnCheckedChangeListener {
        Subscription subscription;
        CheckBox cbxSwitch;
        View progress;

        SubscriptionView(Subscription subscription) {
            this.subscription = subscription;
        }

        SubscriptionView addView(ViewGroup parent) {
            View v = getLayoutInflater().inflate(R.layout.subscription_item, null);

            ((TextView) v.findViewById(R.id.subs_name)).setText(getPrefName());

            cbxSwitch = (CheckBox) v.findViewById(R.id.subs_switch);
            cbxSwitch.setChecked(subscription.isEnabled());
            cbxSwitch.setOnCheckedChangeListener(this);

            progress = v.findViewById(R.id.subs_prog);

            parent.addView(v);

            return this;
        }

        private String getPrefName() {
            String name = "";

            switch (subscription.getTopic()) {
                case NEW_FOLLOWERS:
                    name = getString(R.string.pref_notification_follower);
                    break;
                case RECOMMENDATIONS:
                    name = getString(R.string.pref_notification_recommend);
                    break;
                case MARKETING_EVENTS:
                    name = getString(R.string.pref_notification_market);
                    break;
            }

            return name;
        }

        public void update(Subscription resultSubs) {
            subscription.setEnabled(resultSubs.isEnabled());
            cbxSwitch.setOnCheckedChangeListener(null);
            cbxSwitch.setChecked(resultSubs.isEnabled());
            cbxSwitch.setOnCheckedChangeListener(this);
        }

        public void onJobStarted() {
            progress.setVisibility(View.VISIBLE);
            cbxSwitch.setEnabled(false);
        }

        public void onJobCompleted() {
            progress.setVisibility(View.GONE);
            cbxSwitch.setEnabled(true);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            viewModel.onSubscriptionChanged(subscription, isChecked);
        }
    }
}
