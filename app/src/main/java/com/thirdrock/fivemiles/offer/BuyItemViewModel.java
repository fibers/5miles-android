package com.thirdrock.fivemiles.offer;

import com.thirdrock.domain.Item;
import com.thirdrock.framework.ui.viewmodel.AbsViewModel;
import com.thirdrock.protocol.MakeOfferResp;
import com.thirdrock.repository.OfferRepository;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.thirdrock.fivemiles.offer.BuyItemViewModel.Property.offer_sent;

/**
 * View Model for BuyItem activity
 * Created by ywu on 15/2/2.
 */
public class BuyItemViewModel extends AbsViewModel<BuyItemViewModel.Property> {

    public enum Property {
        offer_sent
    }

    private class MakeOfferObserver extends MajorApiJobObserver<MakeOfferResp> {
        private MakeOfferObserver() {
            super( "make_offer", offer_sent);
        }

        @Override
        public void doOnNext(MakeOfferResp pResp) {
            if(pResp != null) {
                offerLineId = pResp.getOfferLineId();
                super.doOnNext(pResp);
            }
        }
    }

    private final MakeOfferObserver makeOfferObserver;

    @Inject
    OfferRepository offerRepo;

    private Item item;
    private int offerLineId;

    private Subscription subsOffer;

    public BuyItemViewModel() {
        makeOfferObserver = new MakeOfferObserver();
    }

    public void onStop() {
        unsubscribe(subsOffer);
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getOfferLineId() {
        return offerLineId;
    }

    public void setOfferLineId(int offerLineId) {
        this.offerLineId = offerLineId;
    }

//    public void sendMessage(String pMsg) {
//        sendMessageWithPrice(pMsg, EMPTY_STRING);
//    }

    public void sendMessageWithPrice(String pMsg, String priceStr) {
        if (item == null || item.getOwner() == null) {
            return;
        }

        emitMajorJobStarted();
        subsOffer = offerRepo.sendTextMessage(item.getOwner().getId(), item.getId(), priceStr, pMsg)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(makeOfferObserver.reset());
    }
}
