package com.thirdrock.fivemiles.offer;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.AppEventsLogger;
import com.insthub.fivemiles.Activity.MakeOfferActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.thirdrock.domain.ImageInfo;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.ItemThumb;
import com.thirdrock.fivemiles.R;
import com.thirdrock.fivemiles.framework.activity.AbsActivity;
import com.thirdrock.fivemiles.framework.view.NumberInputFilter;
import com.thirdrock.fivemiles.util.CloudHelper;
import com.thirdrock.fivemiles.util.Currencies;
import com.thirdrock.fivemiles.util.ImageWidthCalcHelper;
import com.thirdrock.fivemiles.util.TrackingUtils;
import com.thirdrock.protocol.MakeOfferResp;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;

import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_ITEM;
import static com.insthub.fivemiles.FiveMilesAppConst.ACT_PARA_OFFER_LINE_ID;
import static com.insthub.fivemiles.FiveMilesAppConst.EMPTY_STRING;
import static com.insthub.fivemiles.FiveMilesAppConst.EPSILON;
import static com.insthub.fivemiles.FiveMilesAppConst.VIEW_BUY;
import static com.thirdrock.fivemiles.offer.BuyItemViewModel.Property;
import static com.thirdrock.fivemiles.util.DisplayUtils.formatPriceDigits;
import static com.thirdrock.fivemiles.util.DisplayUtils.toast;
import static com.thirdrock.fivemiles.util.ModelUtils.isValidPrice;
import static com.thirdrock.framework.util.Utils.doOnLayoutChange;
import static com.thirdrock.framework.util.Utils.setBackground;

public class BuyItemActivity extends AbsActivity<BuyItemViewModel, Property> {

    public static final double MIN_PRICE_RATIO = 0.3;

    @Inject
    BuyItemViewModel viewModel;

    @InjectView(R.id.root_view)
    View rootView;

    @InjectView(R.id.txt_offer_hint)
    TextView txtOfferHint;

    @InjectView(R.id.lbl_currency)
    TextView txtCurrency;

    @InjectView(R.id.edt_offer_price)
    EditText edtPrice;

    private boolean originPriceCleaned;
    private DisplayImageOptions itemImgOpts;

    private AppEventsLogger fbEventsLogger;

    @Override
    protected String getScreenName() {
        return VIEW_BUY;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_buy_item;
    }

    @Override
    protected BuyItemViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {

        fbEventsLogger = AppEventsLogger.newLogger(this);

        itemImgOpts = new DisplayImageOptions.Builder()
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        getWindow().setBackgroundDrawableResource(R.drawable.loading);
        findViewById(R.id.top_view_logo).setVisibility(View.VISIBLE);

        initPriceEditor();

        onIntent(getIntent());
    }

    @Override
    protected void onResume(){
        super.onResume();

        fbEventsLogger.logEvent(VIEW_BUY);
    }

    private void initPriceEditor() {
        edtPrice.setFilters(new InputFilter[]{new NumberInputFilter(0, 9999999.99)});
        edtPrice.setImeOptions(EditorInfo.IME_ACTION_SEND);
        edtPrice.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (EditorInfo.IME_ACTION_SEND != actionId) {
                    return false;
                }

                sendOffer();
                return true;
            }
        });
    }

    private void onIntent(Intent intent) {
        Item item = (Item) intent.getSerializableExtra(ACT_PARA_ITEM);
        viewModel.setItem(item);

        int offerLineId = intent.getIntExtra(ACT_PARA_OFFER_LINE_ID, 0);
        viewModel.setOfferLineId(offerLineId);

        showItemThumb(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        viewModel.onStop();
    }

    @Override
    public void onPropertyChanged(Property property, Object oldValue, Object newValue) throws Exception {
        switch (property) {
            case offer_sent:
                MakeOfferResp resp = (MakeOfferResp) newValue;
                if(resp != null) {
                    int offerLineId = resp.getOfferLineId();
                    openOfferLine(offerLineId);
                }

                break;
        }
    }

    private void showItemThumb(ItemThumb itemThumb) {
        if (itemThumb == null) {
            return;
        }

        showBasicInfo(itemThumb);
    }

    private void showBasicInfo(final ItemThumb item) {
        txtOfferHint.setText(getString(R.string.txt_buy_item_hint, item.getTitle()));
        txtCurrency.setText(Currencies.getCurrencySymbol(item.getCurrencyCode()));
        edtPrice.setText(formatPriceDigits(item.getPrice()));

        doOnLayoutChange(rootView, new Runnable() {
            @Override
            public void run() {
                loadItemImage(item);
            }
        });
    }

    protected void loadItemImage(ItemThumb item) {
        ImageInfo img = item.getDefaultImage();
        if (img == null) {
            return;
        }

        ImageSize imgSize = getReasonableImageSize(rootView.getMeasuredWidth(),
                rootView.getMeasuredHeight());
        String url = CloudHelper.toCropUrl(img.getUrl(),
                imgSize.getWidth(), imgSize.getHeight(), "fill", true);

        ImageLoader.getInstance().loadImage(url, imgSize, itemImgOpts, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                setBackground(getWindow(), loadedImage);
            }
        });
    }

    private ImageSize getReasonableImageSize(int w, int h) {
        double ratio = w > 0 ? ((double) h) / w : 1;
        int fitWidth = ImageWidthCalcHelper.getOnlineImageWidth((int) (w * 0.8));
        int fitHeight = (int) (fitWidth * ratio);
        return new ImageSize(fitWidth, fitHeight);
    }

    @OnClick(R.id.top_view_back)
    void onBack() {
        finish();

        trackTouch("buy_back");
    }

    @OnClick(R.id.btn_offer)
    void sendOffer() {

        fbEventsLogger.logEvent("offer_send");

        if (!validOffer()) {
            return;
        }
        Item item = viewModel.getItem();

        String priceStr = edtPrice.getText().toString().trim();
        double price = Double.parseDouble(priceStr);
        String localizedPrice;
        if (price < EPSILON) { // 低于EPSILON的正值被认为是0
            localizedPrice = getString(R.string.price_free);
        } else {
            String currencyCode = (item == null)? EMPTY_STRING : item.getCurrencyCode();
            localizedPrice = Currencies.formatCurrency(currencyCode, price);
        }
        String title = (item == null)? "item" : item.getTitle();
        String offerMessage = getResources().getString(R.string.msg_offer, title, localizedPrice);

        viewModel.sendMessageWithPrice(offerMessage, priceStr);

        trackTouch("make_offer");
        TrackingUtils.trackAFEvent("make_offer", null);
    }

    @OnClick(R.id.edt_offer_price)
    @SuppressWarnings("unused")
    void onPriceEditorClicked() {
        if (!originPriceCleaned) {
            edtPrice.setText("");
            originPriceCleaned = true;
        }

        trackTouch("edt_offer_price");
    }

    private boolean validOffer() {
        Item item = viewModel.getItem();
        String strOffer = edtPrice.getText().toString().trim();

        if (!isValidPrice(strOffer)) {
            toast(R.string.error_price_required);
            edtPrice.selectAll();
            return false;
        }

        if (item != null && isValidPrice(item.getPrice())) {
            Double price = item.getPrice();
            Double offer = Double.valueOf(strOffer);

            if (isOfferTooLow(offer, price)) {
                toast(R.string.err_offer_too_low);
                edtPrice.selectAll();
                return false;
            }
        }

        return true;
    }

    private boolean isOfferTooLow(Double offer, Double price) {
        return offer < price * MIN_PRICE_RATIO;
    }

    private void openOfferLine(int offerLineId) {
        startActivity(new Intent(this, MakeOfferActivity.class)
                        .putExtra(ACT_PARA_OFFER_LINE_ID, offerLineId)
// 既然有了offerline id，就不需要传Item了 .putExtra(ACT_PARA_ITEM, viewModel.getItem())

        );
        finish();
    }
}
