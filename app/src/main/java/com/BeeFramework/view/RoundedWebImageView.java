package com.BeeFramework.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.thirdrock.fivemiles.R;

import static com.thirdrock.fivemiles.util.DisplayUtils.toPixels;

/**
 * User: howie
 * Date: 13-4-22
 * Time: 下午5:31
 */
public class RoundedWebImageView extends ImageView{

//    private Paint paint;
//    private int roundWidth = 5;
//    private int roundHeight = 5;
//    private Paint paint2;

    private boolean drawBorder;  // draw a circle border
    private final float borderWidth, borderInset;

    public RoundedWebImageView(Context context, AttributeSet attSet) {
        super(context, attSet);
        init(context, attSet);
        borderWidth = toPixels(getContext(), 3f);
        borderInset = borderWidth / 2f;
    }

    private void init(Context context, AttributeSet attrs) {
//        paint = new Paint();
//        paint.setColor(Color.WHITE);
//        paint.setAntiAlias(true);
//        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
//
//        paint2 = new Paint();
//        paint2.setXfermode(null);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.RoundedWebImageView,
                0, 0);

        try {
            drawBorder = a.getBoolean(R.styleable.RoundedWebImageView_border, false);
        } finally {
            a.recycle();
        }
    }


//    @Override
//    public void draw(Canvas canvas) {
//
//        roundWidth = getWidth()/2;
//        roundHeight = getHeight()/2;
//        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
//        Canvas canvas2 = new Canvas(bitmap);
//        super.draw(canvas2);
//        drawLiftUp(canvas2);
//        drawRightUp(canvas2);
//        drawLiftDown(canvas2);
//        drawRightDown(canvas2);
//        canvas.drawBitmap(bitmap, 0, 0, paint2);
//        bitmap.recycle();
//    }



//    private void drawLiftUp(Canvas canvas) {
//        Path path = new Path();
//        path.moveTo(0, roundHeight);
//        path.lineTo(0, 0);
//        path.lineTo(roundWidth, 0);
//        path.arcTo(new RectF(
//                0,
//                0,
//                roundWidth*2,
//                roundHeight*2),
//                -90,
//                -90);
//        path.close();
//        canvas.drawPath(path, paint);
//    }
//
//    private void drawLiftDown(Canvas canvas) {
//        Path path = new Path();
//        path.moveTo(0, getHeight()-roundHeight);
//        path.lineTo(0, getHeight());
//        path.lineTo(roundWidth, getHeight());
//        path.arcTo(new RectF(
//                0,
//                getHeight()-roundHeight*2,
//                0+roundWidth*2,
//                getWidth()),
//                90,
//                90);
//        path.close();
//        canvas.drawPath(path, paint);
//    }
//
//    private void drawRightDown(Canvas canvas) {
//        Path path = new Path();
//        path.moveTo(getWidth()-roundWidth, getHeight());
//        path.lineTo(getWidth(), getHeight());
//        path.lineTo(getWidth(), getHeight()-roundHeight);
//        path.arcTo(new RectF(
//                getWidth()-roundWidth*2,
//                getHeight()-roundHeight*2,
//                getWidth(),
//                getHeight()), 0, 90);
//        path.close();
//        canvas.drawPath(path, paint);
//    }
//
//    private void drawRightUp(Canvas canvas) {
//        Path path = new Path();
//        path.moveTo(getWidth(), roundHeight);
//        path.lineTo(getWidth(), 0);
//        path.lineTo(getWidth()-roundWidth, 0);
//        path.arcTo(new RectF(
//                getWidth()-roundWidth*2,
//                0,
//                getWidth(),
//                0+roundHeight*2),
//                -90,
//                90);
//        path.close();
//        canvas.drawPath(path, paint);
//    }


    /**
     * 转换图片成圆形
     * @param bitmap 传入Bitmap对象
     * @return
     */
    public Bitmap toRoundBitmap(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        float roundPx;
        float left,top,right,bottom,dst_left,dst_top,dst_right,dst_bottom;
        if (width <= height) {
            roundPx = width / 2;
            top = 0;
            bottom = width;
            left = 0;
            right = width;
            height = width;
            dst_left = 0;
            dst_top = 0;
            dst_right = width;
            dst_bottom = width;
        } else {
            roundPx = height / 2;
            float clip = (width - height) / 2;
            left = clip;
            right = width - clip;
            top = 0;
            bottom = height;
            width = height;
            dst_left = 0;
            dst_top = 0;
            dst_right = height;
            dst_bottom = height;
        }

        Bitmap output = Bitmap.createBitmap(width,
                height, Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff004242;
        final Paint paint = new Paint();
        final Rect src = new Rect((int)left, (int)top, (int)right, (int)bottom);
        final Rect dst = new Rect((int)dst_left, (int)dst_top, (int)dst_right, (int)dst_bottom);
        final RectF rectF = new RectF(dst);

        paint.setAntiAlias(true);

        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, src, dst, paint);

        // draw an orange border
        if (drawBorder) {
            paint.reset();
            paint.setARGB(0x66, 0xff, 0x88, 0x30);
            paint.setFlags(Paint.ANTI_ALIAS_FLAG);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(borderWidth);
            canvas.drawCircle(width / 2f, height / 2f, roundPx - borderInset + 1, paint);
        }
        return output;
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        Bitmap square = toRoundBitmap(bm);
        super.setImageBitmap(square);
//        if (bm.getWidth() != bm.getHeight())
//        {
//
//        }
//        else
//        {
//            super.setImageBitmap(bm);
//        }

    }
}