package com.BeeFramework.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;

public class MyProgressDialog {
	
	public Dialog mDialog;
	private AnimationDrawable animationDrawable = null;
	
	public MyProgressDialog(Context context, String message) {
		
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(R.layout.progress_view, null);

		TextView text = (TextView) view.findViewById(R.id.progress_message);
		text.setText(message);
		ImageView loadingImage = (ImageView) view.findViewById(R.id.progress_view);
		loadingImage.setImageResource(R.anim.loading_animation);
		animationDrawable = (AnimationDrawable)loadingImage.getDrawable();
		animationDrawable.setOneShot(false);
		animationDrawable.start();
		
		mDialog = new Dialog(context, R.style.dialog_prog);
		mDialog.setContentView(view);
		mDialog.setCanceledOnTouchOutside(false);
		
	}

    public boolean isShowing() {
        return mDialog.isShowing();
    }

	public MyProgressDialog show() {
        mDialog.show();
        return this;
	}
	
	public MyProgressDialog setCanceledOnTouchOutside(boolean cancel) {
		mDialog.setCanceledOnTouchOutside(cancel);
        return this;
	}

    public MyProgressDialog setCancelable(boolean flag) {
        mDialog.setCancelable(flag);
        return this;
    }
	
	public void dismiss() {
		if(mDialog.isShowing()) {
            try {
                mDialog.dismiss();
                animationDrawable.stop();
            }
            catch (Exception e) {
                L.e(e);
            }
		}
	}

}
