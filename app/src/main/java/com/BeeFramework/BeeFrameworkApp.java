package com.BeeFramework;

/*
 *	 ______    ______    ______
 *	/\  __ \  /\  ___\  /\  ___\
 *	\ \  __<  \ \  __\_ \ \  __\_
 *	 \ \_____\ \ \_____\ \ \_____\
 *	  \/_____/  \/_____/  \/_____/
 *
 *
 *	Copyright (c) 2013-2014, {Bee} open source community
 *	http://www.bee-framework.com
 *
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a
 *	copy of this software and associated documentation files (the "Software"),
 *	to deal in the Software without restriction, including without limitation
 *	the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *	and/or sell copies of the Software, and to permit persons to whom the
 *	Software is furnished to do so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in
 *	all copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *	IN THE SOFTWARE.
 */

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;

import com.BeeFramework.Utils.CustomExceptionHandler;
import com.external.activeandroid.app.Application;
import com.facebook.Session;
import com.insthub.fivemiles.SESSION;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LRULimitedMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import org.json.JSONObject;

import java.io.File;

public class BeeFrameworkApp extends Application implements OnClickListener{
    private static BeeFrameworkApp instance;
    private ImageView bugImage;
    public Context currContext;

    private WindowManager wManager ;
    private boolean flag = true;
    
    public Handler messageHandler;

    private JSONObject bncReferringParams;

    public static BeeFrameworkApp getInstance()
    {
        if (instance == null) {
            instance = new BeeFrameworkApp();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        instance = this;
//        long start = System.currentTimeMillis();
        super.onCreate();
//        long end = System.currentTimeMillis();
//        System.out.println("timing app parent parent: " + (end - start));

        //FrontiaApplication.initFrontiaApplication(this);
        initConfig();
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + AppConst.LOG_DIR_PATH;
        File storePath = new File(path);
        storePath.mkdirs();
        Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler(
                path, null));
        
        initImageLoader(this);
    }

    /**
     * SESSION在getInstance()里面会完成初始化，不必手动去初始化。
     */
    void initConfig() {
//        long start = System.currentTimeMillis();
//        SESSION.getInstance().load();     // SESSION在构造单例之后会马上初始化
//        long end = System.currentTimeMillis();
//        System.out.println("timing initConfig: " + (end - start));

        if (SESSION.getInstance().isFbLogin()) {
            Session fbSession = Session.getActiveSession();
            if (fbSession == null || fbSession.isClosed()) {
                Session.openActiveSessionFromCache(this);
            }
        }
    }
    
    public void showBug(final Context context) {
        // empty
    }

    public void onClick(View v) {
//    	if(flag != false) {
//    		Intent intent = new Intent(BeeFrameworkApp.getInstance().currContext,DebugTabActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//    	}
//        flag = true;
    }
    
    public static void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you may tune some of them,
		// or you can create default configuration by
		//  ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
//				.threadPriority(2)
//				.threadPoolSize(2)
                .memoryCache(new LRULimitedMemoryCache(5 * 1024 * 1024))
				.denyCacheImageMultipleSizesInMemory()                      // 使用cloudinary，每个url的图片大小都是一样的。而对应的view，有时会因为计算的原因，大小计算会不准确。没啥用
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
				.diskCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.LIFO)
//                .writeDebugLogs()   // Remove for release app
				.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}

    /**
     * 用户未登陆时，缓存branch参数。防止未登陆用户进入系统
     * @param referringParams
     */
    public void storeReferringParams(JSONObject referringParams) {
        this.bncReferringParams = referringParams;
    }

    public JSONObject retrieveReferringParams(){
        return this.bncReferringParams;
    }

    public void clearReferringParams(){
        this.bncReferringParams = null;
    }

}
