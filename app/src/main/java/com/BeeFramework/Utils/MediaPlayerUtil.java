package com.BeeFramework.Utils;

import android.media.MediaPlayer;

public class MediaPlayerUtil {

	public static MediaPlayer player;
	
	public MediaPlayerUtil() {
		
		if(player != null) {
			player.release();
	    	player=null;
		}
		player = new MediaPlayer();
	}
	
}
