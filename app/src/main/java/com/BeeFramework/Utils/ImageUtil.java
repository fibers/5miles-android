package com.BeeFramework.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import com.insthub.fivemiles.FiveMilesApp;
import com.insthub.fivemiles.FiveMilesAppConst;
import com.thirdrock.framework.util.L;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.insthub.fivemiles.FiveMilesAppConst.EMPTY_STRING;

public class ImageUtil {

    public static String adjustImage(String path, int maxBmpSize, int maxWidth) {
        return adjustImage(path, null, maxBmpSize, maxWidth);
    }

    public static String adjustImage(Uri pImageUri, int pMaxBmpSize, int pMaxWidth) {
        return adjustImage(pImageUri, null, pMaxBmpSize, pMaxWidth);
    }

    /**
     * 返回修正后文件的路径
     * TODO 重构整个图片处理逻辑，改写成处理ContentProvider的数据，而不是绝对路径
     */
    public static String adjustImage(Uri pImageUri, String pCacheName, int pMaxBmpSize, int pMaxWidth) {
        Log.d("AdjustImage","start="+System.currentTimeMillis());

        String resultPath = EMPTY_STRING;
        try {
            InputStream imageInputStream = getInputStreamFromUri(pImageUri);
            if (imageInputStream == null) {
                L.e("Image InputSream is null");
                return null;
            }

            // ------------------------ 获取图片宽度 ------------------------
            Options options = new Options();
            options.inJustDecodeBounds = true;                            // 并不真正decode到内存，只是获取高宽
            BitmapFactory.decodeStream(imageInputStream, null, options);                      // options.inJustDecodeBounds == true的时候返回null，options里面含有图片的高宽
            // 从这里开始的decodeFile都是真正decode图片到内存中
            options.inJustDecodeBounds = false;

            // ------------------------ 若图片最长边超出最大范围，缩放 ------------------------
            // 若图片超过最大范围的两倍，需要将其缩放到2倍以内，再解码并加载到内存中，节省内存
            // 因为图片可能存在旋转，所以长宽的最大值相同
            options.inSampleSize = ImageUtil.calculateInSampleSize(options, pMaxBmpSize, pMaxBmpSize);


            // InputStream会自动关闭，在这里需要重新打开
            imageInputStream = getInputStreamFromUri(pImageUri);
            if (imageInputStream == null) {
                L.e("Image InputSream is null");
                return null;
            }

            // 所有图片都要先解码，再进行编码，实现图片压缩
            Bitmap originBitmap = BitmapFactory.decodeStream(imageInputStream);        // inJustDecodeBounds = true

            // 若图片解码失败，则返回null
            if (null == originBitmap) {
                L.w("image from stream " + imageInputStream + " can't be decoded into Bitmap");
                return null;
            }

            Matrix matrix = new Matrix();
            boolean isRotated = false;
            // ------------------------ 有些手机如三星在拍照的时候会自动旋转90度，这里要转过来 ------------------------
            int degree = getImageOrientation(pImageUri);
            if (degree != 0){                                           // 被旋转过了
                L.d("start rotating bitmap with orient degree " + degree);
                isRotated = true;
                matrix.postRotate(degree);
            }

            // ------------------------- 缩放图片 -------------------------
            // 保证图片宽度不超过最大值
            int memBmpWidth = isRotated? originBitmap.getHeight() : originBitmap.getWidth();
            if (memBmpWidth > pMaxBmpSize) {
                float memScale = pMaxWidth / (float)memBmpWidth;
                matrix.postScale(memScale, memScale);
            }

            Bitmap processedBitmap = Bitmap.createBitmap(originBitmap, 0, 0,
                    originBitmap.getWidth(), originBitmap.getHeight(), matrix, true);

            // 生成一个新文件，所以path设为null
            resultPath = saveBitmapToFile(processedBitmap, pCacheName, null);
            Log.d("AdjustImage", "end=" + System.currentTimeMillis());
        } catch (OutOfMemoryError err) {
            L.e("OOM" + err);
        }

        return resultPath;
    }

    public static InputStream getInputStreamFromUri(Uri pImageUri) {
        InputStream imageInputStream = null;
        try {
            imageInputStream = FiveMilesApp.appCtx.getContentResolver().openInputStream(pImageUri);
        } catch (FileNotFoundException e) {
            L.e("FileNotFoundException thrown by Uri " + pImageUri + ", and stack trace is " + e);
        }
        return imageInputStream;
    }

    /**
     * 对原始图片进行处理：若图片的大小超过了最大范围，则进行缩放；若图片被旋转，则纠正过来；压缩；
     * @param path  要处理的图片存放路径
     */
    public synchronized static String adjustImage(String path, String cacheName, int maxBmpSize, int maxWidth) {
        Log.d("AdjustImage","start="+System.currentTimeMillis());
        String resultPath = path;
        try {
            // ------------------------ 获取图片宽度 ------------------------
            Options options = new Options();
            options.inJustDecodeBounds = true;                            // 并不真正decode到内存，只是获取高宽
            BitmapFactory.decodeFile(path, options);                      // options.inJustDecodeBounds == true的时候返回null，options里面含有图片的高宽
            // 从这里开始的decodeFile都是真正decode图片到内存中
            options.inJustDecodeBounds = false;

            // ------------------------ 若图片最长边超出最大范围，缩放 ------------------------
            // 若图片超过最大范围的两倍，需要将其缩放到2倍以内，再解码并加载到内存中，节省内存
            // 因为图片可能存在旋转，所以长宽的最大值相同
            options.inSampleSize = ImageUtil.calculateInSampleSize(options, maxBmpSize, maxBmpSize);

            // 所有图片都要先解码，再进行编码，实现图片压缩
            Bitmap memBitmap = BitmapFactory.decodeFile(path, options);        // inJustDecodeBounds = true

            // 若图片解码失败，则返回原来的路径
            if (null == memBitmap) {
                L.w("image in " + path + " can't be decoded into Bitmap");
                return resultPath;
            }

            Matrix matrix = new Matrix();
            boolean isRotated = false;
            // ------------------------ 有些手机如三星在拍照的时候会自动旋转90度，这里要转过来 ------------------------
            int degree = getImageOrientation(path);
            if (degree != 0){                                           // 被旋转过了
                L.d("start rotating bitmap with orient degree " + degree);
                isRotated = true;
                matrix.postRotate(degree);
            }

            // ------------------------- 缩放图片 -------------------------
            // 保证图片宽度不超过最大值
            int memBmpWidth = isRotated? memBitmap.getHeight() : memBitmap.getWidth();
            if (memBmpWidth > maxWidth) {
                float memScale = maxWidth / (float)memBmpWidth;
                matrix.postScale(memScale, memScale);
            }

            Bitmap adjustedBmp = Bitmap.createBitmap(memBitmap, 0, 0,
                    memBitmap.getWidth(), memBitmap.getHeight(), matrix, true);

            // ------------------------ 将图片保存到文件，并返回 ------------------------
            resultPath = saveBitmapToFile(adjustedBmp, cacheName, path);
            Log.d("AdjustImage", "end=" + System.currentTimeMillis());
        } catch (OutOfMemoryError err) {
            L.e("OOM" + err);
        }

        return resultPath;
    }

    private static String saveBitmapToFile(Bitmap bitmap, String cacheName, String path) {
        if (bitmap == null){
            L.w("bitmap is null in saveBitmapToFile()");
            return path;
        }

        File file = new File(FiveMilesAppConst.FILEPATH + "img/");
        if (!file.exists()) {
            file.mkdirs();
        }

        String fileName;
        if (path != null && path.startsWith(FiveMilesAppConst.FILEPATH)) {
            fileName = path;
        } else {
            if (!TextUtils.isEmpty(cacheName)) {
                fileName = FiveMilesAppConst.FILEPATH + "img/" + cacheName;
            } else {
                fileName = FiveMilesAppConst.FILEPATH + "img/" + FiveMilesAppConst.imageName();
            }
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(fileName);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 60, fos);// 把数据写入文件
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                bitmap.recycle();
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return fileName;
    }

    public static int getGoogleStaticMapWidth(float widthInDimen) {
        float calculatedWidth = widthInDimen / (float)FiveMilesAppConst.GOOGLE_STATIC_MAP_SCALE;

        return Math.round(calculatedWidth);
    }

    public static int getGoogleStaticMapHeight(float heightInDimen) {
        float calculatedWidth = heightInDimen / (float)FiveMilesAppConst.GOOGLE_STATIC_MAP_SCALE;

        return Math.round(calculatedWidth);
    }

    public static int dp2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    public static int dp2px(float dp) {
        float density = FiveMilesApp.appCtx.getResources().getDisplayMetrics().density;
        return Math.round(dp * density);
    }

    public static float sp2px(float sp){
        final float scale = FiveMilesApp.appCtx.getResources().getDisplayMetrics().scaledDensity;
        return sp * scale;
    }

    public static int getImageOrientation(String photoPath){
        return getImageOrientation(new File(photoPath));
    }

    public static int getImageOrientation(File photoFile) {
        return getImageOrientation(Uri.fromFile(photoFile));
    }

    public static int getImageOrientation(Uri photoUri) {
        int orient = 0;

        try {
            String scheme = photoUri.getScheme();
            if ("file".equalsIgnoreCase(scheme)) {
                orient = getImageOrientationFromFile(photoUri);
            } else {
                orient = getImageOrientationFromImageStore(photoUri);
            }
        } catch (Exception e) {
            L.e(e);
        }

        return orient;
    }

    public static int getImageOrientationFromImageStore(Uri uri) {
        Cursor cursor = null;
        try {
            Context ctx = FiveMilesApp.getInstance();
            cursor = ctx.getContentResolver().query(uri,
                    new String[]{MediaStore.Images.ImageColumns.ORIENTATION},
                    null, null, null);

            if (cursor == null || cursor.getCount() < 1) {
                return 0;
            }

            cursor.moveToFirst();
            return cursor.getInt(0);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static int getImageOrientationFromFile(Uri uri) throws Exception {
        int angle = 0;

        File f = new File(uri.getPath());
        ExifInterface exif = new ExifInterface(f.getPath());
        int orientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            angle = 90;
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            angle = 180;
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            angle = 270;
        }

        return angle;
    }

    // rotate bitmap if necessary, according to the camera orientation
    public static Bitmap rotateBitmap(Bitmap source, int orient) {
        if (orient == 0) {
            return source;
        }
        L.d("start rotating bitmap with orient degree " + orient);

        if (source == null){
            L.w("source bitmap is null in rotateBitmap()");
            return null;
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(orient);
        return Bitmap.createBitmap(source, 0, 0,
                source.getWidth(), source.getHeight(), matrix, true);
    }

    /**
     * http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
     * Android Demo中的代码，直接拿过来用了
     */
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     * http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
     * Android Demo中的代码，直接拿过来用了
     */
    public static Bitmap decodeBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }
}
