package com.BeeFramework.Utils;

import android.text.TextUtils;

import com.insthub.fivemiles.FiveMilesApp;
import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * User: howie
 * Date: 13-5-11
 * Time: 下午4:09
 */
// TODO replace or move it from BeeFramework
public final class TimeUtil {

    private static final ThreadLocal<SimpleDateFormat> DATETIME_FMT =
            new ThreadLocal<SimpleDateFormat>() {
                @Override
                protected SimpleDateFormat initialValue() {
                    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'UTC'");
                    fmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                    return fmt;
                }
            };

    private static final long ONE_DAY_MS = 86400000;

    private TimeUtil() {}

    public static String getNow() {
        return getElapse(new Date());
    }

    public static Date parse(String timeStr) {
        Date dt = new Date(0);

        if (TextUtils.isEmpty(timeStr)) {
            return dt;
        }

        try {
            SimpleDateFormat format = DATETIME_FMT.get();
            dt = format.parse(timeStr);
        } catch (ParseException e) {
            L.e(e);
        }

        return dt;
    }

    public static String timeAgo(String timeStr) {
        if (TextUtils.isEmpty(timeStr)) {
            return "";
        }

        Date date = parse(timeStr);
        return getElapse(date);
    }

    public static String timeAgo(long timestamp) {
        if (timestamp <= 0)
            return "";
        Date date = new Date(timestamp);
        return getElapse(date);
    }

    public static String getElapse(Date date) {
        if (date == null) {
            date = new Date();
        }

//        AQUtility.debug("input timestamp: " + date.getTime());
        long timeStamp = date.getTime();

//        AQUtility.debug("current timestamp: " + System.currentTimeMillis());
        long currentTimeStamp = System.currentTimeMillis();
        long seconds = (currentTimeStamp - timeStamp) / 1000;
//        AQUtility.debug("seconds: " + seconds);

        long minutes = Math.abs(seconds / 60);
        long hours = Math.abs(minutes / 60);
        long days = Math.abs(hours / 24);


        if (seconds < 60) {
            return getString(R.string.time_elapse_now);
        } else if (seconds < 120) {
            return getString(R.string.time_elapse_1m);
        } else if (minutes < 60) {
            return getString(R.string.time_elapse_m, minutes);
        } else if (minutes < 120) {
            return getString(R.string.time_elapse_1h);
        } else if (hours < 24) {
            return getString(R.string.time_elapse_h, hours);
        } else if (hours < 24 * 2) {
            return getString(R.string.time_elapse_1d);
        } else if (days < 30) {
            return getString(R.string.time_elapse_d, days);
        } else if (days < 365) {
            return getString(R.string.time_elapse_month,
                    new BigDecimal(days / 30).setScale(0, BigDecimal.ROUND_HALF_UP));
        } else {
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
//            String dateString = formatter.format(date);
//            return dateString;
            return DateFormat.getDateInstance(DateFormat.MEDIUM).format(date);
        }
    }

    public static String toConciseTime(String timeStr) {
        if (TextUtils.isEmpty(timeStr)) {
            return "";
        }

        return toConciseTime(parse(timeStr), false);
    }

    public static String toConciseTime(int tsInSec) {
        return toConciseTime(tsInSec, false);
    }

    public static String toConciseTime(int tsInSec, boolean includeTime) {
        if (tsInSec <= 0) {
            return "";
        }

        return toConciseTime(new Date(tsInSec * 1000L), includeTime);
    }

    public static String toConciseTime(Date dt, boolean includeTime) {
        if (dt == null) {
            return "";
        }

        long currTs = System.currentTimeMillis();
        long elapse = currTs - dt.getTime();

        // date style使用DateFormat.MEDIUM，time style使用DateFormat.SHORT。日期若使用DateFormat.SHORT，会使用两位数显示。
        DateFormat dtFmt = elapse < ONE_DAY_MS ?
                DateFormat.getTimeInstance(DateFormat.SHORT) :
                (includeTime ?
                        DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT) :
                        DateFormat.getDateInstance(DateFormat.MEDIUM));

        return dtFmt.format(dt);
    }

    private static String getString(int resId, Object...args) {
        return FiveMilesApp.getInstance().getString(resId, args);
    }
}
