package com.BeeFramework.Utils;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;

public class AnimationUtil {

	/**
	 * 弹出动画效果(从下到上)
	 */
	public static void showAnimation(View view) {
		AnimationSet animationSet = new AnimationSet(true);
		TranslateAnimation translateAnimation = new TranslateAnimation(
				Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, 0f,
				Animation.RELATIVE_TO_SELF, 1f, Animation.RELATIVE_TO_SELF, 0f);
		translateAnimation.setDuration(200);
		animationSet.addAnimation(translateAnimation);
		view.startAnimation(animationSet);
	}
	
	/**
	 * 退出动画效果(从上到下)
	 */
	public static void backAnimation(View view) {
		AnimationSet animationSet = new AnimationSet(true);
		TranslateAnimation translateAnimation = new TranslateAnimation(
				Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, 0f,
				Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, 1f);
		translateAnimation.setDuration(200);
		animationSet.addAnimation(translateAnimation);
		view.startAnimation(animationSet);
	}
	
	/**
	 * 弹出动画效果(从上到下)
	 */
	public static void showAnimationFromTop(View view) {
		AnimationSet animationSet = new AnimationSet(true);
		TranslateAnimation translateAnimation = new TranslateAnimation(
				Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, 0f,
				Animation.RELATIVE_TO_SELF, -1f, Animation.RELATIVE_TO_SELF, 0f);
		translateAnimation.setDuration(200);
		animationSet.addAnimation(translateAnimation);
		view.startAnimation(animationSet);
	}
	
	/**
	 * 退出动画效果(从下到上)
	 */
	public static void backAnimationFromBottom(View view) {
		AnimationSet animationSet = new AnimationSet(true);
		TranslateAnimation translateAnimation = new TranslateAnimation(
				Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, 0f,
				Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, -1f);
		translateAnimation.setDuration(200);
		animationSet.addAnimation(translateAnimation);
		view.startAnimation(animationSet);
	}
	
}
