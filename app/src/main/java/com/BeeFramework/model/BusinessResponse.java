package com.BeeFramework.model;

import org.json.JSONArray;
import org.json.JSONException;

import com.external.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.text.ParseException;

public interface BusinessResponse
{    
	public abstract void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) throws JSONException;
}
