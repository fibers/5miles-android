package com.BeeFramework.model;

/*
 *	 ______    ______    ______
 *	/\  __ \  /\  ___\  /\  ___\
 *	\ \  __<  \ \  __\_ \ \  __\_
 *	 \ \_____\ \ \_____\ \ \_____\
 *	  \/_____/  \/_____/  \/_____/
 *
 *
 *	Copyright (c) 2013-2014, {Bee} open source community
 *	http://www.bee-framework.com
 *
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a
 *	copy of this software and associated documentation files (the "Software"),
 *	to deal in the Software without restriction, including without limitation
 *	the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *	and/or sell copies of the Software, and to permit persons to whom the
 *	Software is furnished to do so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in
 *	all copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *	IN THE SOFTWARE.
 */

import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;

import com.BeeFramework.view.MyProgressDialog;
import com.BeeFramework.view.ToastView;
import com.external.androidquery.AQuery;
import com.external.androidquery.callback.AjaxCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.thirdrock.fivemiles.R;
import com.thirdrock.framework.util.L;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BaseModel implements BusinessResponse {

    public static final String TAG = BaseModel.class.getSimpleName();

    protected BeeQuery aq;
    protected ArrayList<BusinessResponse> businessResponseArrayList = new ArrayList<BusinessResponse>();
    protected Context mContext;

//    public SharedPreferences shared;
//    public SharedPreferences.Editor editor;

    public BaseModel() {

    }

    public BaseModel(Context context) {
        aq = new BeeQuery(context);
        mContext = context;
//        shared = context.getSharedPreferences(FiveMilesAppConst.USERINFO, 0);
//        editor = shared.edit();
    }

    public Context getContext() {
        return mContext;
    }

    protected void saveCache() {
        return;
    }

    protected void cleanCache() {
        return;
    }

    public void addResponseListener(BusinessResponse listener) {
        if (!businessResponseArrayList.contains(listener)) {
            businessResponseArrayList.add(listener);
        } else {

        }
    }

    public void removeResponseListener(BusinessResponse listener) {
        businessResponseArrayList.remove(listener);
    }

    //公共的错误处理
    public void callback(BeeCallback callback, String url, JSONObject jo, AjaxStatus status) {
//        if (null == jo)
//            jo = new JSONObject();
//
        if (null == jo) {
//            ToastView toast = new ToastView(mContext, "The data load failure");
//            toast.setGravity(Gravity.CENTER, 0, 0);
//            toast.show();
//            return;
            L.e("ajax request failed, url:%s, status:[%s] %s",
                    url,
                    status.getCode(),
                    status.getMessage());
        }

    }

    public void errorCallback() {
        ToastView toast = new ToastView(mContext, mContext.getString(R.string.error_network_failure));
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    //公共的错误处理
    public boolean errorCallback(String url, String errorCode, String errorDesc) {
        String errorMsg = null;

        if (errorCode != null && errorCode.startsWith("5")) {    // 5xx错误提示服务器内部错误
            errorMsg = mContext.getResources().getString(R.string.error_server_internal);
        } else {
            errorMsg = errorDesc;
        }

        ToastView toast = new ToastView(mContext, errorMsg);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        return true;
    }

    public boolean errorCallback(String url, AjaxStatus status) {
        String msg = status.getRestErrorMessage();

        if (TextUtils.isEmpty(msg)) {
            msg = status.getError();
        }

        if (TextUtils.isEmpty(msg)) {
            msg = status.getMessage();
        }

        return errorCallback(url, String.valueOf(status.getCode()), msg);
    }

    public void OnMessageResponse(String url, JSONObject jo, AjaxStatus status) throws JSONException {

        for (BusinessResponse iterable_element : businessResponseArrayList) {
            iterable_element.OnMessageResponse(url, jo, status);
        }

        if (businessResponseArrayList.size() == 0) {

        }
    }

    public <K> AQuery ajax(AjaxCallback<K> callback) {
        return aq.ajax(callback);
    }

    public <K> AQuery ajaxProgress(AjaxCallback<K> callback) {
        MyProgressDialog pro = new MyProgressDialog(mContext,
                mContext.getString(R.string.msg_loading));
        return (AQuery) aq.progress(pro.mDialog).ajax(callback);
    }

    public <K> AQuery ajaxProgress(AjaxCallback<K> callback, String str) {
        MyProgressDialog pro = new MyProgressDialog(mContext, str);
        return (AQuery) aq.progress(pro.mDialog).ajax(callback);
    }

}
