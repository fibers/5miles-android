package com.BeeFramework.model;

import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import com.external.androidquery.callback.AjaxCallback;
import com.external.androidquery.callback.AjaxStatus;
import com.insthub.fivemiles.Protocol.*;

/*
 *	 ______    ______    ______
 *	/\  __ \  /\  ___\  /\  ___\
 *	\ \  __<  \ \  __\_ \ \  __\_
 *	 \ \_____\ \ \_____\ \ \_____\
 *	  \/_____/  \/_____/  \/_____/
 *
 *
 *	Copyright (c) 2013-2014, {Bee} open source community
 *	http://www.bee-framework.com
 *
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a
 *	copy of this software and associated documentation files (the "Software"),
 *	to deal in the Software without restriction, including without limitation
 *	the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *	and/or sell copies of the Software, and to permit persons to whom the
 *	Software is furnished to do so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in
 *	all copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *	IN THE SOFTWARE.
 */

public class MockServer
{
    private static MockServer instance;
    public static MockServer getInstance()
    {
        if (instance == null) {
            instance = new MockServer();
        }
        return instance;
    }

    public static <K> void ajax(AjaxCallback<K> callback)
    {
        String url = callback.getUrl();
        Class<K> type = callback.getType();
        K ret = null;

        try {
            ret = handleRequest(url, type);
            callback.callback(url, ret, new AjaxStatus(200, "OK"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

//		JSONObject responseJsonObject = new JSONObject();
//		callback.result = (K) responseJsonObject.toString();
//		((BeeCallback) callback).callback(callback.getUrl(), responseJsonObject,
//                callback.getStatus());
   }

    @SuppressWarnings("unchecked")
    private static <K> K handleRequest(String url, Class<K> type) throws JSONException {
        Object ret = null;

        if (TextUtils.isEmpty(url)) {
            return null;
        }

        if (url.startsWith(ApiInterface.MESSAGE_BUYING)) {
            ret = mockBuyingMessages();
        }
        else if (url.startsWith(ApiInterface.MESSAGE_SELLING)) {
            ret = mockSellingMessages();
        }
        else if (url.startsWith(ApiInterface.MESSAGE_SYSTEM)) {
            ret = mockSystemMessages();
        }
        else if (url.startsWith(ApiInterface.OFFER_LINE_DETAILS)) {
            ret = mockOfferLineDetail();
        }

        return (K) ret;
    }

    private static JSONObject mockBuyingMessages() throws JSONException {
        String jsonStr = "{\n" +
                "  \"meta\": {\n" +
                "    \"limit\": 20,\n" +
                "    \"offset\": 0,\n" +
                "    \"total_count\": 1,\n" +
                "    \"previous\": null,\n" +
                "    \"next\": null\n" +
                "  },\n" +
                "  \"objects\": [\n" +
                "    {\n" +
                "      \"unread_num\": 3,\n" +
                "      \"item_image\": \"http://res.cloudinary.com/fivemiles/image/upload/v1409187633/n0a6wfvuzhkzekzjfnln.jpg\",\n" +
                "      \"type\": \"O\",\n" +
                "      \"offer_type\": 2,\n" +
                "      \"message\": \"offer comment is here\",\n" +
                "      \"nick\": \"Me\",\n" +
                "      \"from_user_id\": \"GEzr83omVj\",\n" +
                "      \"from_user_nick\": \"User King\",\n" +
                "      \"head\": \"http://res.cloudinary.com/fivemiles/image/upload/v1408676759/wzgk2lx3dtyquwd0whgq.png\",\n" +
                "      \"thread_id\": 12,\n" +
                "      \"timestamp\": 1408420050\n" +
                "    },\n" +
                "    {\n" +
                "      \"unread_num\": 13,\n" +
                "      \"item_image\": \"http://res.cloudinary.com/fivemiles/image/upload/v1409187633/n0a6wfvuzhkzekzjfnln.jpg\",\n" +
                "      \"type\": \"O\",\n" +
                "      \"price\": \"$344.49\",\n" +
                "      \"message\": \"offer comment is here\",\n" +
                "      \"nick\": \"Me\",\n" +
                "      \"from_user_id\": \"GEzr83omVj\",\n" +
                "      \"from_user_nick\": \"User King\",\n" +
                "      \"head\": \"http://res.cloudinary.com/fivemiles/image/upload/v1408676759/wzgk2lx3dtyquwd0whgq.png\",\n" +
                "      \"accepted\": false,\n" +
                "      \"thread_id\": 12,\n" +
                "      \"timestamp\": 1408420050\n" +
                "    }, {\n" +
                "      \"unread_num\": 0,\n" +
                "      \"item_image\": \"http://res.cloudinary.com/fivemiles/image/upload/v1409187633/n0a6wfvuzhkzekzjfnln.jpg\",\n" +
                "      \"type\": \"F\",\n" +
                "      \"nick\": \"Me\",\n" +
                "      \"rate_num\": 4,\n" +
                "      \"message\": \"feedback comment is here\",\n" +
                "      \"from_user_id\": \"GEzr83omVj\",\n" +
                "      \"from_user_nick\": \"User King\",\n" +
                "      \"head\": \"http://res.cloudinary.com/fivemiles/image/upload/v1408676759/wzgk2lx3dtyquwd0whgq.png\",\n" +
                "      \"thread_id\": 3,\n" +
                "      \"status\": 1,\n" +
                "      \"timestamp\": 1408420050\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        return new JSONObject(jsonStr);
    }

    private static JSONObject mockSellingMessages() throws JSONException {
        String jsonStr = "{\n" +
                "  \"meta\": {\n" +
                "    \"limit\": 20,\n" +
                "    \"offset\": 0,\n" +
                "    \"total_count\": 1,\n" +
                "    \"previous\": null,\n" +
                "    \"next\": null\n" +
                "  },\n" +
                "  \"objects\": [\n" +
                "  ]\n" +
                "}";
        return new JSONObject(jsonStr);
    }

    public static JSONObject mockSystemMessages() throws JSONException {
        String jsonStr = "{\n" +
                "  \"meta\": {\n" +
                "    \"limit\": 20,\n" +
                "    \"offset\": 0,\n" +
                "    \"total_count\": 2,\n" +
                "    \"previous\": null,\n" +
                "    \"next\": null\n" +
                "  },\n" +
                "  \"objects\": [{\n" +
                "      \"id\": 2,\n" +
                "      \"text\": \"Glasses was moved to another category: xx\",\n" +
                "      \"timestamp\": 1408420064,\n" +
                "      \"unread\": true\n" +
                "    },{\n" +
                "      \"id\": 3,\n" +
                "      \"text\": \"item click test\",\n" +
                "      \"timestamp\": 1408420064,\n" +
                "      \"unread\": true,\n" +
                "      \"image\": \"http://res.cloudinary.com/fivemiles/image/upload/v1413696296/q5kzlvoglgd6vkygtgrt.jpg\",\n" +
                "      \"action\": \"i:OvYEZ7JEXEgK8ze6\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        return new JSONObject(jsonStr);
    }

    public static JSONObject mockOfferLineDetail() throws JSONException {
        String jsonStr = "{\n" +
                "    \"meta\": {\n" +
                "        \"buyer\": {\n" +
                "            \"portrait\": \"http://res.cloudinary.com/fivemiles/image/upload/v1409130452/mzgzxr9fgyt4lv1hn99g.jpg\",\n" +
                "            \"nickname\": \"wc\",\n" +
                "            \"id\": \"dKzoVqOJPD\"\n" +
                "        },\n" +
                "        \"item\": {\n" +
                "            \"title\": \"we the people\",\n" +
                "            \"local_price\": \"£0.99\",\n" +
                "            \"created_at\": \"1409131367\",\n" +
                "            \"location\": {\n" +
                "                \"lat\": 39.998444,\n" +
                "                \"lon\": 116.4806\n" +
                "            },\n" +
                "            \"owner\": {\n" +
                "                \"portrait\": \"http://res.cloudinary.com/fivemiles/image/upload/v1409130962/voccr9uaqypfqcgdqvrg.jpg\",\n" +
                "                \"nickname\": \"we\",\n" +
                "                \"id\": \"DAjr5kOdbQ\"\n" +
                "            },\n" +
                "            \"id\": \"3vl0b7rvKPnXwZzM\",\n" +
                "            \"currency\": \"GBP\",\n" +
                "            \"state\": \"1\",\n" +
                "            \"mediaLink\": \"\",\n" +
                "            \"images\": [\n" +
                "                {\n" +
                "                    \"url\": \"http://res.cloudinary.com/fivemiles/image/upload/v1409130981/rexxiy0nky3lerr4zpcb.jpg\",\n" +
                "                    \"width\": 1024,\n" +
                "                    \"imageLink\": \"http://res.cloudinary.com/fivemiles/image/upload/v1409130981/rexxiy0nky3lerr4zpcb.jpg\",\n" +
                "                    \"height\": 732\n" +
                "                }\n" +
                "            ],\n" +
                "            \"cat_id\": \"0\",\n" +
                "            \"price\": \"0.99\",\n" +
                "            \"desc\": \"we the people\"\n" +
                "        },\n" +
                "        \"latest_ts\": 1409725685,\n" +
                "        \"sold_to_others\": false\n" +
                "    },\n" +
                "    \"objects\": [\n" +
                "      {\n" +
                "        \"local_price\": \"\",\n" +
                "        \"text\": \"@#%Hi call\",\n" +
                "        \"price\": 0,\n" +
                "        \"from_buyer\": true,\n" +
                "        \"from_me\": false,\n" +
                "        \"timestamp\": \"2014-09-02T07:42:02UTC\",\n" +
                "        \"type\": 2,\n" +
                "        \"id\": \"60\"\n" +
                "      }\n" +
                "    ]\n" +
                "  }";
        return new JSONObject(jsonStr);
    }
}
