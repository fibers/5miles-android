package com.BeeFramework.model;

import com.external.androidquery.callback.AjaxStatus;

/**
* Created by lishangqiang on 2014/9/4.
*/
public interface HttpStatusConsumer {
    public void consumeHttpStatus(String url, AjaxStatus status);
}
