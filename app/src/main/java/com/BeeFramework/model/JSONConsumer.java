package com.BeeFramework.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
* Created by lishangqiang on 2014/9/4.
*/
public interface JSONConsumer {
    public void consumeJSON(JSONObject jo) throws JSONException;
}
