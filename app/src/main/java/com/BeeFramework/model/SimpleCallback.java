package com.BeeFramework.model;

import com.external.androidquery.callback.AjaxStatus;
import com.thirdrock.fivemiles.util.ModelUtils;

import org.apache.http.HttpStatus;
import com.thirdrock.framework.util.L;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lishangqiang on 2014/9/3.
 */
public class SimpleCallback extends  BeeCallback<JSONObject> {

    private BaseModel model;

    private JSONConsumer jsonConsumer;
    private HttpStatusConsumer statusConsumer = new HttpStatusConsumer() {
        @Override
        public void consumeHttpStatus(String url, AjaxStatus status) {
            if (status.getCode() != HttpStatus.SC_OK)
                model.errorCallback(url, status);
        }
    };

    public SimpleCallback(BaseModel model) {
        this.model = model;
    }

    public SimpleCallback jsonConsumer(JSONConsumer jsonConsumer) {
        this.jsonConsumer = jsonConsumer;
        return this;
    }

    public SimpleCallback statusConsumer(HttpStatusConsumer statusConsumer) {
        this.statusConsumer = statusConsumer;
        return this;
    }

    @Override
    public void callback(String url, JSONObject jo, AjaxStatus status) {
        if (status.getCode() == 200) {
            try {
                model.callback(this, url, jo, status);
                if (jo != null && jsonConsumer != null) {
                    String error = jo.optString("error");
                    if(ModelUtils.isEmpty(error)) {
                        jsonConsumer.consumeJSON(jo);
                        model.OnMessageResponse(url, jo, status);
                    }
                    else {
                        String error_description = jo.optString("error_description");
                        model.errorCallback(url, error, error_description);
                    }
                }
            } catch (JSONException e) {
                L.e("api failed", e);
                model.errorCallback();
            }
        }
        if (statusConsumer != null) {
            statusConsumer.consumeHttpStatus(url, status);
        }
    }


}
