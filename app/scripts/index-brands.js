var brandData = require('../src/main/res/raw/brands');
var brands = [];
var alphabet = {};

brandData.data.forEach(function (b) {
  brands.push(b.replace(/^\s+|\s+$/g, ''));
});
brands.sort(function (a, b) {
  var ac = a[0].toUpperCase();
  var bc = b[0].toUpperCase();
  if (ac < bc) {
    return -1;
  } else if (ac > bc) {
    return 1;
  } else {
    return 0;
  }
});

for (var i = 0; i < brands.length; i++) {
  var c = brands[i][0].toUpperCase();
  if (alphabet[c] === undefined) {
    alphabet[c] = i;
  }
}

console.log(JSON.stringify({
  data: brands,
  index: alphabet
}, null, '\t'));
