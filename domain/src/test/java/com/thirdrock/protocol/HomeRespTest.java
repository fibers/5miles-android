package com.thirdrock.protocol;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.thirdrock.domain.ItemThumb;

import org.junit.Test;

import java.net.URL;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class HomeRespTest {

    @Test
    public void testHomeRespParsing() throws Exception {
        URL dataFile = getClass().getResource("home-resp.json");
        String json = Resources.toString(dataFile, Charsets.UTF_8);

        HomeResp resp = HomeResp__JsonHelper.parseFromJson(json);
        Meta meta = resp.getMeta();
        List<ItemThumb> items = resp.getItems();

        assertTrue(isNotEmpty(meta.getNext()));
        assertNull(meta.getPrevious());
        assertEquals(100, meta.getTotalCount());
        assertTrue(meta.getLimit() > 0);
        assertTrue(meta.getOffset() > 0);

        assertThat(items).hasSize(2)
                .extracting("id", "price", "location.longitude", "location.latitude")
                .contains(
                        tuple("One01", 56.0, -0.45f, -23.7f),
                        tuple("Two02", 23.0, 116f, 40f)
                );
        assertThat(items).extracting("images.size").contains(6, 1);
    }
}