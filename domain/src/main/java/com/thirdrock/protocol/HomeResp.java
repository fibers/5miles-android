package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.domain.ItemThumb;

import java.util.Collections;
import java.util.List;

/**
 * Created by ywu on 14-9-27.
 */
@JsonType
public class HomeResp {
    @JsonField(fieldName = "meta")
    Meta meta;

    @JsonField(fieldName = "objects")
    List<ItemThumb> items;

    HomeResp() {

    }

    public Meta getMeta() {
        return meta;
    }

    public List<ItemThumb> getItems() {
        return items == null ? Collections.<ItemThumb>emptyList() :
                Collections.unmodifiableList(items);
    }
}
