package com.thirdrock.protocol;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;

/**
 * A wrapper of SystemAction type and related action data.
 * Created by ywu on 15/4/10.
 */
public class SystemActionData {
    public static final Pattern ACT_PATTERN = Pattern.compile("^([a-z0-9]+)(?::(.+?))?(?:,([a-z0-9]+):(.+))?$", CASE_INSENSITIVE);

    SystemAction action;
    String actionData;
    Map<String, String> extraActionData = Collections.emptyMap();

    public SystemActionData() {
    }

    public SystemActionData(SystemAction action, String actionData, Map<String, String> extraActionData) {
        this.action = action;
        this.actionData = actionData;
        this.extraActionData = extraActionData;
    }

    public static SystemActionData valueOf(String actionStr) {
        SystemAction actionType = null;
        String actionData = null;
        Map<String, String> extraActionData = new HashMap<>();

        if (actionStr != null && actionStr.trim().length() > 0) {
            Matcher mt = ACT_PATTERN.matcher(actionStr.trim());
            if (mt.matches()) {
                actionType = SystemAction.parse(mt.group(1));
                actionData = mt.group(2);

                // extra action data
                String k = mt.group(3);
                String v = mt.group(4);
                if (StringUtils.isNotEmpty(k) && StringUtils.isNotEmpty(v)) {
                    extraActionData.put(k, v);
                }
            } else {
                System.err.println("SystemAction parsing failed, invalid action string: " + actionStr);
            }
        }

        return new SystemActionData(actionType, actionData, extraActionData);
    }

    public SystemAction getAction() {
        return action;
    }

    public String getActionData() {
        return actionData;
    }

    public Map<String, String> getExtraActionData() {
        return Collections.unmodifiableMap(extraActionData);
    }
}
