package com.thirdrock.protocol;

/**
 * Created by ywu on 14/10/20.
 */
public enum SystemAction {
    message, profile, home, item, link, search, category, featured_collection, reviews;

    public static SystemAction parse(String v) {
        SystemAction act = home;
        if (v == null || v.length() == 0) {
            return act;
        }

        switch (v.charAt(0)) {
            case 'm':  // rename to 'n' since v3.0
            case 'n':  // 'n' for notification, alias to system message
                act = message;
                break;
            case 'p':
                act = profile;
                break;
            case 'i':
                act = item;
                break;
            case 'u':
                act = link;
                break;
            case 's':
                act = search;
                break;
            case 'c':
                act = category;
                break;
            case 'f':
                act = featured_collection;
                break;
            case 'r':
                act = reviews;
                break;
            default:
                act = home;
                break;
        }

        return act;
    }
}
