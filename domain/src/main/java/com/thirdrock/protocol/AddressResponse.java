package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.ArrayList;

@JsonType
public class AddressResponse {

	@JsonField(fieldName = "results")
	public ArrayList<Address> results = new ArrayList<>();

	@JsonField(fieldName = "status")
	public String status;

}


