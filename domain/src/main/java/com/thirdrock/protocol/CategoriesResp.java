package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.domain.CategoryInfo;

import java.util.Collections;
import java.util.List;

/**
 * 对应/categories/返回json,的model
 * Created by jubin on 6/3/15.
 */
@JsonType
public class CategoriesResp {
    @JsonField(fieldName = "meta")
    Meta meta;

    @JsonField(fieldName = "objects")
    List<CategoryInfo> categories;

    public Meta getMeta() {
        return meta;
    }

    public List<CategoryInfo> getCategories() {
        return categories == null ? Collections.<CategoryInfo>emptyList() : categories;
    }
}
