package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.domain.Campaign;

import java.util.List;

/**
 * Created by ywu on 14/12/9.
 */
@JsonType
public class CampaignResp {
    @JsonField(fieldName = "meta")
    Meta meta;

    @JsonField(fieldName = "objects")
    List<Campaign> campaigns;

    public Meta getMeta() {
        return meta;
    }

    public List<Campaign> getCampaigns() {
        return campaigns;
    }
}