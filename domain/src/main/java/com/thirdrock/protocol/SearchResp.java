package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.domain.CategoryInfo;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.SearchFilter;

import java.util.Collections;
import java.util.List;

/**
 * Search response
 * Created by ywu on 15/3/11.
 */
@JsonType
public class SearchResp {

    @JsonField(fieldName = "meta")
    Meta meta;

    @JsonField(fieldName = "objects")
    List<Item> items;

    @JsonField(fieldName = "res_ext")
    FilterMeta filterMeta;

    public Meta getMeta() {
        return meta;
    }

    public List<Item> getItems() {
        return items == null ? Collections.<Item>emptyList() : items;
    }

    /**
     * The filters which lead to this search result
     */
    public SearchFilter getSearchFilter() {
        return filterMeta == null ? null : filterMeta.getFilter();
    }

    /**
     * Category-based stats of the search result
     */
    public List<CategoryInfo> getCategoryStats() {
        return filterMeta == null ? Collections.<CategoryInfo>emptyList() :
                filterMeta.getCategoryStats();
    }
}
