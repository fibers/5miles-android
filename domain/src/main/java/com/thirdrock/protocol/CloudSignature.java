package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * Created by ywu on 14-10-7.
 */
@JsonType
public class CloudSignature {
    @JsonField(fieldName = "timestamp")
    int timestamp;

    @JsonField(fieldName = "api_key")
    String apiKey;

    @JsonField(fieldName = "signature")
    String signature;

    CloudSignature() {

    }

    public int getTimestamp() {
        return timestamp;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getSignature() {
        return signature;
    }
}
