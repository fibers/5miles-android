package com.thirdrock.protocol.offer;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.io.Serializable;

/**
 * Message in chat
 * Created by jubin on 1/6/15.
 */
@JsonType
public class ChatMessage implements Serializable{
    public static final int MSG_TYPE_TEXT = 0;
    public static final int MSG_TYPE_IMAGE = 1;
    public static final int MSG_TYPE_LOCATION = 2;

    @JsonField(fieldName = "id")
    String id;

    @JsonField(fieldName = "msg_type")
    int messageType;

    @JsonField(fieldName = "local_price")
    String localPrice;

    @JsonField(fieldName = "timestamp")
    String timestamp;

    @JsonField(fieldName = "price")
    double price;

    @JsonField(fieldName = "from_buyer")
    boolean isFromBuyer;

    @JsonField(fieldName = "from_me")
    boolean isFromMe;

    @Deprecated
    @JsonField(fieldName = "text")
    String text;

    @JsonField(fieldName = "type")
    int type;

    @JsonField(fieldName = "payload")
    ChatMessagePayload payload;

    private boolean isTemp = false;

    /**
     * Kitkat及以上使用Android系统的Uri，非KitKat使用FILE_SCHEME
     */
    public static ChatMessage getTempImageMessage(String pImagePathOrUri, boolean pIsSeller, boolean pIsKitKat) {
        ChatMessage msg = new ChatMessage();
        msg.isTemp = true;
        msg.isFromMe = true;
        msg.isFromBuyer = !pIsSeller;
        msg.messageType = MSG_TYPE_IMAGE;
        if (pIsKitKat) {
            msg.payload = ChatMessagePayload.getTempImagePayloadKitkat(pImagePathOrUri);
        } else {
            msg.payload = ChatMessagePayload.getTempImagePayload(pImagePathOrUri);
        }

        return msg;
    }

    public static ChatMessage getTempLocationMessage(double pLat, double pLng, String pPlaceName
            , String pAddressName, String pMapThumbUrl, boolean pIsSeller) {
        ChatMessage msg = new ChatMessage();
        msg.isTemp = true;
        msg.isFromMe = true;
        msg.isFromBuyer = !pIsSeller;
        msg.messageType = MSG_TYPE_LOCATION;
        msg.payload = ChatMessagePayload.getTempLocationPayload(pLat, pLng, pPlaceName
                , pAddressName, pMapThumbUrl);

        return msg;
    }

    public String getId() {
        return id;
    }

    public int getMessageType() {
        return messageType;
    }

    public String getLocalPrice() {
        return localPrice;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public double getPrice() {
        return price;
    }

    public boolean isFromBuyer() {
        return isFromBuyer;
    }

    public boolean isFromMe() {
        return isFromMe;
    }

    public String getText() {
        return text;
    }

    public int getType() {
        return type;
    }

    public ChatMessagePayload getPayload() {
        return payload;
    }

    public void setIsTemp(boolean isTemp) {
        this.isTemp = isTemp;
    }

    public boolean isTemp() {
        return isTemp;
    }
}
