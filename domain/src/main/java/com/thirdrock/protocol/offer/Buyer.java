package com.thirdrock.protocol.offer;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.domain.IUser;
import com.thirdrock.domain.Location;

/**
 * Buyer in offerline details
 * Created by jubin on 1/6/15.
 */
@JsonType
public class Buyer implements IUser {                // FIXME 抽象为User，User有更多字段，直接转换过去可能会有问题
    @JsonField(fieldName = "id")
    String id;

    @JsonField(fieldName = "nickname")
    String nickname;

    @JsonField(fieldName = "post_index")
    int postIndex;

    @JsonField(fieldName = "place")
    String placeStr;

    @JsonField(fieldName = "verified")
    boolean verified;

    @JsonField(fieldName = "portrait")
    String avatarUrl;

    @JsonField(fieldName = "location")
    Location location;

    public String getId() {
        return id;
    }

    public String getNickname() {
        return nickname;
    }

    public int getPostIndex() {
        return postIndex;
    }

    public String getPlaceStr() {
        return placeStr;
    }

    public boolean isVerified() {
        return verified;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public Location getLocation() {
        return location;
    }

}
