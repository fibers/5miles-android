package com.thirdrock.protocol.offer;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.domain.Item;

/**
 * Created by jubin on 1/6/15.
 */
@JsonType
public class OfferlineMeta {
    @JsonField(fieldName = "buyer")
    Buyer buyer;

    @JsonField(fieldName = "item")
    Item item;

    @JsonField(fieldName = "latest_ts")
    long latestTs;

    @JsonField(fieldName = "user_blocked")
    boolean userBlocekd;

    @JsonField(fieldName = "sold_to_others")
    boolean soldToOthers;

    public Buyer getBuyer() {
        return buyer;
    }

    public Item getItem() {
        return item;
    }

    public long getLatestTs() {
        return latestTs;
    }

    public boolean isUserBlocekd() {
        return userBlocekd;
    }

    public boolean isSoldToOthers() {
        return soldToOthers;
    }
}