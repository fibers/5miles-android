package com.thirdrock.protocol.offer;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.List;

/**
 * Offerline json from server
 * Created by jubin on 1/6/15.
 */
@JsonType
public class OfferLineDetailResp {
    @JsonField(fieldName = "meta")
    OfferlineMeta meta;

    @JsonField(fieldName = "objects")
    List<ChatMessage> chatMsgList;

    public List<ChatMessage> getChatMsgList() {
        return chatMsgList;
    }

    public OfferlineMeta getMeta() {

        return meta;
    }
}
