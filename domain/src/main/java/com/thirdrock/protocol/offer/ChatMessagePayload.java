package com.thirdrock.protocol.offer;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * Payload in ChatMessage
 * Created by jubin on 1/6/15.
 */
@JsonType
public class ChatMessagePayload {

    @JsonField(fieldName = "text")
    String text;

    @JsonField(fieldName = "picture_url")
    String imageUrl;

    @JsonField(fieldName = "place_name")
    String placeName;

    @JsonField(fieldName = "address_name")
    String addressName;

    @JsonField(fieldName = "lat")
    double latitude;

    @JsonField(fieldName = "lon")
    double longitude;

    @JsonField(fieldName = "address_map_thumb")
    String mapThumbUrl;

    public ChatMessagePayload() {
        // empty
    }

    public static ChatMessagePayload getTempImagePayload(String pLocalImageUri) {
        ChatMessagePayload payload = new ChatMessagePayload();
        payload.imageUrl = "file://" + pLocalImageUri;
        return payload;
    }

    public static ChatMessagePayload getTempLocationPayload(double pLat, double pLng
            , String pPlaceName, String pAddressName, String pMapThumbUrl) {
        ChatMessagePayload payload = new ChatMessagePayload();
        payload.latitude = pLat;
        payload.longitude = pLng;
        payload.placeName = pPlaceName;
        payload.addressName = pAddressName;
        payload.mapThumbUrl = pMapThumbUrl;

        return payload;
    }

    public String getText() {
        return text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getPlaceName() {
        return placeName;
    }

    public String getAddressName() {
        return addressName;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getMapThumbUrl() {
        return mapThumbUrl;
    }

    public static ChatMessagePayload getTempImagePayloadKitkat(String pImagePathOrUri) {
        ChatMessagePayload payload = new ChatMessagePayload();
        payload.imageUrl = pImagePathOrUri;

        return payload;
    }
}
