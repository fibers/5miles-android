package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.domain.Subscription;

import java.util.List;

/**
 * Response body for getting notification preferences
 * Created by ywu on 14/11/3.
 */
@JsonType
public class GetSubscriptionsResp {

    @JsonField(fieldName = "data")
    List<Subscription> subscriptions;

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }
}
