package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.domain.User;

import java.util.Collections;
import java.util.List;

/**
 * Created by ywu on 14/11/24.
 */
@JsonType
public class GetLikersResp {

    @JsonField(fieldName = "meta")
    Meta meta;

    @JsonField(fieldName = "objects")
    List<User> users;

    public Meta getMeta() {
        return meta;
    }

    public List<User> getUsers() {
        return users == null ? Collections.<User>emptyList() : Collections.unmodifiableList(users);
    }
}
