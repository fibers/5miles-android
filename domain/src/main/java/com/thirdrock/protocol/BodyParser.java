package com.thirdrock.protocol;

import com.thirdrock.domain.AppConfig;
import com.thirdrock.domain.AppConfig__JsonHelper;
import com.thirdrock.domain.FeaturedCollection;
import com.thirdrock.domain.FeaturedCollection__JsonHelper;
import com.thirdrock.domain.Item;
import com.thirdrock.domain.Item__JsonHelper;
import com.thirdrock.domain.Review;
import com.thirdrock.domain.Review__JsonHelper;
import com.thirdrock.protocol.offer.OfferLineDetailResp;
import com.thirdrock.protocol.offer.OfferLineDetailResp__JsonHelper;

/**
 * Created by ywu on 14-10-12.
 */
public final class BodyParser {

    private BodyParser() {

    }

    @SuppressWarnings("unchecked")
    public static <T> T parse(String body, Class<T> type) throws Exception {
        if (Void.class.equals(type)) {
            return null;
        } else if (String.class.equals(type)) {
            return (T) body;
        } else if (HomeResp.class.equals(type)) {
            return (T) HomeResp__JsonHelper.parseFromJson(body);
        } else if (CloudSignature.class.equals(type)) {
            return (T) CloudSignature__JsonHelper.parseFromJson(body);
        } else if (ListItemResp.class.equals(type)) {
            return (T) ListItemResp__JsonHelper.parseFromJson(body);
        } else if (ShortLinkResp.class.equals(type)) {
            return (T) ShortLinkResp__JsonHelper.parseFromJson(body);
        } else if (Item.class.equals(type)) {
            return (T) Item__JsonHelper.parseFromJson(body);
        } else if (GetLikersResp.class.equals(type)) {
            return (T) GetLikersResp__JsonHelper.parseFromJson(body);
        } else if (CampaignResp.class.equals(type)) {
            return (T) CampaignResp__JsonHelper.parseFromJson(body);
        } else if (FeaturedCollection.class.equals(type)) {
            return (T) FeaturedCollection__JsonHelper.parseFromJson(body);
        } else if (MakeOfferResp.class.equals(type)) {
            return (T) MakeOfferResp__JsonHelper.parseFromJson(body);
        } else if (RenewItemResp.class.equals(type)) {
            return (T) RenewItemResp__JsonHelper.parseFromJson(body);
        } else if (SearchResp.class.equals(type)) {
            return (T) SearchResp__JsonHelper.parseFromJson(body);
        } else if (CategoriesResp.class.equals(type)) {
            return (T) CategoriesResp__JsonHelper.parseFromJson(body);
        } else if (ReviewListResp.class.equals(type)) {
            return (T) ReviewListResp__JsonHelper.parseFromJson(body);
        } else if (SellersNearbyResp.class.equals(type)) {
            return (T) SellersNearbyResp__JsonHelper.parseFromJson(body);
        } else if (AppConfig.class.equals(type)) {
            return (T) AppConfig__JsonHelper.parseFromJson(body);
        } else if (AddressResponse.class.equals(type)) {
            return (T) AddressResponse__JsonHelper.parseFromJson(body);
        } else if (Address.class.equals(type)) {
            return (T) Address__JsonHelper.parseFromJson(body);
        } else if (AddressComponent.class.equals(type)) {
            return (T) AddressComponent__JsonHelper.parseFromJson(body);
        } else if (OfferLineDetailResp.class.equals(type)) {
            return (T) OfferLineDetailResp__JsonHelper.parseFromJson(body);
        } else if (MyItemsResp.class.equals(type)){
            return (T) MyItemsResp__JsonHelper.parseFromJson(body);
        } else if (Review.class.equals(type)){
            return (T) Review__JsonHelper.parseFromJson(body);
        }

        throw new IllegalArgumentException("unknown body type: " + type);
    }

}
