package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.Map;

/**
 * Created by ywu on 14-10-7.
 */
@JsonType
public class Push {
    public static enum Type {
        system, offer, comment;

        public static Type parse(String v) {
            Type type = system;
            if (v == null || v.length() == 0) {
                return type;
            }

            switch (v.charAt(0)) {
                case 'o':
                    type = offer;
                    break;
                case 'c':
                    type = comment;
                    break;
                default:
                    type = system;
                    break;
            }

            return type;
        }
    }

    public static final int PUSH_SOUND = 1;
    public static final int PUSH_VIBRATE = 2;
    public static final String EXTRA_KEY_CAMPAIGN_ID = "cid";

    @JsonField(fieldName = "t", alternateFieldNames = "type")
    String type;

    @JsonField(fieldName = "a", alternateFieldNames = "action")
    String action;

    @JsonField(fieldName = "txt", alternateFieldNames = "text")
    String text;

    @JsonField(fieldName = "title")
    String title;

    @JsonField(fieldName = "id", alternateFieldNames = "item_id")
    String item_id;

    @JsonField(fieldName = "olid", alternateFieldNames = "offerline_id")
    int offerLineId;

    @JsonField(fieldName = "mid", alternateFieldNames = "msg_id")
    String msgId;

    @JsonField(fieldName = "img")
    String image;

    @JsonField(fieldName = "flag")
    int flags = PUSH_SOUND | PUSH_VIBRATE;  // flags for sound/vibrate

    public String getRfTag() {
        return rfTag;
    }

    @JsonField(fieldName = "r", alternateFieldNames = "rf_tag")
    String rfTag;

    private Type enumType;
    private SystemActionData actionWrapper = new SystemActionData();
    private boolean parsed;

    Push() {

    }

    private void doParsing() {
        if (parsed) {
            return;
        }

        try {
            if (type != null && type.trim().length() > 0) {
                enumType = Type.parse(type);
            }

            // parse action & extract data
            actionWrapper = SystemActionData.valueOf(action);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        parsed = true;
    }

    public Type getType() {
        doParsing();
        return enumType;
    }

    public SystemAction getActionType() {
        doParsing();
        return actionWrapper.action;
    }

    public String getActionData() {
        doParsing();
        return actionWrapper.actionData;
    }

    public String getText() {
        doParsing();
        return text;
    }

    public String getTitle() {
        return title;
    }

    public int getOfferLineId() {
        doParsing();
        return offerLineId;
    }

    public String getItemId() {
        doParsing();
        return item_id;
    }

    public String getMsgId() {
        doParsing();
        return msgId;
    }

    public String getImage() {
        return image;
    }

    public int getFlags() {
        return flags;
    }

    public boolean shouldSound() {
        return (flags & Push.PUSH_SOUND) == Push.PUSH_SOUND;
    }

    public boolean shouldVibrate() {
        return (flags & Push.PUSH_VIBRATE) == Push.PUSH_VIBRATE;
    }

    public Map<String, String> getExtraActionData() {
        return actionWrapper.getExtraActionData();
    }
}
