package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * sample json: {@code {"reason_content": "Prohibited item","id": 5}} <br/>
 * Created by jubin on 6/5/15.
 */
@JsonType
public class ReportReason {
    @JsonField(fieldName = "reason_content")
    String reasonContent;

    @JsonField(fieldName = "id")
    int id;

    public String getReasonContent() {
        return reasonContent;
    }

    public int getId() {
        return id;
    }
}
