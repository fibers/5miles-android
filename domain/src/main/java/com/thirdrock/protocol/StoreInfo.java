package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * Created by jubin on 13/1/15.
 */
@JsonType
public class StoreInfo {
    StoreInfo(){ /** leave empty*/ }

    @JsonField(fieldName = "store_url")
    String storeUrl;

    @JsonField(fieldName = "portrait")
    String portrait;

    @JsonField(fieldName = "nickname")
    String nickname;

    @JsonField(fieldName = "id")
    String fuzzId;

    @JsonField(fieldName = "is_robot")
    int isRobot;

    public String getStoreUrl() {
        return storeUrl;
    }

    public String getPortrait() {
        return portrait;
    }

    public String getNickname() {
        return nickname;
    }

    public String getFuzzId() {
        return fuzzId;
    }

    public int getIsRobot() {
        return isRobot;
    }
}
