package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * Created by ywu on 14-9-27.
 */
@JsonType
public class Meta implements Serializable {
    @JsonField(fieldName = "next")
    String next;

    @JsonField(fieldName = "previous")
    String previous;

    @JsonField(fieldName = "total_count")
    int totalCount;

    @JsonField(fieldName = "limit")
    int limit;

    @JsonField(fieldName = "offset")
    int offset;

    public Meta() {

    }

    public String getNext() {
        return next;
    }

    public String getPrevious() {
        return previous;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public int getLimit() {
        return limit;
    }

    public int getOffset() {
        return offset;
    }

    public boolean hasNext() {
        return StringUtils.isNotEmpty(next);
    }
}
