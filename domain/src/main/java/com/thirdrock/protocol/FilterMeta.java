package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.domain.CategoryInfo;
import com.thirdrock.domain.SearchFilter;

import java.util.Collections;
import java.util.List;

/**
 * Metadata of search filters
 * Created by ywu on 15/3/11.
 */
@JsonType
public class FilterMeta {

    /**
     * Applied search filters
     */
    @JsonField(fieldName = "refind")
    SearchFilter filter;

    /**
     * Category-based stats of the search result
     */
    @JsonField(fieldName = "refind_category")
    List<CategoryInfo> categoryStats;

    public SearchFilter getFilter() {
        return filter;
    }

    /**
     * Category-based stats of the search result
     */
    public List<CategoryInfo> getCategoryStats() {
        return categoryStats == null ? Collections.<CategoryInfo>emptyList() :
                Collections.unmodifiableList(categoryStats);
    }
}
