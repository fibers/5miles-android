package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.domain.Item;

import java.util.Collections;
import java.util.List;

/**
 * Created by fibers on 15/7/6.
 */
@JsonType
public class MyItemsResp {

    @JsonField(fieldName = "meta")
    Meta meta;

    @JsonField(fieldName = "objects")
    List<Item> listMyiItems;

    public List<Item> getListMyiItems() {
        return listMyiItems == null ? Collections.<Item>emptyList() : Collections.unmodifiableList(listMyiItems);
    }

    public Meta getMeta() {
        return meta;
    }
}
