package com.thirdrock.protocol;


import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.ArrayList;

@JsonType
public class AddressComponent {
    @JsonField(fieldName = "short_name")
    public String short_name;

    @JsonField(fieldName = "long_name")
    public String long_name;


    @JsonField(fieldName = "types")
    public ArrayList<String> types;

}