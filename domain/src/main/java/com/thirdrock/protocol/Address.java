package com.thirdrock.protocol;


import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.ArrayList;

@JsonType
public class Address {

    @JsonField(fieldName = "formatted_address")
    public String formatted_address;

    @JsonField(fieldName = "address_components")
    public ArrayList<AddressComponent> address_components = new ArrayList<AddressComponent>();

    @JsonField(fieldName = "geometry")
    public Geometry geometry = new Geometry();
}