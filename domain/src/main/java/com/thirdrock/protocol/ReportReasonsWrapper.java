package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.List;

/**
 * sample json: {@code {"objects": [{"reason_content": "Prohibited item", "id": 5},
 * {"reason_content": "Duplicate listing", "id": 6}], "type": 0}}   <br/>
 * Created by jubin on 6/5/15.
 */
@JsonType
public class ReportReasonsWrapper {
    @JsonField(fieldName = "objects")
    List<ReportReason> reportReasonList;

    @JsonField(fieldName = "type")
    int type;

    public int getType() {
        return type;
    }

    public List<ReportReason> getReportReasonList() {
        return reportReasonList;
    }
}
