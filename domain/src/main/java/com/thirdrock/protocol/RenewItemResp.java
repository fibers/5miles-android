package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * Created by jubin on 5/2/15.
 */
@JsonType
public class RenewItemResp {
    @JsonField(fieldName = "renew_ttl")
    long renewTtl;

    public long getRenewTtl() {
        return renewTtl;
    }
}
