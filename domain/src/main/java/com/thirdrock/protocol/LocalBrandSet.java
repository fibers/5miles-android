package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.Collections;
import java.util.List;

/**
 * Brand data set from local raw resouce
 * Created by ywu on 14/11/11.
 */
@JsonType
public class LocalBrandSet {

    /**
     * Version number of the data set.
     */
    @JsonField(fieldName = "version")
    int version;

    @JsonField(fieldName = "data")
    List<String> brandList;

    public int getVersion() {
        return version;
    }

    public List<String> getBrandList() {
        return brandList != null ? Collections.unmodifiableList(brandList) :
                Collections.<String>emptyList();
    }
}
