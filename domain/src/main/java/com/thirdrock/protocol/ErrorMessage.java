package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * Created by jubin on 16/1/15.
 */
@JsonType
public class ErrorMessage {

    @JsonField(fieldName = "err_code")
    int errorCode;

    @JsonField(fieldName = "err_msg", alternateFieldNames = {"message"})
    String message;

    public String getMessage() {
        return message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public String toString() {
        return message;
    }
}
