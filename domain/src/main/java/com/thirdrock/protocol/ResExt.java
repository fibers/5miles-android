package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * Extra fields of the api response
 * Created by ywu on 15/4/20.
 */
@JsonType
public class ResExt {
    @JsonField(fieldName = "review_score")
    double reputationScore;

    @JsonField(fieldName = "review_num")
    int reviewCount;

    public double getReputationScore() {
        return reputationScore;
    }

    public int getReviewCount() {
        return reviewCount;
    }
}
