package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * Created by changqi on 15/6/9.
 */

@JsonType
public class Geometry {

    @JsonField(fieldName = "location")
    public Location location = new Location();
}
