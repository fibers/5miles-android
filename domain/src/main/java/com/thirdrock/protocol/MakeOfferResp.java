package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * Response body for making offer;
 * Created by ywu on 15/2/3.
 */
@JsonType
public class MakeOfferResp {

    @JsonField(fieldName = "offerline_id")
    int offerLineId;

    @JsonField(fieldName = "offer_id")
    String offerId;

    public int getOfferLineId() {
        return offerLineId;
    }

    public String getMessageId() {
        return offerId;
    }
}
