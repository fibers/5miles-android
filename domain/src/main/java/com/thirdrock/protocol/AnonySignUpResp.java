package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * 已不再使用匿名登陆
 * Created by jubin on 20/3/15.
 */
@Deprecated
@JsonType
public class AnonySignUpResp {
    @JsonField(fieldName = "id")
    String id;

    @JsonField(fieldName = "city")
    String city;

    @JsonField(fieldName = "verified")
    boolean isVerified;

    @JsonField(fieldName = "is_anonymous")
    int isAnonymousIndicator;

    @JsonField(fieldName = "facebook_verified")
    boolean isFacebookVerified;

    @JsonField(fieldName = "country")
    String country;

    @JsonField(fieldName = "created_at")
    long createdAt;

    @JsonField(fieldName = "lon")
    double lon;

    @JsonField(fieldName = "lat")
    double lat;

    @JsonField(fieldName = "token")
    String token;

    @JsonField(fieldName = "state")
    int state;

    @JsonField(fieldName = "last_login_at")
    long lastLoginAt;

    @JsonField(fieldName = "appsflyer_user_id")
    String appsflyerUserId;

    @JsonField(fieldName = "portrait")

    String portraitUrl;

    @JsonField(fieldName = "region")
    String region;

    @JsonField(fieldName = "nickname")
    String nickname;

    @JsonField(fieldName = "email")
    String email;

    public String getUserId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public boolean isAnonymous() {
        return isAnonymousIndicator > 0;
    }

    public boolean isFacebookVerified() {
        return isFacebookVerified;
    }

    public String getCountry() {
        return country;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }

    public String getToken() {
        return token;
    }

    public int getState() {
        return state;
    }

    public long getLastLoginAt() {
        return lastLoginAt;
    }

    public String getAppsflyerUserId() {
        return appsflyerUserId;
    }

    public String getPortraitUrl() {
        return portraitUrl;
    }

    public String getRegion() {
        return region;
    }

    public String getNickname() {
        return nickname;
    }

    public String getEmail() {
        return email;
    }
}
