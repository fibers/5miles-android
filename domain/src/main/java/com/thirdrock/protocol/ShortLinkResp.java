package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * Created by ywu on 14-10-13.
 */
@JsonType
public class ShortLinkResp {
    @JsonField(fieldName = "short_url")
    String shortUrl;

    public String getShortUrl() {
        return shortUrl;
    }
}
