package com.thirdrock.protocol;

import com.thirdrock.domain.EnumDeliveryType;
import com.thirdrock.domain.ImageInfo;
import com.thirdrock.domain.ImageInfo__JsonHelper;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by ywu on 14-10-11.
 */
public class ListItemReq implements Serializable {
    public String itemId;  // item id when editing
    public String title;
    public String description;  // desc
    public int categoryId;  // category
    public String brand;  // brand name
    public String mediaLink;
    public double price;
    public Double originPrice;
    public String currency;
    public float latitude;  // lat
    public float longitude;  // lon
    public EnumDeliveryType deliveryType;
    public boolean autoShare;

    private String images = "";
    public String city;
    public String region;
    public String country;

    public void setImageList(List<ImageInfo> imageList) {
        if (imageList == null || imageList.isEmpty()) {
            return;
        }

        StringBuilder imgsBuf = new StringBuilder();
        imgsBuf.append("[");

        for (int i = 0; i < imageList.size(); i++) {
            ImageInfo img = imageList.get(i);
            try {
                imgsBuf.append(ImageInfo__JsonHelper.serializeToJson(img));
                if (i < imageList.size() - 1) {
                    imgsBuf.append(",");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        images = imgsBuf.append("]").toString();
    }

    public String getImages() {
        return images;
    }
}
