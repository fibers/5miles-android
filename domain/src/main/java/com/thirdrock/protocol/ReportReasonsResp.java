package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.List;

/**
 * sample json: {@code {"objects": [{"objects": [{"reason_content": "Prohibited item", "id": 5},
 * {"reason_content": "Duplicate listing", "id": 6}, {"reason_content": "Counterfeit", "id": 7},
 * {"reason_content": "Inappropriate item description", "id": 8},
 * {"reason_content": "Includes links/phone numbers/emails", "id": 9}], "type": 0}]}}   <br/>
 * Created by jubin on 6/5/15.
 */
@JsonType
public class ReportReasonsResp {
    @JsonField(fieldName = "objects")
    List<ReportReasonsWrapper> reportReasonsWrapperList;

    public List<ReportReasonsWrapper> getReportReasonsWrapperList() {
        return reportReasonsWrapperList;
    }
}
