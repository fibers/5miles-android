package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.domain.SellerInfo;

import java.util.List;

/**
 * Created by jubin on 14/4/15.
 */
@JsonType
public class SellersNearbyResp {
    @JsonField(fieldName = "objects")
    List<SellerInfo> sellerInfoList;

    public List<SellerInfo> getSellerInfoList() {
        return sellerInfoList;
    }
}
