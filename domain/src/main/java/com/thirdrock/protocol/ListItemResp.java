package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * Created by ywu on 14-10-11.
 */
@JsonType
public class ListItemResp {
    @JsonField(fieldName = "new_id")
    String id;

    public String getId() {
        return id;
    }
}
