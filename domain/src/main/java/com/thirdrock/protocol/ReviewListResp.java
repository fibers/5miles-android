package com.thirdrock.protocol;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.domain.Review;

import java.util.Collections;
import java.util.List;

/**
 * Created by ywu on 15/4/20.
 */
@JsonType
public class ReviewListResp {
    @JsonField(fieldName = "meta")
    Meta meta;

    @JsonField(fieldName = "res_ext")
    ResExt extras;

    @JsonField(fieldName = "objects")
    List<Review> reviews;

    public Meta getMeta() {
        return meta;
    }

    public double getReputationScore() {
        return extras == null ? 0 : extras.getReputationScore();
    }

    public int getReviewCount() {
        return extras == null ? 0 : extras.getReviewCount();
    }

    public List<Review> getReviews() {
        return reviews == null ? Collections.<Review>emptyList() : Collections.unmodifiableList(reviews);
    }
}
