package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.io.Serializable;

import static com.thirdrock.domain.EnumPushMethod.EMAIL;

/**
 * State of a subscription topic
 * Created by ywu on 14/11/3.
 */
@JsonType
public class Subscription implements Serializable, Cloneable {

    @JsonField(fieldName = "type")
    int topicId;

    @JsonField(fieldName = "method")
    int pushMethodId;

    @JsonField(fieldName = "state")
    boolean enabled = true;

    private EnumTopic topic;
    private EnumPushMethod method;

    private boolean parsed;

    public Subscription() {

    }

    public Subscription(EnumTopic topic, EnumPushMethod method) {
        this.topic = topic;
        this.method = method;

        this.topicId = topic.ordinal();
        this.pushMethodId = method.ordinal();
        this.parsed = true;
    }

    private void doParsing() {
        if (parsed) {
            return;
        }

        parsed = true;
        topic = EnumTopic.valueOf(topicId);
        method = EnumPushMethod.valueOf(pushMethodId);
    }

    public String getKey() {
        doParsing();
        if (topic == null || method == null) {
            return null;
        }

        return getSubsPrefKey(topic, method);
    }

    public static String getSubsPrefKey(EnumTopic topic, EnumPushMethod method) {
        return "user_subs_" + topic + "_" + method;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public EnumTopic getTopic() {
        doParsing();
        return topic;
    }

    public EnumPushMethod getMethod() {
        doParsing();
        return method;
    }

    public int getTopicId() {
        return topicId;
    }

    public int getPushMethodId() {
        return pushMethodId;
    }

    @Override
    public Subscription clone() {
        try {
            return (Subscription) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
