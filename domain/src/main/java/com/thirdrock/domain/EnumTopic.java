package com.thirdrock.domain;

/**
 * Topics users can subscribe.
 * Created by ywu on 14/11/3.
 */
public enum EnumTopic {

    UNKNOWN, NEW_FOLLOWERS, RECOMMENDATIONS, MARKETING_EVENTS;

    public static EnumTopic valueOf(int v) {
        EnumTopic[] vs = EnumTopic.values();
        return (v >= 0 && v < vs.length) ? vs[v] : UNKNOWN;
    }
}
