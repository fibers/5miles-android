package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.io.Serializable;

/**
 * Created by ywu on 14-9-27.
 */
@JsonType
public class User implements Serializable, IUser{
    @JsonField(fieldName = "id", alternateFieldNames = "fuzzy_user_id")
    String id;

    @JsonField(fieldName = "portrait")
    String avatar;

    @JsonField(fieldName = "nickname")
    String nickname;

    @JsonField(fieldName = "verified")
    boolean verified;

    @JsonField(fieldName = "email_verified")
    boolean emailVerified;

    @JsonField(fieldName = "mobile_phone_verified")
    boolean phoneVerified;

    @JsonField(fieldName = "facebook_verified")
    boolean facebookVerified;

    @JsonField(fieldName = "review_num")
    int reviewCount;

    @JsonField(fieldName = "review_score")
    double reputationScore;

    public User() {

    }

    public User(String pId, String pNickname, boolean pIsVerified, String pAvatarUrl) {
        this.id = pId;
        this.nickname = pNickname;
        this.verified = pIsVerified;
        this.avatar = pAvatarUrl;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setVerified(boolean isVerified) {
        this.verified = isVerified;
    }

    public void setEmailVerified(boolean isEmailVerified) {
        this.emailVerified = isEmailVerified;
    }

    public void setPhoneVerified(boolean isPhoneVerified) {
        this.phoneVerified = isPhoneVerified;
    }

    public void setFacebookVerified(boolean isFacebookVerified) {
        this.facebookVerified = isFacebookVerified;
    }

    public int getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    public double getReputationScore() {
        return reputationScore;
    }

    public void setReputationScore(double reputationScore) {
        this.reputationScore = reputationScore;
    }

    public String getId() {
        return id;
    }

    public String getAvatarUrl() {
        return avatar;
    }

    public String getNickname() {
        return nickname;
    }

    public boolean isVerified() {
        return verified;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public boolean isPhoneVerified() {
        return phoneVerified;
    }

    public boolean isFacebookVerified() {
        return facebookVerified;
    }
}
