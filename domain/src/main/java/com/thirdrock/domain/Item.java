package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.Collections;
import java.util.List;

/**
 * Created by ywu on 14-9-27.
 */
@JsonType
public class Item extends ItemThumb {

    @JsonField(fieldName = "desc")
    String description;

    @JsonField(fieldName = "cat_id")
    int categoryId;

    @JsonField(fieldName = "brand_name")
    String brand;

    @JsonField(fieldName = "brand_id")
    int brandId;

    @JsonField(fieldName = "created_at")
    int created;

    @JsonField(fieldName = "owner")
    User owner;

    @JsonField(fieldName = "shipping_method")
    int deliveryTypeId;

    @JsonField(fieldName = "offerLines")
    List<OfferLine> offerLines;

    @JsonField(fieldName = "liked")
    boolean liked;

    @JsonField(fieldName = "renew_ttl")
    long renewTtl;

    @JsonField(fieldName = "deletable")
    boolean deletable;

    @JsonField(fieldName = "review_score")
    double reviewScore = -1.0;

    @JsonField(fieldName = "review_num")
    int reviewNum;

    private EnumDeliveryType deliveryType;

    public Item() {

    }

    public String getDescription() {
        return description;
    }

    public int getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(int id){
        categoryId = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getBrandId() {
        return brandId;
    }

    public int getCreated() {
        return created;
    }

    public User getOwner() {
        return owner;
    }

    public EnumDeliveryType getDeliveryType() {
        if (deliveryType == null) {
            if (deliveryTypeId > 0) {
                deliveryType = EnumDeliveryType.valueOf(deliveryTypeId);
            } else {
                deliveryType = EnumDeliveryType.BOTH;
            }
        }
        return deliveryType;
    }

    public List<OfferLine> getOfferLines() {
        return offerLines == null ? Collections.<OfferLine>emptyList() :
                Collections.unmodifiableList(offerLines);
    }

    public boolean isLiked() {
        return liked;
    }

    public double getReviewScore() {
        return reviewScore;
    }

    public int getReviewNum() {
        return reviewNum;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public long getRenewTtl() {
        return renewTtl;
    }

    public void setRenewTtl(long renewTtl){
        this.renewTtl = renewTtl;
    }


    public boolean isDeletable(){
        return deletable;
    }

}
