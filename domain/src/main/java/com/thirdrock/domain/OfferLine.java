package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.io.Serializable;

/**
 * A line of offer/chat thread.
 * Created by ywu on 14/11/20.
 */
@JsonType
public class OfferLine implements Serializable {

    @JsonField(fieldName = "id")
    int id;

    @JsonField(fieldName = "description")
    String description;

    @JsonField(fieldName = "buyer")
    User buyer;

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public User getBuyer() {
        return buyer;
    }
}
