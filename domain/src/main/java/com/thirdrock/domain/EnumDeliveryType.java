package com.thirdrock.domain;

/**
 * Delivery types available.
 * Created by ywu on 14/11/18.
 */
public enum EnumDeliveryType {
    UNKNOWN, EXCHANGE_IN_PERSON, POST, BOTH;

    public static EnumDeliveryType valueOf(int v) {
        EnumDeliveryType[] vs = EnumDeliveryType.values();
        return (v >= 0 && v < vs.length) ? vs[v] : UNKNOWN;
    }
}
