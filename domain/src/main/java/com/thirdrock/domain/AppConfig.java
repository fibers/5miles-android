package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.List;

/**
 * Global configuration
 * Created by ywu on 15/4/29.
 */
@JsonType
public class AppConfig {
    public static final String DEFAULT_API_V = "v2";

    @JsonType
    static class Switches {

        @JsonField(fieldName = "review")
        boolean review = true;

    }

    @JsonField(fieldName = "api_version")
    String apiVersion = DEFAULT_API_V;

    @JsonField(fieldName = "switchers")
    Switches switches = new Switches();

    @JsonField(fieldName = "categories")
    List<CategoryInfo> categoryInfoList;

    public String getApiVersion() {
        return apiVersion;
    }

    public boolean isReviewEnabled() {
        return switches.review;
    }

    public List<CategoryInfo> getCategoryInfoList() {
        return categoryInfoList;
    }
}
