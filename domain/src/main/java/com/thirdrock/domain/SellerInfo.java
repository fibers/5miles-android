package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.List;

/**
 * isToFollow是新加的字段，记录哪些seller要follow，不与json中任何字段对应
 * Created by jubin on 14/4/15.
 */
@JsonType
public class SellerInfo implements ILocation{
    @JsonField(fieldName = "verified")
    boolean verified;

    public boolean isVerified() {
        return verified;
    }

    public boolean isFollowing() {
        return following;
    }

    public String getPlace() {
        return place;
    }

    public Location getLocation() {
        return location;
    }

    public int getItemNum() {
        return itemNum;
    }

    public void setFollowing(boolean following) {
        this.following = following;
    }

    public String getPortrait() {
        return portrait;
    }

    public String getNickName() {
        return nickName;
    }

    public String getId() {
        return id;
    }

    public List<ImageInfo> getItemImages() {
        return itemImages;
    }

    @JsonField(fieldName = "following")
    boolean following;

    @JsonField(fieldName = "place")
    String place;

    @JsonField(fieldName = "location")
    Location location;

    @JsonField(fieldName = "item_num")
    int itemNum;

    @JsonField(fieldName = "portrait")
    String portrait;

    @JsonField(fieldName = "nickname")
    String nickName;

    @JsonField(fieldName = "id")
    String id;

    @JsonField(fieldName = "item_images")
    List<ImageInfo> itemImages;

    private boolean isToFollow;

    public boolean isToFollow() {
        return isToFollow;
    }

    public void setToFollow(boolean isToFollow) {
        this.isToFollow = isToFollow;
    }

    @Override
    public double getLatitude() {
        if (location != null) {
            return location.getLatitude();
        } else {
            return 0;
        }
    }

    @Override
    public double getLongitude() {
        if (location != null) {
            return location.getLongitude();
        } else {
            return 0;
        }
    }
}
