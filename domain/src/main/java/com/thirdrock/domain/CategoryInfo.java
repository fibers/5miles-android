package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Created by jubin on 5/3/15.
 */
@JsonType
public class CategoryInfo implements Serializable {
    public static final int ID_ALL_CATEGORY = -1;

    @JsonField(fieldName = "id",
            valueExtractFormatter = "${parser_object}.getValueAsInt(com.thirdrock.domain.CategoryInfo.ID_ALL_CATEGORY)")
    int id;

    @JsonField(fieldName = "title")
    String title;

    @JsonField(fieldName = "icon")
    String iconUrl;

    @JsonField(fieldName = "hover_icon")
    String hoverIconUrl;

    /**
     * Number of items belong to the category, available in search result only
     */
    @JsonField(fieldName = "cnt")
    int itemCount;

    @JsonField(fieldName = "child")
    List<CategoryInfo> children;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    /**
     * Number of items belong to the category, available in search result only
     */
    public int getItemCount() {
        return itemCount;
    }

    public List<CategoryInfo> getChildren() {
        return children == null ? Collections.<CategoryInfo>emptyList() : children;
    }

    public void setChildren(List<CategoryInfo> children) {
        this.children = children;
    }

    public String getHoverIconUrl() {
        return hoverIconUrl;
    }

    public void setHoverIconUrl(String hoverIconUrl) {
        this.hoverIconUrl = hoverIconUrl;
    }

    @Override
    public boolean equals(Object otherObj) {
        if (otherObj == this){
            return true;
        }

        if (otherObj instanceof CategoryInfo) {
            CategoryInfo that = (CategoryInfo) otherObj;

            return this.id == that.id;
        }

        return false;
    }

    @Override
    public int hashCode(){
        return id;
    }
}
