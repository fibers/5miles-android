package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.io.Serializable;
import java.util.List;
import java.util.Observable;

/**
 * Created by ywu on 14-9-25.
 */
@JsonType
public class ItemThumb extends Observable implements Serializable {
    public static final int IS_NEW = 1;
    public static final int NOT_NEW = 0;

    @JsonField(fieldName = "id")
    String id;

    @JsonField(fieldName = "title")
    String title;

    @JsonField(fieldName = "local_price")
    String localPrice;

    @JsonField(fieldName = "price")
    Double price;

    @JsonField(fieldName = "original_price")
    Double originPrice;

    @JsonField(fieldName = "currency")
    String currency;

    @JsonField(fieldName = "images")
    List<ImageInfo> images;

    @JsonField(fieldName = "imgReadyToLoad")
    List<String> imgReadyToLoad;


    @JsonField(fieldName = "location")
    Location location;

    @JsonField(fieldName = "city")
    String city;

    @JsonField(fieldName = "country")
    String country;

    @JsonField(fieldName = "region")
    String province;

    @JsonField(fieldName = "mediaLink")
    String mediaLink;

    @JsonField(fieldName = "state")
    int stateId;

    @JsonField(fieldName = "updated_at")
    int lastUpdated;  // renew time

    @JsonField(fieldName = "modified_at")
    int lastModified;  // edit time

    @JsonField(fieldName = "is_new")
    int newTag;

    @JsonField(fieldName = "rf_tag")
    String rfTag;

    private EnumItemState state;
    private ImageInfo thumbImage;  // 最近一次可用的缩略图link，用于快速预览

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getLocalPrice() {
        return localPrice;
    }

    public Double getPrice() {
        return price;
    }

    public Double getOriginPrice() {
        return originPrice;
    }

    public String getCurrencyCode() {
        return currency;
    }

    public List<ImageInfo> getImages() {
        return images;
    }

    public Location getLocation() {
        return location;
    }

    public String getCity() {
        return city;
    }

    public ImageInfo getDefaultImage() {
        if (images == null || images.isEmpty()) {
            return null;
        }

        return images.get(0);
    }

    public String getMediaLink() {
        return mediaLink;
    }
    public void setMediaLink(String link){
        mediaLink =  link;
    }

    public ImageInfo getThumbImage() {
        return thumbImage;
    }

    public String getRfTag() {
        return rfTag;
    }

    public void setThumbImage(ImageInfo thumbImage) {
        this.thumbImage = thumbImage;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLocalPrice(String localPrice) {
        this.localPrice = localPrice;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setOriginPrice(Double originPrice) {
        this.originPrice = originPrice;
    }

    public void setImages(List<ImageInfo> images) {
        this.images = images;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public void setIsNew(int newTag){
        if (IS_NEW == newTag){
            this.newTag = IS_NEW;
        } else {
            this.newTag = NOT_NEW;
        }
    }

    public void setRfTag(String rfTag) {
        this.rfTag = rfTag;
    }

    public boolean isNew(){
        return (IS_NEW == this.newTag);
    }

    public int getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(int lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public int getLastModified() {
        return lastModified;
    }

    public void setLastModified(int lastModified) {
        this.lastModified = lastModified;
    }

    public List<String> getImgReadyToLoad() {
        return imgReadyToLoad;
    }

    public void setImgReadyToLoad(List<String> imgReadyToLoad) {
        this.imgReadyToLoad = imgReadyToLoad;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj instanceof ItemThumb) {
            ItemThumb that = (ItemThumb) obj;
            return this.id != null && this.id.equals(that.id);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : super.hashCode();
    }

    public boolean isDirty(ItemThumb that) {
        if (!equals(that)) {
            throw new IllegalArgumentException("should provide a copy of the same Item!");
        }

        // TODO 确认状态变更会更新lastModified
        return lastUpdated != that.lastUpdated || lastModified != that.lastModified;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public EnumItemState getState() {
        if (state == null) {
            state = EnumItemState.valueOf(stateId);
        }

        return state;
    }

    public boolean isSold() {
        EnumItemState state = getState();
        return state != null && state.isSold();
    }
}
