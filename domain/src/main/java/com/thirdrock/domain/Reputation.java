package com.thirdrock.domain;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * User reputation, contains a summary and the detailed list of reviews
 * Created by ywu on 15/4/20.
 */
public class Reputation {
    double reputationScore;

    int reviewCount;

    List<Review> reviews = new LinkedList<>();

    public void setReputationScore(double reputationScore) {
        this.reputationScore = reputationScore;
    }

    public double getReputationScore() {
        return reputationScore;
    }

    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    public int getReviewCount() {
        return reviewCount;
    }

    public List<Review> getReviews() {
        return reviews == null ? Collections.<Review>emptyList() : Collections.unmodifiableList(reviews);
    }

    public void addReview(Review review) {
        if (review != null) {
            reviews.add(review);
        }
    }

    public void addReviews(List<Review> reviews) {
        if (reviews != null) {
            this.reviews.addAll(reviews);
        }
    }
}
