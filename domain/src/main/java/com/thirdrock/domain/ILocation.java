package com.thirdrock.domain;

/**
 * Created by jubin on 17/4/15.
 */
public interface ILocation {
    double getLatitude();
    double getLongitude();
}
