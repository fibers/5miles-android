package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * Created by ywu on 14-9-25.
 */
@JsonType
public class ImageInfo implements Serializable {
    @JsonField(fieldName = "imageLink", alternateFieldNames = "url")
    String url;

    @JsonField(fieldName = "width")
    int width;

    @JsonField(fieldName = "height")
    int height;

    String fitUrl;  // cropped url which fits the screen
    int fitWidth, fitHeight;  // cropped image size

    public ImageInfo() {

    }

    public ImageInfo(String url, int width, int height) {
        this.url = url;
        this.width = width;
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double getHeightRatio() {
        return width > 0 ? ((double) height) / width : 0;
    }

    public String getFitUrl() {
        return fitUrl;
    }

    public void setFitUrl(String fitUrl) {
        this.fitUrl = fitUrl;
    }

    public boolean hasFitUrl() {
        return StringUtils.isNotEmpty(fitUrl);
    }

    public int getFitWidth() {
        return fitWidth;
    }

    public void setFitWidth(int fitWidth) {
        this.fitWidth = fitWidth;
    }

    public int getFitHeight() {
        return fitHeight;
    }

    public void setFitHeight(int fitHeight) {
        this.fitHeight = fitHeight;
    }

    @Override
    public boolean equals(Object obj){
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof ImageInfo)) {
            return false;
        }

        ImageInfo that = (ImageInfo) obj;
        return this.url.equals(that.getUrl())
                && this.width == that.getWidth()
                && this.height == that.getHeight();
    }
}
