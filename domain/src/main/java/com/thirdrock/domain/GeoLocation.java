package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by changqi on 15/5/14.
 */

@JsonType
public class GeoLocation implements Serializable {
    @JsonField(fieldName = "lat")
    float latitude;

    @JsonField(fieldName = "lon")
    float longitude;

    @JsonField(fieldName = "country")
    String country;

    @JsonField(fieldName = "region")
    String region;

    @JsonField(fieldName = "city")
    String city;

    public GeoLocation(){

    }
    public GeoLocation(float latitude, float longitude, String country, String region, String city){
        this.latitude = latitude;
        this.longitude = longitude;
        this.country = country;
        this.region = region;
        this.city = city;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setParams(Map<String,String> params){
        params.put("latitude",""+latitude);
        params.put("longitude",""+longitude);
        params.put("city",city);
        params.put("region", region);
        params.put("country",country);
    }
}
