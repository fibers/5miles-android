package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.io.Serializable;

/**
 * lat & lon in fivemiles
 * Created by ywu on 14-9-25.
 */
@JsonType
public class Location implements Serializable {
    @JsonField(fieldName = "lat")
    float latitude;

    @JsonField(fieldName = "lon")
    float longitude;

    Location() {

    }

    public Location(float latitude, float longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }
}
