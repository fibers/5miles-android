package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.domain.ItemThumb;

import java.util.Collections;
import java.util.List;

/**
 * Featured item collection response.
 * Created by ywu on 14/12/29.
 */
@JsonType
public class FeaturedCollection {

    @JsonField(fieldName = "title")
    String title;

    @JsonField(fieldName = "banner_url")
    String bannerUrl;

    @JsonField(fieldName = "banner_width")
    int bannerWidth;

    @JsonField(fieldName = "banner_height")
    int bannerHeight;

    @JsonField(fieldName = "items")
    List<ItemThumb> items;

    public String getTitle() {
        return title;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public int getBannerWidth() {
        return bannerWidth;
    }

    public int getBannerHeight() {
        return bannerHeight;
    }

    public List<ItemThumb> getItems() {
        return items == null ? Collections.<ItemThumb>emptyList() :
                Collections.unmodifiableList(items);
    }

    @Deprecated
    public void setItems(List<ItemThumb> items) {
        this.items = items;
    }
}
