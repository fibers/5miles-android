package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * Criteria set for searching items
 * Created by ywu on 15/3/11.
 */
@JsonType
public class SearchFilter {

    @JsonField(fieldName = "distance")
    int distanceRange;

    @JsonField(fieldName = "cat_id")
    int category;

    @JsonField(fieldName = "root_cat_id")
    int rootCategory;

    @JsonField(fieldName = "orderby")
    int orderBy;

    @JsonField(fieldName = "price_min")
    Integer minPrice;

    @JsonField(fieldName = "price_max")
    Integer maxPrice;

    @JsonField(fieldName = "user_verifed",
            valueExtractFormatter = "${parser_object}.getIntValue() == 1",
            serializeCodeFormatter = "${generator_object}.writeNumberField(\"${json_fieldname}\", ${object_varname}.${field_varname} ? 1 : 0)")
    boolean verifiedUserOnly;

    private int refind = 1;  // the refind flag

    public int getDistanceRange() {
        return distanceRange;
    }

    public void setDistanceRange(int distanceRange) {
        this.distanceRange = distanceRange;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(int orderBy) {
        this.orderBy = orderBy;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }

    public boolean isVerifiedUserOnly() {
        return verifiedUserOnly;
    }

    public void setVerifiedUserOnly(boolean verifiedUserOnly) {
        this.verifiedUserOnly = verifiedUserOnly;
    }

    public int getRootCategory() {
        return rootCategory;
    }

    public void setRootCategory(int rootCategory) {
        this.rootCategory = rootCategory;
    }

    public String toQueryString() {
        StringBuilder buf = new StringBuilder("refind=").append(refind);

        if (distanceRange >= 0) {
            buf.append("&distance=").append(distanceRange);
        }

        if (category != 0) {
            buf.append("&cat_id=").append(category);
        }

        if(rootCategory != 0){
            buf.append("&root_cat_id=").append(rootCategory);
        }

        if (orderBy >= 0) {
            buf.append("&orderby=").append(orderBy);
        }

        buf.append("&user_verifed=").append(verifiedUserOnly ? 1 : 0);

        if (minPrice != null && minPrice > 0) {
            buf.append("&price_min=").append(minPrice);
        }

        if (maxPrice != null && maxPrice > 0) {
            buf.append("&price_max=").append(maxPrice);
        }

        return buf.toString();
    }

    public void setRefind(int refind) {
        this.refind = refind;
    }
}
