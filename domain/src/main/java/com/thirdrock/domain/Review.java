package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

/**
 * A user review
 * Created by ywu on 15/4/17.
 */
@JsonType
public class Review {

    @JsonField(fieldName = "id")
    int id;

    @JsonField(fieldName = "score")
    int rating;

    @JsonField(fieldName = "comment")
    String comment = "";

    @JsonField(fieldName = "created_at")
    int created;

    @JsonField(fieldName = "reviewer")
    User reviewer;

    @JsonField(fieldName = "reported")
    boolean reported;

    @JsonField(fieldName = "reply_content")
    String replyContent;

    public Review() {
    }

    public Review(int rating, String comment) {
        this.rating = rating;
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getCreated() {
        return created;
    }

    public User getReviewer() {
        return reviewer;
    }

    public boolean isReported() {
        return reported;
    }

    public void setReported(boolean reported) {
        this.reported = reported;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }
}
