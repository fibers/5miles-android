package com.thirdrock.domain;

/**
 * Types of campaign
 * Created by ywu on 14/12/9.
 */
public enum EnumCampaignType {
    UNKNOWN, LAUNCH, BANNER;

    public static EnumCampaignType valueOf(int v) {
        EnumCampaignType[] vs = EnumCampaignType.values();
        return (v >= 0 && v < vs.length) ? vs[v] : UNKNOWN;
    }
}
