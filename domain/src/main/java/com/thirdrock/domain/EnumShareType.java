package com.thirdrock.domain;

/**
 * Created by jubin on 30/12/14.
 */
public enum EnumShareType {
    INVITE, ITEM, SHOP, UNKNOWN
}
