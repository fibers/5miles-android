package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;
import com.thirdrock.protocol.SystemAction;
import com.thirdrock.protocol.SystemActionData;

import java.util.Map;

/**
 * Advertise campaign.
 * Created by ywu on 14/12/9.
 */
@JsonType
public class Campaign {
    @JsonField(fieldName = "id")
    int id;

    @JsonField(fieldName = "campaign_type")
    int typeId;

    @JsonField(fieldName = "image_url")
    String imageUrl;

    @JsonField(fieldName = "image_width")
    int imageWidth;

    @JsonField(fieldName = "image_height")
    int imageHeight;

    @JsonField(fieldName = "click_action")
    String clickAction;

    @JsonField(fieldName = "begin_at")
    int beginAt;

    @JsonField(fieldName = "end_at")
    int endAt;

    private EnumCampaignType type;
    private SystemActionData actionWrapper = new SystemActionData();
    private boolean parsed;

    public int getId() {
        return id;
    }

    public EnumCampaignType getType() {
        if (type == null) {
            type = EnumCampaignType.valueOf(typeId);
        }

        return type;
    }

    public void setType(EnumCampaignType type) {
        this.type = type;
        this.typeId = type.ordinal();
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public String getClickAction() {
        return clickAction;
    }

    public int getBeginAt() {
        return beginAt;
    }

    public int getEndAt() {
        return endAt;
    }

    public boolean isActive() {
        if (beginAt == endAt) {
            // ignore time range
            return true;
        }

        long now = System.currentTimeMillis() / 1000;
        return beginAt <= now && endAt > now;
    }

    public SystemAction getActionType(){
        doParsing();
        return actionWrapper.getAction();
    }

    public String getActionData(){
        doParsing();
        return actionWrapper.getActionData();
    }

    protected void doParsing(){
        if (parsed) {
            return;
        }

        try {
            // parse action & extract data
            actionWrapper = SystemActionData.valueOf(clickAction);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        parsed = true;
    }

    /**
     * 判断广告是否有更新的标识
     */
    public long getVersion() {
        return (id + imageUrl).hashCode();
    }

    public Map<String, String> getExtraActionData() {
        return actionWrapper.getExtraActionData();
    }
}
