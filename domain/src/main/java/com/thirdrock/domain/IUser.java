package com.thirdrock.domain;

/**
 * Interface for user related operation
 * Created by jubin on 4/6/15.
 */
public interface IUser {
    String getId();
    String getNickname();
    boolean isVerified();
    String getAvatarUrl();
}
