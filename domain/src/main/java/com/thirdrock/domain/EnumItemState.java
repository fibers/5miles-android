package com.thirdrock.domain;


/**
 * State list of an item
 * Created by ywu on 14/11/20.
 *
 * RECEIVED and SELLER_RATED is abandoned since 2.5
 */
public enum EnumItemState {
    // RECEIVED and SELLER_RATED is abandoned since 2.5
    LISTING, SOLD, RECEIVED, SELLER_RATED, UNAPPROVED, UNAVAILABLE, UNLISTED;

    public static EnumItemState valueOf(int v) {
        EnumItemState[] vs = EnumItemState.values();
        EnumItemState state = (v >= 0 && v < vs.length) ? vs[v] : LISTING;

        // RECEIVED和SELLER_RATED目前设置为SOLD，兼容性。
        switch (state){
            case  RECEIVED:
                return SOLD;
            case SELLER_RATED:
                return SOLD;
            default:
                return state;
        }
    }

    /**
     * SOLD, RECEIVED和SELLER_RATED会返回true
     * @return
     */
    public boolean isSold() {
        return this == SOLD || this == RECEIVED || this == SELLER_RATED;
    }

}
