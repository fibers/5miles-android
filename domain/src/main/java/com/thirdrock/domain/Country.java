package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.io.Serializable;

/**
 * Country info
 * Created by ywu on 15/1/9.
 */
@JsonType
public class Country implements Serializable, Comparable<Country> {

    /**
     * Country/region code, an uppercase ISO 3166 2-letter code.
     */
    @JsonField(fieldName = "code")
    String countryCode;

    @JsonField(fieldName = "name")
    String displayName;

    /**
     * Country code for international phone call
     */
    @JsonField(fieldName = "no")
    String areaNumber;

    public String getCountryCode() {
        return countryCode;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getAreaNumber() {
        return areaNumber;
    }

    @Override
    public String toString() {
        return displayName;
    }

    @Override
    public int hashCode() {
        return countryCode.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof Country) {
            Country that = (Country) obj;
            return countryCode.equals(that.countryCode);
        }

        return false;
    }

    @Override
    public int compareTo(Country o) {
        return countryCode.compareTo(o.countryCode);
    }
}
