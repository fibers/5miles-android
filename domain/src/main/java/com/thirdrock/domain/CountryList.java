package com.thirdrock.domain;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.Collections;
import java.util.List;

/**
 * Created by ywu on 15/1/9.
 */
@JsonType
public class CountryList {

    @JsonField(fieldName = "data")
    List<Country> countries;

    public List<Country> getCountries() {
        return countries == null ? Collections.<Country>emptyList() :
                Collections.unmodifiableList(countries);
    }
}
