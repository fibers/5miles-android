package com.thirdrock.domain;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by fibers on 15/7/6.
 */
public class MyItems {

    private List<Item> items = new LinkedList<Item>();

    public List<Item> getItems() {
        return items;
    }

    public void addItems(List<Item> newItems){
        items.addAll(newItems);
    }

}
