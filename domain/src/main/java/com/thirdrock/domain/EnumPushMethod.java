package com.thirdrock.domain;

/**
 * Event notification methods.
 * Created by ywu on 14/11/3.
 */
public enum EnumPushMethod {

    UNKNOWN, PUSH, EMAIL;

    public static EnumPushMethod valueOf(int v) {
        EnumPushMethod[] vs = EnumPushMethod.values();
        return (v >= 0 && v < vs.length) ? vs[v] : UNKNOWN;
    }

}
